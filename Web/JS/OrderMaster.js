﻿var getid = "", WeightTax = 0.00;
var PackerSecurityEnable = false;
$(document).ready(function () {
    $('#bodycaption').addClass('menu-collapsed');
    $('#txtDeliveryDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true,
        min: new Date(),
        firstDay: 0
    });
    //bindZipCode();
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    getid = getQueryString('OrderNo');
    if (getid != null) {
        bindDropdown();

        editOrder(getid);

    }

    $("#ddlProduct").select2().on("select2:select", function (e) {

    });

});
var MLTaxRate = 0.00, MLTaxType = 0,WeightTaxRate=0.00;
function bindDropdown() {

    $.ajax({
        type: "POST",
        url: "orderMaster.aspx/bindAllDropdown",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var DropDown = $.parseJSON(response.d);
                for (var i = 0; i < DropDown.length; i++) {
                    var AllDropDownList = DropDown[i];
                    var Status = AllDropDownList.Status;
                    for (var p = 0; p < Status.length; p++) {
                        var StatusList = Status[p];
                        $('#txtOrderStatus').val(StatusList.StatusType);
                    }
                    var ddlShippingType = $("#ddlShippingType");
                    $("#ddlShippingType option:not(:first)").remove();
                    var ShippingTypeList = AllDropDownList.ShippingType;
                    for (var k = 0; k < ShippingTypeList.length; k++) {
                        var Shipping = ShippingTypeList[k];
                        var option = $("<option />");
                        option.html(Shipping.ShippingType);
                        option.val(Shipping.AutoId);
                        option.attr('EnabledTax', Shipping.EnabledTax)
                        ddlShippingType.append(option);
                    }
                    $("#ddlShippingType").val('1');

                }
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function LoadProducts() {
    if ($("#LoadProducts").attr('class') == "ft-plus") {
        var length = $('#ddlProduct > option').length;
        if (length == 1) {
            bindProductDropdown();
        }
    }
};
function bindProductDropdown() {
    $.ajax({
        type: "POST",
        url: "orderMaster.aspx/bindProductDropdown",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        async: false,
        success: function (response) {
            console.log(response);

            if (response.d != "Session Expired") {
                var DropDown = $.parseJSON(response.d);
                for (var i = 0; i < DropDown.length; i++) {
                    var AllDropDownList = DropDown[i];

                    var Product = AllDropDownList.Product;
                    var ddlProduct = $("#ddlProduct");
                    $("#ddlProduct option:not(:first)").remove();
                    for (var j = 0; j < Product.length; j++) {
                        var ProductList = Product[j];
                        var option = $("<option />");
                        option.html(ProductList.ProductName);
                        option.val(ProductList.AutoId);
                        option.attr('WeightOz', ProductList.WeightOz);
                        option.attr('MLQty', ProductList.MLQty);
                        ddlProduct.append(option);
                    }
                    ddlProduct.select2();


                }
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}




function BindUnittype() {

    $("#alertStockQty").hide();
    var productAutoId = $("#ddlProduct option:selected").val();
    if (productAutoId == '0') {
        $("#txtReqQty").val(1);
    }
    $.ajax({
        type: "POST",
        url: "orderMaster.aspx/bindUnitType",
        data: "{'productAutoId':" + productAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var unitDefault = $(xmldoc).find("Table1");
                var count = 0;

                $("#ddlUnitType option:not(:first)").remove();
                $.each(unitType, function () {
                    $("#ddlUnitType").append("<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                });
                if (unitDefault.length > 0) {
                    if (BUnitAutoId == 0) {
                        $("#ddlUnitType").val($(unitDefault).find('AutoId').text()).change();
                    } else {
                        $("#ddlUnitType").val(BUnitAutoId).change();
                        BUnitAutoId = 0;
                    }
                } else {
                    $("#ddlUnitType").val(0);
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
};

var price, taxRate, SRP, minPrice, CustomPrice, GP; // ---------- Global Variables ---------------

function BindunitDetails() {

    $("#btnAdd").attr('disabled', true);
    if ($("#ddlUnitType option:selected").attr('eligibleforfree') == 1) {
        $('#chkFreeItem').attr('disabled', false);
    } else {
        $('#chkFreeItem').attr('disabled', true);
    }
    $('#chkFreeItem').prop('checked', false);
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        UnitAutoId: $("#ddlUnitType").val(),
        CustAutoId: $("#ddlCustomer").val()
    };

    if (data.UnitAutoId != 0) {
        $.ajax({
            type: "POST",
            url: "orderMaster.aspx/selectQtyPrice",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {


                    var xmldoc = $.parseXML(response.d);
                    var stockAndPrice = $(xmldoc).find("Table");
                    price = parseFloat(stockAndPrice.find("Price").text()).toFixed(2);
                    minPrice = stockAndPrice.find("MinPrice").text();
                    CostPrice = stockAndPrice.find("CostPrice").text();
                    CustomPrice = stockAndPrice.find("CustomPrice").text();
                    taxRate = stockAndPrice.find("TaxRate").text();
                    SRP = stockAndPrice.find("SRP").text();
                    GP = stockAndPrice.find("GP").text();
                    $("#btnAdd").attr('disabled', false);

                    if ($("#ddlUnitType").val() != 0) {
                        $("#alertStockQty").text('');
                        if (Number($("#ddlUnitType").val()) == 1) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " case ] ," +
                                " [ Base Price : $" + price + " ]");
                        } else if (Number($("#ddlUnitType").val()) == 2) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " box ] ," +
                                " [ Base Price : $" + price + " ]");
                        } else {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " pcs ] ," +
                                " [ Base Price : $" + price + " ]");
                        }
                        $("#alertStockQty").show();
                    } else {
                        $("#alertStockQty").hide();
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
};

$('#chkIsTaxable').click(function () {
    $('#chkExchange').prop('checked', false);
    $('#chkFreeItem').prop('checked', false);
})
$('#chkExchange').click(function () {
    $('#chkIsTaxable').prop('checked', false);
    $('#chkFreeItem').prop('checked', false);
})
$('#chkFreeItem').click(function () {
    $('#chkIsTaxable').prop('checked', false);
    $('#chkExchange').prop('checked', false);
})
/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                          Add Row to table
/*-------------------------------------------------------------------------------------------------------------------------------*/
$("#btnAdd").click(function () {
    var flag1 = false;
    var validatecheck = dynamicInputTypeSelect2('selectvalidate');
    if (!validatecheck) {
        checkRequiredField();
    }
    else {
        validatecheck = checkRequiredField();
    }
    if (validatecheck) {
        var productAutoId = $("#ddlProduct option:selected").val();
        var unitAutoId = $("#ddlUnitType option:selected").val();
        var MLQty = $("#ddlProduct option:selected").attr('MLQty');
        var WeightOz = $("#ddlProduct option:selected").attr('WeightOz');
        if (CustomPrice == "") {
            UnitPrice = parseFloat(price).toFixed(2);
        } else {
            if (minPrice > CustomPrice) {
                UnitPrice = parseFloat(minPrice).toFixed(2);
            } else {
                UnitPrice = parseFloat(CustomPrice).toFixed(2);
            }
        }
        var chkIsTaxable = $('#chkIsTaxable').prop('checked');
        var chkExchange = $('#chkExchange').prop('checked');
        var chkFreeItem = $('#chkFreeItem').prop('checked');
        var IsExchange = 0, IsTaxable = 0, IsFreeItem = 0, chkcount = 0;
        if (chkIsTaxable == true) {
            IsTaxable = 1;
            chkcount = 1;
        }
        if (chkExchange == true) {
            IsExchange = 1;
            chkcount = chkcount + 1;
            MLQty = 0.00;
            WeightOz = 0.00;
        }
        if (chkFreeItem == true) {
            IsFreeItem = 1;
            chkcount = chkcount + 1;
            MLQty = 0.00;
            WeightOz = 0.00;
        }
        if (MLTaxType == 0) {
            MLQty = 0.00;
        }
        if (chkcount > 1) {
            toastr.error('Please check only one checkbox.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

            return;
        }
        if ($("#txtReqQty").val() == 0) {
            toastr.error('Required quantity can not be Zero.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

            return;
        }
        //Start
        if (UnitPrice == "undefined" || UnitPrice == "isNaN") {
            toastr.error('Invalid Product.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        if (minPrice == "undefined" || minPrice == "isNaN") {
            toastr.error('Invalid Product.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        //End 04-09-2019 By Rizwan Ahmad

        $("#emptyTable").hide();
        $("#alertStockQty").hide();

        var tr = $("#tblProductDetail tbody tr");
        for (var i = 0; i <= tr.length - 1; i++) {
            if ($(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem && $(tr[i]).find(".ProName > span").attr("productautoid") == productAutoId &&
                $(tr[i]).find(".UnitType > span").attr("unitautoid") == unitAutoId && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange) {
                var reqQty = Number($(tr[i]).find(".ReqQty input").val()) + Number($("#txtReqQty").val());
                $(tr[i]).find(".ReqQty input").val(reqQty);
                flag1 = true;

                rowCal($(tr[i]).find(".ReqQty > input"));
                $('#tblProductDetail tbody tr:first').before($(tr[i]));
                if (chkIsTaxable == true && ($(tr[i]).find(".TaxRate > span").attr("TypeTax")) == "0") {
                    toastr.success('Product has been added successfully but product has been not set taxable, now product cannot be set taxable .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
                else if (chkIsTaxable == false && ($(tr[i]).find(".TaxRate > span").attr("TypeTax")) == "1") {
                    toastr.success('Product has been added succesfully but product has already taxable,so now product has been added with taxable .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
                else {
                    toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
            }

            if (chkFreeItem) {
                if (($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == '0') && $(tr[i]).find(".UnitType > span").attr("unitautoid") == unitAutoId) {
                    flag1 = true;
                    toastr.error("You can't add different unit of added product.", 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#ddlProduct").select2('val', '0');
                    $("#ddlUnitType").val(0);
                    $("#txtReqQty").val("1");
                    return;

                }
            }
            if (chkExchange) {
                if ($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId && $(tr[i]).find(".UnitType > span").attr("unitautoid") != unitAutoId && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == '0' && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange) {
                    flag1 = true;
                    toastr.error("You can't add different unit of added product.", 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#ddlProduct").select2('val', '0');
                    $("#ddlUnitType").val(0);
                    $("#txtReqQty").val("1");
                    return;
                }
            }

            if ($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId &&
                $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange
                && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem) {
                flag1 = true;
                toastr.error("You can't add different unit of added product.", 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#ddlProduct").select2('val', '0');
                $("#ddlUnitType").val(0);
                $("#txtReqQty").val("1");
                return;
            }
        }

        if (!flag1) {
            var product = $("#ddlProduct option:selected").text().split("--");
            var row = $("#tblProductDetail thead tr").clone(true);
            $(".ProId", row).html("<span IsFreeItem='" + IsFreeItem + "'></span>" + product[0]);
            if (IsFreeItem == 1) {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");

            } else if (IsTaxable == 1) {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");

            } else if (IsExchange == 1) {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");

            } else {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>");
            }
            $(".UnitType", row).html("<span UnitAutoId='" + $("#ddlUnitType option:selected").val() + "' QtyPerUnit='" + $("#ddlUnitType option:selected").attr("QtyPerUnit") + "'>" + $("#ddlUnitType option:selected").text() + "</span>");
            if ($("#hiddenOrderStatus").val() == "3") {
                $(".ReqQty", row).html("<input type='text' maxlength='4' onkeypress='return isNumberKey(event)' class='form-control input-sm' border-primary runat='server' value='" + $("#txtReqQty").val() + "'/>");
            } else {
                $(".ReqQty", row).html("<input type='text' maxlength='4' onkeypress='return isNumberKey(event)' class='form-control input-sm border-primary text-center' runat='server' onkeyup='rowCal(this)' value='" + $("#txtReqQty").val() + "'/>");
            }
            $(".Barcode", row).text("");
            $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * parseInt($("#ddlUnitType option:selected").attr("QtyPerUnit")));
            if ($('#chkExchange').prop('checked') == false && $('#chkFreeItem').prop('checked') == false) {

                if (CustomPrice == "") {
                    $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='" + parseFloat(price).toFixed(2) + "' minprice='" + minPrice + "' BasePrice='" + price + "' />");
                } else {
                    UnitPrice = parseFloat(parseFloat(minPrice) > parseFloat(CustomPrice) ? price : CustomPrice).toFixed(2);
                    $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='" + parseFloat(parseFloat(minPrice) > parseFloat(CustomPrice) ? price : CustomPrice).toFixed(2) + "' minprice='" + minPrice + "' BasePrice='" + price + "' />");
                }
            } else {
                UnitPrice = 0.00;
                $(".UnitPrice", row).html("<input type='text' maxlength='6' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='0.00' minprice='0.00' BasePrice='0.00' disabled/>");
            }

            $(".SRP", row).text(SRP);
            $(".GP", row).text(GP);
            $(".OM_MinPrice", row).text(minPrice);
            $(".OM_CostPrice", row).text(CostPrice);
            $(".ItemType", row).text("New");
            $(".AutoId", row).text(0);
            var taxType = '', TypeTax = 0;
            if ($('#chkIsTaxable').prop('checked') == true) {
                taxType = '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>';
                TypeTax = 1;
            }
            var IsExchange1 = '';
            if ($('#chkExchange').prop('checked') == true) {
                IsExchange1 = 'Exchange'
            }

            if ($('#hiddenEmpTypeVal').val() == 8) {
                if (TypeTax == 1) {
                    $(".TaxRate", row).html("<input type='checkbox' onclick='SetIsTax(this)' checked/><span WeightOz='" + WeightOz + "' MLQty='" + MLQty + "' TypeTax='" + TypeTax + "'> </span>" + taxType);
                } else {
                    $(".TaxRate", row).html("<input type='checkbox' onclick='SetIsTax(this)'  /><span WeightOz='" + WeightOz + "' MLQty='" + MLQty + "' TypeTax='" + TypeTax + "'> </span>");
                }
            }
            else {
                $(".TaxRate", row).html("<span WeightOz='" + WeightOz + "' MLQty='" + MLQty + "' TypeTax='" + TypeTax + "'> </span>" + taxType);
            }
            if (IsExchange == 1) {
                $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>" + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
            } else {
                $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>");
            }
            $(".NetPrice", row).text("0.00");
            $(".QtyRemain", row).text("0");
            $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-delete'></span></a>");
            if ($('#tblProductDetail tbody tr').length > 0) {
                $('#tblProductDetail tbody tr:first').before(row);
            }
            else {
                $('#tblProductDetail tbody').append(row);
            }
            rowCal(row.find(".ReqQty > input"));
            toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

            var Draftdata = {
                CustomerAutoId: $("#ddlCustomer").val(),
                DraftAutoId: $("#DraftAutoId").val(),
                ShippingType: $("#ddlShippingType").val(),
                DeliveryDate: $("#txtDeliveryDate").val(),
                Remarks: $("#txtOrderRemarks").val(),
                productAutoId: productAutoId,
                unitAutoId: unitAutoId,
                ReqQty: $("#txtReqQty").val(),
                QtyPerUnit: $("#ddlUnitType option:selected").attr("QtyPerUnit"),
                UnitPrice: UnitPrice,
                minprice: minPrice,
                SRP: SRP,
                GP: GP,
                IsTaxable: IsTaxable,
                IsExchange: IsExchange,
                IsFreeItem: IsFreeItem,
                MLQty: MLQty
            }
            $.ajax({
                type: "POST",
                url: "orderMaster.aspx/SaveDraftOrder",
                data: "{'dataValues':'" + JSON.stringify(Draftdata) + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                async: false,
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d != "false") {
                            $("#DraftAutoId").val(response.d);
                        }
                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }

        $('#chkIsTaxable').prop('checked', false);
        $('#chkExchange').prop('checked', false);
        $('#chkFreeItem').prop('checked', false);
        $("#txtBarcode").val('');
        $("#txtBarcode").focus();
        $("#ddlProduct").select2('val', '0');
        $("#ddlUnitType").val(0);
        $("#txtReqQty").val("1");
    }
    else {
        toastr.error('All * fields are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                          Calculations
/*-------------------------------------------------------------------------------------------------------------------------------*/
function rowCal(e) {

    var row = $(e).closest("tr");
    if ($("#DraftAutoId").val() != '') {
        var Draftdata = {
            DraftAutoId: $("#DraftAutoId").val(),
            ProductAutoId: $(row).find('.ProName span').attr('ProductAutoId'),
            UnitAutoId: $(row).find('.UnitType span').attr('UnitAutoId'),
            ReqQty: $(row).find('.ReqQty input').val(),
            UnitPrice: $(row).find('.UnitPrice input').val(),
            IsExchange: $(row).find('.IsExchange span').attr('isexchange'),
            IsFreeItem: $(row).find('.ProId span').attr('isfreeitem'),
        }
        $.ajax({
            type: "POST",
            url: "orderMaster.aspx/UpdateDraftReq",
            data: "{'dataValues':'" + JSON.stringify(Draftdata) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d != "false") {
                        var totalPcs = (Number($(e).val()) * Number(row.find(".UnitType span").attr("QtyPerUnit")));
                        row.find(".TtlPcs").text(totalPcs);

                        row.find(".QtyRemain").text(Number(row.find(".ReqQty input").val()) - Number($(e).val()));

                        //SRP = Number(row.find(".SRP").text());
                        price = Number(row.find(".UnitPrice input").val());
                        //GP = ((SRP - price) / SRP * 100).toFixed(2);
                        //row.find(".GP").text(GP);

                        if (Number($("#hiddenEmpTypeVal").val()) > 2 && Number($("#hiddenEmpTypeVal").val()) < 5) {
                            var netPrice = (price * Number(row.find(".QtyShip input").val()));
                        } else {
                            var netPrice = (price * Number(row.find(".ReqQty input").val()));
                        }
                        console.log(netPrice.toFixed(2))
                        row.find(".NetPrice").text(netPrice.toFixed(2));
                        calTotalAmount();
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });

    } else {

        var totalPcs = (Number($(e).val()) * Number(row.find(".UnitType span").attr("QtyPerUnit")));
        row.find(".TtlPcs").text(totalPcs);
        row.find(".QtyRemain").text(Number(row.find(".ReqQty input").val()) - Number($(e).val()));
        price = Number(row.find(".UnitPrice input").val());
        if (Number($("#hiddenEmpTypeVal").val()) > 2 && Number($("#hiddenEmpTypeVal").val()) < 5) {
            var netPrice = (price * Number(row.find(".QtyShip input").val()));
        } else {
            var netPrice = (price * Number(row.find(".ReqQty input").val()));
        }
        row.find(".NetPrice").text(netPrice.toFixed(2));
        calTotalAmount();
    }


}

function changePrice(e) {
    var row = $(e).closest("tr");
    rowCal(row.find(".ReqQty > input"));
    //if (Number($(e).val()) < Number($(e).attr("minprice"))|| Number($(e).val()) > Number($(e).attr("BasePrice"))) {
    if (Number($(e).val()) < Number($(e).attr("minprice"))) {
        $(e).css("background-color", "#FFCCBC").addClass("PriceNotOk");
    } else {
        $(e).css("background-color", "#FFF").removeClass("PriceNotOk");
    }
}

function calTotalAmount() {
    var total = 0.00;

    if (Number($("#hiddenEmpTypeVal").val()) == 3 || Number($("#hiddenEmpTypeVal").val()) == 8) {
        $("#tblProductDetail tbody tr").each(function () {
            total += Number($(this).find(".NetPrice").text());
        });
    } else {
        $("#tblProductDetail tbody tr").each(function () {
            total += Number($(this).find(".UnitPrice input").val()) * Number($(this).find(".ReqQty input").val());
        });
    }

    $("#txtTotalAmount").val(total.toFixed(2));
    calOverallDisc1();
}

function calTotalTax() {
   
    var totalTax = 0.00, qty = 0;
    var MLQty = 0.00, WeightOzQty = 0;
    var EnabledTax = $("#ddlShippingType option:selected").attr('enabledtax');
    $("#tblProductDetail tbody tr").each(function () {
        if (Number($("#hiddenEmpTypeVal").val()) == 3 || Number($("#hiddenEmpTypeVal").val()) == 8) {
            qty = Number($(this).find(".QtyShip input").val());
        } else {
            qty = Number($(this).find(".ReqQty input").val());
        }
        MLQty = parseFloat(MLQty) + (parseFloat($(this).find('.TaxRate span').attr('MLQty')) * qty * parseFloat($(this).find('.UnitType span').attr('qtyperunit')));

        WeightOzQty += (parseFloat($(this).find('.TaxRate span').attr('WeightOz')) * qty * parseFloat($(this).find('.UnitType span').attr('qtyperunit')));
        if ($(this).find('.TaxRate span').attr('typetax') == 1) {
            var totalPrice = Number($(this).find(".NetPrice").text());
            var Disc = totalPrice * Number($("#txtOverallDisc").val()) * 0.01;

            var priceAfterDisc = totalPrice - Disc;
            if ($('#ddlTaxType option:selected').attr("taxvalue") != undefined) {
                totalTax += priceAfterDisc * Number($('#ddlTaxType option:selected').attr("taxvalue")) * 0.01;
            }
        }
    });
    $("#txtTotalTax").val(totalTax.toFixed(2));
    if (Number(EnabledTax) == 1) {
        $("#txtMLQty").val(MLQty.toFixed(2));
        $("#txtMLTax").val((MLQty * MLTaxRate).toFixed(2));
        $("#txtWeightTax").val((WeightOzQty * WeightTax).toFixed(2));
        $("#txtWeightQuantity").val(WeightOzQty.toFixed(2));
    }
    else {
        $("#txtWeightTax").val('0.00');
        $("#txtMLTax").val('0.00');
        $("#txtMLQty").val('0');
        $("#txtWeightQuantity").val('0');
    }

    calGrandTotal();
}


function calOverallDisc1() {

    var DiscAmt = parseFloat($("#txtDiscAmt").val()) || 0.00;
    var TotalAmount = (Number($("#txtTotalAmount").val()));
    if (parseFloat(DiscAmt) > parseFloat(TotalAmount)) {
        $("#txtDiscAmt").val('0.00');
        $("#txtOverallDisc").val('0.00');
    }
    else {
        if (parseFloat(TotalAmount) == 0) {
            $("#txtOverallDisc").val('0.00');
        } else {
            var per = (DiscAmt / TotalAmount) * 100;
            $("#txtOverallDisc").val(per.toFixed(2));
        }
        calTotalTax();
    }
}
function calOverallDisc() {
    var factor = 0.00;
    if ($("#txtOverallDisc").val() == "") {
        $("#txtOverallDisc").val('0.00');
    }
    else if (parseFloat($("#txtOverallDisc").val()) > 100) {
        $("#txtOverallDisc").val('0.00');
        toastr.error('Discount can not be greater than 100%.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    factor = Number($("#txtOverallDisc").val()) * 0.01 || 0.00;
    $("#txtDiscAmt").val((Number($("#txtTotalAmount").val()) * factor).toFixed(2));
    calTotalTax();
}

function calGrandTotal() {
    var DiscAmt = 0.00;
    if ($("#txtDiscAmt").val() != "") {
        DiscAmt = parseFloat($("#txtDiscAmt").val())
    }
    var Shipping = 0.00;
    if ($("#txtShipping").val() != "") {
        Shipping = parseFloat($("#txtShipping").val())
    }

    var grandTotal = parseFloat($("#txtTotalAmount").val()) - parseFloat(DiscAmt) + parseFloat(Shipping)
        + parseFloat($("#txtTotalTax").val()) + parseFloat($("#txtMLTax").val()) + parseFloat($("#txtWeightTax").val());
    var round = Math.round(grandTotal);
    $("#txtAdjustment").val((round - grandTotal).toFixed(2));
    $("#txtGrandTotal").val(round.toFixed(2));
    funcheckprop();

}


function deleteItemrecord(e) {
    swal({
        title: "Are you sure?",
        text: "You want to delete this product.",
        icon: "warning",
        showCancelButton: true,
        allowOutsideClick: false,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleterow(e);
        } 
    })
}

function deleterow(e) {
    
    var tr = $(e).closest('tr');

    var data = {
        DraftAutoId: $("#DraftAutoId").val(),
        ProductAutoId: $(tr).find('.ProName span').attr('ProductAutoId'),
        UnitAutoId: $(tr).find('.UnitType span').attr('UnitAutoId'),
        isfreeitem: $(tr).find('.ProId span').attr('isfreeitem'),
        IsExchange: $(tr).find('.IsExchange span').attr('IsExchange')
    }
    if ($("#DraftAutoId").val() != '') {
        $.ajax({
            type: "POST",
            url: "orderMaster.aspx/DeleteDraftItem",
            data: "{'DataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {

                    swal("", "Product deleted successfully.", "success");
                } else {
                    location.href = '/';
                }
                $(e).closest('tr').remove();

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        $(e).closest('tr').remove();
        swal("", "Item deleted successfully.", "success");
    }


    calTotalAmount();
    if ($("#tblProductDetail tbody tr").length == 0) {
        $("#emptyTable").show();
    } else {
        $("#emptyTable").hide();
    }
}
/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                          Generate Order
/*-------------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                     Edit Order Details
var ThisOrderCreditAmount = 0.00;
/*-------------------------------------------------------------------------------------------------------------------------------*/
function editOrder(orderNo) {
    
    $.ajax({
        type: "POST",
        url: "orderMaster.aspx/editOrder",
        data: "{'orderNo':'" + orderNo + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var RemarksDetails = $(xmldoc).find("Table");
                var order = $(xmldoc).find("Table1");
                var items = $(xmldoc).find("Table2");
                var ProductItme = $(xmldoc).find("Table3");
                var TaxMaster = $(xmldoc).find("Table5");
                MLTaxRate = TaxMaster.find('MLTaxRate').text();
                WeightTaxRate = TaxMaster.find('WeigthTaxRate').text();
                MLTaxType = TaxMaster.find('MLTaxType').text();
                $("#Table2 tbody tr").remove();
                if ($(RemarksDetails).length > 0) {
                    $('#Div2').show();

                    var rowtest = $("#Table2 thead tr").clone();
                    $.each(RemarksDetails, function (index) {
                        $(".SRNO", rowtest).text((Number(index) + 1));
                        $(".EmployeeName", rowtest).text($(this).find("EmpName").text());
                        $(".EmployeeType", rowtest).text($(this).find("EmpType").text());
                        $(".Remarks", rowtest).html('<p style="width:335px !Important;white-space: normal;text-align:justify;margin:0;">'+$(this).find("Remarks").text()+'</p>');
                        $("#Table2 tbody").append(rowtest);
                        rowtest = $("#Table2 tbody tr:last").clone(true);
                    })
                } else {
                    $('#Div2').hide();
                }
                $("#btnUpdateOrder").show();
                $("#btnReset").show();
                var ProductItems = [];
                $("#ddlitemproduct option:not(:first)").remove();

                $.each(ProductItme, function () {
                    ProductItems.push({
                        AutoId: $(this).find("AutoId").text(),
                        UnitType: $(this).find("UnitTypeAutoId").text(),
                        ProductName: $(this).find("ProductName").text()
                    });
                    $("#ddlitemproduct").append($("<option value='" + $(this).find("AutoId").text() + "' unitautoid='" + $(this).find("UnitTypeAutoId").text() + "'>" + $(this).find("ProductName").text() + "</option>"));
                });
                localStorage.setItem("ProductItems", JSON.stringify(ProductItems))
                $("#ddlitemproduct").select2();

                var orderStatus = Number($(order).find("StatusCode").text());

                $("#hiddenStatusCode").val($(order).find("StatusCode").text());
                $("#txtHOrderAutoId").val($(order).find("AutoId").text());
                $("#txtOrderId").val($(order).find("OrderNo").text());
                $("#txtOrderRemarks").val($(order).find("ManagerRemarks").text());
                $("#txtPackerRemarks").val($(order).find("PackerRemarks").text());
                //$("#txtManagerRemarks").val($(order).find("ManagerRemarks").text());
                $("#txtOrderDate").val($(order).find("OrderDate").text());
                $('#txtAssignDate').pickadate({
                    format: 'mm/dd/yyyy',
                    formatSubmit: 'mm/dd/yyyy',
                    selectYears: true,
                    selectMonths: true,
                    min: -7,
                    firstDay: 0
                });
                $("#txtCustomer").val($(order).find("CustomerName").text());

                $("#ddlCustomer").val($(order).find("CustomerAutoId").text());
                $("#txtCustomerType").val($(order).find("CustomerType").text());

                var TaxTypeMaster = $(xmldoc).find("Table4");
                $("#ddlTaxType option").remove();
                $.each(TaxTypeMaster, function () {
                    $("#ddlTaxType").append("<option TaxValue='" + $(this).find("Value").text() + "' value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TaxableType").text() + "-(" + $(this).find("Value").text() + ")</option>");
                });
                $("#ddlTaxType").val($(order).find("TaxType").text());
                $("#txtDeliveryDate").val($(order).find("DeliveryDate").text());
                $("#txtOrderStatus").val($(order).find("Status").text());
                $("#hiddenOrderStatus").val($(order).find("StatusCode").text());

                $("#txtTerms").val($(order).find("TermsDesc").text());
                $("#ddlShippingType").val($(order).find("ShippingType").text()).change();
                $("#txtTimes").val($(order).find("Times").text());
                $("#txtHTimes").val($(order).find("Times").text());
                $("#txtHWarehouseRemark").val($(order).find("WarehouseRemarks").text());
                $("#txtAdjustment").val($(order).find("AdjustmentAmt").text());
                $("#txtWarehouseRemark").val($(order).find("WarehouseRemarks").text());

                $("#hiddenBillAddrAutoId").val($(order).find("BillAddrAutoId").text());
                $("#txtBillAddress").val($(order).find("BillAddr").text());                

                $("#hiddenShipAddrAutoId").val($(order).find("ShipAddrAutoId").text());
                $("#txtShipAddress").val($(order).find("ShipAddr").text());                

                $("#txtTotalAmount").val($(order).find("TotalAmount").text());
                $("#txtDiscAmt").val($(order).find("OverallDiscAmt").text());

                if ($("#ddlShippingType option:selected").attr('enabledtax') == 1) {
                    $("#txtMLQty").val($(order).find("MLQty").text());
                    $("#txtMLTax").val($(order).find("MLTax").text());
                    $("#txtWeightQuantity").val($(order).find("Weigth_OZQty").text());
                    $("#txtWeightTax").val($(order).find("Weigth_OZTaxAmount").text());
                }

                if ($(order).find("Weigth_OZTax").text() != '') {
                    WeightTax = $(order).find("Weigth_OZTax").text();
                }
                $("#txtTotalTax").val($(order).find("TotalTax").text());
                $("#txtGrandTotal").val($(order).find("GrandTotal").text());
                $("#txtDeductionAmount").val($(order).find("DeductionAmount").text());
                $("#CreditMemoAmount").text($(order).find("CustomerCredit").text());

                if ($(order).find("CreditAmount").text() != "") {
                    $("#txtStoreCreditAmount").val($(order).find("CreditAmount").text());


                }
                if (parseFloat($(order).find("CreditAmount").text()) < 0) {
                    ThisOrderCreditAmount = parseFloat($(order).find("CreditAmount").text().replace('-', ''));
                }

                if ($(order).find('PayableAmount').text() != '') {
                    $("#txtPaybleAmount").val($(order).find("PayableAmount").text());
                } else {
                    $("#txtPaybleAmount").val($(order).find("GrandTotal").text());
                }
                if ($(order).find("DeductionAmount").text() != "")
                    $("#txtDeductionAmount").val($(order).find("DeductionAmount").text());
                $("#txtAmtPaid").val($(order).find("AmtPaid").text());
                $("#txtAmtDue").val($(order).find("AmtDue").text());
                $("#txtOverallDisc").val($(order).find("OverallDisc").text());
                $("#txtShipping").val($(order).find("ShippingCharges").text());
                $("#tblProductDetail tbody tr").remove();
                var row = $("#tblProductDetail thead tr").clone(true);
                $.each(items, function () {

                    $(".ProId", row).html("<span isFreeItem='" + $(this).find("isFreeItem").text() + "' ItemAutoId='" + $(this).find("ItemAutoId").text() + "'></span><productid>" + $(this).find("ProductId").text() + "</productid>");

                    if ($(this).find("isFreeItem").text() == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'></span><productName>" + $(this).find("ProductName").text() + "</productName> <item class='badge badge badge-pill badge-success'>Free</item>");
                    }
                    else if ($(this).find("IsExchange").text() == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'></span><productName>" + $(this).find("ProductName").text() + "</productName> <item class='badge badge badge-pill badge-primary'>Exchange</item>");
                    }
                    else if ($(this).find("Tax").text() == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'></span><productName>" + $(this).find("ProductName").text() + "</productName> <item class='badge badge badge-pill badge-danger'>Taxable</item>");
                    }
                    else {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'></span><productName>" + $(this).find("ProductName").text() + "</productName> <item class='badge badge badge-pill'></item>");
                    }
                    $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                    $(".ReqQty", row).html("<input type='text' maxlength='4' class='form-control input-sm border-primary text-center' onkeypress='return isNumberKey(event)' runat='server' onkeyup='rowCal(this)' value='" + $(this).find("RequiredQty").text() + "'/>");
                    $(".PerPrice", row).text($(this).find("PerpiecePrice").text());
                    if (orderStatus == 2) {
                        $(".TtlPcs", row).text("0");
                        $(".QtyRemain", row).text($(this).find("RequiredQty").text());
                    } else {
                        $(".TtlPcs", row).text($(this).find("TotalPieces").text());
                        $(".QtyRemain", row).text($(this).find("RemainQty").text());
                    }
                    if (orderStatus == 9) {
                        if ($(this).find("QtyShip").text() > 0) {
                            $(row).hide();
                        } else {
                            $(row).show();
                        }
                    }
                    $(".NetPrice", row).text($(this).find("NetPrice").text());
                    if (Number($(this).find("IsExchange").text()) == 0 && Number($(this).find("isFreeItem").text()) == 0) {
                        $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' onkeypress='return isNumberDecimalKey(event,this)' style='text-align:right;width:70px;' maxlength='6' value='" + $(this).find("UnitPrice").text() + "' onkeyup='changePrice(this)' minprice='" + $(this).find("MinPrice").text() + "' baseprice='" + $(this).find("Price").text() + "'/>");
                    } else {
                        $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' onkeypress='return isNumberDecimalKey(event,this)' style='text-align:right;width:70px;' maxlength='6'  value='" + $(this).find("UnitPrice").text() + "' disabled onkeyup='changePrice(this)' minprice='" + $(this).find("MinPrice").text() + "' baseprice='" + $(this).find("Price").text() + "'/>");
                    }

                    $(".SRP", row).text($(this).find("SRP").text());
                    $(".GP", row).text($(this).find("GP").text());
                    $(".OM_MinPrice", row).text($(this).find("OM_MinPrice").text());
                    $(".OM_CostPrice", row).text($(this).find("OM_CostPrice").text());
                    $(".ItemType", row).text("Old");
                    $(".AutoId", row).text($(this).find("ItemAutoId").text());


                    if (Number($(this).find("IsExchange").text()) == 0 && $(this).find("isFreeItem").text() == 0) {
                        if (Number($(this).find("Tax").text()) == 1) {
                            $(".TaxRate", row).html('<input type="checkbox" onclick="SetIsTax(this)"  checked /><span typetax="1" MLQty="' + $(this).find('UnitMLQty').text() + '" WeightOz="' + $(this).find('WeightOz').text() + '"></span>');
                        } else {
                            $(".TaxRate", row).html('<input type="checkbox" onclick="SetIsTax(this)" /><span typetax="0" MLQty="' + $(this).find('UnitMLQty').text() + '" WeightOz="' + $(this).find('WeightOz').text() + '"></span>');
                        }
                    } else {
                        if (Number($(this).find("Tax").text()) == 1) {
                            $(".TaxRate", row).html('<span typetax="1" MLQty="' + $(this).find('UnitMLQty').text() + '" WeightOz="' + $(this).find('WeightOz').text() + '"></span>' + '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>');
                        } else {
                            $(".TaxRate", row).html('<span typetax="0" MLQty="' + $(this).find('UnitMLQty').text() + '" WeightOz="' + $(this).find('WeightOz').text() + '"></span>');
                        }
                    }
                    if (Number($(this).find("IsExchange").text()) == 1) {
                        $(".IsExchange", row).html('<span IsExchange="1"></span>' + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                    } else {
                        $(".IsExchange", row).html('<span IsExchange="0"></span>');
                    }

                    $(".Barcode", row).text($(this).find("Barcode").text());

                    if (orderStatus == 0) {
                        $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='EditOrderItem(this)'><span class='ft-edit'></span></a>&nbsp;&nbsp;<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-delete'></span></a>");
                    }
                    else {
                        $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-delete'></span></a>");
                    }

                    $('#tblProductDetail tbody').append(row);
                    $("#tblProductDetail tbody").append(row);
                    row = $("#tblProductDetail tbody tr:last").clone(true);

                });



                $("#panelProduct").hide();
                $("#btnUpdate").hide();
                $("#btnUpdateOrder").hide();
                $("#tblProductDetail").find(".Action").hide();
                $("#popBillAddr").hide();
                $("#popShipAddr").hide();
                $("#hidenopackedbox").show();
                $("#btnCancelOrder").show();
                if (orderStatus == 0) {
                    $("#btnUpdateOrder").show();
                    $("#panelProduct").show();
                    $("#btnApproveOrder").show();
                    $("#tblProductDetail").find(".Action").show();
                    $("#ddlProduct").attr('disabled', false);
                    $("#ddlUnitType").attr('disabled', false);
                    $("#txtReqQty").attr('disabled', false);

                    $("#tblProductDetail tbody tr").each(function () {

                        if ($(this).find('.ProId span').attr('isfreeitem') == 1 || $(this).find('.IsExchange span').attr('isexchange') == 1) {
                            $(this).find(".UnitPrice input").attr("disabled", true);
                            $(this).find(".ReqQty input").attr("disabled", false);
                        }
                        else {
                            $(this).find(".UnitPrice input").attr("disabled", false);
                            $(this).find(".ReqQty input").attr("disabled", false);
                        }

                    });

                    $("#txtOverallDisc").attr('disabled', false);
                    $("#txtDiscAmt").attr('disabled', false);
                    $("#txtShipping").attr('disabled', false);
                    $("#txtBarcode").attr('disabled', false);
                }

                $("#txtReadBorCode").removeAttr("disabled");

            } else {
                location.href = '/';
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}

function EditOrderItem(e) {


    var tr = $(e).closest('tr');
    var ItemAutoId = $(tr).find('.ProId span').attr('ItemAutoId');
    $('#txtProductId').val($(tr).find('.ProId productid').html());
    $('#txtProductName').val($(tr).find('.ProName productName').html());
    if ($(tr).find('.TaxRate span').attr('typetax') == '1') {
        $('#chkTaxablePop').prop('checked', true);
    } else {
        $('#chkTaxablePop').prop('checked', false);
    }
    if ($(tr).find('.IsExchange span').attr('isexchange') == '1') {
        $('#chkExchangePop').prop('checked', true);
    } else {
        $('#chkExchangePop').prop('checked', false);
    }
    if ($(tr).find('.ProId span').attr('isfreeitem') == '1') {
        $('#chkFreePop').prop('checked', true);
    } else {
        $('#chkFreePop').prop('checked', false);
    }

    $('#ItemAutoId').val(ItemAutoId);
    $('#PopChangeItemType').modal('show');
    $('#txtProductId').attr('disabled', true);
    $('#txtProductName').attr('disabled', true);

}
function CloseCheckPop() {
    $('#PopChangeItemType').modal('hide');
}

function updateIteminList() {
    if ($("#ddlTaxType").val() == '0') {
        swal("", "Tax Type is not available for this Customer", "error");
    } else {
        swal({
            title: "Are you sure?",
            text: "You want to change Item Type.",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, cancel!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes, Change it!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                updateItemTypefromDB();
            } else {
                $("#PopChangeItemType").modal('hide');
            }
        })
    }
}

function updateItemTypefromDB() {

    var Exchange = 0, FreeItem = 0, Taxable = 0;
    if ($("#chkExchangePop").prop('checked')) {
        Exchange = 1;
    }
    if ($("#chkFreePop").prop('checked')) {
        FreeItem = 1;
    }
    if ($("#chkTaxablePop").prop('checked')) {
        Taxable = 1;
    }
    var dataValue = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        ItemAutoId: $("#ItemAutoId").val(),
        IsExchange: Exchange,
        IsFreeItem: FreeItem,
        IsTaxable: Taxable
    }
    $.ajax({
        type: "POST",
        url: "orderMaster.aspx/ItemTypeUpdate",
        data: "{'dataValue':'" + JSON.stringify(dataValue) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = "/";
            } else if (response.d == 'false') {
                swal("Error!", "Oops! Something went wrong.Please try later.", "error")
            } else {
                var xmldoc = $.parseXML(response.d);
                var order = $(xmldoc).find("Table");
                
                $("#tblProductDetail tbody tr").each(function () {

                    if ($(this).find('.ProId span').attr('itemautoid') == $('#ItemAutoId').val()) {
                        $(this).find('.ProName item').removeClass("badge-primary");
                        $(this).find('.ProName item').removeClass("badge-success");
                        $(this).find('.ProName item').removeClass("badge-danger");

                        $(this).find('.TaxRate span').attr('typetax', 0);
                        $(this).find('.IsExchange span').attr('isexchange', 0);
                        $(this).find('.ProId span').attr('isfreeitem', 0);

                        if (Exchange == 0 && FreeItem == 0) {
                            if ($(order).find('CustomPrice').text() == "") {
                                $(this).find(".UnitPrice input").removeAttr('disabled');
                                $(this).find(".UnitPrice input").val($(order).find('Price').text());
                                $(this).find(".NetPrice").text(parseFloat(parseFloat($(order).find('Price').text()) * parseInt($(this).find(".ReqQty input").val())).toFixed(2));
                            } else {
                                $(this).find(".UnitPrice input").removeAttr('disabled');
                                $(this).find(".UnitPrice input").val(parseFloat(parseFloat($(order).find('MinPrice').text()) > parseFloat($(order).find('CustomPrice').text()) ? $(order).find('Price').text() : $(order).find('CustomPrice').text()).toFixed(2));
                                $(this).find(".NetPrice").text(parseFloat(parseFloat($(this).find(".UnitPrice input").val()) * parseInt($(this).find(".ReqQty input").val())).toFixed(2));
                            }
                        } else {
                            $(this).find(".UnitPrice input").attr('disabled', 'disabled');
                            $(this).find(".UnitPrice input").val('0.00');
                            $(this).find(".NetPrice").text('0.00');

                        }
                        if (Exchange == 1) {
                            $(this).find('.ProName item').addClass("badge-primary");
                            $(this).find('.ProName item').html('Exchange');
                            $(this).find('.IsExchange span').attr('isexchange', 1);
                        } else if (FreeItem == 1) {
                            $(this).find('.ProName item').addClass("badge-success");
                            $(this).find('.ProName item').html('Free');
                            $(this).find('.ProId span').attr('isfreeitem', 1);
                        } else if (Taxable == 1) {
                            $(this).find('.ProName item').addClass("badge-danger");
                            $(this).find('.ProName item').html('Taxable');
                            $(this).find('.TaxRate span').attr('typetax', 1);
                        } else {
                            $(this).find('.ProName item').html('');
                        }
                    }
                });
                calTotalAmount();
                swal("", "Item changed successfully.", "success").then(function () {
                    $("#PopChangeItemType").modal('hide');
                });
            }
        },
        error: function (result) {
        },
        failure: function (result) {

        }
    })
}

function SetIsTax(e) {
    if (confirm('Are you sure you want to change.')) {
        if ($(e).prop('checked')) {
            $(e).closest('td').find('span').attr('typetax', '1');
        } else {
            $(e).closest('td').find('span').attr('typetax', '0');
        }
    } else {
        if ($(e).closest('td').find('span').attr('typetax') == '1') {
            $(e).prop('checked', true);
        } else {
            $(e).prop('checked', false);
        }
    }
    calTotalAmount();
};

function funcheckprop(e) {
    $('#txtStoreCreditAmount').attr('disabled', true);
    if ($("#txtStoreCreditAmount").val() == '') {
        $("#txtStoreCreditAmount").val('0.00');
    }
    if ($(e).attr('id') != 'txtStoreCreditAmount') {
        $("#txtStoreCreditAmount").val('0.00');
    }
    var tr = $(e).closest('tr');
    var deductionAmount = 0.00;
    var GrandTotal = $("#txtGrandTotal").val();
    var CreditLimit = parseFloat($("#CreditMemoAmount").text());
    var payable = 0.00;
    $("#tblCreditMemoList tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            deductionAmount += parseFloat($(this).find('.ReturnValue').text());
        }
    });

    $("#txtDeductionAmount").val(parseFloat(deductionAmount).toFixed(2));
    if (parseFloat(deductionAmount) > parseFloat(GrandTotal)) {
        payable = 0.00;
        $("#txtStoreCreditAmount").val(parseFloat(GrandTotal).toFixed(2) - parseFloat(deductionAmount).toFixed(2));
    } else {
        payable = parseFloat(GrandTotal) - parseFloat(deductionAmount);
    }
    $("#txtPaybleAmount").val(payable.toFixed(2));
}


function itemProduct() {
    if (!PackerSecurityEnable) {
        $("#txtSecurity").val('');
        $("#txtSecurity").removeAttr('disabled');
        $('#SecurityEnabled').modal('show');
        $("#txtSecurity").focus();
    }
}

function AddProductQty() {
    if (!PackerSecurityEnable) {
        itemProduct();
        return;
    }
    if (Page_ClientValidate('AddProductQty')) {
        if ($("#txtQty").val() != '' || $("#txtQty").val() != '') {
            $.ajax({
                type: "Post",
                async: false,
                url: "orderMaster.aspx/AddProductQty",
                data: "{'ProductAutoId':'" + $("#ddlitemproduct").val() + "','UnitAutoId':'" + $("#ddlitemproduct").find(':selected').attr('unitautoid') + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        ;
                        var xmldoc = $.parseXML(response.d);
                        var product = $(xmldoc).find("Table");

                        var prodAutoId = $("#ddlitemproduct").val();
                        var unitAutoId = $("#ddlitemproduct").find(':selected').attr('unitautoid');
                        var stock = $(product).find("Stock").text();
                        var barcode = $(product).find("Barcode").text();
                        var flag1 = true, qty = 0, pieces = 0;
                        var countforscan = 0;
                        if (product.length > 0) {
                            $("#tblProductDetail tbody tr").each(function () {
                                var IsExchange = 0, IsFreeItem = 0;
                                if ($("#IsExchange").prop('checked')) {
                                    IsExchange = 1;
                                }
                                if ($("#IsFreeItem").prop('checked')) {
                                    IsFreeItem = 1;
                                }
                                if ($(this).find(".ProId span").attr("IsFreeItem") == IsFreeItem && $(this).find(".ProName span").attr("productautoid") == prodAutoId && $(this).find(".UnitType span").attr("unitautoid") == unitAutoId && IsExchange == $(this).find(".IsExchange span").attr("isexchange")) {
                                    qty = Number($("#txtQty").val()) + Number($(this).find(".QtyShip input").val());
                                    pieces = qty * Number($(this).find(".UnitType span").attr('qtyperunit'));

                                    if (pieces <= stock && qty <= Number($(this).find(".ReqQty input").val())) {
                                        $(this).find(".Barcode").text(barcode);
                                        if (qty == Number($(this).find(".ReqQty input").val())) {
                                            $(this).find(".QtyShip input").val(qty).attr("disabled", false).css("background-color", "#fff").removeClass("verify");
                                            countforscan = countforscan + 1;
                                        } else {
                                            $(this).find(".QtyShip input").val(qty).attr("disabled", false).css("background-color", "#FFECB3").addClass("verify");
                                        }

                                        $(this).find(".QtyRemain").text(Number($(this).find(".QtyRemain").text()) - qty);
                                        rowCal($(this).find(".QtyShip input"));

                                        var trfind = $(this);
                                        $('#tblProductDetail tbody tr:first').before(trfind);
                                    } else {
                                        if (pieces > stock) {
                                            $("#barcodemsg").html("Error ! Qty is not avaiable in stock");
                                            $("#BarCodenotExists").modal("show");
                                        }

                                        if (qty > Number($(this).find(".ReqQty input").val())) {
                                            $("#barcodemsg").html("Error ! Shipped Qty cannot be more than Required Qty");
                                            $("#BarCodenotExists").modal("show");

                                        }
                                    }

                                } else if ($(this).find(".ProName span").attr("productautoid") == prodAutoId) {
                                    if (Number($(this).find(".QtyShip input").val()) != Number($(this).find(".ReqQty input").val())) {
                                        countforscan = countforscan + 1;
                                    }
                                }
                            });


                            $("#tblProductDetail tbody tr").each(function () {

                                if ($(this).find(".ProName span").attr("productautoid") == prodAutoId) {
                                    if (Number($(this).find(".ReqQty input").val()) == Number($(this).find(".QtyShip input").val())) {
                                        if (countforscan == 0)
                                            countforscan = 1;
                                    } else {
                                        countforscan = 2;
                                    }
                                }
                            })
                            if (Number(countforscan) == 1) {
                                $("#ddlitemproduct option[value='" + prodAutoId + "']").remove();
                            }
                        } else {
                            $("#barcodemsg").html("Error ! Barcode does not exist");
                            $("#BarCodenotExists").modal("show");
                        }


                        $("#txtScanBarcode").val("").focus();
                        $("#txtQty").val("1");
                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        else {
            $("#barcodemsg").html("Message ! Please enter the qty");
            $("#BarCodenotExists").modal("show");
        }
    }
}



/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                          Update Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function UpdateOrder(ApproveStatus) {
    
    var flag1 = false, flag2 = false, CheckTax = false;
    var CreditNo = '';
    $("#tblCreditMemoList tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            CreditNo += $(this).find('.Action input').val() + ',';

        }
    })
    if ($("#tblProductDetail tbody tr").length > 0) {
        $("#tblProductDetail tbody tr").each(function () {
            if ($(this).find(".ReqQty input").val() == "" || $(this).find(".ReqQty input").val() == null || Number($(this).find(".ReqQty input").val()) <= 0) {
                $(this).find(".ReqQty input").addClass('border-warning');
                flag1 = true;
            } else {
                $(this).find(".ReqQty input").removeClass('border-warning');
            }

            if ($("#hiddenEmpTypeVal").val() != '7') {
                if (($(this).find('.ProId span').attr('IsFreeItem') == 0 && $(this).find('.IsExchange span').attr("isexchange") != 1) && Number($(this).find(".UnitPrice input").val()) < Number($(this).find(".UnitPrice input").attr('minprice'))) {
                    flag2 = true;
                    $(this).find(".UnitPrice input").addClass('border-warning');
                }
            }
            if ($(this).find('.TaxRate span').attr('typetax') == '1') {
                if (!$("#ddlTaxType").val()) {
                    CheckTax = true;
                }
            }
        });
        if (CheckTax == true) {
            toastr.error('Tax is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else if (!flag1 && !flag2) {
            var Product = [];

            $("#tblProductDetail tbody tr").each(function () {
                Product.push({
                    'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
                    'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
                    'QtyPerUnit': $(this).find('.UnitType').find('span').attr('QtyPerUnit'),
                    'RequiredQty': $(this).find('.ReqQty input').val(),
                    'Barcode': $(this).find('.Barcode').text(),
                    'QtyShip': 0,
                    'TotalPieces': $(this).find('.TtlPcs').text(),
                    'UnitPrice': $(this).find('.UnitPrice input').val(),
                    'SRP': $(this).find('.SRP').text(),
                    'GP': $(this).find('.GP').text(),
                    'Tax': $(this).find('.TaxRate span').attr('typetax'),
                    'IsExchange': $(this).find('.IsExchange span').attr('IsExchange'),
                    'IsFreeItem': $(this).find('.ProId span').attr('IsFreeItem'),
                    'NetPrice': $(this).find('.NetPrice').text(),
                    'OM_MinPrice': $(this).find('.OM_MinPrice').text(),
                    'OM_CostPrice': $(this).find('.OM_CostPrice').text(),
                    'ItemType': $(this).find('.ItemType').text(),
                    'AutoId': $(this).find('.AutoId').text()

                });
            });

            var orderData = {
                OrderAutoId: $("#txtHOrderAutoId").val(),
                Status: $("#hiddenStatusCode").val(),
                OrderDate: $("#txtOrderDate").val(),
                DeliveryDate: $("#txtDeliveryDate").val(),
                CustomerAutoId: $("#ddlCustomer").val(),
                Remarks: $("#txtOrderRemarks").val(),
                BillAddrAutoId: $("#hiddenBillAddrAutoId").val(),
                ShipAddrAutoId: $("#hiddenShipAddrAutoId").val(),
                TotalAmount: $("#txtTotalAmount").val(),
                OverallDisc: $("#txtOverallDisc").val(),
                OverallDiscAmt: $("#txtDiscAmt").val(),
                ShippingCharges: $("#txtShipping").val(),
                TotalTax: $("#txtTotalTax").val(),
                GrandTotal: $("#txtGrandTotal").val(),
                ShippingType: parseInt($("#ddlShippingType").val()),
                MLQty: parseFloat($("#txtMLQty").val()),
                TaxType: parseFloat($("#ddlTaxType").val()),
                ApproveStatus: ApproveStatus
            };

            $.ajax({
                type: "Post",
                url: "orderMaster.aspx/updateOrder",

                data: JSON.stringify({ TableValues: JSON.stringify(Product), orderData: JSON.stringify(orderData) }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d == 'true') {
                            if ($("#hiddenEmpTypeVal").val() == '8' && $("#hiddenStatusCode").val() > 0) {
                                swal("", "Order updated successfully.", "success").then(function () {
                                    location.href = '/Manager/Manager_viewOrder.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                                });
                            }
                            else {
                                swal("", "Order updated successfully.", "success").then(
                                    function () {
                                        location.reload();
                                    }
                                );
                            }
                        } else {
                            swal("Error !", response.d, "error");
                        }

                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                },
                failure: function (result) {
                }
            });
        } else {
            if (flag1) {

                toastr.error('Required Quantity cannot be blank and zero.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            } else {

                toastr.error('Unit Price can not be less than Min. Price.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
    } else {

        toastr.error('No Product Added in the List', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }

}


function ClosePop() {
    $('#SecurityEnvalid').modal('hide');
    if ($("#hiddenEmpTypeVal").val() == '8') {
        location.href = '/Manager/Manager_viewOrder.aspx?OrderAutoId=' + $('#txtHOrderAutoId').val();
    }
}

/*-------------------------------------------------------------------------------------------------------------------*/
//                                                  Get Address List
/*-------------------------------------------------------------------------------------------------------------------*/
var AddrType; //----Global Variable For Address Type(Value is '11' for Billing Addr and '22' for Shipping Addr)



function confirm_Cancellation_Of_Order() {
    $("#OrderBarcode").modal("toggle");
    $('#txtCancellationRemarks').val('');
    $('#txtCancellationRemarks').removeAttr('disabled');
    $('#txtCancellationRemarks').removeClass('border-warning');
}

function cancelOrder() {
    
    var data={
        OrderAutoId: $("#txtHOrderAutoId").val(),
        Remarks: $('#txtCancellationRemarks').val()
    }
    $.ajax({
        type: "Post",
        url: "orderMaster.aspx/cancelOrder",
        data: JSON.stringify({dataValues:JSON.stringify(data)}),        
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var status = $(xmldoc).find("Table");
                $("#txtOrderStatus").val($(status).find("StatusType").text());
                $("#hiddenOrderStatus").val($(status).find("Status").text());
                $("#btnEditOrder").hide();
                $("#btnCancelOrder").hide();
                $("#btnAsgnDrv").hide();
                $("#OrderBarcode").modal("toggle");
                swal("", "Order has been cancelled successfully.", "success").then(function () {
                    location.href = '/Web/WebOrderList.aspx';
                });

            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function InvalidBarcode() {
    $("#txtBarcode").val("");
    $("#txtBarcode").focus();
}
var BUnitAutoId = 0;
function readBarcode() {
    ;
    var Barcode = $("#txtBarcode").val();
    if (Barcode != "") {
        var chkIsTaxable = $('#chkIsTaxable').prop('checked');
        var chkExchange = $('#chkExchange').prop('checked');
        var chkFreeItem = $('#chkFreeItem').prop('checked');
        var IsExchange = 0, IsTaxable = 0, IsFreeItem = 0;
        if (chkIsTaxable == true) {
            IsTaxable = 1;
        }
        if (chkExchange == true) {
            IsExchange = 1;
        }
        if (chkFreeItem == true) {
            IsFreeItem = 1;

        }
        var data = {
            CustomerAutoId: $("#ddlCustomer").val(),
            Barcode: Barcode,
            DraftAutoId: $("#DraftAutoId").val(),
            ShippingType: $("#ddlShippingType").val(),
            DeliveryDate: $("#txtDeliveryDate").val(),
            ReqQty: '1',
            IsTaxable: IsTaxable,
            IsExchange: IsExchange,
            IsFreeItem: IsFreeItem
        }
        $.ajax({
            type: "POST",
            url: "orderMaster.aspx/GetBarDetails",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
               
                if (response.d == "Free not allow") {
                    $("#txtBarcode").removeAttr('onchange', 'readBarcode()');
                    $("#txtBarcode").attr('onchange', 'InvalidBarcode()');
                    swal({
                        title: "Error!",
                        text: "Product can not be sold as free",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            $("#txtBarcode").removeAttr('onchange', 'InvalidBarcode()');
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                            $("#txtBarcode").attr('onchange', 'readBarcode()');
                        }
                    });
                    $("#txtBarcode").val('');
                    $("#txtBarcode").focus();
                }
                else if (response.d == "Invalid Barcode") {
                    $("#txtBarcode").removeAttr('onchange', 'readBarcode()');
                    $("#txtBarcode").attr('onchange', 'InvalidBarcode()');
                    swal({
                        title: "Error!",
                        text: "Barcode does not exists.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            $("#txtBarcode").removeAttr('onchange', 'InvalidBarcode()');
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                            $("#txtBarcode").attr('onchange', 'readBarcode()');
                        }
                    });
                    $("#txtBarcode").val('');
                    $("#txtBarcode").focus();
                    $("#yes_audio")[0].play();
                }
                else if (response.d == "Inactive Product") {
                    $("#txtBarcode").removeAttr('onchange', 'readBarcode()');
                    $("#txtBarcode").attr('onchange', 'InvalidBarcode()');
                    swal({
                        title: "Error!",
                        text: "Inactive Product can not be sold",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            $("#txtBarcode").removeAttr('onchange', 'InvalidBarcode()');
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                            $("#txtBarcode").attr('onchange', 'readBarcode()');
                        }
                    });
                    $("#txtBarcode").val('');
                    $("#txtBarcode").focus();
                }
                else {
                    var xmldoc = $.parseXML(response.d);
                    var product = $(xmldoc).find("Table");
                    var productAutoId = $(product).find('ProductAutoId').text();
                    var unitAutoId = $(product).find('UnitType').text();
                    $("#DraftAutoId").val($(product).find('DraftAutoId').text());
                    var flag1 = false;
                    var tr = $("#tblProductDetail tbody tr");
                    for (var i = 0; i <= tr.length - 1; i++) {
                        if ($(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem && $(tr[i]).find(".ProName > span").attr("productautoid") == productAutoId &&
                            $(tr[i]).find(".UnitType > span").attr("unitautoid") == unitAutoId && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange) {
                            var reqQty = Number($(tr[i]).find(".ReqQty input").val()) + 1;
                            $(tr[i]).find(".ReqQty input").val(reqQty);
                            flag1 = true;
                            rowCal($(tr[i]).find(".ReqQty > input"));
                            $('#tblProductDetail tbody tr:first').before($(tr[i]));
                            if (chkIsTaxable == true && ($(tr[i]).find(".TaxRate > span").attr("TypeTax")) == "0") {
                                toastr.success('Product has been added successfully but product has been not set taxable, now product cannot be set taxable .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                clear();
                                return;
                            }
                            else if (chkIsTaxable == false && ($(tr[i]).find(".TaxRate > span").attr("TypeTax")) == "1") {
                                toastr.success('Product has been added succesfully but product has already taxable,so now product has been added with taxable .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                clear();
                                return;
                            }
                            else {
                                toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                clear();
                                return;
                            }
                        }

                        if (chkFreeItem) {
                            if (($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == '0') && $(tr[i]).find(".UnitType > span").attr("unitautoid") != unitAutoId) {
                                flag1 = true;
                                toastr.error('Product already added in list.', 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                clear();
                                return;

                            }
                        }
                        if (chkExchange) {
                            if ($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId && $(tr[i]).find(".UnitType > span").attr("unitautoid") != unitAutoId && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == '0' && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange) {
                                flag1 = true;
                                toastr.error('Product already added in list.', 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                clear();
                                return;
                            }
                        }

                        if ($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId &&
                            $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange
                            && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem) {
                            flag1 = true;
                            toastr.error('Product already added in list.', 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                            clear();
                            return;
                        }
                    }

                    if (!flag1) {
                        var row = $("#tblProductDetail thead tr").clone(true);
                        $(".ProId", row).html($(product).find('ProductId').text() + "<span IsFreeItem='" + IsFreeItem + "'></span>");

                        if (IsFreeItem == 1) {
                            $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");
                        }
                        else if (IsExchange == 1) {
                            $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");
                        }
                        else if (IsTaxable == 1) {
                            $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                        }
                        else {
                            $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>");
                        }
                        $(".UnitType", row).html("<span UnitAutoId='" + unitAutoId + "' QtyPerUnit='" + $(product).find('UnitQty').text() + "'>" + $(product).find('UnitName').text() + "</span>");
                        $(".ReqQty", row).html("<input type='text' maxlength='4' onkeypress='return isNumberKey(event)' class='form-control input-sm border-primary text-center' runat='server' onkeyup='rowCal(this)' value='1'/>");
                        $(".Barcode", row).text("");
                        //$(".QtyShip", row).html("<input type='text' class='form-control input-sm text-center' onkeypress='return isNumberKey(event)' style='width:70px;' disabled value='0' onkeyup='rowCal(this);shipVsReq(this)' />");
                        $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * $(product).find('UnitQty').text());
                        if ($('#chkExchange').prop('checked') == false && $('#chkFreeItem').prop('checked') == false) {
                            if ($(product).find('CustomPrice').text() == "") {
                                $(".UnitPrice", row).html("<input type='text' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='" + $(product).find('Price').text() + "' minprice='" + $(product).find('MinPrice').text() + "' BasePrice='" + $(product).find('Price').text() + "' />");
                            } else {
                                $(".UnitPrice", row).html("<input type='text' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='" + (parseFloat($(product).find('CustomPrice').text()) >= parseFloat($(product).find('MinPrice').text()) ? $(product).find('CustomPrice').text() : $(product).find('Price').text()) + "' minprice='" + $(product).find('MinPrice').text() + "' BasePrice='" + $(product).find('Price').text() + "' />");
                            }
                        } else {
                            $(".UnitPrice", row).html("<input type='text' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='0.00' minprice='0.00' BasePrice='0.00' disabled/>");
                        }
                        $(".SRP", row).text($(product).find('SRP').text());
                        $(".GP", row).text($(product).find('GP').text());
                        $(".OM_MinPrice", row).text($(product).find('MinPrice').text());
                        $(".OM_CostPrice", row).text($(product).find('CostPrice').text());
                        $(".ItemType", row).text("New");
                        var taxType = '', TypeTax = 0;
                        if ($('#chkIsTaxable').prop('checked') == true) {
                            taxType = 'Taxable';
                            TypeTax = 1;
                        }
                        var IsExchange1 = '';
                        if ($('#chkExchange').prop('checked') == true) {
                            IsExchange1 = 'Exchange';
                        }
                        if (TypeTax == 1) {
                            $(".TaxRate", row).html("<span TypeTax='" + TypeTax + "' MLQty='" + $(product).find('MLQty').text() + "' WeightOz='" + $(product).find('WeightOz').text() + "'> </span>" + '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>');
                        } else {
                            $(".TaxRate", row).html("<span TypeTax='" + TypeTax + "' MLQty='" + $(product).find('MLQty').text() + "' WeightOz='" + $(product).find('WeightOz').text() + "'> </span>");
                        }
                        if (IsExchange == 1) {
                            $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>" + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                        } else {
                            $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>");
                        }

                        $(".NetPrice", row).text("0.00");
                        $(".QtyRemain", row).text("0");
                        $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-delete'></span></a>");
                        if ($('#tblProductDetail tbody tr').length > 0) {
                            $('#tblProductDetail tbody tr:first').before(row);
                        }
                        else {
                            $('#tblProductDetail tbody').append(row);
                        }
                        //if ($("#hiddenOrderStatus").val() != "3") {
                        rowCal(row.find(".ReqQty > input"));
                        //}
                        $("#txtQuantity, #txtTotalPieces").val("0");
                        if ($('#tblProductDetail tbody tr').length > 0) {
                            $('#emptyTable').hide();
                            $("#ddlCustomer").attr('disabled', true);
                        }

                        toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        $("#txtBarcode").val('');
                        $("#txtBarcode").focus();
                    }
                    else {
                        toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    }
                    $("#txtBarcode").val('');
                    $("#txtBarcode").focus();

                } 

            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        $("#ddlProduct").val(0).change();
        $("#alertBarcodeCount").hide();
        $("#ddlProduct").attr('disabled', false);
        $("#ddlUnitType").attr('disabled', false);
    }
}
function clear() {
    $("#panelProduct select").val(0);
    $("#txtQuantity, #txtTotalPieces").val("0");
    $("#alertBarcodeCount").hide();
    $("#txtBarcode").val('');
    $("#txtReqQty").focus();
    $("#txtBarcode").blur();
    $("#txtBarcode").val('');
    $("#txtBarcode").focus();
}



function getProductQty(AutoId) {
    $("#ddlProduct").val(AutoId).change();
}



function clickonSecurity() {

    $.ajax({
        type: "Post",
        url: "orderMaster.aspx/clickonSecurity",
        data: "{'CheckSecurity':'" + $("#txtSecurity").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    PackerSecurityEnable = true;
                    $("#SecurityEnabled").modal('hide');
                    $('#btnpkd').removeAttr('disabled');
                } else {
                    $('#SecurityEnvalid').modal('show');
                    $('#Sbarcodemsg').html('Invalid Security Key.')
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}


function approveOrder(ApproveStatus) {

    var Product = [];

    $("#tblProductDetail tbody tr").each(function () {
        Product.push({
            'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
            'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
            'QtyPerUnit': $(this).find('.UnitType').find('span').attr('QtyPerUnit'),
            'RequiredQty': $(this).find('.ReqQty input').val(),
            'Barcode': $(this).find('.Barcode').text(),
            'QtyShip': 0,
            'TotalPieces': $(this).find('.TtlPcs').text(),
            'UnitPrice': $(this).find('.UnitPrice input').val(),
            'SRP': $(this).find('.SRP').text(),
            'GP': $(this).find('.GP').text(),
            'Tax': $(this).find('.TaxRate span').attr('typetax'),
            'IsExchange': $(this).find('.IsExchange span').attr('IsExchange'),
            'IsFreeItem': $(this).find('.ProId span').attr('IsFreeItem'),
            'NetPrice': $(this).find('.NetPrice').text(),
            'OM_MinPrice': $(this).find('.OM_MinPrice').text(),
            'OM_CostPrice': $(this).find('.OM_CostPrice').text(),
            'ItemType': $(this).find('.ItemType').text(),
            'AutoId': $(this).find('.AutoId').text()

        });
    });

    var orderData = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        Status: $("#hiddenStatusCode").val(),
        OrderDate: $("#txtOrderDate").val(),
        DeliveryDate: $("#txtDeliveryDate").val(),
        CustomerAutoId: $("#ddlCustomer").val(),
        Remarks: $("#txtOrderRemarks").val(),
        BillAddrAutoId: $("#hiddenBillAddrAutoId").val(),
        ShipAddrAutoId: $("#hiddenShipAddrAutoId").val(),
        TotalAmount: $("#txtTotalAmount").val(),
        OverallDisc: $("#txtOverallDisc").val(),
        OverallDiscAmt: $("#txtDiscAmt").val(),
        ShippingCharges: $("#txtShipping").val(),
        TotalTax: $("#txtTotalTax").val(),
        GrandTotal: $("#txtGrandTotal").val(),
        ShippingType: parseInt($("#ddlShippingType").val()),
        MLQty: parseFloat($("#txtMLQty").val()),
        TaxType: parseFloat($("#ddlTaxType").val()),
        ApproveStatus: ApproveStatus
    };

    $.ajax({
        type: "Post",
        url: "orderMaster.aspx/updateOrder",
        data: JSON.stringify({ TableValues: JSON.stringify(Product), orderData: JSON.stringify(orderData) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.d != "Session Expired") {

                if (response.d == 'true') {
                    swal("", "Order approved successfully.", "success").then(function () {
                        location.href = '/Web/WebOrderList.aspx';
                    });
                } else {
                    swal("Error!", response.d, "error")
                }

            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error")
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error")
        }
    });
}


function approveOrderSecurity() {
    var flag1 = false, flag2 = false, CheckTax = false;
    var CreditNo = '';
    $("#tblCreditMemoList tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            CreditNo += $(this).find('.Action input').val() + ',';

        }
    })
    if ($("#tblProductDetail tbody tr").length > 0) {
        $("#tblProductDetail tbody tr").each(function () {
            if ($(this).find(".ReqQty input").val() == "" || $(this).find(".ReqQty input").val() == null || Number($(this).find(".ReqQty input").val()) <= 0) {
                $(this).find(".ReqQty input").addClass("border-warning");
                flag1 = true;
            } else {
                $(this).find(".ReqQty input").removeClass("border-warning");
            }

            if ($("#hiddenEmpTypeVal").val() != '7') {
                if (($(this).find('.ProId span').attr('IsFreeItem') == 0 && $(this).find('.IsExchange span').attr("isexchange") != 1) && Number($(this).find(".UnitPrice input").val()) < Number($(this).find(".UnitPrice input").attr('minprice'))) {
                    flag2 = true;
                    $(this).find(".UnitPrice input").addClass('border-warning');
                }
            }
            if ($(this).find('.TaxRate span').attr('typetax') == '1') {
                if (!$("#ddlTaxType").val()) {
                    CheckTax = true;
                }
            }
        });
        if (CheckTax == true) {
            toastr.error('Tax is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else if (!flag1 && !flag2) {
            swal({
                title: "Are you sure?",
                text: "You want to approve order.",
                icon: "warning",
                showCancelButton: true,
                allowOutsideClick: false,
                closeOnClickOutside: false,
                buttons: {
                    cancel: {
                        text: "No, cancel!",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: true,

                    },
                    confirm: {
                        text: "Yes, approve it.",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false

                    }
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    approveOrder(22);
                }
            })
        }
        else {
            if (flag1) {

                toastr.error('Required Quantity cannot be blank and zero.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            } else {

                toastr.error('Unit Price can not be less than Min. Price.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
    } else {

        toastr.error('No Product Added in the List', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}


function cancelOrders() {
    if ($('#txtCancellationRemarks').val() == '') {
        $('#txtCancellationRemarks').addClass('border-warning');
        $('#txtCancellationRemarks').focus();
        toastr.error('Cancellation Remark is mandatory.', 'warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else {
        swal({
            title: "Are you sure?",
            text: "You want to cancel this order.",
            icon: "warning",
            showCancelButton: true,
            allowOutsideClick: false,
            closeOnClickOutside: false,
            buttons: {
                cancel: {
                    text: "No, cancel!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,

                },
                confirm: {
                    text: "Yes",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false

                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                cancelOrder();
            }
        })
    }
}
