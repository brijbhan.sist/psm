﻿
$(document).ready(function () {
    ViewProfile();
})

function ViewProfile() {
    debugger
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/employeeMaster.asmx/ViewProfile",
        data: "{}",
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Emp = $(xmldoc).find("Table"); 
            $("#EmpType").val($(Emp).find("EmpType").text());
            if ($(Emp).find("EmployeeCode").text() == null || $(Emp).find("EmployeeCode").text() == "") {
                $("#EmpId").val($(Emp).find("EmpId").text());
            }
            else {
                $("#EmpId").val($(Emp).find("EmployeeCode").text());
            }
            if ($(Emp).find("ProfileName").val() == null || $(Emp).find("ProfileName").text() == "") {
                $("#ProfileName").val($(Emp).find("FirstName").text());
            }
            else {
                $("#ProfileName").val($(Emp).find("ProfileName").text());
            }
            $("#fname").val($(Emp).find("FirstName").text());
            $("#lname").val($(Emp).find("LastName").text());
            $("#Adress").val($(Emp).find("Address").text());
            $("#State").val($(Emp).find("State").text());
            $("#lcity").val($(Emp).find("City").text());
            $("#lzipcode").val($(Emp).find("Zipcode").text());
            $("#lemail").val($(Emp).find("Email").text());
            $("#lcontact").val($(Emp).find("Contact").text());
            $("#imgPreview").attr("src", $(Emp).find("ImageURL").text());
            $("#imgPreview").show();
            $("#lusername").val($(Emp).find("UserName").text());
            //var TEST = $(Emp).find("UserName").text();
            //$("#txtUserName").val(TEST);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}