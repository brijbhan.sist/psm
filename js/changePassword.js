﻿
$('#btnChangePassword').click(function () {
    debugger;
    if (dynamicALL('req')) {
        if ($('#txtConfirmPassword').val() != $('#txtNewPassword').val()) {
            toastr.error('Old Password New Password do not match !!', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        var data = {
            OldPassword: $('#txtOldPassword').val(),
            NewPassword: $('#txtNewPassword').val(),
        }
        console.log(data);
        $.ajax({
            type: "POST",
            url: "/WebAPI/passwordChange.asmx/changePassword",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                swal("","Password changed successfully.","success");
                $("input[type='password']").val('');
            },
            error: function (result) {
                swal("Error", JSON.parse(result.responseText).d, "error");
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('All fields required !!', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
