﻿function Capitalize(id) {
    var txt = document.getElementById(id);
    txt.value = txt.value.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
}
function uppercase(id) {
    var txt = document.getElementById(id);
    txt.value = txt.value.replace(/\w\S*/g, function (txt) { return txt.substr(0).toUpperCase(); });
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
function isNumberFixedDecimalKey(evt, obj, validateamt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var value = obj.value;
    var value1 = value.split('.');
    var dotcontains = value.indexOf(".") != -1;
    if (dotcontains) {
        if (charCode == 46) { return false; }
    }       
    if (charCode == 46) {
        return true;
    }
    if (parseFloat(value) > parseFloat($("#" + validateamt).val())) {
        toastr.error('Discount cannot be greater than sub total.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return false;
    }
}
function onlyNumbersWithColon(e) {
    var charCode;
    if (e.keyCode > 0) {
        charCode = e.which || e.keyCode;
    }
    else if (typeof (e.charCode) != "undefined") {
        charCode = e.which || e.keyCode;
    }
    if (charCode == 46)
        return true
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
function ImproveDecimal(e) {
    if (isNaN(parseFloat($(e).val()).toFixed(2))) {
        $(e).val(parseFloat("0").toFixed(2));
       
    } else {
        $(e).val(parseFloat($(e).val()).toFixed(2));
    }
}
function isNumberDecimalKey(evt, obj) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    var value = obj.value;
    var value1 = value.split('.');
    var dotcontains = value.indexOf(".") != -1;
    if (dotcontains)
        if (charCode == 46) return false;
    if (charCode == 46) {
        return true;
    }
    else if (value1[0].length > 5) {
        var ck = false;
        if ((dotcontains == true && value1[1].length < 3)) {
            ck = true;
        } else {
            return false;
        }
        if (!ck) {
            return false;
        }
        return false
    }

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
//function checkSpecialCharacter(e, obj) {
//    debugger;
//    if (obj.val().length == 0) {
//        var k = e.which;
//        var ok = k >= 65 && k <= 90 || // A-Z
//            k >= 97 && k <= 122 || // a-z
//            k >= 48 && k <= 57; // 0-9

//        if (!ok) {
//          return  e.preventDefault();
//        }
//    }
//} 
var format = 'IN';
function dateformat(timestamp) {
    var dateVal = "/Date(" + timestamp + ")/";
    var date = new Date(parseFloat(dateVal.substr(6)));
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    if (month < 10) month = "0" + month;
    if (day < 10) day = "0" + day;
    if (format == "IN") {
        if (hh > 0) {
            var fulldate = day + "/" + month + "/" + year + ' ' + hh + ':' + (mm > 9 ? mm : '0' + mm);
        }
        else {
            var fulldate = day + "/" + month + "/" + year;
        }


    }
    if (format == "US") {

        var fulldate = month + "/" + day + "/" + year;
    }
    return fulldate;
}
function exDate(date) {
    var billDate = date.split('/');
    return billDate[1] + "/" + billDate[0] + "/" + billDate[2];
}
function goBack() {
    window.history.back();
}

//----------------------------------

var iWords = ['Zero', ' One', ' Two', ' Three', ' Four', ' Five', ' Six', ' Seven', ' Eight', ' Nine'];
var ePlace = ['Ten', ' Eleven', ' Twelve', ' Thirteen', ' Fourteen', ' Fifteen', ' Sixteen', ' Seventeen', ' Eighteen', ' Nineteen'];
var tensPlace = ['', ' Ten', ' Twenty', ' Thirty', ' Forty', ' Fifty', ' Sixty', ' Seventy', ' Eighty', ' Ninety'];
var inWords = [];

var numReversed, inWords, actnumber, i, j;

function tensComplication() {
    'use strict';
    if (actnumber[i] === 0) {
        inWords[j] = '';
    } else if (actnumber[i] === 1) {
        inWords[j] = ePlace[actnumber[i - 1]];
    } else {
        inWords[j] = tensPlace[actnumber[i]];
    }
}

function convertPriceToWords(rupees) {
    'use strict';
    var junkVal = rupees;
    junkVal = Math.floor(junkVal);
    var obStr = junkVal.toString();
    numReversed = obStr.split('');
    actnumber = numReversed.reverse();

    if (Number(junkVal) >= 0) {
        //do nothing
    } else {
        window.alert('wrong Number cannot be converted');
        return false;
    }
    if (Number(junkVal) === 0) {
        document.getElementById('container').innerHTML = obStr + '' + 'Rupees Zero Only';
        return false;
    }
    if (actnumber.length > 9) {
        window.alert('Oops!!!! the Number is too big to covertes');
        return false;
    }



    var iWordsLength = numReversed.length;
    var finalWord = '';
    j = 0;
    for (i = 0; i < iWordsLength; i++) {
        switch (i) {
            case 0:
                if (actnumber[i] === '0' || actnumber[i + 1] === '1') {
                    inWords[j] = '';
                } else {
                    inWords[j] = iWords[actnumber[i]];
                }
                inWords[j] = inWords[j] + ' Only';
                break;
            case 1:
                tensComplication();
                break;
            case 2:
                if (actnumber[i] === '0') {
                    inWords[j] = '';
                } else if (actnumber[i - 1] !== '0' && actnumber[i - 2] !== '0') {
                    inWords[j] = iWords[actnumber[i]] + ' Hundred and';
                } else {
                    inWords[j] = iWords[actnumber[i]] + ' Hundred';
                }
                break;
            case 3:
                if (actnumber[i] === '0' || actnumber[i + 1] === '1') {
                    inWords[j] = '';
                } else {
                    inWords[j] = iWords[actnumber[i]];
                }
                if (actnumber[i + 1] !== '0' || actnumber[i] > '0') {
                    inWords[j] = inWords[j] + ' Thousand';
                }
                break;
            case 4:
                tensComplication();
                break;
            case 5:
                if (actnumber[i] === '0' || actnumber[i + 1] === '1') {
                    inWords[j] = '';
                } else {
                    inWords[j] = iWords[actnumber[i]];
                }
                if (actnumber[i + 1] !== '0' || actnumber[i] > '0') {
                    inWords[j] = inWords[j] + ' Lakh';
                }
                break;
            case 6:
                tensComplication();
                break;
            case 7:
                if (actnumber[i] === '0' || actnumber[i + 1] === '1') {
                    inWords[j] = '';
                } else {
                    inWords[j] = iWords[actnumber[i]];
                }
                inWords[j] = inWords[j] + ' Crore';
                break;
            case 8:
                tensComplication();
                break;
            default:
                break;
        }
        j++;
    }


    inWords.reverse();
    for (i = 0; i < inWords.length; i++) {
        finalWord += inWords[i];
    }
    return finalWord;
}

function calHours(endTime, startTime) {
    if (endTime != '') {
        var endVal = "/Date(" + endTime + ")/";
        var endDate = new Date(parseFloat(endVal.substr(6)));
        var startVal = "/Date(" + startTime + ")/";
        var startDate = new Date(parseFloat(startVal.substr(6)));
        var diff = (endDate - startDate);

        var seconds = Math.floor(diff / 1000); //ignore any left over units smaller than a second
        var minutes = Math.floor(seconds / 60);
        seconds = seconds % 60;
        var hours = Math.floor(minutes / 60);
        minutes = minutes % 60;
        return hours + ":" + minutes;
    }
    else {
        return '0'
    }
}