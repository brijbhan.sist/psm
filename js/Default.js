﻿
$(function () {
    
    var body = document.getElementsByTagName('body')[0];
    body.onkeypress = function (e) {
        if (e.keyCode === 13) {
            $('#btnlogin').click();
            if ($("#txtusername").val() == '') {
                $("#txtusername").focus();
                return;
            }
            if ($("#txtpassword").val() == '') {
                $("#txtpassword").focus();
            }
        }
    }
});



function openContact() {
    $('#contactModal').modal('show');
}
function login() {
    localStorage.removeItem("messgeTime"); 
    var UserName = $("#txtusername").val();
    var Password = $("#txtpassword").val();
    $.ajax({
        type: "POST",
        url: "/WebAPI/default.asmx/loginUser",
        data: "{'UserName':'" + UserName + "','Password':'" + Password + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () { 
        },
        complete: function () { 
        },
       
        success: function (response) {                               
            if (response.d == 'Access Denied') {
                swal("", "Access Denied", "error");
            }
            else if (response.d == 'Username And / Or Password Incorrect') {
                swal("", "Username / Password Incorrect", "error");
            }
            else {
                var xmldoc = $.parseXML(response.d);           
                var loginDetails = $(xmldoc).find("Table");                            
                if (loginDetails.length > 0) {                                   
                    if ($(loginDetails).find("Submsg").text() != "0") {
                        swal("Alert", $(loginDetails).find("Submsg").text(), "warning").then(function () {
                            if ($(loginDetails).find("ExpMsg").text() != "1") {
                                document.location.href = "/Admin/mydashboard.aspx";
                            }
                            else {
                                location.href = "/"                              
                            }
                        })
                    } else {
                        document.location.href = "/Admin/mydashboard.aspx";
                    }
                }
                else {
                    swal("", "Username / Password Incorrect", "error");
                }
            }
        },
        error: function (result) {          
            swal("", "Error ! Username / Password Incorrect", "warning");         
        },
        failure: function (result) {
            swal("", "Error ! Access Denied", "warning");         
        }
    });
}

function Bindlogo() {

    $.ajax({
        type: "POST",
        url: "/WebAPI/default.asmx/Bindlogo",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            var xmldoc = $.parseXML(response.d);
            var Companylogo = $(xmldoc).find("Table");

            $("#logo").attr("src", "../Img/logo/" + $(Companylogo).find("logo").text());
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
