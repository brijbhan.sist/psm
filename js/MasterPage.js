﻿$('ul.nav li.dropdown').hover(function () {
    $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(150);
}, function () {
    $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(150);
});

function ImplemntDebugger() {
    if (location.port == "localhost") {
        debugger;
    }
}
function checkRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '0' || $(this).val().trim() == '0.00') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlreq').each(function () {
        if ($(this).val()== '' || $(this).val()== '0' || $(this).val() == '-Select-' || $(this).val()== null) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlSreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}
//For input type text and dropdown
function dynamicALL(className) {
    var boolcheck = true;
    try {
        $('.' + className).each(function () {
            if ($(this).val().trim() == '' || $(this).val().trim() == '0.00' || $(this).val().trim() == '0') {
                boolcheck = false;
                $(this).addClass('border-warning');
            } else {
                $(this).removeClass('border-warning');
            }
        });
    } catch (e) {

    }
    return boolcheck;
}
//For input dropdown select2 single
function dynamicInputTypeSelect2(className) {
    var boolcheck = true;
    $('.' + className).each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}
//For input dropdown select2 multiple
function dynamicInputTypeSelect2Multi(className) {
    var boolcheck = true;
    $('.' + className).each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == null || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--multiple').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--multiple').removeAttr('style');
        }
    });
    return boolcheck;
}

// For Help pop up 

function GetPageInformation(idPage) {
    var data = {
        PageId: idPage
    }
    $.ajax({
        type: "POST",
        url: "/Admin/WebAPI/PageTitleMaster.asmx/getPageTitleMaster",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                $("#hMessage").text('');
                var pages = $(xmldoc).find('Table');
                var html = ''
                if (pages.length > 0) {
                    $("#modalHelp").modal('show');
                    $.each(pages, function () {
                        html += '<div style ="padding:10px;>'
                        html += '<p style="text-align:justify;font-style: initial; font-variant-caps: titling-caps;">' + $(this).find("UserDescription").text() + '</p >'
                        html += '</div>'
                        if ($("#hEmpTypeNo").val() == 1) {
                            if ($(this).find("AdminDescription").text() != '') {
                                html += '<div style ="border:2px solid #e5e5e5;padding:10px;">'
                                html += '<p style="text-align:justify;font-style: initial; font-variant-caps: titling-caps;">' + $(this).find("AdminDescription").text() + '</p >'
                                html += '</div>'
                            }
                        }
                    })
                    $("#hMessage").html(html);
                    $("#hTitle").html($(pages).find("PageTitle").text());
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}