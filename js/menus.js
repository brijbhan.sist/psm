﻿(function ($) {
    $.fn.generateMenus = function (obj) {
        var showModule=[];
        $.each(obj, function () {
            if ($(this).find('ModuleId').text() == "0") {
               
                var parentId = $(this).find('AutoId').text();
                var parent = {
                    id: $(this).find('AutoId').text(),
                    label: $(this).find('ModuleName').text(),
                    url:$(this).find('url').text()
                }

                var children = [];

                $.each(obj, function () {
                    if ($(this).find('ModuleId').text() == parentId) {
                        children.push({
                            id: $(this).find('AutoId').text(),
                            label: $(this).find('ModuleName').text(),
                            url:$(this).find('url').text()
                        });
                    }
                });

                showModule.push({
                    id: $(this).find('AutoId').text(),
                    label: $(this).find('ModuleName').text(),
                    url:$(this).find('url').text(),
                    children: children
                });

            };
        });
        var htm = '';
        htm = "<ul class='nav navbar-nav collapse navbar-collapse'>"
        for (var i = 0; i < showModule.length; i++) {
            if (typeof showModule[i].children == 'undefined' || showModule[i].children.length==0) {
                htm += "<li><a href='"+showModule[i].url+"'>" + showModule[i].label + "</a></li>";
            }
            else {
                htm += "<li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='"+showModule[i].url+"'>" + showModule[i].label + "<span class='caret'></span></a>";
                htm += "<ul class='dropdown-menu' align='left'>";
                for (var j = 0; j < showModule[i].children.length; j++) {
                    htm += "<li><a href='"+showModule[i].children[j].url+"'>" + showModule[i].children[j].label + "</a></li>";
                }
                htm += "</ul></li>";
            }
        }
        htm += "</ul>";
        this.html(htm);
    }
}(jQuery));