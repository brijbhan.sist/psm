﻿$(document).ready(function () {
    ShowMessage(); 
});

function ShowMessage() {
        $.ajax({
            type: "Post",
            url: "/WebAPI/WShowMessage.asmx/ShowMessage",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
               
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var message = $(xmldoc).find("Table");
                    $("#MsgDashborad").find("#message").html('');
                    if (message.length > 0) {

                        $.each(message, function () {
                            debugger;
                            $("#MessageAutoId").val($(this).find("MessageAutoId").text());
                            $("#MsgDashborad").find("#title").html($(this).find("Title").text());
                            var html = '';
                            if ($(this).find('ImageURL').text().trim() != '') {
                                html = "<img src=" + $(this).find("ImageURL").text().trim() + ">";
                                html += "<br/>";
                            }
                            
                            html += "<pre style='background-color:transparent;color:#18191c;text-align:left'>" + $(this).find("Message").text() + "</pre>";
                            $("#MsgDashborad").find("#message").html(html);
                        });
                        $('#MsgDashborad').modal('show');
                    } else {
                        $('#MsgDashborad').modal('hide');
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                console.log(result);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
     
}

function ImplemntDebugger() {
    if (location.port == "localhost") {
        debugger;
    }
}

function getNotification() {
    $("#div_Lists").css('display', 'none');
    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/getNotification",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "false") {
                var xmldoc = $.parseXML(response.d);
                var MessageList = $(xmldoc).find("Table");
                var TotalCount = $(xmldoc).find("Table1");
                var html = '';
                $('#div_Lists').html('');
                if (MessageList.length > 0) {
                    $.each(MessageList, function () {
                        html += '<a href="#" class="dropdown-item">';
                        html += '<div class="media">';
                        html += '<img src="https://png.pngtree.com/png-vector/20190710/ourmid/pngtree-user-vector-avatar-png-image_1541962.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">';
                        html += '<div class="media-body" style="margin-left: -24px;margin-bottom: -15px;">';
                        html += '<h3 class="dropdown-item-title" onclick="Dash_ViewMsg(' + $(this).find("AutoId").text() + ')">' + $(this).find("Message").text().slice(0,10) +'...'+ '';

                        html += '</h3><br/>';
                        html += '<p class="text-sm text-muted"><i class=" ft-clock mr-1"></i>' + $(this).find("MsgDate").text() + '</p>';
                        html += '</div>';
                        html += '</div>';
                        html += '</a>';
                        html += '<div class="dropdown-divider"></div>';

                    });
                    html += '<a href="#" class="dropdown-item dropdown-footer">See All Messages</a>';
                    $("#txt_TotalNotification").text($(TotalCount).find("TotalCount").text())
                    $('#div_Lists').append(html);
                } else {
                    $('#div_Lists').append('<h6 style="text-align:center">No message found.</h6>');
                }
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Dash_ViewMsg(MsgAutoId) {
    debugger;
    $.ajax({
        type: "POST",
        url: "/admin/WebAPI/WDashBoardMaster.asmx/getNotification_Message",
        data: "{'AutoId':'" + MsgAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "false") {
                var xmldoc = $.parseXML(response.d);
                var Message = $(xmldoc).find("Table");

                if (Message.length > 0) {
                    $("#Dash_NotificationMsg").text($(Message).find("Message").text());
                    $("#MsgDashborad_Mesg").modal('show');
                    $("#div_Lists").css('display', 'block');
                    getNotification();
                }
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function snozeMessage() {
    $.ajax({
        type: "POST",
        url: "/WebAPI/WShowMessage.asmx/readmessage",
        data: "{'AutoId':'" + $("#MessageAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            $('#MsgDashborad').modal('hide');
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}