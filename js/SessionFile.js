﻿$(document).ready(function () {
    if ($("#AuthStatus").val() == "1") {
        swal("", "Unauthorized Access !", "warning");
        $("#AuthStatus").val('0');
    }
    setInterval(function () {
        SessionFile();
    },10000)
})
function SessionFile() {
    $.ajax({
        type: "POST",
        url: "/WebAPI/WSessionFile.asmx/SessionFile",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
           
        },
        complete: function () {
           
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                

            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            location.href = '/';
        },
        error: function (result) {
            location.href = '/';
        }
    });
}