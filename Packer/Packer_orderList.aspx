﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="Packer_orderList.aspx.cs" Inherits="Packer_orderList" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input id="HDDomain" runat="server" type="hidden" />

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a></li>
                        <li class="breadcrumb-item">Order List</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height: 400px">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="alert alert-success alert-dismissable fade in" id="alertSuccessDelete" style="display: none;">
                                    <a aria-label="close" id="successDeleteClose" class="close" style="cursor: pointer;">&times;</a>
                                    <span></span>
                                </div>
                                <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />

                                <input type="hidden" id="hiddenPackerAutoId" runat="server" />
                                <input type="hidden" id="hiddenEmpType" runat="server" />
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary"  style="width: 100% !important;" id="ddlCustomer" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group" id="SalesPerson">
                                        <select class="form-control input-sm border-primary"  style="width: 100% !important;" id="ddlSalesPerson" runat="server">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control input-sm border-primary" placeholder="Order No" id="txtSOrderNo" onfocus="this.select()" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlSStatus">
                                            <option value="0">All Status</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;"> From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" placeholder="Order From Date" onchange="setdatevalidation(1);" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;"> To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" placeholder="Order To Date" id="txtSToDate" onchange="setdatevalidation(2);" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" style="width: 100% !important;" id="ddlShippingType">
                                            <option value="0">All Shipping Type</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered" id="tblOrderList">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td class="action text-center">Action</td>
                                                    <td class="status  text-center">Status</td>
                                                    <td class="orderNo  text-center">Order No</td>
                                                    <td class="orderDt text-center">Order Date</td>
                                                    <td class="SalesPerson">Sales Person</td>
                                                    <td class="cust">Customer</td>
                                                    <td class="product text-center">Products</td>
                                                    <td class="Shipping text-center">Shipping Type</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>

                                    </div>
                                </div>
                                <div class="row mt-1">
                                     <div class="col-md-2 col-sm-2">
                                            <select id="ddlPageSize" onchange="getOrderList(1)" class="form-control border-primary input-sm">
                                                <option selected="selected" value="10">10</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="500">500</option>
                                                <option value="1000">1000</option>
                                                <option value="0">All</option>
                                            </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Modal -->
    <div id="modalOrderLog" class="modal fade text-left show" role="dialog">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Order Log</h4>
                            <label><b>Order No </b>&nbsp;:&nbsp;<span id="lblOrderNo"></span>&nbsp;&nbsp;<b>Order Date</b>&nbsp;:&nbsp;<span id="lblOrderDate"></span></label>

                        </div>
                    </div>

                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblOrderLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="ActionBy">Action By</td>
                                    <td class="Date text-center">Date</td>
                                    <td class="Action">Action</td>
                                    <td class="Remark">Remark</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Packer/JS/Packer_OrderList.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>

</asp:Content>

