﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="Packer_viewOrder.aspx.cs"
    Inherits="Manager_Manager_viewOrder" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .zoom:hover {
            -ms-transform: scale(3); /* IE 9 */
            -webkit-transform: scale(3); /* Safari 3-8 */
            transform: scale(3);
        }

        #CheckStock th, #CheckStock td {
            padding: 0 5px 0 5px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">View Order Details</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="/packer/Packer_orderList.aspx">Order List</a></li>
                        <li class="breadcrumb-item">View Order Details</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-menu-right box-shadow-2 px-2" onclick="location.href='/Packer/Packer_orderList.aspx'"
                    id="btnGroupDrop1" type="button"
                    aria-expanded="false">
                    Back</button>

            </div>
        </div>
    </div>
    <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
    <input type="hidden" id="hiddenurl" runat="server" />
    <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />
    <input type="hidden" id="hiddenCustAutoId" />
    <input type="hidden" id="hfOrdStatus" />
    <input type="hidden" id="hfItemKey" value="0" />
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Order Details</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="label">Order No</label>
                                        <input type="text" id="txtOrderId" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>

                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="label">Order Date</label>
                                        <input type="text" id="txtOrderDate" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>

                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="label">Order Type</label>
                                        <input type="text" id="txtOrderType" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="label">Order Status</label>
                                        <input type="text" id="txtOrderStatus" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="label">Delivery Date</label>
                                        <input type="text" id="txtDeliveryDate" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label control-space">Sales Person</label>
                                        <input type="text" id="txtSalesPerson" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="label">Customer</label>
                                        <input type="text" id="txtCustomer" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label control-space">Customer Type</label>
                                        <input type="text" id="txtCustomerType" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="label">Terms</label>
                                        <input type="text" class="form-control input-sm border-primary" id="txtTerms" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-2 form-group">
                                        <label class="control-label control-space">Shipping Type</label>
                                        <input type="text" id="txtShippingType" class="form-control input-sm border-primary" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Billing Address</label>
                                        <textarea class="form-control border-primary input-sm" id="txtBillAddress" readonly="readonly"> </textarea>

                                    </div>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Shipping Address</label>
                                        <textarea class="form-control border-primary input-sm" id="txtShipAddress" readonly="readonly"> </textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h4 class="form-section">Order Content</h4>
                                <div id="divBarcode" style="display: none;">
                                    <div class="row">
                                        <div class="col-md-2 col-ms-2">
                                            <label class="control-label">Quantity <span class="required">*</span></label>
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm border-primary" id="txtQty" onkeypress="return isNumberKey(event)" value="1" maxlength="6" />
                                            </div>
                                        </div>
                                        <div class="col-md-1  col-sm-1 ">
                                            <label class="control-label">
                                                Is Exchange 
                                            </label>
                                            <div class="form-group">
                                                <input type="checkbox" id="IsExchange" onchange="changetype(this,'IsFreeItem')" />
                                            </div>
                                        </div>
                                        <div class="col-md-1  col-sm-1 ">
                                            <label class="control-label">
                                                Is Free Item 
                                            </label>
                                            <div class="form-group">
                                                <input type="checkbox" id="IsFreeItem" onchange="changetype(this,'IsExchange')" />
                                            </div>
                                        </div>
                                        <div class="col-md-3  col-ms-3">
                                            <label class="control-label">Enter Barcode</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm border-primary" id="txtScanBarcode" autocomplete="off" maxlength="20" onchange="checkBarcode()" />
                                            </div>
                                        </div>
                                        <div class="col-md-3  col-ms-3">
                                            <label class="control-label">
                                                Product<span class="required"> *</span>
                                            </label>
                                            <div class="form-group">
                                                <select class="form-control input-sm border-primary" id="ddlitemproduct" runat="server" onchange="itemProduct()" style="width: 100% !important">
                                                    <option value="0" style="display: none">-Select-</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2  col-ms-2">
                                            <div class="pull-left">
                                                <button type="button" class="btn btn-facebook buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="AddProductQty()" style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="tblProductDetail" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="TAction text-center" style="display: none">Action</td>
                                                        <td class="PImage text-center">Image</td>
                                                        <td class="ProId text-center">ID</td>
                                                        <td class="ProName">Product Name</td>

                                                        <td class="UnitType text-center">Unit</td>
                                                        <td class="RequiredQty text-center">Ordered<br>
                                                            Qty</td>
                                                        <td class="Barcode text-center">Barcode</td>
                                                        <td class="QtyShip text-center" style="width: 5%">Packed Qty</td>
                                                        <td class="QtyRemain text-center">Not Packed
                                                            <br />
                                                            Qty
                                                        </td>
                                                        <td class="TtlPcs text-center">Total<br />
                                                            Pieces</td>
                                                        <td class="IsExchange text-center" style="display: none">Exchange</td>
                                                        <td class="AddOnPiece text-center" style="display: none">Add On Piece</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7  col-sm-7">
                    <div class="row" id="tblPackedDetails" style="display: none">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Packing Details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card-content collapse" style="">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="tblPacked" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SRNO">SN</td>
                                                        <td class="PackedId">Packed ID</td>
                                                        <td class="PackedDate">Packed Date</td>
                                                        <td class="PackedBy">Packed By</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="Div2" style="">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Order Remark Details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse" style="">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="Table2" class="table table-striped table-bordered">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SRNO text-center">SN</td>
                                                        <td class="EmployeeName">Employee Name</td>
                                                        <td class="EmployeeType">Employee Type</td>
                                                        <td class="Remarks" style="white-space: normal;">Remark</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="" id="CustomerType" />
                    <input type="hidden" value="" id="ShippingAutoId" />
                    <div class="row" id="packerdetails" style="">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Packer Details</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show" style="">
                                    <div class="card-body">
                                        <label>Remark</label><textarea maxlength="500" id="txtPackerRemarks" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5  col-sm-5">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row">
                                            <label class="col-md-7 col-sm-7 form-group text-right" style="white-space: nowrap">
                                                No. of Packed Boxes <span class="red">*</span>
                                            </label>
                                            <div class="col-md-5 col-sm-5 form-group">
                                                <div class="input-group">

                                                    <div class="input-group-prepend" onclick="Minus()" id="mns">
                                                        <span class="input-group-text input-sm input-group-text-custom"><i class="ft-minus"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control border-primary input-sm text-center" value="1" maxlength="3" disabled="disabled" onkeypress="return isNumberKey(event)" id="txtPackedBoxes" runat="server" />
                                                    <div class="input-group-prepend" onclick="Plus()" id="ads">
                                                        <span class="input-group-text input-sm input-group-text-custom"><i class="ft-plus"></i></span>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-footer">
                                        <div class="form-group row">
                                            <div class="col-md-12 pull-right" style="margin: 0; padding: 0;">

                                                <button type="button" id="btneditOrder" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm form-group" onclick="PrintOrder()"><b>Print Order</b></button>
                                                <button type="button" id="btnGenOrderCC" class="btn btn-success buttonAnimation round box-shadow-1 btn-sm form-group" style="display: none" onclick="Pack()"><b>Pack</b></button>
                                                <button type="button" id="btnBackOrder" class="btn btn-purple buttonAnimation round box-shadow-1 btn-sm form-group" onclick="viewOrderLog()"><b>View Log</b></button>
                                                <button type="button" id="btnGenBar" class="btn btn-info Animation round box-shadow-1 btn-sm form-group" style="display: none;" onclick="printGenOrderModel()"><b>Print Order Barcode</b></button>
                                                <button type="button" id="btnAddBox" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm form-group" onclick="AddNewBoxOnPOPUp()" style="display: none;"><b>Changes Boxes</b></button>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
    <script>
        function changetype(e, idhtml) {
            $('#' + idhtml).prop('checked', false);
        }
    </script>
    <div id="CheckStock" class="modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content ">
                <div class="modal-header">
                    <h4 class="modal-title"><b>Stock Details</b></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <br />

                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="tblProductList" class="table table-striped table-bordered">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="Pop_PImage text-center width-10-per">Product Image</td>
                                            <td class="Pop_ProId text-center width4per">Product ID</td>
                                            <td class="Pop_ProName">Product Name</td>
                                            <td class="Pop_RequiredQty text-center width4per">Ordered Qty</td>
                                            <td class="Pop_QtyShip text-center width4per">Packed Qty</td>
                                            <td class="Pop_AvilStock text-center width4per">Available Qty</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="ok" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" data-dismiss="modal"><b>OK</b></button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalEditPacking" class="modal fade text-left show" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><b>Edit Unit Type</b></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <br />

                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-1  col-ms-1"></div>
                        <div class="col-md-10  col-ms-10">
                            <label class="control-label">Unit Type</label>
                            <div class="form-group">
                                <input type="hidden" id="hdnproductAutoId" />
                                <input type="hidden" id="hdnOrderAutoId" />
                                <input type="hidden" id="hdnOrderQty" />
                                <input type="hidden" id="hdnUnitAutoIdCheck" />
                                <input type="hidden" id="hdnordCheck" />
                                <select class="form-control input-sm border-primary" id="ddlUnitType" runat="server" style="width: 100% !important" onchange="calculateOrderQty()">
                                    <option value="0" style="display: none">-Select-</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1  col-ms-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-1  col-ms-1"></div>
                        <div class="col-md-10  col-ms-10">
                            <label class="control-label">Order Qty  </label>
                            <div class="form-group">
                                <input type="text" maxlength="4" class="form-control border-primary input-sm req text-left" onkeypress="return isNumberKey(event)" id="txtOrderQty" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-1  col-ms-1"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnupdateUnittype" class="btn btn-primary buttonAnimation round box-shadow-1  btn-sm" onclick="UpdateUnittype()"><b>Update</b></button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalOrderLog" class="modal fade text-left show" role="dialog">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row w-100">
                        <div class="col-md-8">
                            <h4 class="modal-title">Order Log</h4>
                            <b>Order No</b>&nbsp;:&nbsp;<span id="Span1"></span>&nbsp;&nbsp;<b>Order Date</b>&nbsp;:&nbsp;<span id="lblOrderDate"></span>
                        </div>

                    </div>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblOrderLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="ActionBy">Action By</td>
                                    <td class="Date text-center">Date</td>
                                    <td class="Action">Action</td>
                                    <td class="Remark">Remark</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="SecurityEnabled" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Check Security </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurity" class="form-control input-sm border-primary" onpaste="return false;" oncopy="return false;" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <span id="errormsg"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm animated" onclick="clickonSecurity()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1 btn-sm animated" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="SecurityEnvalid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="Sbarcodemsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="BarCodenotExists" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="barcodemsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick="focusonBarcode()">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="MsgNormal" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="lblMsgNormal"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdAllowQtyConfirmation" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong>Product stock is not available for this sales person.<br />
                            Do you want to override existing quantity with required quantity.</strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="OpenSecurity()">Yes</button>
                    <button type="button" class="btn btn-danger" onclick="CloseAllowConfirm()">No</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdSecurityCheck" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Manager security check </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtManagerSecurityCheck" class="form-control input-sm border-primary" />
                        </div>
                    </div>




                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <span id="errormsgVoid"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnMulipleCheck" onclick="CheckManagerSecurity()">OK</button>
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnManual" style="display: none" onclick="CheckManualManagerSecurity()">OK</button>

                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal" onclick="closeMnagerSecurity()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="ModalAddBox" class="modal fade text-left show" role="dialog">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><b>Change Boxes</b></h4>
                    <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>

                    <br />

                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 form-group">Existing Boxes</div>
                        <div class="col-md-8 form-group">
                            <input type="text" id="txtExistBoxNo" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">New Boxes</div>
                        <div class="col-md-8 form-group">
                            <div class="input-group">

                                <div class="input-group-prepend" onclick="Minus1()" id="mns1">
                                    <span class="input-group-text input-sm input-group-text-custom"><i class="ft-minus"></i></span>
                                </div>
                                <input type="text" class="form-control border-primary input-sm text-center" value="1" maxlength="3" disabled="disabled" onkeypress="return isNumberKey(event)" id="txtNoofBox" runat="server" />
                                <div class="input-group-prepend" onclick="Plus1()" id="ads1">
                                    <span class="input-group-text input-sm input-group-text-custom"><i class="ft-plus"></i></span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSaveNewBoxes" class="btn btn-warning buttonAnimation round box-shadow-1  btn-sm" onclick="btnSaveAddBox()"><b>Update</b></button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="PopPrintTemplate" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Select Invoice Template</h4>
                </div>
                <div class="modal-body">
                    <div class="container" id="packerAssignPrint">
                        <%--<div class="clearfix"></div>
                        <div class="row form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="Templ1" />
                                Template 1
                            </div>
                        </div>--%>
                        <%--<div class="clearfix"></div>
                        <div class="row form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="Templ2" />
                                Template 2
                            </div>
                        </div>
                         <div class="clearfix"></div>--%>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="GenBar()">Print</button>
                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal" onclick="closePrintPop()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <audio class="audios" id="yes_audio" controls preload="none" style="display: none">
        <source src="/Audio/NOExists.mp3" type="audio/mpeg" />
    </audio>
    <audio class="audios" id="No_audio" controls preload="none" style="display: none">
        <source src="/Audio/WrongProduct.mp3" type="audio/mpeg" />
    </audio>
    <audio class="audios" id="Stock" controls preload="none" style="display: none">
        <source src="/Audio/Stock.mp3" type="audio/mpeg" />
    </audio>
    <audio class="audios" id="QtyMore" controls preload="none" style="display: none">
        <source src="/Audio/QtyMoreThen.mp3" type="audio/mpeg" />
    </audio>

</asp:Content>

