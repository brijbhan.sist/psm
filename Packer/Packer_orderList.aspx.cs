﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Packer_orderList : System.Web.UI.Page
{
    

    protected void Page_load(object sender, EventArgs e)
    {
        try
        {
            if (Session["EmpTypeNo"] != null)
            {
                if (Session["EmpTypeNo"].ToString() == "3")
                {
                    hiddenEmpType.Value = Session["EmpTypeNo"].ToString();
                    string[] GtUrl = Request.Url.Host.ToString().Split('.');
                    HDDomain.Value = GtUrl[0].ToLower();
                }else
                {
                    Session.Abandon();
                    Response.Redirect("~/Default.aspx", false);
                }
            }else
            {
                Session.Abandon();
                Response.Redirect("~/Default.aspx", false);
            }
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
    }
}