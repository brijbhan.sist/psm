﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_Manager_viewOrder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Packer/js/Packer_OrderView.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
        try
        {
            Uri myuri = new Uri(System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
            string pathQuery = myuri.PathAndQuery;
            string hostName = myuri.ToString().Replace(pathQuery, "");
            hiddenurl.Value = hostName;
            if (hostName == "http://psmnj.a1whm.com")
            {
                String hotjar = "2502197";
                Page.Header.Controls.Add(
                new System.Web.UI.LiteralControl(
                   "<!-- Hotjar Tracking Code for http://psmnj.a1whm.com/ --><script>(function(h,o,t,j,a,r){h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};h._hjSettings={ hjid:" + hotjar + ",hjsv:6};a=o.getElementsByTagName('head')[0];r=o.createElement('script');r.async=1;r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;" + "a.appendChild(r);})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');</script>"
               ));
            }
        }
        catch (Exception)
        {

        }
        try
        {
            if (Session["EmpTypeNo"] != null)
            {
                if (Session["EmpTypeNo"].ToString() != "3")
                {
                    Session.Abandon();
                    Response.Redirect("~/Default.aspx", false);
                }
            }
            else
            {
                Session.Abandon();
                Response.Redirect("~/Default.aspx", false);
            }
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }

    }
}