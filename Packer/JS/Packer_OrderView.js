﻿var rowupdate = "";
var CustomerId; var MLQtyRate = 0.00; var CustomerType = '', prdt = '', unt = 0, qty = 0;
$(document).ready(function () {
    $('#hfItemKey').val('');
    var getid = getQueryString('OrderAutoId');
    if (getid != null) {
        getOrderData(getid);
        $("#txtHOrderAutoId").val(getid);
    }
    $('#txtScanBarcode').focus();
});
var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};

function getPackerAssignPrintData(ShippingAutoId) {
    var data = {
        ShippingAutoId: ShippingAutoId
    };
    $.ajax({
        type: "POST",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/getPackerAssignPrintData",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {
            console.log(result);
            var xmldoc = $.parseXML(result.d);
            var packerPrint = $(xmldoc).find('Table');
            var packerPrintHtml = ``;
            if (packerPrint.length > 0) {
                if (packerPrint.length == 1) {
                    $.each(packerPrint, function () {
                        var Url = $(this).find("Url").text();
                        var finalUrl = Url + "?OrderId=" + $("#txtOrderId").val();
                        window.open(finalUrl, "popUpWindow", "height=400,width=400,left=10,top=10,,scrollbars=yes,menubar=no");
                    });

                }
                else {
                    var i = 1;

                    $.each(packerPrint, function () {
                        if (i == 1) {
                            var checkStatus = "checked";
                        }
                        packerPrintHtml += `<div class="clearfix"></div>
                        <div class="row form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="Templ` + i + `" value="` + $(this).find("Url").text() + `" class="printTemplateClass" ` + checkStatus + ` />
                                `+ $(this).find("PrintTemplate").text() + `
                            </div>
                        </div>`;

                        i++;

                    });
                    $('#PopPrintTemplate').modal('show');
                    $("#packerAssignPrint").html(packerPrintHtml);
                }

            } else {
                toastr.error('Template not found', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            }


        },
        error: function (result) {

        }

    });
}


function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/getOrderData",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {

            if (response.d == 'Session Expired') {
                location.href = '/';
            } else {
                var xmldoc = $.parseXML(response.d);
                var order = $(xmldoc).find("Table");
                var product = $(xmldoc).find("Table1");
                var RemarksDetails = $(xmldoc).find("Table2");
                var pkg = $(xmldoc).find("Table3");
                var ProductItme = $(xmldoc).find("Table4");
                $("#tblPacked tbody tr").remove();
                var rowP = $("#tblPacked thead tr:last").clone(true);
                if (pkg.length > 0) {
                    $.each(pkg, function (index) {
                        $(".SRNO", rowP).text(Number(index) + 1);
                        $(".PackedId", rowP).text($(this).find("PackingId").text());
                        $(".PackedDate", rowP).text($(this).find("PkgDate").text());
                        $(".PackedBy", rowP).text($(this).find("Packer").text());
                        $('#tblPacked').find("tbody").append(rowP);
                        rowP = $("#tblPacked tbody tr:last").clone(true);
                    });
                }
                var ProductItems = [], itemtype = 0;
                var htm = '';
                $("#ddlitemproduct option:not(:first)").remove();
                $.each(ProductItme, function (index) {
                    htm = '';
                    if ($(this).find("IsExchange").text() == '1') {
                        itemtype = 1;
                        htm = '(<product class="badge badge badge-pill badge-primary">Exchange</product>)'
                    }
                    else if ($(this).find("isFreeItem").text() == '1') {
                        itemtype = 2;
                        htm = '(<product class="badge badge badge-pill badge-success">Free</product>)'
                    }

                    ProductItems.push({
                        AutoId: $(this).find("AutoId").text(),
                        UnitType: $(this).find("UnitTypeAutoId").text(),
                        ProductName: $(this).find("ProductName").text(),
                        itemType: itemtype
                    });
                    $("#ddlitemproduct").append($("<option seq='" + itemtype + "' value='" + $(this).find("AutoId").text() + "' unitautoid='" + $(this).find("UnitTypeAutoId").text() + "'>" + $(this).find("ProductName").text() + htm + "</option> "));
                    itemtype = 0;
                });
                localStorage.setItem("ProductItems", JSON.stringify(ProductItems))
                $("#ddlitemproduct").select2();
                $("#Table2 tbody tr").remove();
                if ($(RemarksDetails).length > 0) {

                    var rowtest = $("#Table2 thead tr").clone();
                    $.each(RemarksDetails, function (index) {
                        $(".SRNO", rowtest).html((Number(index) + 1));
                        $(".EmployeeName", rowtest).html($(this).find("EmpName").text());
                        $(".EmployeeType", rowtest).html($(this).find("EmpType").text());
                        $(".Remarks", rowtest).html($(this).find("Remarks").text());
                        $("#Table2 tbody").append(rowtest);
                        rowtest = $("#Table2 tbody tr:last").clone(true);
                    });
                }
                CustomerId = $(order).find("CustomerId").text();
                $("#txtTaxType").val($(order).find("TaxType").text());
                $("#txtHOrderAutoId").val($(order).find("AutoId").text());
                $("#txtOrderId").val($(order).find("OrderNo").text());
                $("#lblOrderno").text($(order).find("OrderNo").text());
                $("#txtOrderType").val($(order).find("OrderType").text());
                $("#txtPackerRemarks").val($(order).find("PackerRemarks").text());
                $("#txtCustomerType").val($(order).find("CustomerTypeName").text());
                $("#txtSalesPerson").val($(order).find("SalesPerson").text());
                $("#txtShippingType").val($(order).find("ShippingType").text());

                $("#hfOrdStatus").val($(order).find("StatusCode").text());
                $("#txtOrderDate").val($(order).find("OrderDate").text());
                if ($(order).find("CommentType").text() != '')
                    $("#ddlCommentType").val($(order).find("CommentType").text());
                $("#txtComment").val($(order).find("Comment").text());
                $("#txtDeliveryDate").val($(order).find("DeliveryDate").text());
                $("#txtOrderStatus").val($(order).find("Status").text());
                $("#txtCustomer").val($(order).find("CustomerName").text());
                $("#hiddenCustAutoId").val($(order).find("CustAutoId").text());
                $("#txtTerms").val($(order).find("Terms").text());
                $("#txtTerms").val($(order).find("TermsDesc").text());
                $("#OrderRemarks").val($(order).find("OrderRemarks").text());
                CustomerType = $(order).find("CustomerType").text();
                $("#CustomerType").val(CustomerType);
                $("#ShippingAutoId").val($(order).find("ShippingAutoId").text());

                $("#txtBillAddress").val($(order).find("BillAddr").text());
                $("#txtShipAddress").val($(order).find("ShipAddr").text());
                if ($(order).find("PackedBoxes").text() == '') {
                    if ($(order).find("StatusCode").text() == "9")//StatusCode
                    {
                        $("#txtPackedBoxes").val(0);
                    }
                    else {
                        $("#txtPackedBoxes").val(1);
                    }
                }
                else {
                    if ($(order).find("StatusCode").text() == "9")//StatusCode
                    {
                        $("#txtPackedBoxes").val(0);
                    }
                    else {
                        $("#txtPackedBoxes").val($(order).find("PackedBoxes").text());
                    }
                }
                $("#lblNoOfbox").text($(order).find("PackedBoxes").text());
                $("#tblProductDetail tbody tr").remove();
                var row = $("#tblProductDetail thead tr").clone(true);
                var checkview = false;
                $.each(product, function () {
                    $(".ProId", row).html("<span oldPacked='" + $(this).find("AddOnPackedPeice").text() + "' isFreeItem='" + $(this).find("isFreeItem").text() + "'></span>" + $(this).find("ProductId").text());
                    if (Number($(this).find("isFreeItem").text()) == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");

                    } else if (Number($(this).find("IsExchange").text()) == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");
                    }
                    else if (Number($(this).find("Tax").text()) == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>" + " <product class='badge badge badge-pill badge-danger '>Taxable</product>");
                    } else {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                    }
                    $(".PImage", row).html("<img src='" + $(this).find('ThumbnailImageUrl').text() + "' class='zoom img img-circle img-responsive' OnError='this.src =\"http://psmnj.a1whm.com/Attachments/default_pic.png\"' class='zoom img img-circle img-responsive' style='width: 40px;height: 40px;'  >");
                    if (CustomerType == '3') {
                        $(".TAction", row).html("<b><a title='Edit' href='#' onclick='EditPackType(" + $(this).find("OIAutoId").text() + "," + $(this).find("ProductAutoId").text() + "," + $(this).find("UnitAutoId").text() + "," + $(this).find("RequiredQty").text() + ")'><span class='la la-edit'></span></a></b>");
                    } else {
                        $(".TAction", row).html('');
                        $('#tblProductDetail').find('.TAction').hide();
                    }

                    $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                    $(".TtlPcs", row).text($(this).find("TotalPieces").text());
                    $(".QtyRemain", row).text($(this).find("QtyRemain").text());
                    $(".UnitPrice", row).html($(this).find("UnitPrice").text());
                    if (parseFloat($(this).find("Stock").text()) == 0) {
                        $(".RequiredQty", row).html($(this).find("RequiredQty").text());
                        $(".RequiredQty", row).addClass('bgStock');
                    }
                    else {
                        $(".RequiredQty", row).html($(this).find("RequiredQty").text());
                        $(".RequiredQty", row).removeClass('bgStock');
                    }

                    if (Number($(this).find("IsExchange").text()) == 1) {
                        $(".IsExchange", row).html('<span class="la la-check-circle success" IsExchange="1"></span>');
                    } else {
                        $(".IsExchange", row).html('<span IsExchange="0"></span>');
                    }
                    if ($(order).find('StatusCode').text() == 9) {
                        if ($(this).find("AddOnQty").text() == 0) {
                            $(row).hide();
                        } else {
                            $(row).show();
                            checkview = true;
                        }
                    }
                    $(".Barcode", row).text($(this).find("Barcode").text());
                    if ($(this).find("QtyShip").text() == null || $(this).find("QtyShip").text() == "" || $(this).find("QtyShip").text() == "0") {
                        $(".QtyShip", row).html("<input type='text' class='form-control border-primary input-sm text-center' maxlength='4' disabled onkeypress='return isNumberKey(event)' value='0' onchange='checkManual(this)' />");
                    } else {
                        $(".QtyShip", row).html("<input type='text' class='form-control border-primary input-sm  text-center' maxlength='4' onkeypress='return isNumberKey(event)' value='" + $(this).find("QtyShip").text() + "' onchange='checkManual(this)' />");
                    }
                    if (Number($(this).find("AddOnQty").text()) > 0) {
                        var AddOnQ = Number($(this).find("QtyPerUnit").text()) * Number($(this).find("AddOnQty").text());
                        $(".AddOnPiece", row).html('<span addonpacked="0" IsAddon=' + Number($(this).find("AddOnQty").text()) + '>' + AddOnQ + '</span>');
                    }
                    else {
                        $(".AddOnPiece", row).html('<span addonpacked="0" IsAddon="0">' + 0 + '</span>');
                    }
                    $('#tblProductDetail tbody').append(row);
                    row = $("#tblProductDetail tbody tr:last").clone(true);


                    /// add-on product can scanned which is not add by sales Manager

                });

                if ($(order).find('StatusCode').text() == '2' || $(order).find('StatusCode').text() == '9') {
                    $('#btnGenOrderCC').show();
                    $('#divBarcode').show();
                    if (CustomerType == '3') {
                        $('#tblProductDetail').find('.TAction').show();
                    }
                } else {
                    $('#btnGenOrderCC').hide();
                    $('#divBarcode').hide();
                    $('input').attr('disabled', 'disabled');
                    $('textarea').attr('disabled', 'disabled');
                    $('#tblProductDetail').find('.TAction').hide();
                }
                if (!checkview) {
                    $('#tblProductDetail tbody tr').show();
                }
                if (($(order).find('StatusCode').text() == 3 || $(order).find('StatusCode').text() == 9 || $(order).find('StatusCode').text() == 10)) {
                    $("#btnGenBar").show();
                }
                if (($(order).find('StatusCode').text() == 3 || $(order).find('StatusCode').text() == 10)) {
                    $("#ads").removeAttr('onclick');
                    $("#mns").removeAttr('onclick');
                    $("#btnAddBox").show();
                }
                else {
                    $("#ads").attr('onclick');
                    $("#mns").attr('onclick');
                    $("#btnAddBox").hide();
                }
                if ($(order).find('StatusCode').text() == 9) {
                    $("#lblPackedBoxes").text($(order).find("PackedBoxes").text());
                    $("#showPreviousBox").show();
                } else {
                    $("#showPreviousBox").hide();
                }
            }
          
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function EditPackType(OAutoId, ProductAutoId, UnitAutoId, OrderQty) {
    $("#hdnproductAutoId").val(ProductAutoId);
    $("#hdnOrderAutoId").val(OAutoId);
    $("#txtOrderQty").val(OrderQty);
    $("#modalEditPacking").modal('show');
    $("#hdnOrderQty").val(OrderQty);
    $("#hdnUnitAutoIdCheck").val(UnitAutoId);

    var productAutoId = ProductAutoId;
    $.ajax({
        type: "POST",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/bindUnitTypes",
        data: "{'productAutoId':" + productAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var unitDefault = $(xmldoc).find("Table1");
                var count = 0;

                $("#ddlUnitType option:not(:first)").remove();
                $.each(unitType, function () {
                    $("#ddlUnitType").append("<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                });
                $("#ddlUnitType").val(UnitAutoId);

                $("#hdnordCheck").val($("#ddlUnitType option:selected").attr("QtyPerUnit"));
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function calculateOrderQty() {
    var QtyPUnit = 0; var OrdQty = 0; TotalReqQty = 0; var oldOrdQty = 0;
    QtyPUnit = $("#ddlUnitType option:selected").attr("QtyPerUnit");
    OrdQty = $("#hdnordCheck").val();
    oldOrdQty = $("#hdnOrderQty").val();
    TotalReqQty = OrdQty * oldOrdQty / QtyPUnit;
    $("#txtOrderQty").val(Math.ceil(TotalReqQty));

}
function UpdateUnittype() {
    if (Number($("#txtOrderQty").val()) == 0) {
        toastr.error("Order quantity can't be zero or blank.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        $("#txtOrderQty").addClass('border-warning');
    }
    else {
        $("#txtOrderQty").removeClass('border-warning');
        var data = {
            OrderAutoId: $("#hdnOrderAutoId").val(),
            UnitType: $("#ddlUnitType").val(),
            QtyPerUnit: $("#ddlUnitType option:selected").attr("QtyPerUnit"),
            OrderQty: $("#txtOrderQty").val(),
        }
        $.ajax({
            type: "POST",
            url: "/Packer/WebAPI/WPackerOrderMaster.asmx/UpdateUnitType",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (result) {
                if (result.d == "Session Expired") {
                    window.location = "/";
                }
                else if (result.d == 'Success') {

                    swal("", "Unit Type updated successfully.", "success").then(function () {
                        $("#modalEditPacking").modal('hide');
                        getOrderData($("#txtHOrderAutoId").val());
                    });
                } else {
                    swal("", result.d, "error");
                }
            },
            error: function (result) {

            },
            failure: function (result) {

            }
        });
    }
}
function GenBar() {
    if ($('input.printTemplateClass:checked').length == 0) {
        toastr.error('Please select template .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else {
        var Url = $('input.printTemplateClass:checked').val();
        var finalUrl = Url + "?OrderId=" + $("#txtOrderId").val();
        window.open(finalUrl, "popUpWindow", "height=400,width=400,left=10,top=10,,scrollbars=yes,menubar=no");
    }

    //if ($("#Templ1").prop('checked') == true) {

    //    window.open("/Packer/orderBarcodePreview.html?OrderId=" + $("#txtOrderId").val(), "popUpWindow", "height=400,width=400,left=10,top=10,,scrollbars=yes,menubar=no");
    //}
    //else if ($("#Templ2").prop('checked') == true) {
    //    window.open("/Packer/orderBarcodeTemplate2.html?OrderId=" + $("#txtOrderId").val(), "popUpWindow", "height=400,width=400,left=10,top=10,,scrollbars=yes,menubar=no");
    //}
}


function printGenOrderModel() {
    $("#Templ1").attr('disabled', false);
    $("#Templ2").attr('disabled', false);
    $("#Templ1").prop('checked', true);
    var ShippingAutoId = $("#ShippingAutoId").val();
    getPackerAssignPrintData(ShippingAutoId);

}
function PrintOrder() {
    window.open("/Packer/OrderPrint.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=400,width=400,left=10,top=10,,scrollbars=yes,menubar=no");
}
function viewOrderLog() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WOrderLog.asmx/viewOrderLog",
        data: "{'OrderAutoId':" + $("#txtHOrderAutoId").val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var orderlog = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");

            $("#Span1").text(order.find("OrderNo").text());
            $("#lblOrderDate").text(order.find("OrderDate").text());

            $("#tblOrderLog tbody tr").remove();
            var row = $("#tblOrderLog thead tr").clone(true);
            if (orderlog.length > 0) {
                $("#EmptyTable").hide();
                $.each(orderlog, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("ActionDate").text());
                    $(".Action", row).text($(this).find("Action").text());
                    $(".Remark", row).text($(this).find("Remarks").text());

                    $("#tblOrderLog tbody").append(row);
                    row = $("#tblOrderLog tbody tr:last").clone(true);
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}
var PackerSecurityEnable = false;
function itemProduct() {
    if (!PackerSecurityEnable) {
        $("#txtSecurity").val('');
        $("#txtSecurity").removeAttr('disabled');
        $('#SecurityEnabled').modal('show');
        setTimeout(function () {
            $('#txtSecurity').focus();
        }, 1000);
    }
}
function clickonSecurity() {
    if ($("#txtSecurity").val() != '') {
        $('#hfItemKey').val($('#txtSecurity').val());
    }
    $.ajax({
        type: "Post",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/clickonSecurity",
        data: "{'CheckSecurity':'" + $("#txtSecurity").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    PackerSecurityEnable = true;
                    $("#SecurityEnabled").modal('hide');
                    $('#btnpkd').removeAttr('disabled');
                } else {
                    toastr.error('Access Denied .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function chechvalidation() {
    var check = true;
    $('#ddlitemproduct').closest('div').find('.select2-selection--single').removeAttr('style');
    $('#txtQty').removeClass('border-warning');
    if ($('#ddlitemproduct').val() == '0' || $('#ddlitemproduct').val() == '' || $('#ddlitemproduct').val() == null) {
        $('#ddlitemproduct').closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        check = false;
    }

    if ($('#txtQty').val() == '' || $('#txtQty').val() == '0') {
        $('#txtQty').addClass('border-warning');
        check = false;

    }
    return check;
}
function AddProductQty() {
    var Seq = $("#ddlitemproduct").find(':selected').attr('seq')
    var PType = 0, Exchange = 0, Free = 0, check = 0;
    localStorage.removeItem("RemainQty");
    localStorage.removeItem("NotShipQty");
    Exchange = $("#IsExchange").prop('checked') == true ? 1 : 0;
    Free = $("#IsFreeItem").prop('checked') == true ? 1 : 0;
    if (Number(Seq) == 1 && Exchange == 0) {
        toastr.error('You have chosen exchange product please check exchange check box.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return
    }
    if (Number(Seq) == 2 && Free == 0) {
        toastr.error('You have chosen free product please check free check box.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return
    }
    if (!PackerSecurityEnable) {
        itemProduct();
        return;
    }
    $("#tblProductDetail tbody tr").each(function () {
        if ($(this).find(".ProName span").attr("productautoid") == $("#ddlitemproduct").val() && $(this).find(".ProId span").attr("IsFreeItem") == Free && $("#ddlitemproduct").find(':selected').attr('unitautoid') == $(this).find(".UnitType span").attr("unitautoid") && Exchange == $(this).find(".IsExchange span").attr("isexchange")) {
            if (Number($(this).find(".RequiredQty").text()) < Number($(this).find(".QtyShip input").val()) + Number($('#txtQty').val())) {
                check = 1;
            }
        }
    })
    if (check == 1) {
        $("#QtyMore")[0].play();
        $('#txtScanBarcode').removeAttr('onchange');
        $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
        swal({
            title: "",
            text: "Shipped quantity is more than required qty",
            icon: "error",
            closeOnClickOutside: false
        }).then(function () {
            $("#txtScanBarcode").focus();
            $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
        })
        $("#txtScanBarcode").focus();
        invalidbarcodechnage();
        BarcodeData = [];
        return;
    }
    if (chechvalidation()) {
        if ($("#txtQty").val() != '') {
            var data = {
                OrderAutoId: $("#txtHOrderAutoId").val(),
                ProductAutoId: $("#ddlitemproduct").val(),
                UnitAutoId: $("#ddlitemproduct").find(':selected').attr('unitautoid'),
                IsExchange: $("#IsExchange").prop('checked') == true ? 1 : 0,
                IsFreeItem: $("#IsFreeItem").prop('checked') == true ? 1 : 0,
                Qty: $('#txtQty').val(),
                AddedItemKey: $('#hfItemKey').val()
            }
            $.ajax({
                type: "Post",
                async: false,
                url: "/Packer/WebAPI/WPackerOrderMaster.asmx/AddProductQty",
                data: JSON.stringify({ dataValue: JSON.stringify(data) }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d == "Barcode") {
                            $("#yes_audio")[0].play();
                            $('#txtScanBarcode').removeAttr('onchange');
                            $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
                            swal({
                                title: "",
                                text: "Barcode does not exists.",
                                icon: "error",
                                closeOnClickOutside: false
                            }).then(function () {
                                $("#txtScanBarcode").focus();
                                $("#txtScanBarcode").val("");
                                $("#txtScanBarcode").removeAttr('onchange', 'checkBarcode()');
                            })
                            $("#txtScanBarcode").focus();
                            invalidbarcodechnage();
                            BarcodeData = [];
                        }
                        else if (response.d == "Item") {
                            $("#No_audio")[0].play();
                            $('#txtScanBarcode').removeAttr('onchange');
                            $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
                            swal({
                                title: "",
                                text: "Item does not exists.",
                                icon: "error",
                                closeOnClickOutside: false
                            }).then(function () {
                                $("#txtScanBarcode").focus();
                                $("#txtScanBarcode").val("");
                                $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                            })
                            $("#txtScanBarcode").focus();
                            invalidbarcodechnage();
                            BarcodeData = [];
                        }
                        else if (response.d == "Stock") {
                            $("#Stock")[0].play();
                            $('#txtScanBarcode').removeAttr('onchange');
                            $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
                            swal({
                                title: "",
                                text: "Stock is not available.",
                                icon: "error",
                                closeOnClickOutside: false
                            }).then(function () {
                                $("#txtScanBarcode").focus();
                                $("#txtScanBarcode").val("");
                                $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                            })
                            $("#txtScanBarcode").focus();
                            invalidbarcodechnage();
                            BarcodeData = [];
                        }
                        else if (response.d == "Less") {
                            $("#mdAllowQtyConfirmation").modal('show');
                            return
                        }
                        else {
                            var xmldoc = $.parseXML(response.d);
                            var product = $(xmldoc).find("Table");
                            var prodAutoId = $("#ddlitemproduct").val();
                            var unitAutoId = $("#ddlitemproduct").find(':selected').attr('unitautoid');
                            var stock = $(product).find("Stock").text();
                            var barcode = $(product).find("Barcode").text();
                            var flag1 = true, flag2 = false, qty = 0, pieces = 0;
                            var countforscan = 0; var IsExchange = 0, IsFreeItem = 0;
                            if (product.length > 0) {
                                $("#tblProductDetail tbody tr").each(function () {
                                    if ($("#IsExchange").prop('checked')) {
                                        IsExchange = 1; PType = 1;
                                    }
                                    if ($("#IsFreeItem").prop('checked')) {
                                        IsFreeItem = 1; PType = 2;
                                    }
                                    if ($(this).find(".ProId span").attr("IsFreeItem") ==
                                        IsFreeItem && $(this).find(".ProName span").attr("productautoid") ==
                                        prodAutoId && $(this).find(".UnitType span").attr("unitautoid") ==
                                        unitAutoId && IsExchange == $(this).find(".IsExchange span").attr("isexchange")) {
                                        if ($("#hfOrdStatus").val() == "9") {
                                            qty = Number($("#txtQty").val());
                                        }
                                        else {
                                            qty = Number($("#txtQty").val()) + Number($(this).find(".QtyShip input").val());
                                        }
                                        pieces = qty * Number($(this).find(".UnitType span").attr('qtyperunit'));
                                        if (pieces <= stock && qty <= Number($(this).find(".RequiredQty").text())) {
                                            $(this).find(".Barcode").text(barcode);
                                            if (qty == Number($(this).find(".RequiredQty").text())) {
                                                $(this).find(".QtyShip input").val(qty).attr("disabled", false).css("background-color", "#fff").removeClass("verify");
                                                countforscan = countforscan + 1;
                                            } else {
                                                if ($("#hfOrdStatus").val() == "9") {
                                                    var addonQty = Number($(this).find(".QtyShip input").val());
                                                    $(this).find(".QtyShip input").val(qty + addonQty).attr("disabled", false).css("background-color", "white").addClass("verify");
                                                }
                                                else {
                                                    $(this).find(".QtyShip input").val(qty).attr("disabled", false).css("background-color", "#FFECB3").addClass("verify");
                                                }
                                            }
                                            if (Number($(this).find(".RequiredQty").text()) < qty || Number($(this).find(".RequiredQty").text()) == qty) {
                                                $(this).find(".QtyRemain").text(Number($(this).find(".RequiredQty").text()) - qty);
                                            }
                                            rowCal($(this).find(".QtyShip input"));

                                            var trfind = $(this);
                                            $('#tblProductDetail tbody tr:first').before(trfind);
                                        } else {
                                            if (pieces > stock) {
                                                $("#Stock")[0].play();
                                                $('#txtScanBarcode').removeAttr('onchange');
                                                $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
                                                flag2 = true;
                                                swal({
                                                    title: "",
                                                    text: "Stock is not available.",
                                                    icon: "error",
                                                    closeOnClickOutside: false
                                                }).then(function () {
                                                    $("#txtScanBarcode").val("");
                                                    $("#txtScanBarcode").focus();
                                                    $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                                                })
                                                $("#txtScanBarcode").focus();
                                                invalidbarcodechnage()
                                                BarcodeData = [];
                                            }
                                            if (qty > Number($(this).find(".RequiredQty").text())) {
                                                $("#QtyMore")[0].play();
                                                flag2 = true;
                                                swal({
                                                    title: "",
                                                    text: "Shipped Qty (" + qty + ") cannot be more than required quantity (" + $(this).find(".RequiredQty").text() + ")",
                                                    icon: "error",
                                                    closeOnClickOutside: false
                                                }).then(function () {
                                                    $("#txtScanBarcode").focus();
                                                    $("#txtScanBarcode").val("");
                                                    $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                                                })
                                                $("#txtScanBarcode").focus();
                                                invalidbarcodechnage()
                                                BarcodeData = [];
                                            }
                                        }
                                    } else if ($(this).find(".ProName span").attr("productautoid") == prodAutoId && $(this).find(".ProId span").attr("IsFreeItem") == IsFreeItem && $(this).find(".UnitType span").attr("unitautoid") == unitAutoId && IsExchange == $(this).find(".IsExchange span").attr("isexchange")) {
                                        if (Number($(this).find(".QtyShip input").val()) != Number($(this).find(".RequiredQty").text())) {
                                            countforscan = countforscan + 1;
                                        }
                                    }
                                });
                                if (flag2) {
                                    return;
                                }
                                $("#tblProductDetail tbody tr").each(function () {
                                    if ($(this).find(".ProName span").attr("productautoid") == prodAutoId && $(this).find(".ProId span").attr("IsFreeItem") == IsFreeItem && $(this).find(".UnitType span").attr("unitautoid") == unitAutoId && IsExchange == $(this).find(".IsExchange span").attr("isexchange")) {
                                        if (Number($(this).find(".RequiredQty").text()) == Number($(this).find(".QtyShip input").val())) {
                                            countforscan = 1;
                                        }
                                        else {
                                            countforscan = 2;
                                        }
                                    }
                                })
                                if (Number(countforscan) == 1) {
                                    $("#ddlitemproduct option").each(function () {
                                        var $this = $(this);
                                        if ($this.attr('seq') == PType && $this.val() == prodAutoId) {
                                            $(this).remove();
                                        }
                                    })
                                }
                                if ($('#mySelectList > option').length == 0) {
                                    localStorage.removeItem("ProductItems");
                                }
                            }
                        }
                        $("#txtScanBarcode").val("").focus();
                        $("#txtQty").val("1");
                        $("#IsExchange").prop("Checked") == false
                        $("#IsFreeItem").prop("Checked") == false
                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        else {
            toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function rowCal(e) {
    var row = $(e).closest("tr");
    if ((Number($(e).val()) == Number(row.find(".QtyRemain").html()))) {
        localStorage.setItem('RemainQty', Number(row.find(".QtyRemain").html()));
    }
    else if ((Number($(e).val()) > Number(row.find(".QtyRemain").html()))) {
        localStorage.setItem('RemainQty', (Number($(e).val()) - Number(row.find(".QtyRemain").html())));
    }
    else if ((Number($(e).val()) < Number(row.find(".QtyRemain").html()))) {
        localStorage.setItem('RemainQty', (Number(row.find(".QtyRemain").html()) - Number($(e).val())));
    }
    localStorage.setItem('NotShipQty', row.find(".QtyRemain").html());
    var totalPcs = (Number($(e).val()) * Number(row.find(".UnitType span").attr("QtyPerUnit")));
    row.find(".TtlPcs").text(totalPcs);
    if (Number($(e).val()) < Number(row.find(".RequiredQty").text()) || Number($(e).val()) == Number(row.find(".RequiredQty").text())) {
        row.find(".QtyRemain").text(Number(row.find(".RequiredQty").text()) - Number($(e).val()));
    }
}
function rowCalManual(e) {
    var row = $(e).closest("tr");
    if ((Number($(e).val()) == Number(row.find(".QtyRemain").html()))) {
        localStorage.setItem('RemainQty', Number(row.find(".QtyRemain").html()));
    }
    else if ((Number($(e).val()) > Number(row.find(".QtyRemain").html()))) {
        localStorage.setItem('RemainQty', (Number($(e).val()) - Number(row.find(".QtyRemain").html())));
    }
    else if ((Number($(e).val()) < Number(row.find(".QtyRemain").html()))) {
        localStorage.setItem('RemainQty', (Number(row.find(".QtyRemain").html()) - Number($(e).val())));
    }
    localStorage.setItem('NotShipQty', row.find(".QtyRemain").html());
    var totalPcs = (Number($(e).val()) * Number(row.find(".UnitType span").attr("QtyPerUnit")));
    row.find(".TtlPcs").text(totalPcs);
    if (Number($(e).val()) < Number(row.find(".RequiredQty").text()) || Number($(e).val()) == Number(row.find(".RequiredQty").text())) {
        row.find(".QtyRemain").text(Number(row.find(".RequiredQty").text()) - Number($(e).val()));
    }
    CheckStock(e);
}
function CheckStock(e) {
    var row = $(e).closest("tr");
    data = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        ProductAutoId: row.find('.ProName').find('span').attr('ProductAutoId'),
        unitAutoId: row.find('.UnitType span').attr('unitautoid'),
        IsFreeItem: row.find('.ProId span').attr('IsFreeItem'),
        IsExchange: row.find('.IsExchange span').attr('IsExchange'),
        QtyShip: Number($(e).val())
    }
    $.ajax({
        type: "Post",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/CheckStock",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var product = $(xmldoc).find("Table");
                if ($(product).find('msg').text() == "Stock Not Available") {
                    $("#Stock")[0].play();
                    $("#txtScanBarcode").removeAttr('change', 'checkBarcode()');
                    $("#txtScanBarcode").atrr('change', 'invalidbarcodechnage()');
                    swal({
                        title: "",
                        text: "Stock is not available.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function () {
                        row.find(".QtyShip input").val($(product).find('PackedBoxes').text());//remain 
                        row.find(".QtyRemain ").html(Number(row.find(".RequiredQty").text()) - Number($(product).find('PackedBoxes').text()));
                        row.find(".TtlPcs").html(Number(row.find(".QtyShip input").val()) * Number(row.find(".UnitType span").attr('qtyperunit')))
                        var htm = '';
                        if (Number(row.find(".RequiredQty").html()) > Number(row.find(".QtyRemain").html())) {
                            var getUnit = JSON.parse(localStorage.getItem('ProductItems'));
                            if (IsFreeItem == '1') {
                                htm = '(<product class="badge badge badge-pill badge-success">Exchange</product>)'
                            }
                            else if (IsExchange == '1') {
                                htm = '(<product class="badge badge badge-pill badge-primary">Exchange</product>)'
                            }
                            $.grep(getUnit, function (e) {
                                if (e.AutoId == data.productAutoId && e.UnitType == data.unitautoid) {
                                    $("#ddlitemproduct option[value='" + data.productAutoId + "']").remove();
                                    $("#ddlitemproduct").append($("<option value='" + e.AutoId + "' unitautoid='" + e.UnitType + "'>" + e.ProductName + htm + "</option>"));
                                }
                            })

                        }
                        $("#txtScanBarcode").focus();
                        $("#txtScanBarcode").val("");
                        $("#txtScanBarcode").atrr('change', 'checkBarcode()');
                        $(e).addClass('border-warning');
                    });
                    invalidbarcodechnage();
                    BarcodeData = [];
                }
                else {
                    $(e).removeClass('border-warning');
                    checkManual(e);
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
var PrdtItms = [];
function checkManual(e) {
    var htm = "", itemtype = 0;
    var OrdId = $("#txtHOrderAutoId").val();
    var tr = $(e).closest("tr");
    var reqQty = Number(tr.find(".RequiredQty").text());
    var qtyShip = Number(tr.find(".QtyShip input").val());
    var productAutoId = tr.find(".ProName span").attr('productautoid');
    var unitautoid = tr.find(".UnitType span").attr('unitautoid');
    var proName = tr.find(".ProId").text() + " " + tr.find(".ProName span").html();
    var isF = tr.find(".ProId span").attr('isfreeitem');
    var isE = tr.find(".IsExchange span").attr('isexchange');
    if (isF == "1") {
        htm = '(<product class="badge badge badge-pill badge-success">Free</product>)'
        itemtype = 2;
    }
    else if (isE == "1") {
        htm = '(<product class="badge badge badge-pill badge-primary">Exchange</product>)'
        itemtype = 1;
    }
    PrdtItms.push({
        AutoId: productAutoId,
        UnitType: unitautoid,
        ProductName: proName,
        itemType: itemtype
    })
    localStorage.setItem("ProductItems", JSON.stringify(PrdtItms));
    var getUnit = JSON.parse(localStorage.getItem('ProductItems'));
    if (Number(qtyShip) > Number(reqQty)) {
        $("#QtyMore")[0].play();
        var msg = "Shipped Qty (" + qtyShip + ") cannot be more than Required Qty (" + reqQty + ")";
        tr.find(".QtyShip input").val(Number(tr.find(".RequiredQty").html()) - Number(tr.find(".QtyRemain").html()));
        $('#txtScanBarcode').removeAttr('onchange');
        $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
        swal({
            title: "",
            text: msg,
            icon: "error",
            closeOnClickOutside: false
        }).then(function () {
            $("#txtScanBarcode").focus();
            $("#txtScanBarcode").val("");
            $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
        })
        invalidbarcodechnage()
        BarcodeData = [];
        return;
    }
    if (qtyShip == "" || qtyShip == 0) {
        tr.find(".QtyShip input").css("background-color", "#FFCCBC").addClass("verify");
        $.grep(getUnit, function (e) {
            if (e.AutoId == productAutoId && e.UnitType == unitautoid && e.itemType == itemtype) {
                $("#ddlitemproduct option").each(function (index, item) {
                    var $this = $(item);
                    if ($this.attr('seq') == itemtype && $this.val() == productAutoId) {
                        $this.remove();
                    }
                })
                //$("#ddlitemproduct option[value='" + productAutoId + "']").remove();
                $("#ddlitemproduct").append($("<option seq='" + itemtype + "' value='" + e.AutoId + "' unitautoid='" + e.UnitType + "'>" + e.ProductName + htm + "</option>"));
            }
        })
        UpdateAllocatedQuantity(e);
    } else {
        if (Number(qtyShip) > Number(reqQty)) {
            $("#QtyMore")[0].play();
            var msg = "Shipped Qty (" + qtyShip + ") cannot be more than Required Qty (" + reqQty + ")";
            $('#txtScanBarcode').removeAttr('onchange');
            $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
            swal({
                title: "",
                text: msg,
                icon: "error",
                closeOnClickOutside: false
            }).then(function () {
                tr.find(".QtyShip input").val(Number(tr.find(".RequiredQty").html()) - Number(tr.find(".QtyRemain").html()));
                tr.find(".TtlPcs").html(Number(tr.find(".QtyShip input").val()) * Number(tr.find(".UnitType span").attr('qtyperunit')))

                if (Number(tr.find(".RequiredQty").html()) > Number(tr.find(".QtyRemain").html())) {
                    $.grep(getUnit, function (e) {
                        if (e.AutoId == productAutoId && e.UnitType == unitautoid && e.itemType == itemtype) {
                            $("#ddlitemproduct option").each(function (index, item) {
                                var $this = $(item);
                                if ($this.attr('seq') == itemtype && $this.val() == productAutoId) {
                                    $this.remove();
                                }
                            })
                            $("#ddlitemproduct").append($("<option seq='" + itemtype + "' value='" + e.AutoId + "' unitautoid='" + e.UnitType + "'>" + e.ProductName + htm + "</option>"));
                        }
                    })
                }
                $("#txtScanBarcode").focus();
                $("#txtScanBarcode").val("");
                $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
            });
            invalidbarcodechnage();
            BarcodeData = [];
        }
        else {
            if (Number(qtyShip) < Number(reqQty)) {
                tr.find(".QtyShip input").css("background-color", "#FFCCBC").addClass("verify");
                var ProductItme = JSON.parse(localStorage.getItem('ProductItems'));
                $.grep(ProductItme, function (e) {
                    if (e.AutoId == productAutoId && e.UnitType == unitautoid && e.itemType == itemtype) {
                        $("#ddlitemproduct option").each(function (index, item) {
                            var $this = $(item);
                            if ($this.attr('seq') == itemtype && $this.val() == productAutoId) {
                                $this.remove();
                            }
                        })
                        $("#ddlitemproduct").append($("<option seq='" + itemtype + "' value='" + e.AutoId + "' unitautoid='" + e.UnitType + "'>" + e.ProductName + htm + "</option>"));
                    }
                })
            }
            else if (Number(qtyShip) == Number(reqQty)) {
                tr.find(".QtyShip input").css("background-color", "#fff").addClass("verify");
                var ProductItme = JSON.parse(localStorage.getItem('ProductItems'));
                $.grep(ProductItme, function (e) {
                    if (e.AutoId == productAutoId && e.UnitType == unitautoid && e.itemType == itemtype) {
                        $("#ddlitemproduct option").each(function (index, item) {
                            var $this = $(item);
                            if ($this.attr('seq') == itemtype && $this.val() == productAutoId) {
                                $this.remove();
                            }
                        })
                    }
                })
            }
            UpdateAllocatedQuantity(e);
        }
    }

}
function focusonBarcode() {
    $("#BarCodenotExists").modal('hide');
    $("#txtScanBarcode").focus();
    $("#txtScanBarcode").val('');
    $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
}
function invalidbarcodechnage() {

    $("#txtScanBarcode").val('');
    $("#txtScanBarcode").focus();
}
var BarcodeData = [];
function checkBarcode() {
    var Barcodeitem = { 
        "IsExchange": $("#IsExchange").prop('checked') == true ? 1 : 0,
        "IsFreeItem": $("#IsFreeItem").prop('checked') == true ? 1 : 0,
        "ReqQty": $('#txtQty').val() == "" ? 1 : $('#txtQty').val(),
        "BarCode": $("#txtScanBarcode").val(),
        "Status": 0,
    };
    BarcodeData.push(Barcodeitem);
    $("#txtScanBarcode").val('');
    $("#txtScanBarcode").focus()
    for (var i = 0; i < BarcodeData.length; i++) {
        var item = BarcodeData[i];
        if (item.Status == 0) {
            getbarcodeloop(item.IsExchange, item.IsFreeItem, item.ReqQty, item.BarCode);
            item.Status = 1;
        }       
    }

}
function getbarcodeloop(IsExchange,IsFreeItem, ReqQty,BarCode) {
    localStorage.removeItem("RemainQty");
    localStorage.removeItem("NotShipQty");
    var data = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        IsExchange: IsExchange,
        IsFreeItem: IsFreeItem,
        ReqQty: ReqQty,
        BarCode: BarCode.trim()
    } 
    $.ajax({
        type: "Post",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/checkBarcode",
        data: JSON.stringify({ pobj: data }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
          
        },
        complete: function () {
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                if (response.d == "Barcode") {
                    $('#txtScanBarcode').removeAttr('onchange');
                    $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
                    $("#yes_audio")[0].play();
                    swal({
                        title: "",
                        text: "Barcode (" + BarCode+") does not exists.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function () {
                        $("#txtScanBarcode").focus();
                        $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                    });
                    $("#txtScanBarcode").focus();
                    invalidbarcodechnage()
                    BarcodeData = [];
                }
                else if (response.d == "Item") {
                    $('#txtScanBarcode').removeAttr('onchange');
                    $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
                    $("#No_audio")[0].play();
                    swal({
                        title: "",
                        text: "Item does not exist.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function () {
                        $("#txtScanBarcode").focus();
                        $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                    });
                    $("#txtScanBarcode").focus();
                    invalidbarcodechnage()
                    BarcodeData = [];
                }
                else if (response.d == "Exchange") {
                    $('#txtScanBarcode').removeAttr('onchange');
                    $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
                    $("#No_audio")[0].play();
                    swal({
                        title: "Error!",
                        text: "You have chosen exchange product please check exchange check box.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function () {
                        $("#txtScanBarcode").focus();
                        $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                    });
                    $("#txtScanBarcode").focus();
                    invalidbarcodechnage()
                    BarcodeData = [];
                }
                else if (response.d == "FreeItem") {
                    $('#txtScanBarcode').removeAttr('onchange');
                    $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
                    $("#No_audio")[0].play();
                    swal({
                        title: "",
                        text: "You have chosen free product please check free check box.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function () {
                        $("#txtScanBarcode").focus();
                        $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                    });
                    $("#txtScanBarcode").focus();
                    invalidbarcodechnage()
                    BarcodeData = [];
                }
                else if (response.d == "Stock") {
                    $('#txtScanBarcode').removeAttr('onchange');
                    $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
                    $("#Stock")[0].play();
                    swal({
                        title: "",
                        text: "Stock is not available.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function () {
                        $("#txtScanBarcode").focus();
                        $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                    });
                    $("#txtScanBarcode").focus();
                    invalidbarcodechnage()
                    BarcodeData = [];
                }
                else if (response.d == "Less") {
                    $("#mdAllowQtyConfirmation").modal('show');
                    return
                }
                else {
                    var xmldoc = $.parseXML(response.d);
                    var product = $(xmldoc).find("Table");
                    var itemtype = 0;
                    if (data.IsExchange == 1) {
                        itemtype = 1;
                    }
                    else if (data.IsFreeItem == 1) {
                        itemtype = 2;
                    }
                    var prodAutoId = $(product).find("ProductAutoId").text();
                    var unitAutoId = $(product).find("UnitAutoId").text();
                    var stock = $(product).find("Stock").text();
                    var flag1 = true, flag2 = false, qty = 0, pieces = 0;
                    var countforscan = 0;
                    if (product.length > 0) {
                        $("#tblProductDetail tbody tr").each(function (index) {
                            var IsExchange = 0, IsFreeItem = 0;;
                            if ($("#IsExchange").prop('checked')) {
                                IsExchange = 1;
                            }
                            if ($("#IsFreeItem").prop('checked')) {
                                IsFreeItem = 1;
                            }
                            if ($(this).find(".ProId span").attr("IsFreeItem") == IsFreeItem && $(this).find(".ProName span").attr("productautoid") == prodAutoId && $(this).find(".UnitType span").attr("unitautoid") == unitAutoId && IsExchange == $(this).find(".IsExchange span").attr("isexchange")) {
                                if ($("#hfOrdStatus").val() == "9") {
                                    stock = Number(stock) + Number($(this).find(".ProId span").attr("oldpacked"));
                                }
                                qty = Number($("#txtQty").val()) + Number($(this).find(".QtyShip input").val());
                                pieces = qty * Number($(this).find(".UnitType span").attr('qtyperunit'));
                                if (pieces <= stock && qty <= Number($(this).find(".RequiredQty").text())) {

                                    $(this).find(".Barcode").text($(product).find("Barcode").text());
                                    if (qty == Number($(this).find(".RequiredQty").text())) {

                                        $("#ddlitemproduct option").each(function (index, ditem) {
                                            var $this = $(ditem);
                                            if ($this.attr('seq') == itemtype && $this.val() == prodAutoId) {
                                                $this.remove();
                                            }
                                        })
                                        $(this).find(".QtyShip input").val(qty).attr("disabled", false).css("background-color", "#fff").removeClass("verify");

                                        countforscan = countforscan + 1;
                                    } else {
                                        $(this).find(".QtyShip input").val(qty).attr("disabled", false).css("background-color", "#FFECB3").addClass("verify");
                                    }
                                    if (Number($(this).find(".RequiredQty").text()) < qty || Number($(this).find(".RequiredQty").text()) == qty) {

                                        $(this).find(".QtyRemain").text(Number($(this).find(".RequiredQty").text()) - qty);
                                    }
                                    rowCal($(this).find(".QtyShip input"));
                                    $('#tblProductDetail tbody tr:first').before($(this));
                                    $("#txtScanBarcode").focus();


                                } else {
                                    if (pieces > stock) {
                                        $('#txtScanBarcode').removeAttr('onchange');
                                        $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
                                        $("#Stock")[0].play();
                                        swal({
                                            title: "",
                                            text: "Stock is not available.",
                                            icon: "error",
                                            closeOnClickOutside: false
                                        }).then(function () {
                                            $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                                            $("#txtScanBarcode").focus();
                                        });
                                        $("#txtScanBarcode").focus();
                                        invalidbarcodechnage()
                                        BarcodeData = [];
                                    }

                                    if (qty > Number($(this).find(".RequiredQty").text())) {
                                        $('#txtScanBarcode').removeAttr('onchange');
                                        $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
                                        $("#QtyMore")[0].play();
                                        flag2 = true;
                                        swal({
                                            title: "",
                                            text: "Shipped quantity is more than required quantity.",
                                            icon: "error",
                                            closeOnClickOutside: false
                                        }).then(function () {
                                            $("#txtScanBarcode").focus();
                                            $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                                        });
                                        $("#txtScanBarcode").focus();
                                        invalidbarcodechnage()
                                        BarcodeData = [];
                                    }


                                }
                                flag1 = false;
                            }
                        });
                        if (flag2) {
                            $("#txtScanBarcode").focus();
                            return;
                        }
                        if (countforscan > 1) {
                            $("#ddlitemproduct option[value='" + prodAutoId + "']").hide();
                        }
                        if (flag1) {
                            $('#txtScanBarcode').removeAttr('onchange');
                            $("#txtScanBarcode").attr('onchange', 'invalidbarcodechnage()');
                            $("#No_audio")[0].play();
                            swal({
                                title: "",
                                text: "Item does not exist.",
                                icon: "error",
                                closeOnClickOutside: false
                            }).then(function () {
                                $("#txtScanBarcode").focus();
                                $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                            });
                            $("#txtScanBarcode").focus();
                            invalidbarcodechnage()
                            BarcodeData = [];

                        } else {
                            $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                        }

                    }
                }
                //$("#IsExchange").prop('checked', false);
                //$("#IsFreeItem").prop('checked', false);
                $("#txtQty").val("1");
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function UpdateAllocatedQuantity(e) {
    var OrdId = $("#txtHOrderAutoId").val();
    var tr = $(e).closest("tr");
    var reqQty = Number(tr.find(".RequiredQty").text());
    var qtyShip = Number(tr.find(".QtyShip input").val());
    var productAutoId = tr.find(".ProName span").attr('productautoid');
    var unitautoid = tr.find(".UnitType span").attr('unitautoid'); //QtyRemain
    var RemainQty = 0, totalPcs = 0, OverrideQty = 0;
    OverrideQty = tr.find(".QtyRemain").text();
    var isFree = tr.find(".ProId span").attr('isfreeitem');
    var isExchange = tr.find(".IsExchange span").attr('isexchange');
    if (Number(reqQty) > Number(qtyShip) || Number(reqQty) == Number(qtyShip)) {
        var data = {
            OrderAutoId: $("#txtHOrderAutoId").val(),
            productAutoId: productAutoId,
            unitautoid: unitautoid,
            qtyShip: qtyShip,
            isFree: isFree,
            isExchange: isExchange
        }
        $.ajax({
            type: "Post",
            url: "/Packer/WebAPI/WPackerOrderMaster.asmx/DeallocateProduct",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == "true") {
                    toastr.success('Packed quantity has been updated successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    if (Number(reqQty) > Number(qtyShip)) {
                        RemainQty = Number(Number(reqQty) - Number(qtyShip));
                        tr.find(".QtyRemain").text(RemainQty);
                    }
                    else {
                        RemainQty = Number(Number(reqQty) - Number(qtyShip));
                        tr.find(".QtyRemain").text(RemainQty);
                        RemainQty = reqQty;
                    }
                    tr.find(".QtyShip input").val(qtyShip);
                    totalPcs = (Number(qtyShip) * Number(tr.find(".UnitType span").attr("QtyPerUnit")));
                    tr.find(".TtlPcs").text(totalPcs);
                }
                else if (response.d == "Less") {
                    swal({
                        title: "",
                        text: "Product stock is not enough for this sales person.Do you want to override existing quantity with required quantity?",
                        icon: "",
                        showCancelButton: true,
                        buttons: {
                            cancel: {
                                text: "No, Cancel.",
                                value: null,
                                visible: true,
                                className: "btn-warning",
                                closeModal: true,
                            },
                            confirm: {
                                text: "Yes, Override.",
                                value: true,
                                visible: true,
                                className: "",
                                closeModal: true
                            }
                        }
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            tr.find(".QtyShip input").css("background-color", "rgb(255, 236, 179)").addClass("verify");
                            tr.find(".QtyShip input").val(Number(reqQty) - Number(OverrideQty));

                            totalPcs = (Number(Number(reqQty) - Number(OverrideQty)) * Number(tr.find(".UnitType span").attr("QtyPerUnit")));
                            tr.find(".TtlPcs").text(totalPcs);
                            $("#mdSecurityCheck").modal('show');
                            prdt = productAutoId; unt = unitautoid; qty = Number(qtyShip);
                            $("#btnMulipleCheck").hide();
                            $("#btnManual").show();
                        }
                        else {
                            tr.find(".QtyShip input").css("background-color", "rgb(255, 236, 179)").addClass("verify");
                            tr.find(".QtyShip input").val(Number(reqQty) - Number(OverrideQty));

                            totalPcs = (Number(Number(reqQty) - Number(OverrideQty)) * Number(tr.find(".UnitType span").attr("QtyPerUnit")));
                            tr.find(".TtlPcs").text(totalPcs);
                        }
                    });
                }
                else if (response.d == "AllocationNotUsed") {
                    swal({
                        title: "",
                        text: "Product stock is not enough for this sales person.Do you want to override existing quantity with required quantity?",
                        icon: "",
                        showCancelButton: true,
                        buttons: {
                            cancel: {
                                text: "No, Cancel!",
                                value: null,
                                visible: true,
                                className: "btn-warning",
                                closeModal: true,
                            },
                            confirm: {
                                text: "Yes, Override!",
                                value: true,
                                visible: true,
                                className: "",
                                closeModal: true
                            }
                        }
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            tr.find(".QtyShip input").css("background-color", "rgb(255, 236, 179)").addClass("verify");
                            tr.find(".QtyShip input").val(0);
                            totalPcs = Number(reqQty) * Number(tr.find(".UnitType span").attr("QtyPerUnit"));
                            tr.find(".TtlPcs").text(totalPcs);
                            tr.find(".QtyRemain").text(tr.find(".RequiredQty").text());
                            OverrideQtyManual(OrdId, productAutoId, unitautoid, Number(qtyShip));
                        }
                        else {
                            tr.find(".QtyShip input").css("background-color", "rgb(255, 236, 179)").addClass("verify");
                            tr.find(".QtyShip input").val(0);
                            totalPcs = Number(reqQty) * Number(tr.find(".UnitType span").attr("QtyPerUnit"));
                            tr.find(".TtlPcs").text(totalPcs);
                            tr.find(".QtyRemain").text(tr.find(".RequiredQty").text());
                        }
                    });
                }
                else if (response.d == "false") {
                    swal("", "Oops! Something went wrong. Please try later", "error")
                }
                else {
                    var xmldoc = $.parseXML(response.d);
                    var product = $(xmldoc).find("Table");
                    if ($(product).find('msg').text() == "StockNotAvailable") {

                        $('#txtScanBarcode').removeAttr('onchange');
                        $('#txtScanBarcode').attr('onchange', 'invalidbarcodechnage()');
                        $("#Stock")[0].play();
                        swal({
                            title: "",
                            text: "Stock is not available.",
                            icon: "error",
                            closeOnClickOutside: false
                        }).then(function () {
                            tr.find(".QtyShip input").css("background-color", "rgb(255, 236, 179)").addClass("verify");
                            tr.find(".QtyShip input").val(Number(reqQty) - Number(OverrideQty));
                            totalPcs = (Number(Number(reqQty) - Number(OverrideQty)) * Number(tr.find(".UnitType span").attr("QtyPerUnit")));
                            tr.find(".TtlPcs").text(totalPcs);
                            $(e).addClass('border-warning');
                            $("#txtScanBarcode").val("");
                            $("#txtScanBarcode").focus();
                            $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                        });
                        $("#txtScanBarcode").focus();
                        invalidbarcodechnage()
                        BarcodeData = [];
                    }
                    else if ($(product).find('msg').text() == "Stock Available") {
                        toastr.success('Quantity updated successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        if (Number(reqQty) > Number(qtyShip)) {
                            RemainQty = Number(Number(reqQty) - Number(qtyShip));
                            tr.find(".QtyRemain").text(RemainQty);
                        }
                        else {
                            RemainQty = Number(Number(reqQty) - Number(qtyShip));
                            tr.find(".QtyRemain").text(RemainQty);
                        }
                        tr.find(".QtyShip input").val(qtyShip);
                        totalPcs = (Number(qtyShip) * Number(tr.find(".UnitType span").attr("QtyPerUnit")));
                        tr.find(".TtlPcs").text(totalPcs);
                        tr.find(".Barcode").text($(product).find('Barcode').text());
                    }
                }
            }
        });
    }
}
/*-------------------------------------------------------------------------------------------------------------------*/
function Pack() {
    
    $("#txtPackedBoxes").removeClass('border-warning');
    if ($("#txtPackedBoxes").val() == '') {
        toastr.error('No. of packed boxes is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtPackedBoxes").addClass('border-warning');
        return;
    }
    if ($("#hfOrdStatus").val() != "9") {
        if (parseInt($("#txtPackedBoxes").val()) == 0) {
            $("#txtPackedBoxes").addClass('border-warning');
            toastr.error('No. of packed boxes should be greater than zero.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
    }
    var flag1 = false, flag2 = false, verify = false, count = 0;
    $("#tblProductDetail tbody tr").each(function () {
        if ($(this).find(".Barcode").text() == "") {
            count++;
        }
    });
    var checkPack = 0, notPacked = 0, tbllen = 0;
    $("#tblProductDetail tbody tr").each(function () {
        if (Number($(this).find(".RequiredQty").text()) != Number($(this).find(".QtyShip input").val()))//RequiredQty 
        {
            checkPack += checkPack + 1;
        }
        if (Number($(this).find(".QtyShip input").val()) == 0) {
            if (notPacked == 0) {
                notPacked += notPacked + 1
            }
            else {
                notPacked = notPacked + 1;
            }
        }
    });
    if (checkPack != 0) {
        verify = true;
    }
    if (count == $("#tblProductDetail tbody tr").length) {
        if (notPacked == count) {
            flag1 = true;
        }
    }
    tbllen = $('#tblProductDetail tr').length - 1;
    if (notPacked == tbllen) {
        flag1 = true;
        flag2 = true;
    }
    if (!flag1 && !flag2) {
        $("#tblProductDetail tbody tr > .QtyShip").each(function () {
            if (($(this).find("input").val() == '' || $(this).find("input").val() == '0')) {
                verify = true;
            }

        });
        if (!verify) {
            packingConfirmed();
        } else {
            swal({
                title: "Are you sure?",
                text: "Ship quantity of some product are less than ordered quantity. Are you sure you want to proceed?",
                icon: "",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "No, Cancel.",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Yes, Ship.",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    packingConfirmed();
                }
            });
        }

    } else {
        if (flag1) {
            toastr.error('Error : No item is packed.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    }
}
function packingConfirmed() {
    var Product = [], AllProduct = [];
    $("#tblProductDetail tbody tr").each(function () {

        if ($("#hfOrdStatus").val() == "9") {
            var newPacked = 0;
            newPacked = Number($(this).find('.QtyShip').find('input').val());
            newPacked = newPacked * Number($(this).find(".UnitType span").attr('qtyperunit'));
            var oldpacked = Number($(this).find('.ProId').find('span').attr('oldpacked'));
            if (oldpacked != newPacked) {
                Product.push({
                    'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
                    'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
                    'Barcode': $(this).find('.Barcode').text(),
                    'QtyShip': $(this).find('.QtyShip').find('input').val(),
                    'Pcs': newPacked,
                    'OldPacked': oldpacked,
                    'RemainQty': $(this).find('.QtyRemain').text(),
                    'isfreeitem': $(this).find('.ProId').find('span').attr('isfreeitem'),
                    'IsExchange': $(this).find('.IsExchange').find('span').attr('isexchange')
                });
            }
        }
        else {
            Product.push({
                'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
                'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
                'Barcode': $(this).find('.Barcode').text(),
                'QtyShip': $(this).find('.QtyShip').find('input').val(),
                'Pcs': $(this).find('.TtlPcs').text(),
                'OldPacked': oldpacked || 0,
                'RemainQty': $(this).find('.QtyRemain').text(),
                'isfreeitem': $(this).find('.ProId').find('span').attr('isfreeitem'),
                'IsExchange': $(this).find('.IsExchange').find('span').attr('isexchange')
            });
        }
        AllProduct.push({
            'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
            'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
            'Barcode': $(this).find('.Barcode').text(),
            'QtyShip': $(this).find('.QtyShip').find('input').val(),
            'Pcs': $(this).find('.TtlPcs').text(),
            'OldPacked': $(this).find('.ProId').find('span').attr('oldpacked')||0,
            'RemainQty': $(this).find('.QtyRemain').text(),
            'isfreeitem': $(this).find('.ProId').find('span').attr('isfreeitem'),
            'IsExchange': $(this).find('.IsExchange').find('span').attr('isexchange')
        });

    });

    var orderData = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        PackerRemarks: $("#txtPackerRemarks").val(),
        PackedBoxes: parseInt($("#txtPackedBoxes").val())
    };

    $.ajax({
        type: "Post",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/generatePacking",
        data: JSON.stringify({ dataValues: JSON.stringify(orderData), TableValues: JSON.stringify(Product), AllProduct: JSON.stringify(AllProduct)  }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var msg = $(xmldoc).find("Table1");
                var product = $(xmldoc).find("Table");
                if ($(msg).find("msg").text() == "Stock Not Available") {
                    $("#tblProductList tbody tr").remove();
                    var row = $("#tblProductList thead tr").clone(true);
                    $.each(product, function () {
                        $(".Pop_ProId", row).text($(this).find("ProductId").text());
                        $(".Pop_ProName", row).text($(this).find("ProductName").text());
                        $(".Pop_PImage", row).html("<img src='" + $(this).find('ThumbnailImageUrl').text() + "' class='zoom img img-circle img-responsive' style='width: 50px;height: 50px;'  >");
                        $(".Pop_RequiredQty", row).text($(this).find("RequiredQty").text());
                        $(".Pop_QtyShip", row).text($(this).find("QtyShip").text());
                        $(".Pop_AvilStock", row).text($(this).find("AvilQty").text());
                        $('#tblProductList tbody').append(row);
                        row = $("#tblProductList tbody tr:last").clone(true);
                    });
                    $("#CheckStock").modal('show');

                }
                else if (response.d == "InvalidPacker") {
                    swal("", "Unauthorized Packer. Please re-login.", "error").then(function () {
                        location.href = '/';
                    });
                }
                else if ($(msg).find("msg").text() == "Order has been packed") {
                    swal("", "Order packed successfully.", "success").then(function () {
                        getOrderData($("#txtHOrderAutoId").val());
                        BarcodeData = [];
                    });
                }
                else if ($(msg).find("msg").text() == "Order has been already packed") {
                    swal("", "Order has been already packed.", "error").then(function () {
                        location.href = '/';
                    });
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Plus() {
    if ($("#txtPackedBoxes").val() != "999") {
        var No = 0;
        No = (Number($("#txtPackedBoxes").val())) + 1;
        $("#txtPackedBoxes").val(No);
    } else {
        toastr.error('Error : You can not add more than 999 boxes.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function Minus() {
    if ($("#txtPackedBoxes").val() != "1" && $("#txtPackedBoxes").val() != "0") {
        var No = 0;
        No = (Number($("#txtPackedBoxes").val())) - 1;
        $("#txtPackedBoxes").val(No);
    } else {
        toastr.error('Error : 1 box required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function OpenSecurity() {
    $("#mdSecurityCheck").modal('show');
    $("#mdAllowQtyConfirmation").modal('hide');
}
function CloseAllowConfirm() {
    $("#mdAllowQtyConfirmation").modal('hide');
    $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
    $("#txtScanBarcode").val('');
}
function closeMnagerSecurity() {
    $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
    $("#txtScanBarcode").val('');
    $("#btnMulipleCheck").show();
    $("#btnManual").hide();
}
function CheckManagerSecurity() {
    var ProductAutoId = $("#ddlitemproduct").val();
    if (ProductAutoId == "0") {
        checkmanagersecuritykeyBarcode()
    }
    else {
        checkmanagersecuritykey()
    }
}
function CheckManualManagerSecurity() {
    checkmanagersecuritymanual(prdt, unt, qty);
}
function checkmanagersecuritymanual(productAutoId, unitautoid, qtyShip) {
    $("#btnMulipleCheck").show();
    $("#btnManual").hide();
    var data = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        ProductAutoId: productAutoId,
        UnitAutoId: unitautoid,
        CheckSecurity: $("#txtManagerSecurityCheck").val(),
        Quantity: qtyShip
    }
    $.ajax({
        type: "Post",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/clickonManagerSecurity",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    $("#mdSecurityCheck").modal('hide');
                    $("#txtManagerSecurityCheck").val('');
                    OverrideQtyManual($("#txtHOrderAutoId").val(), productAutoId, unitautoid, Number(qtyShip));
                } else {
                    toastr.error('Access Denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
function checkmanagersecuritykey() {
    var data = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        ProductAutoId: $("#ddlitemproduct").val(),
        UnitAutoId: $("#ddlitemproduct").find(':selected').attr('unitautoid'),
        CheckSecurity: $("#txtManagerSecurityCheck").val(),
        Quantity: $('#txtQty').val()
    }
    $.ajax({
        type: "Post",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/clickonManagerSecurity",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    $("#mdSecurityCheck").modal('hide');
                    $("#txtManagerSecurityCheck").val('');
                    $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                } else {
                    toastr.error('Access Denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
function checkmanagersecuritykeyBarcode() {
    var Barcode = $("#txtScanBarcode").val();
    var data = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        Qty: $('#txtQty').val() == "" ? 1 : $('#txtQty').val(),
        BarCode: Barcode.trim(),
        CheckSecurity: $("#txtManagerSecurityCheck").val()
    }
    $.ajax({
        type: "Post",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/clickonManagerSecurityBarcode",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    $("#mdSecurityCheck").modal('hide');
                    $("#txtScanBarcode").val('');
                    $("#txtScanBarcode").attr('onchange', 'checkBarcode()');
                } else {
                    toastr.error('Access Denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
function OverrideQtyManual(OID, PID, UID, QShip) {
    var data = {
        OrderAutoId: OID,
        ProductAutoId: PID,
        UnitAutoId: UID,
        Quantity: QShip
    }
    $.ajax({
        type: "Post",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/UpdateQtyManual",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'true') {
                $("#txtManagerSecurityCheck").val('');
            }
            else if (response.d = "Session Expired") {
                location.href = "/";
            }
            else {
                toastr.error('Oops something went wrong. Please try later.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function AddNewBoxOnPOPUp() {
    $("#ModalAddBox").modal('show');
    $("#txtExistBoxNo").val($("#txtPackedBoxes").val());
    $("#txtNoofBox").val($("#txtPackedBoxes").val());
}
function btnSaveAddBox() {
    var data = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        NoOfBoxes: $("#txtNoofBox").val()
    }
    $.ajax({
        type: "POST",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/SaveAddBoxes",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == "Session Expired") {
                window.location = "/";
            }
            else if (result.d == 'Success') {

                swal("", "Packed boxes has been updated successfully.", "success").then(function () {
                    $("#ModalAddBox").modal('hide');
                    getOrderData($("#txtHOrderAutoId").val());
                });
            } else {
                swal("", result.d, "error");
            }
        },
        error: function (result) {

        },
        failure: function (result) {

        }
    });
}
function Plus1() {
    if ($("#txtNoofBox").val() != "999") {
        var No = 0;
        No = (Number($("#txtNoofBox").val())) + 1;
        $("#txtNoofBox").val(No);
    } else {
        toastr.error('Error : You can not add more than 999 boxes.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function Minus1() {
    if ($("#txtNoofBox").val() != "1") {
        var No = 0;
        No = (Number($("#txtNoofBox").val())) - 1;
        $("#txtNoofBox").val(No);
    } else {
        toastr.error('Error : 1 box is Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}