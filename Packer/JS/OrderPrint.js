﻿$(document).ready(function () {
    debugger;
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('OrderAutoId');
    if (getid != null) {
        if (localStorage.getItem('checkprint') != '2') {
            getOrderData(getid);
        }
        else {
            AddOngetOrderData(getid);
        }
    }
});

/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                     Edit Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/getOrderData",
        data: "{'OrderAutoId':'" + OrderAutoId + "','check':'" + 1 + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            debugger
            var xmldoc = $.parseXML(response.d);
            var order = $(xmldoc).find("Table");
            var items = $(xmldoc).find("Table1");
            var orderId = $(order).find("OrderNo").text();
            $("#barcode").html(DrawCode39Barcode(orderId));
            var shipId = Number($(order).find("ShippingAutoId").text());
            if (shipId == 11 || shipId == 12 || shipId == 13 || shipId == 14 || shipId == 15 || shipId == 16) {
                $("#salesQuoteHash").show();
            }
            else {
                $("#salesQuoteHash").hide();
            }
            $("#orderId").text(orderId);
            $("#remarks").text($(order).find("OrderRemarks").text());
            $("#WarehouseRemarks").text($(order).find("WarehouseRemarks").text());
            if ($(order).find("OrderRemarks").text() != "") {
                $("#remarks").closest('tr').show();
            } else {
                $("#remarks").closest('tr').hide();
            }

            if ($(order).find("WarehouseRemarks").text() != "") {
                $("#WarehouseRemarks").closest('tr').show();
            } else {
                $("#WarehouseRemarks").closest('tr').hide();
            }

            $("#acNo").text($(order).find("CustomerId").text());
            $("#storeName").text($(order).find("CustomerName").text());
            $("#ContPerson").text($(order).find("Contact").text());
            $("#terms").text($(order).find("TermsDesc").text());

            $("#shipAddr").text($(order).find("ShipAddr").text() + ", " +
                $(order).find("State2").text() + ", " +
                $(order).find("City2").text() + ", " +
                $(order).find("Zipcode2").text());

            $("#billAddr").text($(order).find("BillAddr").text() + ", " +
                $(order).find("State1").text() + ", " +
                $(order).find("City1").text() + ", " +
                $(order).find("Zipcode1").text());

            $("#salesRep").html("<b>" + $(order).find("SalesPerson").text() + "</b>");
            $("#soDate").text($(order).find("OrderDate").text());
            $("#custType").text($(order).find("CustomerType").text());
            $("#pkdBy").html("<b>" + $(order).find("PackerName").text() + "</b>");
            $("#shipVia").text($(order).find("ShippingType").text());
            $("#CheckBy").text($(order).find("WarehouseName").text());
            if ($(order).find('DBName').text() == 'psmwpa') {
                $(".Location").hide();
            }
                
                var row = $("#tblProduct thead tr").clone(true);
            var RequiredQty = 0;
            $.each(items, function () {
                RequiredQty += parseInt($(this).find("RequiredQty").text())
                $(".ProId", row).text($(this).find("ProductId").text());
                $(".TotalPieces", row).text($(this).find("UIM").text());
                $(".ProName", row).text($(this).find("ProductName").text().replace(/&quot;/g, "\'"));
                $(".Qty", row).text($(this).find("RequiredQty").text());
                $(".UnitType", row).text($(this).find("UnitType").text());
                $(".Location", row).text($(this).find("ProductLocation").text());
                if (parseFloat($(this).find("Stock").text()) > 0) {
                    $(".Stock", row).html('<span style="color:green">In</span>');
                }
                else {
                    $(".Stock", row).html('<span style="color:red">Out</span>');
                }
                $('#tblProduct tbody').append(row);
                row = $("#tblProduct tbody tr:last").clone(true);
            });
            var totalQtyByCat = 0;
            $('#tblProduct tbody tr').each(function () {
                totalQtyByCat += Number($(this).find(".Qty").text());
            });
            $("#RequiredQty").html(RequiredQty)
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function AddOngetOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "/Driver/WebAPI/updateDelivery.asmx/AddOngetOrderData",
        data: "{'OrderAutoId':'" + OrderAutoId + "','check':'" + 1 + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var order = $(xmldoc).find("Table");
            var items = $(xmldoc).find("Table1");
            var orderId = $(order).find("OrderNo").text();
            $("#barcode").html(DrawCode39Barcode(orderId));
            $("#orderId").text(orderId);
            var shipId = Number($(order).find("ShippingAutoId").text());
            if (shipId == 11 || shipId == 12 || shipId == 13 || shipId == 14 || shipId == 15 || shipId == 16) {
                $("#salesQuoteHash").show();
            }
            else {
                $("#salesQuoteHash").hide();
            }
            $("#remarks").text($(order).find("OrderRemarks").text());
            $("#WarehouseRemarks").text($(order).find("WarehouseRemarks").text());
            if ($(order).find("OrderRemarks").text() != "") {
                $("#remarks").closest('tr').show();
            } else {
                $("#remarks").closest('tr').hide();
            }

            if ($(order).find("WarehouseRemarks").text() != "") {
                $("#WarehouseRemarks").closest('tr').show();
            } else {
                $("#WarehouseRemarks").closest('tr').hide();
            }

            $("#acNo").text($(order).find("CustomerId").text());
            $("#storeName").text($(order).find("CustomerName").text());
            $("#ContPerson").text($(order).find("Contact").text());
            $("#terms").text($(order).find("TermsDesc").text());

            $("#shipAddr").text($(order).find("ShipAddr").text() + ", " +
                $(order).find("State2").text() + ", " +
                $(order).find("City2").text() + ", " +
                $(order).find("Zipcode2").text());

            $("#billAddr").text($(order).find("BillAddr").text() + ", " +
                $(order).find("State1").text() + ", " +
                $(order).find("City1").text() + ", " +
                $(order).find("Zipcode1").text());

            $("#salesRep").html("<b>" + $(order).find("SalesPerson").text() + "</b>");
            $("#soDate").text($(order).find("OrderDate").text());
            $("#custType").text($(order).find("CustomerType").text());
            $("#pkdBy").html("<b>" + $(order).find("PackerName").text() + "</b>");
            $("#shipVia").text($(order).find("ShippingType").text());
            $("#CheckBy").text($(order).find("WarehouseName").text());

            var row = $("#tblProduct thead tr").clone(true);

            $.each(items, function () {
                $(".ProId", row).text($(this).find("ProductId").text());
                $(".TotalPieces", row).text($(this).find("TotalPieces").text());
                $(".ProName", row).text($(this).find("ProductName").text().replace(/&quot;/g, "\'"));
                $(".Qty", row).text($(this).find("RequiredQty").text());
                $(".UnitType", row).text($(this).find("UnitType").text());
                $(".Location", row).text($(this).find("ProductLocation").text());
                if (parseFloat($(this).find("Stock").text()) > 0) {
                    $(".Stock", row).html('<span style="color:green">In</span>');
                }
                else {
                    $(".Stock", row).html('<span style="color:red">Out</span>');
                }
                $('#tblProduct tbody').append(row);
                row = $("#tblProduct tbody tr:last").clone(true);
            });
            var totalQtyByCat = 0;
            $('#tblProduct tbody tr').each(function () {
                totalQtyByCat += Number($(this).find(".Qty").text());
            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}