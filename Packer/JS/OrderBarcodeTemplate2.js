﻿$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('OrderId');
    if (getid != null) {
        barcode_Print(getid);
    }
});

function barcode_Print(OrderNo) {
    $.ajax({
        type: "Post",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/barcode_Print",
        data: "{'OrderNo':'" + OrderNo + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var customer = $(xmldoc).find("Table");
            var CompanyAddress = $(xmldoc).find("Table1");

            var boxes = Number($(customer).find("StartBox").text());
            var startcount = 1;
            if (Number($(customer).find("PackedBoxes").text()) > 0) {
                if (Number($(customer).find("StartBox").text()) != Number($(customer).find("PackedBoxes").text())) {
                    startcount = (Number($(customer).find("StartBox").text()) - Number($(customer).find("PackedBoxes").text()) + 1);
                }
            }
            var value = "";
            var btype = "code128";
            var renderer = "css";

            var quietZone = false;

            var settings = {
                output: renderer,
            };

            var row1 = "";
            for (var i = startcount; i <= boxes; i++) {
                var codevalue = (OrderNo + '/' + i);
                value = {
                    code: codevalue, rect: true
                };
                $("#barcodeContent").barcode(value, btype, settings);
                $("#orderBarcodeContent").barcode(OrderNo, btype, settings);
                row1 += '<div style="text-align:center;">';
                row1 += '<div style="font-size: 46px;font-weight: 600;">Box ' + i + ' of ' + boxes + '</div>';
                row1 += '<div>To,</div>';
                row1 += '<div>' + $(customer).find("CustomerName").text() + '</div> ';
                row1 += '<div>' + $(customer).find("State").text() + ', ' + $(customer).find("City").text() + ', ' + $(customer).find("Zipcode").text() + '</div><br/>';
                row1 += '<div style="height:70px">' + $("#barcodeContent").html() + '</div><br/>';
                row1 += '<div style="font-size: 50px;font-weight: 600;">' + OrderNo + '</div><br/>';
                row1 += '<div style="height:70px">' + $("#orderBarcodeContent").html() + '</div><br/><br/>';
                row1 += "</div>";
                if (i < boxes) {
                    row1 += "<p  style='page-break-before: always'></p><br /> ";
                }


                $("#barcodeContent").html('');
                $("#orderBarcodeContent").html('');
            }
            $("#divPrint").html(row1);


            window.print();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}