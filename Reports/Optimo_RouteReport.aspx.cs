﻿using DLLOptimo_RouteReport;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_Optimo_RouteReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/Optimo_RouteReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetRouteReport(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Optimo_RouteReport pobj = new PL_Optimo_RouteReport();

        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt16(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt16(jdv["PageIndex"]);
                BL_Optimo_RouteReport.GetRouteReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }

    }
    [WebMethod(EnableSession = true)]
    public static string GetPlanningDetails(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Optimo_RouteReport pobj = new PL_Optimo_RouteReport();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.PlanningId = Convert.ToInt32(jdv["PlanningId"]);
                BL_Optimo_RouteReport.GetPlanningDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetDriverDetails(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Optimo_RouteReport pobj = new PL_Optimo_RouteReport();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.PlanningId = Convert.ToInt32(jdv["PlanningId"]);
                pobj.ReportAutoId = Convert.ToInt32(jdv["DriverAutoId"]);
                BL_Optimo_RouteReport.GetDriverDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetPrintDriverDetails(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Optimo_RouteReport pobj = new PL_Optimo_RouteReport();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.PlanningId = Convert.ToInt32(jdv["PlanningId"]);
                pobj.ReportAutoId = Convert.ToInt32(jdv["DriverAutoId"]);
             
                BL_Optimo_RouteReport.GetPrintDriverDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetAllPrintDriverDetails(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Optimo_RouteReport pobj = new PL_Optimo_RouteReport();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.PlanningId = Convert.ToInt32(jdv["PlanningId"]);
                pobj.ReportAutoId = Convert.ToInt32(jdv["DriverAutoId"]);
                pobj.AutoId = Convert.ToInt32(jdv["OrderBy"]);
                BL_Optimo_RouteReport.GetPrintDriverDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}