﻿using DLLCheckReceivedPayment;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_CheckReceivedpayment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/CheckReceivedPayment.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetReport(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_CheckReceivedPayment pobj = new PL_CheckReceivedPayment();
            pobj.Customer = Convert.ToInt32(jdv["CustomerStatus"]);
            pobj.ReceivedBy = Convert.ToInt32(jdv["ReceivedBy"]);
            pobj.Status = Convert.ToInt32(jdv["Status"]);
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            if (jdv["DepositFromDate"] != "")
                pobj.DepositFromDate = Convert.ToDateTime(jdv["DepositFromDate"]);
            if (jdv["DepositToDate"] != "")
                pobj.DepositToDate = Convert.ToDateTime(jdv["DepositToDate"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

            BL_CheckReceivedPayment.GetReport(pobj);

            return pobj.Ds.GetXml();

        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string BindCustomer()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_CheckReceivedPayment pobj = new PL_CheckReceivedPayment();
                BL_CheckReceivedPayment.GetCustomer(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}