﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLSaleByCategory;
using System.Data;
using DllUtility;
using System.Web.Script.Serialization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public partial class Reports_Sale_By_Category : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    public static string BindCategory()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_DLLSaleByCategory pobj = new PL_DLLSaleByCategory();
                BL_DLLSaleByCategory.BindCategory(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                    
                    return json;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string BindSubCategory(string CategoryAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_DLLSaleByCategory pobj = new PL_DLLSaleByCategory();
                pobj.CategoryAutoId= Convert.ToInt32(CategoryAutoId);
                BL_DLLSaleByCategory.BindSubCategory(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                    
                    return json;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetReportDetail(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_DLLSaleByCategory pobj = new PL_DLLSaleByCategory();
                pobj.CategoryAutoId = Convert.ToInt32(jdv["Category"]);
                pobj.SubCategoryAutoId = Convert.ToInt32(jdv["subCategory"]);
                
                if (jdv["FromDate"] != "")
                {
                    pobj.orderfrom = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_DLLSaleByCategory.BindProductSaleReport(pobj);
                //string json = "";
                //foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                //{
                //    json += dr[0].ToString();
                //}
                //if (json == "")
                //{
                //    json = "[]";
                //}
                return pobj.Ds.GetXml();


            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}