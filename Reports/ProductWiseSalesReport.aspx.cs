﻿using DLLByProductSalesReportDetail;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_OrderWiseOpenBalanceReport_ : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/js/ProductWiseSalesReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindCustomer(string CustomerType, string SalesPersonAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_ProductSalesReportDetail pobj = new PL_ProductSalesReportDetail();
                pobj.CustomerType = Convert.ToInt32(CustomerType);
                pobj.SalesPersonAutoId = Convert.ToInt32(SalesPersonAutoId);
                BL_ProductSalesReportDetail.BindCustomer(pobj);

                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindProductCategory()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_ProductSalesReportDetail pobj = new PL_ProductSalesReportDetail();
                BL_ProductSalesReportDetail.BindProductCategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindProductSubCategory(string CategoryAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_ProductSalesReportDetail pobj = new PL_ProductSalesReportDetail();
                pobj.CategoryAutoId = Convert.ToInt32(CategoryAutoId);
                BL_ProductSalesReportDetail.BindProductSubCategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindProduct(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ProductSalesReportDetail pobj = new PL_ProductSalesReportDetail();
                pobj.BrandAutoIdStr = jdv["BrandId"];
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubCategoryId = Convert.ToInt32(jdv["SubCategoryAutoId"]);
                BL_ProductSalesReportDetail.BindProduct(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindSalesPersonandStatus()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_ProductSalesReportDetail pobj = new PL_ProductSalesReportDetail();
                BL_ProductSalesReportDetail.BindSalesPersonandStatus(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetReportDetail(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ProductSalesReportDetail pobj = new PL_ProductSalesReportDetail();
                pobj.BrandAutoIdStr = jdv["BrandAutoIdStr"];
                pobj.ProductAutoIdStr = jdv["ProductAutoId"];
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(jdv["SalesAutoId"]);
                pobj.SubCategoryId = Convert.ToInt32(jdv["SubCategoryAutoId"]); 
                pobj.CustomerType = Convert.ToInt32(jdv["CustomerType"]);
                if (jdv["FromDate"] != "")
                {
                    pobj.CloseOrderFromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.CloseOrderToDate = Convert.ToDateTime(jdv["ToDate"]);
                }

                pobj.SearchBy = Convert.ToInt32(jdv["SearchBy"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_ProductSalesReportDetail.BindProductSaleReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}