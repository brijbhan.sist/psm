﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="Shipped_ProductWiseSalesReport.aspx.cs" Inherits="Reports_Shipped_ProductWiseSalesReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissable fade in" id="alertSuccessDelete" style="display: none;">
            <a aria-label="close" id="successDeleteClose" class="close" style="cursor: pointer;">&times;</a>
            <span></span>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" style="text-align: center;">
                <b>Product Wise Sales Report</b>
            </div>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="panel-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <select class="form-control input-sm" id="ddlAllCustomer">
                                            <option value="0">- All Customer -</option>
                                        </select>
                                    </div>
                                     <div class="form-group">
                                        <select class="form-control input-sm" id="ddlAllPerson">
                                            <option value="0">- All Sales Person -</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control input-sm" id="ddlAllCategory" onchange="BindProductSubCategory()">
                                            <option value="0">- All Category -</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control input-sm" id="AllSubCategory" onchange="BindProduct()">
                                            <option value="0">- All Sub Category -</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control input-sm" id="ddlProduct">
                                            <option value="0">- All Product -</option>
                                        </select>
                                    </div>
                                     <div class="form-group">
                                        <select class="form-control input-sm" id="ddlStatus">
                                            <option value="0">- All Status -</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm date" placeholder="From Date" id="txtSFromDate" onfocus="this.select()" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm date" placeholder="To Date" id="txtSToDate" onfocus="this.select()" />
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control input-sm" id="ddlPageSize">
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="30">30</option>
                                            <option value="100">100</option>
                                            <option value="200">200</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="tblOrderList">
                        <thead class="bg-blue white">
                            <tr>
                                <td class="OrderDate">Date</td>
                                <td class="OrderNo">Order No</td>
                                <td class="CustomerName">Customer Name</td>  
                                <td class="BillingAddress">Billing Address</td>                                
                                <td class="SalesPerson">Sales Person</td>
                                <td class="ProductId">Product ID</td>
                                <td class="ProductName">Product Name</td>
                                <td class="TotalPieces">Qty</td> 
                                <td class="UnitPrice">Unit Price</td>
                                <td class="PayableAmount" style="text-align: right">Item Total Amount</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7">Total</td>
                                <td id="TotalQty" style="text-align: left">0</td>
                                <td></td>
                                <td id="AmtDue">0.00</td>
                            </tr>
                             <tr>
                                <td colspan="7">AVG Per Piece</td>
                                <td style="text-align: left"></td>
                                <td></td>
                                <td id="AVGAmount" style="text-align:right">0.00</td>
                            </tr>
                        </tfoot>
                    </table>
                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                </div>
                <div class="Pager"></div>
            </div>
        </div>
    </div>
    <script src="js/ProductWiseSalesReport.js?ID=8.8.98" type="text/javascript"></script>
</asp:Content>


