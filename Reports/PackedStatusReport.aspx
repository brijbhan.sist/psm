﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/MasterPage.master" CodeFile="PackedStatusReport.aspx.cs" Inherits="Reports_PackedStatusReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblProductSalesStock tbody .ProductId, .Peice, .Box, .Case, .AvailableQty {
            text-align: center;
        }

        .tblwth {
            width: 7% !important;
        }

        .MyTableHeader tbody td {
            background: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order Profitability Report</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Order</a></li>
                        <li class="breadcrumb-item">Order Profitability Report</</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10036)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport()">Export</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm" onchange="GetCustomerBySalesPerson()" id="ddlSalesPerson">
                                                <option value="0">All Sales Person</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm" id="ddlShippingType">
                                                <option value="0">All Shipping Type</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm" id="ddlCustomer">
                                                <option value="0">All Customer</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtOrderNo" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick="searchReport();">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
    <div class="content-body">
        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                              <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblProfitReportDetail">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SalesPerson tblwth">Sales Person</td>
                                                        <td class="CustomerId tblwth">Customer ID</td>
                                                        <td class="CustomerName left">Customer Name</td>
                                                        <td class="OrderNo tblwth text-center">Order No</td>
                                                        <td class="DeliveryDate tblwth text-center">Delivery Date</td>
                                                        <td class="ShippingType left">Shipping Type</td>
                                                        <td class="TotalProduct tblwth text-center">Total Product</td>
                                                        <td class="OrderItems tblwth text-center">Total Order Item</td>
                                                        <td class="SubTotal width4per price right">Sub Total</td>
                                                        <td class="OverallDiscount tblwth price right">Overall Disc.</td>
                                                        <td class="PayableAmt tblwth price right">Payable Amt.</td>
                                                        <td class="Profit price right width4per">Profit</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="8" style="text-align: center !important">Total</td>
                                                        <td id="TSubTotal" class="right">0.00</td>
                                                        <td id="TOverallDiscount" class="right">0.00</td>
                                                        <td id="TPayableAmt" class="right">0.00</td>
                                                        <td id="TProfit" class="right">0.00</td>
                                                    </tr>
                                                    <tr style="font-weight: bold;" id="trTotal">
                                                        <td colspan="8" style="text-align: center !important">Overall Total</td>
                                                        <td id="OTSubTotal" class="right">0.00</td>
                                                        <td id="OTOverallDiscount" class="right">0.00</td>
                                                        <td id="OTPayableAmt" class="right">0.00</td>
                                                        <td id="OTProfit" class="right">0.00</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select class="form-control input-sm border-primary pagesize" onchange="missingReportOnChange(1)" id="ddlPageSize">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Order Profitability Report
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="p_SalesPerson text-center tblwth">Sales Person</td>
                        <td class="p_CustomerId width5per" style="width: 5% !important">Customer ID</td>
                        <td class="p_CustomerName left">Customer Name</td>
                        <td class="p_OrderNo" style="text-align: center !important; width: 5% !important">Order No</td>
                        <td class="p_DeliveryDate" style="text-align: center !important; width: 5% !important">Delivery Date</td>
                        <td class="p_ShippingType left">Shipping Type</td>
                        <td class="p_TotalProduct" style="text-align: center !important; width: 5% !important">Total Product</td>
                        <td class="p_OrderItems" style="text-align: center !important; width: 6% !important">Total Order Item</td>
                        <td class="p_SubTotal price right" style="width: 4% !important">Sub Total</td>
                        <td class="p_OverallDiscount price right" style="width: 5% !important">Overall Disc.</td>
                        <td class="p_PayableAmt price right width5per" style="width: 5% !important;">Payable Amt.</td>
                        <td class="P_Profit price right" style="width: 3% !important">Profit</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="8" class="text-center"><b>Total</b></td>
                        <td id="PTSubTotal" style="text-align: right; font-weight: bold;">0.00</td>
                        <td id="PTOverallDiscount" style="text-align: right; font-weight: bold;">0.00</td>
                        <td id="PTPayableAmt" style="text-align: right; font-weight: bold;">0.00</td>
                        <td id="PTProfit" style="text-align: right; font-weight: bold;">0.00</td>
                    </tr>

                </tfoot>
            </table>
        </div>
    </div>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>
