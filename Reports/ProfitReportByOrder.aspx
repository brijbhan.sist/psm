﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/MasterPage.master" CodeFile="ProfitReportByOrder.aspx.cs" Inherits="Reports_ProfitReportByOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblProductSalesStock tbody .ProductId, .Peice, .Box, .Case, .AvailableQty {
            text-align: center;
        }

        .tblwth {
            width: 7%!important;
        }
        .MyTableHeader tbody td{
            background:none;
        }
   

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Profit Report By Order</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Packer Report</a></li>
                        <li class="breadcrumb-item">Profit Report By Order</</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport()">Export</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                   <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm" id="ddlCustomer">
                                                <option value="0">All Customer</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm" id="ddlSalesPerson">
                                                <option value="0">All Sales Person</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm" id="ddlShippingType">
                                                <option value="0">All Shipping Type</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick="searchReport();">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>

    <div class="content-body">
        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblProfitReportDetail">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SalesPerson tblwth">Sales Person</td>
                                                        <td class="CustomerId tblwth">Customer ID</td>
                                                        <td class="CustomerName" style="width:10%;">Customer Name</td>
                                                        <td class="City tblwth">City</td>
                                                        <td class="State tblwth">State</td>
                                                        <td class="Zipcode tblwth">Zipcode</td>
                                                        <td class="OrderNo tblwth">Order No</td>
                                                        <td class="DeliveryDate tblwth">Delivery Date</td>
                                                        <td class="ShippingType tblwth">Shipping Type</td>
                                                        <td class="TotalProduct tblwth">Total Product</td>
                                                        <td class="OrderItems tblwth">Total Order Item</td>
                                                        <td class="SubTotal tblwth">Sub Total</td>
                                                        <td class="OverallDiscount tblwth">Overall Disc.</td>
                                                        <td class="PayableAmt tblwth">Payable Amt.</td>
                                                        <td class="Alert tblwth">Alert</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" onchange="missingReportOnChange(1)" id="ddlPageSize">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">

                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Profit Report By Order
                    <br />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="p_SalesPerson tblwth">Sales Person</td>
                        <td class="p_CustomerId tblwth">Customer ID</td>
                        <td class="p_CustomerName" style="width:10%;">Customer Name</td>
                        <td class="p_City tblwth">City</td>
                        <td class="p_State tblwth">State</td>
                        <td class="p_Zipcode tblwth">Zipcode</td>
                        <td class="p_OrderNo tblwth">Order No</td>
                        <td class="p_DeliveryDate tblwth">Delivery Date</td>
                        <td class="p_ShippingType tblwth">Shipping Type</td>
                        <td class="p_TotalProduct tblwth">Total Product</td>
                        <td class="p_OrderItems tblwth">Total Order Item</td>
                        <td class="p_SubTotal tblwth">Sub Total</td>
                        <td class="p_OverallDiscount tblwth">Overall Disc.</td>
                        <td class="p_PayableAmt tblwth">Payable Amt.</td>
                        <td class="p_Alert tblwth">Alert</td>

                    </tr>
                </thead>
                 <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>
