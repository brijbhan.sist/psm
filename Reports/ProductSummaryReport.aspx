﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ProductSummaryReport.aspx.cs" Inherits="Sales_ProductSummaryReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Product Summary</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Report</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Sales Report</a>
                        </li>
                        <li class="breadcrumb-item active">Product Summary
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-3 col-sm-12">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product Id" id="txtSProductId" />
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product Name" id="txtProductName" />
                                    </div>                            
                                    <div class="col-md-3 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="From Date" id="txtFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="To Date" id="txtToDate" onfocus="this.select()" />
                                        </div>
                                    </div>                                                          
                                </div>
                                 <div class="row form-group">
                                         <div class="col-md-12 col-sm-12">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1 pull-right btn-sm" id="btnSearch">Search</button>
                                    </div>
                                    </div>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblProductSummaryReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="ProductId text-center">Product ID</td>
                                                        <td class="ProductName">Product Name</td>
                                                        <td class="NetSales">Net Sales</td>                                                      
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="getRecords(1)">
                                            <option value="10" selected="selected">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive">
            <table class="table table-striped table-bordered hidden" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="ProductId text-center">Product ID</td>
                        <td class="ProductName">Product Name</td>
                        <td class="NetSales">Net Sales</td>   
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissable fade in" id="alertSuccessDelete" style="display: none;">
            <a aria-label="close" id="successDeleteClose" class="close" style="cursor: pointer;">&times;</a>
            <span></span>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/ProductSummary.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

