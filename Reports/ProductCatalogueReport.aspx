﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ProductCatalogueReport.aspx.cs" Inherits="Admin_ProductCatalogueReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 7%;
        }

        .Pager {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Product Catalog Report</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Product Report</a></li>
                        <li class="breadcrumb-item active">By Catalog</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10039)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport">Export</button>
                </div>
            </div>
        </div>
    </div>


    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlCategoryName" onchange="BindSubCategory();" style="width: 100%;">
                                            <option selected="selected" value="0">All Category</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlSubCategoryName">
                                            <option selected="selected" value="0">All Subcategory</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <input type="text" id="txtProductId" class="form-control input-sm border-primary" placeholder="Product ID" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" id="txtProductName" class="form-control input-sm border-primary" placeholder="Product Name" />
                                    </div>
                                    <div class="col-md-1 form-group">
                                        <input type="button" id="btnSearch" value="Search" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblProductCatelogReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="ProductId text-center width4per">Product ID</td>
                                                        <td class="CategoryName left" style="width: 13%">Category</td>
                                                        <td class="SubcategoryName left" style="width: 10%">Subcategory</td>
                                                        <td class="ProductName left">Product</td>
                                                        <td class="UnitType text-center width3per">Unit Type</td>
                                                        <td class="Qty text-center width4per">Quantity</td>
                                                        <td class="CostPrice width6per" style="text-align: right;">Cost Price</td>
                                                        <td class="SRP width3per" style="text-align: right;">SRP</td>
                                                        <td class="wholesaleminprice width6per" style="text-align: right;">Wholesale Min Price</td>
                                                        <td class="RetailMinPrice width6per" style="text-align: right;">Retailer Minimum Price</td>
                                                        <td class="BasePrice width6per" style="text-align: right;">Base Price</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right" id="pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Product Catalog Report
                    <br />
                    <span class="text-center" style="font-size: 8px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="P_ProductId text-center" style="width: 5%">Product ID</td>
                        <td class="P_CategoryName left" style="width: 13%">Category</td>
                        <td class="P_SubcategoryName left" style="width: 10%;">Subcategory</td>
                        <td class="P_ProductName left">Product</td>
                        <td class="P_UnitType text-center" style="width: 5%">Unit Type</td>
                        <td class="P_Qty text-center tblwth" style="width: 6%">Quantity</td>
                        <td class="P_CostPrice tblwth" style="text-align: right; width: 6%">Cost Price</td>
                        <td class="P_SRP right tblwth" style="text-align: right; width: 3%">SRP</td>
                        <td class="P_wholesaleminprice tblwth" style="text-align: right; width: 6%">Wholesale Min Price</td>
                        <td class="P_RetailMinPrice" style="text-align: right; width: 6%">Retailer Minimum Price</td>
                        <td class="P_BasePrice" style="text-align: right; width: 6%">Base Price</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/ProductCatalogueReport.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

