﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="OtpReport.aspx.cs" Inherits="Reports_OtpReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By VAPE</h3>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">OPT Report</a></li>
                        <li class="breadcrumb-item">By VAPE</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10025)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height: 400px">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm  date" placeholder="From Order Date" id="txtDateFrom" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="To Order Date" id="txtDateTo" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <select class="form-control border-primary input-sm" id="ddlTaxType">
                                                <option value="0">All Tax Type</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick="OtpReport(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblOtpReportSummary">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="InvoiceDate text-center wth7">Invoice Date</td>
                                                        <td class="InvoiceNumber text-center wth7">Invoice Number</td>
                                                        <%--<td class="NameofCommonCarrier left wth7">Carrier Name</td>--%>
                                                        <td class="SellerName left wth8">Seller Name</td>
                                                        <td class="Address left">Address</td>
                                                        <td class="City left wth6">City</td>
                                                        <td class="Zipcode text-center wth5">Zip Code</td>
                                                        <td class="StateName left wth7">State</td>
                                                        <td class="OTPLicenseNumber text-center wth6">OPT License No.</td>
                                                        <td class="BrandFamily text-center wth7">Brand Family</td>
                                                        <td class="ProductDescription left wth9">Product Description</td>
                                                        <td class="QuantityofItemSold text-center wth5">Sold Item Qty</td>
                                                        <td class="PurchasePrice price right wth5">Purchase Price</td>
                                                        <td class="TotalTaxCollected price right" style="width: 5%;">Total Tax Collection</td>
                                                        <td class="TaxCollectedYesOrNo text-center" style="width: 5%;">Tax Collection Status(Y/N)</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="10" class="text-center">Total</td>
                                                        <td id="TotalQtyS" class="text-center">0</td>
                                                        <td id="TotalPurchasePriceS" class="price right">0.00</td>
                                                        <td id="TotalTaxS" class="price right">0.00</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="10" class="text-center">Overall Total</td>
                                                        <td id="OTotalQty" class="text-center">0</td>
                                                        <td id="OTotalPurchasePrice" class="price right">0.00</td>
                                                        <td id="OTotalTax" class="price right">0.00</td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="  OtpReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">

            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    OPT Report By VAPE
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="P_InvoiceDate text-center wth7">Invoice Date</td>
                        <td class="P_InvoiceNumber text-center wth7">Invoice Number</td>
                        <%--<td class="P_NameofCommonCarrier left wth7">Carrier Name</td>--%>
                        <td class="P_SellerName left wth8">Seller Name</td>
                        <td class="P_Address left ">Address</td>
                        <td class="P_City left wth6">City</td>
                        <td class="P_Zipcode wth5" style="text-align: center !important">Zip Code</td>
                        <td class="P_StateName left wth7">State</td>
                        <td class="P_OTPLicenseNumber text-center wth6">OPT License No.</td>
                        <td class="P_BrandFamily wth7" style="text-align: center !important">Brand Family</td>
                        <td class="P_ProductDescription left wth9">Product Description</td>
                        <td class="P_QuantityofItemSold wth5" style="text-align: center !important">Sold Item Qty</td>
                        <td class="P_PurchasePrice right wth5">Purchase Price</td>
                        <td class="P_TotalTaxCollected right wth5">Total Tax Collection</td>
                        <td class="P_TaxCollectedYesOrNo wth5" style="text-align: center !important">Tax Collection Status (Y/N)</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr style="font-weight: bold;">
                        <td colspan="10" style="text-align: right !important;font-weight:bold;">Overall Total</td>
                        <td id="TotalQty" class="text-center" style="font-weight:bold;">0</td>
                        <td id="TotalPurchasePrice" class="RIGHT" style="font-weight:bold;">0.00</td>
                        <td id="TotalTax" class="RIGHT" style="font-weight:bold;">0.00</td>
                        <td></td>
                    </tr>

                </tfoot>
            </table>
        </div>
    </div>

    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

