﻿using DLLWrongPackingDetails;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_WrongPackingDetailsReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/WrongPackingDetailsReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredFields'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetPackingDetails(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_WrongPackingDetails pobj = new PL_WrongPackingDetails();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.ProductId = jdv["ProductId"];
                pobj.PageIndex = jdv["PageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_WrongPackingDetails.GetPackingDetails(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }

    }

    [WebMethod(EnableSession = true)]
    public static string GetProductList()
    {       
        PL_WrongPackingDetails pobj = new PL_WrongPackingDetails();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                BL_WrongPackingDetails.GetProductList(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }

    }
}