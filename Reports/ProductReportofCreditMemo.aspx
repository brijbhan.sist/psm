﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ProductReportofCreditMemo.aspx.cs" Inherits="Reports_ProductReportofCreditMemo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 5%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server"> 

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Credit Memo By Product</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Product Report</a>
                        </li>
                        <li class="breadcrumb-item active">Credit Memo By Product
                        </li>
                         <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10001)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport">Export</button>
                </div>
                <%--<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item">Export</button>                  
                </div>--%>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area2">
            <div class="row form-group">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control input-sm" id="ddlSalesPerson" onchange="BindCustomer()">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control input-sm" id="ddlCustomer">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    </div>
                                 <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                   From Credit Memo Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary date" placeholder="" id="txtFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                  To Credit Memo Date  <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary date" placeholder="" id="txtToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    
                                    <div class="col-sm-12 col-md-12">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1 pull-right  btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>
                                <%--<div class="row form-group">

                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <table class="MyTableHeader" id="tblProductReportofCreditMemo">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td class="CustomerId" style="width: 7%;text-align:center">Customer ID</td>
                                                    <td class="CustomerName left">Customer Name</td>
                                                    <td class="CreditNo" style="width: 6%;text-align:center">Credit No</td>
                                                    <td class="CreditDate" style="width: 7%;text-align:center">Credit Date</td>
                                                    <td class="SalesPerson center" style="width: 7%;text-align:center">Sales Person</td>
                                                    <td class="AppliedOrder center" style="width: 7%;text-align:center">Applied Order</td>
                                                    <td class="ProductId text-center" style="width: 7%;text-align:center">Product ID</td>
                                                    <td class="ProductName left">Product Name</td>
                                                    <td class="Unit text-center " style="width: 5%;text-align:center">Unit</td>
                                                    <td class="AcceptedQty text-center " style="width: 7%;text-align:center">Accepted Qty</td>
                                                    <td class="NetAmount price tblwth" style="width: 6%;text-align:right">Net Amount</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot>
                                                <tr style="font-weight: bold">
                                                    <td colspan="9" class="text-right">Total</td>
                                                    <td id="TotalAcceptedQty" class="center">0</td>
                                                    <td id="TotalNetAmount" class="right">0.00</td>
                                                </tr>
                                                <tr style="font-weight: bold">
                                                    <td colspan="9" class="text-right">Overall</td>

                                                    <td id="OverAllAcceptedQty" class="center">0</td>
                                                    <td id="OverAllNetAmount" class="right">0.00</td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Credit Memo By Product
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="P_CustomerId" style="width: 6%;text-align:center">Customer ID</td>
                        <td class="P_CustomerName left">Customer Name</td>
                        <td class="P_CreditNo" style="width: 6%;text-align:center">Credit No</td>
                        <td class="P_CreditDate" style="width: 7%;text-align:center">Credit Date</td>
                        <td class="P_SalesPerson center" style="width: 6%;text-align:center">Sales Person</td>
                        <td class="P_AppliedOrder center" style="width: 7%;text-align:center">Applied Order</td>
                        <td class="P_ProductId text-center" style="width: 7%;text-align:center">Product ID</td>
                        <td class="P_ProductName left">Product Name</td>
                        <td class="P_Unit text-center" style="width: 5%;text-align:center">Unit</td>
                        <td class="P_AcceptedQty text-center" style="width: 6%;text-align:center">Accepted Qty</td>
                        <td class="P_NetAmount price tblwth" style="width: 6%;text-align:right">Net Amount</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="9" style="font-weight: bold;text-align:right" class="text-right">Total</td>
                        <td id="OverAllAcceptedQtyPrint" style="font-weight: bold;text-align:center" class="center">0</td>
                        <td id="OverAllNetAmountPrint" style="font-weight: bold;text-align:right" class="right">0.00</td>
                    </tr>
                </tfoot>

            </table>
        </div>

    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="js/ProductReportofCreditMemo.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

