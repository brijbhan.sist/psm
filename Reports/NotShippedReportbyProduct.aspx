﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="NotShippedReportbyProduct.aspx.cs" Inherits="Reports_NotShippedReportbyProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 11%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Product </h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Not Shipped Report</a></li>
                        <li class="breadcrumb-item active">By Product</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10022)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">

                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">From Packing Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" onchange="setdatevalidation(1)" placeholder="From Packing Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">To Packing Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" onchange="setdatevalidation(2)" placeholder="To Packing Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlSortBy">
                                            <option value="1">By Product Id</option>
                                            <option value="2">By Product Name</option>
                                            <option value="3">By Not Shipped Qty</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlSortOrder">
                                            <option value="1">Ascending</option>
                                            <option value="2">Descending</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlStatus">
                                            <option value="0">All Status</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlSalesPerson">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick="getNotShippedReport(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblNotShippedReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="ProductId text-center width6per">Product ID</td>
                                                        <td class="ProductName left">Product</td>
                                                        <td class="Unit text-center width6per">Unit</td>
                                                        <td class="DefaultStock text-center width6per">Current Stock</td>
                                                        <td class="RequiredQty text-center width5per">Order Qty</td>
                                                        <td class="QtyShip text-center width7per">Shipped Qty</td>
                                                        <td class="RemainQty text-center width8per">Not Shipped Qty</td>
                                                        <td class="Amount price width12per" style="text-align: right;">Not Shipped Amount</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="4">Total</td>                                                      
                                                        <td id="ordqty">0</td>
                                                        <td id="shipqty" class="center">0</td>
                                                        <td id="nitshipqty" class="center">0</td>
                                                        <td id="notshipamt" class="right">0.00</td>
                                                    </tr>
                                                    <tr style="font-weight: bold;" id="trOTotal">
                                                        <td colspan="4">Overall Total</td>                                                
                                                        <td id="ordqtys">0</td>
                                                        <td id="shipqtys" class="center">0</td>
                                                        <td id="nitshipqtys" class="center">0</td>
                                                        <td id="notshipamts" class="right">0.00</td>
                                                    </tr>

                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPagesize" onchange="getNotShippedReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10 ">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">

            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Not Shipped Report By Product 
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="P_ProductId text-center tblwth" style="width: 11%">Product ID</td>
                        <td class="P_ProductName left">Product</td>
                        <td class="P_Unit text-center tblwth" style="width: 11%">Unit</td>
                        <td class="P_DefaultStock text-center width6per">Current Stock</td>
                        <td class="P_RequiredQty text-center tblwth" style="width: 11%">Order Qty</td>
                        <td class="P_QtyShip text-center tblwth" style="width: 11%">Shipped Qty</td>
                        <td class="P_RemainQty text-center tblwth" style="width: 11%">Not Shipped Qty</td>
                        <td class="P_Amount right tblwth" style="text-align: right; width: 11%;">Not Shipped Amount</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>

                    <tr>
                        <td colspan="4" style="font-weight: bold;">Overall Total</td>
                   
                        <td id="ordqtyss" style="font-weight: bold;">0</td>
                        <td id="shipqtyss" style="font-weight: bold;" class="center">0</td>
                        <td id="nitshipqtyss" style="font-weight: bold;" class="center">0</td>
                        <td id="notshipamtss" style="font-weight: bold;" class="right">0.00</td>
                    </tr>

                </tfoot>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/NotShippedReportbyProduct.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

