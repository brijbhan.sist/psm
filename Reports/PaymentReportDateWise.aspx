﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="PaymentReportDateWise.aspx.cs" Inherits="Reports_PaymentRportDateWise" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 7%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Order Payment</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>  </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>  </li>
                        <li class="breadcrumb-item"><a href="#">Account Report</a></li>
                        <li class="breadcrumb-item active">By Order Payment</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10011)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport() ">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height:400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlType">
                                            <option value="0">All Payment Mode</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                    From Settled Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" onchange="setdatevalidation(1)" placeholder="From Settled Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                   To Settled Date  <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" onchange="setdatevalidation(2)" placeholder="To Settled Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick="GetPaymentDateWiseReport(1);">Search</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblpaymentReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="PayDate" style="width:6%;">Pay Date </td>
                                                        <td class="OrderNo" style="width:6%;">Order No </td>
                                                        <td class="OrderAmount tblwth">Order Amount</td>
                                                        <td class="DueAmount tblwth">Due Amount Before </td>
                                                        <td class="PayAmount tblwth">Pay Amount </td>
                                                        <td class="ApplyAmount tblwth">Apply Amount</td>
                                                        <td class="DueBalance tblwth">Due Balance After</td>
                                                        <td class="CreditAmount tblwth">Transfer to Store Credit </td>
                                                        <td class="Paymentmode tblwth">Payment Mode</td>
                                                        <td class="ReceivedBy tblwth left" style="width:11%;">Payment Received By</td>
                                                        <td class="ApplyBy left" style="width:11%;">Payment Apply By</td>
                                                        <td class="Remark left">Payment Remark</td>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold;" id="trTotal">
                                                        <td class="right" colspan="2">Total Amount</td>
                                                        <td class="right" id="TotalOrderAmount">0.00</td>
                                                        <td class="right" id="TotalDueAmount">0.00</td>
                                                        <td class="right" id="TotalPayAmount">0.00</td>
                                                        <td class="right" id="TotalApplyAmount">0.00</td>
                                                        <td class="right" id="TotalDueBalance">0.00</td>
                                                        <td class="right" id="TotalCreditAmount">0.00</td>
                                                        <td colspan="5"></td>
                                                    </tr>
                                                    <tr style="font-weight: bold;">
                                                        <td class="right" colspan="2">Overall Amount</td>
                                                        <td class="right" id="TotalOrderAmounts">0.00</td>
                                                        <td class="right" id="TotalDueAmounts">0.00</td>
                                                        <td class="right" id="TotalPayAmounts">0.00</td>
                                                        <td class="right" id="TotalApplyAmounts">0.00</td>
                                                        <td class="right" id="TotalDueBalances">0.00</td>
                                                        <td class="right" id="TotalCreditAmounts">0.00</td>
                                                        <td colspan="5"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="GetPaymentDateWiseReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                  By Order Payment
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">

                <thead class="bg-blue white">
                    <tr>
                        <td class="PayDate text-center" style="width:5%;">Pay Date</td>
                        <td class="OrderNo text-center" style="width:5%;">Order No</td>
                        <td class="OrderAmount right text-center wth6">Order Amount</td>
                        <td class="DueAmount right text-center wth6">Due Amount Before</td>
                        <td class="PayAmount right text-center wth6">Pay Amount </td>
                        <td class="ApplyAmount right text-center wth6">Apply Amount</td>
                        <td class="DueBalance right text-center wth6">Due Balance After</td>
                        <td class="CreditAmount right text-center wth6">Transfer to Store Credit</td>
                        <td class="Paymentmode text-center wth6">Payment Mode</td>
                        <td class="ReceivedBy text-center" style="width:8%;">Payment Received By</td>
                        <td class="ApplyBy text-center" style="width:8%;">Payment Apply By</td>
                        <td class="Remark left text-center">Payment Remark</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>

                    <tr>
                        <td class="right" style="font-weight: bold;" colspan="2">Total Amount</td>
                        <td class="right" style="font-weight: bold;" id="TotalOrderAmountw">0.00</td>
                        <td class="right" style="font-weight: bold;" id="TotalDueAmountw">0.00</td>
                        <td class="right" style="font-weight: bold;" id="TotalPayAmountw">0.00</td>
                        <td class="right" style="font-weight: bold;" id="TotalApplyAmountw">0.00</td>
                        <td class="right" style="font-weight: bold;" id="TotalDueBalancew">0.00</td>
                        <td class="right" style="font-weight: bold;" id="TotalCreditAmountw">0.00</td>
                        <td colspan="5"></td>

                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

   
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

