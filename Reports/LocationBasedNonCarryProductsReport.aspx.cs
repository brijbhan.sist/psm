﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLLocationBasedNonCarryProductsReport;
using Newtonsoft.Json;

public partial class Reports_NonCarryProductsReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/Js/LocationBasedNonCarryProductsReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]

    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string bindCategory()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_NonCarryProductsReport pobj = new PL_NonCarryProductsReport();
            try
            {
                BL_NonCarryProductsReport.bindCategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string bindSubcategory(int categoryAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_NonCarryProductsReport pobj = new PL_NonCarryProductsReport();
            try
            {
                pobj.CategoryAutoId = categoryAutoId;
                BL_NonCarryProductsReport.bindSubcategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]

    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string GetReport(PL_NonCarryProductsReport pobj)
    {
        try
        {
            BL_NonCarryProductsReport.Getreport(pobj);
            return pobj.Ds.GetXml();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]

    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string InactiveProduct(string TableValues)
    {
        PL_NonCarryProductsReport pobj = new PL_NonCarryProductsReport();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoIds = TableValues;
                BL_NonCarryProductsReport.InactiveProduct(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}