﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllLogoutTrackingReport;

public partial class Reports_LogoutTrackingReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/LogoutTrackingReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetLogoutTrackingReport(string dataValue)
    {
        try
        {
            PL_LogoutTrackingReport pobj = new PL_LogoutTrackingReport();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            pobj.Opcode = 41;
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            pobj.UserName = jdv["UserName"];
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            DL_LogoutTrackingReport.ReturnTable(pobj);
            return pobj.Ds.GetXml();
        }
        catch (Exception)
        {
            return "false";
        }
    }
}