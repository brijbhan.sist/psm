﻿var row1 = "";
$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    BindSalesPerson();
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);

});

function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
};

/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function BindSalesPerson() {
    $.ajax({
        type: "POST",
        url: "SalesBy_customer_SalesPerson.aspx/BindSalesPerson",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlSalesPerson").append("<option value='" + getData[index].SId + "'>" + getData[index].SP + "</option>");
                });
                $("#ddlSalesPerson").select2({
                    placeholder: 'All Sales Person',
                    allowClear: true
                });
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}


/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function BindReport(PageIndex) {
    var SalesPersons = 0;
    $("#ddlSalesPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPersons = $(this).val() + ',';
        } else {
            SalesPersons += $(this).val() + ',';
        }
    });
    var data = {
        SalesPerson: SalesPersons,
        PageSize: $("#ddlPaging").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "SalesBy_customer_SalesPerson.aspx/SaleByCustomerSalesPersonDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: successBindSalesPersonReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successBindSalesPersonReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var OrderTotal = $(xmldoc).find("Table");
            var OrderTotals = $(xmldoc).find("Table2");
            $("#tblOrderList tbody tr").remove();
            var row = $("#tblOrderList thead tr").clone(true);
            var GrandAmount = 0.00, NoOfOrders = 0.;
            if (OrderTotal.length > 0) {
                $("#EmptyTable").hide();

                $.each(OrderTotal, function () {
                    $(".EmployeeName", row).text($(this).find("EmployeeName").text());
                    $(".NoOfOrders", row).text($(this).find("NoOfOrders").text());
                    $(".TotalOrderAmount", row).text($(this).find("GrandTotal").text());
                    NoOfOrders += parseInt($(this).find("NoOfOrders").text());
                    GrandAmount += parseFloat($(this).find("GrandTotal").text());
                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);
                });
                $('#AVGPiece').html(parseFloat(GrandAmount / NoOfOrders).toFixed(2));
            } else {
                // $("#EmptyTable").show();
                $('#AVGPiece').html('0.00');

            }
            $('#GrandAmount').html(parseFloat(GrandAmount).toFixed(2));
            $('#NoOfOrders').html(parseFloat(NoOfOrders).toFixed());

            $(OrderTotals).each(function () {
                $('#NoOfOrderss').html(parseFloat($(this).find('NoOfOrders').text()).toFixed(0));
                $('#GrandAmounts').html(parseFloat($(this).find('GrandTotal').text()).toFixed(2));
            });
            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}


function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var SalesPersons = 0;
    $("#ddlSalesPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPersons = $(this).val() + ',';
        } else {
            SalesPersons += $(this).val() + ',';
        }
    });
    var data = {
        SalesPerson: SalesPersons,
        PageSize: 0,
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/WBindSalesPersonReport.asmx/SaleByCustomerSalesPersonDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ReportDetails = $(xmldoc).find("Table");
                    var OTotal = $(xmldoc).find("Table2");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    var GrandAmount = 0.00, NoOfOrders = 0.00;
                    if (ReportDetails.length > 0) {
                        $("#EmptyTable").hide();
                        var html1 = "", TotalPiece = 0, TotalCase = 0, TotalBox = 0, ItemQty = 0, ProductTotal = 0.00, ProductId = 0, CustomerAutoId = 0;
                        var R_TotalPiece = 0, R_TotalCase = 0, R_TotalBox = 0;
                        $.each(ReportDetails, function (index) {

                            $(".EmployeeNamea", row).text($(this).find("EmployeeName").text());
                            $(".NoOfOrdersa", row).text($(this).find("NoOfOrders").text());
                            $(".TotalOrderAmounta", row).text($(this).find("GrandTotal").text());
                            NoOfOrders += parseInt($(this).find("NoOfOrders").text());
                            GrandAmount += parseFloat($(this).find("GrandTotal").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                        $('#AVGPieces').html(parseFloat(GrandAmount / NoOfOrders).toFixed(2));
                        $('#GrandAmount').html(parseFloat(GrandAmount).toFixed(2));
                        $('#NoOfOrders').html(parseFloat(NoOfOrders).toFixed());

                        $(OTotal).each(function () {
                            $('#NoOfOrdersq').html(parseFloat($(this).find('NoOfOrders').text()).toFixed(0));
                            $('#GrandAmountq').html(parseFloat($(this).find('GrandTotal').text()).toFixed(2));
                        });

                        if (us == 1) {

                            var image = $("#imgName").val();
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                            $("#PrintDate").text("Print Date :" + (new Date()).format("MM/dd/yyyy hh:mm tt"));
                            $("#PrintLogo").attr("src", "/Img/logo/" + image);
                            if ($("#txtSFromDate").val() != "") {
                                $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                            }
                            mywindow.document.write($(PrintTable1).clone().html());
                           

                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        }
                        if (us == 2) {
                            $("#PrintTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "Sales Report by Customer Sales Person",
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });
                        }
                    }
                }
            }
           

        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
};




