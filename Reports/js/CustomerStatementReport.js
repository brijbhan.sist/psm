﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $("#lblBalanceForwardAmount").text('0.00');
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    BindCustomer();

});

function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
};

/*---------------------------------------------------Bind Customer List-----------------------------------------------------------*/
function BindCustomer() {

    $.ajax({
        type: "POST",
        url: "customerStatementReport.aspx/BindCustomerList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);

            $("#ddlCustomer option:not(:first)").remove();
            $.each(getData, function (index, item) {
                $("#ddlCustomer").append("<option value='" + getData[index].CUId + "'>" + getData[index].CUN + "</option>");
            });
            $("#ddlCustomer").select2({
                placeholder: '[Choose Customer]',               
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}


/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function BindReport(PageIndex) {
    if ($("#ddlPageSize").val() == "0") {
        $(".Pager").hide();
    }
    else {
        $(".Pager").show();
    }
    var CustomerList = 0;
    $("#ddlCustomer option:selected").each(function (i) {
        if (i == 0) {
            CustomerList = $(this).val() + ',';
        } else {
            CustomerList += $(this).val() + ',';
        }
    });
    var data = {
        CustomerList: CustomerList,
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: PageIndex,
        PageSize: $('#ddlPageSize').val()
    };
    $.ajax({
        type: "POST",
        url: "customerStatementReport.aspx/BindCustomerStatementReport",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successBindSalesPersonReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successBindSalesPersonReport(response) {
    if (response.d == 'false') {
    } else {
        var xmldoc = $.parseXML(response.d);
        var OrderTotal = $(xmldoc).find("Table");
        var BalanceAmount = $(xmldoc).find("Table3");
        var FinalOrderTotal = $(xmldoc).find("Table2");

        var ForwardAmount = 0.00;
        if ($(BalanceAmount).find("BalanceForwardAmount").text() != '') {
            ForwardAmount = $(BalanceAmount).find("BalanceForwardAmount").text();
            $("#lblBalanceForwardAmount").text(parseFloat($(BalanceAmount).find("BalanceForwardAmount").text()).toFixed(2));
        }
        $("#tblCustomerStatementReport tbody tr").remove();
        var row = $("#tblCustomerStatementReport thead tr").clone(true);
        var TotalBalanceAmount = 0.00, FinalTotalBalanceAmount = 0.00, NoOfOrders = 0.;
        if (OrderTotal.length > 0) {
            $("#EmptyTable").hide();
            $.each(OrderTotal, function (index) {
                $(".TransactionDate", row).text($(this).find("TransactionDate").text());
                $(".TransactionType", row).text($(this).find("TransactionType").text());
                $(".TotalOrderAmount", row).text($(this).find("OrderAmount").text());
                $(".TotalPaidAmount", row).text($(this).find("PaidAmount").text());
                if (index == 0) {
                    TotalBalanceAmount = ((parseFloat(ForwardAmount) + parseFloat(TotalBalanceAmount) + parseFloat($(this).find("OrderAmount").text())) - parseFloat($(this).find("PaidAmount").text()))
                }
                else {
                    TotalBalanceAmount = ((parseFloat(TotalBalanceAmount) + parseFloat($(this).find("OrderAmount").text())) - parseFloat($(this).find("PaidAmount").text()));
                }

                $(".TotalBalanceAmount", row).text(TotalBalanceAmount.toFixed(2));

                $("#tblCustomerStatementReport tbody").append(row);
                row = $("#tblCustomerStatementReport tbody tr:last").clone(true);
            });

            $.each(FinalOrderTotal, function (index) {
                if (index == 0) {
                    FinalTotalBalanceAmount = ((parseFloat(ForwardAmount) + parseFloat(FinalTotalBalanceAmount) + parseFloat($(this).find("OrderAmount").text())) - parseFloat($(this).find("PaidAmount").text()))
                }
                else {
                    FinalTotalBalanceAmount = ((parseFloat(FinalTotalBalanceAmount) + parseFloat($(this).find("OrderAmount").text())) - parseFloat($(this).find("PaidAmount").text()));
                }
            })

            $("#lblOpenBalance").text(FinalTotalBalanceAmount.toFixed(2));

        } else {
           // $("#EmptyTable").show();
            $("#lblOpenBalance").text(FinalTotalBalanceAmount.toFixed(2));
        }


        var pager = $(xmldoc).find("Table1");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
    }
}

function SearchReport() {
    $("#ddlCustomer").closest('div').find('.select2-selection--single').removeAttr('style');
    if ($("#ddlCustomer").val() == "-1") {

        $("#ddlCustomer").closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        toastr.error('Please choose customer', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' })

    }
    else {
        BindReport(1);
    }
}

function CustomerChange() {
    $(".Pager").hide();
    $("#tblCustomerStatementReport tbody tr").remove();
    $("#lblOpenBalance").html('0.00');

}

function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var CustomerList = 0;
    $("#ddlCustomer option:selected").each(function (i) {
        if (i == 0) {
            CustomerList = $(this).val() + ',';
        } else {
            CustomerList += $(this).val() + ',';
        }
    });
    var data = {
        CustomerList: CustomerList,
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: 1,
        PageSize: 0
    };
    $.ajax({
        type: "POST",
        url: "customerStatementReport.aspx/BindCustomerStatementReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var OrderTotal = $(xmldoc).find("Table");
                    var PrintDate = $(xmldoc).find("Table1");
                    var BalanceAmount = $(xmldoc).find("Table3");
                    var FinalOrderTotal = $(xmldoc).find("Table2");
                    var ForwardAmount = 0.00;
                   // $("#PrintTable thead tr").remove();
                    $("#PrintTable tbody tr").remove();
                    var TotalBalanceAmount = 0.00, FinalTotalBalanceAmount = 0.00, NoOfOrders = 0.;
                    var row = $("#PrintTable thead tr:last-child").clone(true);
                    if (OrderTotal.length > 0) {
                        if ($(BalanceAmount).find("BalanceForwardAmount").text() != '') {
                            ForwardAmount = $(BalanceAmount).find("BalanceForwardAmount").text();
                            $("#P_lblBalanceForwardAmount").text(parseFloat($(BalanceAmount).find("BalanceForwardAmount").text()).toFixed(2));
                        }
                        $.each(OrderTotal, function (index) {
                            $(".P_TransactionDate", row).text($(this).find("TransactionDate").text());
                            $(".P_TransactionType", row).text($(this).find("TransactionType").text());
                            $(".P_TotalOrderAmount", row).text($(this).find("OrderAmount").text());
                            $(".P_TotalPaidAmount", row).text($(this).find("PaidAmount").text());
                            if (index == 0) {
                                TotalBalanceAmount = ((parseFloat(ForwardAmount) + parseFloat(TotalBalanceAmount) + parseFloat($(this).find("OrderAmount").text())) - parseFloat($(this).find("PaidAmount").text()))
                            }
                            else {
                                TotalBalanceAmount = ((parseFloat(TotalBalanceAmount) + parseFloat($(this).find("OrderAmount").text())) - parseFloat($(this).find("PaidAmount").text()));
                            }

                            $(".P_TotalBalanceAmount", row).text(TotalBalanceAmount.toFixed(2));
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                        $.each(FinalOrderTotal, function (index) {
                            if (index == 0) {
                                FinalTotalBalanceAmount = ((parseFloat(ForwardAmount) + parseFloat(FinalTotalBalanceAmount) + parseFloat($(this).find("OrderAmount").text())) - parseFloat($(this).find("PaidAmount").text()))
                            }
                            else {
                                FinalTotalBalanceAmount = ((parseFloat(FinalTotalBalanceAmount) + parseFloat($(this).find("OrderAmount").text())) - parseFloat($(this).find("PaidAmount").text()));
                            }
                        })

                        $("#P_lblOpenBalance").text(FinalTotalBalanceAmount.toFixed(2));

                    }
                    else {
                        $("#P_lblOpenBalance").text(FinalTotalBalanceAmount.toFixed(2));
                    }                   
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body style="overflow:hidden;overflow-y:scroll;">');
                        $("#PrintDate").text("Print Date : " + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);                    
                        if ($("#txtSFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);

                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "CustomerStatement",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblCustomerStatementReport tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblCustomerStatementReport tbody tr').length > 0) {
    CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}



