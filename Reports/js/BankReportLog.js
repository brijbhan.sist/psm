﻿$(document).ready(function () {
    $('#txtFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtFromDate").val(month + '/' + day + '/' + year);
    $("#txtToDate").val(month + '/' + day + '/' + year);
    
});
function Pagevalue(e) {
    PaymentDailyReport(parseInt($(e).attr("page")));
}

function PaymentDailyReport(PageIndex) {
    var data = {
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "BankReportLog.aspx/GetReportPaymentDailyReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: SuccessCustomerCreditReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function SuccessCustomerCreditReport(response) {
    if (response.d == 'false') {
    }
    else {
        var xmldoc = $.parseXML(response.d);
        var Report = $(xmldoc).find("Table");
        var OverallTotal = $(xmldoc).find("Table2");
        var TotalCust = 0, TotalOrder = 0, TotalCash = 0, Totalcheck = 0; TotalMoneyorder = 0; TotalCredit = 0; TotalApplyCredit = 0;
        var TotalStoredCredit = 0; GrandTotal = 0; Totalshort = 0; TotalExpense = 0; TotalDepositAmt = 0, TotalElectronicTransfer = 0;
        $("#tblReportPaymentDailyReport tbody tr").remove();
        var row = $("#tblReportPaymentDailyReport thead tr").clone(true);
        if (Report.length > 0) {
            $("#EmptyTable").hide();
            $.each(Report, function () {
                $(".Action", row).html("<a href='#'class='ft-printer' onclick='PrintOrder(\"" + $(this).find("AutoId").text() + "\")'></a>&nbsp;&nbsp;<a href='#' onclick='ViewLog(\"" + $(this).find("AutoId").text() + "\")' class='ft-eye'></a>")
                $(".Date", row).text($(this).find("PaymentDate").text());
                $(".TotalNoOfCustomer", row).text($(this).find("TotalNoOfCustomer").text());
                $(".TotalNoOfOrder", row).text($(this).find("noOfOrder").text());
                $(".TotalCash", row).text(parseFloat($(this).find("TotalCash").text()).toFixed(2));
                $(".TotalCheck", row).text(parseFloat($(this).find("TotalCheck").text()).toFixed(2));
                $(".TotalMoneyOrder", row).text(parseFloat($(this).find("TotalMoneyOrder").text()).toFixed(2));
                $(".TotalCreditCard", row).text(parseFloat($(this).find("TotalCreditCard").text()).toFixed(2));
                $(".TotalStoreCredit", row).text(parseFloat($(this).find("TotalStoreCredit").text()).toFixed(2));
                $(".CreditAmount", row).text(parseFloat(parseFloat($(this).find("CreditAmount").text())).toFixed(2));
                $(".ElectronicTransfer", row).text(parseFloat(parseFloat($(this).find("TotalElectronicTransfer").text())).toFixed(2));
                $(".TotalPaid", row).text(parseFloat($(this).find("TotalPaid").text()).toFixed(2));
                $(".Short", row).text(parseFloat($(this).find("Short").text()).toFixed(2));
                $(".Expense", row).html(parseFloat($(this).find("Expense").text()).toFixed(2)); 
                $(".Deposite", row).html(parseFloat($(this).find("DepositAmount").text()).toFixed(2));
                $("#tblReportPaymentDailyReport tbody").append(row);
                $(".ElectronicTransfer", row).css('background-color', 'white');
                $(".TotalStoreCredit", row).css('background-color', 'white');
                $(".Short", row).css('background-color', 'white');
                $(".Expense", row).css('background-color', 'white');
                row = $("#tblReportPaymentDailyReport tbody tr:last").clone(true);

                TotalCust += parseFloat($(this).find("TotalNoOfCustomer").text());
                TotalOrder += parseFloat($(this).find("noOfOrder").text());

                TotalCash += parseFloat($(this).find("TotalCash").text());
                Totalcheck += parseFloat($(this).find("TotalCheck").text());
                TotalMoneyorder += parseFloat($(this).find("TotalMoneyOrder").text());
                TotalCredit += parseFloat($(this).find("TotalCreditCard").text());
                TotalApplyCredit += parseFloat($(this).find("TotalStoreCredit").text());
                TotalElectronicTransfer += parseFloat($(this).find("TotalElectronicTransfer").text());
                TotalStoredCredit += parseFloat($(this).find("CreditAmount").text());
                GrandTotal+= parseFloat($(this).find("TotalPaid").text());
                Totalshort += parseFloat($(this).find("Short").text());
                TotalExpense += parseFloat($(this).find("Expense").text());
                TotalDepositAmt += parseFloat($(this).find("DepositAmount").text());
            });
            $("#TotalCustomer").html(TotalCust);
            $("#TotalOrder").html(parseFloat(TotalOrder).toFixed(0));
            $("#Totalcase").html(parseFloat(TotalCash).toFixed(2));
            $("#TotalCheck").html(parseFloat(Totalcheck).toFixed(2));
            $("#TotalMoneyorder").html(parseFloat(TotalMoneyorder).toFixed(2));
            $("#TotalCreditcard").html(parseFloat(TotalCredit).toFixed(2));
            $("#TotalstorecreditApply").html(parseFloat(TotalApplyCredit).toFixed(2));
            $("#TotalstoredCreditGenerated").html(parseFloat(TotalStoredCredit).toFixed(2));
            $("#TotalGrand").html(parseFloat(GrandTotal).toFixed(2));
            $("#TotalShort").html(parseFloat(Totalshort).toFixed(2));
            $("#TotalExpense").html(parseFloat(TotalExpense).toFixed(2));
            $("#TotalDepositAmount").html(parseFloat(TotalDepositAmt).toFixed(2));
            $("#TotalstoredElectricTransfer").html(parseFloat(TotalElectronicTransfer).toFixed(2));
        }
        else {
            $("#TotalCustomer").html('0');
            $("#TotalOrder").html('0');
            $("#Totalcase").html('0.00');
            $("#TotalCheck").html('0.00');
            $("#TotalMoneyorder").html('0.00');
            $("#TotalCreditcard").html('0.00');
            $("#TotalstorecreditApply").html('0.00');
            $("#TotalstoredCreditGenerated").html('0.00');
            $("#TotalGrand").html('0.00');
            $("#TotalShort").html('0.00');
            $("#TotalExpense").html('0.00');
            $("#TotalDepositAmount").html('0.00');
            $("#TotalstoredElectricTransfer").html('0.00');
        }
        if (OverallTotal.length > 0) {
            if ($(OverallTotal).find("TotalNoOfCustomer").text() == "") {
                $("#TotalCustomers").html('0');
                $("#TotalOrdesr").html('0');
                $("#Totalcases").html('0.00');
                $("#TotalChecks").html('0.00');
                $("#TotalMoneyorders").html('0.00');
                $("#TotalCreditcards").html('0.00');

                $("#TotalstorecreditApplys").html('0.00');
                $("#TotalstoredCreditGenerateds").html('0.00');
                $("#TotalstoredElectricTransfers").html('0.00');

                $("#TotalGrands").html('0.00');
                $("#TotalShorts").html('0.00');
                $("#TotalExpenses").html('0.00');
                $("#TotalDepositAmounts").html('0.00');
            }
            else {
                $("#TotalCustomers").html($(OverallTotal).find("TotalNoOfCustomer").text());
                $("#TotalOrdesr").html($(OverallTotal).find("noOfOrder").text());
                $("#Totalcases").html(parseFloat($(OverallTotal).find("TotalCash").text()).toFixed(2));
                $("#TotalChecks").html(parseFloat($(OverallTotal).find("TotalCheck").text()).toFixed(2));
                $("#TotalMoneyorders").html(parseFloat($(OverallTotal).find("TotalMoneyOrder").text()).toFixed(2));
                $("#TotalCreditcards").html(parseFloat($(OverallTotal).find("TotalCreditCard").text()).toFixed(2));
                $("#TotalstorecreditApplys").html(parseFloat($(OverallTotal).find("TotalStoreCredit").text()).toFixed(2));
                $("#TotalstoredCreditGenerateds").html(parseFloat($(OverallTotal).find("CreditAmount").text()).toFixed(2));
                $("#TotalstoredElectricTransfers").html(parseFloat($(OverallTotal).find("TotalElectronicTransfer").text()).toFixed(2));
                $("#TotalGrands").html(parseFloat($(OverallTotal).find("TotalPaid").text()).toFixed(2));
                $("#TotalShorts").html(parseFloat($(OverallTotal).find("Short").text()).toFixed(2));
                $("#TotalExpenses").html(parseFloat($(OverallTotal).find("Expense").text()).toFixed(2));
                $("#TotalDepositAmounts").html(parseFloat($(OverallTotal).find("TotalDepositAmount").text()).toFixed(2));
            }
        }

        var pager = $(xmldoc).find("Table1");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
    }
}

function PrintOrder(ReportAutoId) {
    window.open("/Reports/PaymentLog_BankReportLog.html?dt=" + ReportAutoId, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}
function ViewLog(ReportAutoId) {
    location.href = "/Reports/BankReportLogView.aspx?dt=" + ReportAutoId;
}

function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "BankReportLog.aspx/GetReportPaymentDailyReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',

                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                debugger
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var Report = $(xmldoc).find("Table");
                    var PrintDate = $(xmldoc).find("Table1");
                    var TotalCust = 0, TotalOrder = 0, TotalCash = 0, Totalcheck = 0; TotalMoneyorder = 0; TotalCredit = 0; TotalApplyCredit = 0;
                    var TotalStoredCredit = 0; GrandTotal = 0; Totalshort = 0; TotalExpense = 0; TotalDepositAmt = 0, ElectronicTransfer = 0;
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr:last-child").clone(true);
                    if (Report.length > 0) {
                        $.each(Report, function () {
                            $(".P_Date", row).text($(this).find("PaymentDate").text());
                            $(".P_TotalNoOfCustomer", row).html($(this).find("TotalNoOfCustomer").text());
                            $(".P_TotalNoOfOrder", row).html($(this).find("noOfOrder").text());
                            $(".P_TotalCash", row).html(parseFloat($(this).find("TotalCash").text()).toFixed(2));
                            $(".P_TotalCheck", row).html(parseFloat($(this).find("TotalCheck").text()).toFixed(2));
                            $(".P_TotalMoneyOrder", row).html(parseFloat($(this).find("TotalMoneyOrder").text()).toFixed(2));
                            $(".P_TotalCreditCard", row).html(parseFloat($(this).find("TotalCreditCard").text()).toFixed(2));
                            $(".P_TotalStoreCredit", row).html(parseFloat($(this).find("TotalStoreCredit").text()).toFixed(2));
                           // var total = parseFloat(parseFloat(parseFloat($(this).find("TotalPaid").text()) + parseFloat($(this).find("CreditAmount").text()))).toFixed(2);
                            $(".P_TotalPaid", row).html(parseFloat($(this).find("TotalPaid").text()).toFixed(2));
                            $(".P_Short", row).html(parseFloat($(this).find("Short").text()).toFixed(2));
                            $(".P_Expense", row).html(parseFloat($(this).find("Expense").text()).toFixed(2));
                            $(".P_CreditAmount", row).html(parseFloat($(this).find("CreditAmount").text()).toFixed(2));
                            $(".P_ElectronicTransfer", row).html(parseFloat($(this).find("TotalElectronicTransfer").text()).toFixed(2));
                            //total = parseFloat(total - (parseFloat($(this).find("TotalElectronicTransfer").text()) + parseFloat($(this).find("Short").text()) + parseFloat($(this).find("Expense").text()) + parseFloat($(this).find("TotalStoreCredit").text())));
                            $(".P_Deposite", row).html(parseFloat($(this).find("DepositAmount").text()).toFixed(2));
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);

                            TotalCust += parseFloat($(this).find("TotalNoOfCustomer").text());
                            TotalOrder += parseFloat($(this).find("noOfOrder").text());

                            TotalCash += parseFloat($(this).find("TotalCash").text());
                            Totalcheck += parseFloat($(this).find("TotalCheck").text());
                            TotalMoneyorder += parseFloat($(this).find("TotalMoneyOrder").text());
                            TotalCredit += parseFloat($(this).find("TotalCreditCard").text());
                            TotalApplyCredit += parseFloat($(this).find("TotalStoreCredit").text());
                            TotalStoredCredit += parseFloat($(this).find("CreditAmount").text());
                            //var TotalGrandS = parseFloat(parseFloat(parseFloat($(this).find("TotalPaid").text()) + parseFloat($(this).find("CreditAmount").text())));
                            GrandTotal += parseFloat($(this).find("TotalPaid").text());
                            Totalshort += parseFloat($(this).find("Short").text());
                            TotalExpense += parseFloat($(this).find("Expense").text());
                            ElectronicTransfer += parseFloat($(this).find("TotalElectronicTransfer").text());
                            TotalDepositAmt += parseFloat($(this).find("DepositAmount").text());
                        });
                        $("#OTotalCustomers").html(TotalCust);
                        $("#OTotalOrdesr").html(parseFloat(TotalOrder).toFixed(0));
                        $("#OTotalcases").html(parseFloat(TotalCash).toFixed(2));
                        $("#OTotalChecks").html(parseFloat(Totalcheck).toFixed(2));
                        $("#OTotalMoneyorders").html(parseFloat(TotalMoneyorder).toFixed(2));
                        $("#OTotalCreditcards").html(parseFloat(TotalCredit).toFixed(2));
                        $("#OTotalstorecreditApplys").html(parseFloat(TotalApplyCredit).toFixed(2));
                        $("#OTotalstoredCreditGenerateds").html(parseFloat(TotalStoredCredit).toFixed(2));
                        $("#OTotalGrands").html(parseFloat(GrandTotal).toFixed(2));
                        $("#OTotalShorts").html(parseFloat(Totalshort).toFixed(2));
                        $("#OTotalExpenses").html(parseFloat(TotalExpense).toFixed(2));
                        $("#OTotalDepositAmounts").html(parseFloat(TotalDepositAmt).toFixed(2));
                        $("#OElectronicTransfer").html(parseFloat(ElectronicTransfer).toFixed(2));
                    }
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + PrintDate.find('PrintDate').text());
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtFromDate").val() + " To " + $("#txtToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "BankReportLog" + (new Date()).format("MM/dd/yyyy hh:mm tt"),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblReportPaymentDailyReport tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblReportPaymentDailyReport tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}