﻿
$(document).ready(function () {
    getReport(1);
});


function Pagevalue(e) {
    getReport(parseInt($(e).attr("page")));
}

function getReport(PageIndex) {
     var pobj = new Object();
   
    pobj.PageSize = $("#ddlPageSize").val();
    pobj.PageIndex = PageIndex;

    var data = {
        pobj: pobj
    };
    $.ajax({
        type: "POST",
        url: "UpcomingMigrationReport.aspx/GetReport",
         data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetRouteList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetRouteList(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var List = $(xmldoc).find("Table");

            $("#tblOrderList tbody tr").remove();
            var row = $("#tblOrderList thead tr:last").clone(true);

            if (List.length > 0) {

                $.each(List, function (index) {

                    $(".Action", row).html('<button type="button" class="btn btn-success buttonAnimation round  btn-sm" onclick="MigrateOrder('+$(this).find("orderAutoid").text()+')">Migrate</button>');
                    $(".OrderNo", row).html($(this).find("OrderNo").text());
                    $(".OrderDate", row).html($(this).find("OrderDate").text());
                    $(".NoPayment ", row).html($(this).find("NoofPayment").text());
                    $(".LastPaymentDate ", row).html($(this).find("settledDate").text());
                    $(".PayableAmount ", row).html($(this).find("PayableAmount").text());
                    $(".SettledAmount ", row).html($(this).find("SettledAmt").text());
                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);

                });
                $("#EmptyTable").hide();

            } else {
                $("#EmptyTable").show();
            }
            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });

        }
    }
    else {
        location.href = "/";
    }
}

function MigrateOrder(OrderAutoId) {
        swal({
        title: "Are you sure?",
        text: "You want to Migrate this order.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                icon: "warning",
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes.",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
           Migration(OrderAutoId);
        }
    });
}

function Migration(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "UpcomingMigrationReport.aspx/Migration",
       data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
          
            if (response.d == 'true') {
                swal('', 'Migration is completed successfully.', 'success');
                getReport(1);
            }
            else {
                swal('', 'Opps something went wrong.', 'error');
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var pobj = new Object();
    pobj.ProductId = $("#txtProductId").val().trim();
    pobj.ProductName = $("#txtProductName").val().trim();
    pobj.Month = $("#ddlMonth").val();
    pobj.PageSize = 0;
    pobj.PageIndex = 1;

    var data = {
        pobj: pobj
    };
    $.ajax({
        type: "POST",
        url: "UpcomingMigrationReport.aspx/Getreport",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var PrintDate = $(xmldoc).find("Table1");
                    var List = $(xmldoc).find("Table");

                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (List.length > 0) {

                        $("#RptTable").empty();

                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Non Carry Products Report</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + $(PrintDate).find("PrintDate").text() + "</span></div>"
                        row1 += "<table id='RptTable' class='PrintMyTableHeader MyTableHeader'>"
                        row1 += "<thead>"
                        row1 += "<tr>"
                        row1 += "<td style='text-align:center !important;font-weight:bold'>Product Id</td>"
                        row1 += "<td style='text-align:center !important;font-weight:bold'>Product Name</td>"
                        row1 += "<td style='text-align:center !important;font-weight:bold'>Last Order Date</td>"
                        row1 += "</tr>"
                        row1 += "</thead>"
                        row1 += "<tbody>"

                        if (List.length > 0) {
                            $.each(List, function (index) {
                                row1 += "<tr>";
                                row1 += "<td style='text-align:center !important'>" + $(this).find("ProductId").text() + "</td>";
                                row1 += "<td style='text-align:left !important'>" + $(this).find("ProductName").text() + "</td>";
                                row1 += "<td style='text-align:center !important'>" + $(this).find("lastdate").text() + "</td>";
                                row1 += "</tr>";
                            });
                            row1 += "</tbody>"

                            row1 += "</table>"

                        }
                    }

                        if (us == 1) {
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style></style>');
                            mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                            mywindow.document.write('</head><body>');
                            mywindow.document.write(row1);
                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);

                        }
                        if (us == 2) {
                            $("#ExcelDiv").append(row1);
                            $("#RptTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "UpcomingMigrationReport",
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });

                        }
                    }
                }
                else {
                    location.href = "/";
                }
            }

        });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}



