﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    BindCustomer();
    BindProductSubCategory();
    BindProduct();

});

function Pagevalue(e) {
    getReport(parseInt($(e).attr("page")));
};

/*---------------------------------------------------Bind Customer-----------------------------------------------------------*/
function BindCustomer() {
    $.ajax({
        type: "POST",
        url: "ProductWiseReceiveReport.aspx/BindCustomer",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {

                    var getData = $.parseJSON(response.d);
                    $("#ddlAllCategory option:not(:first)").remove();
                    $.each(getData[0].Category, function (index, item) {
                        $("#ddlAllCategory").append("<option value='" + item.CID + "'>" + item.CN+"</option>");
                    });
                    $("#ddlAllCategory").select2();


                    $("#ddlAllCustomer option:not(:first)").remove();
                    $.each(getData[0].Vendor, function (index, item) {
                        $("#ddlAllCustomer").append("<option value='" + item.VID + "'>" + item.VN + "</option>");
                    });
                    $("#ddlAllCustomer").select2();

                   
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}




/*---------------------------------------------------Bind Sub Category-----------------------------------------------------------*/
function BindProductSubCategory() {
    $.ajax({
        type: "POST",
        url: "ProductWiseReceiveReport.aspx/BindProductSubCategory",
        data: "{'CategoryAutoId':'" + $("#ddlAllCategory").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        cashe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);
                    $("#AllSubCategory option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#AllSubCategory").append("<option value='" + getData[index].SCID + "'>" + getData[index].SCN + "</option>");
                    });
                    $("#AllSubCategory").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function BindProduct() {
    $.ajax({
        type: "POST",
        url: "ProductWiseReceiveReport.aspx/BindProduct",
        data: "{'SubCategoryId':'" + $("#AllSubCategory").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlProduct option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlProduct").append("<option value='" + getData[index].PID + "'>" + getData[index].PN + "</option>");
                    });
                    $("#ddlProduct").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}


/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function getReport(PageIndex) {

    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        BillNo: $("#txtSBillNo").val(),
        CustomerTypeAutoId: 0,
        SalesAutoId: 0,
        CustomerAutoId: $("#ddlAllCustomer").val(),
        CategoryAutoId: $("#ddlAllCategory").val(),
        SubCategoryAutoId: $("#AllSubCategory").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: 0,
        PageIndex: PageIndex,
        PageSize: $('#ddlPageSize').val()
    };
    $.ajax({
        type: "POST",
        url: "ProductWiseReceiveReport.aspx/GetReportDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successgetReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successgetReport(response) {
    if (response.d != "Session Expired") {
        if (response.d == 'false') {
        } else {
            var xmldoc = $.parseXML(response.d);
            var ReportDetails = $(xmldoc).find("Table");
            var OTotal = $(xmldoc).find("Table2");
            $("#tblOrderList tbody tr").remove();
            var row = $("#tblOrderList thead tr").clone(true);
            var TotalPieceQty = 0, TotalBoxQty = 0, TotalCaseQty = 0, TotalAmount = 0.00;
            if (ReportDetails.length > 0) {
                $("#EmptyTable").hide();
                var html = "", TotalPiece = 0, TotalCase = 0, TotalBox = 0, ItemQty = 0, ProductTotal = 0.00, ProductId = 0, CustomerAutoId = 0;
                debugger
                $.each(ReportDetails, function (index) {
                    ItemQty++;
                    if (ProductId != '0' && (ProductId != $(this).find("ProductId").text() || CustomerAutoId != $(this).find("CustomerAutoId").text())) {
                        html = "<tr style='font - weight: 700; background: oldlace;'><td colspan='5' style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>Total</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalPiece + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalBox + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalCase + "</b></td><td style='text-align: center;color:#dd0b3c;background:#faf2f2;'>-</td><td style='text-align: center;color:#dd0b3c;background:#faf2f2;'>-</td><td style='text-align: center;color:#dd0b3c;background:#faf2f2;'>-</td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + ProductTotal.toFixed(2) + "</b></td></tr>";
                        $("#tblOrderList tbody").append(html);
                        TotalPiece = 0, TotalCase = 0; TotalBox = 0, ProductTotal = 0.00, ItemQty = 0;
                    }
                    $(".PricePerPiece", row).text($(this).find("PricePerPiece").text());
                    $(".BillDate", row).text($(this).find("BillDate").text());
                    $(".ProductId", row).text($(this).find("ProductId").text());
                    $(".ProductName", row).text($(this).find("ProductName").text());
                    $(".BillNo", row).text($(this).find("BillNo").text());
                    $(".VendorName", row).text($(this).find("VendorName").text());
                    $(".UnitPrice", row).text($(this).find("UnitPrice").text());
                    $(".SellingPrice", row).text($(this).find("sellingprice").text());
                    $(".TotalPieces", row).html($(this).find("TotalPiece").text());
                    $(".TotalBox", row).html($(this).find("TotalBox").text());
                    $(".TotalCase", row).html($(this).find("TotalCase").text());
                    TotalPieceQty = TotalPieceQty + parseInt($(this).find("TotalPiece").text());
                    TotalBoxQty = TotalBoxQty + parseInt($(this).find("TotalBox").text());
                    TotalCaseQty = TotalCaseQty + parseInt($(this).find("TotalCase").text());
                    TotalAmount = TotalAmount + parseFloat($(this).find("PayableAmount").text());
                    TotalPiece = TotalPiece + parseInt($(this).find("TotalPiece").text());
                    TotalBox = TotalBox + parseInt($(this).find("TotalBox").text());
                    TotalCase = TotalCase + parseInt($(this).find("TotalCase").text());
                    ProductTotal = ProductTotal + parseFloat($(this).find("PayableAmount").text());
                    $(".PayableAmount", row).html($(this).find("PayableAmount").text());
                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);
                    ProductId = $(this).find("ProductId").text();
                    CustomerAutoId = $(this).find("CustomerAutoId").text();
                    ItemQty = $(this).find("ItemQty").text();
                });
                html = "<tr style='font - weight: 700; background: oldlace;'><td colspan='5' style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>Total</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalPiece + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalBox + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalCase + "</b></td><td style='text-align: center;color:#dd0b3c;background:#faf2f2;'>-</td><td style='text-align: center;color:#dd0b3c;background:#faf2f2;'>-</td><td style='text-align: center;color:#dd0b3c;background:#faf2f2;'>-</td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + ProductTotal.toFixed(2) + "</b></td></tr>";
                $("#tblOrderList tbody").append(html);

                $('#TotalPieceQty').html(parseFloat(TotalPieceQty).toFixed(0));
                $('#TotalBoxQty').html(parseFloat(TotalBoxQty).toFixed(0));
                $('#TotalCaseQty').html(parseFloat(TotalCaseQty).toFixed(0));
                $('#AmtDue').html(parseFloat(TotalAmount).toFixed(2));
                if (TotalPieceQty > 0) {
                    $('#TotalPieceAVG').html(parseFloat(TotalAmount / TotalPieceQty).toFixed(2));
                } else {
                    $('#TotalPieceAVG').html('0.00');
                }
                if (TotalBoxQty > 0) {
                    $('#TotalBoxAVG').html(parseFloat(TotalAmount / TotalBoxQty).toFixed(2));
                } else {
                    $('#TotalBoxAVG').html('0.00');
                }
                if (TotalCaseQty > 0) {
                    $('#TotalCaseAVG').html(parseFloat(TotalAmount / TotalCaseQty).toFixed(2));
                } else {
                    $('#TotalCaseAVG').html('0.00');
                }

            } else {
                $('#TotalQty').html('0');
                $('#AmtDue').html('0.00');
               // $("#EmptyTable").show();
                $('#TotalCaseAVG').html('0.00');
                $('#TotalBoxAVG').html('0.00');
                $('#TotalPieceAVG').html('0.00');
                $('#TotalPieceQty').html('0');
                $('#TotalBoxQty').html('0');
                $('#TotalCaseQty').html('0');
                $('#AmtDue').html('0');
            }
            $(OTotal).each(function () {
                $('#TotalPieceQtys').html(parseFloat($(this).find("TotalPiece").text()).toFixed(0));
                $('#TotalBoxQtys').html(parseFloat($(this).find("TotalBox").text()).toFixed(0));
                $('#TotalCaseQtys').html(parseFloat($(this).find("TotalCase").text()).toFixed(0));
                $('#AmtDues').html(parseFloat($(this).find("PayableAmount").text()).toFixed(2));
            });

            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}



function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        BillNo: $("#txtSBillNo").val(),
        CustomerTypeAutoId: 0,
        SalesAutoId: 0,
        CustomerAutoId: $("#ddlAllCustomer").val(),
        CategoryAutoId: $("#ddlAllCategory").val(),
        SubCategoryAutoId: $("#AllSubCategory").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: 0,
        PageIndex: 1,
        PageSize: 0
    };
    $.ajax({
        type: "POST",
        url: "ProductWiseReceiveReport.aspx/GetReportDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ReportDetails = $(xmldoc).find("Table");
                    var PrintDate = $(xmldoc).find("Table1");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    var TotalPieceQty = 0, TotalBoxQty = 0, TotalCaseQty = 0, TotalAmount = 0.00;
                    if (ReportDetails.length > 0) {
                        var html = "", TotalPiece = 0, TotalCase = 0, TotalBox = 0, ItemQty = 0, ProductTotal = 0.00, ProductId = 0, CustomerAutoId = 0;
                        $.each(ReportDetails, function () {
                            ItemQty++;
                            if (ProductId != '0' && (ProductId != $(this).find("ProductId").text() || CustomerAutoId != $(this).find("CustomerAutoId").text())) {
                                html = "<tr style='font-weight:bold;' background:oldlace;'><td colspan='5' style='text-align:right;color:#dd0b3c;background:#faf2f2;'>Total</td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'>" + TotalPiece + "</td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'>" + TotalBox + "</td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'>" + TotalCase + "</td><td style='text-align: center;color:#dd0b3c;background:#faf2f2;'>-</td><td style='text-align: center;color:#dd0b3c;background:#faf2f2;'>-</td><td style='text-align: center;color:#dd0b3c;background:#faf2f2;'>-</td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + ProductTotal.toFixed(2) + "</b></td></tr>";
                                $("#PrintTable tbody").append(html);
                                TotalPiece = 0, TotalCase = 0; TotalBox = 0, ProductTotal = 0.00, ItemQty = 0;
                            }
                            $(".P_PricePerPiece", row).text($(this).find("PricePerPiece").text());
                            $(".P_BillDate", row).text($(this).find("BillDate").text());
                            $(".P_ProductId", row).text($(this).find("ProductId").text());
                            $(".P_ProductName", row).text($(this).find("ProductName").text());
                            $(".P_BillNo", row).text($(this).find("BillNo").text());
                            $(".P_VendorName", row).text($(this).find("VendorName").text());
                            $(".P_UnitPrice", row).text($(this).find("UnitPrice").text());
                            $(".P_SellingPrice", row).text($(this).find("sellingprice").text());
                            $(".P_TotalPieces", row).html($(this).find("TotalPiece").text());
                            $(".P_TotalBox", row).html($(this).find("TotalBox").text());
                            $(".P_TotalCase", row).html($(this).find("TotalCase").text());
                            TotalPieceQty = TotalPieceQty + parseInt($(this).find("TotalPiece").text());
                            TotalBoxQty = TotalBoxQty + parseInt($(this).find("TotalBox").text());
                            TotalCaseQty = TotalCaseQty + parseInt($(this).find("TotalCase").text());
                            TotalAmount = TotalAmount + parseFloat($(this).find("PayableAmount").text());
                            TotalPiece = TotalPiece + parseInt($(this).find("TotalPiece").text());
                            TotalBox = TotalBox + parseInt($(this).find("TotalBox").text());
                            TotalCase = TotalCase + parseInt($(this).find("TotalCase").text());
                            ProductTotal = ProductTotal + parseFloat($(this).find("PayableAmount").text());
                            $(".P_PayableAmount", row).html($(this).find("PayableAmount").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                            ProductId = $(this).find("ProductId").text();
                            CustomerAutoId = $(this).find("CustomerAutoId").text();
                            ItemQty = $(this).find("ItemQty").text();
                            
                        });
                        html = "<tr style='font-weight:bold;' background:oldlace;'><td colspan='5' style='text-align:right;color:#dd0b3c;background:#faf2f2;'>Total</td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'>" + TotalPiece + "</td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'>" + TotalBox + "</td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'>" + TotalCase + "</td><td style='text-align: center;color:#dd0b3c;background:#faf2f2;'>-</td><td style='text-align: center;color:#dd0b3c;background:#faf2f2;'>-</td><td style='text-align: center;color:#dd0b3c;background:#faf2f2;'>-</td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + ProductTotal.toFixed(2) + "</b></td></tr>";
                        $("#PrintTable tbody").append(html);

                        $('#P_TotalPieceQty').html(parseFloat(TotalPieceQty).toFixed(0));
                        $('#P_TotalBoxQty').html(parseFloat(TotalBoxQty).toFixed(0));
                        $('#P_TotalCaseQty').html(parseFloat(TotalCaseQty).toFixed(0));
                        $('#P_AmtDue').html(parseFloat(TotalAmount).toFixed(2));
                        if (TotalPieceQty > 0) {
                            $('#P_TotalPieceAVG').html(parseFloat(TotalAmount / TotalPieceQty).toFixed(2));
                        } else {
                            $('#P_TotalPieceAVG').html('0.00');
                        }
                        if (TotalBoxQty > 0) {
                            $('#P_TotalBoxAVG').html(parseFloat(TotalAmount / TotalBoxQty).toFixed(2));
                        } else {
                            $('#P_TotalBoxAVG').html('0.00');
                        }
                        if (TotalCaseQty > 0) {
                            $('#P_TotalCaseAVG').html(parseFloat(TotalAmount / TotalCaseQty).toFixed(2));
                        } else {
                            $('#P_TotalCaseAVG').html('0.00');
                        }
                    }
                    if (us == 1) {
                       
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtSFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                       

                        mywindow.document.write('</body></html>');
                        try {
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        } catch (e) {
                            setTimeout(function () {
                                mywindow.print();
                            }, 10000);
                        }
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "InventoryReportByProductReceive",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}



