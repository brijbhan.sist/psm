﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtDateFrom").val(month + '/' + day + '/' + year);
    $("#txtDateTo").val(month + '/' + day + '/' + year);
    bindDropDown();
});
function bindDropDown() {
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/WItemWiseReport.asmx/bindDropDown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) { 
            if (response.d != "Session Expired") { 
               
                $("#ddlProduct option:not(:first)").remove();
                $.each(response.d[0].PList, function (i,item) {
                    $("#ddlProduct").append("<option value='" + item.AutoId + "'>" + item.PName  + "</option>");
                });
                $("#ddlProduct").select2()


                $("#ddlStatus option:not(:first)").remove();
                $.each(response.d[0].STList, function (i, item) {
                    $("#ddlStatus").append("<option value='" + item.AutoId + "'>" + item.StatusType  + "</option>");
                });
                $("#ddlStatus").select2()
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
$("#btnSearch").click(function () {
    ProductWiseSalesStockReport(1);
})
function Pagevalue(e) {
    ProductWiseSalesStockReport(parseInt($(e).attr("page")));
}
function ProductWiseSalesStockReport(PageIndex) {
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        Status: $("#ddlStatus").val(),
        FromDate: $("#txtDateFrom").val(),
        ToDate: $("#txtDateTo").val(),
        PageIndex: PageIndex,
        PageSize: $("#ddlPageSize").val()
    };
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/WItemWiseReport.asmx/bindPrductSalesStockReport",
        data: "{ 'dataValues':'" + JSON.stringify(data) + "' }",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var ProductDetail = $(xmldoc).find("Table1");
                var OrderTotal = $(xmldoc).find("Table2");
                $("#tblProductSalesStock tbody tr").remove();
                if (ProductDetail.length > 0) {
                    $("#EmptyTable").hide();
                    var TotalPiece = 0; TotalBox = 0; TotalCase = 0; TotalAvlQty = 0;
                    var row = $("#tblProductSalesStock thead tr").clone(true);
                    $.each(ProductDetail, function () {
                        $(".ProductId", row).text($(this).find("ProductId").text());
                        $(".ProductName", row).text($(this).find("ProductName").text());
                        $(".Peice", row).text($(this).find("Peice").text());
                        $(".Box", row).text($(this).find("Box").text());
                        $(".Case", row).text($(this).find("Case").text());
                        $(".AvailableQty", row).text($(this).find("AvailableQty").text());
                        $("#tblProductSalesStock tbody").append(row);
                        row = $("#tblProductSalesStock tbody tr:last").clone(true);

                        TotalPiece = TotalPiece + parseInt($(this).find("Peice").text());
                        TotalBox = TotalBox + parseInt($(this).find("Box").text());
                        TotalCase = TotalCase + parseInt($(this).find("Case").text());
                        TotalAvlQty = TotalAvlQty + parseInt($(this).find("AvailableQty").text());
                    });

                    $('#Totalpeice').html(parseFloat(TotalPiece));
                    $('#TotalBox').html(parseFloat(TotalBox));
                    $('#Totalcase').html(parseFloat(TotalCase));
                    $('#TotalAvlQty').html(parseFloat(TotalAvlQty));
                } else {
                    //  $("#EmptyTable").show();
                    $('#Totalpeice').html('0');
                    $('#TotalBox').html('0');
                    $('#Totalcase').html('0');
                    $('#TotalAvlQty').html('0');
                    $('#Totalpeices').html('0');
                    $('#TotalBoxs').html('0');
                    $('#Totalcases').html('0');
                    $('#TotalAvlQtys').html('0');
                }
                $(OrderTotal).each(function () {
                    $('#Totalpeices').html(parseFloat($(this).find('Peice').text()));
                    $('#TotalBoxs').html(parseFloat($(this).find('Box').text()));
                    $('#Totalcases').html(parseFloat($(this).find('Case').text()));
                    $('#TotalAvlQtys').html(parseFloat($(this).find('AvailableQty').text()));
                });
                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
                if ($("#ddlPageSize").val() == '0') {
                    $(".Pager").hide();
                }
                else {
                    $(".Pager").show();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function CreateTable(us) {
    var image = $("#imgName").val();
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        Status: $("#ddlStatus").val(),
        FromDate: $("#txtDateFrom").val(),
        ToDate: $("#txtDateTo").val(),
        PageIndex: 1,
        PageSize: 0
    };
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/WItemWiseReport.asmx/bindPrductSalesStockReport",
        data: "{ 'dataValues':'" + JSON.stringify(data) + "' }",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ProductDetail = $(xmldoc).find("Table1");
                    var OrderTotal = $(xmldoc).find("Table2");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (ProductDetail.length > 0) {
                        $.each(ProductDetail, function () {
                            $(".P_ProductId", row).text($(this).find("ProductId").text());
                            $(".P_ProductName", row).text($(this).find("ProductName").text());
                            $(".P_Peice", row).text($(this).find("Peice").text());
                            $(".P_Box", row).text($(this).find("Box").text());
                            $(".P_Case", row).text($(this).find("Case").text());
                            $(".P_AvailableQty", row).text($(this).find("AvailableQty").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                    }
                    $(OrderTotal).each(function () {
                        $('#Totalpeicess').html(parseFloat($(this).find('Peice').text()));
                        $('#TotalBoxss').html(parseFloat($(this).find('Box').text()));
                        $('#Totalcasess').html(parseFloat($(this).find('Case').text()));
                        $('#TotalAvlQtyss').html(parseFloat($(this).find('AvailableQty').text()));
                    });
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + (new Date()).format("MM/dd/yyyy hh:mm tt"));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtDateFrom").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtDateFrom").val() + " To " + $("#txtDateTo").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html()); 

                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "ByPickProduct",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblProductSalesStock tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
$("#btnExport").click(function () {
    if ($('#tblProductSalesStock tbody tr').length > 0) {
    CreateTable(2);
}
else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
}
});
