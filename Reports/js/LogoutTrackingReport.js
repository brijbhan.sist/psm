﻿$(document).ready(function () {
    $('#txtFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtFromDate").val(month + '/' + day + '/' + year);
    $("#txtToDate").val(month + '/' + day + '/' + year);

});

/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function LogtrackingReport(PageIndex) {
    var data = {

        UserName: $.trim($("#txtUsername").val()),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageSize: $("#PageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "LogoutTrackingReport.aspx/GetLogoutTrackingReport",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successLogtrackingReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function successLogtrackingReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var Logtracking = $(xmldoc).find("Table1");           
            $("#tblLogtrackingReport tbody tr").remove();

            var row = $("#tblLogtrackingReport thead tr").clone(true);
            if (Logtracking.length > 0) {
                var i = 1;
                $("#EmptyTable").hide();
                $.each(Logtracking, function () {

                    $(".SN", row).text(i);
                    $(".userName", row).text($(this).find("userName").text());
                    $(".accessToken", row).text($(this).find("accessToken").text());
                    $(".OrderNo", row).text($(this).find("OrderNo").text());
                    $(".appversion", row).text($(this).find("appversion").text());
                    $(".deviceId", row).text($(this).find("deviceId").text());
                    $(".LogoutTime", row).html($(this).find("Logouttimestamp").text());
                    $(".Created", row).html($(this).find("CreatedDatetime").text());
                    $("#tblLogtrackingReport tbody").append(row);
                    row = $("#tblLogtrackingReport tbody tr:last").clone(true);
                    i++;
                });
            }
            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "page",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}
function Pagevalue(e) {
    LogtrackingReport(parseInt($(e).attr("page")));
};

