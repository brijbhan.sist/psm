﻿$(document).ready(function () {
    BindCustomer();
    $('#txtDateFrom').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtDateTo').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtDateFrom").val(month + '/' + day + '/' + year);
    $("#txtDateTo").val(month + '/' + day + '/' + year);
});
function BindCustomer() {

    $.ajax({
        type: "POST",
        url: "OrderSubmitTimeReport.aspx/CutomerName",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);

            $("#ddlCustomerName option:not(:first)").remove();
            $.each(getData, function (index, item) {
                $("#ddlCustomerName").append("<option value='" + getData[index].SID + "'>" + getData[index].SP + "</option>");
            });

            $("#ddlCustomerName").select2()

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Pagevalue(e) {
    OrderSubmitReport(parseInt($(e).attr("page")));
}
$("#ddlPageSize").change(function () {
    OrderSubmitReport(1);
})
$("#btnSearch").click(function () {
    OrderSubmitReport(1);
});
function OrderSubmitReport(PageIndex) {
    var data = {
        DateFrom: $("#txtDateFrom").val(),
        DateTo: $("#txtDateTo").val(),       
        PackerAutoId: $("#ddlCustomerName").val(),
        RouteStatus: $("#ddlRouteStatus").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "OrderSubmitTimeReport.aspx/GetOrderSubmitReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: SuccessResport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function SuccessResport(response) {
    var xmldoc = $.parseXML(response.d);

    var ResportDetail = $(xmldoc).find("Table1");
    var OrderTotal = $(xmldoc).find("Table2");
    $("#tblOrderSubmitTimeReport tbody tr").remove();
    var row = $("#tblOrderSubmitTimeReport thead tr").clone(true);
    var TotalAmount = 0.00;
    if (ResportDetail.length > 0) {
        $("#EmptyTable").hide();
        $("#tblOrderSubmitTimeReport thead tr").show();
        $.each(ResportDetail, function (index) {

            if ($(this).find("RouteStatus").text() == 'ON') {

                row.css('background', '#91e7b3');
            }
            else {
                row.css('background', '#ffafaf');
            }
            $(".OrderDate", row).text($(this).find("OrderDate").text());
            $(".CustomerName", row).text($(this).find("CustomerName").text());
            $(".OrderNo", row).text($(this).find("OrderNo").text());
            $(".GrandTotal", row).text($(this).find("GrandTotal").text());
            $(".RouteStatus", row).text($(this).find("RouteStatus").text());
            $(".SalePerson", row).text($(this).find("SalePerson").text());
            TotalAmount = TotalAmount + parseInt($(this).find("GrandTotal").text());
            //$(".status", row).text($(this).find("StatusCode").text());

            if (Number($(this).find("StatusCode").text()) == 1) {
                $(".status", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("Status").text() + "</span>");
            } else if (Number($(this).find("StatusCode").text()) == 2) {
                $(".status", row).html("<span class='badge badge badge-pill badge-primary'>" + $(this).find("Status").text() + "</span>");
            } else if (Number($(this).find("StatusCode").text()) == 3) {
                $(".status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            } else if (Number($(this).find("StatusCode").text()) == 4) {
                $(".status", row).html("<span class='badge badge badge-pill badge-secondary'>" + $(this).find("Status").text() + "</span>");
            } else if (Number($(this).find("StatusCode").text()) == 5) {
                $(".status", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("Status").text() + "</span>");
            } else if (Number($(this).find("StatusCode").text()) == 6) {
                $(".status", row).html("<span class='badge badge badge-pill' style='background-color:#8FBC8F'>" + $(this).find("Status").text() + "</span>");
            } else if (Number($(this).find("StatusCode").text()) == 7) {
                $(".status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
            } else if (Number($(this).find("StatusCode").text()) == 8) {
                $(".status", row).html("<span class='badge badge badge-pill badge-green'>" + $(this).find("Status").text() + "</span>");
            }
            else if (Number($(this).find("StatusCode").text()) == 9) {
                $(".status", row).html("<span class='badge badge badge-pill' style='background-color:green'>" + $(this).find("Status").text() + "</span>");
            }
            else if (Number($(this).find("StatusCode").text()) == 10) {
                $(".status", row).html("<span class='badge badge badge-pill' style='background-color:#61b960'>" + $(this).find("Status").text() + "</span>");
            }
            else if (Number($(this).find("StatusCode").text()) == 11) {
                $(".status", row).html("<span class='badge badge badge-pill' style='background-color:#690878'>" + $(this).find("Status").text() + "</span>");
            }

            if ($(this).find("Status").text() != 'New' || Number($("#hiddenEmpType").val()) > 2) {
                if (Number($("#hiddenEmpType").val()) == 3 && (Number($(this).find("StatusCode").text()) <= 2)) {
                    $(".action", row).html("<b><a href='/Sales/orderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'>View</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:;' onclick='print_NewOrder(this)'>Print</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'>Log</a></b>");
                } else {
                    //alert($("#hiddenEmpType").val())
                    if (Number($("#hiddenEmpType").val()) == 4 && (Number($(this).find("StatusCode").text()) <= 3)) {
                        $(".action", row).html("<b><a href='/Sales/orderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'>View</a>&nbsp;&nbsp;|<a href='javascript:;'><span class='glyphicon glyphicon-remove' onclick='deleteOrder(\"" + $(this).find("AutoId").text() + "\")'></span><b></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'>Log</a></b>");

                    } else {
                        //if (Number($("#hiddenEmpType").val()) == 1 && (Number($(this).find("StatusCode").text()) == 6)) {
                        //    $(".action", row).html("<b><a href='/Driver/updateDelivery.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'>View</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'>Log</a></b>");
                        //}
                        //else {
                        //    $(".action", row).html("<b><a href='/Sales/orderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'>View</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'>Log</a></b>");
                        //}

                    }
                }
            }
            $('#TotalAmt').html(parseFloat(TotalAmount).toFixed(2));
            $("#tblOrderSubmitTimeReport tbody").append(row);
            row = $("#tblOrderSubmitTimeReport tbody tr:last").clone(true);

        });
        $(OrderTotal).each(function () {
            $('#TotalAmts').html(parseFloat($(this).find('GrandTotal').text()).toFixed(2));
        });

    } else {
        $('#TotalAmt').html('0.00');
        $('#TotalAmts').html('0.00');
    }

    if ($("#ddlPageSize").val() == '0') {
        $(".Pager").hide();
    }
    else {
        $(".Pager").show();
        var pager = $(xmldoc).find("Table");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
    }
}

function CreateTable(us) {
    var image = $("#imgName").val();
    var data = {
        DateFrom: $("#txtDateFrom").val(),
        DateTo: $("#txtDateTo").val(),
        PackerAutoId: $("#ddlCustomerName").val(),
        RouteStatus: $("#ddlRouteStatus").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "OrderSubmitTimeReport.aspx/GetOrderSubmitReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var PrintDate = $(xmldoc).find("Table");
                    var ResportDetail = $(xmldoc).find("Table1");
                    var OrderTotal = $(xmldoc).find("Table2");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr:last-child").clone(true);
                    var TotalAmount = 0.00;
                    if (ResportDetail.length > 0) {
                        $.each(ResportDetail, function () {
                            if ($(this).find("RouteStatus").text() == 'ON') {

                                row.css('background', '#91e7b3');
                            }
                            else {
                                row.css('background', '#ffafaf');
                            }
                            $(".P_OrderDate", row).text($(this).find("OrderDate").text());
                            $(".P_CustomerName", row).text($(this).find("CustomerName").text());
                            $(".P_OrderNo", row).text($(this).find("OrderNo").text());
                            $(".P_GrandTotal", row).text($(this).find("GrandTotal").text());
                            $(".P_RouteStatus", row).text($(this).find("RouteStatus").text());
                            $(".P_SalePerson", row).text($(this).find("SalePerson").text());
                            TotalAmount = TotalAmount + parseInt($(this).find("GrandTotal").text());

                            if (Number($(this).find("StatusCode").text()) == 1) {
                                $(".P_status", row).html("<span>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 2) {
                                $(".P_status", row).html("<span>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 3) {
                                $(".P_status", row).html("<span>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 4) {
                                $(".P_status", row).html("<span>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 5) {
                                $(".P_status", row).html("<span>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 6) {
                                $(".P_status", row).html("<span>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 7) {
                                $(".P_status", row).html("<span>" + $(this).find("Status").text() + "</span>");
                            } else if (Number($(this).find("StatusCode").text()) == 8) {
                                $(".P_status", row).html("<span>" + $(this).find("Status").text() + "</span>");
                            }
                            else if (Number($(this).find("StatusCode").text()) == 9) {
                                $(".P_status", row).html("<span>" + $(this).find("Status").text() + "</span>");
                            }
                            else if (Number($(this).find("StatusCode").text()) == 10) {
                                $(".P_status", row).html("<span>" + $(this).find("Status").text() + "</span>");
                            }
                            else if (Number($(this).find("StatusCode").text()) == 11) {
                                $(".P_status", row).html("<span>" + $(this).find("Status").text() + "</span>");
                            }
                            $('#P_TotalAmt').html(parseFloat(TotalAmount).toFixed(2));
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                    }
                    $(OrderTotal).each(function () {
                        $('#TotalAmts').html(parseFloat($(this).find('GrandTotal').text()).toFixed(2));
                    });
                    if (us == 1) {
                        
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + PrintDate.find('PrintDate').text());
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtDateFrom").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtDateFrom").val() + " To " + $("#txtDateTo").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "OrderSubmitReportByDateAndTime" + (new Date()).format("MM/dd/yyyy hh:mm tt"),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderSubmitTimeReport tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('Please select atleast one Record in this list', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderSubmitTimeReport tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('Please select atleast one Record in this list', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}