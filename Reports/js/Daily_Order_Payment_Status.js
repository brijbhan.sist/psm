﻿$(document).ready(function () {
    $('.date').pickadate({
        min:new Date('01/01/2019'),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtOrderCloseFromDate").val(month + '/' + day + '/' + year);
    $("#txtOrderCloseToDate").val(month + '/' + day + '/' + year);
    BindCustomerDetails();
});

function setdatevalidation(val) {
    var fdate = $("#txtSFromDate").val();
    var tdate = $("#txtSToDate").val();
    var dfdate = $("#txtOrderCloseFromDate").val();
    var dtdate = $("#txtOrderCloseToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromDate").val(tdate);
        }
    }
    else if (val == 3) {
        if (dfdate == '' || new Date(dfdate) > new Date(dtdate)) {
            $("#txtOrderCloseToDate").val(dfdate);
        }
    }
    else if (val == 4) {
        if (dfdate == '' || new Date(dfdate) > new Date(dtdate)) {
            $("#txtOrderCloseFromDate").val(dtdate);
        }
    }
}
function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
};/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function BindReport(index) {
    var data = {
        PageSize: $("#ddlPaging").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        CloseOrderFromDate: $("#txtOrderCloseFromDate").val(),
        CloseOrderToDate: $("#txtOrderCloseToDate").val(),
        SalesPerson: $('#ddlSalesPerson').val(),
        DriverAutoId: $('#ddlDriver').val(),
        OrderStatus: $('#ddlStatus').val(),
        PaymentStatus: $('#ddlPaymentStatus').val(),
        Index: index
    };
    $.ajax({
        type: "POST",
        url: "Daily_Order_Payment_Status.aspx/OrderPaymentStatusReport",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successReadytoShipReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function successReadytoShipReport(response) {
    if (response.d == 'false') {
    } else {
        var xmldoc = $.parseXML(response.d);
        var OrderTotal = $(xmldoc).find("Table");
        var OTotal = $(xmldoc).find("Table2");
        $("#tblOrderList tbody tr").remove();
        var row = $("#tblOrderList thead tr").clone(true);
        var GrandAmount = 0.00, PaidAmount = 0.0, DueAmount = 0.00;
        if (OrderTotal.length > 0) {
            $("#EmptyTable").hide();
            $.each(OrderTotal, function () {
                $(".OrderDate", row).text($(this).find("OrderDate").text());
                $(".OrderNo", row).text($(this).find("OrderNo").text());
                $(".CustomerName", row).text($(this).find("CustomerName").text());

                if ($(this).find("Status").text() == "1") {
                    $(".OrderStatus", row).html("<span class='badge badge badge-pill Status_New'>" + $(this).find("OrderStatus").text() + "</span>");
                }
                else if ($(this).find("Status").text() == "2") {
                    $(".OrderStatus", row).html("<span class='badge badge badge-pill Status_Processed'>" + $(this).find("OrderStatus").text() + "</span>");
                }
                else if ($(this).find("Status").text() == "3") {
                    $(".OrderStatus", row).html("<span class='badge badge badge-pill Status_Packed'>" + $(this).find("OrderStatus").text() + "</span>");
                }
                else if ($(this).find("Status").text() == "4") {
                    $(".OrderStatus", row).html("<span class='badge badge badge-pill Status_Ready_to_Ship'>" + $(this).find("OrderStatus").text() + "</span>");
                }
                else if ($(this).find("Status").text() == "5") {
                    $(".OrderStatus", row).html("<span class='badge badge badge-pill Status_Shipped'>" + $(this).find("OrderStatus").text() + "</span>");
                }
                else if ($(this).find("Status").text() == "6") {
                    $(".OrderStatus", row).html("<span class='badge badge badge-pill Status_Delivered'>" + $(this).find("OrderStatus").text() + "</span>");
                }
                else if ($(this).find("Status").text() == "9") {
                    $(".OrderStatus", row).html("<span class='badge badge badge-pill Status_Add_On'>" + $(this).find("OrderStatus").text() + "</span>");
                }
                else if ($(this).find("Status").text() == "10") {
                    $(".OrderStatus", row).html("<span class='badge badge badge-pill Status_Add_On_Packed'>" + $(this).find("OrderStatus").text() + "</span>");
                }
                else if ($(this).find("Status").text() == "11") {
                    $(".OrderStatus", row).html("<span class='badge badge badge-pill Status_Close'>" + $(this).find("OrderStatus").text() + "</span>");
                }



                if ($(this).find("PaymentStatus").text() == "PAID") {
                    $(".PaymentStatus", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("PaymentStatus").text() + "</span>");
                }
                else if ($(this).find("PaymentStatus").text() == "NOT PAID") {
                    $(".PaymentStatus", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("PaymentStatus").text() + "</span>");
                }
                else if ($(this).find("PaymentStatus").text() == "N/A") {
                    $(".PaymentStatus", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("PaymentStatus").text() + "</span>");
                }
                else if ($(this).find("PaymentStatus").text() == "PARTIAL PAID") {
                    $(".PaymentStatus", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("PaymentStatus").text() + "</span>");
                }
                $(".OrderTotal", row).text($(this).find("OrderTotal").text());
                $(".PaidAmount", row).text($(this).find("PaidAmount").text());
                $(".DueAmount", row).text($(this).find("DueAmount").text());
                $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                $(".WarehouseManager", row).text($(this).find("WarehouseManager").text());
                $(".Packer", row).text($(this).find("Packer").text());
                GrandAmount += parseFloat($(this).find("OrderTotal").text());
                PaidAmount += parseFloat($(this).find("PaidAmount").text());
                DueAmount += parseFloat($(this).find("DueAmount").text());
                $(".SalesManager", row).text($(this).find("SalesManager").text());
                $(".Driver", row).text($(this).find("Driver").text());
                $(".Account", row).text($(this).find("Account").text());
                $("#tblOrderList tbody").append(row);
                row = $("#tblOrderList tbody tr:last").clone(true);
            });
        }
        else {
            //$("#EmptyTable").show();
            $('#TGrandAmount').html('0.00');
            $('#TPaidAmount').html('0.00');
            $('#TDueAmount').html('0.00');
            //$('#TGrandAmounts').html('0.00');
            //$('#TPaidAmounts').html('0.00');
            //$('#TDueAmounts').html('0.00');
        }
        $('#TGrandAmount').html(parseFloat(GrandAmount).toFixed(2));
        $('#TPaidAmount').html(parseFloat(PaidAmount).toFixed(2));
        $('#TDueAmount').html(parseFloat(DueAmount).toFixed(2));

        $(OTotal).each(function () {
            $('#TGrandAmounts').html(parseFloat($(this).find('OrderTotal').text()).toFixed(2));
            $('#TPaidAmounts').html(parseFloat($(this).find('PaidAmount').text()).toFixed(2));
            $('#TDueAmounts').html(parseFloat($(this).find('DueAmount').text()).toFixed(2));
        });

        var pager = $(xmldoc).find("Table1");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });

        

    }
}

function BindCustomerDetails() {
    $.ajax({
        type: "POST",
        url: "Daily_Order_Payment_Status.aspx/bindStatus",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlStatus option:not(:first)").remove();
                $.each(getData[0].AllStatus, function (index, item) {
                    $("#ddlStatus").append("<option value='" + item.SId + "'>" + item.ST + "</option>");
                });
                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(getData[0].SPeson, function (index, item) {
                    $("#ddlSalesPerson").append("<option value='" + item.SPId + "'>" + item.SPN + "</option>");
                });
                $("#ddlSalesPerson").select2();
                $("#ddlDriver option:not(:first)").remove();
                $.each(getData[0].Driver, function (index, item) {
                    $("#ddlDriver").append("<option value='" + item.DId + "'>" + item.DN + "</option>");
                });
                $("#ddlDriver").select2();

            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function CreateTable(us) {
    var image = $("#imgName").val();
    var data = {
        PageSize: 0,
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        CloseOrderFromDate: $("#txtOrderCloseFromDate").val(),
        CloseOrderToDate: $("#txtOrderCloseToDate").val(),
        SalesPerson: $('#ddlSalesPerson').val(),
        DriverAutoId: $('#ddlDriver').val(),
        OrderStatus: $('#ddlStatus').val(),
        PaymentStatus: $('#ddlPaymentStatus').val(),
        Index: 1
    };
    $.ajax({
        type: "POST",
        url: "Daily_Order_Payment_Status.aspx/OrderPaymentStatusReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async:true,
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    debugger
                    var xmldoc = $.parseXML(response.d);
                    var OrderTotal = $(xmldoc).find("Table");
                    var PrintDate = $(xmldoc).find("Table1");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr:last-child").clone(true);
                    var GrandAmount = 0.00, PaidAmount = 0.0, DueAmount = 0.00;
                    if (OrderTotal.length > 0) {
                        $.each(OrderTotal, function () {
                            $(".P_OrderDate", row).text($(this).find("OrderDate").text());
                            $(".P_OrderNo", row).text($(this).find("OrderNo").text());
                            $(".P_CustomerName", row).text($(this).find("CustomerName").text());
                            $(".P_OrderStatus", row).html($(this).find("OrderStatus").text());
                            $(".P_PaymentStatus", row).html($(this).find("PaymentStatus").text());

                            $(".P_OrderTotal", row).text($(this).find("OrderTotal").text());
                            $(".P_PaidAmount", row).text($(this).find("PaidAmount").text());
                            $(".P_DueAmount", row).text($(this).find("DueAmount").text());
                            $(".P_SalesPerson", row).text($(this).find("SalesPerson").text());
                            $(".P_WarehouseManager", row).text($(this).find("WarehouseManager").text());
                            $(".P_Packer", row).text($(this).find("Packer").text());
                            GrandAmount += parseFloat($(this).find("OrderTotal").text());
                            PaidAmount += parseFloat($(this).find("PaidAmount").text());
                            DueAmount += parseFloat($(this).find("DueAmount").text());
                            $(".P_SalesManager", row).text($(this).find("SalesManager").text());
                            $(".P_Driver", row).text($(this).find("Driver").text());
                            $(".P_Account", row).text($(this).find("Account").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });

                        $('#P_TGrandAmount').html(parseFloat(GrandAmount).toFixed(2));
                        $('#P_TPaidAmount').html(parseFloat(PaidAmount).toFixed(2));
                        $('#P_TDueAmount').html(parseFloat(DueAmount).toFixed(2));
                    }
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                           mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                    // mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtSFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                        
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "DailyOrderPaymentStatus(" + PrintDate.find('PrintDate').text() + ")",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('Please select atleast one Record in this list', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
    CreateTable(2);
    }
    else {
        toastr.error('Please select atleast one Record in this list', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
