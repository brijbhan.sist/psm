﻿$(document).ready(function () {
    bindCategory();
});
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "WebAPI/ProductCatalogueReportasmx.asmx/Category",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var GetCategory = $(xmldoc).find("Table");

                    $("#ddlCategoryName option:not(:first)").remove();
                    $.each(GetCategory, function () {
                        $("#ddlCategoryName").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CategoryName").text() + "</option>");
                    });
                    $("#ddlCategoryName").select2()
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function BindSubCategory() {
    var data = {
        CategoryAutoId: $("#ddlCategoryName").val()
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/ProductCatalogueReportasmx.asmx/SubCategory",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var GetCategory = $(xmldoc).find("Table");

                    $("#ddlSubCategoryName option:not(:first)").remove();
                    $.each(GetCategory, function () {
                        $("#ddlSubCategoryName").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Subcategory").text() + "</option>");
                    });
                    $("#ddlSubCategoryName").select2()
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function Pagevalue(e) {
    GetProductCatalogue(parseInt($(e).attr("page")));
}
$("#btnSearch").click(function () {
    GetProductCatalogue(1);
});
$("#ddlPageSize").change(function () {
    GetProductCatalogue(1);
})
function GetProductCatalogue(PageIndex) {
    var data = {
        ProductId: $("#txtProductId").val().trim(),
        ProductName2: $("#txtProductName").val().trim(),
        CategoryAutoId: $("#ddlCategoryName").val(),
        SubCategoryName: $("#ddlSubCategoryName").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/ProductCatalogueReportasmx.asmx/GetProductCatelogReportList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetRouteList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetRouteList(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var RouteList = $(xmldoc).find("Table");
            $("#tblProductCatelogReport tbody tr").remove();
            var row = $("#tblProductCatelogReport thead tr").clone(true);
            if (RouteList.length > 0) {
                $("#EmptyTable").hide();
                $.each(RouteList, function (index) {
                    $(".ProductId", row).text($(this).find("ProductId").text());
                    $(".CategoryName", row).text($(this).find("CategoryName").text());
                    $(".SubcategoryName", row).text($(this).find("SubcategoryName").text());
                    $(".ProductName", row).text($(this).find("ProductName").text());
                    $(".UnitType", row).text($(this).find("UnitType").text());
                    $(".Qty", row).text($(this).find("Qty").text());
                    $(".RetailMinPrice", row).text($(this).find("RetailMinPrice").text());
                    $(".CostPrice", row).text($(this).find("CostPrice").text());
                    $(".BasePrice", row).text($(this).find("BasePrice").text());
                    $(".wholesaleminprice", row).text($(this).find("wholesaleminprice").text());
                    $(".SRP", row).text($(this).find("SRP").text());
                    $("#tblProductCatelogReport tbody").append(row);
                    row = $("#tblProductCatelogReport tbody tr:last").clone(true);
                });

            } else {
                $("#EmptyTable").show();
            }

            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}

function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        ProductId: $("#txtProductId").val(),
        ProductName2: $("#txtProductName").val(),
        CategoryAutoId: $("#ddlCategoryName").val(),
        SubCategoryName: $("#ddlSubCategoryName").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/ProductCatalogueReportasmx.asmx/GetProductCatelogReportList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var RouteList = $(xmldoc).find("Table");
                    var PrintDate = $(xmldoc).find("Table1");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (RouteList.length > 0) {
                        $.each(RouteList, function () {
                            $(".P_ProductId", row).text($(this).find("ProductId").text());
                            $(".P_CategoryName", row).text($(this).find("CategoryName").text());
                            $(".P_SubcategoryName", row).text($(this).find("SubcategoryName").text());
                            $(".P_ProductName", row).text($(this).find("ProductName").text());
                            $(".P_UnitType", row).text($(this).find("UnitType").text());
                            $(".P_Qty", row).text($(this).find("Qty").text());
                            $(".P_RetailMinPrice", row).text($(this).find("RetailMinPrice").text());
                            $(".P_CostPrice", row).text($(this).find("CostPrice").text());
                            $(".P_BasePrice", row).text($(this).find("BasePrice").text());
                            $(".P_wholesaleminprice", row).text($(this).find("wholesaleminprice").text());
                            $(".P_SRP", row).text($(this).find("SRP").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                    }
                    if (us == 1) {
                        //var image = $("#imgName").val();
                        //var divToPrint = document.getElementById("PrintTable");
                        //var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        //mywindow.document.write('<html><head><style>#PrintTable tbody tr .right{text-align:right;} #PrintTable tbody tr .left{text-align:left;} #PrintTable tbody tr .price{text-align:right;}#PrintTable {border-collapse: collapse;width: 100%;}#PrintTable td, #PrintTable th {border: 1px solid black;padding: 8px;}#PrintTable tr:nth-child(even){background-color: #f2f2f2;}#PrintTable thead {padding-top: 12px;padding-bottom: 12px;background-color:#C8E8F9;color:black;}.text-right{text-align:right;}.text-left{text-align:left;}.text-center{text-align:center;}</style>');
                        //mywindow.document.write('</head><body>');
                        //mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;'><img src='/Img/logo/" + image + "' style='float:left;' height='70px' width='140px'/><h3 style='float:center;'>Product Catalog Report</h3><h4 style='float:right;margin-right:10px;'>Date: " + (new Date()).format("MM/dd/yyyy") + "</h4></div>")
                        //mywindow.document.write(divToPrint.outerHTML);
                        //mywindow.document.write('</body></html>');
                        //mywindow.print();
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        mywindow.document.write($(PrintTable1).clone().html());
                        //mywindow.document.write(divToPrint.outerHTML);

                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "ProductCatalogReport",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblProductCatelogReport tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
$("#btnExport").click(function () {
    if ($('#tblProductCatelogReport tbody tr').length > 0) {
    CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});