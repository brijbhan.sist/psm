﻿
$(document).ready(function () {   
    getLocationwiseBarCodeReport(1);  
});
function Pagevalue(e) {
    getLocationwiseBarCodeReport(parseInt($(e).attr("page")));
}
function getLocationwiseBarCodeReport(PageIndex) {
    var data = {       
        PageIndex: PageIndex,
        PageSize: $('#ddlPagesize').val()
    }; 
    $.ajax({
        type: "POST",
        url: "BarCodeReportMaster.aspx/LocationwiseBarCodeReport",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({ 
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successgetBarCodeITemWiseReportMaster,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function successgetBarCodeITemWiseReportMaster(response) {
    if (response.d == 'Session Expired') {
        location.href = "/";
    } else {
        debugger;
        var xmldoc = $.parseXML(response.d);
        var ReportDetails = $(xmldoc).find("Table");
        $("#BarCodeITemWiseReportMaster tbody tr").remove();
        var row = $("#BarCodeITemWiseReportMaster thead tr").clone(true);
        if (ReportDetails.length > 0) {
            $("#EmptyTable").hide();
            $("#BarCodeITemWiseReportMaster").show();
            $.each(ReportDetails, function (index) {
                $(".SN", row).text(index+1);
                $(".db", row).text($(this).find("db").text());
                $(".ProductId", row).text($(this).find("ProductId").text());
                $(".productname", row).text($(this).find("productname").text());
                $(".UnitAutoId", row).text($(this).find("UnitAutoId").text());
                $(".Barcode", row).text($(this).find("Barcode").text());
                $(".BarcodeType", row).html($(this).find("BarcodeType").text());
                $(".CreatedDate", row).text($(this).find("CreatedDate").text());
                $(".ProductId1", row).text($(this).find("ProductId1").text());
                $(".UnitAutoId1", row).text($(this).find("Unit1").text());
                $(".Barcode1", row).text($(this).find("Barcode1").text());
                $(".BarcodeType1", row).html($(this).find("BarcodeType1").text());
                $("#BarCodeITemWiseReportMaster tbody").append(row);
                row = $("#BarCodeITemWiseReportMaster tbody tr:last").clone(true);               
            });           
        } 
        var pager = $(xmldoc).find("Table");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
    }
} 
$("#ddlPagesize").change(function () {
    getLocationwiseBarCodeReport(1);
})
