﻿$(document).ready(function () {
    
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
        defaultDate: new Date()
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = (('' + month).length < 2 ? '0' : '') + month + '/' +
        (('' + day).length < 2 ? '0' : '') + day + '/' + d.getFullYear();

    $("#txtSFromDate").val(output);
    $("#txtSToDate").val(output);
    BindReport(1);

});
function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
}
//$("#ddlPageSize").change(function () {
//    BindReport(1);
//})
function getSearchdata() {
    BindReport(1);
}
function BindReport(PageIndex) {

        var data = {
            FromDate: $("#txtSFromDate").val(),
            ToDate: $("#txtSToDate").val(),
            PageSize: $("#ddlPageSize").val(),
            PageIndex: PageIndex
        };
        $.ajax({
            type: "POST",
            url: "ApiTrackingReport.aspx/BindReport",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                console.log(response);
                if (response.d != "Session Expired") {
                    if (response.d == 'false') {
                        location.href = "/Default.aspx"
                    }
                    else {
                        ApiLogReport(response);
                    }
                }
                else {
                    location.href = "/";
                }
            },
            error: function (result) {
                alert(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
}

function ApiLogReport(response) {

    var xmldoc = $.parseXML(response.d);
    var ProductStatusSaleReportDetail = $(xmldoc).find("Table1");
    $("#tblApiTrackingReport tbody tr").remove();
    var row = $("#tblApiTrackingReport thead tr").clone(true);
    if (ProductStatusSaleReportDetail.length > 0) {
        $("#EmptyTable").hide();
        $("#tblApiTrackingReport thead tr").show();
        $.each(ProductStatusSaleReportDetail, function (index) {
            $(".T_AccessToken", row).text($(this).find("accessToken").text());
            $(".T_AppVersion", row).text($(this).find("appversion").text());
            $(".T_UserName", row).text($(this).find("UserName").text());
            $(".T_DeviceId", row).text($(this).find("deviceId").text());
            $(".T_StartDate", row).text($(this).find("starttimestamp").text());
            $(".T_EndDate", row).text($(this).find("endtimestamp").text());
            $(".T_CreatedOn", row).text($(this).find("creationdate").text());
            $(".T_Minute", row).text($(this).find("DiffInMinute").text());
            $(".T_Second", row).text($(this).find("DiffInSecond").text());
            $("#tblApiTrackingReport tbody").append(row);
            row = $("#tblApiTrackingReport tbody tr:last").clone(true);
        });
    }
    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}
function CustcheckRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {

        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');

        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}