﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true

    });
    BindDropdown();
    GetReport(1);
});
function BindDropdown() {
    $.ajax({
        type: "POST",
        url: "ProductPacked_WithoutBarcodeReport.aspx/bindDropDown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);
                  
                    $("#ddlProduct option:not(:first)").remove();
                    $.each(getData[0].Product, function (index, Pro) {
                        $("#ddlProduct").append("<option value='" + Pro.AutoId + "'>" + Pro.ProductName + "</option>");
                    });
                    $("#ddlProduct").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function searchReport() {
    GetReport(1);
}

function Pagevalue(e) {
    GetReport(parseInt($(e).attr("page")));
};

function GetReport(PageIndex) {
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        OrderNo: $("#txtOrderNo").val().trim(),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageIndex: PageIndex,
        PageSize: $('#ddlPageSize').val()
    };

    $.ajax({
        type: "POST",
        url: "ProductPacked_WithoutBarcodeReport.aspx/GetReport",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ReportDetails = $(xmldoc).find('Table1');
                    $("#tblOrderSaleReport tbody tr").remove();
                    
                    if (ReportDetails.length > 0) {
                        $('#EmptyTable').hide();
                        var html = '';
                        $.each(ReportDetails, function () {
                            html += '<tr>';
                            html += '<td class="ProductId text-center tblwth" style="width:90px;white-space:nowrap">' + $(this).find("ProductId").text() + '</td>';
                            html += '<td class="Name text-left" style="width:72px;white-space:nowrap">' + $(this).find("ProductName").text() + '</td>';
                            html += '<td class="OrderNo tblwth text-center">' + $(this).find("OrderNo").text() + '</td>';
                            html += '<td class="PackedBy text-center" style="width:50px;white-space:nowrap;width:10%">' + $(this).find("PackedBy").text() + ' </td>';
                            html += '<td class="PackedDate text-center" style="width:15%">' + $(this).find("PackedDate").text() + '</td>';
                            html += '<td class="ApprovedBy text-center" style="width:10%">' + $(this).find("ApprovedBy").text() + '</td>';
                            html += '</tr>';

                        });
                        $('#tblOrderSaleReport tbody').html(html);

                    }
                    
                    var pager = $(xmldoc).find("Table");
                    $(".Pager").ASPSnippets_Pager({
                        ActiveCssClass: "current",
                        PagerCssClass: "pager",
                        PageIndex: parseInt(pager.find("PageIndex").text()),
                        PageSize: parseInt(pager.find("PageSize").text()),
                        RecordCount: parseInt(pager.find("RecordCount").text())
                    });


                }
            }
            else {
                location.href = "/";
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}
function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}

function CreateTable(us) {
  
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        OrderNo: $("#txtOrderNo").val().trim(),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageIndex: 1,
        PageSize: 0
    };


    $.ajax({
        type: "POST",
        url: "ProductPacked_WithoutBarcodeReport.aspx/GetReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var OrderTotal = $(xmldoc).find("Table1");
                    var PrintDate = $(xmldoc).find("Table2");
                    if ($("#txtFromDate").val() != "") {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Product Packed Without Barcode Report</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + $(PrintDate).find("PrintDate").text() + "</span><br/><span class='DateRangeCSS' style='float:center;margin-top: 0.5%; font-size: 9px; color: black; font-weight: bold;'>Date Range: " + $("#txtFromDate").val() + " To " + $("#txtToDate").val() + "</span><br/></div>"
                    }
                    else {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Product Packed Without Barcode Report</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + $(PrintDate).find("PrintDate").text() + "</span></div>"
                    }
                    row1 += "<table class='PrintMyTableHeader MyTableHeader' id='RptTable'>"
                    row1 += "<thead  class='bg-blue white'>"
                    row1 += "<tr>"
                    if (us == 1) {
                        row1 += '<td class="ProductId text-center tblwth" style="width:90px;white-space:nowrap;text-align:center;">Product ID</td>';
                        row1 += '<td class="Name text-center" style="width:72px;white-space:nowrap;text-align:center;">Product Name</td>';
                        row1 += '<td class="OrderNo tblwth text-center" style="text-align:center;">Order No.</td>';
                        row1 += '<td class="PackedBy tblwth text-center" style="width:50px;white-space:nowrap;text-align:center;">Packed By</td>';
                        row1 += '<td class="PackedDate tblwth text-center" style="text-align:center;">Packed Date</td>';
                        row1 += '<td class="ApprovedBy tblwth text-center" style="text-align:center;">Approved By</td>';
                    } else {
                        row1 += '<td class="ProductId text-center tblwth" style="width:90px;white-space:nowrap;text-align:center;">Product ID</td>';
                        row1 += '<td class="Name text-center" style="width:72px;white-space:nowrap;text-align:center;">Product Name</td>';
                        row1 += '<td class="OrderNo tblwth text-center" style="text-align:center;">Order No.</td>';
                        row1 += '<td class="PackedBy tblwth text-center" style="width:50px;white-space:nowrap;text-align:center;">Packed By</td>';
                        row1 += '<td class="PackedDate tblwth text-center" style="text-align:center;">Packed Date</td>';
                        row1 += '<td class="ApprovedBy tblwth text-center" style="text-align:center;">Approved By</td>';
                    }
                    row1 += "</tr>"
                    row1 += "</thead>"
                    row1 += "<tbody>"
                    if (OrderTotal.length > 0) {
                        var i = 1;
                        $.each(OrderTotal, function (index) {
                            row1 += '<tr>';
                            row1 += '<td class="ProductId text-center tblwth" style="width:90px;white-space:nowrap;text-align:center;">' + $(this).find("ProductId").text() + '</td>';
                            row1 += '<td class="Name text-left" style="width:72px;white-space:nowrap;">' + $(this).find("ProductName").text() + '</td>';
                            row1 += '<td class="OrderNo tblwth text-center" style="text-align:center;">' + $(this).find("OrderNo").text() + '</td>';
                            row1 += '<td class="PackedBy tblwth text-center" style="width:50px;white-space:nowrap;text-align:center;">' + $(this).find("PackedBy").text() + ' </td>';
                            row1 += '<td class="PackedDate tblwth text-center" style="text-align:center;">' + $(this).find("PackedDate").text() + '</td>';
                            row1 += '<td class="ApprovedBy tblwth text-center" style="text-align:center;">' + $(this).find("ApprovedBy").text() + '</td>';
                            row1 += '</tr>';

                        });
                        row1 += "</tbody>"
                       
                        row1 += "</table>"
                        if (us == 1) {
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style>#RptTable {border-collapse: collapse;width: 100%;}#RptTable td, #RptTable th {border: 1px solid black;}#RptTable tr:nth-child(even){background-color: #f2f2f2;}#RptTable thead {padding-top: 12px;padding-bottom: 12px;text-align:center;background-color:#e9e8e8 !important;color:black;}.text-right{text-align:right;}.text-left{text-align:left;}.text-center{text-align:center;}</style>');
                            mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                            mywindow.document.write('</head><body>');
                            mywindow.document.write(row1);
                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        }
                        if (us == 2) {
                            $("#ExcelDiv").html(row1);
                            $("#RptTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "Product Packed Without Barcode Report",
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });

                        }
                    }

                }
                else {
                    location.href = "/";
                }
            }

        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderSaleReport tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderSaleReport tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
