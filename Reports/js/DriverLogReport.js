﻿$(document).ready(function () {
    BindReport(1);
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
})
function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
}
$("#ddlPageSize").change(function () {
    BindReport(1);
})
function getSearchdata() {
    BindReport(1);
}
function BindReport(PageIndex) {
    debugger
    var data = {
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "DriverLogReport.aspx/BindReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.d != "Session Expired") {
                if (response.d == 'false') {
                    location.href = "/Default.aspx"
                }
                else {
                    SuccessPackerResport(response);
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function SuccessPackerResport(response) {
    var xmldoc = $.parseXML(response.d);
    var PackerResportDetail = $(xmldoc).find("Table1");
    $("#tblDriverLogReport tbody tr").remove();
    var row = $("#tblDriverLogReport thead tr").clone(true);
    if (PackerResportDetail.length > 0) {
        $("#EmptyTable").hide();
        $("#tblDriverLogReport thead tr").show();
        $.each(PackerResportDetail, function (index) {
            $(".PlanningId", row).text($(this).find("PlanningId").text());
            $(".DriverName", row).text($(this).find("DriverName").text());
            $(".PlanningDriverName", row).text($(this).find("PlanningDriverName").text());
            $(".DriverNo", row).text($(this).find("DriverNo").text());
            $(".NoOfStop", row).text($(this).find("NoOfStop").text());
            $(".CreateDate", row).text($(this).find("CreateDate").text());
            $(".DriverStartTime", row).text($(this).find("DriverStartTime").text());
            $(".DraverEndTime", row).text($(this).find("DraverEndTime").text());

            $(".CarName", row).text($(this).find("CarName").text());
            $(".RouteDate", row).text($(this).find("RouteDate").text());
            $(".ManagerName", row).text($(this).find("ManagerName").text());
            $("#tblDriverLogReport tbody").append(row);
            row = $("#tblDriverLogReport tbody tr:last").clone(true);
        });
    }
    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}