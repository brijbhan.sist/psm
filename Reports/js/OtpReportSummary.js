﻿$(document).ready(function () {
   
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    bindTaxType();
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtDateFrom").val(month + '/' + day + '/' + year);
    $("#txtDateTo").val(month + '/' + day + '/' + year);
});

function Pagevalue(e) { 
    OtpReport(parseInt($(e).attr("page")));
}

function OtpReport(PageIndex) {
    var data = {
        DateFrom: $("#txtDateFrom").val(),
        DateTo: $("#txtDateTo").val(),
        PageSize: $("#ddlPageSize").val(),
        TaxType: $("#ddlTaxType").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "OtpReport.aspx/GetOtpReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: SuccessOtpReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function SuccessOtpReport(response) {
    console.log(response.d);
    var xmldoc = $.parseXML(response.d);
    var OtpReportDetail = $(xmldoc).find("Table");
    var OT = $(xmldoc).find("Table2");
    var TotalQty = 0, TotalPurchasePrice = 0, TotalTax = 0;
    $("#tblOtpReportSummary tbody tr").remove();
    var row = $("#tblOtpReportSummary thead tr").clone(true); 
    if (OtpReportDetail.length > 0) {
        $("#EmptyTable").hide();
        $("#tblOtpReportSummary thead tr").show();
        $.each(OtpReportDetail, function (index) {
            console.log("If call");

            $(".InvoiceDate", row).html($(this).find("InvoiceDate").text());
            $(".InvoiceNumber", row).html($(this).find("InvoiceNumber").text());
            $(".NameofCommonCarrier", row).html($(this).find("NameofCommonCarrier").text());
            $(".SellerName", row).html($(this).find("SellerName").text());
            $(".Address", row).html($(this).find("Address").text());
            $(".City", row).html($(this).find("City").text());
            $(".StateName", row).html($(this).find("StateName").text());
            $(".OTPLicenseNumber", row).html($(this).find("OTPLicenseNumber").text());
            $(".BrandFamily", row).html($(this).find("BrandFamily").text());
            $(".ProductDescription", row).html($(this).find("ProductDescription").text());
            $(".QuantityofItemSold", row).html($(this).find("QuantityofItemSold").text());
            $(".PurchasePrice", row).html($(this).find("PurchasePrice").text());
            $(".TotalTaxCollected", row).html($(this).find("TotalTaxCollected").text());
            $(".TaxCollectedYesOrNo", row).html($(this).find("TaxCollectedYesOrNo").text());
            $(".Zipcode", row).html($(this).find("Zipcode").text());
            $("#tblOtpReportSummary tbody").append(row);
            row = $("#tblOtpReportSummary tbody tr:last").clone(true);

            TotalQty += parseFloat($(this).find("QuantityofItemSold").text());
            TotalPurchasePrice += parseFloat($(this).find("PurchasePrice").text());
            TotalTax += parseFloat($(this).find("TotalTaxCollected").text());
        });
        $("#TotalQtyS").html(parseFloat(TotalQty).toFixed(0));
        $("#TotalPurchasePriceS").html(parseFloat(TotalPurchasePrice).toFixed(2));
        $("#TotalTaxS").html(parseFloat(TotalTax).toFixed(2));
     }
    else
    {
        $("#TotalQtyS").html('0');
        $("#TotalPurchasePriceS").html('0.00');
        $("#TotalTaxS").html('0.00');
        $("#OTotalQty").html('0');
        $("#OTotalPurchasePrice").html('0.00');
        $("#OTotalTax").html('0.00');
    }  
    if ($(OT).find("TotalSoldQty").text() != '') {
        $("#OTotalQty").html(parseFloat($(OT).find("TotalSoldQty").text()).toFixed(0));
    } else {
        $("#OTotalQty").html('0');
    }

    if ($(OT).find("TotalPrice").text() != '') {
        $("#OTotalPurchasePrice").html(parseFloat($(OT).find("TotalPrice").text()).toFixed(2));
    } else {
        $("#OTotalPurchasePrice").html('0.00');
    }

    if ($(OT).find("Totaltax").text() != '') {
        $("#OTotalTax").html(parseFloat($(OT).find("Totaltax").text()).toFixed(2));
    } else {
        $("#OTotalTax").html('0.00');
    }

    
    var pager = $(xmldoc).find("Table1");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}

function CreateTable(us) {
    var image = $("#imgName").val();
    var data = {
        DateFrom: $("#txtDateFrom").val(),
        DateTo: $("#txtDateTo").val(),
        TaxType: $("#ddlTaxType").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "OtpReport.aspx/GetOtpReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var OtpReportDetail = $(xmldoc).find("Table");
                    var PrintTime = $(xmldoc).find("Table3");
                    var Printingtime = ($(PrintTime).find("PrintTime").text());
                    var TotalQty = 0, TotalPurchasePrice = 0, TotalTax = 0;
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr:last-child").clone(true);
                    if (OtpReportDetail.length > 0) {
                        $.each(OtpReportDetail, function () {
                            $(".P_InvoiceDate", row).html($(this).find("InvoiceDate").text());
                            $(".P_InvoiceNumber", row).html($(this).find("InvoiceNumber").text());
                            $(".P_NameofCommonCarrier", row).html($(this).find("NameofCommonCarrier").text());
                            $(".P_SellerName", row).html($(this).find("SellerName").text());
                            $(".P_Address", row).html($(this).find("Address").text());
                            $(".P_City", row).html($(this).find("City").text());
                            $(".P_StateName", row).html($(this).find("StateName").text());
                            $(".P_OTPLicenseNumber", row).html($(this).find("OTPLicenseNumber").text());
                            $(".P_BrandFamily", row).html($(this).find("BrandFamily").text());
                            $(".P_ProductDescription", row).html($(this).find("ProductDescription").text());
                            $(".P_QuantityofItemSold", row).html($(this).find("QuantityofItemSold").text());
                            $(".P_PurchasePrice", row).html($(this).find("PurchasePrice").text());
                            $(".P_TotalTaxCollected", row).html($(this).find("TotalTaxCollected").text());
                            $(".P_TaxCollectedYesOrNo", row).html($(this).find("TaxCollectedYesOrNo").text());
                            $(".P_Zipcode", row).html($(this).find("Zipcode").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);

                            TotalQty += parseFloat($(this).find("QuantityofItemSold").text());
                            TotalPurchasePrice += parseFloat($(this).find("PurchasePrice").text());
                            TotalTax += parseFloat($(this).find("TotalTaxCollected").text());
                        });
                        $("#TotalQty").html(parseFloat(TotalQty).toFixed(0));
                        $("#TotalPurchasePrice").html(parseFloat(TotalPurchasePrice).toFixed(2));
                        $("#TotalTax").html(parseFloat(TotalTax).toFixed(2));
                    }                   
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" +" "+ Printingtime );
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtDateFrom").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtDateFrom").val() + " To " + $("#txtDateTo").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);

                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "OPT Report By Vape" + (new Date()).format("MM/dd/yyyy hh:mm tt"),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOtpReportSummary tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOtpReportSummary tbody tr').length > 0) {
    CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function bindTaxType() {
    $.ajax({
        type: "POST",
        url: "OtpReport.aspx/BindTaxType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlTaxType option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlTaxType").append("<option value='" + getData[index].TID + "'>" + getData[index].TT + "</option>");
                });
               
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}