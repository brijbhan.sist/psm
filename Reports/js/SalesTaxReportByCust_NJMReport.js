﻿var row1 = "", c4 = 0;
$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    BindCustomerDropdown()
    BindProductCategory();
    BindProductSubCategory();
    BindSalesPersonandStatus();
    BindProductByBrand();
   
});
function setdatevalidation(val) {
    var fdate = $("#txtSFromDate").val();
    var tdate = $("#txtSToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromDate").val(tdate);
        }
    }
}
function Pagevalue(e) {
    getReport(parseInt($(e).attr("page")));
};
function BindCustomerDropdown() {
    $.ajax({
        type: "POST",
        url: "SalesTaxReportByCust_NJMReport.aspx/CustomerDropdown",
        data: "{'SalesPersonAutoId':'" + $("#ddlAllPerson").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {

                    var getData = $.parseJSON(response.d);
                    $("#ddlAllCustomer option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlAllCustomer").append("<option value='" + getData[index].CUID + "'>" + getData[index].CUN + "</option>");
                    });
                    $("#ddlAllCustomer").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
var BrandId = 0;
function BindSalesPersonandStatus() {
    $.ajax({
        type: "POST",
        url: "SalesTaxReportByCust_NJMReport.aspx/BindCustomer",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {

                    var getData = $.parseJSON(response.d);

                    $("#ddlAllPerson option:not(:first)").remove();
                    $.each(getData[0].SalesPer, function (index, item) {
                        $("#ddlAllPerson").append("<option value='" + item.EID + "'>" + item.EN + "</option>");
                    });
                    $("#ddlAllPerson").select2();


                    $("#ddlBrand").attr('multiple', 'multiple')
                    $("#ddlBrand option:not(:first)").remove();
                    $.each(getData[0].Brand, function (index, item) {
                        $("#ddlBrand").append("<option value='" + item.BID + "'>" + item.BN + "</option>");
                    });

                    $("#ddlBrand").select2({
                        placeholder: 'All Brand',
                        allowClear: true
                    })
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function BindProductByBrand() {
    debugger;
    BrandId = 0;
    $("#ddlBrand option:selected").each(function (i) {
        console.log(i)
        if (i == 0) {
            BrandId = $(this).val() + ',';

        } else {
            BrandId += $(this).val() + ',';
        }
    });
    if (BrandId == "0,") {
        BrandId = "0";
    }
    var data = {
        CategoryAutoId: $("#ddlAllCategory").val(),
        SubCategoryAutoId: $("#AllSubCategory").val(),
        BrandId: BrandId.toString()
    };
    $.ajax({
        type: "POST",
        url: "SalesTaxReportByCust_NJMReport.aspx/BindProductByBrand",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);
                    $("#ddlProduct option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlProduct").append("<option value='" + getData[index].PID + "'>" + getData[index].PN + "</option>");
                    });
                    $("#ddlProduct").select2();

                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function BindProductCategory() {
    $.ajax({
        type: "POST",
        url: "SalesTaxReportByCust_NJMReport.aspx/BindProductCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlAllCategory option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlAllCategory").append("<option value='" + getData[index].CId + "'>" + getData[index].CN + "</option>");
                    });
                    $("#ddlAllCategory").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
/*---------------------------------------------------Bind Sub Category-----------------------------------------------------------*/
function BindProductSubCategory() {
    $.ajax({
        type: "POST",
        url: "SalesTaxReportByCust_NJMReport.aspx/BindProductSubCategory",
        data: "{'CategoryAutoId':'" + $("#ddlAllCategory").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {

                    var getData = $.parseJSON(response.d);

                    $("#AllSubCategory option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#AllSubCategory").append("<option value='" + getData[index].SCID + "'>" + getData[index].SCN + "</option>");
                    });
                    $("#AllSubCategory").select2();


                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function getReportByPagingFilter() {
    if ($("#txtSFromDate").val() == "" || $("#txtSToDate").val() == "") {
        toastr.error('From Date and To Date is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtSFromDate").addClass('border-warning');
        $("#txtSToDate").addClass('border-warning');
    }
    else {
        getReport(1);
    }
}

function getReportByShorting(num) {
    if ($("#txtSFromDate").val() == "" || $("#txtSToDate").val() == "") {
        toastr.error('From Date and To Date is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtSFromDate").addClass('border-warning');
        $("#txtSToDate").addClass('border-warning');
    }
    else {
        $("#hfDataSorting").val(num);
        getReport(1);
    }
}
function getReport(PageIndex) {
    BrandId = 0;
    $("#ddlBrand option:selected").each(function (i) {
        if (i == 0) {
            BrandId = $(this).val() + ',';
        } else {
            BrandId += $(this).val() + ',';
        }
    });
    if (BrandId == "0,") {
        BrandId = "0"
    }
    var data = {
        CustomerAutoId: $("#ddlAllCustomer").val(),
        SalesAutoId: $("#ddlAllPerson").val(),
        ProductAutoId: $("#ddlProduct").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        CategoryAutoId: $("#ddlAllCategory").val(),
        SubCategoryAutoId: $("#AllSubCategory").val(),
        BrandId: BrandId.toString(),
        PageIndex: PageIndex,
        PageSize: $('#ddlPageSize').val()
    };

    $.ajax({
        type: "POST",
        url: "SalesTaxReportByCust_NJMReport.aspx/GetReportDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ReportDetails = $(xmldoc).find("Table1");
                    var OrderTotal = $(xmldoc).find("Table2");
                    $("#tblOrderList tbody tr").remove();
                    var row = $("#tblOrderList thead tr").clone(true);
                    var TotalPieceQty = 0;
                    if (ReportDetails.length > 0) {
                        $("#EmptyTable").hide();
                        $("#tblOrderList").show();

                        $.each(ReportDetails, function (index) {
                            $(".CustomerID", row).text($(this).find("CustomerId").text());
                            $(".CustomerName", row).text($(this).find("CustomerName").text());
                            $(".BrandName", row).text($(this).find("BrandName").text());
                            $(".ProductId", row).text($(this).find("ProductId").text());
                            $(".ProductName", row).text($(this).find("ProductName").text());
                            $(".Unit", row).text($(this).find("Unit").text());
                            $(".TotalPieces", row).html($(this).find("SoldQty").text());
                            TotalPieceQty = TotalPieceQty + parseFloat($(this).find("SoldQty").text());

                            $(".PayableAmount", row).html($(this).find("PayableAmount").text());
                            $("#tblOrderList tbody").append(row);
                            row = $("#tblOrderList tbody tr:last").clone(true);
                            ProductId = $(this).find("ProductId").text();
                            CustomerAutoId = $(this).find("CustomerAutoId").text();
                        });
                        $('#TotalPieceQty').html(parseFloat(TotalPieceQty).toFixed(2));

                    } else {
                        $('#AmtDue').html('0');
                        $('#TotalPieceQty').html('0');
                        $('#AmtDue').html('0');
                    }

                    $(OrderTotal).each(function () {
                        if ($(this).find("TotalSoldQty").text() > 0) {
                            $('#TotalPieceQtys').html(parseFloat($(this).find("TotalSoldQty").text()).toFixed(2));
                        } else {
                            $('#TotalPieceQtys').html('0');
                        }

                    });

                    var pager = $(xmldoc).find("Table");
                    $(".Pager").ASPSnippets_Pager({
                        ActiveCssClass: "current",
                        PagerCssClass: "pager",
                        PageIndex: parseInt(pager.find("PageIndex").text()),
                        PageSize: parseInt(pager.find("PageSize").text()),
                        RecordCount: parseInt(pager.find("RecordCount").text())
                    });
                }
            }
            else {
                location.href = "/";
            }
            c4 = c4 + 1;

        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function CreateTable(us) {
    BrandId = 0;
    $("#ddlBrand option:selected").each(function (i) {
        if (i == 0) {
            BrandId = $(this).val() + ',';
        } else {
            BrandId += $(this).val() + ',';
        }
    });
    if (BrandId == "0,") {
        BrandId = "0"
    }
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        CustomerAutoId: $("#ddlAllCustomer").val(),
        ProductAutoId: $("#ddlProduct").val(),
        SalesAutoId: $("#ddlAllPerson").val(),
        CategoryAutoId: $("#ddlAllCategory").val(),
        SubCategoryAutoId: $("#AllSubCategory").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        BrandId: BrandId.toString(),
        PageIndex: 1,
        PageSize: 0,
        //SortBySoldQty: $("#hfDataSorting").val()
    };

    $.ajax({
        type: "POST",
        url: "SalesTaxReportByCust_NJMReport.aspx/GetReportDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                debugger;
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var OrderTotal = $(xmldoc).find("Table1");
                    var printDate = $(xmldoc).find("Table2");
                    $("#RptTable").empty();
                    if ($("#txtSFromDate").val() != "") {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Vape Tax Report By Customer</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + $(printDate).find("PrintDate").text() + "</span><br/><span class='DateRangeCSS' style='float:center;margin-top: 0.5%; font-size: 9px; color: black; font-weight: bold;'>Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val() + "</span><br/></div>"
                    }
                    else {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Vape Tax Report By Customer</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + $(printDate).find("PrintDate").text() + "</span></div>"
                    }
                    row1 += "<table id='RptTable' class='PrintMyTableHeader MyTableHeader'>"
                    row1 += "<thead>"
                    row1 += "<tr>"
                    row1 += "<td style='text-align:center !important'>Customer ID</td>"
                    row1 += "<td class='text-left'>Customer Name</td>"
                    row1 += "<td style='text-align:center !important'>Brand Name</td>"
                    row1 += "<td style='text-align:center !important'>Product ID</td>"
                    row1 += "<td class='text-left'>Product Name</td>"
                    row1 += "<td class='text-left'>Unit</td>"
                    row1 += "<td  style='text-align:center !important'>Sold Qty</td>"

                    row1 += "</tr>"
                    row1 += "</thead>"
                    row1 += "<tbody>"
                    var TotalPieceQty = 0;
                    if (OrderTotal.length > 0) {
                        ProductTotal = 0.00, ProductId = 0, CustomerAutoId = 0;
                        $.each(OrderTotal, function (index) {
                            row1 += "<tr>";
                            row1 += "<td style='text-align:center !important'>" + $(this).find("CustomerId").text() + "</td>";
                            row1 += "<td class='left'>" + $(this).find("CustomerName").text() + "</td>";
                            row1 += "<td style='text-align:center !important'>" + $(this).find("BrandName").text() + "</td>";
                            row1 += "<td style='text-align:center !important'>" + $(this).find("ProductId").text() + "</td>";
                            row1 += "<td class='left'>" + $(this).find("ProductName").text() + "</td>";
                            row1 += "<td class='center'>" + $(this).find("Unit").text() + "</td>";
                            row1 += "<td style='text-align:center !important'>" + $(this).find("SoldQty").text() + "</td>";

                            TotalPieceQty = TotalPieceQty + parseFloat($(this).find("SoldQty").text());

                            row1 += "</tr>";
                        });
                        row1 += "</tbody>"
                        row1 += "<tfoot>"
                        row1 += "<tr>"
                        row1 += "<td colspan='6'  style='text-align:center !important;font-weight:bold'>Total</td>"
                        row1 += "<td class='text-center' style='font-weight:bold'>" + parseFloat(TotalPieceQty).toFixed(2) + "</td>"

                        row1 += "</tr>"
                        row1 += "</tfoot>"
                        row1 += "</table>"
                        if (us == 1) {
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style></style>');
                            mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/app-assets/css/custom.css"/></head><body>');
                            mywindow.document.write('</head><body>');
                            mywindow.document.write(row1);
                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        }
                        if (us == 2) {
                            $("#ExcelDiv").append(row1);
                            $("#RptTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "VapeTaxReportByCustomer" + (new Date()).format("MM/dd/yyyy hh:mm tt"),
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });

                        }
                    }

                }
                else {
                    location.href = "/";
                }
            }

        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function OpenPopUp() {
    toastr.success('Order Status - Closed.', 'Info', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
}


