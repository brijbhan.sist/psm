﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true

    });
    BindDropdown();
});
function BindDropdown() {
    $.ajax({
        type: "POST",
        url: "PackedStatusReport.aspx/bindDropDown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);
                    $("#ddlShippingType option:not(:first)").remove();
                    $.each(getData[0].Shipping, function (index, item) {
                        $("#ddlShippingType").append("<option value='" + item.AutoId + "'>" + item.ShippingType + "</option>");
                    });
                    $("#ddlShippingType").select2({
                        multiple: true,
                        placeholder: 'All Shipping Type',
                        allowClear: true
                    });
                    $("#ddlCustomer option:not(:first)").remove();
                    $.each(getData[0].Customer, function (index, cusm) {
                        $("#ddlCustomer").append("<option value='" + cusm.AutoId + "'>" + cusm.CustomerName + "</option>");
                    });
                    $("#ddlCustomer").select2();
                    $("#ddlSalesPerson option:not(:first)").remove();
                    $.each(getData[0].SalesPerson, function (index, item) {
                        $("#ddlSalesPerson").append("<option value='" + item.SID + "'>" + item.SP + "</option>");
                    });
                    $("#ddlSalesPerson").select2({
                        multiple: true,
                        placeholder: 'All Sales Person',
                        allowClear: true
                    });
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function GetCustomerBySalesPerson() {
    var array = $("#ddlSalesPerson").val();
    var SalesPersons = array.join(", ");
    var data = {
        SalesAutoId: SalesPersons
    };
    $.ajax({
        type: "POST",
        url: "PackedStatusReport.aspx/GetCustomerBySalesPerson",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);                   
                    $("#ddlCustomer option:not(:first)").remove();
                    $.each(getData[0].Customer, function (index, cusm) {
                        $("#ddlCustomer").append("<option value='" + cusm.AutoId + "'>" + cusm.CustomerName + "</option>");
                    });
                    $("#ddlCustomer").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function GetProfitReport(PageIndex) {
    var array = $("#ddlSalesPerson").val();
    var SalesPersons = array.join(", ");
    var array1 = $("#ddlShippingType").val();
    var shippingType = array1.join(", ");
    var data = {
        CustomerAutoId: $("#ddlCustomer").val() || 0,
        SalesAutoId: SalesPersons||0,
        ShippingAutoId: shippingType||0,
        OrderNo: $("#txtOrderNo").val().trim(),
        PageIndex: PageIndex || 1,
        PageSize: $('#ddlPageSize').val()
    };
    $.ajax({
        type: "POST",
        url: "PackedStatusReport.aspx/GetProfitReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });

        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfReport,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfReport(response) {
    var xmldoc = $.parseXML(response.d);
    var ReportDetails = $(xmldoc).find("Table1");
    var tblOverallTotal = $(xmldoc).find("Table2");
    $("#tblProfitReportDetail tbody tr").remove();
    var row = $("#tblProfitReportDetail thead tr").clone(true);
    var SubTotal = 0.00, OverAllDiscount = 0.00, PaybleAmount = 0.00, Profit = 0.00;
    if (ReportDetails.length > 0) {
        $.each(ReportDetails, function () {
            $(".PayableAmt", row).text($(this).find("PayableAmount").text());
            $(".OverallDiscount", row).text($(this).find("OverallDiscAmt").text());
            $(".CustomerId", row).text($(this).find("CustomerId").text());
            $(".CustomerName", row).text($(this).find("CustomerName").text());
            $(".OrderNo", row).text($(this).find("OrderNo").text());
            $(".SalesPerson", row).text($(this).find("SalesRep").text());
            $(".DeliveryDate", row).text($(this).find("DeliveryDate").text());
            $(".ShippingType", row).text($(this).find("ShippingType").text());
            $(".TotalProduct", row).text($(this).find("NoOfProducts").text());
            $(".OrderItems", row).text($(this).find("NoOfItems_Ordered").text());
            $(".SubTotal", row).text($(this).find("SubTotal").text());
            $(".Profit", row).text($(this).find("Profit").text());
            $("#tblProfitReportDetail tbody").append(row);
            row = $("#tblProfitReportDetail tbody tr:last").clone(true);
            SubTotal = SubTotal + parseFloat($(this).find("SubTotal").text());
            OverAllDiscount = OverAllDiscount + parseFloat($(this).find("OverallDiscAmt").text());
            PaybleAmount = PaybleAmount + parseFloat($(this).find("PayableAmount").text());
            Profit = Profit + parseFloat($(this).find("Profit").text());
        });
        $("#TSubTotal").html(parseFloat(SubTotal).toFixed(2));
        $("#TOverallDiscount").html(parseFloat(OverAllDiscount).toFixed(2));
        $("#TPayableAmt").html(parseFloat(PaybleAmount).toFixed(2));
        $("#TProfit").html(parseFloat(Profit).toFixed(2));
        $(tblOverallTotal).each(function () {
            $('#OTSubTotal').html($(this).find('SubTotal').text());
            $('#OTOverallDiscount').html(parseFloat($(this).find('OverallDiscAmt').text()).toFixed(2));
            $('#OTPayableAmt').html(parseFloat($(this).find('PayableAmount').text()).toFixed(2));
            $('#OTProfit').html(parseFloat($(this).find('Profit').text()).toFixed(2));
        });
    }
    else {
        $('#tblProfitReportDetail tbody tr').remove();
        $("#TSubTotal").html('0.00');
        $("#TOverallDiscount").html('0.00');
        $("#TPayableAmt").html('0.00');
        $("#TProfit").html('0.00');

        $("#OTSubTotal").html('0.00');
        $("#OTOverallDiscount").html('0.00');
        $("#OTPayableAmt").html('0.00');
        $("#OTProfit").html('0.00');
    }
    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
    //if (Number($("#ddlPageSize").val()) == '0') {
    //    $("#trTotal").hide();
    //}
    //else if (Number(pager.find("RecordCount").text()) < Number(pager.find("PageSize").text())) {
    //    $("#trTotal").hide();
    //}
    //else {
    //    $("#trTotal").show();
    //}
}
function searchReport() {
    GetProfitReport(1);
}
function missingReportOnChange(pageindex) {
    GetProfitReport(pageindex);
}
function Pagevalue(e) {
    GetProfitReport(parseInt($(e).attr("page")));
};
function CreateTable(us) {
    row1 = "";
    var SalesPersons = 0, shippingType = 0;
    var array = $("#ddlSalesPerson").val();
    var SalesPersons = array.join(", ");
    var array1 = $("#ddlShippingType").val();
    var shippingType = array1.join(", ");
    var data = {
        CustomerAutoId: $("#ddlCustomer").val() || 0,
        SalesAutoId: SalesPersons||0,
        ShippingAutoId: shippingType||0,
        OrderNo: $("#txtOrderNo").val().trim(),
        PageIndex: 1,
        PageSize: 0
    };
    $.ajax({
        type: "POST",
        url: "PackedStatusReport.aspx/GetProfitReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ReportDetails = $(xmldoc).find("Table1");
                    var tblPOverallTotal = $(xmldoc).find("Table2");
                    var PrintTime = $(xmldoc).find("Table3");
                    var Printingtime = ($(PrintTime).find("PrintTime").text());
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    var PSubTotal = 0.00, POverAllDiscount = 0.00, PPaybleAmount = 0.00, PProfit = 0.00;
                    if (ReportDetails.length > 0) {
                        $.each(ReportDetails, function () {
                            $(".p_PayableAmt", row).text($(this).find("PayableAmount").text());
                            $(".p_OverallDiscount", row).text($(this).find("OverallDiscAmt").text());
                            $(".p_CustomerId", row).text($(this).find("CustomerId").text());
                            $(".p_CustomerName", row).text($(this).find("CustomerName").text());
                            $(".p_OrderNo", row).text($(this).find("OrderNo").text());
                            $(".p_SalesPerson", row).text($(this).find("SalesRep").text());
                            $(".p_DeliveryDate", row).text($(this).find("DeliveryDate").text());
                            $(".p_ShippingType", row).text($(this).find("ShippingType").text());
                            $(".p_TotalProduct", row).text($(this).find("NoOfProducts").text());
                            $(".p_OrderItems", row).text($(this).find("NoOfItems_Ordered").text());
                            $(".p_SubTotal", row).text($(this).find("SubTotal").text());
                            $(".P_Profit", row).text($(this).find("Profit").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                            PSubTotal = PSubTotal + parseFloat($(this).find("SubTotal").text());
                            POverAllDiscount = POverAllDiscount + parseFloat($(this).find("OverallDiscAmt").text());
                            PPaybleAmount = PPaybleAmount + parseFloat($(this).find("PayableAmount").text());
                            PProfit = PProfit + parseFloat($(this).find("Profit").text());
                        });
                        $("#PTSubTotal").html(parseFloat(PSubTotal).toFixed(2));
                        $("#PTOverallDiscount").html(parseFloat(POverAllDiscount).toFixed(2));
                        $("#PTPayableAmt").html(parseFloat(PPaybleAmount).toFixed(2));
                        $("#PTProfit").html(parseFloat(PProfit).toFixed(2));

                    }
                    else {
                        $("#PTSubTotal").html('0.00');
                        $("#PTOverallDiscount").html('0.00');
                        $("#PTPayableAmt").html('0.00');
                        $("#PTProfit").html('0.00');

                        $('#OPOTSubTotal').html('0.00');
                        $('#OPOTOverallDiscount').html('0.00');
                        $('#OPOTPayableAmt').html('0.00');
                        $('#OPOTProfit').html('0.00');
                    }
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:8%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + " " + Printingtime);
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "Order Profitability Report " + (new Date()).format("MM/dd/yyyy hh:mm tt"),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}
/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblProfitReportDetail tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblProfitReportDetail tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}