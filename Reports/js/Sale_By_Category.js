﻿var row1 = "";
$(document).ready(function () {
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    BindCategory();

});
function Pagevalue(e) {
    getReport(parseInt($(e).attr("page")));
};

$("#ddlPageSize").change(function () {
    getReport(1);
})

function BindCategory() {
    debugger;
    $.ajax({
        type: "POST",
        url: "Sale_By_Category.aspx/BindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var DropDown = $.parseJSON(response.d);
                    for (var i = 0; i < DropDown.length; i++) {
                        var AllDropDownList = DropDown[i];
                        var Category = AllDropDownList.Category;
                        var ddlCategory = $("#ddlCategory");
                        $("#ddlCategory option:not(:first)").remove();
                        for (var k = 0; k < Category.length; k++) {
                            var CategoryDropList = Category[k];
                            var option = $("<option />");
                            option.html(CategoryDropList.CategoryName);
                            option.val(CategoryDropList.AutoId);
                            ddlCategory.append(option);
                        }
                        ddlCategory.select2();
                    }
                    
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}
function BindSubCategory() {
    $.ajax({
        type: "POST",
        url: "/Reports/Sale_By_Category.aspx/BindSubCategory",
        data: "{'CategoryAutoId':'" + $("#ddlCategory").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var DropDown = $.parseJSON(response.d);
                    for (var i = 0; i < DropDown.length; i++) {
                        var AllDropDownList = DropDown[i];
                        var SubCategory = AllDropDownList.SubCategory;
                        var ddlsubCategory = $("#ddlsubCategory");
                        $("#ddlsubCategory option:not(:first)").remove();
                        for (var a = 0; a < SubCategory.length; a++) {
                            var SubCategoryDropList = SubCategory[a];
                            var option = $("<option />");
                            option.html(SubCategoryDropList.SubcategoryName);
                            option.val(SubCategoryDropList.AutoId);
                            ddlsubCategory.append(option);
                        }
                        ddlsubCategory.select2();
                    }
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function getReport(PageIndex) {
   
    var data = {
        subCategory: $("#ddlsubCategory").val(),
        Category: $("#ddlCategory").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: PageIndex,
        PageSize: $('#ddlPageSize').val()
    };

    $.ajax({
        type: "POST",
        url: "/Reports/Sale_By_Category.aspx/GetReportDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successgetReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function successgetReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var ReportDetails = $(xmldoc).find("Table1");
            var OrderTotal = $(xmldoc).find("Table2");
            $("#tblOrderList tbody tr").remove();
            var row = $("#tblOrderList thead tr").clone(true);
            var TotalSale = 0.00;
            //var TotalNetSoldP = 0.00; TotalNetSaleRev = 0.00; TotalcostP = 0.00;
            if (ReportDetails.length > 0) {
                $("#EmptyTable").hide();
                $("#tblOrderList").show();

                $.each(ReportDetails, function (index) {
                    $(".Category", row).text($(this).find("CategoryName").text());
                    $(".subCategory", row).text($(this).find("SubcategoryName").text());
                    $(".TotalSale", row).text($(this).find("TotalSales").text());
                    //$(".FirstName", row).text($(this).find("FirstName").text());
                    //$(".ProductId", row).text($(this).find("ProductId").text());
                    //$(".ProductName", row).text($(this).find("ProductName").text());
                    //TotalSoldDefaultQty = TotalSoldDefaultQty + parseFloat($(this).find("Net_Sold_Default_Qty").text());
                    //TotalNetSoldP = TotalNetSoldP + parseFloat($(this).find("Net_Sold_Pc").text());
                    //TotalNetSaleRev = TotalNetSaleRev + parseFloat($(this).find("Net_Sale_Rev").text());
                    TotalSale = TotalSale + parseFloat($(this).find("TotalSales").text());
                   
                    //$(".Unit", row).text($(this).find("UnitType").text());
                    //$(".PcPerUnit", row).text($(this).find("Pcperunit").text());
                    //$(".CustomPrice", row).text($(this).find("CustomPrice").text());
                    //$(".Sold_Default_Qty", row).text($(this).find("Sold_Default_Qty").text());
                    //$(".Sold_Pc", row).text($(this).find("Sold_Pc").text());
                    //$(".SaleRev", row).text($(this).find("SaleRev").text());
                    //$(".Net_Sold_Default_Qty", row).text($(this).find("Net_Sold_Default_Qty").text());
                    //$(".Net_Sold_Pc", row).text($(this).find("Net_Sold_Pc").text());
                    //$(".Net_Sale_Rev", row).text($(this).find("Net_Sale_Rev").text());
                    //$(".costP", row).text(parseFloat($(this).find("costP").text()).toFixed(2));
                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);

                    //ProductId = $(this).find("ProductId").text();
                    //CustomerAutoId = $(this).find("CustomerAutoId").text();
                });
                $('#TotalSales').html(parseFloat(TotalSale).toFixed(2));
                //$('#TotalNetSoldP').html(parseFloat(TotalNetSoldP).toFixed(2));
                //$('#TotalNetSaleRev').html(parseFloat(TotalNetSaleRev).toFixed(2));
                //$('#TotalcostP').html(parseFloat(TotalcostP).toFixed(2));
            } else {
                $('#TotalSale').html('0.00');
                //$('#TotalNetSoldP').html('0.00');
                //$('#TotalNetSaleRev').html('0.00');
                //$('#TotalcostP').html('0.00');
            }

            $(OrderTotal).each(function () {
                $('#overallTotalSales').html(parseFloat($(this).find("TotalSales").text()).toFixed(2));
                //$('#T_TotalNetSoldP').html(parseFloat($(this).find("Net_Sold_Pc").text()).toFixed(2));
                //$('#T_TotalNetSaleRev').html(parseFloat($(this).find("Net_Sale_Rev").text()).toFixed(2));
                //$('#T_TotalcostP').html(parseFloat($(this).find("costP").text()).toFixed(2));
            });

            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }

}
$("#btnSearch").click(function () {
    getReport(1);
})
function CreateTable(us) {

    row1 = "";
    var image = $("#imgName").val();
    var data = {
        subCategory: $("#ddlsubCategory").val(),
        Category: $("#ddlCategory").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: 1,
        PageSize: 0
    };

    $.ajax({
        type: "POST",
        url: "/Reports/Sale_By_Category.aspx/GetReportDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var OrderTotal = $(xmldoc).find("Table");
                    $("#RptTable").empty();
                    if ($("#txtSFromDate").val() != "") {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Sale By Category</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + (new Date()).format("MM/dd/yyyy hh:mm tt") + "</span><br/><span class='DateRangeCSS' style='float:center;margin-top: 0.5%; font-size: 9px; color: black; font-weight: bold;'>Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val() + "</span><br/></div>"
                    }
                    else {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Sale By Category</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + (new Date()).format("MM/dd/yyyy hh:mm tt") + "</span></div>"
                    }

                    row1 += "<table id='RptTable' class='PrintMyTableHeader MyTableHeader'>"
                    row1 += "<thead>"
                    row1 += "<tr>"
                    row1 += "<td class='text-left'>Category</td>"
                    row1 += "<td class='text-left'>Sub Category</td>"
                    row1 += "<td class='text-right'>Total Sale</td>"
                    row1 += "</tr>"
                    row1 += "</thead>"
                    row1 += "<tbody>"
                    if (OrderTotal.length > 0) {
                        var TotalSale = 0.00; 
                        $.each(OrderTotal, function (index) {
                            row1 += "<tr>";
                            row1 += "<td class='left'>" + $(this).find("CategoryName").text() + "</td>";
                            row1 += "<td class='left'>" + $(this).find("SubcategoryName").text() + "</td>";

                            row1 += "<td class='right'>" + $(this).find("TotalSales").text() + "</td>";
                            TotalSale = TotalSale + parseFloat($(this).find("TotalSales").text());
                            row1 += "</tr>";
                        });
                        row1 += "</tbody>"
                        row1 += "<tfoot>"
                        row1 += "<tr>"
                        row1 += "<td colspan='2' class='text-right'>Total</td>"
                        row1 += "<td style='text-align:right !important'>" + parseFloat(TotalSale).toFixed(2) + "</td>"
                       
                        row1 += "</tr>"
                        row1 += "</tfoot>"
                        row1 += "</table>"
                        if (us == 1) {
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style></style>');
                            mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/app-assets/css/custom.css"/></head><body>');
                            mywindow.document.write('</head><body>');
                            mywindow.document.write(row1);
                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        }
                        if (us == 2) {
                            $("#ExcelDiv").append(row1);
                            $("#RptTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "Sale By Category",
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });

                        }
                    }

                }
                else {
                    location.href = "/";
                }
            }

        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
$("#btnExport").click(function () {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
function OpenPopUp() {
    toastr.success('Order Status - Closed.', 'Info', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
}


