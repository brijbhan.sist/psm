﻿var row1 = "";
$(document).ready(function () {
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    BindProductCategory();
    BindProductSubCategory();
    BindSalesPersonandStatus();
    BindProductByBrand();
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
});
function Pagevalue(e) {
    getReport(parseInt($(e).attr("page")));
};

function BindSalesPersonandStatus() {
    $.ajax({
        type: "POST",
        url: "SalesByBasePrice.aspx/BindCustomer",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {

                    var getData = $.parseJSON(response.d);

                    $("#ddlAllPerson option:not(:first)").remove();
                    $.each(getData[0].SalesPer, function (index, item) {
                        $("#ddlAllPerson").append("<option value='" + item.EID + "'>" + item.EN + "</option>");
                    });
                    $("#ddlAllPerson").attr('multiple', 'multiple');
                    $("#ddlAllPerson").select2();



                    $("#ddlBrand option:not(:first)").remove();
                    $.each(getData[0].Brand, function (index, item) {
                        $("#ddlBrand").append("<option value='" + item.BID + "'>" + item.BN + "</option>");
                    });
                    $("#ddlBrand").attr('multiple', 'multiple');
                    $("#ddlBrand").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}

function BindProductByBrand() {
    $("#ddlBrand option:selected").each(function (i) {
        if (i == 0) {
            BrandId = $(this).val() + ',';
        } else {
            BrandId += $(this).val() + ',';
        }
    });
    if (BrandId == "0,") {
        BrandId = ""
    }
    var data = {
        CategoryAutoId: $("#ddlAllCategory").val(),
        SubCategoryAutoId: $("#AllSubCategory").val(),
        BrandId: BrandId.toString()
    };
    $.ajax({
        type: "POST",
        url: "SalesByBasePrice.aspx/BindProductByBrand",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);
                    $("#ddlProduct option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlProduct").append("<option value='" + getData[index].PID + "'>" + getData[index].PN + "</option>");
                    });
                    $("#ddlProduct").select2();

                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}

function BindProductCategory() {
    $.ajax({
        type: "POST",
        url: "SalesByBasePrice.aspx/BindProductCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlAllCategory option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlAllCategory").append("<option value='" + getData[index].CId + "'>" + getData[index].CN + "</option>");
                    });
                    $("#ddlAllCategory").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
/*---------------------------------------------------Bind Sub Category-----------------------------------------------------------*/
function BindProductSubCategory() {
    $.ajax({
        type: "POST",
        url: "SalesByBasePrice.aspx/BindProductSubCategory",
        data: "{'CategoryAutoId':'" + $("#ddlAllCategory").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {

                    var getData = $.parseJSON(response.d);

                    $("#AllSubCategory option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#AllSubCategory").append("<option value='" + getData[index].SCID + "'>" + getData[index].SCN + "</option>");
                    });
                    $("#AllSubCategory").select2();


                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function getReport(PageIndex) {
    $("#ddlAllPerson option:selected").each(function (i) {
        if (i == 0) {
            PersonId = $(this).val() + ',';
        } else {
            PersonId += $(this).val() + ',';
        }
    });
    if (PersonId == "0,") {
        PersonId = "0"
    }
    $("#ddlBrand option:selected").each(function (i) {
        if (i == 0) {
            BrandId = $(this).val() + ',';
        } else {
            BrandId += $(this).val() + ',';
        }
    });
    if (BrandId == "0,") {
        BrandId = "0"
    }
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        SalesAutoId: PersonId.toString(),
        CategoryAutoId: $("#ddlAllCategory").val(),
        SubCategoryAutoId: $("#AllSubCategory").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        BrandId: BrandId.toString(),
        PageIndex: PageIndex,
        PageSize: $('#ddlPageSize').val()
    };

    $.ajax({
        type: "POST",
        url: "SalesByBasePrice.aspx/GetReportDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successgetReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function successgetReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var ReportDetails = $(xmldoc).find("Table");
            var OrderTotal = $(xmldoc).find("Table2");
            $("#tblOrderList tbody tr").remove();
            var row = $("#tblOrderList thead tr").clone(true);
            var TotalSoldQty = 0, TotalSales_In_This_BasePrice = 0.00;
            var Sales_At_BasePrice = 0.00; Sales_At_PriceLeavel = 0.00;
            if (ReportDetails.length > 0) {
                $("#EmptyTable").hide();
                $("#tblOrderList").show();
                var html = "", ProductTotal = 0.00, ProductId = 0, CustomerAutoId = 0;
                $.each(ReportDetails, function (index) {
                    $(".Sales", row).text($(this).find("SalesRep").text());
                    $(".ProductId", row).text($(this).find("ProductId").text());
                    $(".ProductName", row).text($(this).find("ProductName").text());
                    $(".Unit", row).text($(this).find("UnitType").text() + " [" + $(this).find("Qty").text() + " Piece]");
                    $(".QtySold", row).html($(this).find("QtySold").text() );
                    TotalSoldQty = TotalSoldQty + parseFloat($(this).find("QtySold").text());
                    TotalSales_In_This_BasePrice = TotalSales_In_This_BasePrice + parseFloat($(this).find("TotalSales_In_This_BasePrice").text());
                    Sales_At_BasePrice = Sales_At_BasePrice + parseFloat($(this).find("Sales_At_BasePrice").text());
                    Sales_At_PriceLeavel = Sales_At_PriceLeavel + parseFloat($(this).find("Sales_At_PriceLeavel").text());
                    $(".BasePrice", row).html($(this).find("BasePrice").text());
                    $(".thisBasePrice", row).html($(this).find("TotalSales_In_This_BasePrice").text());
                    $(".atBasePrice", row).html($(this).find("Sales_At_BasePrice").text());
                    $(".otherthanBasePrice", row).html($(this).find("Sales_At_PriceLeavel").text());

                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);

                    //ProductId = $(this).find("ProductId").text();
                    //CustomerAutoId = $(this).find("CustomerAutoId").text();
                });
                $('#TotalSoldQty').html(parseFloat(TotalSoldQty).toFixed(2));
                $('#TotalSales_In_This_BasePrice').html(parseFloat(TotalSales_In_This_BasePrice).toFixed(2));
                $('#Sales_At_BasePrice').html(parseFloat(Sales_At_BasePrice).toFixed(2));
                $('#Sales_At_PriceLeavel').html(parseFloat(Sales_At_PriceLeavel).toFixed(2));
            } else {
                $('#TotalSales_In_This_BasePrice').html('0.00');
                $('#Sales_At_BasePrice').html('0.00');
                $('#Sales_At_PriceLeavel').html('0.00');
                $('#TotalSoldQty').html('0');
            }

            $(OrderTotal).each(function () {
                if (OrderTotal.length > 0) {
                    $('#T_TotalSoldQty').html(parseFloat($(this).find("QtySold").text()).toFixed(2));
                    $('#T_TotalSales_In_This_BasePrice').html(parseFloat($(this).find("TotalSales_In_This_BasePrice").text()).toFixed(2));
                    $('#T_Sales_At_BasePrice').html(parseFloat($(this).find("Sales_At_BasePrice").text()).toFixed(2));
                    $('#T_Sales_At_PriceLeavel').html(parseFloat($(this).find("Sales_At_PriceLeavel").text()).toFixed(2));
                }
                else {
                    $('#T_TotalSoldQty').html('0.00');
                    $('#T_TotalSales_In_This_BasePrice').html('0.00');
                    $('#T_Sales_At_BasePrice').html('0.00');
                    $('#T_Sales_At_PriceLeavel').html('0.00');
                }
            });

            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }

}

function CreateTable(us) {
    $("#ddlAllPerson option:selected").each(function (i) {
        if (i == 0) {
            PersonId = $(this).val() + ',';
        } else {
            PersonId += $(this).val() + ',';
        }
    });
    if (PersonId == "0,") {
        PersonId = "0"
    }
    $("#ddlBrand option:selected").each(function (i) {
        if (i == 0) {
            BrandId = $(this).val() + ',';
        } else {
            BrandId += $(this).val() + ',';
        }
    });
    if (BrandId == "0,") {
        BrandId = "0"
    }
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        SalesAutoId: PersonId.toString(),
        CategoryAutoId: $("#ddlAllCategory").val(),
        SubCategoryAutoId: $("#AllSubCategory").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        BrandId: BrandId.toString(),
        PageIndex: 1,
        PageSize: 0
    };

    $.ajax({
        type: "POST",
        url: "SalesByBasePrice.aspx/GetReportDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var OrderTotal = $(xmldoc).find("Table");
                    var PrintDate = $(xmldoc).find("Table1");
                    $("#RptTable").empty();
                    if ($("#txtSFromDate").val() != "") {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Sales By Base Price</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date : " + PrintDate.find('PrintDate').text() + "</span><br/><span class='DateRangeCSS' style='float:center;margin-top: 0.5%; font-size: 9px; color: black; font-weight: bold;'>Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val() + "</span><br/></div>"
                    }
                    else {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Sales By Base Price</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date : " + PrintDate.find('PrintDate').text() + "</span></div>"
                    }

                    row1 += "<table id='RptTable' class='PrintMyTableHeader MyTableHeader'>"
                    row1 += "<thead>"
                    row1 += "<tr>"
                    row1 += "<td style='text-align:center !important;white-space:nowrap'>Sales Person</td>"
                    row1 += "<td style='text-align:center !important'>Product ID</td>"
                    row1 += "<td class='text-left'>Product Name</td>"
                    row1 += "<td class='text-left'>Sold<br> Unit Type</td>"
                    row1 += "<td  style='text-align:center !important'>Qty Sold</td>"
                    row1 += "<td class='text-center'>Base Price</td>"
                    row1 += "<td class='text-center'>Total Sales<br /> While This <br />Base Price</td>"
                    row1 += "<td class='text-center'>Total Sales <br />At<br /> Base Price</td>"
                    row1 += "<td class='text-center'>Total Sales <br />Not At<br />Base Price</td>"
                    row1 += "</tr>"
                    row1 += "</thead>"
                    row1 += "<tbody>"
                    var TotalSoldQty = 0, TotalSales_In_This_BasePrice = 0.00;
                    var Sales_At_BasePrice = 0.00; Sales_At_PriceLeavel = 0.00;
                    if (OrderTotal.length > 0) {
                        ProductTotal = 0.00, ProductId = 0, CustomerAutoId = 0;
                        $.each(OrderTotal, function (index) {
                            row1 += "<tr>";
                            row1 += "<td style='text-align:center !important'>" + $(this).find("SalesRep").text() + "</td>";
                            row1 += "<td style='text-align:center !important'>" + $(this).find("ProductId").text() + "</td>";
                            row1 += "<td class='left'>" + $(this).find("ProductName").text() + "</td>";
                            row1 += "<td class='center'>" + $(this).find("UnitType").text() + " [" + $(this).find("Qty").text() +" Piece]</td>";
                            row1 += "<td class='center'>" + $(this).find("QtySold").text() +"</td>";
                            row1 += "<td style='text-align:right !important'>" + $(this).find("BasePrice").text() + "</td>";
                            row1 += "<td style='text-align:right !important'>" + $(this).find("TotalSales_In_This_BasePrice").text() + "</td>";
                            row1 += "<td style='text-align:right !important'>" + $(this).find("Sales_At_BasePrice").text() + "</td>";
                            row1 += "<td style='text-align:right !important'>" + $(this).find("Sales_At_PriceLeavel").text() + "</td>";
                            TotalSoldQty = TotalSoldQty + parseFloat($(this).find("QtySold").text());
                            TotalSales_In_This_BasePrice = TotalSales_In_This_BasePrice + parseFloat($(this).find("TotalSales_In_This_BasePrice").text());
                            Sales_At_BasePrice = Sales_At_BasePrice + parseFloat($(this).find("Sales_At_BasePrice").text());
                            Sales_At_PriceLeavel = Sales_At_PriceLeavel + parseFloat($(this).find("Sales_At_PriceLeavel").text());

                            $('#TotalSoldQty').html(parseFloat(TotalSoldQty).toFixed(2));
                            $('#TotalSales_In_This_BasePrice').html(parseFloat(TotalSales_In_This_BasePrice).toFixed(2));
                            $('#Sales_At_BasePrice').html(parseFloat(Sales_At_BasePrice).toFixed(2));
                            $('#Sales_At_PriceLeavel').html(parseFloat(Sales_At_PriceLeavel).toFixed(2));
                            row1 += "</tr>";
                        });
                        row1 += "</tbody>"
                        row1 += "<tfoot>"
                        row1 += "<tr>"
                        row1 += "<td colspan='4'  style='text-align:center !important;font-weight:bold'>Total</td>"
                        row1 += "<td class='text-center' style='font-weight:bold'>" + parseFloat(TotalSoldQty).toFixed(2) + "</td>"
                        row1 += "<td></td>"
                        row1 += "<td style='text-align:right !important;font-weight:bold'>" + parseFloat(TotalSales_In_This_BasePrice).toFixed(2) + "</td>"
                        row1 += "<td style='text-align:right !important;font-weight:bold'>" + parseFloat(Sales_At_BasePrice).toFixed(2) + "</td>"
                        row1 += "<td style='text-align:right !important;font-weight:bold'>" + parseFloat(Sales_At_PriceLeavel).toFixed(2) + "</td>"
                        row1 += "</tr>"
                        row1 += "</tfoot>"
                        row1 += "</table>"
                        if (us == 1) {
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style></style>');
                            mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/app-assets/css/custom.css"/></head><body>');
                            mywindow.document.write('</head><body>');
                            mywindow.document.write(row1);
                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        }
                        if (us == 2) {
                            $("#ExcelDiv").append(row1);
                            $("#RptTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "SalesByBasePrice" + (new Date()).format("MM/dd/yyyy hh:mm tt"),
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });

                        }
                    }

                }
                else {
                    location.href = "/";
                }
            }

        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*---------------Print Code------------------*/
function PrintElem() {
    debugger
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function OpenPopUp() {
    toastr.success('Order Status - Closed.', 'Info', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
}


