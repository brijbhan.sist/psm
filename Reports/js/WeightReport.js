﻿$(document).ready(function () {

    $('#txtFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });

    $('#txtToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    BindBrand();
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtFromDate").val(month + '/' + day + '/' + year);
    $("#txtToDate").val(month + '/' + day + '/' + year);
})
function BindBrand() {
    $.ajax({
        type: "POST",
        url: "WeightReport.aspx/GetBrand",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            var getData = $.parseJSON(response.d);
            $("#ddlBrand option:not(:first)").remove();
            $.each(getData[0].Brand, function (index, item) {

                $("#ddlBrand").append("<option value='" + item.BID + "'>" + item.BN + "</option>");
            });
            $("#ddlBrand").attr('multiple', 'multiple');
            $("#ddlBrand").select2({
                placeholder: 'All Brand',
                allowClear: true
            });

            $("#ddlProduct option:not(:first)").remove();
            $.each(getData[0].Product, function (index, item) {

                $("#ddlProduct").append("<option value='" + item.PID + "'>" + item.PN + "</option>");
            });
            $("#ddlProduct").select2();

            $("#ddlState option:not(:first)").remove();
            $.each(getData[0].Status, function (index, item) {

                $("#ddlState").append("<option value='" + item.SID + "'>" + item.SN + "</option>");
            });
            $("#ddlState").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
function Brandchange() {
    var Brand = "0";
    $("#ddlBrand option:selected").each(function (i) {
        if (i == 0) {
            Brand = $(this).val() + ',';
        } else {
            Brand += $(this).val() + ',';
        }
    });
    if ($("#ddlBrand").val() == "0") {
        Brand = "0"
    }

    $.ajax({
        type: "POST",
        url: "WeightReport.aspx/GetProductOnChange",
        data: "{'dataValue':'" + Brand + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            var getData = $.parseJSON(response.d);
            $("#ddlProduct option:not(:first)").remove();
            $.each(getData, function (index, item) {
                $("#ddlProduct").append("<option value='" + getData[index].PCID + "'>" + getData[index].PCN + "</option>");
            });
            $("#ddlProduct").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function Pagevalue(e) {
    getRecords(parseInt($(e).attr("page")));
}
$('#btnSearch').click(function () {
    getRecords(1);
})
function getRecords(PageIndex) {
    var Brand = "0";
    $("#ddlBrand option:selected").each(function (i) {
        if (i == 0) {
            Brand = $(this).val() + ',';
        } else {
            Brand += $(this).val() + ',';
        }
    });
    if ($("#ddlBrand").val() == "0") {
        Brand = "0"
    }
    var data = {
        BrandName: Brand,
        ProductAutoId: $("#ddlProduct").val(),
        StateAutoId: $("#ddlState").val(),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "WeightReport.aspx/GetReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'false') {
                    location.href = "/Default.aspx"
                }
                else {
                    SuccessPackerResport(response);
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function SuccessPackerResport(response) {
    var totalOzSold = 0, totalOzTax = 0, OverallOzSlod = 0, OverallOztax = 0;
    var xmldoc = $.parseXML(response.d);
    var PackerResportDetail = $(xmldoc).find("Table");
    var TotalTable = $(xmldoc).find("Table2");
    $("#tblWeightReport tbody tr").remove();
    var row = $("#tblWeightReport thead tr").clone(true);
    if (PackerResportDetail.length > 0) {
        $("#EmptyTable").hide();
        $("#tblWeightReport thead tr").show();
        $.each(PackerResportDetail, function (index) {
            $(".InvoiceDate", row).html($(this).find("InvoiceDate").text());
            $(".InvoiceNo", row).html($(this).find("InvoiceNumber").text());
            $(".BrandName", row).html($(this).find("BrandName").text());
            $(".Customer", row).html($(this).find("CustomerName").text());
            $(".Address", row).html($(this).find("Address").text());
            $(".City", row).html($(this).find("City").text());
            $(".StateName", row).html($(this).find("StateName").text());
            $(".ZipCode", row).html($(this).find("Zipcode").text());
            $(".OTPLicence", row).html($(this).find("OPTLicence").text());
            $(".ProductDescription", row).html($(this).find("ProductDescription").text());
            $(".TotalOzSold", row).html($(this).find("TotalOzSold").text());
            $(".TotalOzTax", row).html($(this).find("TotalOzTax").text());
            $("#tblWeightReport tbody").append(row);
            row = $("#tblWeightReport tbody tr:last").clone(true);
            totalOzSold += parseFloat($(this).find("TotalOzSold").text());
            totalOzTax += parseFloat($(this).find("TotalOzTax").text());
        });
        $("#OZSold").html(parseFloat(totalOzSold).toFixed(2));
        $("#OZTax").html(parseFloat(totalOzTax).toFixed(2));
    } else {
        // $("#EmptyTable").show();
        $("#OZSold").html('0.00');
        $("#OZTax").html('0.00');

    }
    if (TotalTable.length > 0) {
        $("#TotalOzSolds").html($(TotalTable).find("TotalOzSolds").text());
        $("#TotalOzTaxs").html($(TotalTable).find("TotalOzTaxs").text());
    }
    var pager = $(xmldoc).find("Table1");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}

function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var Brand = "0";
    $("#ddlBrand option:selected").each(function (i) {
        if (i == 0) {
            Brand = $(this).val() + ',';
        } else {
            Brand += $(this).val() + ',';
        }
    });
    if ($("#ddlBrand").val() == "0") {
        Brand = "0"
    }
    var data = {
        BrandName: Brand,
        ProductAutoId: $("#ddlProduct").val(),
        StateAutoId: $("#ddlState").val(),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        //PageSize: $("#ddlPageSize").val(),
        // PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "WeightReport.aspx/WeightReportExport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var Report = $(xmldoc).find("Table");
                    var TotalTable = $(xmldoc).find("Table1");
                    var PrintTime = $(xmldoc).find("Table2");
                    var Printingtime = ($(PrintTime).find("PrintTime").text());
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (Report.length > 0) {
                        $("#EmptyTable").hide();
                        $("#PrintTable thead tr").show();
                        $.each(Report, function (index) {
                            $(".InvoiceDate", row).html($(this).find("InvoiceDate").text());
                            $(".InvoiceNo", row).html($(this).find("InvoiceNumber").text());
                            $(".BrandName", row).html($(this).find("BrandName").text());
                            $(".Customer", row).html($(this).find("CustomerName").text());
                            $(".Address", row).html($(this).find("Address").text());
                            $(".City", row).html($(this).find("City").text());
                            $(".StateName", row).html($(this).find("StateName").text());
                            $(".ZipCode", row).html($(this).find("Zipcode").text());
                            $(".OTPLicence", row).html($(this).find("OPTLicence").text());
                            $(".ProductDescription", row).html($(this).find("ProductDescription").text());
                            $(".TotalOzSold", row).html($(this).find("TotalOzSold").text());
                            $(".TotalOzTax", row).html($(this).find("TotalOzTax").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                        if (TotalTable.length > 0) {
                            $("#PrintTotalOzSolds").html($(TotalTable).find("PrintTotalOzSolds").text());
                            $("#PrintTotalOzTaxs").html($(TotalTable).find("PrintTotalOzTaxs").text());
                        }
                        if (us == 1) {
                            
                            var image = $("#imgName").val();
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                            $("#PrintDate").text("Print Date :" + " " + Printingtime);
                            $("#PrintLogo").attr("src", "/Img/logo/" + image);
                            if ($("#txtFromDate").val() != "") {
                                $("#DateRange").text("Date Range: " + $("#txtFromDate").val() + " To " + $("#txtToDate").val());
                            }
                            mywindow.document.write($(PrintTable1).clone().html());
                            //mywindow.document.write(divToPrint.outerHTML);

                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        }
                        if (us == 2) {
                            $("#PrintTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "Weight Tax Report(" + (new Date()).format("MM/dd/yyyy hh:mm tt") + ")",
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });
                        }
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblWeightReport tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblWeightReport tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}