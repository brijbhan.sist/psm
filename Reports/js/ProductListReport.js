﻿$(document).ready(function () {
    bindCategory();
    bindSubcategory();
});
function Pagevalue(e) {
    getProductList(parseInt($(e).attr("page")));

    if ($(e).closest(".Pager").attr('id') == 'ProductPager') {
        getProductList(parseInt($(e).attr("page")));
    }
};
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "ProductListReport.aspx/bindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getDate = $.parseJSON(response.d);

            $("#ddlCategory option:not(:first)").remove();
            $.each(getDate[0].Category, function (index, items) {
                $("#ddlCategory").append("<option value='" + items.AutoId + "'>" + items.CategoryName + "</option>");
            });
            $("#ddlCategory").select2();

            $("#ddlbrand option:not(:first)").remove();
            $.each(getDate[0].Brand, function (index, items) {
                $("#ddlbrand").append("<option value='" + items.AutoId + "'>" + items.BrandName + "</option>");
            });
            $("#ddlbrand").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*-------------------------------------------------------Bind Subcategory-----------------------------------------------------------*/
function bindSubcategory() {
    var categoryAutoId = $("#ddlCategory").val();
    $.ajax({
        type: "POST",
        url: "ProductListReport.aspx/bindSubCategory",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);

            $("#ddlSubCategory option:not(:first)").remove();
            $.each(getData, function (index, item) {
                $("#ddlSubCategory").append("<option value=' " + getData[index].AutoId + "'>" + getData[index].SubcategoryName + "</option>");
            });
            $("#ddlSubCategory").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
/*-------------------------------------------------------Get Product List----------------------------------------------------------*/
function getProductList(PageIndex) {
    if (checkRequiredField1()) {

        var data = {
            WebsiteDisplay: $("#ddlWDisplay").val(),
            ProductName: $("#txtSProductName").val().trim(),
            Category: $("#ddlCategory").val(),
            SubCategory: $("#ddlSubCategory").val(),
            Brand: $("#ddlbrand").val(),
            ProductId: $("#txtProductId").val().trim(),
            PageSize: $("#ddlPageSize").val(),
            pageIndex: PageIndex
        };
        $.ajax({
            type: "POST",
            url: "ProductListReport.aspx/getProductList",
            data: JSON.stringify({ 'dataValue': JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: successGetProductList,
            error: function (result) {
                alert(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });

    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function successGetProductList(response) {
    var xmldoc = $.parseXML(response.d);
    var productList = $(xmldoc).find("Table1");

    $("#tblProductList tbody tr").remove();

    var row = $("#tblProductList thead tr").clone(true);
    if (productList.length > 0) {
        $("#EmptyTable").hide();
        $.each(productList, function (index) {
            if ($(this).find("ShowOnWebsite").text() == 'Yes') {
                $(".Action", row).html('<div class="btn-group btn-group-toggle" data-toggle="buttons" style="border: 1px solid #8e9194;border-radius: 4px;"><label class= "btn btn-success active" style="padding: 8px;"><input type="radio" checked="checked" name="show" value="1" autocomplete="off" /><span style="color: #ffff;"> Yes</span></label ><label class="btn btn-white" style="padding: 7px;color: #000 !important;"><input type="radio" name="show" autocomplete="off" onchange="chkProduct_OnClicks(' + $(this).find("ProductId").text() + ', this);" value="0" />No</label></div>');
            }
            else {
                $(".Action", row).html('<div class="btn-group btn-group-toggle" data-toggle="buttons" style="border: 1px solid #8e9194;border-radius: 4px;"><label class="btn btn-white" style="padding: 7px;color: #000 !important"><input type="radio" value="1" name="show" onchange="chkProduct_OnClicks(' + $(this).find("ProductId").text() + ', this);" autocomplete="off"/> Yes</label ><label class="btn btn-danger active" style="padding: 8px;"><input type="radio" name="show" autocomplete="off" checked="checked" value="0"/><span style="color: #ffff;">No</span></label></div>');
            }
            $(".ProductId", row).text($(this).find("ProductId").text());
            $(".Category", row).text($(this).find("Category").text());
            $(".Subcategory", row).text($(this).find("Subcategory").text());
            $(".ProductName", row).text($(this).find("ProductName").text().replace(/&quot;/g, "\'"));
            $(".BrandName", row).text($(this).find("BrandName").text());
            if ($(this).find("ShowOnWebsite").text() == 'Yes') {
                $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("ShowOnWebsite").text() + "</span>");
            }
            else {
                $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("ShowOnWebsite").text() + "</span>");
            }
            $(".ImageUrl", row).html("<img src='" + $(this).find('ImageUrl').text() + "' OnError='this.src =\"http://psmnj.a1whm.com/Attachments/default_pic.png\"' class='zoom img img-circle img-responsive' style='width: 50px;height: 50px;'  >");


            $("#tblProductList tbody").append(row);
            row = $("#tblProductList tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }


    var pager = $(xmldoc).find("Table");
    $("#ProductPager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}
function checkRequiredField1() {
    var boolcheck = true;

    $('.ddlreq').each(function () {
        if ($(this).val().trim() == '' || $(this).val().trim() == '-Select-' || $(this).val().trim() == null) {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}

function chkProduct_OnClick(ProductId, e) {
    var check = "";
    debugger;
    var tr = $(e).closest('tr');   
    check = tr.find('.Action input[name=show]:checked').val();
    var data = {
        ProductId: ProductId,
        ShowOnWebsite: check,
    };
    $.ajax({
        type: "POST",
        url: "ProductListReport.aspx/UpdateWebsiteStatus",
        data: JSON.stringify({ 'dataValue': JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            if (result.d == "Session Expired") {
                window.location = "/";
            }
            else if (result.d == 'Success') {
               // swal("", "Product Updated successfully.", "success").then(function () {
                    getProductList(1);
                //});

            } else {
                swal("Error!", result.d, "error");
            }
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later", "error");

        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later", "error");
        }
    })
}

function chkProduct_OnClicks(ProductId, e) {
    chkProduct_OnClick(ProductId, e);
    //var msg = "You want to Show on website.";
    //if ($(e).val() == '0') {
    //    msg = "You want to hide from website.";
    //}
    //swal({
    //    title: "Are you sure?",
    //    text: msg,
    //    icon: "warning",
    //    showCancelButton: true,
    //    buttons: {
    //        cancel: {
    //            text: "No, cancel.",
    //            value: null,
    //            visible: true,
    //            className: "btn-warning",
    //            closeModal: true,
    //        },
    //        confirm: {
    //            text: "Yes, Change it.",
    //            value: true,
    //            visible: true,
    //            className: "",
    //            closeModal: false
    //        }
    //    }
    //}).then(isConfirm => {
    //    if (isConfirm) {
           

    //    } 
    //})
}
