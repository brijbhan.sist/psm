﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    BindSalesPerson();
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);

});

function BindSalesPerson() {
    $.ajax({
        type: "POST",
        url: "SalesReportByBrand.aspx/BindSalesPerson",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(getData[0].EmployeeList, function (index, item) {
                    $("#ddlSalesPerson").append("<option value='" + item.AutoId + "'>" + item.EmpName + "</option>");
                });
                $("#ddlSalesPerson").attr('multiple', 'multiple')
                $("#ddlSalesPerson").select2({
                    placeholder: 'All Sales Person',
                    allowClear: true
                });
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
function Pagevalue(e) {
    getReport(parseInt($(e).attr("page")));
};
function getReport() {
    var SalesPerson = '0';
    $("#ddlSalesPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPerson = $(this).val() + ',';
        } else {
            SalesPerson += $(this).val() + ',';
        }
    });
    var data = {
        SalesPerson: SalesPerson.toString(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val()
    };
    $.ajax({
        type: "POST",
        url: "SalesReportByBrand.aspx/GetReportDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var SalesList = $(xmldoc).find("Table");
                if (SalesList.length > 0) {
                    $("#EmptyTable").hide();
                    $("#tblProductSalesByBrand tbody tr").remove();
                    var row = $("#tblProductSalesByBrand thead tr:last-child").clone();
                    var netTotal = 0,  Flair = 0, Hemping = 0, Herbsens = 0, Juul = 0, Reclex = 0, SpecialReserve = 0, Startek = 0, GM = 0;
                    $.each(SalesList, function () {
                        Flair = Flair + parseFloat($(this).find("Flair").text());
                        Hemping = Hemping + parseFloat($(this).find("Hemping").text());
                        Herbsens = Herbsens + parseFloat($(this).find("Herbsens").text());
                        Juul = Juul + parseFloat($(this).find("Juul").text());
                        Reclex = Reclex + parseFloat($(this).find("Reclex").text());
                        SpecialReserve = SpecialReserve + parseFloat($(this).find("SpecialReserve").text());
                        Startek = Startek + parseFloat($(this).find("Startek").text());
                        GM = GM + parseFloat($(this).find("GM").text());
                        netTotal = netTotal + parseFloat($(this).find("NetPrice").text());

                        $(".Sales", row).text($(this).find("SalesPerson").text());
                        $(".flairRevenue", row).text($(this).find("Flair").text());
                        $(".flairPer", row).text($(this).find("FlairPer").text());

                        $(".hempRevenue", row).text($(this).find("Hemping").text());
                        $(".hempPer", row).text($(this).find("HempingPer").text());

                        $(".herbRevenue", row).text($(this).find("Herbsens").text());
                        $(".herbPer", row).text($(this).find("HerbsensgPer").text());

                        $(".juulRevenue", row).text($(this).find("Juul").text());
                        $(".juulPer", row).text($(this).find("JuulPer").text());

                        $(".recRevenue", row).text($(this).find("Reclex").text());
                        $(".recPer", row).text($(this).find("ReclexPer").text());

                        $(".specialRevenue", row).text($(this).find("SpecialReserve").text());
                        $(".specialPer", row).text($(this).find("SpecialReservePer").text());

                        $(".startekRevenue", row).text($(this).find("Startek").text());
                        $(".startekPer", row).text($(this).find("StartekPer").text());

                        $(".gmRevenue", row).text($(this).find("GM").text());
                        $(".gmPer", row).text($(this).find("GMPer").text());

                        $(".TotalSales", row).text($(this).find("NetPrice").text());
                        $("#tblProductSalesByBrand tbody").append(row);
                        row = $("#tblProductSalesByBrand tbody tr:last-child").clone();
                    });
                    $("#tblProductSalesByBrand tbody tr").show();

                    $("#flairRevenue").html(parseFloat(Flair).toFixed(2));
                    $("#hempRevenue").html(parseFloat(Hemping).toFixed(2));
                    $("#herbRevenue").html(parseFloat(Herbsens).toFixed(2));
                    $("#juulRevenue").html(parseFloat(Juul).toFixed(2));
                    $("#recRevenue").html(parseFloat(Reclex).toFixed(2));
                    $("#specialRevenue").html(parseFloat(SpecialReserve).toFixed(2));
                    $("#startekRevenue").html(parseFloat(Startek).toFixed(2));
                    $("#gmRevenue").html(parseFloat(GM).toFixed(2));


                    $("#flairPer").html(parseFloat(Flair / netTotal * 100).toFixed(2)+'%');
                    $("#hempPer").html(parseFloat(Hemping / netTotal * 100).toFixed(2) + '%');
                    $("#herbPer").html(parseFloat(Herbsens / netTotal * 100).toFixed(2) + '%');
                    $("#juulPer").html(parseFloat(Juul / netTotal * 100).toFixed(2) + '%');
                    $("#recPer").html(parseFloat(Reclex / netTotal * 100).toFixed(2) + '%');
                    $("#specialPer").html(parseFloat(SpecialReserve / netTotal * 100).toFixed(2) + '%');
                    $("#startekPer").html(parseFloat(Startek / netTotal * 100).toFixed(2) + '%');
                    $("#gmPer").html(parseFloat(GM / netTotal * 100).toFixed(2) + '%');
                    $("#TotalSales").html(parseFloat(netTotal).toFixed(2));
                } else {
                    $("#EmptyTable").show();
                    $("#tblProductSalesByBrand tbody tr").remove();
                    $("#flairRevenue").html('0.00');
                    $("#hempRevenue").html('0.00'); 
                    $("#herbRevenue").html('0.00');
                    $("#juulRevenue").html('0.00');
                    $("#recRevenue").html('0.00');
                    $("#specialRevenue").html('0.00');
                    $("#startekRevenue").html('0.00');
                    $("#gmRevenue").html('0.00');


                    $("#flairPer").html('0.00');
                    $("#hempPer").html('0.00');
                    $("#herbPer").html('0.00');
                    $("#juulPer").html('0.00');
                    $("#recPer").html('0.00');
                    $("#specialPer").html('0.00');
                    $("#startekPer").html('0.00');
                    $("#gmPer").html('0.00');
                    $("#TotalSales").html('0.00');
                }
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function CreateTable(us) {
    row1 = "";
    var SalesPerson = '0';
    var image = $("#imgName").val();
    $("#ddlSalesPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPerson = $(this).val() + ',';
        } else {
            SalesPerson += $(this).val() + ',';
        }
    });
    var data = {
        SalesPerson: SalesPerson.toString(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val()
    };
    $.ajax({
        type: "POST",
        url: "SalesReportByBrand.aspx/GetReportDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var SalesList = $(xmldoc).find("Table");
                    var PrintDate = $(xmldoc).find("Table1");
                    debugger;
                    if ($("#txtFromDate").val() != "") {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Sales By Brand</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + PrintDate.find('PrintDate').text() + "</span><br/><span class='DateRangeCSS' style='float:center;margin-top: 0.5%; font-size: 9px; color: black; font-weight: bold;'>Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val() + "</span><br/></div>"
                    }
                    else {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Sales By Brand</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + PrintDate.find('PrintDate').text() + "</span></div>"
                    }
                    row1 += "<table class='PrintMyTableHeader MyTableHeader' id='RptTable'>"
                    row1 += "<thead  class='bg-blue white'>"
                    row1 += " <tr>"
                    row1 += '<td class="pSales tblwth text-center" rowspan="2" valign="middle" >Sales Person</td>'
                    row1 += '<td class="pFlair tblwth text-center" colspan="2">Flair</td>'
                    row1 +=' <td class="pHemping tblwth text-center" colspan="2">Hemping</td>'
                    row1 += '<td class="pHerbsens tblwth text-center" colspan="2">Herbsens</td>'
                    row1 +=' <td class="pJuul tblwth text-center" colspan="2">Juul</td>'
                    row1 += '<td class="pReclex tblwth text-center" colspan="2">Reclex</td>'
                    row1 += '<td class="pSpecial tblwth text-center" colspan="2">Special Reserve</td>'
                    row1 += '<td class="pStartek tblwth text-center" colspan="2">Startek</td>'
                    row1 +=' <td class="pTotalSales tblwth text-center" colspan="2">GM</td>'
                    row1 += '<td class="pTotalSales tblwth text-center" rowspan="2" valign="middle" >Total Sales</td>'
                    row1 += ' </tr >'
                    row1 += ' <tr>'
                    row1 += ' <td class="pflairRevenue">Revenue</td>'
                    row1 += ' <td class="pflairPer">%</td>'
                    row1 += ' <td class="phempRevenue">Revenue</td>'
                    row1 += ' <td class="phempPer">%</td>'
                    row1 += ' <td class="pherbRevenue">Revenue</td>'
                    row1 += ' <td class="pherbPer">%</td>'
                    row1 += ' <td class="pjuulRevenue">Revenue</td>'
                    row1 += ' <td class="pjuulPer">%</td>'
                    row1 += ' <td class="precRevenue">Revenue</td>'
                    row1 += '  <td class="precPer">%</td>'
                    row1 += '  <td class="pspecialRevenue">Revenue</td>'
                    row1 += '  <td class="pspecialPer">%</td>'
                    row1 += ' <td class="pstartekRevenue">Revenue</td>'
                    row1 += '  <td class="pstartekPer">%</td>'
                    row1 += ' <td class="pgmRevenue">Revenue</td>'
                    row1 += '  <td class="pgmPer">%</td>'
                    row1 += ' </tr>'
                    if (us == 1) {
                        row1 += ' <tr style="display: none">'
                        row1 += ' <td class="Sales tblwth">Sales Person</td>'
                        row1 += '  <td class="pflairRevenue">Revenue</td>'
                        row1 += ' <td class="pflairPer">%</td>'
                        row1 += ' <td class="phempRevenue">Revenue</td>'
                        row1 += ' <td class="phempPer">%</td>'
                        row1 += ' <td class="pherbRevenue">Revenue</td>'
                        row1 += '<td class="pherbPer">%</td>'
                        row1 += ' <td class="pjuulRevenue">Revenue</td>'
                        row1 += ' <td class="pjuulPer">%</td>'
                        row1 += ' <td class="precRevenue">Revenue</td>'
                        row1 += '  <td class="precPer">%</td>'
                        row1 += ' <td class="pspecialRevenue">Revenue</td>'
                        row1 += '  <td class="pspecialPer">%</td>'
                        row1 += ' <td class="pstartekRevenue">Revenue</td>'
                        row1 += '  <td class="pstartekPer">%</td>'
                        row1 += ' <td class="pgmRevenue">Revenue</td>'
                        row1 += ' <td class="pgmPer">%</td>'
                        row1 += '<td class="pTotalSales tblwth">Total Sales</td>'
                        row1 += ' </tr>'
                    }
                    row1 += "</thead>"
                    row1 += "<tbody>"
                    if (SalesList.length > 0) {
                            var netTotal = 0, Flair = 0, Hemping = 0, Herbsens = 0, Juul = 0, Reclex = 0, SpecialReserve = 0, Startek = 0, GM = 0;
                        $.each(SalesList, function (index) {

                            Flair = Flair + parseFloat($(this).find("Flair").text());
                            Hemping = Hemping + parseFloat($(this).find("Hemping").text());
                            Herbsens = Herbsens + parseFloat($(this).find("Herbsens").text());
                            Juul = Juul + parseFloat($(this).find("Juul").text());
                            Reclex = Reclex + parseFloat($(this).find("Reclex").text());
                            SpecialReserve = SpecialReserve + parseFloat($(this).find("SpecialReserve").text());
                            Startek = Startek + parseFloat($(this).find("Startek").text());
                            GM = GM + parseFloat($(this).find("GM").text());
                            netTotal = netTotal + parseFloat($(this).find("NetPrice").text());

                            row1 += ' <tr>'
                            row1 += ' <td class="Sales tblwth left">' + $(this).find("SalesPerson").text()+'</td>'
                            row1 += '  <td class="pflairRevenue right">' + $(this).find("Flair").text() +'</td>'
                            row1 += ' <td class="pflairPer right">' + $(this).find("FlairPer").text()  +'</td>'
                            row1 += ' <td class="phempRevenue right">' + $(this).find("Hemping").text() +'</td>'
                            row1 += ' <td class="phempPer right">' + $(this).find("HempingPer").text()  +'</td>'
                            row1 += ' <td class="pherbRevenue right">' + $(this).find("Herbsens").text() +'</td>'
                            row1 += '<td class="pherbPer right">' + $(this).find("HerbsensgPer").text() +'</td>'
                            row1 += ' <td class="pjuulRevenue right">' + $(this).find("Juul").text() +'</td>'
                            row1 += ' <td class="pjuulPer right">' + $(this).find("JuulPer").text() +'</td>'
                            row1 += ' <td class="precRevenue right">' + $(this).find("Reclex").text() +'</td>'
                            row1 += '  <td class="precPer right">' + $(this).find("ReclexPer").text() +'</td>'
                            row1 += ' <td class="pspecialRevenue right">' + $(this).find("SpecialReserve").text() +'</td>'
                            row1 += '  <td class="pspecialPer right">' + $(this).find("SpecialReservePer").text()  +'</td>'
                            row1 += ' <td class="pstartekRevenue right">' + $(this).find("Startek").text() +'</td>'
                            row1 += '  <td class="pstartekPer right">' + $(this).find("StartekPer").text()+'</td>'
                            row1 += ' <td class="pgmRevenue right">' + $(this).find("GM").text() +'</td>'
                            row1 += ' <td class="pgmPer right">' + $(this).find("GMPer").text() +'</td>'
                            row1 += '<td class="pTotalSales tblwth right">' + $(this).find("NetPrice").text() +'</td>'
                            row1 += ' </tr>'
                        });
                        row1 += "</tbody>"
                        row1 += "<tfoot>"
                        row1 += '<tr style="font-weight:bold;">';
                        row1 += '<td style="font-weight:bold;white-space: nowrap">Total</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(Flair).toFixed(2) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(Flair / netTotal * 100).toFixed(2) + '%</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(Hemping).toFixed(2) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(Hemping / netTotal * 100).toFixed(2) + '%</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(Herbsens).toFixed(2) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(Herbsens / netTotal * 100).toFixed(2) + '%</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(Juul).toFixed(2) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;font-weight:bold;white-space: nowrap">' + parseFloat(Juul / netTotal * 100).toFixed(2) + '%</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(Reclex).toFixed(2) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(Reclex / netTotal * 100).toFixed(2) + '%</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(SpecialReserve).toFixed(2) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(SpecialReserve / netTotal * 100).toFixed(2) + '%</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(Startek).toFixed(2) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(Startek / netTotal * 100).toFixed(2) + '%</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(GM).toFixed(2) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(GM / netTotal * 100).toFixed(2) + '%</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(netTotal).toFixed(2) + '</td>';
                        row1 += "</tfoot>"
                        row1 += "</table>"
                        if (us == 1) {
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style>#RptTable {border-collapse: collapse;width: 100%;}#RptTable td, #RptTable th {border: 1px solid black;}#RptTable tr:nth-child(even){background-color: #f2f2f2;}#RptTable thead {padding-top: 12px;padding-bottom: 12px;text-align:center;background-color:#e9e8e8 !important;color:black;}.text-right{text-align:right;}.text-left{text-align:left;}.text-center{text-align:center;}</style>');
                            mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                            mywindow.document.write('</head><body>');
                            mywindow.document.write(row1);
                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        }
                        if (us == 2) {
                            $("#ExcelDiv").html(row1);
                            $("#RptTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "SalesReportByBrand",
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });
                        }
                    }

                }
                else {
                    location.href = "/";
                }
            }

        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblProductSalesByBrand tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblProductSalesByBrand tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}