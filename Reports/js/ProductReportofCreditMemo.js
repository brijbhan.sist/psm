﻿$(document).ready(function () {
    BindDropdownlist();
    BindCustomer();
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',

        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtFromDate").val(month + '/' + day + '/' + year);
    $("#txtToDate").val(month + '/' + day + '/' + year);

});
function BindDropdownlist() {
    $.ajax({
        type: "POST",
        url: "WebAPI/WProductReportofCreditMemo.asmx/BindDropdownlist",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var data = $.parseJSON(response.d);              

                    $("#ddlSalesPerson option:not(:first)").remove();
                    $.each(data, function (index,item) {
                        $("#ddlSalesPerson").append("<option value='" + data[index].AutoId + "'>" + data[index].SalesPerson + "</option>");
                    });
                    $("#ddlSalesPerson").select2();


                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
}
$("#ddlPageSize").change(function () {
    BindReport(1);
})
$("#btnSearch").click(function () {
    BindReport(1);
});
$("#btnSearch").click(function () {
    BindReport(1);
})
function BindReport(PageIndex) {
    var data = {
        SalesPerson: $("#ddlSalesPerson").val(),
        Customer: $("#ddlCustomer").val(),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/WProductReportofCreditMemo.asmx/ProductReportofCreditMemo",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'false') {
                    location.href = "/Default.aspx"
                }
                else {
                    SuccessPackerResport(response);
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function SuccessPackerResport(response) {
    var xmldoc = $.parseXML(response.d);
    var NetAmount = 0.00, TotalAcceptedQty=0, OverAllAmount = 0.00;
    var PackerResportDetail = $(xmldoc).find("Table1");
    var OverAllAmt = $(xmldoc).find("Table2");
    $("#tblProductReportofCreditMemo tbody tr").remove();
    var row = $("#tblProductReportofCreditMemo thead tr").clone(true);
    if (PackerResportDetail.length > 0) {
        $("#EmptyTable").hide();
        $("#tblProductReportofCreditMemo thead tr").show();
        $.each(PackerResportDetail, function (index) {
            $(".CustomerId", row).text($(this).find("CustomerId").text());
            $(".CustomerName", row).text($(this).find("CustomerName").text());
            $(".CreditNo", row).text($(this).find("CreditNo").text());
            $(".CreditDate", row).text($(this).find("CreditDate").text());
            $(".SalesPerson", row).text($(this).find("SalesPerson").text());
            if (($(this).find("AppliedOrder").text()) == "Not Applied") {
                $(".AppliedOrder", row).html('<span class="badge badge badge-pill badge-danger">' + $(this).find("AppliedOrder").text() + '</span>');
            } else {
                $(".AppliedOrder", row).html('<span class="badge badge badge-pill badge-primary">' + $(this).find("AppliedOrder").text() + '</span>');
            }
            $(".ProductId", row).text($(this).find("ProductId").text());
            $(".ProductName", row).text($(this).find("ProductName").text());
            $(".Unit", row).text($(this).find("UnitType").text());
            $(".AcceptedQty", row).text(parseInt($(this).find("AcceptedQty").text()).toFixed(0));
            $(".NetAmount", row).text(parseFloat($(this).find("NetAmount").text()).toFixed(2));
            $("#tblProductReportofCreditMemo tbody").append(row);
            row = $("#tblProductReportofCreditMemo tbody tr:last").clone(true);          

            TotalAcceptedQty = TotalAcceptedQty + parseInt($(this).find("AcceptedQty").text());
            NetAmount = NetAmount + parseFloat($(this).find("NetAmount").text());
           
        });
        $('#TotalNetAmount').html(parseFloat(NetAmount).toFixed(2)); 
        $('#TotalAcceptedQty').html(parseInt(TotalAcceptedQty).toFixed(0)); 

        $('#OverAllNetAmount').html($(OverAllAmt).find("OverAllNetAmount").text());
        $('#OverAllAcceptedQty').html($(OverAllAmt).find("OverAllAcceptedQty").text());
    } else {
       // $("#EmptyTable").show();
        $('#TotalNetAmount').html('0.00');
        $('#TotalAcceptedQty').html('0');
        $('#OverAllNetAmount').html('0.00');
        $('#OverAllAcceptedQty').html('0');
    }

    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}

function CreateTable(us) {
   
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        SalesPerson: $("#ddlSalesPerson").val() || 0,
        Customer: $("#ddlCustomer").val() || 0,
        DriverAutoId: $("#ddlDriver").val() || 0,
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        Status: $("#ddlStatus").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/WProductReportofCreditMemo.asmx/ProductReportofCreditMemo",  
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var NetAmountPrint = 0.00, TotalAcceptedQtyPrint = 0;
                    var xmldoc = $.parseXML(response.d);
                    var PrintDate = $(xmldoc).find("Table");
                    var PackerResportDetail = $(xmldoc).find("Table1");                   
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (PackerResportDetail.length > 0) {
                        $.each(PackerResportDetail, function () {

                            $(".P_CustomerId", row).text($(this).find("CustomerId").text());
                            $(".P_CustomerName", row).text($(this).find("CustomerName").text());
                            $(".P_CreditNo", row).text($(this).find("CreditNo").text());
                            $(".P_CreditDate", row).text($(this).find("CreditDate").text());
                            $(".P_SalesPerson", row).text($(this).find("SalesPerson").text());
                            if (($(this).find("AppliedOrder").text()) == "Not Applied") {
                                $(".P_AppliedOrder", row).html('<span class="badge badge badge-pill badge-danger">' + $(this).find("AppliedOrder").text() + '</span>');
                            } else {
                                $(".P_AppliedOrder", row).html('<span class="badge badge badge-pill badge-primary">' + $(this).find("AppliedOrder").text() + '</span>');
                            }
                            $(".P_ProductId", row).text($(this).find("ProductId").text());
                            $(".P_ProductName", row).text($(this).find("ProductName").text());
                            $(".P_Unit", row).text($(this).find("UnitType").text());
                            $(".P_AcceptedQty", row).text(parseInt($(this).find("AcceptedQty").text()).toFixed(0));
                            $(".P_NetAmount", row).text(parseFloat($(this).find("NetAmount").text()).toFixed(2));
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);  

                            TotalAcceptedQtyPrint = TotalAcceptedQtyPrint + parseInt($(this).find("AcceptedQty").text());
                            NetAmountPrint = NetAmountPrint + parseFloat($(this).find("NetAmount").text());
                        });     
                        $('#OverAllNetAmountPrint').html(parseFloat(NetAmountPrint).toFixed(2));     
                        $('#OverAllAcceptedQtyPrint').html(parseInt(TotalAcceptedQtyPrint).toFixed(0)); 
                    }
                    if (us == 1) {
                       
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtFromDate").val() + " To " + $("#txtToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "CreditMemoByProduct",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblProductReportofCreditMemo tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
$("#btnExport").click(function () {
    if ($('#tblProductReportofCreditMemo tbody tr').length > 0) {
    CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});

//----------------------------------Bind Customer---------------------------------------------
function BindCustomer() {

    $.ajax({
        type: "POST",
        url: "WebAPI/WProductReportofCreditMemo.asmx//BindCustomer",
        data: "{'SalesPersonAutoId':'" + $('#ddlSalesPerson').val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var data = $.parseJSON(response.d);
                    debugger;
                    $("#ddlCustomer option:not(:first)").remove();
                    $.each(data, function (index, item) {

                        $("#ddlCustomer").append("<option value='" + data[index].CustomerAutoId + "'>" + data[index].CustomerName + "</option>");
                    });
                    $("#ddlCustomer").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}


