﻿$(document).ready(function () {
    $('.date').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    bindCategory();
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
});

function setdatevalidation(val) {
    var fdate = $("#txtSFromDate").val();
    var tdate = $("#txtSToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromDate").val(tdate);
        }
    }
}
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "NotShippedReport.aspx/BindStatussalesperson",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);
                    $("#ddlStatus option:not(:first)").remove();
                    $.each(getData[0].Status, function (index, item) {
                        $("#ddlStatus").append("<option value='" + item.STID + "'>" + item.ST + "</option>");
                    });
                    $("#ddlStatus").select2();
                    $("#ddlSalesPerson option:not(:first)").remove();
                    $.each(getData[0].SalesPerson, function (index, item) {
                        $("#ddlSalesPerson").append("<option value='" + item.SPID + "'>" + item.SPN + "</option>");
                    });
                    $("#ddlSalesPerson").select2({
                        placeholder: 'All Sales Person',
                    });
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*-------------------------------------------------------Get Not Shipped Report----------------------------------------------------------*/
function getNotShippedReport(PageIndex) {
    var data = {
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlStatus").val(),
        SalespersonAutoId: $("#ddlSalesPerson").val(),
        PageSize: $("#ddlPagesize").val(),
        pageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "NotShippedReport.aspx/getNotShippedReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: successNotShippedReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successNotShippedReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var NotshippedReport = $(xmldoc).find("Table1");
            var OrderTotal = $(xmldoc).find("Table2");
            $("#tblNotShippedReport tbody tr").remove();
            var row = $("#tblNotShippedReport thead tr").clone(true);
            var orderqty = 0; Shippedqty = 0; notshippedqty = 0; notshippedamt = 0;
            if (NotshippedReport.length > 0) {
                $("#EmptyTable").hide();
                $.each(NotshippedReport, function () {
                    $(".Location", row).text($(this).find("Location").text());
                    $(".CustomerId", row).text($(this).find("CustomerId").text());
                    $(".CustomerName", row).text($(this).find("CustomerName").text());
                    $(".Orderno", row).text($(this).find("OrderNo").text());
                    $(".OrderDate", row).text($(this).find("OrderDate").text());
                    $(".PackingDate", row).text($(this).find("PackingDate").text());
                    $(".ProductId", row).text($(this).find("ProductId").text());
                    $(".ProductName", row).text($(this).find("ProductName").text());
                    $(".Unit", row).text($(this).find("unit").text());
                    $(".RequiredQty", row).text($(this).find("RequiredQty").text());
                    $(".QtyShip", row).text($(this).find("QtyShip").text());
                    $(".RemainQty", row).text($(this).find("RemainQty").text());
                    $(".Amount", row).text($(this).find("Amount").text());
                    $("#tblNotShippedReport tbody").append(row);
                    row = $("#tblNotShippedReport tbody tr:last").clone(true);

                    orderqty = orderqty + parseInt($(this).find("RequiredQty").text());
                    Shippedqty = Shippedqty + parseInt($(this).find("QtyShip").text());
                    notshippedqty = notshippedqty + parseInt($(this).find("RemainQty").text());
                    notshippedamt = notshippedamt + parseFloat($(this).find("Amount").text());

                });
                $('#notshipamt').html(notshippedamt.toFixed(2));
                $('#nitshipqty').html(notshippedqty);
                $('#shipqty').html(Shippedqty);
                $('#ordqty').html(orderqty);
            } else {
                //  $("#EmptyTable").show();
                $('#notshipamt').html('0.00');
                $('#nitshipqty').html('0');
                $('#shipqty').html('0');
                $('#ordqty').html('0');
            }

            $(OrderTotal).each(function () {
                $('#notshipamts').html(parseFloat($(this).find('Amount').text()).toFixed(2));
                $('#nitshipqtys').html(parseInt($(this).find('RemainQty').text()));
                $('#shipqtys').html(parseInt($(this).find('QtyShip').text()));
                $('#ordqtys').html(parseInt($(this).find('RequiredQty').text()));
            });

            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });

            //if (Number($("#ddlPagesize").val()) == '0') {
            //    $("#trOTotal").hide();
            //}
            //else if (Number(pager.find("RecordCount").text()) < Number(pager.find("PageSize").text())) {
            //    $("#trOTotal").hide();
            //}
            //else {
            //    $("#trOTotal").show();
            //}
        }
    }
    else {
        location.href = "/";
    }
}


function Pagevalue(e) {
    getNotShippedReport(parseInt($(e).attr("page")));
};
function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        Status: $("#ddlStatus").val(),
        SalespersonAutoId: $("#ddlSalesPerson").val(),
        PageSize: 0,
        pageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "NotShippedReport.aspx/getNotShippedReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var PrintDate = $(xmldoc).find("Table");
                    var NotshippedReport = $(xmldoc).find("Table1");
                    var OrderTotal = $(xmldoc).find("Table2");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (NotshippedReport.length > 0) {
                        $.each(NotshippedReport, function () {
                            $(".P_Location", row).text($(this).find("Location").text());
                            $(".P_CustomerId", row).text($(this).find("CustomerId").text());
                            $(".P_CustomerName", row).text($(this).find("CustomerName").text());
                            $(".P_Orderno", row).text($(this).find("OrderNo").text());
                            $(".P_OrderDate", row).text($(this).find("OrderDate").text());
                            $(".P_PackingDate", row).text($(this).find("PackingDate").text());
                            $(".P_ProductId", row).text($(this).find("ProductId").text());
                            $(".P_ProductName", row).text($(this).find("ProductName").text());
                            $(".P_Unit", row).text($(this).find("unit").text());
                            $(".P_RequiredQty", row).text($(this).find("RequiredQty").text());
                            $(".P_QtyShip", row).text($(this).find("QtyShip").text());
                            $(".P_RemainQty", row).text($(this).find("RemainQty").text());
                            $(".P_Amount", row).text($(this).find("Amount").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                    }
                    $(OrderTotal).each(function () {
                        $('#notshipamtss').html(parseFloat($(this).find('Amount').text()).toFixed(2));
                        $('#nitshipqtyss').html(parseInt($(this).find('RemainQty').text()));
                        $('#shipqtyss').html(parseInt($(this).find('QtyShip').text()));
                        $('#ordqtyss').html(parseInt($(this).find('RequiredQty').text()));
                    });
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtSFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                        }
                        mywindow.document.write($(PrintTable).clone().html());
                        mywindow.document.write('</body></html>');
                        try {
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        } catch (e) {
                            setTimeout(function () {
                                mywindow.print();
                            }, 10000);
                        }

                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "NotShippedReportByOrder" + (new Date()).format("MM/dd/yyyy hh:mm tt"),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblNotShippedReport tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblNotShippedReport tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

