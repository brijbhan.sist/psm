﻿$(document).ready(function () {
    BindDropdownlist();
    BindCustomer();
    $('.date').pickadate({
        min: new Date('01/01/2019'),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtFromDate").val(month + '/' + day + '/' + year);
    $("#txtToDate").val(month + '/' + day + '/' + year);
});

function setdatevalidation(val) {
    var fdate = $("#txtFromDate").val();
    var tdate = $("#txtToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtFromDate").val(tdate);
        }
    }
}
function BindDropdownlist() {
    $.ajax({
        type: "POST",
        url: "ProductFreeReport.aspx/BindDropdownlist",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var Customer = $(xmldoc).find("Table");
                    var StatusType = $(xmldoc).find("Table2");
                    var SalesPerson = $(xmldoc).find("Table1");
                    var Category = $(xmldoc).find("Table3");

                    $("#ddlCategory option:not(:first)").remove();
                    $.each(Category, function () {
                        $("#ddlCategory").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CategoryName").text() + "</option>");
                    });
                    $("#ddlCategory").select2();


                    $("#ddlSalesPerson option:not(:first)").remove();
                    $.each(SalesPerson, function () {
                        $("#ddlSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("SalesPerson").text() + "</option>");
                    });
                    $("#ddlSalesPerson").select2();


                    $("#ddlStatus option:not(:first)").remove();
                    $.each(StatusType, function () {
                        $("#ddlStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                    });
                    $("#ddlStatus").select2();

                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
}
$("#ddlPageSize").change(function () {
    BindReport(1);
})
$("#btnSearch").click(function () {
    BindReport(1);
});

$("#btnSearch").click(function () {
    BindReport(1);
})
function BindReport(PageIndex) {
    var data = {
        SalesPerson: $("#ddlSalesPerson").val(),
        Customer: $("#ddlCustomer").val(),
        CategoryAutoId: $("#ddlCategory").val(),
        SubCategoryAutoId: $("#ddlSubcategory").val(),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        Status: $("#ddlStatus").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "ProductFreeReport.aspx/ProductFreeReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'false') {
                    location.href = "/Default.aspx"
                }
                else {
                    SuccessPackerResport(response);
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function SuccessPackerResport(response) {
    var xmldoc = $.parseXML(response.d);
    var Amount = 0.00, OverAllAmount = 0.00;
    var PackerResportDetail = $(xmldoc).find("Table1");
    var OverAllAmt = $(xmldoc).find("Table2");
    $("#tblProductExchangeReport tbody tr").remove();
    var row = $("#tblProductExchangeReport thead tr").clone(true);
    if (PackerResportDetail.length > 0) {
        $("#EmptyTable").hide();
        $("#tblProductExchangeReport thead tr").show();
        $.each(PackerResportDetail, function (index) {
            $(".CustomerId", row).text($(this).find("CustomerId").text());
            $(".CustomerName", row).text($(this).find("CustomerName").text());
            $(".OrderNo", row).text($(this).find("OrderNo").text());
            $(".OrderDate", row).text($(this).find("OrderDate").text());
            $(".SalesPerson", row).text($(this).find("SalesPerson").text());
            $(".ProductId", row).text($(this).find("ProductId").text());
            $(".ProductName", row).text($(this).find("ProductName").text());
            $(".Unit", row).text($(this).find("UnitType").text());
            $(".OrderQty", row).text($(this).find("OrderQty").text());
            $(".ProductPrice", row).text($(this).find("PPrice").text());
            $(".TotalAmmount", row).text((parseFloat($(this).find("PPrice").text()) * parseFloat($(this).find("OrderQty").text())).toFixed(2));
            $("#tblProductExchangeReport tbody").append(row);
            row = $("#tblProductExchangeReport tbody tr:last").clone(true);
            Amount += ((parseFloat($(this).find("PPrice").text())) * (parseFloat($(this).find("OrderQty").text()).toFixed(2)));
        });
        $('#TotalAmount').html(parseFloat(Amount).toFixed(2));
        $('#OverAllAmount').html($(OverAllAmt).find("OverAllAmount").text());
    } else {
        // $("#EmptyTable").show();
        $('#TotalAmount').html('0.00');
        $('#OverAllAmount').html('0.00');
    }

    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}

function CreateTable(us) {

    row1 = "";
    var image = $("#imgName").val();
    var data = {
        SalesPerson: $("#ddlSalesPerson").val() || 0,
        Customer: $("#ddlCustomer").val() || 0,
        CategoryAutoId: $("#ddlCategory").val() || 0,
        SubCategoryAutoId: $("#ddlSubCategory").val() || 0,
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        Status: $("#ddlStatus").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "ProductFreeReport.aspx/ProductFreeReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var totalAmountPrint = 0.00;
                    var xmldoc = $.parseXML(response.d);
                    var PackerResportDetail = $(xmldoc).find("Table1");
                    var PrintDate = $(xmldoc).find("Table");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (PackerResportDetail.length > 0) {
                        $.each(PackerResportDetail, function () {
                            $(".P_CustomerId", row).text($(this).find("CustomerId").text());
                            $(".P_CustomerName", row).text($(this).find("CustomerName").text());
                            $(".P_OrderNo", row).text($(this).find("OrderNo").text());
                            $(".P_OrderDate", row).text($(this).find("OrderDate").text());
                            $(".P_SalesPerson", row).text($(this).find("SalesPerson").text());
                            $(".P_ProductId", row).text($(this).find("ProductId").text());
                            $(".P_ProductName", row).text($(this).find("ProductName").text());
                            $(".P_Unit", row).text($(this).find("UnitType").text());
                            $(".P_OrderQty", row).text($(this).find("OrderQty").text());
                            $(".P_ProductPrice", row).text($(this).find("PPrice").text());
                            $(".P_TotalAmmount", row).text((parseFloat($(this).find("PPrice").text()) * parseFloat($(this).find("OrderQty").text())).toFixed(2));
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                            totalAmountPrint += ((parseFloat($(this).find("PPrice").text())) * (parseFloat($(this).find("OrderQty").text()).toFixed(2)));
                        });
                        $('#TotalAmountPrint').html(parseFloat(totalAmountPrint).toFixed(2));
                    }
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + PrintDate.find('PrintDate').text());
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtFromDate").val() + " To " + $("#txtToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "ProductReportByFree",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblProductExchangeReport tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
$("#btnExport").click(function () {
    if ($('#tblProductExchangeReport tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});

//----------------------------------Bind Customer---------------------------------------------
function BindCustomer() {
    $.ajax({
        type: "POST",
        url: "ProductFreeReport.aspx/BindCustomer",
        data: "{'SalesPersonAutoId':'" + $('#ddlSalesPerson').val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var CustomerDetails = $(xmldoc).find("Table");
                    $("#ddlCustomer option:not(:first)").remove();
                    $.each(CustomerDetails, function () {
                        $("#ddlCustomer").append("<option value='" + $(this).find("CustomerAutoId").text() + "'>" + $(this).find("CustomerName").text() + "</option>");
                    });
                    $("#ddlCustomer").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}


function BindSubcategory() {
    var data = {
        CategoryAutoId: $("#ddlCategory").val() || 0
    };
    $.ajax({
        type: "POST",
        url: "ProductFreeReport.aspx/BindSubcategory",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var Subcategory = $(xmldoc).find("Table");
                    $("#ddlSubcategory option:not(:first)").remove();
                    $.each(Subcategory, function () {
                        $("#ddlSubcategory").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("SubcategoryName").text() + "</option>");
                    });
                    $("#ddlSubcategory").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}