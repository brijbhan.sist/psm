﻿$(document).ready(function () {
    $('.date').pickadate({
        changeMonth: true,       // this option for allowing user to select month
        changeYear: true,       // this option for allowing user to select from year range
        dateFormat: 'mm/dd/yy' // US date format
    });
    BindCustomerType();
    BindCustomer();
});

function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
};

/*---------------------------------Bind Customer Category-----------------------------------*/
function BindCustomerType() {
    $.ajax({
        type: "POST",
        url: "OpenBalanceReportSummary.aspx/BindCustomerType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlCustomertype option:not(:first)").remove();
                $.each(getData[0].CType, function (index, item) {
                    $("#ddlCustomertype").append("<option value='" + item.CId + "'>" + item.Ct + "</option>");
                });
                $("#ddlCustomertype").select2();

                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(getData[0].SalesList, function (index, item) {
                    $("#ddlSalesPerson").append("<option value = " + item.SID + " >" + item.SP + "</option>");
                });
                $("#ddlSalesPerson").select2();

            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}


/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function BindCustomer() {
    var Customertype = $("#ddlCustomertype").val();
    var SalesPerson = $("#ddlSalesPerson").val();
    $.ajax({
        type: "POST",
        url: "OpenBalanceReportSummary.aspx/BindCustomer",
        data: "{'Customertype':'" + Customertype + "','SalesPersonAutoId':'" + SalesPerson + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlCustomer option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    console.log(getData[index])
                    $("#ddlCustomer").append("<option value='" + getData[index].CuId + "'>" + getData[index].CUN + "</option>");
                });
                $("#ddlCustomer").select2();


            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}


/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function BindReport(PageIndex) {
    var data = {
        CustomerType: $("#ddlCustomertype").val(),
        CustomerAutoId: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        PageSize: $("#ddlPaging").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "OpenBalanceReportSummary.aspx/OpenBalanceReportSummary",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successOrderWiseOpenBalanceReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successOrderWiseOpenBalanceReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {

            var xmldoc = $.parseXML(response.d);
            var productBarcode = $(xmldoc).find("Table1");

            var OrderTotal = $(xmldoc).find("Table2");

            $("#tblOrderList tbody tr").remove();
            var TotalOrder = 0; var TotalOrdAmt = 0.00; var TotalPaid = 0.00; var Totaldueamt = 0.00;
            var row = $("#tblOrderList thead tr").clone(true);
            if (productBarcode.length > 0) {
                $("#EmptyTable").hide();
                $.each(productBarcode, function () {

                    $(".CustomerName", row).text($(this).find("CustomerName").text());
                    $(".NoofOrder", row).text($(this).find("NoofOrder").text());
                    $(".TotalOrderAmount", row).text($(this).find("TotalOrderAmount").text());
                    $(".TotalPaid", row).text($(this).find("TotalPaid").text());
                    $(".TotalDue", row).html($(this).find("AmtDue").text());
                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);
                    TotalOrder = TotalOrder + parseInt($(this).find("NoofOrder").text());
                    TotalOrdAmt = TotalOrdAmt + parseFloat($(this).find("TotalOrderAmount").text());
                    TotalPaid = TotalPaid + parseFloat($(this).find("TotalPaid").text());
                    Totaldueamt = Totaldueamt + parseFloat($(this).find("AmtDue").text());
                });
                $('#AmtDued').html(Totaldueamt.toFixed(2));
                $('#TotalOrderAmountd').html(TotalOrdAmt.toFixed(2));
                $('#TotalPaidd').html(TotalPaid.toFixed(2));
                $('#TotalOrderd').html(TotalOrder);
            } else {
                $('#TotalOrder').html('0');
                $('#TotalOrderAmount').html('0.00');
                $('#TotalPaid').html('0.00');
                $('#AmtDue').html('0.00');
                $('#AmtDued').html('0.00');
                $('#TotalOrderAmountd').html('0.00');
                $('#TotalPaidd').html('0.00');
                $('#TotalOrderd').html('0');
                // $("#EmptyTable").show();
                toastr.error('No data available.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }

            $(OrderTotal).each(function () {
                $('#TotalOrderAmount').html(parseFloat($(this).find('TotalOrderAmount').text()).toFixed(2));
                $('#TotalPaid').html(parseFloat($(this).find('TotalPaid').text()).toFixed(2));
                $('#AmtDue').html(parseFloat($(this).find('AmtDue').text()).toFixed(2));
                $('#TotalOrder').html(parseInt($(this).find('NoofOrder').text()));
            });
            //$('#TotalOrder').html(NoofOrder);
            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}


function CreateTable(us) {
    var image = $("#imgName").val();
    var data = {
        CustomerType: $("#ddlCustomertype").val(),
        CustomerAutoId: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "OpenBalanceReportSummary.aspx/OpenBalanceReportSummary",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var PrintDate = $(xmldoc).find("Table");
                    var productBarcode = $(xmldoc).find("Table1");
                    var OrderTotal = $(xmldoc).find("Table2");
                    $("#PrintTable1 tbody tr").remove();
                    var TotalOrder = 0;
                    var row = $("#PrintTable1 thead tr").clone(true);
                    if (productBarcode.length > 0) {
                        $.each(productBarcode, function () {

                            $(".PCustomerName", row).text($(this).find("CustomerName").text());
                            $(".PNoofOrder", row).text($(this).find("NoofOrder").text());
                            $(".PTotalOrderAmount", row).text($(this).find("TotalOrderAmount").text());
                            $(".PTotalPaid", row).text($(this).find("TotalPaid").text());
                            $(".PTotalDue", row).html($(this).find("AmtDue").text());
                            $("#PrintTable1 tbody").append(row);
                            row = $("#PrintTable1 tbody tr:last").clone(true);
                            TotalOrder = TotalOrder + parseInt($(this).find("NoofOrder").text());
                        });
                    }
                    $(OrderTotal).each(function () {
                        $('#PTotalOrderAmount').html(parseFloat($(this).find('TotalOrderAmount').text()).toFixed(2));
                        $('#PTotalPaid').html(parseFloat($(this).find('TotalPaid').text()).toFixed(2));
                        $('#PAmtDue').html(parseFloat($(this).find('AmtDue').text()).toFixed(2));
                    });
                    $('#PTotalOrder').html(TotalOrder);
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);

                        mywindow.document.write($(PrintTable).clone().html());

                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable1").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "Open Balance Report By Customer" + (new Date()).format("MM/dd/yyyy hh:mm tt"),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        var imageurl = $("#imgName").val();
        var Printdate = (new Date()).format("MM/dd/yyyy hh:mm tt");
        var data = {
            CustomerType: $("#ddlCustomertype").val(),
            CustomerAutoId: $("#ddlCustomer").val(),
            SalesPersonAutoId: $("#ddlSalesPerson").val(),
            image:imageurl,
            PrintDate:Printdate,
            PageSize: 0,
            PageIndex: 1
        };
        localStorage.setItem('OpenBalanceReportByCustomer', JSON.stringify(data));
        window.open("/Reports/PrintOpenbalanceReportByCustomer.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
        }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }


}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}


