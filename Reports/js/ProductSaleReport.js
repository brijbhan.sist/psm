﻿var row1 = "";
$(document).ready(function () {
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    BindSalesPersonandStatus();
});
function Pagevalue(e) {
    getReport(parseInt($(e).attr("page")));
};




function BindSalesPersonandStatus() {
    $.ajax({
        type: "POST",
        url: "ProductSaleReport.aspx/BindCustomer",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var DropDown = $.parseJSON(response.d);
                    for (var i = 0; i < DropDown.length; i++) {
                        var AllDropDownList = DropDown[i];
                        var SalePerson = AllDropDownList.SalePerson;
                        var PriceLevel = AllDropDownList.PriceLevel;
                        var ddlSalePerson = $("#ddlAllPerson");
                        $("#ddlAllPerson option:not(:first)").remove();
                        for (var k = 0; k < SalePerson.length; k++) {
                            var SalePersonDropList = SalePerson[k];
                            var option = $("<option />");
                            option.html(SalePersonDropList.EmpName);
                            option.val(SalePersonDropList.EmpAutoId);
                            ddlSalePerson.append(option);
                        }
                        ddlSalePerson.select2({
                            multiple: true
                        });

                        var Product = AllDropDownList.Product;
                        var ddlProduct = $("#ddlProduct");
                        $("#ddlProduct option:not(:first)").remove();
                        for (var j = 0; j < Product.length; j++) {
                            var Products = Product[j];
                            var option = $("<option />");
                            option.html(Products.ProductName);
                            option.val(Products.AutoId);
                            ddlProduct.append(option);
                        }
                        ddlProduct.select2();

                        var ddlPricelevel = $("#ddlPriceLevel");
                        $("#ddlPriceLevel option:not(:first)").remove();
                        for (var k = 0; k < PriceLevel.length; k++) {
                            var Pricelvl = PriceLevel[k];
                            var option = $("<option />");
                            option.html(Pricelvl.PriceLevelName);
                            option.val(Pricelvl.AutoId);
                            ddlPricelevel.append(option);
                        }
                        ddlPricelevel.select2();


                    }
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}


function getReport(PageIndex) {
    var SalesPerson = "";
    $("#ddlAllPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPerson = $(this).val() + ',';
        } else {
            SalesPerson += $(this).val() + ',';
        }
    });
    if (SalesPerson == "0,") {
        SalesPerson = "0"
    }
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        PriceLevelAutoId: $("#ddlPriceLevel").val(),
        SalesAutoId: SalesPerson.toString(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: PageIndex,
        PageSize: $('#ddlPageSize').val()
    };
    console.log(data);
    $.ajax({
        type: "POST",
        url: "ProductSaleReport.aspx/GetReportDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successgetReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function successgetReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var ReportDetails = $(xmldoc).find("Table");
            var OrderTotal = $(xmldoc).find("Table2");
            $("#tblOrderList tbody tr").remove();
            var row = $("#tblOrderList thead tr").clone(true);
            var  TotalSoldDefaultQty = 0.00; TotalNetSoldP = 0.00; TotalNetSaleRev = 0.00; TotalcostP = 0.00;
            if (ReportDetails.length > 0) {
                $("#EmptyTable").hide();
                $("#tblOrderList").show();
              
                $.each(ReportDetails, function (index) {
                    $(".ProductId", row).text($(this).find("ProductId").text());
                    $(".ProductName", row).text($(this).find("ProductName").text());
                    TotalSoldDefaultQty = TotalSoldDefaultQty + parseFloat($(this).find("Net_Sold_Default_Qty").text());
                    TotalNetSoldP = TotalNetSoldP + parseFloat($(this).find("Net_Sold_Pc").text());
                    TotalNetSaleRev = TotalNetSaleRev + parseFloat($(this).find("Net_Sale_Rev").text());
                    TotalcostP = TotalcostP + parseFloat($(this).find("costP").text());
                    $(".PayableAmount", row).html($(this).find("PayableAmount").text());
                    $(".Unit", row).text($(this).find("UnitType").text());
                    $(".Pcs", row).text($(this).find("Pcperunit").text());
                    $(".CustomPrice", row).text($(this).find("CustomPrice").text());
                    $(".SoldDefaultQty", row).text($(this).find("Sold_Default_Qty").text());
                    $(".SoldPcs", row).text($(this).find("Sold_Pc").text());
                    $(".SoldRev", row).text($(this).find("SaleRev").text());
                    $(".SoldDefaultQty", row).text($(this).find("Net_Sold_Default_Qty").text());
                    $(".NetSoldPcs", row).text($(this).find("Net_Sold_Pc").text());
                    $(".NetSaleRev", row).text($(this).find("Net_Sale_Rev").text());
                    $(".CostPrice", row).text(parseFloat($(this).find("costP").text()).toFixed(2));
                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);

                    ProductId = $(this).find("ProductId").text();
                    CustomerAutoId = $(this).find("CustomerAutoId").text();
                });
                $('#TotalSoldDefaultQty').html(parseFloat(TotalSoldDefaultQty).toFixed(2));
                $('#TotalNetSoldP').html(parseFloat(TotalNetSoldP).toFixed(2));
                $('#TotalNetSaleRev').html(parseFloat(TotalNetSaleRev).toFixed(2));
                $('#TotalcostP').html(parseFloat(TotalcostP).toFixed(2));
            } else {
                $('#TotalSoldDefaultQty').html('0.00');
                $('#TotalNetSoldP').html('0.00');
                $('#TotalNetSaleRev').html('0.00');
                $('#TotalcostP').html('0.00');
            }

            $(OrderTotal).each(function () {
                $('#T_TotalSoldDefaultQty').html(parseFloat($(this).find("Net_Sold_Default_Qty").text()).toFixed(2));
                $('#T_TotalNetSoldP').html(parseFloat($(this).find("Net_Sold_Pc").text()).toFixed(2));
                $('#T_TotalNetSaleRev').html(parseFloat($(this).find("Net_Sale_Rev").text()).toFixed(2));
                $('#T_TotalcostP').html(parseFloat($(this).find("costP").text()).toFixed(2));
            });

            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }

}

function CreateTable(us) {
    $("#ddlAllPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPerson = $(this).val() + ',';
        } else {
            SalesPerson += $(this).val() + ',';
        }
    });
    if (SalesPerson == "0,") {
        SalesPerson = "0"
    }
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        PriceLevelAutoId: $("#ddlPriceLevel").val(),
        SalesAutoId: SalesPerson.toString(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: 1,
        PageSize: 0
    };

    $.ajax({
        type: "POST",
        url: "ProductSaleReport.aspx/GetReportDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var OrderTotal = $(xmldoc).find("Table");
                    $("#RptTable").empty();
                    if ($("#txtSFromDate").val() != "") {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Product Sale Report</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + (new Date()).format("MM/dd/yyyy hh:mm tt") + "</span><br/><span class='DateRangeCSS' style='float:center;margin-top: 0.5%; font-size: 9px; color: black; font-weight: bold;'>Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val() + "</span><br/></div>"
                    }
                    else {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Product Sale Report</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + (new Date()).format("MM/dd/yyyy hh:mm tt") + "</span></div>"
                    }

                    row1 += "<table id='RptTable' class='PrintMyTableHeader MyTableHeader'>"
                    row1 += "<thead>"
                    row1 += "<tr>"
                    row1 += "<td class='text-center'>Product ID</td>"
                    row1 += "<td class='text-left'>Product Name</td>"
                    row1 += "<td class='text-center'>Unit Type</td>"
                    row1 += "<td class='text-center'>Pcs Per<br /> Unit</td>"
                    row1 += "<td class='text-left'>Custom <br />Price</td>"
                    row1 += "<td class='text-left'>Sold Default<br /> Qty</td>"
                    row1 += "<td class='text-left'>Sold Pcs</td>"
                    row1 += "<td class='text-left'>Sale Rev</td>"
                    row1 += "<td class='text-left'>Net Sold<br /> Default Qty</td>"
                    row1 += "<td class='text-left'>Net Sold<br /> Pcs</td>"
                    row1 += "<td class='text-left'>Net Sale <br />Rev</td>"
                    row1 += "<td class='text-left'>Cost Price</td>"
                   
                    row1 += "</tr>"
                    row1 += "</thead>"
                    row1 += "<tbody>"
                    if (OrderTotal.length > 0) {
                        var TotalSoldDefaultQty = 0.00; TotalNetSoldP = 0.00; TotalNetSaleRev = 0.00; TotalcostP = 0.00;
                        $.each(OrderTotal, function (index) {
                            row1 += "<tr>";
                            row1 += "<td class='text-center'>" + $(this).find("ProductId").text() + "</td>";
                            row1 += "<td class='left'>" + $(this).find("ProductName").text() + "</td>";

                            row1 += "<td class='center'>" + $(this).find("UnitType").text() + "</td>";
                            row1 += "<td class='center'>" + $(this).find("Pcperunit").text() + "</td>";
                            row1 += "<td class='right'>" + $(this).find("CustomPrice").text() + "</td>";
                            row1 += "<td class='right'>" + $(this).find("Sold_Default_Qty").text() + "</td>";
                            row1 += "<td class='center'>" + $(this).find("Sold_Pc").text() + "</td>";
                            row1 += "<td class='right'>" + $(this).find("SaleRev").text() + "</td>";
                            row1 += "<td class='right'>" + $(this).find("Net_Sold_Default_Qty").text() + "</td>";
                            row1 += "<td class='right'>" + $(this).find("Net_Sold_Pc").text() + "</td>";
                            row1 += "<td class='right'>" + parseFloat($(this).find("Net_Sale_Rev").text()).toFixed(2) + "</td>";
                            row1 += "<td class='right'>" + parseFloat($(this).find("costP").text()).toFixed(2) + "</td>";
                            TotalSoldDefaultQty = TotalSoldDefaultQty + parseFloat($(this).find("Net_Sold_Default_Qty").text());
                            TotalNetSoldP = TotalNetSoldP + parseFloat($(this).find("Net_Sold_Pc").text());
                            TotalNetSaleRev = TotalNetSaleRev + parseFloat($(this).find("Net_Sale_Rev").text());
                            TotalcostP = TotalcostP + parseFloat($(this).find("costP").text());
                            row1 += "</tr>";
                        });
                        row1 += "</tbody>"
                        row1 += "<tfoot>"
                        row1 += "<tr>"
                        row1 += "<td colspan='8' class='text-right'>Total</td>"
                        row1 += "<td style='text-align:right !important'>" + parseFloat(TotalSoldDefaultQty).toFixed(2) + "</td>"
                        row1 += "<td style='text-align:right !important'>" + parseFloat(TotalNetSoldP).toFixed(2) + "</td>"
                        row1 += "<td style='text-align:right !important'>" + parseFloat(TotalNetSaleRev).toFixed(2) + "</td>"
                        row1 += "<td style='text-align:right !important'>" + parseFloat(TotalcostP).toFixed(2) + "</td>"
                        row1 += "</tr>"
                        row1 += "</tfoot>"
                        row1 += "</table>"
                        if (us == 1) {
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style></style>');
                            mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/app-assets/css/custom.css"/></head><body>');
                            mywindow.document.write('</head><body>');
                            mywindow.document.write(row1);
                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        }
                        if (us == 2) {
                            $("#ExcelDiv").append(row1);
                            $("#RptTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "ProductSaleReport",
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });

                        }
                    }

                }
                else {
                    location.href = "/";
                }
            }

        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function OpenPopUp() {
    toastr.success('Order Status - Closed.', 'Info', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
}


