﻿$(document).ready(function () {
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });

    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);

    BindSalesPerson();

});


function Pagevalue(e) {
    getReport(parseInt($(e).attr("page")));
};

/*---------------------------------------------------Bind Customer-----------------------------------------------------------*/
function BindSalesPerson() {
    $.ajax({
        type: "POST",
        url: "Commission_Summary_Report.aspx/BindSalesPerson",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlAllPerson option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlAllPerson").append("<option value='" + getData[index].SID + "'>" + getData[index].SP + "</option>");
                    });
                    $("#ddlAllPerson").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function getReport(PageIndex) {
    var data = {
        SalesAutoId: $("#ddlAllPerson").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: PageIndex,
        PageSize: $('#ddlPageSize').val()
    };
    $.ajax({
        type: "POST",
        url: "Commission_Summary_with_migrate_Report.aspx/GetReportDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successgetReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successgetReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var ReportDetails = $(xmldoc).find("Table");
            var OrderTotal = $(xmldoc).find("Table2");
            $("#tblOrderList tbody tr").remove();
            var row = $("#tblOrderList thead tr").clone(true);
            var TotalSale = 0, NoofProduct = 0, SPCommAmt = 0, MTotalSale = 0, MNoofProduct = 0, MSPCommAmt = 0;
            var T_TotalSale = 0, T_NoofProduct = 0, T_SPCommAmt = 0, MT_TotalSale = 0, MT_NoofProduct = 0,
                MT_SPCommAmt = 0, Total_NoofProductsSold = 0, Total_NoofSales = 0,
                Total_CommAmt = 0, Total_NoofProductsSolds = 0, Total_NoofSaless = 0, Total_CommAmts = 0;
            if (ReportDetails.length > 0) {
                $("#EmptyTable").hide();
                var html = "", SalesPersonAutoId = 0, TotalPiece = 0;
                var SalesPerson = '';
                $.each(ReportDetails, function (index) {
                    if (SalesPersonAutoId != '0' && (SalesPersonAutoId != $(this).find("SalesPersonAutoId").text())) {
                        html = "<tr style='font-weight: 700; background: oldlace;background:#faf2f2;'><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                            + SalesPerson + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                            + NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                            + parseFloat(TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                            + parseFloat(SPCommAmt).toFixed(2) + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                            + MNoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                            + parseFloat(MTotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                            + parseFloat(MSPCommAmt).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                            + parseInt(Total_NoofProductsSold).toFixed(0) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"                          
                            + parseFloat(Total_NoofSales).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                            + parseFloat(Total_CommAmt).toFixed(2) + "</b></td></tr>";
                        $("#tblOrderList tbody").append(html);
                        TotalSale = 0, NoofProduct = 0, SPCommAmt = 0, MNoofProduct = 0, MTotalSale = 0, MSPCommAmt = 0, Total_NoofProductsSold = 0, Total_NoofSales = 0, Total_CommAmt=0;
                        SalesPerson = '';
                    }
                    $(".NoofProduct", row).text($(this).find("NoofProduct").text());
                    $(".MNoofProduct", row).text($(this).find("MNoofProduct").text());
                    $(".CommCode", row).text($(this).find("CommCode").text());
                    $(".TotalSale", row).text(parseFloat($(this).find("TotalSale").text()).toFixed(2));
                    $(".MTotalSale", row).text(parseFloat($(this).find("MTotalSale").text()).toFixed(2));
                    $(".Commission", row).text(parseFloat($(this).find("SPCommAmt").text()).toFixed(2));
                    $(".MSPCommission", row).text(parseFloat($(this).find("MSPCommAmt").text()).toFixed(2));

                    $(".Total_NoofProduct", row).text(parseInt($(this).find("NoofProduct").text()) + parseInt($(this).find("MNoofProduct").text()));
                    $(".Total_NoofSales", row).text((parseFloat($(this).find("TotalSale").text()) + parseFloat($(this).find("MTotalSale").text())).toFixed(2));
                    $(".Total_CommAmt", row).text((parseFloat($(this).find("SPCommAmt").text()) + parseFloat($(this).find("MSPCommAmt").text())).toFixed(2));

                    NoofProduct = NoofProduct + parseInt($(this).find("NoofProduct").text());
                    MNoofProduct = MNoofProduct + parseInt($(this).find("MNoofProduct").text());
                    TotalSale = TotalSale + parseFloat($(this).find("TotalSale").text());
                    MTotalSale = MTotalSale + parseFloat($(this).find("MTotalSale").text());
                    SPCommAmt = SPCommAmt + parseFloat($(this).find("SPCommAmt").text());
                    MSPCommAmt = MSPCommAmt + parseFloat($(this).find("MSPCommAmt").text());

                    T_TotalSale = T_TotalSale + parseFloat($(this).find("TotalSale").text());
                    MT_TotalSale = parseFloat(MT_TotalSale) + parseFloat($(this).find("MTotalSale").text());
                    T_NoofProduct = T_NoofProduct + parseInt($(this).find("NoofProduct").text());
                    MT_NoofProduct = MT_NoofProduct + parseInt($(this).find("MNoofProduct").text());
                    T_SPCommAmt = T_SPCommAmt + parseFloat($(this).find("SPCommAmt").text());
                    MT_SPCommAmt = MT_SPCommAmt + parseFloat($(this).find("MSPCommAmt").text());

                    Total_NoofProductsSold = Total_NoofProductsSold + (parseInt($(this).find("NoofProduct").text()) + parseInt($(this).find("MNoofProduct").text()));
                    Total_NoofSales = Total_NoofSales + (parseFloat($(this).find("TotalSale").text()) + parseFloat($(this).find("MTotalSale").text()));
                    Total_CommAmt = parseFloat(Total_CommAmt) + (parseFloat($(this).find("SPCommAmt").text()) + parseFloat($(this).find("MSPCommAmt").text()));

                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);
                    SalesPersonAutoId = $(this).find("SalesPersonAutoId").text();
                    SalesPerson = $(this).find("SP").text();
                });
                if (SalesPerson != '') {
                    html = "<tr style='font-weight: 700; background: oldlace;background:#faf2f2;'><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                        + SalesPerson + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                        + NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"                      
                        + parseFloat(TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"                        
                        + parseFloat(SPCommAmt).toFixed(2) + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                        + MNoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                        + parseFloat(MTotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                        + parseFloat(MSPCommAmt).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                        + parseInt(Total_NoofProductsSold).toFixed(0) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                        + parseFloat(Total_NoofSales).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"                        
                        + parseFloat(Total_CommAmt).toFixed(2) + "</b></td></tr>";
                    $("#tblOrderList tbody").append(html);
                }
                $('#T_NoofProduct').html(parseFloat(T_NoofProduct).toFixed(0));
                $('#MT_NoofProduct').html(parseFloat(MT_NoofProduct).toFixed(0));
                $('#T_TotalSale').html(parseFloat(T_TotalSale).toFixed(2));
                $('#MT_TotalSale').html(parseFloat(MT_TotalSale).toFixed(2));
                $('#T_SPCommAmt').html(parseFloat(T_SPCommAmt).toFixed(2));
                $('#MT_SPCommAmt').html(parseFloat(MT_SPCommAmt).toFixed(2));

                $('#Total_NoofProductsSold').html(parseFloat(Total_NoofProductsSold).toFixed(0));
                $('#Total_NoofSales').html(parseFloat(Total_NoofSales).toFixed(2));
                $('#Total_CommAmt').html(parseFloat(Total_CommAmt).toFixed(2));

            } else {
                $('#T_NoofProduct').html(parseFloat(T_NoofProduct).toFixed(0));
                $('#MT_NoofProduct').html(parseFloat(MT_NoofProduct).toFixed(0));
                $('#T_TotalSale').html(parseFloat(T_TotalSale).toFixed(2));
                $('#MT_TotalSale').html(parseFloat(MT_TotalSale).toFixed(2));
                $('#T_SPCommAmt').html(parseFloat(T_SPCommAmt).toFixed(2));
                $('#MT_SPCommAmt').html(parseFloat(MT_SPCommAmt).toFixed(2));

                $('#TotalQty').html('0');
                $('#AmtDue').html('0.00');
              //  $("#EmptyTable").show();
            }
            $(OrderTotal).each(function () {
                $('#T_NoofProducts').html($(this).find('NoofOrder').text());
                $('#T_TotalSales').html(parseFloat($(this).find('TotalSale').text()).toFixed(2));
                $('#T_SPCommAmts').html(parseFloat($(this).find('SPCommAmt').text()).toFixed(2));
                $('#MT_NoofProducts').html($(this).find('MNoofOrder').text());
                $('#MT_TotalSales').html(parseFloat($(this).find('MTotalSale').text()).toFixed(2));
                $('#MT_SPCommAmts').html(parseFloat($(this).find('MSPCommAmt').text()).toFixed(2));

                Total_NoofProductsSolds = Total_NoofProductsSolds + (parseInt($(this).find("NoofOrder").text()) + parseInt($(this).find("MNoofOrder").text()));
                Total_NoofSaless = Total_NoofSaless + (parseFloat($(this).find("TotalSale").text()) + parseFloat($(this).find("MTotalSale").text()));
                Total_CommAmts = parseFloat(Total_CommAmts) + (parseFloat($(this).find("SPCommAmt").text()) + parseFloat($(this).find("MSPCommAmt").text()));


                $('#Total_NoofProductsSolds').html(parseInt(Total_NoofProductsSolds).toFixed(0));
                $('#Total_NoofSaless').html(parseFloat(Total_NoofSaless).toFixed(2));
                $('#Total_CommAmts').html(parseFloat(Total_CommAmts).toFixed(2));

                 

            });

            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}

$("#btnSearch").click(function () {
    getReport(1);
})



function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        SalesAutoId: $("#ddlAllPerson").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: 1,
        PageSize: 0
    };
    $.ajax({
        type: "POST",
        url: "Commission_Summary_with_migrate_Report.aspx/GetReportDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ReportDetails = $(xmldoc).find("Table");
                    var PrintDate = $(xmldoc).find("Table1");
                    var OrderTotal = $(xmldoc).find("Table2");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    var TotalSale = 0, NoofProduct = 0, SPCommAmt = 0, T_TotalSale = 0, T_NoofProduct = 0, T_SPCommAmt = 0, P_Total_NoofProductsSold = 0, P_Total_NoofSales = 0, P_Total_CommAmt=0;
                    var T_TotalSale = 0, T_NoofProduct = 0, T_SPCommAmt = 0, MT_NoofProduct = 0, MT_TotalSale = 0, MT_SPCommAmt = 0, Total_NoofProductsSolds = 0, Total_NoofSaless = 0, Total_CommAmts = 0;
                    if (ReportDetails.length > 0) {
                        var html = "", SalesPersonAutoId = 0, TotalPiece = 0;
                        var SalesPerson = '';
                        $.each(ReportDetails, function (index) {
                            if (SalesPersonAutoId != '0' && (SalesPersonAutoId != $(this).find("SalesPersonAutoId").text())) {


                                html = "<tr style='font-weight: 700; background: oldlace;background:#faf2f2;'><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + SalesPerson + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + parseFloat(TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + parseFloat(SPCommAmt).toFixed(2) + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + MT_NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + parseFloat(MT_TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + parseFloat(MT_SPCommAmt).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + parseInt(P_Total_NoofProductsSold).toFixed(0) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + parseFloat(P_Total_NoofSales).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + parseFloat(P_Total_CommAmt).toFixed(2) + "</b></td></tr>";
                                $("#PrintTable tbody").append(html);

                                TotalSale = 0, NoofProduct = 0, SPCommAmt = 0, P_Total_NoofProductsSold = 0,
                                    P_Total_NoofSales = 0, P_Total_CommAmt = 0, MT_NoofProduct = 0, MT_TotalSale = 0, MT_SPCommAmt = 0;
                                SalesPerson = '';
                            }
                            $(".PNoofProduct", row).text($(this).find("NoofProduct").text());
                            $(".PCommCode", row).text($(this).find("CommCode").text());
                            $(".PTotalSale", row).text(parseFloat($(this).find("TotalSale").text()).toFixed(2));
                            $(".PCommission", row).text(parseFloat($(this).find("SPCommAmt").text()).toFixed(2));
                            $(".MPNoofProduct", row).text($(this).find("MNoofProduct").text());
                            $(".MPTotalSale", row).text(parseFloat($(this).find("MTotalSale").text()).toFixed(2));
                            $(".MPCommission", row).text(parseFloat($(this).find("MSPCommAmt").text()).toFixed(2));
                            $(".T_Total_NoofProduct", row).text(parseInt($(this).find("NoofProduct").text()) + parseInt($(this).find("MNoofProduct").text()));
                            $(".T_Total_NoofSales", row).text((parseFloat($(this).find("TotalSale").text()) + parseFloat($(this).find("MTotalSale").text())).toFixed(2));
                            $(".T_Total_CommAmt", row).text((parseFloat($(this).find("SPCommAmt").text()) + parseFloat($(this).find("MSPCommAmt").text())).toFixed(2));
                            TotalSale = TotalSale + parseFloat($(this).find("TotalSale").text());
                            NoofProduct = NoofProduct + parseInt($(this).find("NoofProduct").text());
                            SPCommAmt = SPCommAmt + parseFloat($(this).find("SPCommAmt").text());

                            T_TotalSale = T_TotalSale + parseFloat($(this).find("TotalSale").text());
                            T_NoofProduct = T_NoofProduct + parseInt($(this).find("NoofProduct").text());
                            T_SPCommAmt = T_SPCommAmt + parseFloat($(this).find("SPCommAmt").text());

                            MT_NoofProduct = MT_NoofProduct + parseInt($(this).find("MNoofProduct").text());
                            MT_TotalSale = MT_TotalSale + parseFloat($(this).find("SPCommAmt").text());
                            MT_SPCommAmt = MT_SPCommAmt + parseFloat($(this).find("MSPCommAmt").text());

                            P_Total_NoofProductsSold = P_Total_NoofProductsSold + (parseInt($(this).find("NoofProduct").text()) + parseInt($(this).find("MNoofProduct").text()));
                            P_Total_NoofSales = P_Total_NoofSales + (parseFloat($(this).find("TotalSale").text()) + parseFloat($(this).find("MTotalSale").text()));
                            P_Total_CommAmt = parseFloat(P_Total_CommAmt) + (parseFloat($(this).find("SPCommAmt").text()) + parseFloat($(this).find("MSPCommAmt").text()));

                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                            SalesPersonAutoId = $(this).find("SalesPersonAutoId").text();
                            SalesPerson = $(this).find("SP").text();
                        });
                        if (SalesPerson != '') {
                    
                          

                            html = "<tr style='font-weight: 700; background: oldlace;background:#faf2f2;'><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                + SalesPerson + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                + NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                + parseFloat(TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                + parseFloat(SPCommAmt).toFixed(2) + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                + MT_NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                + parseFloat(MT_TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                + parseFloat(MT_SPCommAmt).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                + parseInt(P_Total_NoofProductsSold).toFixed(0) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                + parseFloat(P_Total_NoofSales).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>"
                                + parseFloat(P_Total_CommAmt).toFixed(2) + "</b></td></tr>";
                            $("#PrintTable tbody").append(html);

                        }
                        $('#PT_NoofProduct').html(parseFloat(T_NoofProduct).toFixed(0));
                        $('#PT_TotalSale').html(parseFloat(T_TotalSale).toFixed(2));
                        $('#PT_SPCommAmt').html(parseFloat(T_SPCommAmt).toFixed(2));
                        $('#MPT_NoofProducts').html(parseFloat(MT_NoofProduct).toFixed(2));
                        $('#MPT_TotalSales').html(parseFloat(MT_TotalSale).toFixed(2));
                        $('#MPT_SPCommAmts').html(parseFloat(MT_SPCommAmt).toFixed(2));

                        $('#PT_Total_NoofProductsSolds').html(parseFloat(P_Total_NoofProductsSold).toFixed(0));
                        $('#PT_Total_NoofSaless').html(parseFloat(P_Total_NoofSales).toFixed(2));
                        $('#PT_Total_CommAmts').html(parseFloat(P_Total_CommAmt).toFixed(2));

                    } else {
                        $('#PT_NoofProduct').html(parseFloat(T_NoofProduct).toFixed(0));
                        $('#PT_TotalSale').html(parseFloat(T_TotalSale).toFixed(2));
                        $('#PT_SPCommAmt').html(parseFloat(T_SPCommAmt).toFixed(2));
                        $('#MPT_NoofProducts').html(parseFloat(MT_NoofProduct).toFixed(2));
                        $('#MPT_TotalSales').html(parseFloat(MT_TotalSale).toFixed(2));
                        $('#MPT_SPCommAmts').html(parseFloat(MT_SPCommAmt).toFixed(2));
                        $('#PT_Total_NoofProductsSolds').html(parseFloat(P_Total_NoofProductsSold).toFixed(0));
                        $('#PT_Total_NoofSaless').html(parseFloat(P_Total_NoofSales).toFixed(2));
                        $('#PT_Total_CommAmts').html(parseFloat(P_Total_CommAmt).toFixed(2));

                        $('#PTotalQty').html('0');
                        $('#PAmtDue').html('0.00');
                    }

                    $(OrderTotal).each(function () {
                        $('#PT_NoofProduct').html($(this).find('NoofOrder').text());
                        $('#PT_TotalSale').html(parseFloat($(this).find('TotalSale').text()).toFixed(2));
                        $('#PT_SPCommAmt').html(parseFloat($(this).find('SPCommAmt').text()).toFixed(2));

                        $('#MPT_NoofProducts').html($(this).find('MNoofOrder').text());
                        $('#MPT_TotalSales').html(parseFloat($(this).find('MTotalSale').text()).toFixed(2));
                        $('#MPT_SPCommAmts').html(parseFloat($(this).find('MSPCommAmt').text()).toFixed(2));

                        Total_NoofProductsSolds = Total_NoofProductsSolds + (parseInt($(this).find("NoofOrder").text()) + parseInt($(this).find("MNoofOrder").text()));
                        Total_NoofSaless = Total_NoofSaless + (parseFloat($(this).find("TotalSale").text()) + parseFloat($(this).find("MTotalSale").text()));
                        Total_CommAmts = parseFloat(Total_CommAmts) + (parseFloat($(this).find("SPCommAmt").text()) + parseFloat($(this).find("MSPCommAmt").text()));


                        $('#PT_Total_NoofProductsSolds').html(parseInt(Total_NoofProductsSolds).toFixed(0));
                        $('#PT_Total_NoofSaless').html(parseFloat(Total_NoofSaless).toFixed(2));
                        $('#PT_Total_CommAmts').html(parseFloat(Total_CommAmts).toFixed(2));

                    });

                    if (us == 1) {
                        
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + $(PrintDate).find('PrintDate').text());
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtSFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        //mywindow.document.write(divToPrint.outerHTML);

                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "EmpCommissionSummaryWithMigrateReport",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
    CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}


