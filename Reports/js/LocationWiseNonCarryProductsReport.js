﻿
$(document).ready(function () {
    bindCategory();
    bindSubcategory();
    getReport(1);
});
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "LocationWiseNonCarryProductsReport.aspx/bindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger
            var getData = $.parseJSON(response.d);
            var category = getData[0].Category;
            var brand = getData[0].Brand;
            var location = getData[0].Location;
            $("#ddlSCategory option:not(:first)").remove();
            $.each(category, function (index, item) {
                $("#ddlSCategory").append("<option value='" + category[index].AutoId + "'>" + category[index].CategoryName + "</option>");
            });
            $("#ddlSCategory").select2();

            $("#ddlBrand option:not(:first)").remove();
            $.each(brand, function (index, item) {
                $("#ddlBrand").append("<option value='" + brand[index].AutoId + "'>" + brand[index].BrandName + "</option>");
            });
            $("#ddlBrand").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*-------------------------------------------------------Bind Subcategory-----------------------------------------------------------*/
function bindSubcategory() {
    debugger
    var categoryAutoId = $("#ddlSCategory").val();
    $.ajax({
        type: "POST",
        url: "LocationWiseNonCarryProductsReport.aspx/bindSubcategory",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var subcategory = $.parseJSON(response.d);
            if (subcategory.length > 0) {
                $("#ddlSSubcategory option:not(:first)").remove();
                $.each(subcategory, function (index, item) {
                    $("#ddlSSubcategory").append("<option value='" + subcategory[index].AutoId + "'>" + subcategory[index].SubcategoryName + "</option>");
                });
                $("#ddlSSubcategory").select2();
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function Pagevalue(e) {
    getReport(parseInt($(e).attr("page")));
}

function getReport(PageIndex) {
    var pobj = new Object();
  //  pobj.ProductId = $("#txtProductId").val().trim();
    pobj.ProductName = $("#txtProductName").val().trim();
    pobj.CategoryAutoId = $("#ddlSCategory").val();
    pobj.SubCategoryAutoId = $("#ddlSSubcategory").val();
    pobj.BrandAutoId = $("#ddlBrand").val();
    pobj.Month = $("#ddlMonth").val();
    pobj.PageSize = $("#ddlPageSize").val();
    pobj.PageIndex = PageIndex;

    var data = {
        pobj: pobj
    };
    $.ajax({
        type: "POST",
        url: "LocationWiseNonCarryProductsReport.aspx/GetReport",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetRouteList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetRouteList(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var TotalCount = $(xmldoc).find("Table");
            var List = $(xmldoc).find("Table1");

            $("#tblOrderList tbody tr").remove();
            var row = $("#tblOrderList thead tr:last").clone(true);
            var totalCount = 0;
            if (List.length > 0) {
                $.each(List, function (index) {
                    debugger
                    totalCount = Number(totalCount) + 1;
                    if ($(this).find("ProductStatus").text() == '1') {
                        $(".Action", row).html("<input type='checkbox' onclick='checkIndividual(this)' name='table_records' id='chkProduct'>");
                    }
                    else {
                        $(".Action", row).html("<input type='checkbox' disabled name='table_records' id='chkProduct'>");
                    }
                    $(".ProductId", row).html('<span ProductAutoId=' + $(this).find("AutoId").text()+'>'+$(this).find("ProductId").text()+'</span>');
                    $(".ProductName", row).html($(this).find("ProductName").text());
                    $(".Category", row).html($(this).find("CategoryName").text());
                    $(".SubCategory", row).html($(this).find("SubcategoryName").text());
                    $(".Brand", row).html($(this).find("BrandName").text()); 
                    $(".Date ", row).html($(this).find("lastdate").text()); 
                    $(".CreateDate ", row).html($(this).find("CreateDate").text());
                    if ($(this).find("ProductStatus").text() == '1') {
                        $(".Status ", row).html('<span class="badge badge-pill" style="background-color:#66ff99">Active</span>');
                    }
                    else {
                        $(".Status ", row).html('<span class="badge badge-pill Status_cancelled" >Inactive</span>');
                    }
                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);
                });
                $("#EmptyTable").hide();
                $("#totalCount").html(TotalCount.find('TotalRecord').text());
            } else {
                $("#EmptyTable").show();
                $("#totalCount").html('0');
            } debugger
            var pager = $(xmldoc).find("Table2");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
            $("#checkall").prop('checked', false);
        }
    }
    else {
        location.href = "/";
    }
}
function checkAll() {
    debugger
    if ($("#checkall").prop('checked') == true) {
        $("#tblOrderList tbody tr").each(function (i, tbl) {
            if ($(this).find(".Status").text() == 'Active') {
                $(this).find('.Action input').prop('checked', true);
            }
        })
    }
    else {
        $("#tblOrderList tbody tr").each(function (i, tbl) {
            $(this).find('.Action input').prop('checked', false);
        })
    }
}
function CreateTable(us) {
    debugger
    var row1 = "";
    var image = $("#imgName").val();
    var pobj = new Object();
    //pobj.ProductId = $("#txtProductName").val().trim();
    pobj.ProductName = $("#txtProductName").val().trim();
    pobj.CategoryAutoId = $("#ddlSCategory").val();
    pobj.SubCategoryAutoId = $("#ddlSSubcategory").val();
    pobj.BrandAutoId = $("#ddlBrand").val();
    pobj.Month = $("#ddlMonth").val();
    pobj.PageSize = 0;
    pobj.PageIndex = 1;
    var data = {
        pobj: pobj
    };
    $.ajax({
        type: "POST",
        url: "LocationWiseNonCarryProductsReport.aspx/Getreport",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var PrintDate = $(xmldoc).find("Table2");
                    var List = $(xmldoc).find("Table1");
                    debugger
                    if (List.length > 0) {

                        $("#RptTable").empty();

                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Manage Non Carry Products</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + $(PrintDate).find("PrintDate").text() + "</span></div>"
                        row1 += "<table id='RptTable' class='PrintMyTableHeader MyTableHeader'>"
                        row1 += "<thead>"
                        row1 += "<tr>"
                        row1 += "<td style='text-align:center !important;font-weight:bold'>Product ID</td>"
                        row1 += "<td style='text-align:center !important;font-weight:bold'>Category</td>"
                        row1 += "<td style='text-align:center !important;font-weight:bold'>Subcategory</td>"
                        row1 += "<td style='text-align:center !important;font-weight:bold'>Product Name</td>"
                        row1 += "<td style='text-align:center !important;font-weight:bold'>Brand</td>"
                        row1 += "<td style='text-align:center !important;font-weight:bold'>Create Date</td>"
                        row1 += "<td style='text-align:center !important;font-weight:bold'>Last Order Date</td>"
                        row1 += "</tr>"
                        row1 += "</thead>"
                        row1 += "<tbody>"

                        if (List.length > 0) {
                            $.each(List, function (index) {
                                row1 += "<tr>";
                                row1 += "<td style='text-align:center !important'>" + $(this).find("ProductId").text() + "</td>";
                                row1 += "<td style='text-align:left !important'>" + $(this).find("CategoryName").text() + "</td>";
                                row1 += "<td style='text-align:left !important'>" + $(this).find("SubcategoryName").text() + "</td>";
                                row1 += "<td style='text-align:left !important'>" + $(this).find("ProductName").text() + "</td>";
                                row1 += "<td style='text-align:left !important'>" + $(this).find("BrandName").text() + "</td>";
                                row1 += "<td style='text-align:center !important'>" + $(this).find("CreateDate").text() + "</td>";
                                row1 += "<td style='text-align:center !important'>" + $(this).find("lastdate").text() + "</td>";
                                row1 += "</tr>";
                            });
                            row1 += "</tbody>"

                            row1 += "</table>"

                        }
                    }
                    console.log(us + ":" + List.length);
                        if (us == 1) {
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style></style>');
                            mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                            mywindow.document.write('</head><body>');
                            mywindow.document.write(row1);
                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);

                        }
                        if (us == 2) {
                            $("#ExcelDiv").html(row1);
                            $("#RptTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "NonCarryProductsReport",
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });

                        }
                    }
                }
                else {
                    location.href = "/";
                }
            }

        });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function InactiveProducts() {
   var Orders ='',check = 0;
    $("#tblOrderList tbody tr").each(function (i) {
        if ($(this).find('.Action input').prop('checked') == true) {
            Orders = Orders + $(this).find('.ProductId span').attr('productautoid') + ",";
            check = 1;
        }
    })
    if (check == 0) {
        toastr.error('Please select atleast one product to set inactive.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    }
    else {
        $.ajax({
            type: "Post",
            url: "LocationWiseNonCarryProductsReport.aspx/InactiveProduct",
            data: "{'TableValues':'" + Orders + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    getReport(1);
                    swal('','Product status has been updated successfully.','success');
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}
function checkIndividual(e) {
    var i = 1;
    if ($(e).prop('checked')) {
        i = 0;
    }
    $("#tblOrderList tbody tr").each(function () {
        $(this).find(".Actions input").prop('checked', false)
    });
    var row = $(e).closest('tr');
    if (i == 0)
        $(row).find(".Actions input").prop('checked', true);
}
function InactiveProduct() {
    var check = 0;
    $("#tblOrderList tbody tr").each(function (i) {
        if ($(this).find('.Action input').prop('checked') == true) {
            check = 1;
        }
    })
    if (check == 0) {
        toastr.error('Please select atleast one product to set inactive.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    }
    swal({
            title: "Are you sure?",
            text: "You want to inactive selected product.",
            icon: "warning",
            showCancelButton: true,
            allowOutsideClick: false,
            closeOnClickOutside: false,
            buttons: {
                cancel: {
                    text: "No, Cancel.",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes.",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            }
        }).then(function (isConfirm) {
        if (isConfirm) {
            InactiveProducts();
        }
    })
}



