﻿var PrintDate = '';
$(document).ready(function () {   
    BindReport(1);
});
function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
};
/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function BindReport(PageIndex) {
    var data = {
        PageSize: $("#ddlPageSize").val(),
        pageIndex: PageIndex,
        ToDate: ""
    };
    $.ajax({
        type: "POST",
        url: "/Reports/ReadytoShipReport.aspx/ReadytoShipReport",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
          
        },
        complete: function () {
            $.unblockUI();
          
        },
        success: successReadytoShipReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successReadytoShipReport(response) {
    if (response.d == 'Session Expired') {
        location.href = "/";
    } else if (response.d != 'false') {
        var xmldoc = $.parseXML(response.d);
        var OrderTotal = $(xmldoc).find("Table");
        $("#tblOrderList tbody tr").remove();
        $("#PrintTable tbody tr").remove();
        var row = $("#tblOrderList thead tr").clone(true);
        var row1 = $("#PrintTable thead tr").clone(true);
        var GrandAmount = 0.00, NoOfOrders = 0.;
        if (OrderTotal.length > 0) {
            $("#EmptyTable").hide();
            $.each(OrderTotal, function () {
                $(".OrderDate", row).text($(this).find("OrderDate").text());
                $(".CustomerName", row).text($(this).find("CustomerName").text());
                $(".OrderNo", row).text($(this).find("OrderNo").text());
                $(".ShippingAddress", row).text($(this).find("CustomerAddress").text());
                $(".DriverName", row).text($(this).find("DriverName").text());
                $(".StopNo", row).text($(this).find("StopNo").text());
                $(".POrderDate", row1).text($(this).find("OrderDate").text());
                $(".PCustomerName", row1).text($(this).find("CustomerName").text());
                $(".POrderNo", row1).text($(this).find("OrderNo").text());
                $(".PShippingAddress", row1).text($(this).find("CustomerAddress").text());
                $(".PDriverName", row1).text($(this).find("DriverName").text());
                $(".PStopNo", row1).text($(this).find("StopNo").text());
                $("#tblOrderList tbody").append(row);
                row = $("#tblOrderList tbody tr:last").clone(true);
                $("#PrintTable tbody").append(row1);
                row1 = $("#PrintTable tbody tr:last").clone(true);
            });

        } else {
           // $("#EmptyTable").show();
        }
        $('#GrandAmount').html(parseFloat(GrandAmount).toFixed(2));
        $('#NoOfOrders').html((OrderTotal.length));
        $('#PNoOfOrders').html((OrderTotal.length));

        var pager = $(xmldoc).find("Table1");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
        PrintDate = pager.find('PrintDate').text();
    }
}

$("#btnSearch").click(function () {
    BindReport(1);

})
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
    //var image = $("#imgName").val();
    //var divToPrint = document.getElementById("PrintTable");
    //var mywindow = window.open('', 'mywindow', 'height=400,width=600');
    //mywindow.document.write('<html><head><style>#PrintTable tbody tr .right{text-align:right;} #PrintTable tbody tr .left{text-align:left;} #PrintTable tbody tr .price{text-align:right;}#PrintTable {border-collapse: collapse;width: 100%;}#PrintTable td, #PrintTable th {border: 1px solid black;padding: 8px;}#PrintTable tr:nth-child(even){background-color: #f2f2f2;}#PrintTable thead {padding-top: 12px;padding-bottom: 12px;background-color:#C8E8F9;color:black;}.text-right{text-align:right;}.text-left{text-align:left;}.text-center{text-align:center;}</style>');
    //mywindow.document.write('</head><body>');
    //mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;'><img src='/Img/logo/" + image + "' style='float:left;' height='70px' width='140px'/><h3 style='float:center;'>Ready To Ship Report</h3><h4 style='float:right;margin-right:10px;'>Date: " + (new Date()).format("MM/dd/yyyy") + "</h4></div>")
    //mywindow.document.write(divToPrint.outerHTML);
    //mywindow.document.write('</body></html>');
        //mywindow.print();
        var image = $("#imgName").val();
        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
        $("#PrintDate").text("Print Date : " + (PrintDate));
        $("#PrintLogo").attr("src", "/Img/logo/" + image);
        mywindow.document.write($(PrintTable1).clone().html());
        //mywindow.document.write(divToPrint.outerHTML);

        mywindow.document.write('</body></html>');
        setTimeout(function () {
            mywindow.print();
        }, 2000);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
$("#btnExport").click(function () {
    if ($('#tblOrderList tbody tr').length > 0) {
    $("#PrintTable").table2excel({
        exclude: ".noExl",
        name: "Excel Document Name",
        filename: "ReadytoShipReport",
        fileext: ".xls",
        exclude_img: true,
        exclude_links: true,
        exclude_inputs: true
    });
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});



