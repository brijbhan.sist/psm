﻿$(document).ready(function () {
    
    BindPacker();
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtDateFrom").val(month + '/' + day + '/' + year);
    $("#txtDateTo").val(month + '/' + day + '/' + year);
});
function BindPacker() {

    $.ajax({
        type: "POST",
        url: "WebAPI/PackerReportSummary.asmx/GetPackerName",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {

                    var xmldoc = $.parseXML(response.d);
                    var GetPacker = $(xmldoc).find("Table");
                    var CommissionCode = $(xmldoc).find("Table1");

                    $("#ddlPackerName option:not(:first)").remove();
                    $.each(GetPacker, function () {
                        $("#ddlPackerName").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("PackerName").text() + "</option>");
                    });

                    $("#ddlPackerName").select2()

                    //$("#ddlCommissionCode option:not(:first)").remove();
                    //$.each(CommissionCode, function () {
                    //    $("#ddlCommissionCode").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CommisssionCode").text() + "</option>");
                    //});

                    //$("#ddlCommissionCode").select2()
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Pagevalue(e) {
    PackerResport(parseInt($(e).attr("page")));
}
$("#ddlPageSize").change(function () {
    PackerResport(1);
})
$("#btnSearch").click(function () {
    PackerResport(1);
});
function PackerResport(PageIndex) {
    var data = {
        DateFrom: $("#txtDateFrom").val(),
        DateTo: $("#txtDateTo").val(),
        PackerAutoId: $("#ddlPackerName").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/PackerReportSummary.asmx/GetPackerResport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: SuccessPackerResport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function SuccessPackerResport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var PackerResportDetail = $(xmldoc).find("Table1");
            var OrderTotal = $(xmldoc).find("Table2");
            $("#tblPackerReportSummary tbody tr").remove();
            var row = $("#tblPackerReportSummary thead tr").clone(true);
            var TotalAssignOrder = 0.00;
            var TotalPackedOrder = 0.00;
            var TotalPendingOrder = 0.00;
            if (PackerResportDetail.length > 0) {
                $("#EmptyTable").hide();

                $("#tblPackerReportSummary thead tr").show();
                $.each(PackerResportDetail, function (index) {
                    $(".PackerAssignDate", row).text($(this).find("PackerAssignDate").text());
                    $(".warehouse", row).text($(this).find("warehouse").text());
                    $(".PackerName", row).text($(this).find("PackerName").text());
                    $(".TotalAssignOrder", row).text($(this).find("TotalAssignOrder").text());
                    TotalAssignOrder = TotalAssignOrder + parseInt($(this).find("TotalAssignOrder").text());
                    TotalPackedOrder = TotalPackedOrder + parseInt($(this).find("TotalPackedOrder").text());
                    TotalPendingOrder = TotalPendingOrder + parseInt($(this).find("TotalPendingOrder").text());
                    $(".TotalPackedOrder", row).text($(this).find("TotalPackedOrder").text());
                    $(".TotalPendingOrder", row).text($(this).find("TotalPendingOrder").text());

                    $('#TotalAssignOrder').html(parseFloat(TotalAssignOrder).toFixed(0));
                    $('#TotalPackedOrder').html(parseFloat(TotalPackedOrder).toFixed(0));
                    $('#TotalPendingOrder').html(parseFloat(TotalPendingOrder).toFixed(0));
                    $("#tblPackerReportSummary tbody").append(row);
                    row = $("#tblPackerReportSummary tbody tr:last").clone(true);

                });

            } else {
                $('#TotalAssignOrder').html('0');
                $('#TotalPackedOrder').html('0');
                $('#TotalPendingOrder').html('0');
                $('#TotalAssignOrders').html('0');
                $('#TotalPackedOrders').html('0');
                $('#TotalPendingOrders').html('0');
                //$("#EmptyTable").show();
            }
            $(OrderTotal).each(function () {
                $('#TotalAssignOrders').html(parseFloat($(this).find('TotalAssignOrder').text()));
                $('#TotalPackedOrders').html(parseFloat($(this).find('TotalPackedOrder').text()));
                $('#TotalPendingOrders').html(parseFloat($(this).find('TotalPendingOrder').text()));
            });
            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}
function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        DateFrom: $("#txtDateFrom").val(),
        DateTo: $("#txtDateTo").val(),
        PackerAutoId: $("#ddlPackerName").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/PackerReportSummary.asmx/GetPackerResport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var PackerResportDetail = $(xmldoc).find("Table1");
                    $("#PrintTable tbody tr").remove();
                    var TotalAssignOrder = 0.00;
                    var TotalPackedOrder = 0.00;
                    var TotalPendingOrder = 0.00;
                    var row = $("#PrintTable thead tr").clone(true);
                    if (PackerResportDetail.length > 0) {
                        $.each(PackerResportDetail, function () {
                            $(".PPackerAssignDate", row).text($(this).find("PackerAssignDate").text());
                            $(".Pwarehouse", row).text($(this).find("warehouse").text());
                            $(".PPackerName", row).text($(this).find("PackerName").text());
                            $(".PTotalAssignOrder", row).text($(this).find("TotalAssignOrder").text());
                            TotalAssignOrder = TotalAssignOrder + parseInt($(this).find("TotalAssignOrder").text());
                            TotalPackedOrder = TotalPackedOrder + parseInt($(this).find("TotalPackedOrder").text());
                            TotalPendingOrder = TotalPendingOrder + parseInt($(this).find("TotalPendingOrder").text());
                            $(".PTotalPackedOrder", row).text($(this).find("TotalPackedOrder").text());
                            $(".PTotalPendingOrder", row).text($(this).find("TotalPendingOrder").text());
                            $('#PTotalAssignOrder').html(parseFloat(TotalAssignOrder).toFixed(0));
                            $('#PTotalPackedOrder').html(parseFloat(TotalPackedOrder).toFixed(0));
                            $('#PTotalPendingOrder').html(parseFloat(TotalPendingOrder).toFixed(0));
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                    }
                   
                    if (us == 1) {
                        //var image = $("#imgName").val();
                        //var divToPrint = document.getElementById("PrintTable");
                        //var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        //mywindow.document.write('<html><head><style>#PrintTable tbody tr .right{text-align:right;} #PrintTable tbody tr .left{text-align:left;} #PrintTable tbody tr .price{text-align:right;}#PrintTable {border-collapse: collapse;width: 100%;}#PrintTable td, #PrintTable th {border: 1px solid black;padding: 8px;}#PrintTable tr:nth-child(even){background-color: #f2f2f2;}#PrintTable thead {padding-top: 12px;padding-bottom: 12px;background-color:#C8E8F9;color:black;}.text-right{text-align:right;}.text-left{text-align:left;}.text-center{text-align:center;}</style>');
                        //mywindow.document.write('</head><body>');
                        //if ($("#txtDateFrom").val() != "") {
                        //    mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;'><img src='/Img/logo/" + image + "' style='float:left;' height='70px' width='140px'/><h3 style='float:center;'>Packer Report By Summary</h3><h4 style='float:right;margin-right:10px;'>Date: " + $("#txtDateFrom").val() + " To " + $("#txtDateTo").val() + "</h4></div>")

                        //}
                        //else {
                        //    debugger
                        //    mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;'><img src='/Img/logo/" + image + "' style='float:left;' height='70px' width='140px'/><h3 style='float:center;'>Packer Report By Summary</h3><h4 style='float:right;margin-right:10px;'>Date: " + (new Date()).format("MM/dd/yyyy") + "</h4></div>")
                        //}
                        //mywindow.document.write(divToPrint.outerHTML);
                        //mywindow.document.write('</body></html>');
                        //mywindow.print();
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + (new Date()).format("MM/dd/yyyy hh:mm tt"));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtDateFrom").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtDateFrom").val() + " To " + $("#txtDateTo").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        //mywindow.document.write(divToPrint.outerHTML);

                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "PackerReportBySummary " + (new Date()).format("MM/ dd / yyyy hh: mm tt"),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblPackerReportSummary tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
$("#btnExport").click(function () {
    if ($('#tblPackerReportSummary tbody tr').length > 0) {
    CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});


