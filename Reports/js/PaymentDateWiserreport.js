﻿$(document).ready(function () {
    $('.date').pickadate({
        min:new Date('01/01/2019'),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    bindDropDowns();
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
});

function setdatevalidation(val) {
    var fdate = $("#txtSFromDate").val();
    var tdate = $("#txtSToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromDate").val(tdate);
        }
    }
}

function bindDropDowns() {
    $.ajax({
        type: "POST",
        url: "PaymentReportDateWise.aspx/GetPaymentMode",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlType option:not(:first)").remove();  //-------------------------------For Input Field
                    $.each(getData, function (index, item) {
                        $("#ddlType").append("<option value='" + getData[index].PId + "'>" + getData[index].PM + "</option>");
                    });
                    $("#ddlType").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Pagevalue(e) {
    GetPaymentDateWiseReport(parseInt($(e).attr("page")));
};
function GetPaymentDateWiseReport(PageIndex) {

    var data = {
        Type: $("#ddlType").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "PaymentReportDateWise.aspx/GetPaymentResport",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),

        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: PaymentDateWiseReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
function PaymentDateWiseReport(response) {
    if (response.d != 'false') {
        var xmldoc = $.parseXML(response.d);
        var PaymentReport = $(xmldoc).find("Table1");
        var OrderTotal = $(xmldoc).find("Table2");
        var TotalOrderAmount = 0, TotalDueAmount = 0, TotalPayAmount = 0, TotalApplyAmount = 0, TotalDueBalance = 0; TotalCreditAmount = 0;
        $("#tblpaymentReport tbody tr").remove();
        var html = '';
        if (PaymentReport.length > 0) {
            $("#EmptyTable").hide();
            var PaymentAutoId = '';
            $.each(PaymentReport, function (index) {
                if ($(this).find("Numberofspan").text() == '1') {
                    html += '<tr><td class="text-center">' + $(this).find("PaymentDate").text() + '</td>';
                    html += '<td class="text-center">' + $(this).find("OrderNo").text() + '</td>';
                    html += '<td class="text-right">' + $(this).find("orderamount").text() + '</td>';
                    html += '<td class="text-right">' + $(this).find("DueamountBefore").text() + '</td>';
                    html += '<td class="text-right">' + $(this).find("PayAmount").text() + '</td>';
                    html += '<td class="text-right">' + $(this).find("ApplyAmount").text() + '</td>';
                    html += '<td class="text-right">' + $(this).find("DueamountAfter").text() + '</td>';
                    html += '<td class="text-right">' + $(this).find("CreditAmount").text() + '</td>';
                    html += '<td class="text-center">' + $(this).find("PaymentMode").text() + '</td>';
                    html += '<td>' + $(this).find("ReceivedBy").text() + '</td>';
                    html += '<td>' + $(this).find("EmpAutoId").text() + '</td>';
                    html += '<td>' + $(this).find("PaymentRemarks").text() + '</td></tr>';
                    PaymentAutoId = $(this).find("PaymentAutoId").text()
                    TotalPayAmount += parseFloat($(this).find("PayAmount").text());
                    TotalCreditAmount += parseFloat($(this).find("CreditAmount").text());
                }
                else {
                    if (PaymentAutoId != $(this).find("PaymentAutoId").text()) {
                        html += '<tr><td class="text-center" rowspan=' + $(this).find("Numberofspan").text() + '>' + $(this).find("PaymentDate").text() + '</td>';
                        html += '<td class="text-center">' + $(this).find("OrderNo").text() + '</td>';
                        html += '<td class="text-right">' + $(this).find("orderamount").text() + '</td>';
                        html += '<td class="text-right">' + $(this).find("DueamountBefore").text() + '</td>';
                        html += '<td class="text-right" rowspan=' + $(this).find("Numberofspan").text() + '>' + $(this).find("PayAmount").text() + '</td>';
                        html += '<td class="text-right" >' + $(this).find("ApplyAmount").text() + '</td>';
                        html += '<td class="text-right">' + $(this).find("DueamountAfter").text() + '</td>';
                        html += '<td class="text-right" rowspan=' + $(this).find("Numberofspan").text() + '>' + $(this).find("CreditAmount").text() + '</td>';
                        if ($(this).find("PayMode").text() == "1") {
                            html += '<td class="text-center" rowspan=' + $(this).find("Numberofspan").text() + '><span>' + $(this).find("PaymentMode").text() + '</span></td>';
                        }
                        if ($(this).find("PayMode").text() == "2") {
                            html += '<td class="text-center" rowspan=' + $(this).find("Numberofspan").text() + '><span>' + $(this).find("PaymentMode").text() + '</span></td>';
                        }
                        if ($(this).find("PayMode").text() == "3") {
                            html += '<td class="text-center" rowspan=' + $(this).find("Numberofspan").text() + '><span>' + $(this).find("PaymentMode").text() + '</span></td>';
                        }
                        if ($(this).find("PayMode").text() == "4") {
                            html += '<td class="text-center" rowspan=' + $(this).find("Numberofspan").text() + '><span>' + $(this).find("PaymentMode").text() + '</span></td>';
                        }
                        if ($(this).find("PayMode").text() == "6") {
                            html += '<td class="text-center" rowspan=' + $(this).find("Numberofspan").text() + '><span>' + $(this).find("PaymentMode").text() + '</span></td>';
                        }
                        html += '<td rowspan=' + $(this).find("Numberofspan").text() + '>' + $(this).find("ReceivedBy").text() + '</td>';
                        html += '<td rowspan=' + $(this).find("Numberofspan").text() + '>' + $(this).find("EmpAutoId").text() + '</td>';
                        html += '<td rowspan=' + $(this).find("Numberofspan").text() + '>' + $(this).find("PaymentRemarks").text() + '</td></tr>';
                        PaymentAutoId = $(this).find("PaymentAutoId").text()
                        TotalPayAmount += parseFloat($(this).find("PayAmount").text());
                        TotalCreditAmount += parseFloat($(this).find("CreditAmount").text());
                    } else {
                        html += '<tr><td class="text-center">' + $(this).find("OrderNo").text() + '</td>';
                        html += '<td class="text-right">' + $(this).find("orderamount").text() + '</td>';
                        html += '<td class="text-right">' + $(this).find("DueamountBefore").text() + '</td>';
                        html += '<td class="text-right">' + $(this).find("ApplyAmount").text() + '</td>';
                        html += '<td class="text-right">' + $(this).find("DueamountAfter").text() + '</td>';
                        PaymentAutoId = $(this).find("PaymentAutoId").text()
                    }
                }
                TotalOrderAmount += parseFloat($(this).find("orderamount").text());
                TotalDueAmount += parseFloat($(this).find("DueamountBefore").text());
                TotalApplyAmount += parseFloat($(this).find("ApplyAmount").text());
                TotalDueBalance += parseFloat($(this).find("DueamountAfter").text());
            });
            $("#tblpaymentReport tbody").append(html);

            $("#TotalOrderAmount").html(parseFloat(TotalOrderAmount).toFixed(2));
            $("#TotalDueAmount").html(parseFloat(TotalDueAmount).toFixed(2));
            $("#TotalPayAmount").html(parseFloat(TotalPayAmount).toFixed(2));
            $("#TotalApplyAmount").html(parseFloat(TotalApplyAmount).toFixed(2));
            $("#TotalDueBalance").html(parseFloat(TotalDueBalance).toFixed(2));
            $("#TotalCreditAmount").html(parseFloat(TotalCreditAmount).toFixed(2));
        } else {
            // $("#EmptyTable").show();
            $("#TotalOrderAmount").html('0.00');
            $("#TotalDueAmount").html('0.00');
            $("#TotalPayAmount").html('0.00');
            $("#TotalApplyAmount").html('0.00');
            $("#TotalDueBalance").html('0.00');
            $("#TotalCreditAmount").html('0.00');
        }
    } else {

    }
    if ($(OrderTotal).find("orderamount").text() != '')
    {
        $("#TotalOrderAmounts").html(parseFloat($(OrderTotal).find("orderamount").text()).toFixed(2));
        $("#TotalDueAmounts").html(parseFloat($(OrderTotal).find("DueamountBefore").text()).toFixed(2));
        $("#TotalPayAmounts").html(parseFloat($(OrderTotal).find("PayAmount").text()).toFixed(2));
        $("#TotalApplyAmounts").html(parseFloat($(OrderTotal).find("ApplyAmount").text()).toFixed(2));
        $("#TotalDueBalances").html(parseFloat($(OrderTotal).find("DueamountAfter").text()).toFixed(2));
        $("#TotalCreditAmounts").html(parseFloat($(OrderTotal).find("CreditAmount").text()).toFixed(2));
    } else {

        $("#TotalOrderAmounts").html('0.00');
        $("#TotalDueAmounts").html('0.00');
        $("#TotalPayAmounts").html('0.00');
        $("#TotalApplyAmounts").html('0.00');
        $("#TotalDueBalances").html('0.00');
        $("#TotalCreditAmounts").html('0.00');
    }
    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
     
}
function CreateTable(us) {
    var image = $("#imgName").val();
    var data = {
        Type: $("#ddlType").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "PaymentReportDateWise.aspx/GetPaymentResport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var PrintDate = $(xmldoc).find("Table");
                    var PaymentReport = $(xmldoc).find("Table1");
                    var TotalOrderAmount = 0, TotalDueAmount = 0, TotalPayAmount = 0, TotalApplyAmount = 0, TotalDueBalance = 0; TotalCreditAmount = 0;
                    var html = '';
                    $("#PrintTable tbody tr").remove();
                    $("#PrintTable thead tr:Last-Child").clone(true);
                    var PaymentAutoId = '';
                    $.each(PaymentReport, function (index) {
                        if ($(this).find("Numberofspan").text() == '1') {
                            html += '<tr><td class="text-center">' + $(this).find("PaymentDate").text() + '</td>';
                            html += '<td class="text-center">' + $(this).find("OrderNo").text() + '</td>';
                            html += '<td class="text-right">' + $(this).find("orderamount").text() + '</td>';
                            html += '<td class="text-right">' + $(this).find("DueamountBefore").text() + '</td>';
                            html += '<td class="text-right">' + $(this).find("PayAmount").text() + '</td>';
                            html += '<td class="text-right">' + $(this).find("ApplyAmount").text() + '</td>';
                            html += '<td class="text-right">' + $(this).find("DueamountAfter").text() + '</td>';
                            html += '<td class="text-right">' + $(this).find("CreditAmount").text() + '</td>';
                            html += '<td class="text-center">' + $(this).find("PaymentMode").text() + '</td>';
                            html += '<td style="max-width: 120px;">' + $(this).find("ReceivedBy").text() + '</td>';
                            html += '<td style="max-width: 120px;">' + $(this).find("EmpAutoId").text() + '</td>';
                            html += '<td style="max-width: 120px;">' + $(this).find("PaymentRemarks").text() + '</td></tr>';
                            PaymentAutoId = $(this).find("PaymentAutoId").text();
                            TotalPayAmount += parseFloat($(this).find("PayAmount").text());
                            TotalCreditAmount += parseFloat($(this).find("CreditAmount").text());
                        }
                        else {
                            if (PaymentAutoId != $(this).find("PaymentAutoId").text()) {
                                html += '<tr><td class="text-center" rowspan=' + $(this).find("Numberofspan").text() + '>' + $(this).find("PaymentDate").text() + '</td>';
                                html += '<td class="text-center">' + $(this).find("OrderNo").text() + '</td>';
                                html += '<td class="text-right">' + $(this).find("orderamount").text() + '</td>';
                                html += '<td class="text-right">' + $(this).find("DueamountBefore").text() + '</td>';
                                html += '<td class="text-right" rowspan=' + $(this).find("Numberofspan").text() + '>' + $(this).find("PayAmount").text() + '</td>';
                                html += '<td class="text-right" >' + $(this).find("ApplyAmount").text() + '</td>';
                                html += '<td class="text-right">' + $(this).find("DueamountAfter").text() + '</td>';
                                html += '<td class="text-right" rowspan=' + $(this).find("Numberofspan").text() + '>' + $(this).find("CreditAmount").text() + '</td>';
                                if ($(this).find("PayMode").text() == "1") {
                                    html += '<td class="text-center" rowspan=' + $(this).find("Numberofspan").text() + '><span>' + $(this).find("PaymentMode").text() + '</span></td>';
                                }
                                if ($(this).find("PayMode").text() == "2") {
                                    html += '<td class="text-center" rowspan=' + $(this).find("Numberofspan").text() + '><span>' + $(this).find("PaymentMode").text() + '</span></td>';
                                }
                                if ($(this).find("PayMode").text() == "3") {
                                    html += '<td class="text-center" rowspan=' + $(this).find("Numberofspan").text() + '><span>' + $(this).find("PaymentMode").text() + '</span></td>';
                                }
                                if ($(this).find("PayMode").text() == "4") {
                                    html += '<td class="text-center" rowspan=' + $(this).find("Numberofspan").text() + '><span>' + $(this).find("PaymentMode").text() + '</span></td>';
                                } if ($(this).find("PayMode").text() == "6") {
                                    html += '<td class="text-center" rowspan=' + $(this).find("Numberofspan").text() + '><span>' + $(this).find("PaymentMode").text() + '</span></td>';
                                }
                                html += '<td rowspan=' + $(this).find("Numberofspan").text() + '>' + $(this).find("ReceivedBy").text() + '</td>';
                                html += '<td rowspan=' + $(this).find("Numberofspan").text() + '>' + $(this).find("EmpAutoId").text() + '</td>';
                                html += '<td rowspan=' + $(this).find("Numberofspan").text() + '>' + $(this).find("PaymentRemarks").text() + '</td></tr>';
                                PaymentAutoId = $(this).find("PaymentAutoId").text();
                                TotalPayAmount += parseFloat($(this).find("PayAmount").text());
                                TotalCreditAmount += parseFloat($(this).find("CreditAmount").text());
                            } else {
                                html += '<tr><td class="text-center">' + $(this).find("OrderNo").text() + '</td>';
                                html += '<td class="text-right">' + $(this).find("orderamount").text() + '</td>';
                                html += '<td class="text-right">' + $(this).find("DueamountBefore").text() + '</td>';
                                html += '<td class="text-right">' + $(this).find("ApplyAmount").text() + '</td>';
                                html += '<td class="text-right">' + $(this).find("DueamountAfter").text() + '</td>';
                                PaymentAutoId = $(this).find("PaymentAutoId").text()
                            }
                        }
                        TotalOrderAmount += parseFloat($(this).find("orderamount").text());
                        TotalDueAmount += parseFloat($(this).find("DueamountBefore").text());
                        TotalApplyAmount += parseFloat($(this).find("ApplyAmount").text());
                        TotalDueBalance += parseFloat($(this).find("DueamountAfter").text());
                    });
                    $("#PrintTable tbody").append(html);
                    $("#TotalOrderAmountw").html(parseFloat(TotalOrderAmount).toFixed(2));
                    $("#TotalDueAmountw").html(parseFloat(TotalDueAmount).toFixed(2));
                    $("#TotalPayAmountw").html(parseFloat(TotalPayAmount).toFixed(2));
                    $("#TotalApplyAmountw").html(parseFloat(TotalApplyAmount).toFixed(2));
                    $("#TotalDueBalancew").html(parseFloat(TotalDueBalance).toFixed(2));
                    $("#TotalCreditAmountw").html(parseFloat(TotalCreditAmount).toFixed(2));
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);                       
                        if ($("#txtSFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "By Order Payment" + (PrintDate.find('PrintDate').text()),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblpaymentReport tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
        if ($('#tblpaymentReport tbody tr').length > 0) {
    CreateTable(2);
        }
        else {
            toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
}
