﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    Employee();
    bindEmployeeType();
    GetOrderAverageReport();
});
function bindEmployeeType() {
    $.ajax({
        type: "POST",
        url: "OrderAverageReport.aspx/bindEmployeeType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlEmpType option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlEmpType").append("<option value='" + getData[index].TID + "'>" + getData[index].TN + "</option>");
                });
                $("#ddlEmpType").select2()
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Employee() {
    var autoid = 0;
    if ($("#ddlEmpType").val != "0") {
        autoid = $("#ddlEmpType").val();
    }
    var data = {
        AutoID: $("#ddlEmpType").val()
    }
    $.ajax({
        type: "POST",
        url: "OrderAverageReport.aspx/bindEmployee",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlEmployee option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlEmployee").append("<option value='" + getData[index].ETId + "'>" + getData[index].Name + "</option>");
                });
                $("#ddlEmployee").select2()
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    })
}
/*-------------------------------------------------------Get Order Average Report----------------------------------------------------------*/
function GetOrderAverageReport() {
    var data = {
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        EmpType: $("#ddlEmpType").val(),
        Employee: $("#ddlEmployee").val()
    };
    $.ajax({
        type: "POST",
        url: "OrderAverageReport.aspx/GetOrderAverageReport",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successOrderAverageReport,
        error: function (result) {
            swal("Error!", result.d, "error");
        },
        failure: function (result) {
            swal("Error!", result.d, "error");
        }
    });
}

function successOrderAverageReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {

            var xmldoc = $.parseXML(response.d);
            var OrderAverage = $(xmldoc).find("Table");
            $("#tblOrderAverageReport tbody tr").remove();
            var row = $("#tblOrderAverageReport thead tr:last-child").clone(true);
            if (OrderAverage.length > 0) {
                $("#EmptyTable").hide();
                $.each(OrderAverage, function () {
                    $(".WDay", row).text($(this).find("WeekDay").text());
                    $(".WDCount", row).text($(this).find("WeekDayCount").text());

                    $(".TOrders", row).text($(this).find("TotalOrders").text());
                    $(".AWOrders", row).text($(this).find("AvgWeeklyOrders").text());

                    $(".PTotal", row).text($(this).find("PackerOrder").text());
                    $(".PAvg", row).text($(this).find("AvgPacker").text());

                    $(".DTotal", row).text($(this).find("DeliverOrder").text());
                    $(".DAvg", row).text($(this).find("AvgDeliver").text());

                    $(".ATotal", row).text($(this).find("ClosedOrder").text());
                    $(".AAvg", row).text($(this).find("AvgClosed").text());

                    $(".CTotal", row).text($(this).find("CancelOrder").text());
                    $(".CAvg", row).text($(this).find("AvgCancel").text());

                    $("#tblOrderAverageReport tbody").append(row);
                    row = $("#tblOrderAverageReport tbody tr:last-child").clone(true);
                });
            } else {
                $("#EmptyTable").show();
            }
            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }

}




function PrintElem() {
    if ($('#tblOrderAverageReport tbody tr').length > 0) {
        var image = $("#imgName").val();
        var divToPrint = document.getElementById("tblOrderAverageReport");
        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
        if ($("#txtSFromDate").val() != "") {
            mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Sale Report By Order Average</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + (new Date()).format("MM/dd/yyyy hh:mm tt") + "</span><br/><span class='DateRangeCSS' style='float:center;margin-top: 0.5%; font-size: 9px; color: black; font-weight: bold;'>Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val() + "</span><br/></div>")
        }
        else {
           
            mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Sale Report By Order Average</span><span style='float:right;margin-right:10px;;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + (new Date()).format("MM/dd/yyyy hh:mm tt") + "</span></div>")
        }
        // mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/app-assets/css/custom.css"/></head><body>');
        $("#PrintDate").text("Print Date :" + (new Date()).format("MM/dd/yyyy hh:mm tt"));
        $("#PrintLogo").attr("src", "/Img/logo/" + image);
        $("#tblOrderAverageReport").addClass("PrintMyTableHeader");
        mywindow.document.write(divToPrint.outerHTML);
        mywindow.document.write('</body></html>');
        setTimeout(function () {
            mywindow.print();
        }, 2000);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    $("#tblOrderAverageReport").removeClass("PrintMyTableHeader");

}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderAverageReport tbody tr').length > 0) {
        $("#tblOrderAverageReport").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: "Sale Report By Order Average",
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}