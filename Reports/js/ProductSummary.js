﻿$(document).ready(function () {

    $('#txtFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });

    $('#txtToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });

})
function Pagevalue(e) {
    getRecords(parseInt($(e).attr("page")));
}

$('#btnSearch').click(function () {
    getRecords(1);
})

function getRecords(PageIndex) {

    var data = {
        ProductId: $("#txtSProductId").val(),
        ProductName: $("#txtProductName").val(),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),      
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/WProductSummaryReport.asmx/ProductSummaryReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'false') {
                    location.href = "/Default.aspx"
                }
                else {
                    SuccessPackerResport(response);
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function SuccessPackerResport(response) {
    var xmldoc = $.parseXML(response.d);
    var PackerResportDetail = $(xmldoc).find("Table");
    $("#tblProductSummaryReport tbody tr").remove();
    var row = $("#tblProductSummaryReport thead tr").clone(true);
    if (PackerResportDetail.length > 0) {
        $("#EmptyTable").hide();
        $("#tblProductSummaryReport thead tr").show();
        $.each(PackerResportDetail, function (index) {           
            $(".ProductId", row).html($(this).find("ProductId").text());
            $(".ProductName", row).html($(this).find("ProductName").text());
            $(".NetSales", row).html($(this).find("TotalNetSales").text());
            $("#tblProductSummaryReport tbody").append(row);
            row = $("#tblProductSummaryReport tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }

    var pager = $(xmldoc).find("Table1");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}




function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        ProductId: $("#txtSProductId").val(),
        ProductName: $("#txtProductName").val(),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageSize: $("#ddlPageSize").val(),
        //PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/WProductSummaryReport.asmx/ProductSummaryReportExport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        //url: "MLQtySummaryReport.aspx/BindReport",
        //data: "{'CustomerAutoId':'" + $('#txtSProductId').val() + "','FromDate':'" + $('#txtFromDate').val() + "','ToDate':'" + $('#txtToDate').val() + "','ProductName':'" + $('#txtProductName').val() + "','PageIndex':'" + 1 + "','PageSize':'" + 0 + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
           
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var Report = $(xmldoc).find("Table");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true); 
                    if (Report.length > 0) {
                        $("#EmptyTable").hide();
                        $("#PrintTable thead tr").show();
                        $.each(Report, function (index) {
                            $(".ProductId", row).html($(this).find("ProductId").text());
                            $(".ProductName", row).html($(this).find("ProductName").text());
                            $(".NetSales", row).html($(this).find("TotalNetSales").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });

                        if (us == 1) {
                            var image = $("#imgName").val();
                            var divToPrint = document.getElementById("PrintTable");
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style>#PrintTable tbody tr .right{text-align:right;} #PrintTable tbody tr .left{text-align:left;} #PrintTable tbody tr .price{text-align:right;}#PrintTable {border-collapse: collapse;width: 100%;}#PrintTable td, #PrintTable th {border: 1px solid black;padding: 8px;}#PrintTable tr:nth-child(even){background-color: #f2f2f2;}#PrintTable thead {padding-top: 12px;padding-bottom: 12px;background-color:#C8E8F9;color:black;}.text-right{text-align:right;}.text-left{text-align:left;}.text-center{text-align:center;}</style>');
                            mywindow.document.write('</head><body>');
                            if ($("#txtFromDate").val() != "") {
                                mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;'><img src='/Img/logo/" + image + "' style='float:left;' height='70px' width='140px'/><h3 style='float:center;'>Product Summary Report</h3><h4 style='float:right;margin-right:10px;'>Date: " + $("#txtFromDate").val() + " To " + $("#txtToDate").val() + "</h4></div>")

                            }
                            else {                                
                                mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;'><img src='/Img/logo/" + image + "' style='float:left;' height='70px' width='140px'/><h3 style='float:center;'>Product Summary Report</h3><h4 style='float:right;margin-right:10px;'>Date: " + (new Date()).format("MM/dd/yyyy") + "</h4></div>")
                            }
                            mywindow.document.write(divToPrint.outerHTML);
                            mywindow.document.write('</body></html>');
                            mywindow.print();

                        }
                        if (us == 2) {                         
                            $("#PrintTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "ProductSummaryReport",
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });
                        }
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    CreateTable(1);

}
/*---------------Export To Excel---------------------------*/
$("#btnExport").click(function () {
    CreateTable(2);
});