﻿$(document).ready(function () {
    bindDropDown();
    $('#txtSFromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    //var d = new Date();
    //var month = d.getMonth() + 1;
    //var day = d.getDate();
    //var output = (('' + month).length < 2 ? '0' : '') + month + '/' + (('' + day).length < 2 ? '0' : '') + day + '/' + d.getFullYear();
    //$("#txtSFromDate").val(output);
    //$("#txtSToDate").val(output);
});
function setdatevalidation(val) {
    var fdate = $("#txtOrdCloseFromDate").val();
    var tdate = $("#txtOrdCloseToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtOrdCloseToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtOrdCloseFromDate").val(tdate);
        }
    }
}
function bindDropDown() {
    $.ajax({
        type: "POST",
        url: "ProductStatusSaleReportBySalesPerson.aspx/bindDropDown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            if (response.d != "Session Expired") {

                $("#ddlProduct option:not(:first)").remove();
                $.each(getData[0].PList, function (i, item) {
                    $("#ddlProduct").append("<option value='" + item.ProductId + "'>" + item.PName + "</option>");
                });
                $("#ddlProduct").select2();

                $("#ddlCustomer option:not(:first)").remove();
                $.each(getData[0].Customer, function (i, item) {
                    $("#ddlCustomer").append("<option value='" + item.CUID + "'>" + item.CUN + "</option>");
                });
                $("#ddlCustomer").select2();


                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(getData[0].SalesPerson, function (i, sp) {
                    $("#ddlSalesPerson").append("<option value='" + sp.AutoId + "'>" + sp.SalesPersonName + "</option>");
                });
                $('#ddlSalesPerson').attr('multiple','multiple')
                $("#ddlSalesPerson").select2({
                    placeholder: 'All Sales Person',
                    allowClear: true                    
                });

            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function bindCustomer() {
    var SalesPersons = '0,';
    $("#ddlSalesPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPersons = $(this).val() + ',';
        } else {
            SalesPersons += $(this).val() + ',';
        }
    });
    if (SalesPersons == '0,') {
        SalesPersons = '0';
    }
    $.ajax({
        type: "POST",
        url: "ProductStatusSaleReportBySalesPerson.aspx/bindCustomer",
        data: "{'SalesPersons':'" + SalesPersons + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);
            if (response.d != "Session Expired") {
                $("#ddlCustomer option:not(:first)").remove();
                $.each(getData[0].Customer, function (i, item) {
                    $("#ddlCustomer").append("<option value='" + item.CUID + "'>" + item.CUN + "</option>");
                });
                $("#ddlCustomer").select2();
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
}
function getSearchdata() {
    BindReport(1);
}
function BindReport(PageIndex) {
    debugger
    var SalesPersons = '0,';
    $("#ddlSalesPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPersons = $(this).val() + ',';
        } else {
            SalesPersons += $(this).val() + ',';
        }
    });
    if (SalesPersons == '0,') {
        SalesPersons = '0';
    }
    if (CustcheckRequiredField()) {
        var data = {
            SalesPersonAutoId: SalesPersons,
            ProductAutoId: $("#ddlProduct").val(),
            CustomerAutoId: $("#ddlCustomer").val(),
            FromDate: $("#txtSFromDate").val(),
            ToDate: $("#txtSToDate").val(),
            PageSize: $("#ddlPageSize").val(),
            PageIndex: PageIndex
        };
        $.ajax({
            type: "POST",
            url: "ProductStatusSaleReportBySalesPerson.aspx/BindReport",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == 'false') {
                        location.href = "/Default.aspx"
                    }
                    else {
                        ProductStatusSaleReport(response);
                    }
                }
                else {
                    location.href = "/";
                }
            },
            error: function (result) {
                alert(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function ProductStatusSaleReport(response) {

    var xmldoc = $.parseXML(response.d);
    var ProductStatusSaleReportDetail = $(xmldoc).find("Table1");
    $("#tblProductStatusSaleReport tbody tr").remove();
    var row = $("#tblProductStatusSaleReport thead tr").clone(true);
    if (ProductStatusSaleReportDetail.length > 0) {
        $("#EmptyTable").hide();
        $("#tblProductStatusSaleReport thead tr").show();
        var naw = 0, processed = 0, AddOn = 0, Packed = 0, AddOnPacked = 0, ReadyToShip = 0, Shipped = 0, Delivered = 0, Close = 0, Total = 0;
        $.each(ProductStatusSaleReportDetail, function (index) {
            $(".T_SalesRep", row).text($(this).find("SalesRep").text());
            $(".T_CustomerName", row).text($(this).find("CustomerName").text());
            $(".T_ProductId", row).text($(this).find("ProductId").text());
            $(".T_ProductName", row).text($(this).find("ProductName").text());
            $(".T_New", row).text($(this).find("New").text());
            $(".T_Processed", row).text($(this).find("Processed").text());
            $(".T_AddOn", row).text($(this).find("Add-On").text());
            $(".T_Packed", row).text($(this).find("Packed").text());

            $(".T_AddOnPacked", row).text($(this).find("Add-On-Packed").text());
            $(".T_ReadyToShip", row).text($(this).find("ReadyToShip").text());
            $(".T_Shipped", row).text($(this).find("Shipped").text());
            $(".T_Delivered", row).text($(this).find("Delivered").text());
            $(".T_Closed", row).text($(this).find("Close").text());
            $(".T_Total", row).text($(this).find("Total").text());
            naw += parseFloat($(this).find("New").text());
            processed += parseFloat($(this).find("Processed").text());
            AddOn += parseFloat($(this).find("Add-On").text());
            Packed += parseFloat($(this).find("Packed").text());
            AddOnPacked += parseFloat($(this).find("Add-On-Packed").text());
            ReadyToShip += parseFloat($(this).find("ReadyToShip").text());
            Shipped += parseFloat($(this).find("Shipped").text());
            Delivered += parseFloat($(this).find("Delivered").text());
            Close += parseFloat($(this).find("Close").text());
            Total += parseFloat($(this).find("Total").text());
            $("#tblProductStatusSaleReport tbody").append(row);
            row = $("#tblProductStatusSaleReport tbody tr:last").clone(true);
        });
        $('#tNew').html(naw.toFixed(0));
        $('#tProcessed').html(processed.toFixed(0));
        $('#tAddOn').html(AddOn.toFixed(0));
        $('#tPacked').html(Packed.toFixed(0));
        $('#tAddOnPacked').html(AddOnPacked.toFixed(0));
        $('#tReadyToShip').html(ReadyToShip.toFixed(0));
        $('#tShipped').html(Shipped.toFixed(0));
        $('#tDelivered').html(Delivered.toFixed(0));
        $('#tClose').html(Close.toFixed(0));
        $('#tTotal').html(Total.toFixed(0));
    }
    else {
        $('#tNew').html('0');
        $('#tProcessed').html('0');
        $('#tAddOn').html('0');
        $('#tPacked').html('0');
        $('#tAddOnPacked').html('0');
        $('#tReadyToShip').html('0');
        $('#tShipped').html('0');
        $('#tDelivered').html('0');
        $('#tClose').html('0');
        $('#tTotal').html('0');
    }
    var grandTotal = $(xmldoc).find("Table2");
    if (grandTotal.length > 0) {
        $('#oNew').html(parseFloat($(grandTotal).find("New").text()).toFixed(0));
        $('#oProcessed').html(parseFloat($(grandTotal).find("Processed").text()).toFixed(0));
        $('#oAddOn').html(parseFloat($(grandTotal).find("AddOn").text()).toFixed(0));
        $('#oPacked').html(parseFloat($(grandTotal).find("Packed").text()).toFixed(0));
        $('#oAddOnPacked').html(parseFloat($(grandTotal).find("AddOnPacked").text()).toFixed(0));
        $('#oReadyToShip').html(parseFloat($(grandTotal).find("ReadyToShip").text()).toFixed(0));
        $('#oShipped').html(parseFloat($(grandTotal).find("Shipped").text()).toFixed(0));
        $('#oDelivered').html(parseFloat($(grandTotal).find("Delivered").text()).toFixed(0));
        $('#oClose').html(parseFloat($(grandTotal).find("Closed").text()).toFixed(0));
        $('#oTotal').html(parseFloat($(grandTotal).find("Total").text()).toFixed(0));
    }
    else {
        $('#oNew').html('0');
        $('#oProcessed').html('0');
        $('#oAddOn').html('0');
        $('#oPacked').html('0');
        $('#oAddOnPacked').html('0');
        $('#oReadyToShip').html('0');
        $('#oShipped').html('0');
        $('#oDelivered').html('0');
        $('#oClose').html('0');
        $('#oTotal').html('0');
    }

    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
    if (Number($("#ddlPageSize").val()) == '0') {
        $("#trTotal").hide();
    }
    else if (Number(pager.find("RecordCount").text()) < Number(pager.find("PageSize").text())) {
        $("#trTotal").hide();
    }
    else {
        $("#trTotal").show();
    }
}
function CustcheckRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {

        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');

        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {
            $(this).removeClass('border-warning');
            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}

function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var SalesPersons = '0,';
    $("#ddlSalesPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPersons = $(this).val() + ',';
        } else {
            SalesPersons += $(this).val() + ',';
        }
    });
    if (SalesPersons == '0,') {
        SalesPersons = '0';
    }
    var data = {
        SalesPersonAutoId: SalesPersons,
        ProductAutoId: $("#ddlProduct").val(),
        CustomerAutoId: $("#ddlCustomer").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "ProductStatusSaleReportBySalesPerson.aspx/BindReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ProductStatusSaleReportDetail = $(xmldoc).find("Table1");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    var naw = 0, processed = 0, AddOn = 0, Packed = 0, AddOnPacked = 0, ReadyToShip = 0, Shipped = 0, Delivered = 0, Close = 0, Total = 0;
                    if (ProductStatusSaleReportDetail.length > 0) {
                        $.each(ProductStatusSaleReportDetail, function () {
                            $(".T_SalesRep", row).text($(this).find("SalesRep").text());
                            $(".T_CustomerName", row).text($(this).find("CustomerName").text());
                            $(".T_ProductId", row).text($(this).find("ProductId").text());
                            $(".T_ProductName", row).text($(this).find("ProductName").text());
                            $(".T_New", row).text($(this).find("New").text());
                            $(".T_Processed", row).text($(this).find("Processed").text());
                            $(".T_AddOn", row).text($(this).find("Add-On").text());
                            $(".T_Packed", row).text($(this).find("Packed").text());

                            $(".T_AddOnPacked", row).text($(this).find("Add-On-Packed").text());
                            $(".T_ReadyToShip", row).text($(this).find("ReadyToShip").text());
                            $(".T_Shipped", row).text($(this).find("Shipped").text());
                            $(".T_Delivered", row).text($(this).find("Delivered").text());
                            $(".T_Closed", row).text($(this).find("Close").text());
                            $(".T_Total", row).text($(this).find("Total").text());
                            naw += parseFloat($(this).find("New").text());
                            processed += parseFloat($(this).find("Processed").text());
                            AddOn += parseFloat($(this).find("Add-On").text());
                            Packed += parseFloat($(this).find("Packed").text());
                            AddOnPacked += parseFloat($(this).find("Add-On-Packed").text());
                            ReadyToShip += parseFloat($(this).find("ReadyToShip").text());
                            Shipped += parseFloat($(this).find("Shipped").text());
                            Delivered += parseFloat($(this).find("Delivered").text());
                            Close += parseFloat($(this).find("Close").text());
                            Total += parseFloat($(this).find("Total").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                        $('#ptNew').html(naw.toFixed(0));
                        $('#ptProcessed').html(processed.toFixed(0));
                        $('#ptAddOn').html(AddOn.toFixed(0));
                        $('#ptPacked').html(Packed.toFixed(0));
                        $('#ptAddOnPacked').html(AddOnPacked.toFixed(0));
                        $('#ptReadyToShip').html(ReadyToShip.toFixed(0));
                        $('#ptShipped').html(Shipped.toFixed(0));
                        $('#ptDelivered').html(Delivered.toFixed(0));
                        $('#ptClose').html(Close.toFixed(0));
                        $('#ptTotal').html(Total.toFixed(0));
                    }


                    if (us == 1) {
                        var PrintTable = $(xmldoc).find("Table");
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + (PrintTable.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtSFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "Product Status Sale Report By Sales Person",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblProductStatusSaleReport tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblProductStatusSaleReport tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}