﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    BindCustomerType();
    BindCustomer();

});

function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
};

/*---------------------------------Bind Customer Category-----------------------------------*/
function BindCustomerType() {
    $.ajax({
        type: "POST",
        url: "OpenBalanceReportDetail.aspx/BindCustomerType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlCustomertype option:not(:first)").remove();
                $.each(getData[0].CType, function (index, item) {
                    $("#ddlCustomertype").append("<option value='" + item.CId + "'>" + item.Ct + "</option>");
                });
                $("#ddlCustomertype").select2();

                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(getData[0].SalesList, function (index, item) {
                    $("#ddlSalesPerson").append("<option value = " + item.SID + " >" + item.SP + "</option>");
                });
                $("#ddlSalesPerson").select2();

            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}


/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function BindCustomer() {
    var Customertype = $("#ddlCustomertype").val();
    var SalesPerson = $("#ddlSalesPerson").val();
    $.ajax({
        type: "POST",
        url: "OpenBalanceReportDetail.aspx/BindCustomer",
        data: "{'Customertype':'" + Customertype + "','SalesPersonAutoId':'" + SalesPerson + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlCustomer option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    console.log(getData[index])
                    $("#ddlCustomer").append("<option value='" + getData[index].CuId + "'>" + getData[index].CUN + "</option>");
                });
                $("#ddlCustomer").select2();


            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function BindReport(PageIndex) {

    var data = {
        CustomerType: $("#ddlCustomertype").val(),
        CustomerAutoId: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        PageSize: $("#ddlPaging").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "OpenBalanceReportDetail.aspx/OpenBalanceReportDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI()
        },
        success: successOrderWiseOpenBalanceReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successOrderWiseOpenBalanceReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {

            var xmldoc = $.parseXML(response.d);
            var productBarcode = $(xmldoc).find("Table");

            var grandTotal = $(xmldoc).find("Table2");


            $("#tblOrderList tbody tr").remove();
            var TotalOrderAmt = 0, TotalPaid = 0, AmtDue = 0, NoOfOrders = 0, thirtyDays = 0, sixDays = 0, ninetyDays = 0, ninetyAbove = 0;
            var row = $("#tblOrderList thead tr").clone(true);
            if (productBarcode.length > 0) {
                $("#EmptyTable").hide();
                $.each(productBarcode, function () {
                    console.log()
                    $(".CustomerName", row).text($(this).find("CustomerName").text());
                    $(".0-30-Days", row).text($(this).find("Days30").text());
                    $(".31-60-Days", row).text($(this).find("Days60").text());
                    $(".61-90-Days", row).text($(this).find("Days90").text());
                    $(".90-Above", row).text($(this).find("Above90").text());
                    $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                    $(".NoofOrder", row).text($(this).find("NoofOrder").text());
                    $(".TotalOrderAmount", row).text($(this).find("OrderTotal").text());
                    $(".TotalPaid", row).text($(this).find("TotalPaid").text());
                    $(".TotalDue", row).html($(this).find("AmtDue").text());
                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);

                    thirtyDays += parseFloat($(this).find('Days30').text());
                    sixDays += parseFloat($(this).find('Days60').text());
                    ninetyDays += parseFloat($(this).find('Days90').text());
                    ninetyAbove += parseFloat($(this).find('Above90').text());
                    NoOfOrders += parseFloat($(this).find('NoofOrder').text());
                    TotalOrderAmt += parseFloat($(this).find('OrderTotal').text());
                    TotalPaid += parseFloat($(this).find('TotalPaid').text());
                    AmtDue += parseFloat($(this).find('AmtDue').text());
                });
                $('#30Days').html(thirtyDays.toFixed(2));
                $('#60Days').html(sixDays.toFixed(2));
                $('#90Days').html(ninetyDays.toFixed(2));
                $('#Above90').html(ninetyAbove.toFixed(2));
                $('#NoOfOrders').html(NoOfOrders);
                $('#TotalOrderAmount').html(TotalOrderAmt.toFixed(2));
                $('#TotalPaid').html(TotalPaid.toFixed(2));
                $('#AmtDue').html(AmtDue.toFixed(2));

                //For overall total
                thirtyDays = parseFloat($(grandTotal).find('Days30').text());
                sixDays = parseFloat($(grandTotal).find('Days60').text());
                ninetyDays = parseFloat($(grandTotal).find('Days90').text());
                ninetyAbove = parseFloat($(grandTotal).find('Above90').text());
                NoOfOrders = parseFloat($(grandTotal).find('NoofOrder').text());
                TotalOrderAmt = parseFloat($(grandTotal).find('OrderTotal').text());
                TotalPaid = parseFloat($(grandTotal).find('TotalPaid').text());
                AmtDue = parseFloat($(grandTotal).find('AmtDue').text());

                $('#gt30Days').html(thirtyDays.toFixed(2));
                $('#gt60Days').html(sixDays.toFixed(2));
                $('#gt90Days').html(ninetyDays.toFixed(2));
                $('#gtAbove90').html(ninetyAbove.toFixed(2));
                $('#gtNoOfOrders').html(NoOfOrders);
                $('#gtTotalOrderAmount').html(TotalOrderAmt.toFixed(2));
                $('#gtTotalPaid').html(TotalPaid.toFixed(2));
                $('#gtAmtDue').html(AmtDue.toFixed(2));
            } else {
                // $("#EmptyTable").show();
                $('#30Days').html('0.00');
                $('#60Days').html('0.00');
                $('#90Days').html('0.00');
                $('#Above90').html('0.00');
                $('#NoOfOrders').html("0");
                $('#TotalOrderAmount').html("0.00");
                $('#TotalPaid').html("0.00");
                $('#AmtDue').html("0.00");
                $('#gt30Days').html('0.00');
                $('#gt60Days').html('0.00');
                $('#gt90Days').html('0.00');
                $('#gtAbove90').html('0.00');
                $('#gtNoOfOrders').html('0.00');
                $('#gtTotalOrderAmount').html('0.00');
                $('#gtTotalPaid').html('0.00');
                $('#gtAmtDue').html('0.00');
            } 
            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}


$("#ddlPaging").change(function () {

    BindReport(1);

})

function CreateTable(us) {
    var image = $("#imgName").val();
    var data = {
        CustomerType: $("#ddlCustomertype").val(),
        CustomerAutoId: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "OpenBalanceReportDetail.aspx/OpenBalanceReportDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var productBarcode = $(xmldoc).find("Table");
                    var grandTotal = $(xmldoc).find("Table2");
                    var PrintTime = $(xmldoc).find("Table3");
                    var Printingtime = ($(PrintTime).find("PrintTime").text());
                    var TotalOrderAmt = 0, TotalPaid = 0, AmtDue = 0, NoOfOrders = 0, thirtyDays = 0, sixDays = 0, ninetyDays = 0, ninetyAbove = 0;
                    $(".PrintTable1 tbody tr").remove();
                    var row = $(".PrintTable1 thead tr").clone(true);
                    if (productBarcode.length > 0) {
                        $.each(productBarcode, function () {
                            $(".PCustomerName", row).text($(this).find("CustomerName").text());
                            $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                            $(".P0-30-Days", row).text($(this).find("Days30").text());
                            $(".P31-60-Days", row).text($(this).find("Days60").text());
                            $(".P61-90-Days", row).text($(this).find("Days90").text());
                            $(".P90-Above", row).text($(this).find("Above90").text());
                            $(".PNoofOrder", row).text($(this).find("NoofOrder").text());
                            $(".PTotalOrderAmount", row).text($(this).find("OrderTotal").text());
                            $(".PTotalPaid", row).text($(this).find("TotalPaid").text());
                            $(".PTotalDue", row).html($(this).find("AmtDue").text());
                            $(".PrintTable1 tbody").append(row);
                            row = $(".PrintTable1 tbody tr:last").clone(true);

                            thirtyDays += parseFloat($(this).find('Days30').text());
                            sixDays += parseFloat($(this).find('Days60').text());
                            ninetyDays += parseFloat($(this).find('Days90').text());
                            ninetyAbove += parseFloat($(this).find('Above90').text());
                            NoOfOrders += parseFloat($(this).find('NoofOrder').text());
                            TotalOrderAmt += parseFloat($(this).find('OrderTotal').text());
                            TotalPaid += parseFloat($(this).find('TotalPaid').text());
                            AmtDue += parseFloat($(this).find('AmtDue').text());
                        });

                        thirtyDays = parseFloat($(grandTotal).find('Days30').text());
                        sixDays = parseFloat($(grandTotal).find('Days60').text());
                        ninetyDays = parseFloat($(grandTotal).find('Days90').text());
                        ninetyAbove = parseFloat($(grandTotal).find('Above90').text());
                        NoOfOrders = parseFloat($(grandTotal).find('NoofOrder').text());
                        TotalOrderAmt = parseFloat($(grandTotal).find('OrderTotal').text());
                        TotalPaid = parseFloat($(grandTotal).find('TotalPaid').text());
                        AmtDue = parseFloat($(grandTotal).find('AmtDue').text());



                        $('#Pgt30Days').html(thirtyDays.toFixed(2));
                        $('#Pgt60Days').html(sixDays.toFixed(2));
                        $('#Pgt90Days').html(ninetyDays.toFixed(2));
                        $('#PgtAbove90').html(ninetyAbove.toFixed(2));
                        $('#PgtNoOfOrders').html(NoOfOrders);
                        $('#PgtTotalOrderAmount').html(TotalOrderAmt.toFixed(2));
                        $('#PgtTotalPaid').html(TotalPaid.toFixed(2));
                        $('#PgtAmtDue').html(AmtDue.toFixed(2));
                    }
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'PRINT', 'height=400,width=600');
                        mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + " " + Printingtime);
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        mywindow.document.write($(PrintTable).clone().html());
                        mywindow.document.write('</body></html>');
                        try {
                            setTimeout(function () {
                                mywindow.print();
                            }, 3000);
                        } catch (e) {
                            setTimeout(function () {
                                mywindow.print();
                            }, 10000);
                        }
                    }

                    if (us == 2) {
                        $(".PrintTable1").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "BalanceReportByDue" + (new Date()).format("MM/dd/yyyy hh:mm tt"),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
};



