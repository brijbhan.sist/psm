﻿var row1 = "";
$(document).ready(function () {
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    BindSalesPerson();

    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
});


function setdatevalidation(val) {
    var fdate = $("#txtSFromDate").val();
    var tdate = $("#txtSToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromDate").val(tdate);
        }
    }
}

function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
};

/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function BindSalesPerson() {
    $.ajax({
        type: "POST",
        url: "OP_SalesBySalesPerson.aspx/BindSalesPerson",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlSalesPerson").append("<option value='" + getData[index].SId + "'>" + getData[index].SP + "</option>");
                });
                $("#ddlSalesPerson").select2({
                    placeholder: 'All Sales Person',
                    allowClear: true
                });
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}

function BindReport(PageIndex) {
    var d1 = new Date($('#txtSFromDate').val());
    var d2 = new Date($('#txtSToDate').val());
   // var datediff = monthDiff(d1, d2);

    if ($("#txtSFromDate").val() == "" && $("#txtSToDate").val() == "") {
        toastr.error('Date field is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtSFromDate").addClass("border-warning");
        $("#txtSToDate").addClass("border-warning");
    }
    //else if (Number(datediff) > 1) {
    //    toastr.error('Date difference cannot be greater than 1 month.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    //}
    else {
        var SalesPersons = 0;
        $("#ddlSalesPerson option:selected").each(function (i) {
            if (i == 0) {
                SalesPersons = $(this).val() + ',';
            } else {
                SalesPersons += $(this).val() + ',';
            }
        });
        if (SalesPersons == '0,') {
            SalesPersons = '0';
        }
        var data = {
            SalesPerson: SalesPersons,
            FromDate: $("#txtSFromDate").val(),
            ToDate: $("#txtSToDate").val(),
            PageSize: $("#ddlPaging").val() || 10,
            PageIndex: PageIndex
        };
        $.ajax({
            type: "POST",
            url: "OP_SalesBySalesPerson.aspx/SaleBySalesPersonDetail",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: successBindSalesPersonReport,
            error: function (result) {
                alert(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}

function successBindSalesPersonReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var DropDown = $.parseJSON(response.d);

            var row = $("#tblOrderList thead tr").clone(true);
            var GrandAmount = 0.00, NoOfOrders = 0.00, NoofPOSOrders = 0.00, POSGrandAmount = 0.00; NoOfMigrateOrder = 0, MigrateSaleAmount = 0.00
            for (var i = 0; i < DropDown.length; i++) {
                $("#tblOrderList tbody tr").remove();
                var AllDropDownList = DropDown[i];
                var OrderList = AllDropDownList.OrderList;
                if (OrderList.length > 0) {
                    $("#EmptyTable").hide();
                    for (var j = 0; j < OrderList.length; j++) {
                        var List = OrderList[j];

                        $(".EmployeeName", row).text(List.EmployeeName);
                        $(".NoOfOrders", row).text(List.NoOfOrders);
                        $(".MigratedOrders", row).text(List.MigratedOrder);
                        $(".MigratedSaleAmount", row).text(parseFloat(List.MigratedSaleAmount).toFixed(2));
                        $(".TotalOrderAmount", row).text(parseFloat(List.GrandTotal).toFixed(2));
                        $(".NoofPOSOrders", row).text(List.NoofPOSOrders);
                        $(".POSTotalAmount", row).text(parseFloat(List.POSTotalAmount).toFixed(2));
                        $(".TotalOrders", row).text(parseInt(List.NoOfOrders) + parseInt(List.NoofPOSOrders) + parseInt(List.MigratedOrder));
                        $(".OrderTotal", row).text(parseFloat(parseFloat(List.GrandTotal) + parseFloat(List.POSTotalAmount) + parseFloat(List.MigratedSaleAmount)).toFixed(2));

                        NoOfOrders += parseInt(List.NoOfOrders);
                        NoofPOSOrders += parseInt(List.NoofPOSOrders);
                        GrandAmount += parseFloat(List.GrandTotal);
                        POSGrandAmount += parseFloat(List.POSTotalAmount);
                        NoOfMigrateOrder += parseFloat(List.MigratedOrder);
                        MigrateSaleAmount += parseFloat(List.MigratedSaleAmount);
                        $("#tblOrderList tbody").append(row);
                        row = $("#tblOrderList tbody tr:last").clone(true);
                    }
                    if (NoOfOrders != 0) {
                        $('#avgSalesPersonAmount').html(parseFloat(GrandAmount / NoOfOrders).toFixed(2) || 0.00);
                    }
                    if (NoofPOSOrders != 0) {
                        $('#avgPOSAmount').html(parseFloat(POSGrandAmount / NoofPOSOrders).toFixed(2) || 0.00);
                    }
                    if (NoOfMigrateOrder != 0) {
                        $('#avgMigratedSaleAmount').html(parseFloat(MigrateSaleAmount / NoOfMigrateOrder).toFixed(2) || 0.00);
                    }
                    if (NoOfOrders + NoofPOSOrders + NoOfMigrateOrder != 0) {
                        $('#AVGPiece').html(parseFloat((GrandAmount + POSGrandAmount + MigrateSaleAmount) / (NoOfOrders + NoofPOSOrders + NoOfMigrateOrder)).toFixed(2) || 0.00);
                    }
                } else {
                    $('#avgSalesPersonAmount').html('0.00');
                    $('#avgPOSAmount').html('0.00');
                    $('#avgMigratedSaleAmount').html('0.00');
                    $('#AVGPiece').html('0.00');
                }
                $('#GrandAmount').html(parseFloat(GrandAmount).toFixed(2));
                $('#POSGrandAmount').html(parseFloat(POSGrandAmount).toFixed(2));
                $('#NoOfOrders').html(parseFloat(NoOfOrders).toFixed());
                $('#NoofPOSOrders').html(parseFloat(NoofPOSOrders).toFixed());
                $('#AllOrders').html(parseFloat(NoOfOrders + NoofPOSOrders + NoOfMigrateOrder).toFixed());
                $('#AllGrandAmount').html(parseFloat(GrandAmount + POSGrandAmount + MigrateSaleAmount).toFixed(2));
                $('#NoofMigrateOrders').html(parseFloat(NoOfMigrateOrder).toFixed());
                $('#MigrateSaleAmount').html(parseFloat(MigrateSaleAmount).toFixed(2));

                var OverAllTotal = AllDropDownList.OverAllTotal;
                for (var j = 0; j < OverAllTotal.length; j++) {
                    var OTotal = OverAllTotal[j];
                    var TOrders = (parseFloat(OTotal.GrandTotal)) + (parseFloat(OTotal.POSTotalAmount) + (parseFloat(OTotal.MigratedSaleAmount)));
                    $('#GrandAmounta').html(parseFloat(OTotal.GrandTotal).toFixed(2));
                    $('#POSGrandAmounta').html(parseFloat(OTotal.POSTotalAmount).toFixed(2));
                    $('#NoOfOrdersa').html(parseFloat(OTotal.NoOfOrders).toFixed());
                    $('#NoofPOSOrdersa').html(parseFloat(OTotal.NoofPOSOrders).toFixed(0));
                    $('#AllOrdersa').html((parseInt(OTotal.NoOfOrders)) + (parseInt(OTotal.NoofPOSOrders)) + (parseInt(OTotal.MigratedOrder)));
                    $('#AllGrandAmounta').html(TOrders.toFixed(2));
                    $('#NoofMigrateOrdersa').html(parseFloat(OTotal.MigratedOrder).toFixed());
                    $('#MigrateSaleAmounta').html(parseFloat(OTotal.MigratedSaleAmount).toFixed(2));
                }
                var paging = AllDropDownList.Paging;
                for (var k = 0; k < paging.length; k++) {
                    var pager = paging[k];
                    $(".Pager").ASPSnippets_Pager({
                        ActiveCssClass: "current",
                        PagerCssClass: "pager",
                        PageIndex: parseInt(pager.PageIndex),
                        PageSize: parseInt(pager.PageSize),
                        RecordCount: parseInt(pager.RecordCount)
                    });
                }
            }
        }
    }
    else {
        location.href = "/";
    }
}



function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var SalesPersons = 0;
    $("#ddlSalesPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPersons = $(this).val() + ',';
        } else {
            SalesPersons += $(this).val() + ',';
        }
    });
    var data = {
        SalesPerson: SalesPersons,
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "OP_SalesBySalesPerson.aspx/SaleBySalesPersonDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                if (response.d != "false") {

                    var DropDown = $.parseJSON(response.d);
                    var row = $("#PrintTable thead tr").clone(true);

                    var GrandAmount = 0.00, NoOfOrders = 0.00, NoofPOSOrders = 0.00, POSGrandAmount = 0.00; MigratedOrders = 0, MigrateSaleAmt = 0.00; PrintDate = '';
                    for (var i = 0; i < DropDown.length; i++) {
                        $("#PrintTable tbody tr").remove();
                        var AllDropDownList = DropDown[i];
                        PrintDate = AllDropDownList.PrintDate;
                        var OrderList = AllDropDownList.OrderList;
                        if (OrderList.length > 0) {
                            $("#EmptyTable").hide();
                            for (var j = 0; j < OrderList.length; j++) {
                                var List = OrderList[j];
                                $(".EmployeeNames", row).text(List.EmployeeName);
                                $(".NoOfOrderss", row).text(List.NoOfOrders);
                                $(".MigratedOrderss", row).text(List.MigratedOrder);
                                $(".MigratedSaleAmounts", row).text(parseFloat(List.MigratedSaleAmount).toFixed(2));
                                $(".TotalOrderAmounts", row).text(parseFloat(List.GrandTotal).toFixed(2));
                                $(".NoofPOSOrderss", row).text(List.NoofPOSOrders);
                                $(".POSTotalAmounts", row).text(parseFloat(List.POSTotalAmount).toFixed(2));
                                $(".TotalOrderss", row).text(parseInt(List.NoOfOrders) + parseInt(List.NoofPOSOrders) + parseInt(List.MigratedOrder));
                                $(".OrderTotals", row).text(parseFloat(parseFloat(List.GrandTotal) + parseFloat(List.POSTotalAmount) + parseFloat(List.MigratedSaleAmount)).toFixed(2));

                                NoOfOrders += parseInt(List.NoOfOrders);
                                NoofPOSOrders += parseInt(List.NoofPOSOrders);
                                GrandAmount += parseFloat(List.GrandTotal);
                                POSGrandAmount += parseFloat(List.POSTotalAmount);
                                MigratedOrders += parseFloat(List.MigratedOrder);
                                MigrateSaleAmt += parseFloat(List.MigratedSaleAmount);
                                $("#PrintTable tbody").append(row);
                                row = $("#PrintTable tbody tr:last").clone(true);
                            } 
                            if (NoOfOrders + NoofPOSOrders + MigratedOrders!=0) {
                                $('#AVGPieces').html(parseFloat((GrandAmount + POSGrandAmount + MigrateSaleAmt) / (NoOfOrders + NoofPOSOrders + MigratedOrders)).toFixed(2) || 0.00);
                            }
                            if (NoOfOrders!= 0) {
                                $('#avgPSalesAmount').html(parseFloat(GrandAmount / NoOfOrders).toFixed(2) || 0.00);
                            }
                            if (NoofPOSOrders != 0) {
                                $('#avgPPOSAmount').html(parseFloat(POSGrandAmount / NoofPOSOrders).toFixed(2) || 0.00);
                            }
                            if (MigratedOrders != 0) {
                                $('#avgPMigratedAmount').html(parseFloat(MigrateSaleAmt / MigratedOrders).toFixed(2) || 0.00);
                            }
                            $('#GrandAmountd').html(parseFloat(GrandAmount).toFixed(2));
                            $('#POSGrandAmountd').html(parseFloat(POSGrandAmount).toFixed(2));
                            $('#NoOfOrdersd').html(parseFloat(NoOfOrders).toFixed());
                            $('#NoofPOSOrdersd').html(parseFloat(NoofPOSOrders).toFixed());
                            $('#AllOrdersd').html(parseFloat(NoOfOrders + NoofPOSOrders + MigratedOrders).toFixed());
                            $('#AllGrandAmountd').html(parseFloat(GrandAmount + POSGrandAmount + MigrateSaleAmt).toFixed(2));
                            $('#NoofMigrateOrderd').html(parseFloat(MigratedOrders).toFixed());
                            $('#MigrateSaleAmountd').html(parseFloat(MigrateSaleAmt).toFixed(2));

                    }
                    if (us == 1) {

                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body style="overflow: hidden;">');
                        $("#PrintDate").text("Print Date :" + PrintDate);
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtSFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        //mywindow.document.write(divToPrint.outerHTML);

                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "Sales Report By Sales Person with Migration",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }

     } 



                    //var xmldoc = $.parseXML(response.d);
                    //var ReportDetails = $(xmldoc).find("Table");
                    //var OTotal = $(xmldoc).find("Table2");
                    //$("#PrintTable tbody tr").remove();
                    //var row = $("#PrintTable thead tr").clone(true);
                    //var GrandAmount = 0.00, NoOfOrders = 0.00, NoofPOSOrders = 0.00, POSGrandAmount = 0.00;MigratedOrders=0,MigrateSaleAmt=0.00
                    //if (ReportDetails.length > 0) {
                    //    $("#EmptyTable").hide();
                       
                    //    $.each(ReportDetails, function (index) {

                    //        $(".EmployeeNames", row).text($(this).find("EmployeeName").text());
                    //        $(".NoOfOrderss", row).text($(this).find("NoOfOrders").text());
                    //        $(".TotalOrderAmounts", row).text(parseFloat($(this).find("GrandTotal").text()).toFixed(2));
                    //        $(".NoofPOSOrderss", row).text($(this).find("NoofPOSOrders").text());
                    //        $(".POSTotalAmounts", row).text(parseFloat($(this).find("POSTotalAmount").text()).toFixed(2));
                    //        $(".TotalOrderss", row).text(parseInt($(this).find("NoOfOrders").text()) + parseInt($(this).find("NoofPOSOrders").text()));
                    //        $(".OrderTotals", row).text(parseFloat(parseFloat($(this).find("GrandTotal").text()) + parseFloat($(this).find("POSTotalAmount").text())).toFixed(2));
                    //        $(".MigratedOrderss", row).text($(this).find("MigratedOrder").text());
                    //        $(".MigratedSaleAmounts", row).text($(this).find("MigratedSaleAmount").text());

                    //        NoOfOrders += parseInt($(this).find("NoOfOrders").text());
                    //        NoofPOSOrders += parseInt($(this).find("NoofPOSOrders").text());
                    //        GrandAmount += parseFloat($(this).find("GrandTotal").text());
                    //        POSGrandAmount += parseFloat($(this).find("POSTotalAmount").text());
                    //        MigratedOrders += parseFloat($(this).find("MigratedOrder").text());
                    //        MigrateSaleAmt += parseFloat($(this).find("MigratedSaleAmount").text());

                    //        $("#PrintTable tbody").append(row);
                    //        row = $("#PrintTable tbody tr:last").clone(true);
                    //    });

                    //    $('#AVGPieces').html(parseFloat((GrandAmount + POSGrandAmount) / (NoOfOrders + NoofPOSOrders)).toFixed(2));
                    //    $('#GrandAmountd').html(parseFloat(GrandAmount).toFixed(2));
                    //    $('#POSGrandAmountd').html(parseFloat(POSGrandAmount).toFixed(2));
                    //    $('#NoOfOrdersd').html(parseFloat(NoOfOrders).toFixed());
                    //    $('#NoofPOSOrdersd').html(parseFloat(NoofPOSOrders).toFixed());
                    //    $('#AllOrdersd').html(parseFloat(NoOfOrders + NoofPOSOrders).toFixed());
                    //    $('#AllGrandAmountd').html(parseFloat(GrandAmount + POSGrandAmount).toFixed(2));
                    //    $('#NoofMigrateOrderd').html(parseFloat(MigratedOrders).toFixed());
                    //    $('#MigrateSaleAmountd').html(parseFloat(MigrateSaleAmt).toFixed(2));

                    //    $(OTotal).each(function () {
                    //        var TOrders = (parseFloat($(this).find("GrandTotal").text())) + (parseFloat($(this).find("POSTotalAmount").text()));

                    //        $('#GrandAmountaw').html(parseFloat($(this).find("GrandTotal").text()).toFixed(2));
                    //        $('#POSGrandAmountaw').html(parseFloat($(this).find("POSTotalAmount").text()).toFixed(2));
                    //        $('#NoOfOrdersaw').html(parseFloat($(this).find("NoOfOrders").text()).toFixed());
                    //        $('#NoofPOSOrdersaw').html(parseFloat($(this).find("NoofPOSOrders").text()).toFixed(0));
                    //        $('#AllOrdersaw').html((parseInt($(this).find("NoOfOrders").text())) + (parseInt($(this).find("NoofPOSOrders").text())));
                    //        $('#AllGrandAmountaw').html(TOrders.toFixed(2));
                    //        $('#NoofMigrateOrdersw').html(parseFloat($(this).find("MigratedOrder").text()).toFixed());
                    //        $('#MigrateSaleAmountw').html(parseFloat($(this).find("MigratedSaleAmount").text()).toFixed(2));
                    //    });

                    //    if (us == 1) {

                    //        var image = $("#imgName").val();
                    //        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                    //        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                    //        $("#PrintDate").text("Print Date :" + (new Date()).format("MM/dd/yyyy hh:mm tt"));
                    //        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                    //        if ($("#txtSFromDate").val() != "") {
                    //            $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                    //        }
                    //        mywindow.document.write($(PrintTable1).clone().html());
                    //        //mywindow.document.write(divToPrint.outerHTML);

                    //        mywindow.document.write('</body></html>');
                    //        setTimeout(function () {
                    //            mywindow.print();
                    //        }, 2000);
                    //    }
                    //    if (us == 2) {
                    //        $("#PrintTable").table2excel({
                    //            exclude: ".noExl",
                    //            name: "Excel Document Name",
                    //            filename: "Sale Report By sales Person",
                    //            fileext: ".xls",
                    //            exclude_img: true,
                    //            exclude_links: true,
                    //            exclude_inputs: true
                    //        });
                    //    }
                    //}
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}



