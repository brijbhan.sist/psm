﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true

    });
    BindDropdown();
    //GetMissingReport(1);
});

function BindDropdown() {
    $.ajax({
        type: "POST",
        url: "ProfitReportByOrder.aspx/bindDropDown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.d != "Session Expired") {

                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlShippingType option:not(:first)").remove();
                    $.each(getData[0].Shipping, function (index, item) {
                        $("#ddlShippingType").append("<option value='" + item.AutoId + "'>" + item.ShippingType + "</option>");
                    });
                    $("#ddlShippingType").select2({
                        multiple: true,
                        });

                    $("#ddlCustomer option:not(:first)").remove();
                    $.each(getData[0].Customer, function (index, cusm) {
                        $("#ddlCustomer").append("<option value='" + cusm.AutoId + "'>" + cusm.CustomerName + "</option>");
                    });
                    $("#ddlCustomer").select2();


                    $("#ddlSalesPerson option:not(:first)").remove();
                    $.each(getData[0].SalesPerson, function (index, item) {
                        $("#ddlSalesPerson").append("<option value='" + item.SID + "'>" + item.SP + "</option>");
                    });
                    $("#ddlSalesPerson").select2({
                        multiple: true,
                    });

                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function GetProfitReport(PageIndex) {
    var array = $("#ddlSalesPerson").val();
    var SalesPersons = array.join(", ");

    var array1 = $("#ddlShippingType").val();
    var shippingType = array1.join(", ");

    var data = {
        CustomerAutoId: $("#ddlCustomer").val() || 0,
        SalesAutoId: SalesPersons,
        ShippingAutoId: shippingType,
        PageIndex: PageIndex || 1,
        PageSize: $('#ddlPageSize').val()
    };
    $.ajax({
        type: "POST",
        url: "ProfitReportByOrder.aspx/GetProfitReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });

        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfReport,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfReport(response) {
    console.log(response);
    var xmldoc = $.parseXML(response.d);
    var ReportDetails = $(xmldoc).find("Table");
    $("#tblProfitReportDetail tbody tr").remove();
    var row = $("#tblProfitReportDetail thead tr").clone(true);
    if (ReportDetails.length > 0) {
        $.each(ReportDetails, function () {

            $(".City", row).text($(this).find("City").text());
            $(".State", row).text($(this).find("StateName").text());
            $(".Zipcode", row).text($(this).find("Zipcode").text());
            $(".PayableAmt", row).text($(this).find("PayableAmount").text());
            $(".OverallDiscount", row).text($(this).find("OverallDiscAmt").text());
            $(".CustomerId", row).text($(this).find("CustomerId").text());
            $(".CustomerName", row).text($(this).find("CustomerName").text());
            $(".OrderNo", row).text($(this).find("OrderNo").text());
            $(".SalesPerson", row).text($(this).find("SalesRep").text());
            $(".DeliveryDate", row).text($(this).find("DeliveryDate").text());
            $(".ShippingType", row).text($(this).find("ShippingType").text());
            $(".TotalProduct", row).text($(this).find("NoOfProducts").text());
            $(".OrderItems", row).text($(this).find("NoOfItems_Ordered").text());
            $(".SubTotal", row).text($(this).find("SubTotal").text());
            if ($(this).find("Alert").text() == 'Check') {
                $(".Alert", row).html("<span style='color:red'>" + $(this).find("Alert").text() + "</span>");
            } else {
                $(".Alert", row).html("<span>" + $(this).find("Alert").text() + "</span>");
            }
            $("#tblProfitReportDetail tbody").append(row);
            row = $("#tblProfitReportDetail tbody tr:last").clone(true);

            

        });


    }
    else {
        $('#tblProfitReportDetail tbody tr').remove();

    }
    var pager = $(xmldoc).find("Table1");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}

function searchReport() {
    GetProfitReport(1);
}

function missingReportOnChange(pageindex) {
    GetProfitReport(pageindex);
}
function Pagevalue(e) {
    GetProfitReport(parseInt($(e).attr("page")));
};



function CreateTable(us) {
    row1 = "";
    var SalesPersons = 0;
    var array = $("#ddlSalesPerson").val();
    var SalesPersons = array.join(", ");

    var array1 = $("#ddlShippingType").val();
    var shippingType = array1.join(", ");
    var data = {
        CustomerAutoId: $("#ddlCustomer").val() || 0,
        SalesAutoId: SalesPersons,
        ShippingAutoId: shippingType,
        PageIndex: 1,
        PageSize: 0
    };
    $.ajax({
        type: "POST",
        url: "ProfitReportByOrder.aspx/GetProfitReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ReportDetails = $(xmldoc).find("Table");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (ReportDetails.length > 0) {
                        $.each(ReportDetails, function () {
                            $(".p_City", row).text($(this).find("City").text());
                            $(".p_State", row).text($(this).find("StateName").text());
                            $(".p_Zipcode", row).text($(this).find("Zipcode").text());
                            $(".p_PayableAmt", row).text($(this).find("PayableAmount").text());
                            $(".p_OverallDiscount", row).text($(this).find("OverallDiscAmt").text());
                            $(".p_CustomerId", row).text($(this).find("CustomerId").text());
                            $(".p_CustomerName", row).text($(this).find("CustomerName").text());
                            $(".p_OrderNo", row).text($(this).find("OrderNo").text());
                            $(".p_SalesPerson", row).text($(this).find("SalesRep").text());
                            $(".p_DeliveryDate", row).text($(this).find("DeliveryDate").text());
                            $(".p_ShippingType", row).text($(this).find("ShippingType").text());
                            $(".p_TotalProduct", row).text($(this).find("NoOfProducts").text());
                            $(".p_OrderItems", row).text($(this).find("NoOfItems_Ordered").text());
                            $(".p_SubTotal", row).text($(this).find("SubTotal").text());
                            if ($(this).find("Alert").text() == 'Check') {
                                $(".p_Alert", row).html("<span style='color:red'>" + $(this).find("Alert").text() + "</span>");
                            } else {
                                $(".p_Alert", row).html("<span>" + $(this).find("Alert").text() + "</span>");
                            }

                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });

                    }
                    if (us == 1) {

                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + (new Date()).format("MM/dd/yyyy hh:mm tt"));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "P&LReportByCustomer",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}


/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblProfitReportDetail tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblProfitReportDetail tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}