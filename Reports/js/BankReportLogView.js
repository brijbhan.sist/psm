﻿$(document).ready(function () {
    debugger;
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var CheckAutoId = getQueryString('dt');
    PaymentDailyReportLog(1, CheckAutoId);
});
function PaymentDailyReportLog(PageIndex, CheckAutoId) {
    var data = {
        ReportAutoId: CheckAutoId,
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "BankReportLog.aspx/PaymentDailyReportLog",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {

        },

        success: SuccessCustomerCreditReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function SuccessCustomerCreditReport(response) {
    debugger;
    if (response.d == 'false') {
    }
    else {
        var xmldoc = $.parseXML(response.d);
        var Report = $(xmldoc).find("Table");
        if (Report.length > 0) {
            $("#TotalNoOfCustomer").text($(Report).find("TotalNoOfCustomer").text());
            $("#TotalNoOfOrder").text($(Report).find("noOfOrder").text());
            $("#TotalCash").text($(Report).find("TotalCash").text());
            $("#TotalCheck").text($(Report).find("TotalCheck").text());
            $("#TotalMoneyOrder").text($(Report).find("TotalMoneyOrder").text());
            $("#TotalCreditCard").text($(Report).find("TotalCreditCard").text());
            $("#TotalStoreCredit").text($(Report).find("TotalStoreCredit").text());
            $("#ElectronicTransfesr").text($(Report).find("TotalElectronicTransfer").text());
            $("#StoreCreditGenerated").text($(Report).find("CreditAmount").text());
            $("#TotalPaid").text(parseFloat($(Report).find("TotalPaid").text()).toFixed(2));
            $("#Short").text(parseFloat($(Report).find("Short").text()).toFixed(2));
            $("#Expense").text(parseFloat($(Report).find("Expense").text()).toFixed(2));
            $("#divReportDate").html('<b>' + $(Report).find("PaymentDate").text() + '</b>');
            $("#DepositeAmount").html($(Report).find("DepositAmount").text());
        }
        var cash = 0.00, check = 0.00, moneyorder = 0.00, creditstore = 0.00, storecredit = 0.00, totalAmoun = 0.00, electronicTransfer = 0.00;
        $("#tblPaymentDailyReportLog tbody tr").remove();
        var row = $("#tblPaymentDailyReportLog thead tr").clone(true);
        var ReportLog = $(xmldoc).find("Table1");
        if (ReportLog.length > 0) {
            $.each(ReportLog, function (index) {
                $(".CustomerName", row).text($(this).find("CustomerName").text());
                $(".OrderNo", row).text($(this).find("OrderNo").text());
                $(".Stime", row).text($(this).find("STime").text());
                $(".Cash", row).text($(this).find("Cash").text());
                $(".tCheck", row).text($(this).find("tCheck").text());
                $(".ChequeNo", row).text($(this).find('ChequeNo').text());

                $(".MoneyOrder", row).text($(this).find("MoneyOrder").text());
                $(".CreditCard", row).text($(this).find("CreditCard").text());
                $(".StoreCredit", row).text($(this).find("StoreCredit").text());
                $(".ElectronicTransfer", row).text($(this).find("ElectronicTransfer").text());
                $(".TotalAmount", row).text($(this).find("TotalAmount").text());

                cash += parseFloat($(this).find("Cash").text());
                check += parseFloat($(this).find("tCheck").text());
                moneyorder += parseFloat($(this).find("MoneyOrder").text());
                creditstore += parseFloat($(this).find("CreditCard").text());
                storecredit += parseFloat($(this).find("StoreCredit").text());
                totalAmoun += parseFloat($(this).find("TotalAmount").text());
                electronicTransfer += parseFloat($(this).find("ElectronicTransfer").text());
                $("#tblPaymentDailyReportLog tbody").append(row);
                row = $("#tblPaymentDailyReportLog tbody tr:last").clone(true);
            });
            $("#Cash").html(cash.toFixed(2));
            $("#Check").html(check.toFixed(2));
            $("#MoneyOrder").html(moneyorder.toFixed(2));
            $("#CreditCard").html(creditstore.toFixed(2));
            $("#StoreCredit").html(storecredit.toFixed(2));
            $("#TotalAmount").html(totalAmoun.toFixed(2));
            $("#ElectronicTransfer").html(electronicTransfer.toFixed(2));
            var cashTotal = 0.00;
            $("#tblCashDetai tbody tr").remove();
            var row = $("#tblCashDetai thead tr").clone(true);
            var CashDetai = $(xmldoc).find("Table2");
            if (CashDetai.length > 0) {
                $.each(CashDetai, function (index) {

                    $(".CurrencyName", row).text($(this).find("CurrencyName").text());
                    $(".NoOfValue", row).text($(this).find("NoOfValue").text());
                    $(".TotalAmount", row).html(parseFloat($(this).find("TotalAmount").text()).toFixed(2));

                    cashTotal = parseFloat(cashTotal) + parseFloat($(this).find("TotalAmount").text());
                    $("#tblCashDetai tbody").append(row);
                    row = $("#tblCashDetai tbody tr:last").clone(true);
                });

                $("#CashTotal").html(parseFloat(cashTotal).toFixed(2));
            }
            else {
                $("#tblCashDetai").hide();
                $("#CashDetailHeader").hide();
            }


            var SortTotal = 0.00, ReceivedAmount = 0.00;
            $("#tblSortDetail tbody tr").remove();
            var row = $("#tblSortDetail thead tr").clone(true);
            var SortDetail = $(xmldoc).find("Table3");
            if (SortDetail.length > 0) {
                $("#tblSortDetailHeader").show();
                $("#tblSortDetail").show();
                $.each(SortDetail, function (index) {
                    $(".PaymentID", row).text($(this).find("PaymentId").text());
                    $(".SettlementDate", row).text($(this).find("PaymentDate").text());
                    $(".sPaymentMode", row).text($(this).find("PaymentMode").text());
                    $(".ReceivedAmount", row).text($(this).find("ReceivedAmount").text());
                    $(".SortAmount", row).text($(this).find("SortAmount").text());
                    $(".ReceivedBy", row).text($(this).find("ReceivedBy").text());
                    $(".SettlementBy", row).text($(this).find("SettlementBy").text());
                    $(".CustomerId", row).text($(this).find("CustomerId").text());
                    $(".CustomerName", row).text($(this).find("CustomerName").text());

                    SortTotal += parseFloat($(this).find("SortAmount").text());
                    ReceivedAmount += parseFloat($(this).find("ReceivedAmount").text());

                    $("#tblSortDetail tbody").append(row);
                    row = $("#tblSortDetail tbody tr:last").clone(true);
                });
                $("#ReceivedAllAmount").html(parseFloat(ReceivedAmount).toFixed(2));
                $("#SortAllAmount").html(parseFloat(SortTotal).toFixed(2));
            }
            else {
                $("#tblSortDetailHeader").hide();
                $("#tblSortDetail").hide();
            }
            var CheckAmount = 0.00;
            $("#tblCheckDetails tbody tr").remove();
            var row = $("#tblCheckDetails thead tr").clone(true);
            var CheckDetails = $(xmldoc).find("Table4");
            if (CheckDetails.length > 0) {
                $("#tblCheckDetailsHeader").show();
                $("#tblCheckDetails").show();
                $.each(CheckDetails, function (index) {

                    $(".SettlementID", row).html($(this).find("SettlementId").text());
                    $(".SettlementDate", row).html($(this).find("SettlementDate").text());
                    $(".RefOrderNo", row).html($(this).find("OrderNo").text());
                    $(".CustemerName", row).html($(this).find("CustomerName").text());
                    $(".CheckNo", row).html($(this).find("ChequeNo").text());
                    $(".CheckDate", row).html($(this).find("ChequeDate").text());
                    $(".CheckAmount", row).html($(this).find("ReceivedAmount").text());
                    $(".CancelDate", row).html($(this).find("CancelDate").text());
                    $(".CancelBy", row).html($(this).find("CancelBy").text());
                    $(".CancelRemark", row).html($(this).find("CancelRemarks").text());
                    CheckAmount += parseFloat($(this).find("ReceivedAmount").text());
                    $("#tblCheckDetails tbody").append(row);
                    row = $("#tblCheckDetails tbody tr:last").clone(true);
                });
                $("#CheckAmount").html(parseFloat(CheckAmount).toFixed(2));
            }
            else {
                $("#SortDetailHeader").hide();
                $("#SortDetail").hide();
            }

            var CreditAmount = 0.00;
            $("#tblGENERATESTORECREDIT tbody tr").remove();
            var row = $("#tblGENERATESTORECREDIT thead tr").clone(true);
            var tblGENERATESTORECREDIT = $(xmldoc).find("Table5");
            if (tblGENERATESTORECREDIT.length > 0) {
                $("#tblGENERATESTORECREDITHeader").show();
                $("#tblGENERATESTORECREDIT").show();
                $.each(tblGENERATESTORECREDIT, function (index) {

                    $(".PaymentId", row).text($(this).find("PaymentId").text());
                    $(".PaymentDate", row).text($(this).find("PaymentDate").text());
                    $(".CustomerId", row).text($(this).find("CustomerId").text());
                    $(".CustomerName", row).text($(this).find("CustomerName").text());
                    $(".PaymentMode", row).text($(this).find("PaymentMode").text());
                    $(".StoreCredit", row).text($(this).find("CreditAmount").text());
                    $(".ReceivedBy", row).text($(this).find("ReceivedBY").text());
                    $(".SettlementBy", row).text($(this).find("SettleBy").text());
                    CreditAmount += parseFloat($(this).find("CreditAmount").text());
                    $("#tblGENERATESTORECREDIT tbody").append(row);
                    row = $("#tblGENERATESTORECREDIT tbody tr:last").clone(true);
                });
                $("#G_Credit").html(parseFloat(CreditAmount).toFixed(2));
            }
            else {
                $("#tblGENERATESTORECREDITHeader").hide();
                $("#tblGENERATESTORECREDIT").hide();
            }

            var ExpenseAmount = 0.00;
            $("#tblExpenseDescription tbody tr").remove();
            var row = $("#tblExpenseDescription thead tr").clone(true);
            var tblGENERATESTORECREDIT = $(xmldoc).find("Table6");
            if (tblGENERATESTORECREDIT.length > 0) {
                $("#expDetail").show();
                $.each(tblGENERATESTORECREDIT, function (index) {

                    $(".ePaymentMode", row).text($(this).find("PaymentMode").text());
                    $(".ExpenseAmount", row).text($(this).find("ExpenseAmount").text());
                    $(".ExpenseBy", row).text($(this).find("ExpenseBy").text());
                    $(".ExpenseDescription", row).text($(this).find("ExpenseDescription").text());
                    ExpenseAmount += parseFloat($(this).find("ExpenseAmount").text());
                    $("#tblExpenseDescription tbody").append(row);
                    row = $("#tblExpenseDescription tbody tr:last").clone(true);
                });
                $("#ExpAmount").html(parseFloat(ExpenseAmount).toFixed(2));
            }
            else {
                $("#expDetail").hide();
            }

            //var ReceivedAmount = 0.00;
            //$("#tblPaymentSettelWithoutOrder tbody tr").remove();
            //var row = $("#tblPaymentSettelWithoutOrder thead tr").clone(true);
            //var tblPaymentSettelWithoutOrder = $(xmldoc).find("Table8");
            //if (tblPaymentSettelWithoutOrder.length > 0) {
            //    $("#tblPaymentSettelWithoutOrderHeader").show();
            //    $("#tblPaymentSettelWithoutOrder").show();
            //    $.each(tblPaymentSettelWithoutOrder, function (index) {

            //        $(".PaymentId", row).text($(this).find("PaymentId").text());
            //        $(".PaymentDate", row).text($(this).find("PaymentDate").text());
            //        $(".CustomerId", row).text($(this).find("CustomerId").text());
            //        $(".CustomerName", row).text($(this).find("CustomerName").text());
            //        $(".PaymentMode", row).text($(this).find("PaymentMode").text());
            //        $(".ReceivedAmount", row).text($(this).find("ReceivedAmount").text());
            //        $(".ReceivedBy", row).text($(this).find("ReceivedBY").text());
            //        $(".SettlementBy", row).text($(this).find("SettleBy").text());
            //        ReceivedAmount += parseFloat($(this).find("ReceivedAmount").text());
            //        $("#tblPaymentSettelWithoutOrder tbody").append(row);
            //        row = $("#tblPaymentSettelWithoutOrder tbody tr:last").clone(true);
            //    });
            //    $("#G_ReceivedAmount").html(parseFloat(ReceivedAmount).toFixed(2));
            //}
            //else {
            //    $("#tblPaymentSettelWithoutOrderHeader").hide();
            //    $("#tblPaymentSettelWithoutOrder").hide();
            //}

            $("#tblSettledAmount tbody tr").remove();
            var row = $("#tblSettledAmount thead tr").clone(true);
            var settledamountdata = $(xmldoc).find("Table8");
            if (settledamountdata.length > 0) {
                $("#SettleAmountHeader").show();
                $("#tblSettledAmount").show();

                var html = `<thead><tr><td colspan="3">Cash</td>`;
                html += `<td rowspan="2" class="text-center" style="vertical-align:middle;">Check</td>`;
                html += `<td rowspan="2" class="text-center" style="vertical-align:middle;">Money Order</td>`;
                html += `<td rowspan="2" class="text-center" style="vertical-align:middle;">Credit Card</td>`;
                html += `<td rowspan="2" class="text-center" style="vertical-align:middle;">Applied Store Credit</td>`;
                html += `<td rowspan="2" class="text-center" style="vertical-align:middle;">Electronic Transfer</td></tr>`;

                html += `<tr><td class="text-center">Received</td>`;
                html += `<td class="text-center">Short</td>`;
                html += `<td class="text-center">Settled</td></tr>`;
                html += `</thead><tbody><tr>`;
                $.each(settledamountdata, function (index) {
                    html += `<td class="text-center">` + $(this).find("CashReceivedAmount").text() + `</td>`;
                    html += `<td class="text-center">` + $(this).find("CashSortAmount").text() + `</td>`;
                    html += `<td class="text-center">` + parseFloat($(this).find("CashReceivedAmount").text() - $(this).find("CashSortAmount").text()).toFixed(2) + `</td>`;
                    html += `<td class="text-center">` + $(this).find("CheckReceivedAmount").text() + `</td>`;
                    html += `<td class="text-center">` + $(this).find("MoneyReceivedAmount").text() + `</td>`;
                    html += `<td class="text-center">` + $(this).find("CreditReceivedAmount").text() + `</td>`;
                    html += `<td class="text-center">` + $(this).find("StoreReceivedAmount").text() + `</td>`;
                    html += `<td class="text-center">` + $(this).find("ElectronicReceivedAmount").text() + `</td>`;
                });
                html += `</tr></tbody>`;
                $("#tblSettledAmount").html(html);
            } else {
                $("#SettleAmountHeader").hide();
                $("#tblSettledAmount").hide();
            }
            $("#tblExpenseCashDetail tbody tr").remove();
            var row = $("#tblExpenseCashDetail thead tr").clone(true);
            var ExpenseCashDetail = $(xmldoc).find("Table8");
            if (ExpenseCashDetail.length > 0) {
                $("#expBillDetail").show();
                $.each(ExpenseCashDetail, function (index) {
                    $(".ExpCurrencyValue", row).text($(this).find("CurrencyName").text());
                    $(".ExpTotalCount", row).text($(this).find("TotalCount").text());
                    $(".ExpTotalAmount", row).html(parseFloat($(this).find("TotalAmount").text()).toFixed(2));

                    ExpcashTotal = parseFloat(cashTotal) + parseFloat($(this).find("TotalAmount").text());
                    $("#tblExpenseCashDetail tbody").append(row);
                    row = $("#tblExpenseCashDetail tbody tr:last").clone(true);
                });

                $("#ExpCashTotal").html(parseFloat(ExpcashTotal).toFixed(2));
            }
            else {
                $("#expBillDetail").hide();
            }

        }

    }
}