﻿var row1 = "";
$(document).ready(function () {
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    BindCustomerType();
    BindCustomer();
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
});

function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
};

/*---------------------------------Bind Customer Category-----------------------------------*/
function BindCustomerType() {
    $.ajax({
        type: "POST",
        url: "OrderWiseOpenBalanceReport.aspx/BindCustomerType",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            debugger;
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlCustomertype option:not(:first)").remove();
                $.each(getData[0].CType, function (index, item) {
                    $("#ddlCustomertype").append("<option value='" + item.CId + "'>" + item.Ct + "</option>");
                });
                $("#ddlCustomertype").select2();

                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(getData[0].SalesList, function (index, item) {
                    $("#ddlSalesPerson").append("<option value = " + item.SID + " >" + item.SP + "</option>");
                });
                $("#ddlSalesPerson").select2();

                $("#ddlShippingType option:not(:first)").remove();
                $.each(getData[0].ShippingType, function (index, item) {
                    $("#ddlShippingType").append("<option value='" + item.AutoId + "'>" + item.ShippingType + "</option>");
                });
                $("#ddlShippingType").attr('multiple', 'multiple')
                $("#ddlShippingType").select2({
                    placeholder: 'All Shipping Type',
                    allowClear: true
                });
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}


/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function BindCustomer() {
    var Customertype = $("#ddlCustomertype").val();
    var SalesPerson = $("#ddlSalesPerson").val();
    $.ajax({
        type: "POST",
        url: "OrderWiseOpenBalanceReport.aspx/BindCustomer",
        data: "{'Customertype':'" + Customertype + "','SalesPersonAutoId':'" + SalesPerson + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlCustomer option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlCustomer").append("<option value='" + getData[index].CuId + "'>" + getData[index].CUN + "</option>");
                });
                $("#ddlCustomer").select2();


            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function BindReport(PageIndex) {
    var ShippingType = '0';
    $("#ddlShippingType option:selected").each(function (i) {
        if (i == 0) {
            ShippingType = $(this).val() + ',';
        } else {
            ShippingType += $(this).val() + ',';
        }
    });
    var data = {
        CustomerType: $("#ddlCustomertype").val(),
        CustomerAutoId: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        ShortBy: $("#ddlShortBy").val(),
        OrderBy: $("#ddlOrderBy").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageSize: $("#ddlPaging").val(),
        ShippingType: ShippingType.toString(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "OrderWiseOpenBalanceReport.aspx/OrderWiseOpenBalanceReport",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successOrderWiseOpenBalanceReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function successOrderWiseOpenBalanceReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var productBarcode = $(xmldoc).find("Table1");

            var OrderTotal = $(xmldoc).find("Table2");
            var TotalOrderAmt = 0, TotalPaid = 0, AmtDue = 0;
            $("#tblOrderList tbody tr").remove();

            var row = $("#tblOrderList thead tr").clone(true);
            if (productBarcode.length > 0) {
                $("#EmptyTable").hide();
                $.each(productBarcode, function () {
                    $(".CustomerId", row).text($(this).find("CustomerId").text());
                    $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                    $(".ShippingType", row).text($(this).find("ShippingType").text());
                    $(".CustomerName", row).text($(this).find("CustomerName").text());
                    $(".OrderDate", row).text($(this).find("OrderDate").text());
                    $(".OrderNo", row).text($(this).find("OrderNo").text());
                    $(".TotalOrderAmount", row).text($(this).find("TotalOrderAmount").text());
                    $(".TotalPaid", row).text($(this).find("TotalPaid").text());
                    $(".TotalDue", row).html($(this).find("AmtDue").text());
                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);

                    TotalOrderAmt += parseFloat($(this).find('TotalOrderAmount').text());
                    TotalPaid += parseFloat($(this).find('TotalPaid').text());
                    AmtDue += parseFloat($(this).find('AmtDue').text());
                });

                $('#TotalOrderAmount').html(TotalOrderAmt.toFixed(2));
                $('#TotalPaid').html(TotalPaid.toFixed(2));
                $('#AmtDue').html(AmtDue.toFixed(2));

            } else {
               // $("#EmptyTable").show();
                $('#TotalOrderAmounts').html('0.00');
                $('#TotalPaids').html('0.00');
                $('#AmtDues').html('0.00');
                $('#TotalOrderAmount').html('0.00');
                $('#TotalPaid').html('0.00');
                $('#AmtDue').html('0.00');
            }

            $(OrderTotal).each(function () {
                $('#TotalOrderAmounts').html(parseFloat($(this).find('TotalOrderAmount').text()).toFixed(2));
                $('#TotalPaids').html(parseFloat($(this).find('TotalPaid').text()).toFixed(2));
                $('#AmtDues').html(parseFloat($(this).find('AmtDue').text()).toFixed(2));
            });
            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}


$("#ddlPaging").change(function () {
    BindReport(1);
})

function CreateTable(us) {
    debugger;
    row1 = "";
    var ShippingType = '0';
    $("#ddlShippingType option:selected").each(function (i) {
        if (i == 0) {
            ShippingType = $(this).val() + ',';
        } else {
            ShippingType += $(this).val() + ',';
        }
    });
    var data = {
        CustomerType: $("#ddlCustomertype").val(),
        CustomerAutoId: $("#ddlCustomer").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        ShortBy: $("#ddlShortBy").val(),
        OrderBy: $("#ddlOrderBy").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageSize: 0,
        ShippingType: ShippingType.toString(),
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "OrderWiseOpenBalanceReport.aspx/OrderWiseOpenBalanceReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var PrintDate = $(xmldoc).find("Table");
                    var productBarcode = $(xmldoc).find("Table1");
                    var OrderTotal = $(xmldoc).find("Table2");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (productBarcode.length > 0) {
                        $.each(productBarcode, function () {
                            $(".PCustomerId", row).text($(this).find("CustomerId").text());
                            $(".PSalesPerson", row).text($(this).find("SalesPerson").text());
                            $(".PShippingType", row).text($(this).find("ShippingType").text());
                            $(".PCustomerName", row).text($(this).find("CustomerName").text());
                            $(".POrderDate", row).text($(this).find("OrderDate").text());
                            $(".POrderNo", row).text($(this).find("OrderNo").text());
                            $(".PTotalOrderAmount", row).text($(this).find("TotalOrderAmount").text());
                            $(".PTotalPaid", row).text($(this).find("TotalPaid").text());
                            $(".PTotalDue", row).html($(this).find("AmtDue").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                    }

                    $(OrderTotal).each(function () {
                        $('#PTotalOrderAmount').html(parseFloat($(this).find('TotalOrderAmount').text()).toFixed(2));
                        $('#PTotalPaid').html(parseFloat($(this).find('TotalPaid').text()).toFixed(2));
                        $('#PAmtDue').html(parseFloat($(this).find('AmtDue').text()).toFixed(2));
                    });
                    if (us == 1) {
                        //var image = $("#imgName").val();
                        //var divToPrint = document.getElementById("PrintTable");
                        //var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        //mywindow.document.write('<html><head><style>#PrintTable tbody tr .right{text-align:right;} #PrintTable tbody tr .left{text-align:left;} #PrintTable tbody tr .price{text-align:right;}#PrintTable {border-collapse: collapse;width: 100%;}#PrintTable td, #PrintTable th {border: 1px solid black;padding: 8px;}#PrintTable tr:nth-child(even){background-color: #f2f2f2;}#PrintTable thead {padding-top: 12px;padding-bottom: 12px;background-color:#C8E8F9;color:black;}.text-right{text-align:right;}.text-left{text-align:left;}.text-center{text-align:center;}</style>');
                        //mywindow.document.write('</head><body>');
                        //if ($("#txtSFromDate").val() != "") {
                        //    mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;'><img src='/Img/logo/" + image + "' style='float:left;' height='70px' width='140px'/><h3 style='float:center;'>Open Balance Report By Order</h3><h4 style='float:right;margin-right:10px;'>Date: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val() + "</h4></div>")

                        //}
                        //else {
                        //    debugger
                        //    mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;'><img src='/Img/logo/" + image + "' style='float:left;' height='70px' width='140px'/><h3 style='float:center;'>Open Balance Report By Order</h3><h4 style='float:right;margin-right:10px;'>Date: " + (new Date()).format("MM/dd/yyyy") + "</h4></div>")
                        //}
                        //mywindow.document.write(divToPrint.outerHTML);
                        //mywindow.document.write('</body></html>');
                        //mywindow.print();
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtSFromDate").val() != "") {
                            $("#DateRange").text("Date Range : " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "Open Balance Report By Order" + (PrintDate.find('PrintDate').text()),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
    CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
