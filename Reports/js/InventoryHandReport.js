﻿$(document).ready(function () {
    if ($("#hiddenForPacker").val() == "") {
        $("#btnBulkUpload").show();
    }
    bindCategory();

});
function Pagevalue(e) {
    getProductList(parseInt($(e).attr("page")));
};
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "InventoryHandReport.aspx/bindCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var getData = $.parseJSON(response.d);

            $("#ddlSCategory option:not(:first)").remove();
            $.each(getData[0].Category, function (index, item) {

                $("#ddlSCategory").append("<option value='" + item.CAID + "'>" + item.CAN + "</option>");
            });
            $("#ddlSCategory").select2();
            $("#ddlBrand option:not(:first)").remove();
            $.each(getData[0].Brand, function (index, item) {
                $("#ddlBrand").append("<option value='" + item.BID + "'>" + item.BN + "</option>");
            });
            $("#ddlBrand").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*-------------------------------------------------------Bind Subcategory-----------------------------------------------------------*/
function BindSubCategory() {
    var categoryAutoId = $("#ddlSCategory").val();
    $.ajax({
        type: "POST",
        url: "InventoryHandReport.aspx/bindSubcategory",
        data: "{'categoryAutoId':'" + categoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
           
            var getData = $.parseJSON(response.d);
           
                $("#ddlSSubcategory option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlSSubcategory").append("<option value=' " + getData[index].SCAID + "'>" + getData[index].SCAN + "</option>");
                });
                $("#ddlSSubcategory").select2();
        
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
/*-------------------------------------------------------Get Product List----------------------------------------------------------*/
function getProductList(PageIndex) {
    var data = {
        CategoryAutoId: $("#ddlSCategory").val(),
        SubcategoryAutoId: $("#ddlSSubcategory").val(),
        Status: $("#ddlStatus").val(),
        ProductId: $("#txtSProductId").val().trim(),
        ProductName: $("#txtSProductName").val().trim(),
        BrandName: $("#ddlBrand").val(),
        BarCode: $("#txtBarCode").val().trim(),
        PageSize: $("#ddlPageSize").val(),
        pageIndex: PageIndex
    };
    localStorage.setItem('SearchFilter', JSON.stringify(data));
    $.ajax({
        type: "POST",
        url: "InventoryHandReport.aspx/getProductList",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetProductList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetProductList(response) {
    var xmldoc = $.parseXML(response.d);
    var productList = $(xmldoc).find("Table1");

    $("#tblProductList tbody tr").remove();

    var row = $("#tblProductList thead tr").clone(true);
    if (productList.length > 0) {
        $("#EmptyTable").hide();
        $.each(productList, function (index) {
            $(".ProductId", row).text($(this).find("ProductId").text());
            $(".Category", row).text($(this).find("Category").text());
            $(".Subcategory", row).text($(this).find("Subcategory").text());
            $(".ProductName", row).text($(this).find("ProductName").text().replace(/&quot;/g, "\'"));
            $(".Stock", row).text($(this).find("Stock").text());
            $(".BrandName", row).text($(this).find("BrandName").text());
            $(".ReOrderMark", row).text($(this).find("ReOrderMark").text());
            if ($(this).find("Status").text() == 'Active') {
                $(".Status", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("Status").text() + "</span>");
            }
            else {
                $(".Status", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("Status").text() + "</span>");
            }

            $(".Vendor", row).html($(this).find("Vendor").text());
            $(".ImageUrl", row).html("<img src='" + $(this).find('ThumbnailImageUrl').text() + "' OnError='this.src =\"http://psmnj.a1whm.com/Attachments/default_pic.png\"' class='zoom img img-circle img-responsive' style='width: 50px;height: 50px;'  >");
            $("#tblProductList tbody").append(row);
            row = $("#tblProductList tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }

    if ($("#hiddenForPacker").val() != "") {
        $("#linkAddNewProduct").hide();
        $(".glyphicon-remove").hide();
        $(".action").hide();
    }

    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblProductList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblProductList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function CreateTable(us) {
    row1 = "";
    if ($("#ddlSCategory").val() == "0" || $("#ddlSCategory").val() == 0) {
        toastr.error('Category is required', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var data = {
        CategoryAutoId: $("#ddlSCategory").val(),
        SubcategoryAutoId: $("#ddlSSubcategory").val(),
        Status: $("#ddlStatus").val(),
        ProductId: $("#txtSProductId").val().trim(),
        ProductName: $("#txtSProductName").val().trim(),
        BrandName: $("#ddlBrand").val(),
        BarCode: $("#txtBarCode").val(),
        PageSize: 0,
        pageIndex: 0
    };
    localStorage.setItem('SearchFilter', JSON.stringify(data));
    $.ajax({
        type: "POST",
        url: "InventoryHandReport.aspx/getProductList",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var RouteList = $(xmldoc).find("Table1");
                    var Todaydate = $(xmldoc).find("Table");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (RouteList.length > 0) {
                        $.each(RouteList, function () {
                            $(".P_ProductId", row).text($(this).find("ProductId").text());
                            $(".P_CategoryName", row).text($(this).find("Category").text());
                            $(".P_SubcategoryName", row).text($(this).find("Subcategory").text());
                            $(".P_ProductName", row).text($(this).find("ProductName").text());
                            $(".P_Stock", row).text($(this).find("Stock").text());
                            $(".P_BrandName", row).text($(this).find("BrandName").text());
                            $(".P_ReOrderMark", row).text($(this).find("ReOrderMark").text());
                            $(".P_Vendor", row).text($(this).find("Vendor").text());
                            $(".P_Status", row).html($(this).find("Status").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                    }
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        console.log($(Todaydate).find("Date").text());
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + $(Todaydate).find("Date").text());
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "Inventory Onhand Report",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}


