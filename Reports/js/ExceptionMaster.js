﻿$(document).ready(function () {
    getList(1);
});
function Pagevalue(e) {
    getList(parseInt($(e).attr("page")));
}

function getList(PageIndex) {
        var data = {            
            PageSize: $("#ddlPageSize").val(),
            pageIndex: PageIndex
        };
        $.ajax({
            type: "POST",
            url: "ExceptionMaster.aspx/BindReport",
            data: JSON.stringify({ 'dataValue': JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: successReport,
            error: function (result) {
                alert(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });

    }
    
function successReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var Exception = $(xmldoc).find("Table");
            $("#tblExceptionMasterResport tbody tr").remove();

            var row = $("#tblExceptionMasterResport thead tr").clone(true);
            if (Exception.length > 0) {
                var i = 1;
                $("#EmptyTable").hide();
                $.each(Exception, function () {

                    //$(".SN", row).text(i);
                    $(".UserName", row).text($(this).find("UserName").text());
                    $(".AppVersion", row).text($(this).find("AppVersion").text());
                    $(".deviceID", row).text($(this).find("deviceID").text());
                    $(".functionName", row).text($(this).find("functionName").text());
                    $(".requestContainer", row).text($(this).find("requestContainer").text());
                    $(".errordetails", row).html($(this).find("errordetails").text());
                    $(".PropertyName", row).html($(this).find("PropertyName").text());
                    $(".errorDate", row).html($(this).find("errorDate").text());
                    $(".Createddate", row).html($(this).find("Createddate").text());
                    $("#tblExceptionMasterResport tbody").append(row);
                    row = $("#tblExceptionMasterResport tbody tr:last").clone(true);
                    //i++;
                });
            }
            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "page",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}
