﻿$(document).ready(function () { 
    getUsersecurityList();

}); 


/*-------------------------------------------------------Get User Security List----------------------------------------------------------*/
function getUsersecurityList() {
   
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/Usersecuritylist.asmx/getUsersecurityList",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetUsersecurityList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetUsersecurityList(response) {
    var xmldoc = $.parseXML(response.d);
    var usersecurityList = $(xmldoc).find("Table");
    console.log(usersecurityList);
    $("#tblUsersecurityList tbody tr").remove();

    var row = $("#tblUsersecurityList thead tr").clone(true);
    if (usersecurityList.length > 0) {
        $("#EmptyTable").hide();
        $.each(usersecurityList, function (index) {
            debugger;
            $(".TypeName", row).text($(this).find("TypeName").text());
            $(".SecurityValue", row).text($(this).find("SecurityValue").text());
            $(".Description", row).text($(this).find("description").text());
            $(".InteralRemark", row).text($(this).find("internalRemarks").text());  
            $("#tblUsersecurityList tbody").append(row);
            row = $("#tblUsersecurityList tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }

    //if ($("#hiddenForPacker").val() != "") {
    //    $("#linkAddNewProduct").hide();
    //    $(".glyphicon-remove").hide();
    //    $(".action").hide();
    //}

    //var pager = $(xmldoc).find("Table");
    //$(".Pager").ASPSnippets_Pager({
    //    ActiveCssClass: "current",
    //    PagerCssClass: "pager",
    //    PageIndex: parseInt(pager.find("PageIndex").text()),
    //    PageSize: parseInt(pager.find("PageSize").text()),
    //    RecordCount: parseInt(pager.find("RecordCount").text())
    //});
}




