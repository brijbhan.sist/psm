﻿var row1 = "";
$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    bindDropdown();
    BindCustomer();
    BindProductSubCategory();
    BindProductDropdown();
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtFromDate").val(month + '/' + day + '/' + year);
    $("#txtToDate").val(month + '/' + day + '/' + year);

});
function Pagevalue(e) {
    OrderSaleReport(parseInt($(e).attr("page")));
};
function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "OrderSaleReport.aspx/BindSalesPerson",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlAllSales option:not(:first)").remove();//For Sales Person
                $.each(getData[0].SalesPerson, function (index, item) {

                    $("#ddlAllSales").append("<option value='" + item.SId + "'>" + item.Sp + "</option>");
                });
                $("#ddlAllSales").select2()

                $("#ddlCustomerType option:not(:first)").remove();//For Customer Type
                $.each(getData[0].CType, function (index, item) {

                    $("#ddlCustomerType").append("<option value='" + item.CId + "'>" + item.Ct + "</option>");
                });
                $("#ddlCustomerType").select2()


                $("#ddlAllCategory option:not(:first)").remove();//For Customer Type
                $.each(getData[0].CatName, function (index, item) {
                    $("#ddlAllCategory").append("<option value='" + item.CAId + "'>" + item.Can + "</option>");
                });
                $("#ddlAllCategory").select2()


            }
            else {
                location.href = "/";
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

//-------------------------------------------------------------------------------Bind Customer--------------------------------------------------
function BindCustomer() {
    var Customertype = $("#ddlCustomerType").val();
    var SalesPerson = $("#ddlAllSales").val();
    $.ajax({
        type: "POST",
        url: "OrderSaleReport.aspx/BindCustomer",
        data: "{'Customertype':'" + Customertype + "','SalesPersonAutoId':'" + SalesPerson + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlAllCustomer option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    console.log(getData[index])
                    $("#ddlAllCustomer").append("<option value='" + getData[index].CuId + "'>" + getData[index].CUN + "</option>");
                });
                $("#ddlAllCustomer").select2();


            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
//----------------------------------------------------------Bind Subcategory-----------------------------------------------------
function BindProductSubCategory() {
    $.ajax({
        type: "POST",
        url: "OrderSaleReport.aspx/BindProductSubCategory",
        data: "{'CategoryAutoId':'" + $("#ddlAllCategory").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlAllSubCategory option:not(:first)").remove();
                $.each(getData, function (index, item) {

                    $("#ddlAllSubCategory").append("<option value='" + getData[index].SCAId + "'>" + getData[index].SCAN + "</option>");
                });
                $("#ddlAllSubCategory").select2();

            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
////---------------------------------------------Bind Product-----------------------------------------------------------------
function BindProductDropdown() {
    var SubcategoryAutoId = $("#ddlAllSubCategory").val();
    var CategoryAutoId = $("#ddlAllCategory").val();
    $.ajax({
        type: "POST",
        url: "OrderSaleReport.aspx/BindProduct",
        data: "{'SubcategoryAutoId':'" + SubcategoryAutoId + "','CategoryAutoId':'" + CategoryAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlAllProduct option:not(:first)").remove();
                $.each(getData, function (index, item) {

                    $("#ddlAllProduct").append("<option value='" + getData[index].PID + "'>" + getData[index].PN + "</option>");
                });
                $("#ddlAllProduct").select2();

            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}


function OrderSaleReport(PageIndex) {
    var data = {
        SalesPerson: $("#ddlAllSales").val(),
        CustomerType: $("#ddlCustomerType").val(),
        Customer: $('#ddlAllCustomer').val(),
        Category: $("#ddlAllCategory").val(),
        SubCategory: $("#ddlAllSubCategory").val(),
        Product: $("#ddlAllProduct").val(),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageIndex: PageIndex,
        PageSize: $('#PageSize').val()
    };

    $.ajax({
        type: "POST",
        url: "OrderSaleReport.aspx/OrderSaleReport",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var order = $(xmldoc).find('Table1');
                    var OrderTotal = $(xmldoc).find("Table2");
                    var TotalNetSale = 0; Totaltax = 0; TotalShippingPrice = 0; TotalGrossSale = 0;
                    $("#tblOrderSaleReport tbody tr").remove();
                    if (order.length > 0) {
                        $('#EmptyTable').hide();
                        var i = 1;
                        var total_order_sum = 0;
                        var total_due = 0;
                        var total_aov = 0.00;
                        var row = $('#tblOrderSaleReport thead tr').clone(true);
                        $.each(order, function () {
                            total_order_sum = parseInt($(this).find("TotalOrder").text()) + total_order_sum;
                            total_due = parseFloat($(this).find("DueAmt").text()) + parseFloat(total_due);
                            total_aov = parseFloat($(this).find("AOV").text()) + parseFloat(total_aov);
                            $(".SN", row).text(i);
                            $(".CustomerId", row).text($(this).find("CustomerAutoId").text());
                            $(".CustomerName", row).text($(this).find("CustomerName").text());
                            $(".TotalNetSales", row).text(parseFloat($(this).find("TotalNetSale").text()).toFixed(2));

                            $(".TotalOrder", row).text($(this).find("TotalOrder").text());
                            $(".AOV", row).text(parseFloat($(this).find("AOV").text()).toFixed(2));
                            $(".TotalDue", row).text(parseFloat($(this).find("DueAmt").text()).toFixed(2));
                            $(".LastOrderDate", row).text($(this).find("LastOrderDate").text());


                            $(".Tax", row).text(parseFloat($(this).find("TotalTax").text()).toFixed(2));
                            $(".Shipping", row).text(parseFloat($(this).find("Shipping").text()).toFixed(2));
                            var gs = Number($(this).find("Shipping").text()) + Number($(this).find("TotalTax").text()) + Number($(this).find("TotalNetSale").text());
                            $(".GrossSale", row).text(gs.toFixed(2));
                            $("#tblOrderSaleReport tbody").append(row);
                            row = $("#tblOrderSaleReport tbody tr:last-child").clone(true);
                            i++;

                            TotalNetSale = TotalNetSale + parseFloat($(this).find("TotalNetSale").text());
                            Totaltax = Totaltax + parseFloat($(this).find("TotalTax").text());
                            TotalShippingPrice = TotalShippingPrice + parseFloat($(this).find("Shipping").text());
                            TotalGrossSale = TotalGrossSale + Number($(this).find("Shipping").text()) + Number($(this).find("TotalTax").text()) + Number($(this).find("TotalNetSale").text());

                        });
                        $('#TNetSale').html(TotalNetSale.toFixed(2));
                        $('#TTAxPrice').html(Totaltax.toFixed(2));
                        $('#TShippingPrice').html(TotalShippingPrice.toFixed(2));
                        $('#TGrossSale').html(TotalGrossSale.toFixed(2));
                        $('#TTotalOrder').html(total_order_sum);
                        $('#TTotalDue').html(total_due.toFixed(2));
                        total_aov = (TotalGrossSale / total_order_sum);
                        $('#TAOV').html(total_aov.toFixed(2));
                        $('#TTAxPrice').html(Totaltax.toFixed(2));
                        $('#TShippingPrice').html(TotalShippingPrice.toFixed(2));
                    }
                    else {
                        //  $('#EmptyTable').show();
                        $('#TNetSale').html('0.00');
                        $('#TTAxPrice').html('0.00');
                        $('#TShippingPrice').html('0.00');
                        $('#TGrossSale').html('0.00');
                        $('#TNetSales').html('0.00');
                        $('#TTAxPrices').html('0.00');
                        $('#TShippingPrices').html('0.00');
                        $('#TGrossSales').html('0.00');
                        $('#TTotalOrder').html('0.00');
                        $('#TTotalDue').html('0.00');
                        $('#TAOV').html('0.00');

                        $('#TotalOrder').html('0.00');
                        $('#TotalDue').html('0.00');
                        $('#AOV').html('0.00');
                    }
                    var pager = $(xmldoc).find("Table");
                    $(".Pager").ASPSnippets_Pager({
                        ActiveCssClass: "current",
                        PagerCssClass: "pager",
                        PageIndex: parseInt(pager.find("PageIndex").text()),
                        PageSize: parseInt(pager.find("PageSize").text()),
                        RecordCount: parseInt(pager.find("RecordCount").text())
                    });

                    $(OrderTotal).each(function () {
                        if (OrderTotal.length > 0) {
                            $('#TotalOrder').html($(this).find('overAllOrder').text());
                            $('#TNetSales').html(parseFloat($(this).find('TotalNetSale').text()).toFixed(2));
                            $('#TTAxPrices').html(parseFloat($(this).find('TotalTax').text()).toFixed(2));
                            $('#TShippingPrices').html(parseFloat($(this).find('Shipping').text()).toFixed(2));
                            $('#TGrossSales').html(parseFloat($(this).find('GrossSales').text()).toFixed(2));
                            $('#TotalDue').html(parseFloat($(this).find('overAllDueAmt').text()).toFixed(2));
                            $('#AOV').html(parseFloat($(this).find('overAllAOV').text()).toFixed(2));
                        }
                    });
                }
            }
            else {
                location.href = "/";
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}

function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        SalesPerson: $("#ddlAllSales").val(),
        CustomerType: $("#ddlCustomerType").val(),
        Customer: $('#ddlAllCustomer').val(),
        Category: $("#ddlAllCategory").val(),
        SubCategory: $("#ddlAllSubCategory").val(),
        Product: $("#ddlAllProduct").val(),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageIndex: 1,
        PageSize: 0
    };

    $.ajax({
        type: "POST",
        url: "OrderSaleReport.aspx/OrderSaleReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var OrderTotal = $(xmldoc).find("Table1");
                    var OTotal = $(xmldoc).find("Table2");

                    if ($("#txtFromDate").val() != "") {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Order Sale Report-By Customer</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + $(OTotal).find('PrintDate').text() + "</span><br/><span class='DateRangeCSS' style='float:center;margin-top: 0.5%; font-size: 9px; color: black; font-weight: bold;'>Date Range: " + $("#txtFromDate").val() + " To " + $("#txtToDate").val() + "</span><br/></div>"
                    }
                    else {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Order Sale Report-By Customer</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + $(OTotal).find('PrintDate').text() + "</span></div>"
                    }
                    row1 += "<table class='PrintMyTableHeader MyTableHeader' id='RptTable'>"
                    row1 += "<thead  class='bg-blue white'>"
                    row1 += "<tr>"
                    row1 += "<td class='text-center'  style='width:3% !important'>SN</td>"
                    row1 += "<td class='text-center'  style='width:4% !important'>Customer ID</td>"
                    row1 += "<td class='text-center' style='text-align:right; width:6 % !important'>Customer</td>"
                    row1 += "<td class='text-center' style='width:4% !important'>Last Order<br> Date</td>"
                    row1 += "<td class='text-center' style='width:3% !important'>Total Order</td>"
                    row1 += "<td class='text-center' style='width:3% !important'>Total Net<br> Sales</td>"
                    row1 += "<td class='text-center' style='width:3% !important'>Tax</td>"
                    row1 += "<td class='text-center' style='width:3% !important'>Shipping</td>"
                    row1 += "<td class='text-center' style='width:3% !important'>Gross Sales</td>"
                    row1 += "<td class='text-center' style='width:3% !important'>Total Due</td>"
                    row1 += "<td class='text-center' style='width:3% !important'>AOV</td>"
                    row1 += "</tr>"
                    row1 += "</thead>"
                    row1 += "<tbody>"
                    if (OrderTotal.length > 0) {
                        var i = 1;
                        $.each(OrderTotal, function (index) {
                            row1 += "<tr>";
                            row1 += "<td class='text-center' style='width:3% !important'>" + i + "</td>";
                            row1 += "<td class='text-center' style='width:4% !important'>" + $(this).find("CustomerAutoId").text() + "</td>";
                            row1 += "<td class='left' >" + $(this).find("CustomerName").text() + "</td>";
                            row1 += "<td class='text-Center' style='width:4% !important'>" + $(this).find("LastOrderDate").text() + "</td>";
                            row1 += "<td class='text-Center' style='width:4% !important'>" + $(this).find("TotalOrder").text() + "</td>";
                            row1 += "<td class='right' style='width:3% !important'>" + parseFloat($(this).find("TotalNetSale").text()).toFixed(2) + "</td>";
                            row1 += "<td class='right' style='width:3% !important'>" + parseFloat($(this).find("TotalTax").text()).toFixed(2) + "</td>";
                            row1 += "<td class='right' style='width:3% !important'>" + parseFloat($(this).find("Shipping").text()).toFixed(2) + "</td>";
                            var gs = Number($(this).find("ShippingCharges").text()) + Number($(this).find("TotalTax").text()) + Number($(this).find("TotalNetSale").text());
                            row1 += "<td class='right' style='width:3% !important'>" + gs.toFixed(2) + "</td>";
                            row1 += "<td class='right' style='width:3% !important'>" + parseFloat($(this).find("DueAmt").text()).toFixed(2) + "</td>";
                            row1 += "<td class='right' style='width:3% !important'>" + parseFloat($(this).find("AOV").text()).toFixed(2) + "</td>";

                            row1 += "</tr>";
                            i++;

                        });
                        row1 += "</tbody>"
                        row1 += "<tfoot>"
                        $(OTotal).each(function () {
                            row1 += "<tr  style='font-weight:bold;'>";
                            row1 += "<td class='text-center' colspan='4' style='font-weight:bold;'>" + 'Overall Total' + "</td>";
                            row1 += "<td class='text-center' style='font-weight:bold;'>" + $(this).find('overAllOrder').text() + "</td>";
                            row1 += "<td class='right wth' style='font-weight:bold;'>" + parseFloat($(this).find('TotalNetSale').text()).toFixed(2) + "</td>";
                            row1 += "<td class='right wth' style='font-weight:bold;'>" + parseFloat($(this).find('TotalTax').text()).toFixed(2) + "</td>";
                            row1 += "<td class='right wth' style='font-weight:bold;'>" + parseFloat($(this).find('Shipping').text()).toFixed(2) + "</td>";
                            row1 += "<td class='right wth' style='font-weight:bold;'>" + parseFloat($(this).find('GrossSales').text()).toFixed(2) + "</td>"
                            row1 += "<td class='right wth' style='font-weight:bold;'>" + parseFloat($(this).find('overAllDueAmt').text()).toFixed(2) + "</td>";
                            row1 += "<td class='right wth' style='font-weight:bold;'>" + parseFloat($(this).find('overAllAOV').text()).toFixed(2) + "</td>"
                            row1 += "</tr>";
                        });
                        row1 += "</tfoot>"
                        row1 += "</table>"
                        if (us == 1) {
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style>#RptTable {border-collapse: collapse;width: 100%;}#RptTable td, #RptTable th {border: 1px solid black;}#RptTable tr:nth-child(even){background-color: #f2f2f2;}#RptTable thead {padding-top: 12px;padding-bottom: 12px;text-align:center;background-color:#e9e8e8 !important;color:black;}.text-right{text-align:right;}.text-left{text-align:left;}.text-center{text-align:center;}</style>');
                            mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                            mywindow.document.write('</head><body>');
                            mywindow.document.write(row1);
                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        }
                        if (us == 2) {
                            $("#ExcelDiv").html(row1);
                            $("#RptTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "Order Sales Report By Customer",
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });

                        }
                    }

                }
                else {
                    location.href = "/";
                }
            }

        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderSaleReport tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderSaleReport tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
