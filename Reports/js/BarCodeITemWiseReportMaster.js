﻿
$(document).ready(function () {   
    getBarCodeITemWiseReportMaster(1);  
});
function Pagevalue(e) {
    getBarCodeITemWiseReportMaster(parseInt($(e).attr("page")));
}
function getBarCodeITemWiseReportMaster(PageIndex) {
    var data = {       
        PageIndex: PageIndex,
        PageSize: $('#ddlPagesize').val()
    }; 
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/BarCodeITemWiseReportMaster.asmx/BindBarCodeITemWiseReportMaster",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({ 
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successgetBarCodeITemWiseReportMaster,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function successgetBarCodeITemWiseReportMaster(response) {
    if (response.d == 'Session Expired') {
        location.href = "/";
    } else {
        var xmldoc = $.parseXML(response.d);
        var ReportDetails = $(xmldoc).find("Table1");
        $("#BarCodeITemWiseReportMaster tbody tr").remove();
        var row = $("#BarCodeITemWiseReportMaster thead tr").clone(true);
        if (ReportDetails.length > 0) {
            $("#EmptyTable").hide();
            $("#BarCodeITemWiseReportMaster").show();
            var html = "", ProductTotal = 0.00, ProductId = 0, CustomerAutoId = 0;
            $.each(ReportDetails, function (index) {
                $(".PSMNJUnit", row).text($(this).find("PSMNJ_Unit").text());
                $(".PSMNJProductId", row).text($(this).find("PSMNJ_productId").text());
                $(".PSMPAUnit", row).text($(this).find("PSMPA_Unit").text());
                $(".PSMPAProductId", row).text($(this).find("PSMPA_productId").text());
                $(".PSMCTUnit", row).text($(this).find("PSMCT_Unit").text());
                $(".PSMCTProductId", row).html($(this).find("PSMCT_productId").text());
                $(".PSMNPAUnit", row).text($(this).find("PSMnpa_Unit").text());
                $(".PSMNPAProductId", row).text($(this).find("PSMnpa_productId").text());
                $(".PSMWPAUnit", row).text($(this).find("PSMWPA_Unit").text());
                $(".PSMWPAProductId", row).text($(this).find("PSMWPA_productId").text());
                $(".Barcode", row).html($(this).find("Barcode").text());
                $("#BarCodeITemWiseReportMaster tbody").append(row);
                row = $("#BarCodeITemWiseReportMaster tbody tr:last").clone(true);               
            });           
        } 
        var pager = $(xmldoc).find("Table");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
        var NoOfBarcode = $(xmldoc).find("Table2");
        $("#njtotalbarcode").html($(NoOfBarcode).find("TotalBarcodeNJ").text());
        $("#patotalbarcode").html($(NoOfBarcode).find("TotalBarcodePA").text());
        $("#npatotalbarcode").html($(NoOfBarcode).find("TotalBarcodeNPA").text());
        $("#cttotalbarcode").html($(NoOfBarcode).find("TotalBarcodeCT").text());
        $("#wpatotalbarcode").html($(NoOfBarcode).find("TotalBarcodeWPA").text());
    }
} 
$("#ddlPagesize").change(function () {
    getBarCodeITemWiseReportMaster(1);
})
