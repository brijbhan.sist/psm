﻿$(document).ready(function () {
    if ($("#hiddenForPacker").val() == "") {
        $("#btnBulkUpload").show();
    }
    bindProduct();
    getProductList(1);

});
function Pagevalue(e) {
    getProductList(parseInt($(e).attr("page")));
};
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function bindProduct() {
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/PackingDetailUpdateLog.asmx/bindProduct",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var product = $(xmldoc).find("Table");

            $("#ddlProduct option:not(:first)").remove();
            $.each(product, function () {
                $("#ddlProduct").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("AutoId").text() + "-" + $(this).find("ProductName").text() + "</option>");
            });
            $("#ddlProduct").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*-------------------------------------------------------Get Product List----------------------------------------------------------*/
function getProductList(PageIndex) {
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        PageSize: $("#ddlPageSize").val(),
        pageIndex: PageIndex
    };
    localStorage.setItem('SearchFilter', JSON.stringify(data));
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/PackingDetailUpdateLog.asmx/getProductList",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetProductList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetProductList(response) {
    var xmldoc = $.parseXML(response.d);
    var productList = $(xmldoc).find("Table1");
    console.log(productList);
    $("#tblProductList tbody tr").remove();

    var row = $("#tblProductList thead tr").clone(true);
    if (productList.length > 0) {
        $("#EmptyTable").hide();
        $.each(productList, function (index) {
            $(".ProductId", row).text($(this).find("ProductId").text());
            $(".ProductName", row).text($(this).find("ProductName").text());
            $(".UnitName", row).text($(this).find("UnitType").text());
            $(".MinPrice", row).text($(this).find("MinPrice").text());
            $(".CostPrice", row).text($(this).find("CostPrice").text());
            $(".Price", row).text($(this).find("Price").text().replace(/&quot;/g, "\'"));
            $(".WH", row).text($(this).find("WHminPrice").text());
            $(".SRP", row).text($(this).find("SRP").text());
            $(".createDate", row).text($(this).find("createDate").text());
        
            $("#tblProductList tbody").append(row);
            row = $("#tblProductList tbody tr:last").clone(true);
        });

    } else {
        $("#EmptyTable").show();
    }

    if ($("#hiddenForPacker").val() != "") {
        $("#linkAddNewProduct").hide();
        $(".glyphicon-remove").hide();
        $(".action").hide();
    }

    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}

$("#btnSearch").click(function () {
    getProductList(1);
})



