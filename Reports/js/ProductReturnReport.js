﻿$(document).ready(function () {
    BindDropdownlist();
    $('.date').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var dt = new Date().format("MM/dd/yyyy") 
    $("#txtCloseFromDate").val(dt);
    $("#txtCloseToDate").val(dt);
    $("#txtFromDate").val(dt);
    $("#txtToDate").val(dt);
});
function setdatevalidation(val) {
    var fdate = $("#txtFromDate").val();
    var tdate = $("#txtToDate").val();
    var cfdate = $("#txtCloseFromDate").val();
    var ctdate = $("#txtCloseToDate").val();

    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtFromDate").val(tdate);
        }
    }
    else if (val == 3) {
        if (cfdate == '' || new Date(cfdate) > new Date(ctdate)) {
            $("#txtCloseToDate").val(cfdate);
        }
    }
    else if (val == 4) {
        if (cfdate == '' || new Date(cfdate) > new Date(ctdate)) {
            $("#txtCloseFromDate").val(ctdate);
        }
    }
}
function BindDropdownlist() {
    $.ajax({
        type: "POST",
        url: "WebAPI/WproductReturnReport.asmx/BindDropdownlist",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);

            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var Customer = $(xmldoc).find("Table");
                    var StatusType = $(xmldoc).find("Table2");
                    var SalesPerson = $(xmldoc).find("Table1");

                    $("#ddlCustomer option:not(:first)").remove();
                    $.each(Customer, function () {
                        $("#ddlCustomer").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CustomerName").text() + "</option>");
                    });
                    $("#ddlCustomer").select2();


                    $("#ddlSalesPerson option:not(:first)").remove();
                    $.each(SalesPerson, function () {
                        $("#ddlSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("SalesPerson").text() + "</option>");
                    });
                    $("#ddlSalesPerson").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
}
$("#ddlPageSize").change(function () {
    BindReport(1);
})
$("#btnSearch").click(function () {
    BindReport(1);
});

function BindReport(PageIndex) {

    var data = {
        SalesPerson: $("#ddlSalesPerson").val(),
        Customer: $("#ddlCustomer").val(),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        CloseFromDate: $("#txtCloseFromDate").val(),
        CloseToDate: $("#txtCloseToDate").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/WproductReturnReport.asmx/ProductExchangeReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'false') {
                    location.href = "/Default.aspx"
                }
                else {
                    SuccessPackerResport(response);
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function SuccessPackerResport(response) {
    var xmldoc = $.parseXML(response.d);
    var PackerResportDetail = $(xmldoc).find("Table1");
    var OrderTotal = $(xmldoc).find("Table2");
    var TotalFreshReturn = 0; TotalDamageReturn = 0;
    $("#tblProductReturnReport tbody tr").remove();
    var row = $("#tblProductReturnReport thead tr").clone(true);
    if (PackerResportDetail.length > 0) {
        $("#EmptyTable").hide();
        $("#tblProductReturnReport thead tr").show();
        $.each(PackerResportDetail, function (index) {
            $(".CustomerId", row).html($(this).find("CustomerId").text());
            $(".CustomerName", row).html($(this).find("CustomerName").text());
            $(".OrderNo", row).html($(this).find("OrderNo").text());
            $(".OrderDate", row).html($(this).find("OrderDate").text());
            $(".ClosedDate", row).html($(this).find("ClosedDate").text());
            $(".ProductId", row).html($(this).find("ProductId").text());
            $(".ProductName", row).html($(this).find("ProductName").text());
            $(".Driver", row).html($(this).find("Driver").text());
            $(".SalesPerson", row).html($(this).find("SalesPerson").text());
            $(".Unit", row).html($(this).find("UnitType").text());
            $(".FreshReturnQty", row).html($(this).find("FreshReturnQty").text());
            $(".DamageReturnQty", row).html($(this).find("DamageReturnQty").text());
            $("#tblProductReturnReport tbody").append(row);
            row = $("#tblProductReturnReport tbody tr:last").clone(true);

            TotalFreshReturn = TotalFreshReturn + parseInt($(this).find("FreshReturnQty").text());
            TotalDamageReturn = TotalDamageReturn + parseInt($(this).find("DamageReturnQty").text());
        });
        $('#TotalFreshReturn').html(TotalFreshReturn);
        $('#TotalDamageReturn').html(TotalDamageReturn);
    } else {
       // $("#EmptyTable").show();
        $('#TotalFreshReturn').html('0');
        $('#TotalDamageReturn').html('0');
    }
    $(OrderTotal).each(function () {
        $('#TotalFreshReturns').html(parseInt($(this).find('FreshReturnQty').text()));
        $('#TotalDamageReturns').html(parseInt($(this).find('DamageReturnQty').text()));
    });
    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
    //if (Number($("#ddlPageSize").val()) == '0') {
    //    $("#trTotal").hide();
    //}
    //else if (Number(pager.find("RecordCount").text()) < Number(pager.find("PageSize").text())) {
    //    $("#trTotal").hide();
    //}
    //else {
    //    $("#trTotal").show();
    //}
}

function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        SalesPerson: $("#ddlSalesPerson").val(),
        Customer: $("#ddlCustomer").val(),
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        CloseFromDate: $("#txtCloseFromDate").val(),
        CloseToDate: $("#txtCloseToDate").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/WproductReturnReport.asmx/ProductExchangeReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var PackerResportDetail = $(xmldoc).find("Table1");
                    var OrderTotal = $(xmldoc).find("Table2");
                    var PrintDate = $(xmldoc).find("Table");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (PackerResportDetail.length > 0) {
                        $.each(PackerResportDetail, function () {
                            $(".P_CustomerId", row).html($(this).find("CustomerId").text());
                            $(".P_CustomerName", row).html($(this).find("CustomerName").text());
                            $(".P_OrderNo", row).html($(this).find("OrderNo").text());
                            $(".P_OrderDate", row).html($(this).find("OrderDate").text());
                            $(".P_ClosedDate", row).html($(this).find("ClosedDate").text());
                            $(".P_ProductId", row).html($(this).find("ProductId").text());
                            $(".P_ProductName", row).html($(this).find("ProductName").text());
                            $(".P_Driver", row).html($(this).find("Driver").text());
                            $(".P_SalesPerson", row).html($(this).find("SalesPerson").text());
                            $(".P_Unit", row).html($(this).find("UnitType").text());
                            $(".P_FreshReturnQty", row).html($(this).find("FreshReturnQty").text());
                            $(".P_DamageReturnQty", row).html($(this).find("DamageReturnQty").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                    }
                    $(OrderTotal).each(function () {
                        $('#TotalFreshReturnss').html(parseInt($(this).find('FreshReturnQty').text()));
                        $('#TotalDamageReturnss').html(parseInt($(this).find('DamageReturnQty').text()));
                    });
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtFromDate").val() + " To " + $("#txtToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);

                    }

                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "ProductReportByReturn" + (new Date()).format("MM/dd/yyyy hh:mm tt"),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblProductReturnReport tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
    $("#btnExport").click(function () {
        if ($('#tblProductReturnReport tbody tr').length > 0) {
    CreateTable(2);
        }
        else {
            toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    });
    //----------------------------------Bind Customer---------------------------------------------
    function BindCustomer() {
        $.ajax({
            type: "POST",
            url: "WebAPI/WproductReturnReport.asmx/BindCustomer",
            data: "{'SalesPersonAutoId':'" + $('#ddlSalesPerson').val() + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            asyn: false,
            casshe: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d != "false") {
                        var xmldoc = $.parseXML(response.d);
                        var CustomerDetails = $(xmldoc).find("Table");
                        $("#ddlCustomer option:not(:first)").remove();
                        $.each(CustomerDetails, function () {
                            $("#ddlCustomer").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CustomerName").text() + "</option>");
                        });
                        $("#ddlCustomer").select2();
                    }
                }
                else {
                    location.href = "/";
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });


    }