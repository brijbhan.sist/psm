﻿$(document).ready(function () {
    $('.date').pickadate({
        min:new Date('01/01/2019'),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });     
    BindCustomer();
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtFromDate").val(month + '/' + day + '/' + year);
    $("#txtToDate").val(month + '/' + day + '/' + year);
    $("#txtDepositFromDate").val(month + '/' + day + '/' + year);
    $("#txtDepositToDate").val(month + '/' + day + '/' + year);
});


function setdatevalidation(val) {
    var fdate = $("#txtFromDate").val();
    var tdate = $("#txtToDate").val();
    var dfdate = $("#txtDepositFromDate").val();
    var dtdate = $("#txtDepositToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtFromDate").val(tdate);
        }
    }
    else if (val == 3) {
        if (dfdate == '' || new Date(dfdate) > new Date(dtdate)) {
            $("#txtDepositToDate").val(dfdate);
        }
    }
    else if (val == 4) {
        if (dfdate == '' || new Date(dfdate) > new Date(dtdate)) {
            $("#txtDepositFromDate").val(dtdate);
        }
    }
}
function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
};
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function BindCustomer() {
    $.ajax({
        type: "POST",
        url: "CheckReceivedpayment.aspx/BindCustomer",
        // data: "{'Customertype':'" + Customertype + "','SalesPersonAutoId':'" + SalesPerson + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                                       
                    var getData = $.parseJSON(response.d);
                    $("#ddlReceivedBy option:not(:first)").remove();
                    $.each(getData[0].EType, function (index, item) {
                        $("#ddlReceivedBy").append("<option value='" + item.ReceivedBy + "'>" + item.EmployeeName + "</option>");
                    });
                    $("#ddlReceivedBy").select2();

                    $("#ddlStatus option:not(:first)").remove();
                    $.each(getData[0].Stype, function (index, item){
                        $("#ddlStatus").append("<option value='" + item.SId + "'>" + item.Stype + "</option>");
                    });
                    $("#ddlStatus").select2();


                    $("#ddlCustomer option:not(:first)").remove();
                    $.each(getData[0].CUstomer, function (index, item) {
                        $("#ddlCustomer").append("<option value='" + item.CID + "'>" + item.CUN + "</option>");
                    });
                    $("#ddlCustomer").select2();

                    
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}


/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function BindReport(PageIndex) {
    var data = {
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        DepositFromDate: $("#txtDepositFromDate").val(),
        DepositToDate: $("#txtDepositToDate").val(),
        CustomerStatus: $("#ddlCustomer").val(),
        Status: $("#ddlStatus").val(),
        ReceivedBy: $("#ddlReceivedBy").val(),
        PageSize: $("#ddlPaging").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "CheckReceivedpayment.aspx/GetReport",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI()
        },
        success: success,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function success(response) {
    debugger;
    if (response.d != "Session Expired") {
        if (response.d != "false") {

            var xmldoc = $.parseXML(response.d);
            var CheckReport = $(xmldoc).find("Table1");
            var OrderTotal = $(xmldoc).find("Table2");
            $("#tblCheckReceivedPayment tbody tr").remove();
            var amount = 0;
            var row = $("#tblCheckReceivedPayment thead tr").clone(true);
            if (CheckReport.length > 0) {
                $("#EmptyTable").hide();
                $.each(CheckReport, function () {
                    $(".PaymentId", row).text($(this).find("PayId").text());
                    $(".ReceivedDate", row).text($(this).find("ReceivedDate").text());
                    $(".CustomerId", row).text($(this).find("CustomerId").text());
                    $(".CustomerName", row).text($(this).find("CustomerName").text());
                    $(".CheckAmount", row).text($(this).find("ReceivedAmount").text());
                    $(".CheckNoRefNo", row).text($(this).find("CheckNo").text());
                    $(".ReceivedBy", row).text($(this).find("ReceivedBy").text());
                    $(".SettelledBy", row).text($(this).find("SettelledBy").text());
                    $(".SettelledDate", row).text($(this).find("PaymentDate").text());
                    $(".DepositDate", row).text($(this).find("CheckDepositDate").text());
                    if ($(this).find("Status").text() == 1) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill badge-info'>" + $(this).find("StatusType").text() + "</span>");
                    } else if ($(this).find("Status").text() == 2) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill badge-success'>" + $(this).find("StatusType").text() + "</span>");
                    }
                    else if ($(this).find("Status").text() == 3) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("StatusType").text() + "</span>");
                    }
                    else if ($(this).find("Status").text() == 4) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill badge-warning'>" + $(this).find("StatusType").text() + "</span>");
                    }
                    else if ($(this).find("Status").text() == 5) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill badge-danger'>" + $(this).find("StatusType").text() + "</span>");
                    }
                    $("#tblCheckReceivedPayment tbody").append(row);
                    row = $("#tblCheckReceivedPayment tbody tr:last").clone(true);
                    amount += parseFloat($(this).find("ReceivedAmount").text());
                });

                $("#TotalAmount").text(parseFloat(amount).toFixed(2));
            } else {
                $("#TotalAmount").text('0.00');
            }
            $(OrderTotal).each(function () {
                $('#TotalAmounts').html(parseFloat($(this).find('ReceivedAmount').text()).toFixed(2));
            });
            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });

            if (Number($("#ddlPaging").val()) == '0') {
                $("#trTotal").hide();
            }
            else if (Number(pager.find("RecordCount").text()) < Number(pager.find("PageSize").text())) {
                $("#trTotal").hide();
            }
            else {
                $("#trTotal").show();
            }

        }
    }
    else {
        location.href = "/";
    }
}

$("#btnSearch").click(function () {

    BindReport(1);

})
$("#ddlPaging").change(function () {

    BindReport(1);

})

function CreateTable(us) {
    var image = $("#imgName").val();
    var data = {
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        DepositFromDate: $("#txtDepositFromDate").val(),
        DepositToDate: $("#txtDepositToDate").val(),
        CustomerStatus: $("#ddlCustomer").val(),
        Status: $("#ddlStatus").val(),
        ReceivedBy: $("#ddlReceivedBy").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "CheckReceivedpayment.aspx/GetReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var PrintDate = $(xmldoc).find("Table");
                    var CheckReport = $(xmldoc).find("Table1");
                    $("#PrintTable tbody tr").remove();
                    var amount = 0;
                    var row = $("#PrintTable thead tr").clone(true);
                    if (CheckReport.length > 0) {
                        $.each(CheckReport, function () {
                            $(".P_PaymentId", row).text($(this).find("PayId").text());
                            $(".P_ReceivedDate", row).text($(this).find("ReceivedDate").text());
                            $(".P_CustomerId", row).text($(this).find("CustomerId").text());
                            $(".P_CustomerName", row).text($(this).find("CustomerName").text());
                            $(".P_CheckAmount", row).text($(this).find("ReceivedAmount").text());
                            $(".P_CheckNoRefNo", row).text($(this).find("CheckNo").text());
                            $(".P_ReceivedBy", row).text($(this).find("ReceivedBy").text());
                            $(".P_DepositDate", row).text($(this).find("CheckDepositDate").text());
                            $(".P_SettelledBy", row).text($(this).find("SettelledBy").text());
                            $(".P_SettelledDate", row).text($(this).find("PaymentDate").text());
                            $(".P_StatusType", row).html($(this).find("StatusType").text());                           
                            amount += parseFloat($(this).find("ReceivedAmount").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                        $("#P_TotalAmount").text(parseFloat(amount).toFixed(2));
                    } else {
                        $("#P_TotalAmount").text('0.00');
                    }
                    if (us == 1) {
                       
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtFromDate").val() + " To " + $("#txtToDate").val());
                        } else {
                            $("#DateRange").html('');
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        //mywindow.document.write(divToPrint.outerHTML);

                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "By Received Check" + (PrintDate.find('PrintDate').text()),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblCheckReceivedPayment tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
     
    if ($('#tblCheckReceivedPayment tbody tr').length > 0) {
    CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}



