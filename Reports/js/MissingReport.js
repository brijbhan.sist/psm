﻿$(document).ready(function () {
    $('.date').pickadate({
        min: new Date('01/01/2019'),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $(".date").val(month + '/' + day + '/' + year);
    BindDropdown();
    BindCustomer();
    //GetMissingReport(1);
});
function setdatevalidation(val) {
    var fdate = $("#txtDateFrom").val();
    var tdate = $("#txtDateTo").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtDateTo").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtDateFrom").val(tdate);
        }
    }
}
function BindDropdown() {
    $.ajax({
        type: "POST",
        url: "MissingReport.aspx/bindDropDown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {

                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlCategory option:not(:first)").remove();
                    $.each(getData[0].Category, function (index, item) {
                        $("#ddlCategory").append("<option value='" + item.CAID + "'>" + item.CAN + "</option>");
                    });
                    $("#ddlCategory").select2();

                    $("#ddlSalesPerson option:not(:first)").remove();
                    $.each(getData[0].SalesPerson, function (index, item) {
                        $("#ddlSalesPerson").append("<option value='" + item.SID + "'>" + item.SP + "</option>");
                    });
                    $("#ddlSalesPerson").select2();


                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function BindSubCategory(category) {
    var data = {
        CategoryAutoId: category
    }
    $.ajax({
        type: "POST",
        url: "MissingReport.aspx/BindSubCategory",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlSubCategory option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlSubCategory").append("<option value='" + getData[index].SCID + "'>" + getData[index].SCN + "</option>");
                    });
                    $("#ddlSubCategory").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function BindCustomer() {
    var data = {
        SalesPerson: $("#ddlSalesPerson").val()
    }
    $.ajax({
        type: "POST",
        url: "MissingReport.aspx/BindCustomer",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlCustomer option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlCustomer").append("<option value='" + getData[index].AutoId + "'>" + getData[index].CustomerName + "</option>");
                    });
                    $("#ddlCustomer").select2();;
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function GetMissingReport(PageIndex) {
    var data = {
        FromDate: $("#txtDateFrom").val(),
        ToDate: $("#txtDateTo").val(),
        CustomerAutoId: $("#ddlCustomer").val() || 0,
        CategoryAutoId: $("#ddlCategory").val() || 0,
        SubCategoryAutoId: $("#ddlSubCategory").val() || 0,
        SalesAutoId: $("#ddlSalesPerson").val() || 0,
        PageIndex: PageIndex || 1,
        PageSize: $('#ddlPageSize').val()
    };
    $.ajax({
        type: "POST",
        url: "MissingReport.aspx/GetMissingReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            
        },
        complete: function () {
            $.unblockUI();
        },
        success: onSuccessOfReport,
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function onSuccessOfReport(response) {
    var xmldoc = $.parseXML(response.d);
    var ReportDetails = $(xmldoc).find("Table");
    $("#tblMissingReportDetail tbody tr").remove();
    var row = $("#tblMissingReportDetail thead tr").clone(true);
    if (ReportDetails.length > 0) {
        $.each(ReportDetails, function () {

            $(".ProductId", row).text($(this).find("ProductId").text());
            $(".ProductName", row).text($(this).find("ProductName").text());
            $(".CustomerId", row).text($(this).find("CustomerId").text());
            $(".CustomerName", row).text($(this).find("CustomerName").text());
            $(".OrderNo", row).text($(this).find("OrderNo").text());
            $(".SalesPerson", row).text($(this).find("SalesRep").text());
            $(".OrderDate", row).text($(this).find("OrderDate").text());
            $(".Packer", row).text($(this).find("Packer").text());
            $(".PackedTime", row).text($(this).find("PackedTime").text());
            $(".Driver", row).text($(this).find("Driver").text());
            $(".DeliveryDate", row).text($(this).find("DeliveryDate").text());
            $(".Account", row).text($(this).find("Account").text());
            $(".OrderCloseDate", row).text($(this).find("OrderCloseDate").text());
            $(".MissingDetails", row).text($(this).find("MissingDetails").text());

            $("#tblMissingReportDetail tbody").append(row);
            row = $("#tblMissingReportDetail tbody tr:last").clone(true);

        });

       
    }
    else {
        $('#tblMissingReportDetail tbody tr').remove();

    }
    var pager = $(xmldoc).find("Table1");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}

function searchReport() {
    GetMissingReport(1);
}

function missingReportOnChange(pageindex) {
    GetMissingReport(pageindex);
}
function Pagevalue(e) {
    GetMissingReport(parseInt($(e).attr("page")));
};



function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        FromDate: $("#txtDateFrom").val(),
        ToDate: $("#txtDateTo").val(),
        CustomerAutoId: $("#ddlCustomerName").val() || 0,
        CategoryAutoId: $("#ddlCategory").val() || 0,
        SubCategoryAutoId: $("#ddlSubCategory").val() || 0,
        SalesAutoId: $("#ddlSalesPerson").val() || 0,
        PageIndex: 1,
        PageSize: 0
    };
    $.ajax({
        type: "POST",
        url: "MissingReport.aspx/GetMissingReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ReportDetails = $(xmldoc).find("Table");
                    var PrintDate = $(xmldoc).find("Table1");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (ReportDetails.length > 0) {
                        $.each(ReportDetails, function () {
                            $(".p_ProductId", row).text($(this).find("ProductId").text());
                            $(".p_ProductName", row).text($(this).find("ProductName").text());
                            $(".p_CustomerId", row).text($(this).find("CustomerId").text());
                            $(".p_CustomerName", row).text($(this).find("CustomerName").text());
                            $(".p_OrderNo", row).text($(this).find("OrderNo").text());
                            $(".p_SalesPerson", row).text($(this).find("SalesRep").text());
                            $(".p_OrderDate", row).text($(this).find("OrderDate").text());
                            $(".p_Packer", row).text($(this).find("Packer").text());
                            $(".p_PackedTime", row).text($(this).find("PackedTime").text());
                            $(".p_Driver", row).text($(this).find("Driver").text());
                            $(".p_DeliveryDate", row).text($(this).find("DeliveryDate").text());
                            $(".p_Account", row).text($(this).find("Account").text());
                            $(".p_OrderCloseDate", row).text($(this).find("OrderCloseDate").text());
                            $(".p_MissingDetails", row).text($(this).find("MissingDetails").text());

                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });

                    }
                    if (us == 1) {

                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtDateFrom").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtDateFrom").val() + " To " + $("#txtDateTo").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "Missing product report",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}


/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblMissingReportDetail tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblMissingReportDetail tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}