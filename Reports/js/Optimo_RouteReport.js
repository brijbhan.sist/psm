﻿var PrintDates = '';
$(document).ready(function () {
    $('#txtFromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtToDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtFromDate").val(month + '/' + day + '/' + year);
    $("#txtToDate").val(month + '/' + day + '/' + year);
    PrintDates = (new Date()).format("MM/dd/yyyy hh:mm tt");
});

function setdatevalidation(val) {
    debugger
    var fdate = $("#txtFromDate").val();
    var tdate = $("#txtToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtFromDate").val(tdate);
        }
    }
}

function Pagevalue(e) {
    GetRouteReport(parseInt($(e).attr("page")));
}

function GetRouteReport(PageIndex) {
    var data = {
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "Optimo_RouteReport.aspx/GetRouteReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: SuccessCustomerCreditReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function SuccessCustomerCreditReport(response) {
    if (response.d == 'false') {
    }
    else {
        var xmldoc = $.parseXML(response.d);
        var Report = $(xmldoc).find("Table");
      
        $("#tblReportPaymentDailyReport tbody tr").remove();
        var row = $("#tblReportPaymentDailyReport thead tr").clone(true);
        if (Report.length > 0) {
            $("#EmptyTable").hide();
            $.each(Report, function () {
                $(".PlanBy", row).text($(this).find("PlanBy").text());
                $(".PlanningDate", row).text($(this).find("RouteDate").text());
                $(".PlanningId", row).html('<a href="#" onclick="GetPlanningDetails(this)">' + $(this).find("PlanningId").text()+'</a>');
                $(".Drivers", row).text($(this).find("NoOfDriver").text());
                $(".Stops", row).text($(this).find("NoOfStop").text());
                $(".Orders", row).text($(this).find("NoOfOrder").text());
                $("#tblReportPaymentDailyReport tbody").append(row);
                $("#tblReportPaymentDailyReport tbody").append('<div class=' + $(this).find("PlanningId").text()+' style="display:none"></div>');
                row = $("#tblReportPaymentDailyReport tbody tr:last").clone(true);

            });
        }
        else {
            $("#EmptyTable").show();
        }
        var pager = $(xmldoc).find("Table1");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
    }
}

function GetPlanningDetails(e) {
    debugger;
    var row = $(e).closest('tr');
   
   var PlanningId=$(row).find(".PlanningId a").html();
    var PlanningDate = $(row).find(".PlanningDate").text();
    $("#PlanningIDName").html("Planning Id : "+PlanningId + " -   Planning Date : " + PlanningDate);
    var data = {
        PlanningId: PlanningId,
    };
    $("#hdnPlanId").val(PlanningId);
    $.ajax({
        type: "POST",
        url: "Optimo_RouteReport.aspx/GetPlanningDetails",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response)
        {
            if (response.d == 'false') {
            }
            else {
                var xmldoc = $.parseXML(response.d);
                var Report = $(xmldoc).find("Table");

                $("#tblPlanningDetails tbody tr").remove();
                var row = $("#tblPlanningDetails thead tr").clone(true);
                if (Report.length > 0) {
                    $("#EmptyTable33").hide();
                    $("#ShowPlanningDetails").modal('show');
                    $.each(Report, function () {
                        $(".Action", row).html('<a href="#" onclick="PrintDriverOrd(' + $(this).find("DriverAutoId").text() + ',' + PlanningId +')"><span class="la la-print"></span></a>');
                        $(".DriverName", row).html('<a href="#" DriverAutoId=' + $(this).find("DriverAutoId").text() + ' PlanningAutoId=' + PlanningId+' onclick="GetDriverDetails(this)">' + $(this).find("Driver").text() + '</a>');
                        $(".StartTime", row).text($(this).find("DriverStartTime").text());
                        $(".EndTime", row).text($(this).find("DraverEndTime").text());
                        $(".TotalStops", row).text($(this).find("NoOfStop").text());
                        $(".TotalOrders", row).text($(this).find("NOOfOrder").text());
                        $(".CarName", row).text($(this).find("CarName").text());
                        $("#tblPlanningDetails tbody").append(row);
                        row = $("#tblPlanningDetails tbody tr:last").clone(true);
                    });
                }
                else {
                    $("#EmptyTable33").show();
                }

            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


function GetDriverDetails(e) {
    debugger;
    var row = $(e).closest('tr');
  
    var data = {
        PlanningId: $(row).find(".DriverName a").attr('planningautoid'),
        DriverAutoId: $(row).find(".DriverName a").attr('driverautoid'),
        images: $("#imgName").val(),
    };
    localStorage.setItem('DriverLog_Print', JSON.stringify(data));
    $("#spOrdDtls").html("Driver Name : " + $(row).find(".DriverName a").html() + " - " + $("#PlanningIDName").html());
    $.ajax({
        type: "POST",
        url: "Optimo_RouteReport.aspx/GetDriverDetails",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: SuccessGetDriverDetails,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function SuccessGetDriverDetails(response) {
    if (response.d == 'false') {
    }
    else {
        var xmldoc = $.parseXML(response.d);
        var Report = $(xmldoc).find("Table");
        var html = '',StopNo='';
        $("#tblDriverDetails tbody tr").remove();
        if (Report.length > 0) {
            $("#EmptyTable34").hide();
            $("#ShowDriverDetails").modal('show');
            $.each(Report, function () {
                if (Number($(this).find("NOOfOrder").text()) == 1) {
                    html += '<tr><td>' + $(this).find("SalesPerson").text() + '</td>'
                    html += '<td>' + $(this).find("StopNo").text() + '</td>'
                    html += '<td>' + $(this).find("CustomerName").text() + '</td>'
                    html += '<td>' + $(this).find("OrderNo").text() + '</td>'
                    html += '<td>' + $(this).find("Status").text() + '</td>'
                    html += '<td>' + $(this).find("StoreOpenTime").text() + '</td>'
                    html += '<td>' + $(this).find("SuggestedTime").text() + '</td>'
                    html += '<td>' + $(this).find("AssignDeliverytime").text() + '</td>'
                    html += '<td>' + $(this).find("DelivereTime").text() + '</td>'
                    html += '<td>' + $(this).find("Remark").text() + '</td>'
                    html += '<td>' + $(this).find("AcctRemarks").text() + '</td></tr>'
                    StopNo = $(this).find("StopNo").text();
                    
                }
                else {
                    if (StopNo != $(this).find("StopNo").text()) {
                        html += '<tr><td>' + $(this).find("SalesPerson").text() + '</td>'
                        html += '<td rowspan=' + $(this).find("NOOfOrder").text() + '>' + $(this).find("StopNo").text() + '</td>'
                        html += '<td rowspan=' + $(this).find("NOOfOrder").text() + '>' + $(this).find("CustomerName").text() + '</td>'
                        html += '<td>' + $(this).find("OrderNo").text() + '</td>'
                        html += '<td>' + $(this).find("Status").text() + '</td>'
                        html += '<td rowspan=' + $(this).find("NOOfOrder").text() + '>' + $(this).find("StoreOpenTime").text() + '</td>'
                        html += '<td rowspan=' + $(this).find("NOOfOrder").text() + '>' + $(this).find("SuggestedTime").text() + '</td>'
                        html += '<td rowspan=' + $(this).find("NOOfOrder").text() + '>' + $(this).find("AssignDeliverytime").text() + '</td>'
                        html += '<td>' + $(this).find("DelivereTime").text() + '</td>'
                        html += '<td>' + $(this).find("Remark").text() + '</td>'
                        html += '<td>' + $(this).find("AcctRemarks").text() + '</td></tr>';
                    }
                    else {
                        html += '<tr><td>' + $(this).find("SalesPerson").text() + '</td>'
                        
                        html += '<td>' + $(this).find("OrderNo").text() + '</td>';
                        html += '<td>' + $(this).find("Status").text() + '</td>'
                        html += '<td>' + $(this).find("DelivereTime").text() + '</td>'
                        html += '<td>' + $(this).find("Remark").text() + '</td>'
                        html += '<td>' + $(this).find("AcctRemarks").text() + '</td></tr>';
                    }
                    StopNo = $(this).find("StopNo").text();
                   
                }
            });
            console.log(html);
            $("#tblDriverDetails tbody").append(html);
        }
        else {
            $("#EmptyTable34").show();
        }

    }
}


function PrintOrder(ReportAutoId) {
    window.open("/Reports/PaymentLog_BankReportLog.html?dt=" + ReportAutoId, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}
function PrintDriverOrd(DriverAutoId, PlanningAutoId) {
    if (DriverAutoId != 0 && PlanningAutoId != 0) {
         var data = {
        PlanningId: PlanningAutoId,
        DriverAutoId: DriverAutoId,
        images: $("#imgName").val()
    };
    localStorage.setItem('DriverLog_Print', JSON.stringify(data));
    }
    window.open("/Reports/DriverLog_Print.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
   
}
function ViewLog(ReportAutoId) {
    location.href = "/Reports/BankReportLogView.aspx?dt=" + ReportAutoId;
}

function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "Optimo_RouteReport.aspx/GetRouteReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',

                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var Report = $(xmldoc).find("Table");
                    var PrintDate = $(xmldoc).find("Table1");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr:last-child").clone(true);
                    if (Report.length > 0) {
                        $.each(Report, function () {
                            $(".P_PlanBy", row).text($(this).find("PlanBy").text());
                            $(".P_PlanningDate", row).text($(this).find("RouteDate").text());
                            $(".P_PlanningId", row).text($(this).find("PlanningId").text());
                            $(".P_Drivers", row).text($(this).find("NoOfDriver").text());
                            $(".P_Stops", row).text($(this).find("NoOfStop").text());
                            $(".P_Orders", row).text($(this).find("NoOfOrder").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);

                        });
                       
                    }
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtFromDate").val() + " To " + $("#txtToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "OptimoRouteReport" + (PrintDate.find('PrintDate').text()),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblReportPaymentDailyReport tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblReportPaymentDailyReport tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function PrintAllDriver_Order() {
    $("#myModalorderBy").modal('show');
}
function Print_AllDriversOrder() {
    $("#myModalorderBy").modal('hide');
   
    var data = {
        images: $("#imgName").val(),
        PlanningId: $("#hdnPlanId").val(),
        DriverAutoId: 0,
        OrderBy: $("input[name='NameType']:checked").val(),
      
    }
    localStorage.setItem('DriverLog_PrintAll', JSON.stringify(data));
    window.open("/Reports/DriverLog_Print.html?dt=" + 1, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");

}

