﻿$(document).ready(function () {
    debugger;
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    BindCategory();
    BindCustomer();
    BindSubCategory
    BindProductSubCategory();
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtDateFrom").val(month + '/' + day + '/' + year);
    $("#txtDateTo").val(month + '/' + day + '/' + year);
});

function Pagevalue(e) {
    GetProductReport(parseInt($(e).attr("page")));
};
$("#ddlPageSize").change(function () {
    GetProductReport(1);
})
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/

function BindCategory() {
    $.ajax({
        type: "POST",
        url: "PLProductWiseReport.aspx/BindCategory",

        data: "{}",
        contentType: "application/json; charset=utf-8",

        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlCategory option:not(:first)").remove();
                    $.each(getData[0].Category, function (index, item) {
                        $("#ddlCategory").append("<option value='" + item.CAID + "'>" + item.CAN + "</option>");
                    });
                    $("#ddlCategory").select2();


                    $("#ddlSalesPerson option:not(:first)").remove();
                    $.each(getData[0].SalesPerson, function (index, item) {
                        $("#ddlSalesPerson").append("<option value='" + item.SID + "'>" + item.SP + "</option>");
                    });
                    $("#ddlSalesPerson").select2();


                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function BindSubCategory() {
    var data = {
        CategoryAutoId: $("#ddlCategory").val()
    }
    $.ajax({
        type: "POST",
        url: "PLProductWiseReport.aspx/BindSubCategory",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlSubCategory option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlSubCategory").append("<option value='" + getData[index].SCID + "'>" + getData[index].SCN + "</option>");
                    });
                    $("#ddlSubCategory").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

//------------------------------------------------------Bind Customer---------------------------------------------
function BindCustomer() {
    $.ajax({
        type: "POST",
        url: "PLProductWiseReport.aspx/BindCustomer",
        data: "{'SalesPersonAutoId':'" + $('#ddlSalesPerson').val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlCustomerName option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlCustomerName").append("<option value='" + getData[index].CUID + "'>" + getData[index].CUN + "</option>");
                    });
                    $("#ddlCustomerName").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}
//---------------------------------------------Bind Subcategory-----------------------------------------------------------------
function BindProductSubCategory() {
    $.ajax({
        type: "POST",
        url: "PLProductWiseReport.aspx/BindProduct",
        data: "{'SubCategoryAutoId':'" + $("#ddlCategory").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlProductName option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlProductName").append("<option value='" + getData[index].PID + "'>" + getData[index].PN + "</option>");
                    });
                    $("#ddlProductName").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
/*-------------------------------------------------------Get Product Report----------------------------------------------------------*/
function GetProductReport(PageIndex) {
    var data = {
        FromDate: $("#txtDateFrom").val(),
        ToDate: $("#txtDateTo").val(),
        CustomerAutoId: $("#ddlCustomerName").val() || 0,
        CategoryAutoId: $("#ddlCategory").val() || 0,
        SubCategoryAutoId: $("#ddlSubCategory").val() || 0,
        ProductAutoId: $("#ddlProductName").val() || 0,
        SalesAutoId: $("#ddlSalesPerson").val() || 0,
        PageIndex: PageIndex,
        PageSize: $('#ddlPageSize').val()
    };
    $.ajax({
        type: "POST",
        url: "PLProductWiseReport.aspx/GetProductReport",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successgetReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successgetReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            debugger;
            var xmldoc = $.parseXML(response.d);
            var ReportDetails = $(xmldoc).find("Table");
            var OTotal = $(xmldoc).find("Table2");
            $("#tblProductWiseResport tbody tr").remove();
            var row = $("#tblProductWiseResport thead tr").clone(true);
            var TotalSoldQty = 0, TotalCostPrice = 0, TotalSoldAmount = 0, TotalProfit = 0.00, TotalPerProfit = 0, totalNoOfOrders = 0;
            if (ReportDetails.length > 0) {
                $("#EmptyTable").hide();
                $.each(ReportDetails, function (index) {

                    $(".ProductId", row).text($(this).find("ProductId").text());
                    $(".ProductName", row).text($(this).find("ProductName").text());
                    $(".FirstOrderDate", row).text($(this).find("FirstOrderDate").text());
                    $(".LastOrderDate", row).text($(this).find("LastOrderDate").text());
                    $(".totalNoOfOrders", row).text($(this).find("totalNoOfOrders").text());
                    $(".TotalSoldQty", row).text($(this).find("TotalQty").text());
                    $(".TotalCostPrice", row).text($(this).find("CostPrice").text());
                    $(".TotalSoldAmount", row).text($(this).find("SoldPrice").text());
                    $(".TotalProfit", row).text($(this).find("Profit").text());
                    $(".ProfitPer", row).text($(this).find("ProfitPer").text());
                    $(".UnitType", row).text($(this).find("UnitType").text());

                    TotalSoldQty = TotalSoldQty + parseInt($(this).find("TotalQty").text());
                    totalNoOfOrders = totalNoOfOrders + parseInt($(this).find("totalNoOfOrders").text());
                    TotalSoldAmount = TotalSoldAmount + parseFloat($(this).find("SoldPrice").text());
                    TotalCostPrice = TotalCostPrice + parseFloat($(this).find("CostPrice").text());
                    TotalProfit = TotalProfit + parseFloat($(this).find("Profit").text());
                    TotalPerProfit = TotalPerProfit + parseFloat($(this).find("ProfitPer").text());
                    $("#tblProductWiseResport tbody").append(row);
                    row = $("#tblProductWiseResport tbody tr:last").clone(true);
                });

                $('#TotalSoldQty').html(parseFloat(TotalSoldQty).toFixed(0));
                $('#totalNoOfOrders').html(parseFloat(totalNoOfOrders).toFixed(0));
                $('#TotalSoldAmount').html(parseFloat(TotalSoldAmount).toFixed(2));
                $('#TotalCostPrice').html(parseFloat(TotalCostPrice).toFixed(2)); 
                $('#TotalProfit').html(parseFloat(TotalProfit).toFixed(2));
                $('#TotalProfitPer').html(parseFloat((parseFloat(TotalProfit).toFixed(2) * 100) / parseFloat(TotalCostPrice).toFixed(2)).toFixed(2));
                
            } else {
                $('#TotalSoldQty').html('0.00');
                $('#totalNoOfOrders').html('0');
                $('#TotalCostPrice').html('0.00');
                $('#TotalSoldAmount').html('0.00');
                $('#TotalProfit').html('0.00');
                $('#TotalProfitPer').html('0.00');
            }
            $(OTotal).each(function () {
                $('#TotalSoldQtya').html(parseFloat($(this).find("TotalQty").text()).toFixed(0));
                $('#totalNoOfOrdersa').html(parseFloat($(this).find("totalNoOfOrders").text()).toFixed(0));
                $('#TotalSoldAmounta').html(parseFloat($(this).find("SoldPrice").text()).toFixed(2));
                $('#TotalCostPricea').html(parseFloat($(this).find("CostPrice").text()).toFixed(2)); 
                $('#TotalProfita').html(parseFloat($(this).find("Profit").text()).toFixed(2));
                $('#TotalProfitPera').html(parseFloat($(this).find("ProfitPer").text()).toFixed(2));
            });
            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}

$("#btnSearch").click(function () {
    GetProductReport(1);
})
function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        FromDate: $("#txtDateFrom").val(),
        ToDate: $("#txtDateTo").val(),
        CustomerAutoId: $("#ddlCustomerName").val() || 0,
        CategoryAutoId: $("#ddlCategory").val() || 0,
        SubCategoryAutoId: $("#ddlSubCategory").val() || 0,
        ProductAutoId: $("#ddlProductName").val() || 0,
        SalesAutoId: $("#ddlSalesPerson").val() || 0,
        PageIndex: 1,
        PageSize: 0
    };
    $.ajax({
        type: "POST",
        url: "PLProductWiseReport.aspx/GetProductReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ReportDetails = $(xmldoc).find("Table");
                    var Print = $(xmldoc).find("Table3");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    var TotalSoldQty = 0, TotalCostPrice = 0, TotalSoldAmount = 0, TotalProfit = 0.00, TotalPerProfit = 0, totalNoOfOrders = 0;
                    if (ReportDetails.length > 0) {
                        $.each(ReportDetails, function () {
                            $(".P_ProductId", row).text($(this).find("ProductId").text());
                            $(".P_ProductName", row).text($(this).find("ProductName").text());
                            $(".P_FirstOrderDate", row).text($(this).find("FirstOrderDate").text());
                            $(".P_LastOrderDate", row).text($(this).find("LastOrderDate").text());
                            $(".P_totalNoOfOrders", row).text($(this).find("totalNoOfOrders").text());
                            $(".P_TotalSoldQty", row).text($(this).find("TotalQty").text());
                            $(".P_TotalCostPrice", row).text($(this).find("CostPrice").text());
                            $(".P_TotalSoldAmount", row).text($(this).find("SoldPrice").text());
                            $(".P_TotalProfit", row).text($(this).find("Profit").text());
                            $(".P_ProfitPer", row).text($(this).find("ProfitPer").text());
                            $(".P_UnitType", row).text($(this).find("UnitType").text());

                            TotalSoldQty = TotalSoldQty + parseInt($(this).find("TotalQty").text());
                            totalNoOfOrders = totalNoOfOrders + parseInt($(this).find("totalNoOfOrders").text());
                            TotalCostPrice = TotalCostPrice + parseFloat($(this).find("CostPrice").text());
                            TotalSoldAmount = TotalSoldAmount + parseFloat($(this).find("SoldPrice").text()); 
                            TotalProfit = TotalProfit + parseFloat($(this).find("Profit").text());
                            TotalPerProfit = TotalPerProfit + parseFloat($(this).find("ProfitPer").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                        $('#P_TotalSoldQty').html(parseFloat(TotalSoldQty).toFixed(0));
                        $('#P_totalNoOfOrders').html(parseFloat(totalNoOfOrders).toFixed(0));
                        $('#P_TotalCostPrice').html(parseFloat(TotalCostPrice).toFixed(2));
                        $('#P_TotalSoldAmount').html(parseFloat(TotalSoldAmount).toFixed(2));
                        $('#P_TotalProfit').html(parseFloat(TotalProfit).toFixed(2));
                        $('#P_TotalProfitPer').html(parseFloat((parseFloat(TotalProfit).toFixed(2) * 100) / parseFloat(TotalCostPrice).toFixed(2)).toFixed(2));

                          $('#P_TotalSoldQty').css('font-weight', 'bold');
                        $('#P_totalNoOfOrders').css('font-weight', 'bold');
                        $('#P_TotalCostPrice').css('font-weight', 'bold');


                        $('#P_TotalSoldAmount').css('font-weight', 'bold');
                        $('#P_TotalProfit').css('font-weight', 'bold');
                        $('#P_TotalProfitPer').css('font-weight', 'bold');
                        $('#lb').css('font-weight', 'bold'); 
                    }
                    if (us == 1) {
                       
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + $(Print).find("PrintDate").text());
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtDateFrom").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtDateFrom").val() + " To " + $("#txtDateTo").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        //mywindow.document.write(divToPrint.outerHTML);

                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "P&LReportByProduct",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblProductWiseResport tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblProductWiseResport tbody tr').length > 0) {
    CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

