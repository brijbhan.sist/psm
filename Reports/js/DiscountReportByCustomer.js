﻿var row1 = "";
$(document).ready(function () {
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    //BindCustomerDropdown()
    BindSalesPersonandCustomer();

});
function Pagevalue(e) {
    getReport(parseInt($(e).attr("page")));
};


function BindCustomerDropdown() {
    var SalesAutoId="0,";
    $("#ddlAllPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesAutoId = $(this).val() + ',';
        } else {
            SalesAutoId += $(this).val() + ',';
        }
    });
    if (SalesAutoId == "0,") {
        SalesAutoId = "0"
    }
    $.ajax({
        type: "POST",
        url: "/Reports/DiscountReportByCustomer.aspx/CustomerDropdown",
        data: "{'SalesPersonAutoId':'" + SalesAutoId.toString() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var Customer = $(xmldoc).find("Table");
                    $("#ddlAllCustomer option:not(:first)").remove();
                    $.each(Customer, function () {
                        $("#ddlAllCustomer").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CustomerName").text() + "</option>");
                    });
                    $("#ddlAllCustomer").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function BindSalesPersonandCustomer() {
     
    $.ajax({
        type: "POST",
        url: "/Reports/DiscountReportByCustomer.aspx/BindCustomer",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {

                    var xmldoc = $.parseXML(response.d);
                    var SalesPersonList = $(xmldoc).find("Table");
                    var CustomerList = $(xmldoc).find("Table3");

                    $("#ddlAllPerson option:not(:first)").remove();
                    $.each(SalesPersonList, function () {
                        $("#ddlAllPerson").append("<option value='" + $(this).find("EmpAutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
                    });
                    $("#ddlAllPerson").select2();

                    $("#ddlAllPerson").attr('multiple', 'multiple');
                    $("#ddlAllPerson").select2({
                        placeholder: 'All Sales Person',
                        allowClear: true
                    });


                    $("#ddlAllCustomer option:not(:first)").remove();
                    $.each(CustomerList, function () {
                        $("#ddlAllCustomer").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CustomerName").text() + "</option>");
                    });
                    $("#ddlAllCustomer").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}

function getReport(PageIndex) {
    var SalesAutoId = "0,";
    $("#ddlAllPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesAutoId = $(this).val() + ',';
        } else {
            SalesAutoId += $(this).val() + ',';
        }
    });
    if (SalesAutoId == "0,") {
        SalesAutoId = "0"
    }
    var data = {
        CustomerAutoId: $("#ddlAllCustomer").val(),
        SalesAutoId: SalesAutoId.toString(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: PageIndex,
        PageSize: $('#ddlPageSize').val()
    };

    $.ajax({
        type: "POST",
        url: "/Reports/DiscountReportByCustomer.aspx/GetReportDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successgetReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function successgetReport(response) {
    console.log("success get response calling");
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var row = '';
            var ReportList = JSON.parse(response.d);
            row = $("#tblOrderList thead tr").clone(true);
            $("#tblOrderList tbody tr").remove();
            var totalDisc = 0.00;
            var totalAmt = 0.00;
            $.each(ReportList[0].ReportList, function (i, ord) {
                totalDisc = totalDisc+ord.OverallDiscAmt;
                totalAmt = totalAmt+ord.TotalAmount;
                 $(".CustomerId", row).text(ord.CustomerId);
                $(".CustomerName", row).text(ord.CustomerName);
                $(".FirstName", row).text(ord.FirstName);
                $(".OrderNo", row).text(ord.OrderNo);
                $(".OrderDate", row).text(ord.OrderDate);
                $(".TotalRowAmount", row).text(parseFloat(ord.TotalAmount).toFixed(2));
                $(".Discountper", row).text(parseFloat(ord.OverallDisc).toFixed(2));
                $(".OverallRowAmount", row).text(parseFloat(ord.OverallDiscAmt).toFixed(2));
                $("#tblOrderList tbody").append(row);
                row = $("#tblOrderList tbody tr:last").clone(true);
            });
            $('#overallTotal').html(parseFloat(ReportList[0].TotalAmount).toFixed(2));
            $('#overallDiscAmt').html(parseFloat(ReportList[0].OverallDiscAmt).toFixed(2));

            $('#TotalAmt').html(parseFloat(totalAmt).toFixed(2));
            $('#TotalDisc').html(parseFloat(totalDisc).toFixed(2));

            var pager = ReportList[0].orderPaging[0];

            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.PageIndex),
                PageSize: parseInt(pager.PageSize),
                RecordCount: parseInt(pager.RecordCount)
            });
        }
    }
    else {
        location.href = "/";
    }

}

function CreateTable(us) {
    var SalesAutoId = "0,";
    $("#ddlAllPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesAutoId = $(this).val() + ',';
        } else {
            SalesAutoId += $(this).val() + ',';
        }
    });
    if (SalesAutoId == "0,") {
        SalesAutoId = "0"
    }
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        CustomerAutoId: $("#ddlAllCustomer").val(),
        SalesAutoId: SalesAutoId.toString(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: 1,
        PageSize: 0
    };

    $.ajax({
        type: "POST",
        url: "/Reports/DiscountReportByCustomer.aspx/GetReportDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var ReportList = JSON.parse(response.d);
                    var PrintDate = ReportList[0].orderPaging
                        $("#RptTable").empty();
                        if ($("#txtSFromDate").val() != "") {
                            row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Discount Report By Customer</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + PrintDate[0].PrintDate + "</span><br/><span class='DateRangeCSS' style='float:center;margin-top: 0.5%; font-size: 9px; color: black; font-weight: bold;'>Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val() + "</span><br/></div>"
                        }
                        else {
                            row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Discount Report By Customer</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + PrintDate[0].PrintDate  + "</span></div>"
                        }

                        row1 += "<table id='RptTable' class='PrintMyTableHeader MyTableHeader'>"
                        row1 += "<thead>"
                        row1 += "<tr>"
                    row1 += "<td class='text-center' style='width:5% !Important'>Customer ID</td>"
                        row1 += "<td class='text-left'>Customer Name</td>"
                    row1 += "<td class='text-center'style='width:8% !Important'>Sales Person</td>"
                    row1 += "<td class='text-center' style='width:5% !Important'>Order No</td>"
                    row1 += "<td class='text-center' style='width:6% !Important'>Order Date</td>"
                    row1 += "<td class='text-center' style='width:6% !Important'>Total Amount</td>"
                    row1 += "<td class='text-center' style='width:5% !Important'>Discount(%)</td>"
                    row1 += "<td class='text-center' style='width:7% !Important'>Discount Amount</td>"
                        row1 += "</tr>"
                        row1 += "</thead>"
                    row1 += "<tbody>"
                    var itemTotalAmount = 0.00, OverallDiscAmt=0.00;
                    $.each(ReportList[0].ReportList, function (i, item) {
                        row1 += "<tr>";
                        row1 += "<td class='text-center'>" + item.CustomerId + "</td>";
                        row1 += "<td class='left'>" + item.CustomerName + "</td>";
                        row1 += "<td class='text-center'>" + item.FirstName + "</td>";
                        row1 += "<td style='text-align:center !important'>" + item.OrderNo + "</td>";
                        row1 += "<td style='text-align:center !important'>" + item.OrderDate + "</td>";
                        row1 += "<td style='text-align:right !important'>" + parseFloat(item.TotalAmount).toFixed(2) + "</td>";
                        itemTotalAmount = parseFloat(itemTotalAmount) + parseFloat(item.TotalAmount);
                        OverallDiscAmt = parseFloat(OverallDiscAmt) + parseFloat(item.OverallDiscAmt);
                        row1 += "<td style='text-align:right !important'>" + parseFloat(item.OverallDisc).toFixed(2) + "</td>";
                        row1 += "<td style='text-align:right !important'>" + parseFloat(item.OverallDiscAmt).toFixed(2) + "</td>";
                        row1 += "</tr>";
                    });
                    row1 += "</tbody>"
                    row1 += "<tfoot>"
                    row1 += "<tr>"
                    row1 += "<td colspan='5' style='font-weight:bold' class='text-right'>Total</td>"
                    row1 += "<td class='text-right' style='font-weight:bold'>" + parseFloat(itemTotalAmount).toFixed(2) + "</td>"
                    row1 += "<td class='text-right' style='font-weight:bold'></td>"
                    row1 += "<td style='text-align:right !important;font-weight:bold'>" + parseFloat(OverallDiscAmt).toFixed(2) + "</td>"
                    row1 += "</tr>"
                    row1 += "</tfoot>"
                    row1 += "</table>"
                    
                    if (us == 1) {
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style></style>');
                        mywindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/app-assets/css/custom.css"/></head><body>');
                        mywindow.document.write('</head><body>');
                        mywindow.document.write(row1);
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#ExcelDiv").html(row1);
                        $("#RptTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "discount Report",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });

                    }
                }

            }


        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function Export() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
};
function OpenPopUp() {
    toastr.success('Order Status - Closed.', 'Info', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
}


