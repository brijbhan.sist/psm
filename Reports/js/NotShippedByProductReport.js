﻿$(document).ready(function () {
   
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    bindCategory();
});
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function bindCategory() {
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/WNotShippedByProductReport.asmx/BindStatussalesperson",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
     
        success: function (response) {
            var xmldoc = $.parseXML(response.d);           
            var xmldoc1 = $.parseXML(response.d);
            var category1 = $(xmldoc1).find("Table1");

            $("#ddlSalesPerson option:not(:first)").remove();
            $.each(category1, function () {
                $("#ddlSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("SalesPersonName").text() + "</option>");
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*-------------------------------------------------------Get Not Shipped Report----------------------------------------------------------*/
function getNotShippedReport(PageIndex) {
    var data = {       
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        ProductName: $("#txtProductName").val(),
        SalespersonAutoId: $("#ddlSalesPerson").val(),
        PageSize: $("#ddlPagesize").val(),
        pageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/WNotShippedByProductReport.asmx/getNotShippedReport",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
       
        success: successNotShippedReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successNotShippedReport(response) {
    var xmldoc = $.parseXML(response.d);
    var NotshippedReport = $(xmldoc).find("Table1");
    $("#tblNotShippedReport tbody tr").remove();
    var row = $("#tblNotShippedReport thead tr").clone(true);
    if (NotshippedReport.length > 0) {
        $("#EmptyTable").hide();
        $.each(NotshippedReport, function () {
          
            $(".ProductId", row).text($(this).find("ProductId").text());
            $(".ProductName", row).text($(this).find("ProductName").text());
            $(".Unit", row).text($(this).find("unit").text());
            $(".RequiredQty", row).text($(this).find("RequiredQty").text());
            $(".QtyShip", row).text($(this).find("QtyShip").text());                      
            $(".RemainQty", row).text($(this).find("RemainQty").text());
            $("#tblNotShippedReport tbody").append(row);
            row = $("#tblNotShippedReport tbody tr:last").clone(true);
        });
    } else {
       // $("#EmptyTable").show();
    }

    var pager = $(xmldoc).find("Table");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
}

$("#btnSearch").click(function () {
    getNotShippedReport(1);

})
function Pagevalue(e) {
    getNotShippedReport(parseInt($(e).attr("page")));
};

$("#ddlPagesize").change(function ()
{
    getNotShippedReport(1);
})
/*---------------Export To Excel---------------------------*/
$("#btnExport").click(function () {
   
    var data = {
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        ProductName: $("#txtProductName").val(),
        SalespersonAutoId: $("#ddlSalesPerson").val()       
    };

    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/WNotShippedByProductReport.asmx/getNotShipedReportExport",
        data: JSON.stringify({dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
       
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var ExportNotshippedReport = $(xmldoc).find("Table1");

            $("#tblNotShippedReportExport tbody tr").remove();           
            if (ExportNotshippedReport.length > 0) {
                $("#EmptyTable").hide();
                var row1 = $("#tblNotShippedReportExport thead tr:first-child").clone(true);
                console.log(row1)
                $.each(ExportNotshippedReport, function () {                                      
                    $("td", row1).eq(0).text($(this).find("ProductId").text());
                    $("td", row1).eq(1).text($(this).find("ProductName").text());
                    $("td", row1).eq(2).text($(this).find("unit").text());
                    $("td", row1).eq(3).text($(this).find("RequiredQty").text());
                    $("td", row1).eq(4).text($(this).find("QtyShip").text());
                    $("td", row1).eq(5).text($(this).find("RemainQty").text());
                    $("#tblNotShippedReportExport tbody").append(row1);
                    row1 = $("#tblNotShippedReportExport tbody tr:last-child").clone(true);
                });
            }
            $("#LblDate").text(new Date().toLocaleDateString());
           

            $("#tblNotShippedReportExport").table2excel({

                exclude: ".noExl",
                name: "Excel Document Name",
                filename: "Not Shipped Product Report",
                fileext: ".xls",
                exclude_img: true,
                exclude_links: true,
                exclude_inputs: true
            });
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


});