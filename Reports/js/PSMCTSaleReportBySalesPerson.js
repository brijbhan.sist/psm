﻿var row1 = "";
$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });

    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtFromDate").val(month + '/' + day + '/' + year);
    $("#txtToDate").val(month + '/' + day + '/' + year);
});
function Pagevalue(e) {
    PSMCTSaleReport(parseInt($(e).attr("page")));
};

function PSMCTSaleReport(PageIndex) {
    if ($("#txtFromDate").val() == '' || $("#txtToDate").val() == '') {
        toastr.error('From Date and To Date is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return
    }
    var data = {
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageIndex: PageIndex,
        PageSize: $('#PageSize').val()
    };

    $.ajax({
        type: "POST",
        url: "PSMCTSaleReportBySalesPerson.aspx/SaleReportBySalesPerson",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ReportDetails = $(xmldoc).find('Table1');
                    $("#tblOrderSaleReport tbody tr").remove();
                    var psmct_UNIT_COST = 0.00;
                    var Sold_Default_Qty = 0.00;
                    var CM_Default_Qty = 0.00;
                    var Net_Sold_Default_Qty = 0.00;
                    var psmct_NET_COST = 0.00;
                    var Net_Sale_Rev = 0.00;
                    var Net_CM_Rev = 0.00;
                    var Net_Total_Sales = 0.00;
                    var Profit = 0.00
                    if (ReportDetails.length > 0) {
                        $('#EmptyTable').hide();
                        var html = '';
                        $.each(ReportDetails, function () {
                            debugger;
                            html += '<tr>';
                            html += '<td class="CategoryName text-center tblwth" style="width:70px;white-space:nowrap">' + $(this).find("CategoryName").text() + '</td>';
                            html += '<td class="ProductId text-center">' + $(this).find("ProductId").text() + '</td>';
                            html += '<td class="ProductName tblwth text-left">' + $(this).find("ProductName").text() + '</td>';
                            html += '<td class="UnitType tblwth" style="text-align: center;">' + $(this).find("UnitType").text() + ' </td>';
                            html += '<td class="psmct_UNIT_COST price tblwth" style="text-align: right;">' + parseFloat($(this).find("PSMCT_UNIT_COST").text()).toFixed(2) + '</td>';
                            html += '<td class="Sold_Default_Qty price tblwth" style="text-align: right;">' + parseFloat($(this).find("Sold_Default_Qty").text()).toFixed(2) + '</td>';
                            html += '<td class="CM_Default_Qty price tblwth" style="text-align: right;">' + parseFloat($(this).find("CM_Default_Qty").text()).toFixed(2) + '</td>';
                            html += '<td class="Net_Sold_Default_Qty price tblwth" style="text-align: right;">' + parseFloat($(this).find("Net_Sold_Default_Qty").text()).toFixed(2) + '</td>';
                            html += '<td class="psmct_NET_COST tblwth" style="text-align: right;">' + parseFloat($(this).find("PSMCT_NET_COST").text()).toFixed(2) + '</td>';
                            html += '<td class="Net_Sale_Rev tblwth" style="text-align: right;">' + parseFloat($(this).find("Net_Sale_Rev").text()).toFixed(2) + '</td>';
                            html += '<td class="Net_CM_Rev tblwth" style="text-align: right;">' + parseFloat($(this).find("Net_CM_Rev").text()).toFixed(2) + '</td>';
                            html += '<td class="Net_Total_Sales tblwth" style="text-align: right;">' + parseFloat($(this).find("Net_Total_Sales").text()).toFixed(2) + '</td>';
                            html += '<td class="Profit tblwth" style="text-align: right;">' + parseFloat($(this).find("Profit").text()).toFixed(2) + '</td>';
                            html += '</tr>';

                            psmct_UNIT_COST += parseFloat($(this).find("PSMCT_UNIT_COST").text());
                            Sold_Default_Qty += parseFloat($(this).find("Sold_Default_Qty").text());
                            CM_Default_Qty += parseFloat($(this).find("CM_Default_Qty").text());
                            Net_Sold_Default_Qty += parseFloat($(this).find("Net_Sold_Default_Qty").text());
                            psmct_NET_COST += parseFloat($(this).find("PSMCT_NET_COST").text());
                            Net_Sale_Rev += parseFloat($(this).find("Net_Sale_Rev").text());
                            Net_CM_Rev += parseFloat($(this).find("Net_CM_Rev").text());
                            Net_Total_Sales += parseFloat($(this).find("Net_Total_Sales").text());
                            Profit += parseFloat($(this).find("Profit").text());

                        });
                        $('#tblOrderSaleReport tbody').html(html);

                        $("#TPSMCT_UNIT_COST").html(parseFloat(psmct_UNIT_COST).toFixed(2))
                        $("#TSold_Default_Qty").html(parseFloat(Sold_Default_Qty).toFixed(2))
                        $("#TCM_Default_Qty").html(parseFloat(CM_Default_Qty).toFixed(2))
                        $("#TNet_Sold_Default_Qty").html(parseFloat(Net_Sold_Default_Qty).toFixed(2))
                        $("#Tpsmct_NET_COST").html(parseFloat(psmct_NET_COST).toFixed(2))
                        $("#TNet_Sale_Rev").html(parseFloat(Net_Sale_Rev).toFixed(2))
                        $("#TNet_CM_Rev").html(parseFloat(Net_CM_Rev).toFixed(2))
                        $("#TNet_Total_Sales").html(parseFloat(Net_Total_Sales).toFixed(2))
                        $("#TProfit").html(parseFloat(Profit).toFixed(2))


                    }
                    else {
                        $("#Tpsmct_UNIT_COST").html('0.00')
                        $("#TSold_Default_Qty").html('0.00')
                        $("#TCM_Default_Qty").html('0.00')
                        $("#TNet_Sold_Default_Qty").html('0.00')
                        $("#Tpsmct_NET_COST").html('0.00')
                        $("#TNet_Sale_Rev").html('0.00')
                        $("#TNet_CM_Rev").html('0.00')
                        $("#TNet_Total_Sales").html('0.00')
                        $("#TProfit").html('0.00')


                        $("#OTpsmct_UNIT_COST").html('0.00');
                        $("#OTSold_Default_Qty").html('0.00');
                        $("#OTCM_Default_Qty").html('0.00');
                        $("#OTNet_Sold_Default_Qty").html('0.00');
                        $("#OTpsmct_NET_COST").html('0.00');
                        $("#OTNet_Sale_Rev").html('0.00');
                        $("#OTNet_CM_Rev").html('0.00');
                        $("#OTNet_Total_Sales").html('0.00');
                        $("#OTProfit").html('0.00')
                    }
                    var overalldetails = $(xmldoc).find("Table2");
                    $("#OTpsmct_UNIT_COST").html(parseFloat($(overalldetails).find('PSMCT_UNIT_COST').text()).toFixed(2));
                    $("#OTSold_Default_Qty").html(parseFloat($(overalldetails).find('Sold_Default_Qty').text()).toFixed(2));
                    $("#OTCM_Default_Qty").html(parseFloat($(overalldetails).find('CM_Default_Qty').text()).toFixed(2));
                    $("#OTNet_Sold_Default_Qty").html(parseFloat($(overalldetails).find('Net_Sold_Default_Qty').text()).toFixed(2));
                    $("#OTpsmct_NET_COST").html(parseFloat($(overalldetails).find('PSMCT_NET_COST').text()).toFixed(2));
                    $("#OTNet_Sale_Rev").html(parseFloat($(overalldetails).find('Net_Sale_Rev').text()).toFixed(2));
                    $("#OTNet_CM_Rev").html(parseFloat($(overalldetails).find('Net_CM_Rev').text()).toFixed(2));
                    $("#OTNet_Total_Sales").html(parseFloat($(overalldetails).find('Net_Total_Sales').text()).toFixed(2));
                    $("#OTProfit").html(parseFloat($(overalldetails).find('Profit').text()).toFixed(2));
                    var pager = $(xmldoc).find("Table");
                    $(".Pager").ASPSnippets_Pager({
                        ActiveCssClass: "current",
                        PagerCssClass: "pager",
                        PageIndex: parseInt(pager.find("PageIndex").text()),
                        PageSize: parseInt(pager.find("PageSize").text()),
                        RecordCount: parseInt(pager.find("RecordCount").text())
                    });


                }
            }
            else {
                location.href = "/";
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
    });
}

function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        FromDate: $("#txtFromDate").val(),
        ToDate: $("#txtToDate").val(),
        PageIndex: 1,
        PageSize: 0
    };


    $.ajax({
        type: "POST",
        url: "PSMCTSaleReportBySalesPerson.aspx/SaleReportBySalesPerson",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var OrderTotal = $(xmldoc).find("Table1");
                    var overalldetails = $(xmldoc).find("Table2");

                    if ($("#txtFromDate").val() != "") {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>PSMCT Product Sold By Sales Person</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + (new Date()).format("MM/dd/yyyy hh:mm tt") + "</span><br/><span class='DateRangeCSS' style='float:center;margin-top: 0.5%; font-size: 9px; color: black; font-weight: bold;'>Date Range: " + $("#txtFromDate").val() + " To " + $("#txtToDate").val() + "</span><br/></div>"
                    }
                    else {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>PSMCT Product Sold By Sales Person</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + (new Date()).format("MM/dd/yyyy hh:mm tt") + "</span></div>"
                    }
                    row1 += "<table class='PrintMyTableHeader MyTableHeader' id='RptTable'>"
                    row1 += "<thead  class='bg-blue white'>"
                    row1 += "<tr>"
                    if (us == 1) {
                        row1 += '<td class="CategoryName text-center" style="width:70px;white-space:nowrap"> Category</td >';
                        row1 += '<td class="ProductId left"  style="width:72px;white-space:nowrap">Product Id</td>';
                        row1 += '<td class="ProductName;" style="text-align:left;">Product Name</td>';
                        row1 += '<td class="UnitType"  style="text-align: center;width:50px;white-space:nowrap">Unit </td>';
                        row1 += '<td class="psmct_UNIT_COST price"  style="text-align: right;width:75px;white-space:nowrap">PSMCT <br/>Unit Cost</td>';
                        row1 += '<td class="Sold_Default_Qty price" style="text-align: right;width:75px;white-space:nowrap">Sold <br> Default Qty</td>';
                        row1 += '<td class="CM_Default_Qty price" style="text-align: right;width:85px;white-space: nowrap">Credit Memo <br>Default Qty</td>';
                        row1 += '<td class="Net_Sold_Default_Qty price" style="text-align: right;width:72px;white-space: nowrap">Net Sold<br> Default Qty</td>';
                        row1 += '<td class="psmct_NET_COST" style="text-align: right;width:10px;white-space: nowrap">PSMCT<br> Net Cost</td>';
                        row1 += '<td class="Net_Sale_Rev" style="text-align: right;width:10px;white-space: nowrap">Net Sale<br> Revenue</td>';
                        row1 += '<td class="Net_CM_Rev" style="text-align: right;width:83px;white-space: nowrap">Credit Memo<br />Net Revenue</td>';
                        row1 += '<td class="Net_Total_Sales" style="text-align: right;width:10px;white-space: nowrap">Net <br>Total Sale</td>';
                        row1 += '<td class="Profit" style="text-align: right;width:10px;white-space: nowrap">Profit</td>';
                    } else {
                        row1 += '<td class="CategoryName text-center" style="font-weight: bold;width:70px;white-space:nowrap">Category</td >';
                        row1 += '<td class="ProductId left"  style="font-weight: bold;width:72px;white-space:nowrap">Product Id</td>';
                        row1 += '<td class="ProductName;" style="font-weight: bold;text-align:left;">Product Name</td>';
                        row1 += '<td class="UnitType"  style="font-weight: bold;text-align: center;width:50px;white-space:nowrap">Unit </td>';
                        row1 += '<td class="psmct_UNIT_COST price"  style="font-weight: bold;text-align: right;width:75px;white-space:nowrap">PSMCT Unit Cost</td>';
                        row1 += '<td class="Sold_Default_Qty price" style="font-weight: bold;text-align: right;width:75px;white-space:nowrap">Sold Default Qty</td>';
                        row1 += '<td class="CM_Default_Qty price" style="font-weight: bold;text-align: right;width:10px;white-space: nowrap">Credit Memo Default Qty</td>';
                        row1 += '<td class="Net_Sold_Default_Qty price" style="font-weight: bold;text-align: right;width:10px;white-space: nowrap">Net Sold Default Qty</td>';
                        row1 += '<td class="psmct_NET_COST" style="font-weight: bold;text-align: right;width:10px;white-space: nowrap">PSMCT Net Cost</td>';
                        row1 += '<td class="Net_Sale_Rev" style="font-weight: bold;text-align: right;width:10px;white-space: nowrap">Net Sale Revenue</td>';
                        row1 += '<td class="Net_CM_Rev" style="font-weight: bold;text-align: right;width:10px;white-space: nowrap">Credit Memo Net Revenue</td>';
                        row1 += '<td class="Net_Total_Sales" style="font-weight: bold;text-align: right;width:10px;white-space: nowrap">Net Total Sale</td>';
                        row1 += '<td class="Profit" style="font-weight: bold;text-align: right;width:10px;white-space: nowrap">Profit</td>';
                    }
                    row1 += "</tr>"
                    row1 += "</thead>"
                    row1 += "<tbody>"
                    if (OrderTotal.length > 0) {
                        var i = 1;
                        $.each(OrderTotal, function (index) {
                            row1 += '<tr>';
                            row1 += '<td class="CategoryName text-center" style="width:70px;white-space:nowrap">' + $(this).find("CategoryName").text() + '</td>';
                            row1 += '<td class="ProductId text-center" style="width:10px;white-space: nowrap">' + $(this).find("ProductId").text() + '</td>';
                            row1 += '<td class="ProductName text-left">' + $(this).find("ProductName").text() + '</td>';
                            row1 += '<td class="UnitType" style="text-align: center;width:10px">' + $(this).find("UnitType").text() + ' </td>';
                            row1 += '<td class="psmct_UNIT_COST price" style="text-align: right;width:10px">' + parseFloat($(this).find("PSMCT_UNIT_COST").text()).toFixed(2) + '</td>';
                            row1 += '<td class="Sold_Default_Qty price" style="text-align: right;width:10px;white-space: nowrap">' + parseFloat($(this).find("Sold_Default_Qty").text()).toFixed(2) + '</td>';
                            row1 += '<td class="CM_Default_Qty price" style="text-align: right;width:10px;white-space: nowrap">' + parseFloat($(this).find("CM_Default_Qty").text()).toFixed(2) + '</td>';
                            row1 += '<td class="Net_Sold_Default_Qty price" style="text-align: right;width:10px;white-space: nowrap">' + parseFloat($(this).find("Net_Sold_Default_Qty").text()).toFixed(2) + '</td>';
                            row1 += '<td class="psmct_NET_COST" style="text-align: right;width:10px;white-space: nowrap">' + parseFloat($(this).find("PSMCT_NET_COST").text()).toFixed(2) + '</td>';
                            row1 += '<td class="Net_Sale_Rev" style="text-align: right;width:10px;white-space: nowrap">' + parseFloat($(this).find("Net_Sale_Rev").text()).toFixed(2) + '</td>';
                            row1 += '<td class="Net_CM_Rev" style="text-align: right;width:10px;white-space: nowrap">' + parseFloat($(this).find("Net_CM_Rev").text()).toFixed(2) + '</td>';
                            row1 += '<td class="Net_Total_Sales" style="text-align: right;width:10px;white-space: nowrap">' + parseFloat($(this).find("Net_Total_Sales").text()).toFixed(2) + '</td>';
                            row1 += '<td class="Profit" style="text-align: right;width:10px;white-space: nowrap">' + parseFloat($(this).find("Profit").text()).toFixed(2) + '</td>';
                            row1 += '</tr>';

                        });
                        row1 += "</tbody>"
                        row1 += "<tfoot>"
                        row1 += '<tr style="font-weight: bold;">';
                        row1 += '<td colspan="5" class="right">Total</td>';
                        row1 += '<td class="right" style="width:10px;font-weight: bold;white-space: nowrap">' + parseFloat($(overalldetails).find('Sold_Default_Qty').text()).toFixed(2);
                        row1 += '<td class="right" style="width:10px;font-weight: bold;white-space: nowrap">' + parseFloat($(overalldetails).find('CM_Default_Qty').text()).toFixed(2);
                        row1 += '<td class="right" style="width:10px;font-weight: bold;white-space: nowrap">' + parseFloat($(overalldetails).find('Net_Sold_Default_Qty').text()).toFixed(2);
                        row1 += '<td class="right" style="width:10px;font-weight: bold;white-space: nowrap">' + parseFloat($(overalldetails).find('PSMCT_NET_COST').text()).toFixed(2);
                        row1 += '<td class="right" style="width:10px;font-weight: bold;white-space: nowrap">' + parseFloat($(overalldetails).find('Net_Sale_Rev').text()).toFixed(2);
                        row1 += '<td class="right" style="width:10px;font-weight: bold;white-space: nowrap">' + parseFloat($(overalldetails).find('Net_CM_Rev').text()).toFixed(2);
                        row1 += '<td class="right" style="width:10px;font-weight: bold;white-space: nowrap">' + parseFloat($(overalldetails).find('Net_Total_Sales').text()).toFixed(2);
                        row1 += '<td class="right" style="width:10px;font-weight: bold;white-space: nowrap">' + parseFloat($(overalldetails).find('Profit').text()).toFixed(2);
                        row1 += "</tfoot>"
                        row1 += "</table>"
                        if (us == 1) {
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style>#RptTable {border-collapse: collapse;width: 100%;}#RptTable td, #RptTable th {border: 1px solid black;}#RptTable tr:nth-child(even){background-color: #f2f2f2;}#RptTable thead {padding-top: 12px;padding-bottom: 12px;text-align:center;background-color:#e9e8e8 !important;color:black;}.text-right{text-align:right;}.text-left{text-align:left;}.text-center{text-align:center;}</style>');
                            mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                            mywindow.document.write('</head><body>');
                            mywindow.document.write(row1);
                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        }
                        if (us == 2) {
                            $("#ExcelDiv").html(row1);
                            $("#RptTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "PSMCT_Product_Sold by Sales person",
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });

                        }
                    }

                }
                else {
                    location.href = "/";
                }
            }

        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function(result) {
        console.log(JSON.parse(result.responseText).d);
    }
    });

}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderSaleReport tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderSaleReport tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
