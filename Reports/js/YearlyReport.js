﻿$(document).ready(function () {
    BindCustomer();
});
function Pagevalue(e) {
    YearlyReport(parseInt($(e).attr("page")));
} 
function BindCustomer() {
    $.ajax({
        type: "POST",
        url: "/Reports/YearlyReport.aspx/BindCustomer",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger;
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#dllCustomer option:not(:first)").remove();
                $.each(getData[0].CustomerList, function (index, item) {
                    $("#dllCustomer").append("<option value='" + item.AutoId + "'>" + item.CustomerName + "</option>");
                });
                $("#dllCustomer").select2();

                $("#dllSelectYear option:not(:first)").remove();
                var diff = 0;
                var d = new Date();
                var c = d.getFullYear();
                var ld = Number(getData[0].YearList);
                diff = Number(c) - ld;
                var n = d.getFullYear() + Number(diff);
                $("#dllSelectYear").prepend("<option value='0'>All</option>");
                if (ld == n) {
                    $("#dllSelectYear").append("<option value='" + ld + "'>" + ld + "</option>");
                }
                else {
                    for (var i = ld; i < n; i++) {
                        $("#dllSelectYear").append("<option value='" + i + "'>" + i + "</option>");
                    }
                }
                    $("#dllSelectYear").val(c);
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
function YearlyReport(PageIndex) {
    debugger;
    var data = {
        SelectYear: $("#dllSelectYear").val(),
        PageSize: $("#ddlPageSize").val(),
        Customer: $("#dllCustomer").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "/Reports/YearlyReport.aspx/GetYearlyReport1",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: SuccessCustomerCreditReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function SuccessCustomerCreditReport(response) {
    if (response.d == 'false') {
        toastr.error('Oops, Something went wrong.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else {
        var xmldoc = $.parseXML(response.d);
        var Report = $(xmldoc).find("Table2");
        $("#tblYearlyReport tbody tr").remove();
        var row = $("#tblYearlyReport thead tr:last-child").clone(true);
        if (Report.length > 0) {

            var JanOrd = 0
                , JanTotal = 0
                , FebOrd = 0
                , FebTotal = 0
                , MarOrd = 0
                , MarTotal = 0
                , AprOrd = 0
                , AprTotal = 0
                , MayOrd = 0
                , MayTotal = 0
                , JunOrd = 0
                , JunTotal = 0
                , JulOrd = 0
                , JulTotal = 0
                , AugOrd = 0
                , AugTotal = 0
                , SepOrd = 0
                , SepTotal = 0
                , OctOrd = 0
                , OctTotal = 0
                , NovOrd = 0
                , NovTotal = 0
                , DecOrd = 0
                , DecTotal = 0
                , YearOrders = 0
                , YearTotal = 0;
            $("#EmptyTable").hide();
            $.each(Report, function () {

                JanOrd = JanOrd + (parseFloat($(this).find("JanOrd").text()));
                JanTotal = JanTotal + (parseFloat($(this).find("JanTotal").text()));
                FebOrd = FebOrd + (parseFloat($(this).find("FebOrd").text()));
                FebTotal = FebTotal + (parseFloat($(this).find("FebTotal").text()));
                MarOrd = MarOrd + (parseFloat($(this).find("MarOrd").text()));
                MarTotal = MarTotal + (parseFloat($(this).find("MarTotal").text()));
                AprOrd = AprOrd + (parseFloat($(this).find("AprOrd").text()));
                AprTotal = AprTotal + (parseFloat($(this).find("AprTotal").text()));
                MayOrd = MayOrd + (parseFloat($(this).find("MayOrd").text()));
                MayTotal = MayTotal + (parseFloat($(this).find("MayTotal").text()));
                JunOrd = JunOrd + (parseFloat($(this).find("JunOrd").text()));
                JunTotal = JunTotal + (parseFloat($(this).find("JunTotal").text()));
                JulOrd = JulOrd + (parseFloat($(this).find("JulOrd").text()));
                JulTotal = JulTotal + (parseFloat($(this).find("JulTotal").text()));
                AugOrd = AugOrd + (parseFloat($(this).find("AugOrd").text()));
                AugTotal = AugTotal + (parseFloat($(this).find("AugTotal").text()));
                SepOrd = SepOrd + (parseFloat($(this).find("SepOrd").text()));
                SepTotal = SepTotal + (parseFloat($(this).find("SepTotal").text()));
                OctOrd = OctOrd + (parseFloat($(this).find("OctOrd").text()));
                OctTotal = OctTotal + (parseFloat($(this).find("OctTotal").text()));
                NovOrd = NovOrd + (parseFloat($(this).find("NovOrd").text()));
                NovTotal = NovTotal + (parseFloat($(this).find("NovTotal").text()));
                DecOrd = DecOrd + (parseFloat($(this).find("DecOrd").text()));
                DecTotal = DecTotal + (parseFloat($(this).find("DecTotal").text()));
                YearOrders = YearOrders + (parseFloat($(this).find("YearOrders").text()));
                YearTotal = YearTotal + (parseFloat($(this).find("YearTotal").text()));
                $(".CustomerName", row).text($(this).find("CustomerName").text());
                $(".SalesRep", row).text($(this).find("SalesRep").text());
                $(".JanOrd", row).text($(this).find("JanOrd").text());
                $(".JanTot", row).text($(this).find("JanTotal").text());
                $(".FebOrd", row).text($(this).find("FebOrd").text());
                $(".FebTot", row).text($(this).find("FebTotal").text());
                $(".MarOrd", row).text($(this).find("MarOrd").text());
                $(".MarTot", row).text($(this).find("MarTotal").text());
                $(".AprOrd", row).text($(this).find("AprOrd").text());
                $(".AprTot", row).text($(this).find("AprTotal").text());
                $(".MayOrd", row).text($(this).find("MayOrd").text());
                $(".MayTot", row).text($(this).find("MayTotal").text());
                $(".JunOrd", row).text($(this).find("JunOrd").text());
                $(".JunTot", row).text($(this).find("JunTotal").text());
                $(".JulOrd", row).text($(this).find("JulOrd").text());
                $(".JulTot", row).text($(this).find("JulTotal").text());
                $(".AugOrd", row).text($(this).find("AugOrd").text());
                $(".AugTot", row).text($(this).find("AugTotal").text());
                $(".SepOrd", row).text($(this).find("SepOrd").text());
                $(".SepTot", row).text($(this).find("SepTotal").text());
                $(".OctOrd", row).text($(this).find("OctOrd").text());
                $(".OctTot", row).text($(this).find("OctTotal").text());
                $(".NovOrd", row).text($(this).find("NovOrd").text());
                $(".NovTot", row).text($(this).find("NovTotal").text());
                $(".DecOrd", row).text($(this).find("DecOrd").text());
                $(".DecTot", row).text($(this).find("DecTotal").text());
                $(".YearlyOrd", row).text($(this).find("YearOrders").text());
                $(".YearlyTot", row).text($(this).find("YearTotal").text());  
                $("#tblYearlyReport tbody").append(row);
                row = $("#tblYearlyReport tbody tr:last-child").clone(true);
            });
            $("#tJanOrd").html(parseFloat(JanOrd));
            $("#tJanTot").html(parseFloat(JanTotal).toFixed(2));
            $("#tFebOrd").html(parseFloat(FebOrd));
            $("#tFebTot").html(parseFloat(FebTotal).toFixed(2));
            $("#tMarOrd").html(parseFloat(MarOrd));
            $("#tMarTot").html(parseFloat(MarTotal).toFixed(2));
            $("#tAprOrd").html(parseFloat(AprOrd));
            $("#tAprTot").html(parseFloat(AprTotal).toFixed(2));
            $("#tMayOrd").html(parseFloat(MayOrd));
            $("#tMayTot").html(parseFloat(MayTotal).toFixed(2));
            $("#tJunOrd").html(parseFloat(JunOrd));
            $("#tJunTot").html(parseFloat(JunTotal).toFixed(2));
            $("#tJulOrd").html(parseFloat(JulOrd));
            $("#tJulTot").html(parseFloat(JulTotal).toFixed(2));
            $("#tAugOrd").html(parseFloat(AugOrd));
            $("#tAugTot").html(parseFloat(AugTotal).toFixed(2));
            $("#tSepOrd").html(parseFloat(SepOrd));
            $("#tSepTot").html(parseFloat(SepTotal).toFixed(2));
            $("#tOctOrd").html(parseFloat(OctOrd));
            $("#tOctTot").html(parseFloat(OctTotal).toFixed(2));
            $("#tNovOrd").html(parseFloat(NovOrd));
            $("#tNovTot").html(parseFloat(NovTotal).toFixed(2));
            $("#tDecOrd").html(parseFloat(DecOrd));
            $("#tDecTot").html(parseFloat(DecTotal).toFixed(2));
            $("#tYearlyOrd").html(parseFloat(YearOrders));
            $("#tYearlyTot").html(parseFloat(YearTotal).toFixed(2)); 
            $("#tblYearlyReport tbody tr").show();
            var OrdTotal = $(xmldoc).find("Table");
            if (OrdTotal.length > 0) {
                $(OrdTotal).each(function () {
                    $("#otJanOrd").html(parseFloat($(this).find("JanOrd").text()));
                    $("#otJanTot").html(parseFloat($(this).find("JanTotal").text()).toFixed(2));
                    $("#otFebOrd").html(parseFloat($(this).find("FebOrd").text()));
                    $("#otFebTot").html(parseFloat($(this).find("FebTotal").text()).toFixed(2));
                    $("#otMarOrd").html(parseFloat($(this).find("MarOrd").text()));
                    $("#otMarTot").html(parseFloat($(this).find("MarTotal").text()).toFixed(2));
                    $("#otAprOrd").html(parseFloat($(this).find("AprOrd").text()));
                    $("#otAprTot").html(parseFloat($(this).find("AprTotal").text()).toFixed(2));
                    $("#otMayOrd").html(parseFloat($(this).find("MayOrd").text()));
                    $("#otMayTot").html(parseFloat($(this).find("MayTotal").text()).toFixed(2));
                    $("#otJunOrd").html(parseFloat($(this).find("JunOrd").text()));
                    $("#otJunTot").html(parseFloat($(this).find("JunTotal").text()).toFixed(2));
                    $("#otJulOrd").html(parseFloat($(this).find("JulOrd").text()));
                    $("#otJulTot").html(parseFloat($(this).find("JulTotal").text()).toFixed(2));
                    $("#otAugOrd").html(parseFloat($(this).find("AugOrd").text()));
                    $("#otAugTot").html(parseFloat($(this).find("AugTotal").text()).toFixed(2));
                    $("#otSepOrd").html(parseFloat($(this).find("SepOrd").text()));
                    $("#otSepTot").html(parseFloat($(this).find("SepTotal").text()).toFixed(2));
                    $("#otOctOrd").html(parseFloat($(this).find("OctOrd").text()));
                    $("#otOctTot").html(parseFloat($(this).find("OctTotal").text()).toFixed(2));
                    $("#otNovOrd").html(parseFloat($(this).find("NovOrd").text()));
                    $("#otNovTot").html(parseFloat($(this).find("NovTotal").text()).toFixed(2));
                    $("#otDecOrd").html(parseFloat($(this).find("DecOrd").text()));
                    $("#otDecTot").html(parseFloat($(this).find("DecTotal").text()).toFixed(2));
                    $("#otYearlyOrd").html(parseFloat($(this).find("YearOrders").text()));
                    $("#otYearlyTot").html(parseFloat($(this).find("YearTotal").text()).toFixed(2));
                });
            }
        }
        else {
            $("#EmptyTable").show();

            $("#tJanOrd").html('0.00');
            $("#tJanTot").html('0.00');
            $("#tFebOrd").html('0.00');
            $("#tFebTot").html('0.00');
            $("#tMarOrd").html('0.00');
            $("#tMarTot").html('0.00');
            $("#tAprOrd").html('0.00');
            $("#tAprTot").html('0.00');
            $("#tMayOrd").html('0.00');
            $("#tMayTot").html('0.00');
            $("#tJunOrd").html('0.00');
            $("#tJunTot").html('0.00');
            $("#tJulOrd").html('0.00');
            $("#tJulTot").html('0.00');
            $("#tAugOrd").html('0.00');
            $("#tAugTot").html('0.00');
            $("#tSepOrd").html('0.00');
            $("#tSepTot").html('0.00');
            $("#tOctOrd").html('0.00');
            $("#tOctTot").html('0.00');
            $("#tNovOrd").html('0.00');
            $("#tNovTot").html('0.00');
            $("#tDecOrd").html('0.00');
            $("#tDecTot").html('0.00');
            $("#tYearlyOrd").html('0.00');
            $("#tYearlyTot").html('0.00');
            //--------------------
            $("#otJanOrd").html('0.00');
            $("#otJanTot").html('0.00');
            $("#otFebOrd").html('0.00');
            $("#otFebTot").html('0.00');
            $("#otMarOrd").html('0.00');
            $("#otMarTot").html('0.00');
            $("#otAprOrd").html('0.00');
            $("#otAprTot").html('0.00');
            $("#otMayOrd").html('0.00');
            $("#otMayTot").html('0.00');
            $("#otJunOrd").html('0.00');
            $("#otJunTot").html('0.00');
            $("#otJulOrd").html('0.00');
            $("#otJulTot").html('0.00');
            $("#otAugOrd").html('0.00');
            $("#otAugTot").html('0.00');
            $("#otSepOrd").html('0.00');
            $("#otSepTot").html('0.00');
            $("#otOctOrd").html('0.00');
            $("#otOctTot").html('0.00');
            $("#otNovOrd").html('0.00');
            $("#otNovTot").html('0.00');
            $("#otDecOrd").html('0.00');
            $("#otDecTot").html('0.00');
            $("#otYearlyOrd").html('0.00');
            $("#otYearlyTot").html('0.00');

        }

        var pager = $(xmldoc).find("Table1");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
    }
}

function PrintOrder(date) {
    window.open("/Reports/PaymentLog.html?dt=" + date, "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}

function CreateTable(us) {
    debugger
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        SelectYear: $("#dllSelectYear").val(),
        PageSize: 0,
        Customer: $("#dllCustomer").val(),
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "/Reports/YearlyReport.aspx/GetYearlyReport1",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',

                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {

                    var JanOrd = 0
                        , JanTotal = 0
                        , FebOrd = 0
                        , FebTotal = 0
                        , MarOrd = 0
                        , MarTotal = 0
                        , AprOrd = 0
                        , AprTotal = 0
                        , MayOrd = 0
                        , MayTotal = 0
                        , JunOrd = 0
                        , JunTotal = 0
                        , JulOrd = 0
                        , JulTotal = 0
                        , AugOrd = 0
                        , AugTotal = 0
                        , SepOrd = 0
                        , SepTotal = 0
                        , OctOrd = 0
                        , OctTotal = 0
                        , NovOrd = 0
                        , NovTotal = 0
                        , DecOrd = 0
                        , DecTotal = 0
                        , YearOrders = 0
                        , YearTotal = 0;
                    var xmldoc = $.parseXML(response.d);
                    var PrintDate = $(xmldoc).find("Table1");
                    var Report = $(xmldoc).find("Table2");
                    var row1 = "";
                    var Year = '';
                    debugger
                    if ($("#dllSelectYear").val() == '0') {
                        Year = 'All';
                    }
                    else {
                        Year = $("#dllSelectYear").val();
                    }

                    if ($("#txtFromDate").val() != "") {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Yearly Report</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + (PrintDate.find('PrintDate').text()) + "</span><br/><span class='DateRangeCSS' style='float:center;margin-top: 0.5%; font-size: 9px; color: black; font-weight: bold;'>Year: " + Year + "</span><br/></div>"
                    }
                    else {
                        row1 += "<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Yearly Report</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + (PrintDate.find('PrintDate').text()) + "</span></div>"
                    }

                    row1 += ' <table class="MyTableHeader PrintMyTableHeader" id="RptTable">'
                    row1 += '  <thead class="bg-blue white">'
                    row1 += '  <tr>'
                    row1 += '   <td class="P_CustomerName text-center width13per" rowspan="2" style="white-space:nowrap">Customer Name</td>'
                    row1 += '  <td class="P_SalesRep text-center width2.5per" rowspan="2" >Sales Person</td>'
                    row1 += '<td class="P_JanOrd text-center width2.5per" colspan="2">January</td>'
                    row1 += '  <td class="P_FebOrd text-center width2.5per" colspan="2">February</td>'
                    row1 += ' <td class="P_MarOrd text-center width2.5per" colspan="2">March</td>'
                    row1 += '  <td class="P_AprOrd text-center width2.5per" colspan="2">April</td>'
                    row1 += '<td class="P_MayOrd text-center width2.5per" colspan="2">May</td>'
                    row1 += '   <td class="P_JunOrd text-center width2.5per" colspan="2">June</td>'
                    row1 += ' <td class="P_JulOrd text-center width2.5per" colspan="2">July</td>'
                    row1 += ' <td class="P_AugOrd text-center width2.5per" colspan="2">August</td>'
                    row1 += ' <td class="P_SepOrd text-center width2.5per" colspan="2">September</td>'
                    row1 += ' <td class="P_OctOrd text-center width2.5per" colspan="2">October</td>'
                    row1 += ' <td class="P_NovOrd text-center width2.5per" colspan="2">November</td>'
                    row1 += '  <td class="P_DecOrd text-center width2.5per" colspan="2">December</td>'
                    row1 += ' <td class="P_YearlyOrd text-center width2.5per" colspan="2" >Year</td>'
                    row1 += '  </tr>'
                    row1 += ' <tr>'
                    row1 += '    <td class="P_JanOrd text-center width2.5per">Order</td>'
                    row1 += '  <td class="P_JanTot text-center width2.5per">Total</td>'
                    row1 += '   <td class="P_FebOrd text-center width2.5per">Order</td>'
                    row1 += '  <td class="P_FebTot text-center width2.5per">Total</td>'
                    row1 += '   <td class="P_MarOrd text-center width2.5per">Order</td>'
                    row1 += '  <td class="P_MarTot text-center width2.5per">Total </td>'
                    row1 += ' <td class="P_AprOrd text-center width2.5per">Order</td>'
                    row1 += '  <td class="P_AprTot text-center width2.5per">Total</td>'
                    row1 += ' <td class="P_MayOrd text-center width2.5per">Order</td>'
                    row1 += ' <td class="P_MayTot text-center width2.5per">Total</td>'
                    row1 += '  <td class="P_JunOrd text-center width2.5per">Order</td>'
                    row1 += ' <td class="P_JunTot text-center width2.5per">Total</td>'
                    row1 += ' <td class="P_JulOrd text-center width2.5per">Order</td>'
                    row1 += ' <td class="P_JulTot text-center width2.5per">Total</td>'
                    row1 += '  <td class="P_AugOrd text-center width2.5per">Order</td>'
                    row1 += '  <td class="P_AugTot text-center width2.5per">Total</td>'
                    row1 += ' <td class="P_SepOrd text-center width2.5per">Order</td>'
                    row1 += '  <td class="P_SepTot text-center width2.5per">Total</td>'
                    row1 += ' <td class="P_OctOrd text-center width2.5per">Order</td>'
                    row1 += ' <td class="P_OctTot text-center width2.5per">Total</td>'
                    row1 += ' <td class="P_NovOrd text-center width2.5per">Order</td>'
                    row1 += '  <td class="P_NovTot text-center width2.5per">Total</td>'
                    row1 += ' <td class="P_DecOrd text-center width2.5per">Order</td>'
                    row1 += '  <td class="P_DecTot text-center width2.5per">Total</td>'
                    row1 += ' <td class="P_YearlyOrd text-center width2.5per">Order</td>'
                    row1 += '<td class="P_YearlyTot text-center width2.5per">Total</td>'
                    row1 += ' </tr>'
                    if (us == 1) {
                        row1 += ' <tr style="display:none">'
                        row1 += '     <td class="P_CustomerName text-center width13per" style="white-space:nowrap">Customer Name</td>'
                        row1 += '   <td class="P_SalesRep text-center width2.5per">Sales Rep</td>'
                        row1 += '  <td class="P_JanOrd text-center width2.5per">January Order</td>'
                        row1 += ' <td class="P_JanTot text-center width2.5per">January Total</td>'
                        row1 += '   <td class="P_FebOrd text-center width2.5per">February Order</td>'
                        row1 += ' <td class="P_FebTot text-center width2.5per">February Total</td>'
                        row1 += '  <td class="P_MarOrd text-center width2.5per">March Order</td>'
                        row1 += ' <td class="P_MarTot text-center width2.5per">March Total </td>'
                        row1 += '    <td class="P_AprOrd text-center width2.5per">April Order</td>'
                        row1 += '    <td class="P_AprTot text-center width2.5per">April Total</td>'
                        row1 += '   <td class="P_MayOrd text-center width2.5per">May Order</td>'
                        row1 += '   <td class="P_MayTot text-center width2.5per">May Total</td>'
                        row1 += '   <td class="P_JunOrd text-center width2.5per">June Order</td>'
                        row1 += '   <td class="P_JunTot text-center width2.5per">June Total</td>'
                        row1 += '    <td class="P_JulOrd text-center width2.5per">July Order</td>'
                        row1 += '   <td class="P_JulTot text-center width2.5per">July Total</td>'
                        row1 += '  <td class="P_AugOrd text-center width2.5per">August Order</td>'
                        row1 += '   <td class="P_AugTot text-center width2.5per">August Total</td>'
                        row1 += '  <td class="P_SepOrd text-center width2.5per">September Order</td>'
                        row1 += '  <td class="P_SepTot text-center width2.5per">September Total</td>'
                        row1 += '  <td class="P_OctOrd text-center width2.5per">October Order</td>'
                        row1 += '   <td class="P_OctTot text-center width2.5per">October Total</td>'
                        row1 += ' <td class="P_NovOrd text-center width2.5per">November Order</td>'
                        row1 += ' <td class="P_NovTot text-center width2.5per">November Total</td>'
                        row1 += ' <td class="P_DecOrd text-center width2.5per">December Order</td>'
                        row1 += ' <td class="P_DecTot text-center width2.5per">December Total</td>'
                        row1 += '  <td class="P_YearlyOrd text-center width2.5per">Yearly Order</td>'
                        row1 += ' <td class="P_YearlyTot text-center width2.5per">Yearly Total</td>'
                        row1 += '  </tr>'
                    }
                    row1 += "</thead>"
                    row1 += "<tbody>"
                    if (Report.length > 0) {
                        $.each(Report, function () {
                            JanOrd = JanOrd + (parseFloat($(this).find("JanOrd").text()));
                            JanTotal = JanTotal + (parseFloat($(this).find("JanTotal").text()));
                            FebOrd = FebOrd + (parseFloat($(this).find("FebOrd").text()));
                            FebTotal = FebTotal + (parseFloat($(this).find("FebTotal").text()));
                            MarOrd = MarOrd + (parseFloat($(this).find("MarOrd").text()));
                            MarTotal = MarTotal + (parseFloat($(this).find("MarTotal").text()));
                            AprOrd = AprOrd + (parseFloat($(this).find("AprOrd").text()));
                            AprTotal = AprTotal + (parseFloat($(this).find("AprTotal").text()));
                            MayOrd = MayOrd + (parseFloat($(this).find("MayOrd").text()));
                            MayTotal = MayTotal + (parseFloat($(this).find("MayTotal").text()));
                            JunOrd = JunOrd + (parseFloat($(this).find("JunOrd").text()));
                            JunTotal = JunTotal + (parseFloat($(this).find("JunTotal").text()));
                            JulOrd = JulOrd + (parseFloat($(this).find("JulOrd").text()));
                            JulTotal = JulTotal + (parseFloat($(this).find("JulTotal").text()));
                            AugOrd = AugOrd + (parseFloat($(this).find("AugOrd").text()));
                            AugTotal = AugTotal + (parseFloat($(this).find("AugTotal").text()));
                            SepOrd = SepOrd + (parseFloat($(this).find("SepOrd").text()));
                            SepTotal = SepTotal + (parseFloat($(this).find("SepTotal").text()));
                            OctOrd = OctOrd + (parseFloat($(this).find("OctOrd").text()));
                            OctTotal = OctTotal + (parseFloat($(this).find("OctTotal").text()));
                            NovOrd = NovOrd + (parseFloat($(this).find("NovOrd").text()));
                            NovTotal = NovTotal + (parseFloat($(this).find("NovTotal").text()));
                            DecOrd = DecOrd + (parseFloat($(this).find("DecOrd").text()));
                            DecTotal = DecTotal + (parseFloat($(this).find("DecTotal").text()));
                            YearOrders = YearOrders + (parseFloat($(this).find("YearOrders").text()));
                            YearTotal = YearTotal + (parseFloat($(this).find("YearTotal").text()));
                            row1 += ' <tr>'
                            row1 += '     <td class="P_CustomerName text-left width13per" style="white-space:nowrap">'+$(this).find("CustomerName").text()+'</td>'
                            row1 += '   <td class="P_SalesRep text-center width2.5per">'+$(this).find("SalesRep").text()+'</td>'
                            row1 += '  <td class="P_JanOrd text-center width2.5per">' + $(this).find("JanOrd").text()+'</td>'
                            row1 += ' <td class="P_JanTot text-right width2.5per">' + $(this).find("JanTotal").text()+'</td>'
                            row1 += '   <td class="P_FebOrd text-center width2.5per">' + $(this).find("FebOrd").text()+'</td>'
                            row1 += ' <td class="P_FebTot text-right width2.5per">' + $(this).find("FebTotal").text()+'</td>'
                            row1 += ' <td class="P_MarTot text-center width2.5per">' + $(this).find("MarOrd").text()+'</td>'
                            row1 += '    <td class="P_AprOrd text-right width2.5per">' + $(this).find("MarTotal").text()+'</td>'
                            row1 += '    <td class="P_AprTot text-center width2.5per">' + $(this).find("AprOrd").text() +'</td>'
                            row1 += '   <td class="P_MayOrd text-right width2.5per">' + $(this).find("AprTotal").text() +'</td>'
                            row1 += '   <td class="P_MayTot text-center width2.5per">' + $(this).find("MayOrd").text() +'</td>'
                            row1 += '   <td class="P_JunOrd text-right width2.5per">' + $(this).find("MayTotal").text() +'</td>'
                            row1 += '   <td class="P_JunTot text-center width2.5per">' + $(this).find("JunOrd").text() +'</td>'
                            row1 += '    <td class="P_JulOrd text-right width2.5per">' + $(this).find("JunTotal").text() +'</td>'
                            row1 += '   <td class="P_JulTot text-center width2.5per">' + $(this).find("JulOrd").text() +'</td>'
                            row1 += '  <td class="P_AugOrd text-right width2.5per">' + $(this).find("JulTotal").text() +'</td>'
                            row1 += '   <td class="P_AugTot text-center width2.5per">' + $(this).find("AugOrd").text() +'</td>'
                            row1 += '  <td class="P_SepOrd text-right width2.5per">' + $(this).find("AugTotal").text()+'</td>'
                            row1 += '  <td class="P_SepTot text-center width2.5per">' + $(this).find("SepOrd").text()+'</td>'
                            row1 += '  <td class="P_OctOrd text-right width2.5per">' + $(this).find("SepTotal").text()+'</td>'
                            row1 += '   <td class="P_OctTot text-center width2.5per">' + $(this).find("OctOrd").text() + '</td>'
                            row1 += '   <td class="P_OctTot text-right width2.5per">' + $(this).find("OctTotal").text() + '</td>'
                            row1 += ' <td class="P_NovOrd text-center width2.5per">' + $(this).find("NovOrd").text()+'</td>'
                            row1 += ' <td class="P_NovTot text-right width2.5per">' + $(this).find("NovTotal").text()+'</td>'
                            row1 += ' <td class="P_DecOrd text-center width2.5per">' + $(this).find("DecOrd").text()+'</td>'
                            row1 += ' <td class="P_DecTot text-right width2.5per">' + $(this).find("DecTotal").text()+'</td>'
                            row1 += '  <td class="P_YearlyOrd text-center width2.5per">' + $(this).find("YearOrders").text()+'</td>'
                            row1 += ' <td class="P_YearlyTot text-right width2.5per">' + $(this).find("YearTotal").text()+'</td>'
                            row1 += '  </tr>'
                        });
                        row1 += "</tbody>"
                        row1 += "<tfoot>"
                        row1 += '<tr style="font-weight:bold;">';
                        row1 += '<td style="font-weight:bold;white-space: nowrap" colspan="2">Total</td>';
                        row1 += '<td class=" text-center" style="font-weight:bold;white-space: nowrap">' + parseFloat(JanOrd) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(JanTotal).toFixed(2) + '</td>';
                        row1 += '<td class=" text-center" style="font-weight:bold;white-space: nowrap">' + parseFloat(FebOrd) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(FebTotal).toFixed(2) + '</td>';
                        row1 += '<td class=" text-center" style="font-weight:bold;white-space: nowrap">' + parseFloat(MarOrd) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(MarTotal).toFixed(2) + '</td>';
                        row1 += '<td class=" text-center" style="font-weight:bold;white-space: nowrap">' + parseFloat(AprOrd) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;font-weight:bold;white-space: nowrap">' + parseFloat(AprTotal).toFixed(2) + '</td>';
                        row1 += '<td class=" text-center" style="font-weight:bold;white-space: nowrap">' + parseFloat(MayOrd) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(MayTotal).toFixed(2) + '</td>';
                        row1 += '<td class=" text-center" style="font-weight:bold;white-space: nowrap">' + parseFloat(JunOrd) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(JunTotal).toFixed(2) + '</td>';
                        row1 += '<td class=" text-center" style="font-weight:bold;white-space: nowrap">' + parseFloat(JulOrd) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(JulTotal).toFixed(2) + '</td>';
                        row1 += '<td class=" text-center" style="font-weight:bold;white-space: nowrap">' + parseFloat(AugOrd) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(AugTotal).toFixed(2) + '</td>';
                        row1 += '<td class=" text-center" style="font-weight:bold;white-space: nowrap">' + parseFloat(SepOrd) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(SepTotal).toFixed(2) + '</td>';
                        row1 += '<td class=" text-center" style="font-weight:bold;white-space: nowrap">' + parseFloat(OctOrd) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(OctTotal).toFixed(2) + '</td>';
                        row1 += '<td class=" text-center" style="font-weight:bold;white-space: nowrap">' + parseFloat(NovOrd) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(NovTotal).toFixed(2) + '</td>';
                        row1 += '<td class=" text-center" style="font-weight:bold;white-space: nowrap">' + parseFloat(DecOrd) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(DecTotal).toFixed(2) + '</td>';
                        row1 += '<td class=" text-center" style="font-weight:bold;white-space: nowrap">' + parseFloat(YearOrders) + '</td>';
                        row1 += '<td class="right" style="font-weight:bold;white-space: nowrap">' + parseFloat(YearTotal).toFixed(2) + '</td>';
                        row1 += "</tfoot>"
                        row1 += "</table>"
                    }
                    if (us == 1) {
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>#RptTable {border-collapse: collapse;width: 100%;}#RptTable td, #RptTable th {border: 1px solid black;}#RptTable tr:nth-child(even){background-color: #f2f2f2;}#RptTable thead {padding-top: 12px;padding-bottom: 12px;text-align:center;background-color:#e9e8e8 !important;color:black;}.text-right{text-align:right;}.text-left{text-align:left;}.text-center{text-align:center;}</style>');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        mywindow.document.write('</head><body>');
                        mywindow.document.write(row1);
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#ExcelDiv").html(row1);
                        $("#RptTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "Yearly Report",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblYearlyReport tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblYearlyReport tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function getProjectReportFunc() {
    $('#dllYearDate').each(function () {
        var year = (new Date()).getFullYear();
        var current = year;
        year -= 3;
        for (var i = 0; i < 6; i++) {
            if ((year + i) == current)
                $(this).append('<option selected value="' + (year + i) + '">' + (year + i) + '</option>');
            else
                $(this).append('<option value="' + (year + i) + '">' + (year + i) + '</option>');
        }

    })
}
