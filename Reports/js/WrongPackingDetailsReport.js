﻿$(document).ready(function () {
    getProductList();
    getWrongPackingDetailsReport(1);
})
function Pagevalue(e) {
    getWrongPackingDetailsReport(parseInt($(e).attr("page")));
};
function getProductList() {
    $.ajax({
        type: "POST",
        url: "WrongPackingDetailsReport.aspx/GetProductList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        cache: false,

        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            debugger;
            var xmldoc = $.parseXML(result.d);
            var ProductList = $(xmldoc).find("Table");
            $("#ddlProduct option").remove();
            $("#ddlProduct").append("<option value='0'>All Product</option>");
            $.each(ProductList, function () {
                $("#ddlProduct").append("<option value='" + $(this).find("ProductId").text() + "'>" + $(this).find("Name").text() + "</option>");
            });
            $("#ddlProduct").select2();
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function getWrongPackingDetailsReport(PageIndex) {
    var data = {
        PageIndex: PageIndex,
        PageSize: $("#ddlPageSize").val(),
        ProductId: $("#ddlProduct").val()
    };
    $.ajax({
        type: "POST",
        url: "WrongPackingDetailsReport.aspx/GetPackingDetails",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        cache: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (result) {
            debugger;
            var xmldoc = $.parseXML(result.d);
            var PackingList = $(xmldoc).find("Table1");
            $("#tblWrongPackingDetailsReport tbody tr").remove();
            var row = $("#tblWrongPackingDetailsReport thead tr:last").clone(true);
            if (PackingList.length > 0) {
                $("#EmptyTable").hide();
                $.each(PackingList, function (index) {
                    $(".ProductId", row).text($(this).find("ProductId").text());
                    $(".UnitType", row).text($(this).find("UnitType").text());
                    $(".NJQty", row).text($(this).find("NJQty").text());
                    $(".PAQty", row).text($(this).find("PAQty").text());
                    $(".NPAQty", row).text($(this).find("NPAQty").text());
                    $(".WPAQty", row).text($(this).find("WPAQty").text());
                    $(".CTQty", row).text($(this).find("CTQty").text());
                    $("#tblWrongPackingDetailsReport tbody").append(row);
                    row = $("#tblWrongPackingDetailsReport tbody tr:last").clone(true);
                });
                $("#tblWrongPackingDetailsReport tbody tr").show();
            } else {
                $("#EmptyTable").show();
            }

            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
            if ($("#ddlPageSize").val() == '0') {
                $(".Pager").hide();
            }
            else {
                $(".Pager").show();
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
//$("#ddlPageSize").change(function () {
//    getWrongPackingDetailsReport(1);
//})