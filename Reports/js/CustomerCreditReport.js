﻿$(document).ready(function () { 
    BindCustomer();
});

function BindCustomer() {
    $.ajax({
        type: "POST",
        url: "Customer_Credit_Report.aspx/BindCustomer",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
         
            var getData = $.parseJSON(response.d);
            $("#ddlCustomer option:not(:first)").remove();
            $.each(getData, function (index, item) {
             
                $("#ddlCustomer").append("<option value='" + getData[index].AutoId + "'>" + getData[index].CustomerName + "</option>");
            });
            $("#ddlCustomer").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function Pagevalue(e) {
  
    CustomerCreditReport(parseInt($(e).attr("page")));
}





function CustomerCreditReport(PageIndex) {
    var data = {
        CustomerAutoId: $("#ddlCustomer").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "Customer_Credit_Report.aspx/GetCustomerCreditReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success:SuccessCustomerCreditReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}




function SuccessCustomerCreditReport(response) {
    if (response.d == 'false') {
    }
    else {
        var xmldoc = $.parseXML(response.d);
        var CustomerCreditReport = $(xmldoc).find("Table1");
        var OrderTotal = $(xmldoc).find("Table2");
        var TcreditAmt = 0;
        $("#tblCustomerCreditReport tbody tr").remove();
        var row = $("#tblCustomerCreditReport thead tr").clone(true);
        if (CustomerCreditReport.length > 0) {
            $("#EmptyTable").hide();
            $.each(CustomerCreditReport, function () {

                $(".CustomerId", row).text($(this).find("CustomerId").text());
                $(".CustomerName", row).text($(this).find("CustomerName").text());
                $(".CreditAmount", row).text($(this).find("CreditAmount").text());
                $("#tblCustomerCreditReport tbody").append(row);
                row = $("#tblCustomerCreditReport tbody tr:last").clone(true);
                TcreditAmt = TcreditAmt + parseFloat($(this).find("CreditAmount").text());
            });
            $('#CreditAmt').html(TcreditAmt.toFixed(2));
        } else {
            // $("#EmptyTable").show();
            $('#CreditAmt').html('0');
        }
        $(OrderTotal).each(function () {
            $('#OACreditAmt').html(parseFloat($(this).find('CreditAmount').text()).toFixed(2));
        });
       
        var pager = $(xmldoc).find("Table");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
    }
}

function CreateTable(us) {
    var image = $("#imgName").val();
    var data = {
        CustomerAutoId: $("#ddlCustomer").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/WCustomerCreditReports.asmx/GetCustomerCreditReport",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var CustomerCreditReport = $(xmldoc).find("Table1");
                    var PrintDate = $(xmldoc).find("Table");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    var P_TcreditAmt = 0;
                    if (CustomerCreditReport.length > 0) {
                        $.each(CustomerCreditReport, function () {
                            $(".P_CustomerId", row).text($(this).find("CustomerId").text());
                            $(".P_CustomerName", row).text($(this).find("CustomerName").text());
                            $(".P_CreditAmount", row).text($(this).find("CreditAmount").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                            P_TcreditAmt = P_TcreditAmt + parseFloat($(this).find("CreditAmount").text());
                        });
                        $('#P_CreditAmt').html(P_TcreditAmt.toFixed(2));
                    }
                    if (us == 1) {
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        //if ($("#txtFromDate").val() != "") {
                        //    $("#DateRange").text("Date Range: " + $("#txtFromDate").val() + " To " + $("#txtToDate").val());
                        //}
                        mywindow.document.write($(PrintTable1).clone().html());
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "CustomerCreditReport(" + (new Date()).format("MM/dd/yyyy hh:mm tt") + ")",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblCustomerCreditReport tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblCustomerCreditReport tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

