﻿$(document).ready(function () {
    bindSalesPerson();
    $('#txtDateFrom').pickadate({
        min: new Date('01/01/2019'),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtDateTo').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtDateFrom").val(month + '/' + day + '/' + year);
    $("#txtDateTo").val(month + '/' + day + '/' + year);

});
function setdatevalidation(val) {
    var fdate = $("#txtDateFrom").val();
    var tdate = $("#txtDateTo").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtDateTo").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtDateFrom").val(tdate);
        }
    }
}
function bindSalesPerson() {
    $.ajax({
        type: "POST",
        url: "ProductWiseCommissionReport.aspx/BindSalesPerson",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {

                    var getData = $.parseJSON(response.d);

                    $("#ddlSalesPersonName option:not(:first)").remove();
                    $.each(getData[0].SalesList, function (index, item) {
                        $("#ddlSalesPersonName").append("<option value='" + item.SID + "'>" + item.SP + "</option>");
                    });

                    $("#ddlSalesPersonName").select2()

                    $("#ddlCommissionCode option:not(:first)").remove();
                    $.each(getData[0].CoCode, function (index, item) {
                        $("#ddlCommissionCode").append("<option value='" + parseFloat(item.CCode).toFixed(4) + "'>" + item.CD + "</option>");
                    });

                    $("#ddlCommissionCode").select2()
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Pagevalue(e) {
    GetProductWiseCommissionReport(parseInt($(e).attr("page")));
}

function GetProductWiseCommissionReport(PageIndex) {
    var data = {
        FromDate: $("#txtDateFrom").val(),
        ToDate: $("#txtDateTo").val(),
        CommissionCode: $("#ddlCommissionCode option:selected").val(),
        ProductId: $("#txtProductId").val().trim(),
        ProductName2: $("#txtProductName").val().trim(),
        SalesPersonAutoId: $("#ddlSalesPersonName").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "ProductWiseCommissionReport.aspx/GetProductWiseCommissionList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetRouteList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetRouteList(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var RouteList = $(xmldoc).find("Table1");
            var OrderTotal = $(xmldoc).find("Table2");
            $("#tblProductCatelogReport tbody tr").remove();
            var row = $("#tblProductCatelogReport thead tr").clone(true);
            var TotalSale = 0, NoofProduct = 0, SPCommAmt = 0;
            var C_TotalSale = 0, C_NoofProduct = 0, C_SPCommAmt = 0;
            var A_TotalSale = 0, A_NoofProduct = 0, A_SPCommAmt = 0;
            var T_TotalSale = 0, T_NoofProduct = 0, T_SPCommAmt = 0,
                T_CreditQty = 0, T_CreditAmount = 0, T_CreditCommissionAmount = 0,
                T_ActualSoldQty = 0, T_ActualSoldAmount = 0, T_ActualCommissionAmount = 0;

            if (RouteList.length > 0) {
                $("#EmptyTable").hide();
                var html = "", SalesPerson = 0, TotalPiece = 0
                $("#tblProductCatelogReport thead tr").show();
                $.each(RouteList, function (index) {
                    if (SalesPerson != '0' && (SalesPerson != $(this).find("SalesPerson").text())) {
                        html = "<tr style='font-weight: 700; background: oldlace;'><td style='text-align:center;color:#dd0b3c;background:#faf2f2;' colspan='5'><b>" + SalesPerson + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                            + NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(SPCommAmt).toFixed(2) + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                            + C_NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(C_NoofProduct).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(C_SPCommAmt).toFixed(2) + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                            + A_NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(A_TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(A_SPCommAmt).toFixed(2) + "</b></td></tr>";
                        $("#tblProductCatelogReport tbody").append(html);
                        TotalSale = 0, NoofProduct = 0, SPCommAmt = 0;
                        C_TotalSale = 0, C_NoofProduct = 0, C_SPCommAmt = 0;
                        A_TotalSale = 0, A_NoofProduct = 0, A_SPCommAmt = 0;
                    }

                    $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                    $(".ProductId", row).text($(this).find("ProductId").text());
                    $(".ProductName", row).text($(this).find("ProductName").text());
                    $(".UnitType", row).text($(this).find("UnitType").text());
                    $(".SoldQty", row).text($(this).find("SoldQty").text());
                    $(".CreditQty", row).text($(this).find("CreditQty").text());
                    $(".CreditAmount", row).text($(this).find("CreditAmount").text());
                    $(".CreditCommissionAmount", row).text($(this).find("CreditCommissionAmount").text());
                    $(".ActualSoldQty", row).text($(this).find("ActualSoldQty").text());
                    $(".ActualSoldAmount", row).text($(this).find("ActualSoldAmount").text());
                    $(".ActualCommissionAmount", row).text($(this).find("ActualCommissionAmount").text());
                    $(".CommCode", row).text($(this).find("CommCode").text());
                    $(".SoldAmount", row).text(parseFloat($(this).find("SoldAmount").text()).toFixed(2));
                    $(".SPCommAmt", row).text(parseFloat($(this).find("SoldCommissionAmount").text()).toFixed(2));

                    SalesPerson = $(this).find("SalesPerson").text();
                    TotalSale = TotalSale + parseFloat($(this).find("SoldAmount").text());
                    NoofProduct = NoofProduct + parseInt($(this).find("SoldQty").text());
                    SPCommAmt = SPCommAmt + parseFloat($(this).find("SoldCommissionAmount").text());

                    C_TotalSale = C_TotalSale + parseFloat($(this).find("CreditAmount").text());
                    C_NoofProduct = C_NoofProduct + parseInt($(this).find("CreditQty").text());
                    C_SPCommAmt = C_SPCommAmt + parseFloat($(this).find("CreditCommissionAmount").text());


                    A_TotalSale = A_TotalSale + parseFloat($(this).find("ActualSoldAmount").text());
                    A_NoofProduct = A_NoofProduct + parseInt($(this).find("ActualSoldQty").text());
                    A_SPCommAmt = A_SPCommAmt + parseFloat($(this).find("ActualCommissionAmount").text());



                    T_TotalSale = T_TotalSale + parseFloat($(this).find("SoldAmount").text());
                    T_NoofProduct = T_NoofProduct + parseInt($(this).find("SoldQty").text());
                    T_SPCommAmt = T_SPCommAmt + parseFloat($(this).find("SoldCommissionAmount").text());

                    T_CreditQty = T_CreditQty + parseFloat($(this).find("CreditQty").text());
                    T_CreditAmount = T_CreditAmount + parseFloat($(this).find("CreditAmount").text());
                    T_CreditCommissionAmount = T_CreditCommissionAmount + parseFloat($(this).find("CreditCommissionAmount").text());

                    T_ActualSoldQty = T_ActualSoldQty + parseFloat($(this).find("ActualSoldQty").text());

                    T_ActualSoldAmount = T_ActualSoldAmount + parseFloat($(this).find("ActualSoldAmount").text());
                    T_ActualCommissionAmount = T_ActualCommissionAmount + parseFloat($(this).find("ActualCommissionAmount").text());


                    $("#tblProductCatelogReport tbody").append(row);
                    row = $("#tblProductCatelogReport tbody tr:last").clone(true);
                    SalesPerson = $(this).find("SalesPerson").text();

                });
                if (SalesPerson != '') {
                    html = "<tr style='font-weight: 700; background: oldlace;'><td style='text-align:center;color:#dd0b3c;background:#faf2f2;' colspan='5'><b>" + SalesPerson + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                        + NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(SPCommAmt).toFixed(2) + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                        + C_NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(C_NoofProduct).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(C_SPCommAmt).toFixed(2) + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                        + A_NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(A_TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(A_SPCommAmt).toFixed(2) + "</b></td></tr>";
                    $("#tblProductCatelogReport tbody").append(html);
                }
                $('#T_NoofProduct').html(parseFloat(T_NoofProduct).toFixed(0));
                $('#T_TotalSale').html(parseFloat(T_TotalSale).toFixed(2));
                $('#T_SPCommAmt').html(parseFloat(T_SPCommAmt).toFixed(2));

                $('#T_CreditQty').html(parseFloat(T_CreditQty).toFixed(0));
                $('#T_CreditAmount').html(parseFloat(T_CreditAmount).toFixed(2));
                $('#T_CreditCommissionAmount').html(parseFloat(T_CreditCommissionAmount).toFixed(2));

                $('#T_ActualSoldQty').html(parseFloat(T_ActualSoldQty).toFixed(0));
                $('#T_ActualSoldAmount').html(parseFloat(T_ActualSoldAmount).toFixed(2));
                //$('#T_ActualCommissionAmount').html(parseFloat(T_ActualCommissionAmount).toFixed(2));
                $('#T_ActualCommissionAmount').html(parseFloat(T_ActualCommissionAmount).toFixed(2));

            } else {
                $('#T_CreditQty').html('0');
                $('#T_CreditAmount').html('0.00');
                $('#T_CreditCommissionAmount').html('0.00');
                $('#TotalQty').html('0');
                $('#AmtDue').html('0.00');
                $('#T_NoofProduct').html('0')
                $('#T_TotalSale').html('0.00');
                $('#T_SPCommAmt').html('0.00');
                $('#T_ActualSoldQty').html('0');
                $('#T_ActualSoldAmount').html('0.00');
                $('#T_ActualCommissionAmount').html('0.00');
            }

            $(OrderTotal).each(function () {
                $('#T_NoofProducts').html(parseFloat($(this).find('SoldQty').text()));
                $('#T_TotalSales').html(parseFloat($(this).find('SoldAmount').text()).toFixed(2));
                $('#T_SPCommAmts').html(parseFloat($(this).find('SoldCommissionAmount').text()).toFixed(2));
                $('#T_CreditQtys').html(parseFloat($(this).find('CreditQty').text()));
                $('#T_CreditAmounts').html(parseFloat($(this).find('CreditAmount').text()).toFixed(2));
                $('#T_CreditCommissionAmounts').html(parseFloat($(this).find('CreditCommissionAmount').text()).toFixed(2));
                $('#T_ActualSoldQtys').html(parseFloat($(this).find('ActualSoldQty').text()));
                $('#T_ActualSoldAmounts').html(parseFloat($(this).find('ActualSoldAmount').text()).toFixed(2));
                $('#T_ActualCommissionAmounts').html(parseFloat($(this).find('ActualCommissionAmount').text()).toFixed(2));
            });

            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
            if (Number($("#ddlPageSize").val()) == '0') {
                $("#trTotal").hide();
            }
            else if (Number(pager.find("RecordCount").text()) < Number(pager.find("PageSize").text())) {
                $("#trTotal").hide();
            }
            else {
                $("#trTotal").show();
            }
        }
    }
    else {
        location.href = "/";
    }
}

function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        FromDate: $("#txtDateFrom").val(),
        ToDate: $("#txtDateTo").val(),
        CommissionCode: $("#ddlCommissionCode option:selected").val(),
        ProductId: $("#txtProductId").val().trim(),
        ProductName2: $("#txtProductName").val().trim(),
        SalesPersonAutoId: $("#ddlSalesPersonName").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "ProductWiseCommissionReport.aspx/GetProductWiseCommissionList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var RouteList = $(xmldoc).find("Table1");
                    var PrintDate = $(xmldoc).find("Table");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    var TotalSale = 0, NoofProduct = 0, SPCommAmt = 0;
                    var C_TotalSale = 0, C_NoofProduct = 0, C_SPCommAmt = 0;
                    var A_TotalSale = 0, A_NoofProduct = 0, A_SPCommAmt = 0;
                    var T_TotalSale = 0, T_NoofProduct = 0, T_SPCommAmt = 0,
                        T_CreditQty = 0, T_CreditAmount = 0, T_CreditCommissionAmount = 0,
                        T_ActualSoldQty = 0, T_ActualSoldAmount = 0, T_ActualCommissionAmount = 0;

                    if (RouteList.length > 0) {
                        var html = "", SalesPerson = 0, TotalPiece = 0
                        $.each(RouteList, function () {
                            if (SalesPerson != '0' && (SalesPerson != $(this).find("SalesPerson").text())) {
                                html = "<tr style='font-weight: 700; background: oldlace;'><td style='text-align:center;color:#dd0b3c;background:#faf2f2;' colspan='5'><b>" + SalesPerson + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(SPCommAmt).toFixed(2) + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + C_NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(C_NoofProduct).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(C_SPCommAmt).toFixed(2) + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + A_NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(A_TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(A_SPCommAmt).toFixed(2) + "</b></td></tr>";
                                $("#PrintTable tbody").append(html);
                                TotalSale = 0, NoofProduct = 0, SPCommAmt = 0;
                                C_TotalSale = 0, C_NoofProduct = 0, C_SPCommAmt = 0;
                                A_TotalSale = 0, A_NoofProduct = 0, A_SPCommAmt = 0;
                            }

                            $(".PSalesPerson", row).text($(this).find("SalesPerson").text());
                            $(".PProductId", row).text($(this).find("ProductId").text());
                            $(".PProductName", row).text($(this).find("ProductName").text());
                            $(".PUnitType", row).text($(this).find("UnitType").text());
                            $(".PSoldQty", row).text($(this).find("SoldQty").text());
                            $(".PCreditQty", row).text($(this).find("CreditQty").text());
                            $(".PCreditAmount", row).text($(this).find("CreditAmount").text());
                            $(".PCreditCommissionAmount", row).text($(this).find("CreditCommissionAmount").text());
                            $(".PActualSoldQty", row).text($(this).find("ActualSoldQty").text());
                            $(".PActualSoldAmount", row).text($(this).find("ActualSoldAmount").text());
                            $(".PActualCommissionAmount", row).text($(this).find("ActualCommissionAmount").text());
                            $(".PCommCode", row).text($(this).find("CommCode").text());
                            $(".PSoldAmount", row).text(parseFloat($(this).find("SoldAmount").text()).toFixed(2));
                            $(".PSPCommAmt", row).text(parseFloat($(this).find("SoldCommissionAmount").text()).toFixed(2));

                            SalesPerson = $(this).find("SalesPerson").text();
                            TotalSale = TotalSale + parseFloat($(this).find("SoldAmount").text());
                            NoofProduct = NoofProduct + parseInt($(this).find("SoldQty").text());
                            SPCommAmt = SPCommAmt + parseFloat($(this).find("SoldCommissionAmount").text());

                            C_TotalSale = C_TotalSale + parseFloat($(this).find("CreditAmount").text());
                            C_NoofProduct = C_NoofProduct + parseInt($(this).find("CreditQty").text());
                            C_SPCommAmt = C_SPCommAmt + parseFloat($(this).find("CreditCommissionAmount").text());


                            A_TotalSale = A_TotalSale + parseFloat($(this).find("ActualSoldAmount").text());
                            A_NoofProduct = A_NoofProduct + parseInt($(this).find("ActualSoldQty").text());
                            A_SPCommAmt = A_SPCommAmt + parseFloat($(this).find("ActualCommissionAmount").text());



                            T_TotalSale = T_TotalSale + parseFloat($(this).find("SoldAmount").text());
                            T_NoofProduct = T_NoofProduct + parseInt($(this).find("SoldQty").text());
                            T_SPCommAmt = T_SPCommAmt + parseFloat($(this).find("SoldCommissionAmount").text());

                            T_CreditQty = T_CreditQty + parseFloat($(this).find("CreditQty").text());
                            T_CreditAmount = T_CreditAmount + parseFloat($(this).find("CreditAmount").text());
                            T_CreditCommissionAmount = T_CreditCommissionAmount + parseFloat($(this).find("CreditCommissionAmount").text());

                            T_ActualSoldQty = T_ActualSoldQty + parseFloat($(this).find("ActualSoldQty").text());

                            T_ActualSoldAmount = T_ActualSoldAmount + parseFloat($(this).find("ActualSoldAmount").text());
                            T_ActualCommissionAmount = T_ActualCommissionAmount + parseFloat($(this).find("ActualCommissionAmount").text());

                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                        if (SalesPerson != '0' && (SalesPerson != $(this).find("SalesPerson").text())) {
                            html = "<tr style='font-weight: 700; background: oldlace;'><td style='text-align:center;color:#dd0b3c;background:#faf2f2;' colspan='5'><b>" + SalesPerson + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                + NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(SPCommAmt).toFixed(2) + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                + C_NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(C_NoofProduct).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(C_SPCommAmt).toFixed(2) + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                + A_NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(A_TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(A_SPCommAmt).toFixed(2) + "</b></td></tr>";
                            $("#PrintTable tbody").append(html);                            
                        }
                        $('#PT_NoofProduct').html(parseFloat(T_NoofProduct).toFixed(0));
                        $('#PT_TotalSale').html(parseFloat(T_TotalSale).toFixed(2));
                        $('#PT_SPCommAmt').html(parseFloat(T_SPCommAmt).toFixed(2));
                        $('#PT_CreditQty').html(parseFloat(T_CreditQty).toFixed(0));
                        $('#PT_CreditAmount').html(parseFloat(T_CreditAmount).toFixed(2));
                        $('#PT_CreditCommissionAmount').html(parseFloat(T_CreditCommissionAmount).toFixed(2));
                        $('#PT_ActualSoldQty').html(parseFloat(T_ActualSoldQty).toFixed(0));
                        $('#PT_ActualSoldAmount').html(parseFloat(T_ActualSoldAmount).toFixed(2));
                        $('#PT_ActualCommissionAmount').html(parseFloat(T_ActualCommissionAmount).toFixed(2));
                    }
                    if (us == 1) {
                        
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + (PrintDate.find('PrintDate').text()));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtDateFrom").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtDateFrom").val() + " To " + $("#txtDateTo").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        //mywindow.document.write(divToPrint.outerHTML);

                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "EmployeeCommissionDetailsReport",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblProductCatelogReport tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblProductCatelogReport tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

