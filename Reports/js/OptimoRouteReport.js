﻿$(document).ready(function () {
   
    $('.date').pickadate({
        changeMonth: true,       // this option for allowing user to select month
        changeYear: true,       // this option for allowing user to select from year range
        dateFormat: 'mm/dd/yy' // US date format
    });
    BindDropDown();
   // BindCustomer();
});

function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
};

/*---------------------------------Bind Customer Category-----------------------------------*/
function BindDropDown() {
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/OptimoRouteReport.asmx/BindDropDown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var Driver = $(xmldoc).find("Table2");
                    var SalesPerson = $(xmldoc).find("Table1");
                    var Status = $(xmldoc).find("Table");
                    $("#ddlDriver option:not(:first)").remove();
                    $.each(Driver, function () {
                        $("#ddlDriver").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Driver").text() + "</option>");
                    });
                    $("#ddlDriver").select2();

                    $("#ddlSalesPerson option:not(:first)").remove();
                    $.each(SalesPerson, function () {
                        $("#ddlSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("SalesPerson").text() + "</option>");
                    });
                    $("#ddlSalesPerson").select2();

                    $("#ddlStatus option:not(:first)").remove();
                    $.each(Status, function () {
                        $("#ddlStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                    });
                    $("#ddlStatus").select2();                
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function BindReport(PageIndex) {
   
    var data = {
        Status: $("#ddlStatus").val(),
        DriverAutoId: $("#ddlDriver").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageSize: $("#ddlPaging").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/OptimoRouteReport.asmx/BindList",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successOrderWiseOpenBalanceReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successOrderWiseOpenBalanceReport(response) {
   
    if (response.d != "Session Expired") {
        if (response.d != "false") {

            var xmldoc = $.parseXML(response.d);
            var OrderList = $(xmldoc).find("Table");
            var OrderTotal = $(xmldoc).find("Table2");

            $("#tblOrderList tbody tr").remove();
            var TotalOrder = 0; var TotalOrdAmt = 0.00; var TotalPaid = 0.00; var Totaldueamt = 0;
            var row = $("#tblOrderList thead tr").clone(true);
            if (OrderList.length > 0) {
                $("#EmptyTable").hide();
                $.each(OrderList, function () {                   
                    $(".OrderNo", row).text($(this).find("OrderNo").text());
                    $(".DeliveryDate", row).text($(this).find("DeliveryDate").text());
                    $(".Addres", row).text($(this).find("Addres").text());
                    $(".PayableAmount", row).text($(this).find("PayableAmount").text());
                    $(".PackedBoxes", row).html($(this).find("PackedBoxes").text());
                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);                 
                    TotalPaid = TotalPaid + parseFloat($(this).find("PayableAmount").text());
                    Totaldueamt = Totaldueamt + parseFloat($(this).find("PackedBoxes").text());
                });
                $('#AmtDued').html(Totaldueamt);            
                $('#TotalPaidd').html(TotalPaid.toFixed(2));            
            } else {
               
                $('#AmtDued').html('0');             
                $('#TotalPaidd').html('0.00');                        
                toastr.error('No data available.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }

            $(OrderTotal).each(function () {                 
                if ($(this).find('PayableAmount').text() != '') {
                    $('#PayableAmount').html(parseInt($(this).find('PayableAmount').text()).toFixed(2));
                    $('#PackedBoxes').html(parseFloat($(this).find('PackedBoxes').text()));
                }
                else {
                    $('#PayableAmount').html('0.00');
                    $('#PackedBoxes').html('0');
                }
               
            });       
            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}

$("#btnSearch").click(function () {
    BindReport(1);
})
$("#ddlPaging").change(function () {
    BindReport(1);
})
function CreateTable(us) {
   
    var image = $("#imgName").val();
    var data = {
        Status: $("#ddlStatus").val(),
        DriverAutoId: $("#ddlDriver").val(),
        SalesPersonAutoId: $("#ddlSalesPerson").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "/Reports/WebAPI/OptimoRouteReport.asmx/BindList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var productBarcode = $(xmldoc).find("Table");

                    var OrderTotal = $(xmldoc).find("Table2");

                    $("#PrintTable1 tbody tr").remove();
                    var TotalOrder = 0;
                    var row = $("#PrintTable1 thead tr").clone(true);
                    if (productBarcode.length > 0) {
                        $.each(productBarcode, function () {

                            $(".POrderNo", row).text($(this).find("OrderNo").text());
                            $(".PDeliveryDate", row).text($(this).find("DeliveryDate").text());
                            $(".PAddres", row).text($(this).find("Addres").text());
                            $(".PPayableAmount", row).text($(this).find("PayableAmount").text());
                            $(".PPackedBoxes", row).html($(this).find("PackedBoxes").text());
                            $("#PrintTable1 tbody").append(row);
                            row = $("#PrintTable1 tbody tr:last").clone(true);
                            TotalOrder = TotalOrder + parseInt($(this).find("NoofOrder").text());
                        });
                    }

                    $(OrderTotal).each(function () {
                        
                        $('#PTotalPaidd').html(parseFloat($(this).find('PayableAmount').text()).toFixed(2));
                        $('#PAmtDued').html(parseFloat($(this).find('PackedBoxes').text()).toFixed(2));
                        
                    });
                    $('#PTotalOrder').html(TotalOrder);
                    if (us == 1) {                     
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + (new Date()).format("MM/dd/yyyy hh:mm tt"));
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);

                        mywindow.document.write($(PrintTable).clone().html());                     
                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable1").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "OptimoRouteReport" + (new Date()).format("MM/dd/yyyy hh:mm tt"),
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }


}
/*---------------Export To Excel---------------------------*/
$("#btnExport").click(function () {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});


