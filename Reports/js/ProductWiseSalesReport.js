﻿var row1 = "";
$(document).ready(function () { 
    $('#txtOrdCloseFromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtOrdCloseToDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });

    var d = new Date();
    var month = d.getMonth() + 1;
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    var day = d.getDate();
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    var year = d.getFullYear();
    $("#txtOrdCloseFromDate").val(month + '/' + day + '/' + year);
    $("#txtOrdCloseToDate").val(month + '/' + day + '/' + year);
    
    BindCustomer();
    BindProduct();
    BindProductCategory();
    BindProductSubCategory();
    BindSalesPersonandStatus();
});
function setdatevalidation(val) {
    var fdate = $("#txtOrdCloseFromDate").val();
    var tdate = $("#txtOrdCloseToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtOrdCloseToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtOrdCloseFromDate").val(tdate);
        }
    }
}
function Pagevalue(e) {
    getReport(parseInt($(e).attr("page")));
};
/*---------------------------------------------------Bind Customer-----------------------------------------------------------*/
function BindCustomer() {
    $.ajax({
        type: "POST",
        url: "ProductWiseSalesReport.aspx/BindCustomer",
        data: "{'CustomerType':'" + $('#ddlCustomerType').val() + "','SalesPersonAutoId':'" + $('#ddlAllPerson').val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) { 
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    if (response.d != "") {
                        var CustomerList = $.parseJSON(response.d);
                        var ddlCustomer = $("#ddlAllCustomer");
                        $("#ddlAllCustomer option:not(:first)").remove();
                        for (var i = 0; i < CustomerList.length; i++) {
                            var Customer = CustomerList[i];
                            var option = $("<option />");
                            option.html(Customer.CustomerName);
                            option.val(Customer.CustomerAutoId);
                            ddlCustomer.append(option);
                        }
                        $("#ddlAllCustomer").select2();
                    }
                    else {
                        $("#ddlAllCustomer option:not(:first)").remove();
                        $("#ddlAllCustomer").select2({
                            placeholder: "All Customer",
                            allowClear: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}
function BindSalesPersonandStatus() {
    $.ajax({
        type: "POST",
        url: "ProductWiseSalesReport.aspx/BindSalesPersonandStatus",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") { 
                if (response.d != "false") {
                    if (response.d != "") {
                        var DropDown = $.parseJSON(response.d);
                        for (var i = 0; i < DropDown.length; i++) {
                            var AllDropDownList = DropDown[i];
                            var PersonList = AllDropDownList.EmpList;
                            var ddlSalesPerson = $("#ddlAllPerson");
                            $("#ddlAllPerson option:not(:first)").remove();
                            for (var j = 0; j < PersonList.length; j++) {
                                var Person = PersonList[j];
                                var option = $("<option />");
                                option.html(Person.EmpName);
                                option.val(Person.EmpAutoId);
                                ddlSalesPerson.append(option);
                            }
                            ddlSalesPerson.select2();

                            var ddlStatus = $("#ddlStatus");
                            $("#ddlStatus option:not(:first)").remove();
                            var statusList = AllDropDownList.StatusList;
                            for (var k = 0; k < statusList.length; k++) {
                                var status = statusList[k];
                                var option = $("<option />");
                                option.html(status.StatusName);
                                option.val(status.AutoId);
                                ddlStatus.append(option);
                            }
                            ddlStatus.append(option);

                            var ddlBrand = $("#ddlBrand");
                            $("#ddlBrand").attr('multiple', 'multiple');
                            $("#ddlBrand option:not(:first)").remove();
                            var BrandList = AllDropDownList.BrandList;
                            for (var k = 0; k < BrandList.length; k++) {
                                var brand = BrandList[k];
                                var option = $("<option />");
                                option.html(brand.BrandName);
                                option.val(brand.AutoId);
                                ddlBrand.append(option);
                            }
                            $("#ddlBrand").select2({
                                placeholder: 'All Brand',
                                allowClear: true
                            });
                            ddlBrand.append(option);
                        }
                    }
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}
function BindProductCategory() {
    $.ajax({
        type: "POST",
        url: "ProductWiseSalesReport.aspx/BindProductCategory",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    if (response.d != "") {
                        var DropDown = $.parseJSON(response.d);
                        for (var i = 0; i < DropDown.length; i++) {
                            var AllDropDownList = DropDown[i];
                            var CategoryList = AllDropDownList.CategoryList;
                            var ddlCategory = $("#ddlAllCategory");
                            $("#ddlAllCategory option:not(:first)").remove();
                            for (var j = 0; j < CategoryList.length; j++) {
                                var Category = CategoryList[j];
                                var option = $("<option />");
                                option.html(Category.CategoryName);
                                option.val(Category.AutoId);
                                ddlCategory.append(option);
                            }
                            ddlCategory.select2();
                            var ddlCustomerType = $("#ddlCustomerType");
                            $("#ddlCustomerType option:not(:first)").remove();
                            var Customertp = AllDropDownList.CustomerType;
                            for (var k = 0; k < Customertp.length; k++) {
                                var CType = Customertp[k];
                                var option = $("<option />");
                                option.html(CType.CustomerType);
                                option.val(CType.AutoId);
                                ddlCustomerType.append(option);
                            }
                            ddlCustomerType.append(option);
                        }
                    }
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
/*---------------------------------------------------Bind Sub Category-----------------------------------------------------------*/
function BindProductSubCategory() {
    $.ajax({
        type: "POST",
        url: "ProductWiseSalesReport.aspx/BindProductSubCategory",
        data: "{'CategoryAutoId':'" + $("#ddlAllCategory").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    if (response.d != "") { 
                        var DropDown = $.parseJSON(response.d);
                        for (var i = 0; i < DropDown.length; i++) {
                            var AllDropDownList = DropDown[i];
                            var SubCategoryList = AllDropDownList.SubCategoryList;
                            if (SubCategoryList != null) {
                                var ddlSubCategory = $("#AllSubCategory");
                                $("#AllSubCategory option:not(:first)").remove();
                                for (var j = 0; j < SubCategoryList.length; j++) {
                                    var SubCategory = SubCategoryList[j];
                                    var option = $("<option />");
                                    option.html(SubCategory.SubcategoryName);
                                    option.val(SubCategory.AutoId);
                                    ddlSubCategory.append(option);
                                }
                                ddlSubCategory.select2();
                                $("#ddlProduct").html('');
                                var ProductDtl = AllDropDownList.ProductList;
                                var ddlProduct = $("#ddlProduct");
                                $("#ddlProduct").attr('multiple', 'multiple')
                                $("#ddlProduct option:not(:first)").remove();
                                for (var i = 0; i < ProductDtl.length; i++) {
                                    var Product = ProductDtl[i];
                                    var option = $("<option />");
                                    option.html(Product.ProductName);
                                    option.val(Product.AutoId);
                                    ddlProduct.append(option);
                                }
                                $("#ddlProduct").select2({
                                    placeholder: 'All Product',
                                    allowClear: true
                                });
                            }
                            else {
                                $("#ddlProduct").html('');
                                $("#AllSubCategory").html('');
                                $("#AllSubCategory").append('<option>All Subcategory</option>');
                            }
                        }                     
                    }
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function BindProduct() {
   var BrandId = 0;
    $("#ddlBrand option:selected").each(function (i) {
        console.log(i)
        if (i == 0) {
            BrandId = $(this).val() + ',';

        } else {
            BrandId += $(this).val() + ',';
        }
    });
    if (BrandId == "0,") {
        BrandId = "0";
    }
    var data = {
        CategoryAutoId: $("#ddlAllCategory").val(),
        SubCategoryAutoId: $("#AllSubCategory").val(),
        BrandId: BrandId.toString()
    };
    $.ajax({
        type: "POST",
        url: "ProductWiseSalesReport.aspx/BindProduct",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        //data: "{'SubCategoryId':'" + $("#AllSubCategory").val() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") { 
                if (response.d != "false") {
                    if (response.d != "") {
                        var ProductList = $.parseJSON(response.d);
                        $("#ddlProduct").html('');
                        var ddlProduct = $("#ddlProduct");
                        $("#ddlProduct").attr('multiple', 'multiple')
                        $("#ddlProduct option:not(:first)").remove();
                        for (var i = 0; i < ProductList.length; i++) {
                            var Product = ProductList[i];
                            var option = $("<option />");
                            option.html(Product.ProductName);
                            option.val(Product.AutoId);
                            ddlProduct.append(option);
                        }
                        $("#ddlProduct").select2({
                            placeholder: 'All Product',
                            allowClear: true
                        });
                    }
                    else {
                        $("#ddlProduct").html('');
                    }
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function getReport(PageIndex) {
    var Product = "0", Brand = "0";
    if ($("#txtOrdCloseFromDate").val() != "" && $("#txtOrdCloseToDate").val() != "") {
        $("#ddlBrand option:selected").each(function (i) {
            if (i == 0) {
                Brand = $(this).val() + ',';
            } else {
                Brand += $(this).val() + ',';
            }
        });
        if ($("#ddlBrand").val() == "0") {
            Brand = "0"
        }
        $("#ddlProduct option:selected").each(function (i) {
            if (i == 0) {
                Product = $(this).val() + ',';
            } else {
                Product += $(this).val() + ',';
            }
        });
        if ($("#ddlProduct").val() == "0") {
            Product = "0"
        }

        var data = {
            BrandAutoIdStr: Brand,
            ProductAutoId: Product,
            SalesAutoId: $("#ddlAllPerson").val(),
            CustomerAutoId: $("#ddlAllCustomer").val(),
            CategoryAutoId: $("#ddlAllCategory").val(),
            SubCategoryAutoId: $("#AllSubCategory").val(),
            FromDate: $("#txtOrdCloseFromDate").val(),
            ToDate: $("#txtOrdCloseToDate").val(),
            SearchBy: $("#ddlSearchBy").val(),
            PageIndex: PageIndex,
            PageSize: $('#ddlPageSize').val(),
            CustomerType: $("#ddlCustomerType").val()
        };
        $.ajax({
            type: "POST",
            url: "ProductWiseSalesReport.aspx/GetReportDetail",
            data: JSON.stringify({ dataValue: JSON.stringify(data) }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: successgetReport,
            error: function (result) {
                alert(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
        $('.date').removeClass('border-warning');
    }
    else {
        toastr.error('Please fill date field.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $('.date').addClass('border-warning');
    }
}
function successgetReport(response) { 
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var ReportDetails = $(xmldoc).find("Table");
            var OTotal = $(xmldoc).find("Table2");
            $("#tblOrderList tbody tr").remove();
            var row = $("#tblOrderList thead tr:last-child").clone(true);
            var TotalPieceQty = 0, TotalBoxQty = 0, TotalCaseQty = 0, TotalAmount = 0.00, TotalCreditAmt = 0.00,
                R_TotalPieceQty = 0, R_TotalCaseQty = 0, R_TotalBoxQty = 0, reportLength = 0;
            reportLength = ReportDetails.length;
            if (ReportDetails.length > 0) {
                $("#EmptyTable").hide();
                var html = "", TotalPiece = 0, TotalCase = 0, TotalBox = 0, ItemQty = 0, ProductTotal = 0.00, ProductId = 0, CustomerAutoId = 0;
                var R_TotalPiece = 0, R_TotalCase = 0, R_TotalBox = 0;
                $.each(ReportDetails, function (index) {
                    ItemQty++;
                    if (ProductId != '0' && (ProductId != $(this).find("ProductId").text())) {
                        html += "<tr style='font-weight:700; background:oldlace;'>"
                        html += "<td colspan='8' style='text-align:right;color:#dd0b3c;background:#faf2f2;' > <b>Total</b></td >"
                        html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalPiece + "</b></td>"
                        html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + R_TotalPieceQty + "</b></td>"
                        html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalBox + "</b></td>"
                        html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + R_TotalBoxQty + "</b></td>"
                        html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalCase + "</b></td>"
                        html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + R_TotalCaseQty + "</b></td>"
                        html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'></td>"
                        html += "<td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + ProductTotal.toFixed(2) + "</b></td></tr > ";
                        $("#tblOrderList tbody").append(html);
                        html = ''; TotalPiece = 0; TotalCase = 0; TotalBox = 0; ProductTotal = 0.00; ItemQty = 0;
                        R_TotalPieceQty = 0; R_TotalCaseQty = 0; R_TotalBoxQty = 0;
                    }
                    TotalPieceQty = TotalPieceQty + parseInt($(this).find("TotalPiece").text());
                    TotalBoxQty = TotalBoxQty + parseInt($(this).find("TotalBox").text());
                    TotalCaseQty = TotalCaseQty + parseInt($(this).find("TotalCase").text());


                    TotalPiece = TotalPiece + parseInt($(this).find("TotalPiece").text());
                    TotalBox = TotalBox + parseInt($(this).find("TotalBox").text());
                    TotalCase = TotalCase + parseInt($(this).find("TotalCase").text());

                    R_TotalPiece = R_TotalPiece + parseInt($(this).find("R_TotalPiece").text());
                    R_TotalBox = R_TotalBox + parseInt($(this).find("R_TotalBox").text());
                    R_TotalCase = R_TotalCase + parseInt($(this).find("R_TotalCase").text());

                    R_TotalPieceQty = R_TotalPieceQty + parseInt($(this).find("R_TotalPiece").text());
                    R_TotalBoxQty = R_TotalBoxQty + parseInt($(this).find("R_TotalBox").text());
                    R_TotalCaseQty = R_TotalCaseQty + parseInt($(this).find("R_TotalCase").text());
                    $(".OrderDate", row).text($(this).find("OrderDate").text());
                    $(".ProductId", row).text($(this).find("ProductId").text());
                    $(".ProductName", row).text($(this).find("ProductName").text());
                    $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                    $(".OrderNo", row).text($(this).find("OrderNo").text());
                    $(".CustomerName", row).text($(this).find("CustomerName").text());
                    $(".BillingAddress", row).text($(this).find("BillingAddress").text());
                    $(".UnitPrice", row).text($(this).find("UnitPrice").text());
                    $(".TotalPieces", row).html($(this).find("TotalPiece").text());
                    $(".TotalBox", row).html($(this).find("TotalBox").text());
                    $(".TotalCase", row).html($(this).find("TotalCase").text());
                    $(".R_TotalPieces", row).html($(this).find("R_TotalPiece").text());
                    $(".R_TotalBox", row).html($(this).find("R_TotalBox").text());
                    $(".R_TotalCase", row).html($(this).find("R_TotalCase").text());
                    $(".CreditMemoDate", row).html($(this).find("CreditDate").text());
                    $(".CreditMemoNo", row).html($(this).find("CreditNo").text());

                    var ActualQty = (parseInt($(this).find("TotalPiece").text()) + parseInt($(this).find("TotalBox").text()) + parseInt($(this).find("TotalCase").text())) - (parseInt($(this).find("R_TotalPiece").text()) + parseInt($(this).find("R_TotalBox").text()) + parseInt($(this).find("R_TotalCase").text()));
                    ProductTotal = (ProductTotal + (parseFloat($(this).find("UnitPrice").text()) * ActualQty));
                    if ($(this).find("CreditNo").text() != "") {
                        $(".PayableAmount", row).html("<span>-" + parseFloat($(this).find("PayableAmount").text()) + "</span>");
                        TotalCreditAmt = TotalCreditAmt + (-1) * parseFloat($(this).find("PayableAmount").text());
                    }
                    else {
                        TotalAmount = TotalAmount + parseFloat($(this).find("PayableAmount").text());
                        $(".PayableAmount", row).html(parseFloat($(this).find("PayableAmount").text()));
                    }
                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);
                    ProductId = $(this).find("ProductId").text();
                    CustomerAutoId = $(this).find("CustomerAutoId").text();
                    ItemQty = $(this).find("ItemQty").text();
                });
                if (reportLength != 0) {
                    html += "<tr style='font-weight:700; background:oldlace;'>"
                    html += "<td colspan='8' style='text-align:right;color:#dd0b3c;background:#faf2f2;' > <b>Total</b></td >"
                    html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalPiece + "</b></td>"
                    html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + R_TotalPieceQty + "</b></td>"
                    html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalBox + "</b></td>"
                    html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + R_TotalBoxQty + "</b></td>"
                    html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalCase + "</b></td>"
                    html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + R_TotalCaseQty + "</b></td>"
                    html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'></td>"
                    html += "<td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + ProductTotal.toFixed(2) + "</b></td></tr > ";
                    $("#tblOrderList tbody").append(html);
                    row = $("#tblOrderList tbody tr:last").clone(true);
                }
                $("#tblOrderList tbody tr").show();
                $('#TotalPieceQty').html(parseFloat(TotalPieceQty).toFixed(0));
                $('#RTotalPieceQty').html(parseFloat(R_TotalPiece).toFixed(0));
                $('#TotalBoxQty').html(parseFloat(TotalBoxQty).toFixed(0));
                $('#RTotalBoxQty').html(parseFloat(R_TotalBox).toFixed(0));
                $('#TotalCaseQty').html(parseFloat(TotalCaseQty).toFixed(0));
                $('#RTotalCaseQty').html(parseFloat(R_TotalCase).toFixed(0));
                $('#AmtDue').html(parseFloat(TotalAmount + TotalCreditAmt).toFixed(2)); 
            } else {
                $('#TotalQty').html('0');
                $('#AmtDue').html('0.00');
                //$("#EmptyTable").show();
                $('#TotalCaseAVG').html('0.00');
                $('#TotalBoxAVG').html('0.00');
                $('#TotalPieceAVG').html('0.00');
                $('#TotalPieceQty').html('0');
                $('#TotalBoxQty').html('0');
                $('#TotalCaseQty').html('0');
                $('#RTotalPieceQty').html('0');
                $('#RTotalBoxQty').html('0');
                $('#RTotalCaseQty').html('0');
                $('#AmtDue').html('0');
            }
            $(OTotal).each(function () {
                $('#OverAllTotalPieceQty').html(parseInt($(this).find('TotalPiece').text()).toFixed(0));
                $('#R_OverAllTotalPieceQty').html(parseFloat($(this).find('R_TotalPiece').text()).toFixed(0));
                $('#OverAllTotalBoxQty').html(parseFloat($(this).find('TotalBox').text()).toFixed(0));
                $('#R_OverAllTotalBoxQty').html(parseFloat($(this).find('R_TotalBox').text()).toFixed(0));
                $('#OverAllTotalCaseQty').html(parseFloat($(this).find('TotalCase').text()).toFixed(0));
                $('#R_OverAllTotalCaseQty').html(parseFloat($(this).find('R_TotalCase').text()).toFixed(0));
                $('#AmtDuea').html(parseFloat(parseFloat($(this).find('PayableAmount').text()) - parseFloat($(this).find('TotalCreditAmt').text())).toFixed(2));
            });

            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}
function CreateTable(us) { 
    row1 = "";
    var image = $("#imgName").val();
    var Product = "0", Brand = "0";
    $("#ddlBrand option:selected").each(function (i) {
        if (i == 0) {
            Brand = $(this).val() + ',';
        } else {
            Brand += $(this).val() + ',';
        }
    });
    if ($("#ddlBrand").val() == "0") {
        Brand = "0"
    }
    $("#ddlProduct option:selected").each(function (i) {
        if (i == 0) {
            Product = $(this).val() + ',';
        } else {
            Product += $(this).val() + ',';
        }
    });
    if ($("#ddlProduct").val() == "0") {
        Product = "0"
    }
    var data = {
        BrandAutoIdStr: Brand,
        ProductAutoId: Product,
        SalesAutoId: $("#ddlAllPerson").val(),
        CustomerAutoId: $("#ddlAllCustomer").val(),
        CategoryAutoId: $("#ddlAllCategory").val(),
        SubCategoryAutoId: $("#AllSubCategory").val(),
        FromDate: $("#txtOrdCloseFromDate").val(),
        ToDate: $("#txtOrdCloseToDate").val(),
        CreditFromDate: $("#txtCreditMemoFromDate").val(),
        CreditToDate: $("#txtCreditMemoToDate").val(),
        SearchBy: $("#ddlSearchBy").val(),
        PageIndex: 1,
        PageSize: 0,
        CustomerType: $("#ddlCustomerType").val()
    };

    $.ajax({
        type: "POST",
        url: "ProductWiseSalesReport.aspx/GetReportDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ReportDetails = $(xmldoc).find("Table");
                    var printDate = $(xmldoc).find("Table1");
                    var OTotal = $(xmldoc).find("Table2");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    var TotalPieceQty = 0, TotalBoxQty = 0, TotalCaseQty = 0, TotalAmount = 0.00, TotalCreditAmt = 0.00,
                        R_TotalPieceQty = 0; R_TotalCaseQty = 0; R_TotalBoxQty = 0;
                    if (ReportDetails.length > 0) {
                        $("#EmptyTable").hide();
                        var html1 = "", TotalPiece = 0, TotalCase = 0, TotalBox = 0, ItemQty = 0, ProductTotal = 0.00, ProductId = 0, CustomerAutoId = 0;
                        var R_TotalPiece = 0, R_TotalCase = 0, R_TotalBox = 0,reportLength = 0;
                        reportLength = ReportDetails.length;
                        $.each(ReportDetails, function (index) {
                            ItemQty++;
                            if (ProductId != '0' && (ProductId != $(this).find("ProductId").text())) {
                                html = "<tr style='font - weight: 700; background: oldlace;'><td colspan='8' style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>Total</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalPiece + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + R_TotalPiece + "</b></td>      <td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalBox + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + R_TotalBox + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalCase + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + R_TotalCase + "</b></td><td style='color:#dd0b3c;background:#faf2f2;'></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + ProductTotal.toFixed(2) + "</b></td></tr>";
                                $("#PrintTable tbody").append(html);
                                TotalPiece = 0; TotalCase = 0; TotalBox = 0; ProductTotal = 0.00; ItemQty = 0;
                                R_TotalPiece = 0; R_TotalCase = 0; R_TotalBox = 0; html = "";
                            }
                            $(".OrderDates", row).text($(this).find("OrderDate").text());
                            $(".ProductIds", row).text($(this).find("ProductId").text());
                            $(".ProductNames", row).text($(this).find("ProductName").text());
                            $(".SalesPersons", row).text($(this).find("SalesPerson").text());

                            $(".OrderNos", row).text($(this).find("OrderNo").text());
                            $(".CustomerNames", row).text($(this).find("CustomerName").text());
                            $(".BillingAddresss", row).text($(this).find("BillingAddress").text());

                            $(".UnitPrices", row).text($(this).find("UnitPrice").text());
                            $(".TotalPiecess", row).html($(this).find("TotalPiece").text());
                            $(".TotalBoxs", row).html($(this).find("TotalBox").text());
                            $(".TotalCases", row).html($(this).find("TotalCase").text());
                            $(".R_TotalPiecess", row).html($(this).find("R_TotalPiece").text());
                            $(".R_TotalBoxs", row).html($(this).find("R_TotalBox").text());
                            $(".R_TotalCases", row).html($(this).find("R_TotalCase").text());

                            $(".CreditMemoDates", row).html($(this).find("CreditDate").text());
                            $(".CreditMemoNos", row).html($(this).find("CreditNo").text());

                            TotalPieceQty = TotalPieceQty + parseInt($(this).find("TotalPiece").text());
                            TotalBoxQty = TotalBoxQty + parseInt($(this).find("TotalBox").text());
                            TotalCaseQty = TotalCaseQty + parseInt($(this).find("TotalCase").text());

                            TotalAmount = TotalAmount + parseFloat($(this).find("PayableAmount").text());

                            TotalPiece = TotalPiece + parseInt($(this).find("TotalPiece").text());
                            TotalBox = TotalBox + parseInt($(this).find("TotalBox").text());
                            TotalCase = TotalCase + parseInt($(this).find("TotalCase").text());

                            R_TotalPieceQty = R_TotalPieceQty + parseInt($(this).find("R_TotalPiece").text());
                            R_TotalBoxQty = R_TotalBoxQty + parseInt($(this).find("R_TotalBox").text());
                            R_TotalCaseQty = R_TotalCaseQty + parseInt($(this).find("R_TotalCase").text());

                            R_TotalPiece = R_TotalPiece + parseInt($(this).find("R_TotalPiece").text());
                            R_TotalBox = R_TotalBox + parseInt($(this).find("R_TotalBox").text());
                            R_TotalCase = R_TotalCase + parseInt($(this).find("R_TotalCase").text());

                            var ActualQty = (parseInt($(this).find("TotalPiece").text()) + parseInt($(this).find("TotalBox").text()) + parseInt($(this).find("TotalCase").text())) - (parseInt($(this).find("R_TotalPiece").text()) + parseInt($(this).find("R_TotalBox").text()) + parseInt($(this).find("R_TotalCase").text()));
                            ProductTotal = (ProductTotal + (parseFloat($(this).find("UnitPrice").text()) * ActualQty));
                            if ($(this).find("CreditNo").text() != "") {
                                $(".PayableAmounts", row).html("<span>-" + parseFloat($(this).find("PayableAmount").text()).toFixed(2) + "</span>");
                                TotalCreditAmt = TotalCreditAmt + parseFloat($(this).find("PayableAmount").text());
                            }
                            else {
                                $(".PayableAmounts", row).html(parseFloat($(this).find("PayableAmount").text()).toFixed(2));
                            }
                            // $(".PayableAmounts", row).html(parseFloat($(this).find("PayableAmount").text()).toFixed(2));
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                            ProductId = $(this).find("ProductId").text();
                            CustomerAutoId = $(this).find("CustomerAutoId").text();
                            ItemQty = $(this).find("ItemQty").text();
                        });
                        if (reportLength != 0) {
                            html += "<tr style='font-weight:700; background:oldlace;'>"
                            html += "<td colspan='8' style='text-align:right;color:#dd0b3c;background:#faf2f2;' > <b>Total</b></td >"
                            html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalPiece + "</b></td>"
                            html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + R_TotalPiece + "</b></td>"
                            html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalBox + "</b></td>"
                            html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + R_TotalBox + "</b></td>"
                            html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + TotalCase + "</b></td>"
                            html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + R_TotalCase + "</b></td>"
                            html += "<td style='text-align:center;color:#dd0b3c;background:#faf2f2;'></td>"
                            html += "<td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + ProductTotal.toFixed(2) + "</b></td></tr > ";
                            $("#PrintTable tbody").append(html);
                            reportLength = 0;
                        }
                        $('#TotalPieceQtys').html(parseFloat(TotalPieceQty).toFixed(0));
                        $('#R_TotalPieceQtys').html(parseFloat(R_TotalPieceQty).toFixed(0));
                        $('#TotalBoxQtys').html(parseFloat(TotalBoxQty).toFixed(0));
                        $('#R_TotalBoxQtys').html(parseFloat(R_TotalBoxQty).toFixed(0));
                        $('#TotalCaseQtys').html(parseFloat(TotalCaseQty).toFixed(0));
                        $('#R_TotalCaseQtys').html(parseFloat(R_TotalCaseQty).toFixed(0));

                        $('#AmtDues').html(parseFloat(TotalAmount - TotalCreditAmt).toFixed(2));

                        if (TotalPieceQty > 0) {
                            $('#TotalPieceAVGs').html(parseFloat((TotalAmount - TotalCreditAmt) / (TotalPieceQty - R_TotalPieceQty)).toFixed(2));
                        } else {
                            $('#TotalPieceAVGs').html('0.00');
                        }
                        //if (TotalBoxQty > 0) {
                        //    $('#TotalBoxAVGs').html(parseFloat((TotalAmount - TotalCreditAmt) / (TotalBoxQty - R_TotalBoxQty)).toFixed(2));
                        //} else {
                        //    $('#TotalBoxAVGs').html('0.00');
                        //}
                        //if (TotalCaseQty > 0) {
                        //    $('#TotalCaseAVGs').html(parseFloat((TotalAmount - TotalCreditAmt) / (TotalCaseQty - R_TotalCaseQty)).toFixed(2));
                        //} else {
                        //    $('#TotalCaseAVGs').html('0.00');
                        //}
                        $(OTotal).each(function () {
                            $('#PrintTotalPieceQty').html(parseInt($(this).find('TotalPiece').text()).toFixed(0));
                            $('#R_PrintTotalPieceQty').html(parseFloat($(this).find('R_TotalPiece').text()).toFixed(0));
                            $('#PrintTotalBoxQty').html(parseFloat($(this).find('TotalBox').text()).toFixed(0));
                            $('#R_PrintTotalBoxQty').html(parseFloat($(this).find('R_TotalBox').text()).toFixed(0));
                            $('#PrintTotalCaseQty').html(parseFloat($(this).find('TotalCase').text()).toFixed(0));
                            $('#R_PrintTotalCaseQty').html(parseFloat($(this).find('R_TotalCase').text()).toFixed(0));
                            $('#AmtDueaa').html(parseFloat(parseFloat($(this).find('PayableAmount').text()) - parseFloat($(this).find('TotalCreditAmt').text())).toFixed(2));//TotalCreditAmt
                        });
                        if (us == 1) {

                            var image = $("#imgName").val();
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                            $("#PrintDate").text("Print Date :" + $(printDate).find("PrintDate").text());
                            $("#PrintLogo").attr("src", "/Img/logo/" + image);
                            if ($("#txtOrdCloseFromDate").val() != "") {
                                $("#DateRange").text("Date Range: " + $("#txtOrdCloseFromDate").val() + " To " + $("#txtOrdCloseToDate").val());
                            }
                            else if ($("#txtCreditMemoFromDate").val() != "") {
                                $("#DateRange").text("Date Range: " + $("#txtCreditMemoFromDate").val() + " To " + $("#txtCreditMemoToDate").val());
                            }
                            else {
                                $("#DateRange").hide();
                            }

                            mywindow.document.write($(PrintTable1).clone().html());
                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        }
                        if (us == 2) {
                            $("#PrintTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "Sales Report By Product Detail",
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });
                        }
                    }
                    else {
                        $('#TotalPieceQtys').html('0.00');
                        $('#R_TotalPieceQtys').html('0.00');
                        $('#TotalBoxQtys').html('0.00');
                        $('#R_TotalBoxQtys').html('0.00');
                        $('#TotalCaseQtys').html('0.00');
                        $('#R_TotalCaseQtys').html('0.00');
                        $('#TotalPieceAVGs').html('0.00');
                        $('#TotalBoxAVGs').html('0.00');
                        $('#TotalCaseAVGs').html('0.00');
                    }
                }
            }
            else {
                location.href = "/";
            }

        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function exportToExcel() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
//--------------------------------------------------Open ToolTip-----------------------------------------------
function OpenPopUp() {
    toastr.error('Credit Memo Status: Approved   Order Status :closed', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    //swal("Alert", "Please fill currency denomination.", "error");

}



