﻿$(document).ready(function () {
    $('#txtSFromDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        min: new Date("01/01/2019"),
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    BindSalesPerson();
});
function setdatevalidation(val) {
    var fdate = $("#txtSFromDate").val();
    var tdate = $("#txtSToDate").val();
    if (val == 1) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSToDate").val(fdate);
        }
    }
    else if (val == 2) {
        if (fdate == '' || new Date(fdate) > new Date(tdate)) {
            $("#txtSFromDate").val(tdate);
        }
    }
}

function Pagevalue(e) {
    getReport(parseInt($(e).attr("page")));
};

/*---------------------------------------------------Bind Customer-----------------------------------------------------------*/
function BindSalesPerson() {
    $.ajax({
        type: "POST",
        url: "Commission_Summary_Report.aspx/BindSalesPerson",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var getData = $.parseJSON(response.d);

                    $("#ddlAllPerson option:not(:first)").remove();
                    $.each(getData, function (index, item) {
                        $("#ddlAllPerson").append("<option value='" + getData[index].SID + "'>" + getData[index].SP + "</option>");
                    });
                    $("#ddlAllPerson").select2();
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function getReport(PageIndex) {
    var data = {
        SalesAutoId: $("#ddlAllPerson").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: PageIndex,
        PageSize: $('#ddlPageSize').val()
    };
    $.ajax({
        type: "POST",
        url: "Commission_Summary_Report.aspx/GetReportDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successgetReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successgetReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var ReportDetails = $(xmldoc).find("Table");
            var OrderTotal = $(xmldoc).find("Table2");
            $("#tblOrderList tbody tr").remove();
            var row = $("#tblOrderList thead tr").clone(true);
            var TotalSale = 0, NoofProduct = 0, SPCommAmt = 0;
            var T_TotalSale = 0, T_NoofProduct = 0, T_SPCommAmt = 0;
            if (ReportDetails.length > 0) {
                $("#EmptyTable").hide();
                var html = "", SalesPersonAutoId = 0, TotalPiece = 0;
                var SalesPerson = '';
                $.each(ReportDetails, function (index) {
                    if (SalesPersonAutoId != '0' && (SalesPersonAutoId != $(this).find("SalesPersonAutoId").text())) {
                        html = "<tr style='font-weight: 700; background: oldlace;'><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + SalesPerson + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                            + NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(SPCommAmt).toFixed(2) + "</b></td></tr>";
                        $("#tblOrderList tbody").append(html);
                        TotalSale = 0, NoofProduct = 0, SPCommAmt = 0;
                        SalesPerson = '';
                    }
                    $(".NoofProduct", row).text($(this).find("NoofProduct").text());
                    $(".CommCode", row).text($(this).find("CommCode").text());
                    $(".TotalSale", row).text(parseFloat($(this).find("TotalSale").text()).toFixed(2));
                    $(".Commission", row).text(parseFloat($(this).find("SPCommAmt").text()).toFixed(2));
                    TotalSale = TotalSale + parseFloat($(this).find("TotalSale").text());
                    NoofProduct = NoofProduct + parseInt($(this).find("NoofProduct").text());
                    SPCommAmt = SPCommAmt + parseFloat($(this).find("SPCommAmt").text());

                    T_TotalSale = T_TotalSale + parseFloat($(this).find("TotalSale").text());
                    T_NoofProduct = T_NoofProduct + parseInt($(this).find("NoofProduct").text());
                    T_SPCommAmt = T_SPCommAmt + parseFloat($(this).find("SPCommAmt").text());

                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);
                    SalesPersonAutoId = $(this).find("SalesPersonAutoId").text();
                    SalesPerson = $(this).find("SP").text();
                });
                if (SalesPerson != '') {
                    html = "<tr style='font-weight: 700; background: oldlace;background:#faf2f2;'><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + SalesPerson + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                        + NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(SPCommAmt).toFixed(2) + "</b></td></tr>";
                    $("#tblOrderList tbody").append(html);
                }
                $('#T_NoofProduct').html(parseFloat(T_NoofProduct).toFixed(0));
                $('#T_TotalSale').html(parseFloat(T_TotalSale).toFixed(2));
                $('#T_SPCommAmt').html(parseFloat(T_SPCommAmt).toFixed(2));

            } else {
                $('#T_NoofProduct').html(parseFloat(T_NoofProduct).toFixed(0));
                $('#T_TotalSale').html(parseFloat(T_TotalSale).toFixed(2));
                $('#T_SPCommAmt').html(parseFloat(T_SPCommAmt).toFixed(2));
                $('#TotalQty').html('0');
                $('#AmtDue').html('0.00');
              //  $("#EmptyTable").show();
            }
            $(OrderTotal).each(function () {
                $('#T_NoofProducts').html($(this).find('NoofOrder').text());
                $('#T_TotalSales').html(parseFloat($(this).find('TotalSale').text()).toFixed(2));
                $('#T_SPCommAmts').html(parseFloat($(this).find('SPCommAmt').text()).toFixed(2));
            });

            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
            if (Number($("#ddlPageSize").val()) == '0') {
                $("#trTotal").hide();
            }
            else if (Number(pager.find("RecordCount").text()) < Number(pager.find("PageSize").text())) {
                $("#trTotal").hide();
            }
            else {
                $("#trTotal").show();
            }
        }
    }
    else {
        location.href = "/";
    }
}

$("#btnSearch").click(function () {
    getReport(1);
})

function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        SalesAutoId: $("#ddlAllPerson").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: 1,
        PageSize: 0
    };
    $.ajax({
        type: "POST",
        url: "Commission_Summary_Report.aspx/GetReportDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ReportDetails = $(xmldoc).find("Table");
                    var PrintDate = $(xmldoc).find("Table1");
                    var OrderTotal = $(xmldoc).find("Table2");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    var TotalSale = 0, NoofProduct = 0, SPCommAmt = 0;
                    var T_TotalSale = 0, T_NoofProduct = 0, T_SPCommAmt = 0;
                    if (ReportDetails.length > 0) {
                        var html = "", SalesPersonAutoId = 0, TotalPiece = 0;
                        var SalesPerson = '';
                        $.each(ReportDetails, function (index) {
                            if (SalesPersonAutoId != '0' && (SalesPersonAutoId != $(this).find("SalesPersonAutoId").text())) {
                                html = "<tr style='font-weight: 700; background: oldlace;'><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + SalesPerson + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(SPCommAmt).toFixed(2) + "</b></td></tr>";
                                $("#PrintTable tbody").append(html);
                                TotalSale = 0, NoofProduct = 0, SPCommAmt = 0;
                                SalesPerson = '';
                            }
                            $(".PNoofProduct", row).text($(this).find("NoofProduct").text());
                            $(".PCommCode", row).text($(this).find("CommCode").text());
                            $(".PTotalSale", row).text(parseFloat($(this).find("TotalSale").text()).toFixed(2));
                            $(".PCommission", row).text(parseFloat($(this).find("SPCommAmt").text()).toFixed(2));
                            TotalSale = TotalSale + parseFloat($(this).find("TotalSale").text());
                            NoofProduct = NoofProduct + parseInt($(this).find("NoofProduct").text());
                            SPCommAmt = SPCommAmt + parseFloat($(this).find("SPCommAmt").text());

                            T_TotalSale = T_TotalSale + parseFloat($(this).find("TotalSale").text());
                            T_NoofProduct = T_NoofProduct + parseInt($(this).find("NoofProduct").text());
                            T_SPCommAmt = T_SPCommAmt + parseFloat($(this).find("SPCommAmt").text());

                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                            SalesPersonAutoId = $(this).find("SalesPersonAutoId").text();
                            SalesPerson = $(this).find("SP").text();
                        });
                        if (SalesPerson != '') {
                            html = "<tr style='font-weight: 700; background: oldlace;'><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + SalesPerson + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                + NoofProduct + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(TotalSale).toFixed(2) + "</b></td><td style='text-align:right;color:#dd0b3c;background:#faf2f2;'><b>" + parseFloat(SPCommAmt).toFixed(2) + "</b></td></tr>";
                            $("#PrintTable tbody").append(html);
                        }
                        $('#PT_NoofProduct').html(parseFloat(T_NoofProduct).toFixed(0));
                        $('#PT_TotalSale').html(parseFloat(T_TotalSale).toFixed(2));
                        $('#PT_SPCommAmt').html(parseFloat(T_SPCommAmt).toFixed(2));

                    } else {
                        $('#PT_NoofProduct').html(parseFloat(T_NoofProduct).toFixed(0));
                        $('#PT_TotalSale').html(parseFloat(T_TotalSale).toFixed(2));
                        $('#PT_SPCommAmt').html(parseFloat(T_SPCommAmt).toFixed(2));
                        $('#PTotalQty').html('0');
                        $('#PAmtDue').html('0.00');
                    }

                    $(OrderTotal).each(function () {
                        $('#PT_NoofProduct').html($(this).find('NoofOrder').text());
                        $('#PT_TotalSale').html(parseFloat($(this).find('TotalSale').text()).toFixed(2));
                        $('#PT_SPCommAmt').html(parseFloat($(this).find('SPCommAmt').text()).toFixed(2));
                    });

                    if (us == 1) {
                        
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date : " + $(PrintDate).find('PrintDate').text());
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtSFromDate").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        //mywindow.document.write(divToPrint.outerHTML);

                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "EmpCommissionSummaryReport",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
    CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}


