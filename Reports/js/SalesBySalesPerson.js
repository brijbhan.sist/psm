﻿var row1 = "";
$(document).ready(function () {
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    BindSalesPerson();
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
});

function Pagevalue(e) {
    BindReport(parseInt($(e).attr("page")));
};

/*---------------------------------------------------Bind Category-----------------------------------------------------------*/
function BindSalesPerson() {
    $.ajax({
        type: "POST",
        url: "SalesBySalesPerson.aspx/BindSalesPerson",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);

                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(getData, function (index, item) {
                    $("#ddlSalesPerson").append("<option value='" + getData[index].SId + "'>" + getData[index].SP + "</option>");                   
                });
                $("#ddlSalesPerson").select2({
                    placeholder: 'All Sales Person',
                    allowClear: true
                });

            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}


/*-------------------------------------------------------Get Bar Code Report----------------------------------------------------------*/
function BindReport(PageIndex) {
    var SalesPersons = 0;
    $("#ddlSalesPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPersons = $(this).val() + ',';
        } else {
            SalesPersons += $(this).val() + ',';
        }
    });
    var data = {
        SalesPerson: SalesPersons,
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageSize: $("#ddlPaging").val() || 10,
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "SalesBySalesPerson.aspx/SaleBySalesPersonDetail",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successBindSalesPersonReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successBindSalesPersonReport(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var OrderTotal = $(xmldoc).find("Table");
            var OTotal = $(xmldoc).find("Table2");
            $("#tblOrderList tbody tr").remove();
            var row = $("#tblOrderList thead tr").clone(true);
            var GrandAmount = 0.00, NoOfOrders = 0.00, NoofPOSOrders = 0.00, POSGrandAmount = 0.00;
            if (OrderTotal.length > 0) {
                $("#EmptyTable").hide();

                $.each(OrderTotal, function () {

                    $(".EmployeeName", row).text($(this).find("EmployeeName").text());
                    $(".NoOfOrders", row).text($(this).find("NoOfOrders").text());
                    $(".TotalOrderAmount", row).text(parseFloat($(this).find("GrandTotal").text()).toFixed(2));
                    $(".NoofPOSOrders", row).text($(this).find("NoofPOSOrders").text());
                    $(".POSTotalAmount", row).text(parseFloat($(this).find("POSTotalAmount").text()).toFixed(2));
                    $(".TotalOrders", row).text(parseInt($(this).find("NoOfOrders").text()) + parseInt($(this).find("NoofPOSOrders").text()));
                    $(".OrderTotal", row).text(parseFloat(parseFloat($(this).find("GrandTotal").text()) + parseFloat($(this).find("POSTotalAmount").text())).toFixed(2));

                    NoOfOrders += parseInt($(this).find("NoOfOrders").text());
                    NoofPOSOrders += parseInt($(this).find("NoofPOSOrders").text());
                    GrandAmount += parseFloat($(this).find("GrandTotal").text());
                    POSGrandAmount += parseFloat($(this).find("POSTotalAmount").text());
                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);
                });
                $('#AVGPiece').html(parseFloat((GrandAmount + POSGrandAmount) / (NoOfOrders + NoofPOSOrders)).toFixed(2) || 0.00);
                if (NoOfOrders == 0) {
                    $('#avgSales').html('0.00');
                }
                else {
                    $('#avgSales').html(parseFloat(GrandAmount / NoOfOrders).toFixed(2) || 0.00);
                }
                if (NoofPOSOrders == 0) {
                    $('#avgPOS').html('0.00');
                }
                else {
                    $('#avgPOS').html(parseFloat(POSGrandAmount / NoofPOSOrders).toFixed(2) || 0.00);

                }
              
            } else {
                $('#avgSales').html('0.00');
                $('#avgPOS').html('0.00');
                $('#AVGPiece').html('0.00');
            }

            $('#GrandAmount').html(parseFloat(GrandAmount).toFixed(2));
            $('#POSGrandAmount').html(parseFloat(POSGrandAmount).toFixed(2));
            $('#NoOfOrders').html(parseFloat(NoOfOrders).toFixed());
            $('#NoofPOSOrders').html(parseFloat(NoofPOSOrders).toFixed());
            $('#AllOrders').html(parseFloat(NoOfOrders + NoofPOSOrders).toFixed());
            $('#AllGrandAmount').html(parseFloat(GrandAmount + POSGrandAmount).toFixed(2));

            $(OTotal).each(function () {
                var TOrders = (parseFloat($(this).find("GrandTotal").text())) + (parseFloat($(this).find("POSTotalAmount").text()));

                $('#GrandAmounta').html(parseFloat($(this).find("GrandTotal").text()).toFixed(2));
                $('#POSGrandAmounta').html(parseFloat($(this).find("POSTotalAmount").text()).toFixed(2));
                $('#NoOfOrdersa').html(parseFloat($(this).find("NoOfOrders").text()).toFixed());
                $('#NoofPOSOrdersa').html(parseFloat($(this).find("NoofPOSOrders").text()).toFixed(0));
                $('#AllOrdersa').html((parseInt($(this).find("NoOfOrders").text())) + (parseInt($(this).find("NoofPOSOrders").text())));
                $('#AllGrandAmounta').html(TOrders.toFixed(2));
            });

            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}



function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var SalesPersons = 0;
    $("#ddlSalesPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPersons = $(this).val() + ',';
        } else {
            SalesPersons += $(this).val() + ',';
        }
    });
    var data = {
        SalesPerson: SalesPersons,
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "SalesBySalesPerson.aspx/SaleBySalesPersonDetail",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ReportDetails = $(xmldoc).find("Table");
                    var PrintDate = $(xmldoc).find("Table1");
                    var OTotal = $(xmldoc).find("Table2");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    var GrandAmount = 0.00, NoOfOrders = 0.00, NoofPOSOrders = 0.00, POSGrandAmount = 0.00;
                    if (ReportDetails.length > 0) {
                        $("#EmptyTable").hide();
                        var html1 = "", TotalPiece = 0, TotalCase = 0, TotalBox = 0, ItemQty = 0, ProductTotal = 0.00, ProductId = 0, CustomerAutoId = 0;
                        var R_TotalPiece = 0, R_TotalCase = 0, R_TotalBox = 0;
                        $.each(ReportDetails, function (index) {

                            $(".EmployeeNames", row).text($(this).find("EmployeeName").text());
                            $(".NoOfOrderss", row).text($(this).find("NoOfOrders").text());
                            $(".TotalOrderAmounts", row).text(parseFloat($(this).find("GrandTotal").text()).toFixed(2));
                            $(".NoofPOSOrderss", row).text($(this).find("NoofPOSOrders").text());
                            $(".POSTotalAmounts", row).text(parseFloat($(this).find("POSTotalAmount").text()).toFixed(2));
                            $(".TotalOrderss", row).text(parseInt($(this).find("NoOfOrders").text()) + parseInt($(this).find("NoofPOSOrders").text()));
                            $(".OrderTotals", row).text(parseFloat(parseFloat($(this).find("GrandTotal").text()) + parseFloat($(this).find("POSTotalAmount").text())).toFixed(2));


                            NoOfOrders += parseInt($(this).find("NoOfOrders").text());
                            NoofPOSOrders += parseInt($(this).find("NoofPOSOrders").text());
                            GrandAmount += parseFloat($(this).find("GrandTotal").text());
                            POSGrandAmount += parseFloat($(this).find("POSTotalAmount").text());

                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                        });
                        $('#avgSalesP').html(parseFloat(GrandAmount / NoOfOrders).toFixed(2)||0.00);
                        $('#avgPOSP').html(parseFloat(POSGrandAmount / NoofPOSOrders).toFixed(2)||0.00);
                        $('#AVGPieceP').html(parseFloat((GrandAmount + POSGrandAmount) / (NoOfOrders + NoofPOSOrders)).toFixed(2)||0.00);

                        $('#GrandAmountd').html(parseFloat(GrandAmount).toFixed(2));
                        $('#POSGrandAmountd').html(parseFloat(POSGrandAmount).toFixed(2));
                        $('#NoOfOrdersd').html(parseFloat(NoOfOrders).toFixed());
                        $('#NoofPOSOrdersd').html(parseFloat(NoofPOSOrders).toFixed());
                        $('#AllOrdersd').html(parseFloat(NoOfOrders + NoofPOSOrders).toFixed());
                        $('#AllGrandAmountd').html(parseFloat(GrandAmount + POSGrandAmount).toFixed(2));
                        if (us == 1) {
                            var image = $("#imgName").val();
                            var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                            mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                            $("#PrintDate").text("Print Date :" + PrintDate.find('PrintDate').text());
                            $("#PrintLogo").attr("src", "/Img/logo/" + image);
                            if ($("#txtSFromDate").val() != "") {
                                $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                            }
                            mywindow.document.write($(PrintTable1).clone().html());
                            //mywindow.document.write(divToPrint.outerHTML);

                            mywindow.document.write('</body></html>');
                            setTimeout(function () {
                                mywindow.print();
                            }, 2000);
                        }
                        if (us == 2) {
                            $("#PrintTable").table2excel({
                                exclude: ".noExl",
                                name: "Excel Document Name",
                                filename: "Sale Report By sales Person",
                                fileext: ".xls",
                                exclude_img: true,
                                exclude_links: true,
                                exclude_inputs: true
                            });
                        }
                    } else {
                        $('#avgSalesP').html('0.00');
                        $('#avgPOSP').html('0.00');
                        $('#AVGPieceP').html('0.00');
                    }
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
        CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}



