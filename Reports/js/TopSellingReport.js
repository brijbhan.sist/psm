﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtSFromDate").val(month + '/' + day + '/' + year);
    $("#txtSToDate").val(month + '/' + day + '/' + year);
    BindSalesPerson();
});

/*---------------------------------------------------Bind SalesPerson and Customer Type-----------------------------------------------------------*/
function BindSalesPerson() {
    $.ajax({
        type: "POST",
        url: "TopSellingReport.aspx/BindSalesPerson",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        asyn: false,
        casshe: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
            } else {
                var getData = $.parseJSON(response.d);
                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(getData[0].SPName, function (index, item) {
                    $("#ddlSalesPerson").append("<option value='" + item.SId + "'>" + item.SP + "</option>");
                });
                $("#ddlSalesPerson").attr('multiple', 'multiple');
                $("#ddlSalesPerson").select2({
                    placeholder: 'All Sales Person',
                    allowClear: true
                });
                $("#ddlCustomerType option:not(:first)").remove();
                
                $.each(getData[0].CType, function (index, item) {
                    console.log[item.CID, item.CUN]
                    $("#ddlCustomerType").append("<option value='" + item.CID + "'>" + item.CUN + "</option>");
                });
                $("#ddlCustomerType").select2

            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function Pagevalue(e) {
    TopSellingReport(parseInt($(e).attr("page")));
};

function TopSellingReport(PageIndex) {

    var SalesPersons = "0";
    $("#ddlSalesPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPersons = $(this).val() + ',';
        } else {
            SalesPersons += $(this).val() + ',';
        }
    });
    var data = {
        Type: $("#ddlType").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageSize: $("#ddlCustomerPageSize").val(),
        SalesPersons: SalesPersons,
        CustomerType: $("#ddlCustomerType").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "TopSellingReport.aspx/TopsellingReport",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: OpenTopsellingReport,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function OpenTopsellingReport(response) {

    if (response.d != 'Session Expired') {
        if (response.d != "false") {
            var xmldoc = $.parseXML(response.d);
            var Sellingreport = $(xmldoc).find("Table");
            var OrderTotal = $(xmldoc).find("Table2");
            $("#tblOrderList tbody tr").remove();
            var TotalPiece = 0; TotalSaleAmt = 0;
            var row = $("#tblOrderList thead tr").clone(true);
            if (Sellingreport.length > 0) {
                $("#EmptyTable").hide();
                $.each(Sellingreport, function () {
                    $(".Sr", row).text($(this).find("RowNumber").text());
                    $(".ProductId", row).text($(this).find("ProductId").text());
                    $(".ProductName", row).text($(this).find("ProductName").text());
                    $(".Unit", row).text($(this).find("UnitType").text());
                    $(".DefaultUnitCount", row).text($(this).find("DEFAULT_UNIT_COUNT").text());
                    $(".TotalPieces", row).text($(this).find("TOTAL_PICES").text());
                    $(".NetSales", row).text($(this).find("NET_SELES").text());
                    $("#tblOrderList tbody").append(row);
                    row = $("#tblOrderList tbody tr:last").clone(true);

                    TotalPiece = TotalPiece + parseInt($(this).find("TOTAL_PICES").text());
                    TotalSaleAmt = TotalSaleAmt + parseFloat($(this).find("NET_SELES").text());
                });
                $('#TotalPiece').html(TotalPiece.toFixed(0));
                $('#TotalSaleAmt').html(TotalSaleAmt.toFixed(2));

            } else {
                //$("#EmptyTable").show();
                $('#TotalPiece').html('0');
                $('#TotalSaleAmt').html('0.00');
                $('#TotalPieces').html('0');
                $('#TotalSaleAmts').html('0.00');
            }
            $(OrderTotal).each(function () {
                $('#TotalPieces').html(parseFloat($(this).find('TOTAL_PICES').text()).toFixed(0));
                $('#TotalSaleAmts').html(parseFloat($(this).find('NET_SELES').text()).toFixed(2));
            });
            var pager = $(xmldoc).find("Table1");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    } else {
        location.href = "/";
    }
}


function PrintElem() {
    if ($('#tblOrderList tbody tr').length > 0) {
    //var image = $("#imgName").val();
    var divToPrint = document.getElementById("tblOrderList");
    //var mywindow = window.open('', 'mywindow', 'height=400,width=600');
    //mywindow.document.write('<html><head><style>#tblOrderList tbody tr .price{text-align:right;}#tblOrderList {border-collapse: collapse;width: 100%;}#tblOrderList td, #RptTable th {border: 1px solid black;padding: 8px;}#tblOrderList tr:nth-child(even){background-color: #f2f2f2;}#tblOrderList thead {padding-top: 12px;padding-bottom: 12px;background-color:#C8E8F9;color:black;}.text-right{text-align:right;}.text-left{text-align:left;}.text-center{text-align:center;}</style>');
    //mywindow.document.write('</head><body>');
    //if ($("#txtSFromDate").val() != "") {
    //    mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;'><img src='/Img/logo/" + image + "' style='float:left;' height='70px' width='140px'/><h3 style='float:center;'>Top Selling Report</h3><h4 style='float:right;margin-right:10px;'>Date: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val() + "</h4></div>")

    //}
    //else {
    //    debugger
    //    mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;'><img src='/Img/logo/" + image + "' style='float:left;' height='70px' width='140px'/><h3 style='float:center;'>Top Selling Report</h3><h4 style='float:right;margin-right:10px;'>Date: " + (new Date()).format("MM/dd/yyyy") + "</h4></div>")
    //}
    //mywindow.document.write(divToPrint.outerHTML);
    //mywindow.document.write('</body></html>');
    //mywindow.print();
   
    var image = $("#imgName").val();
    var mywindow = window.open('', 'mywindow', 'height=400,width=600');
    if ($("#txtSFromDate").val() != "") {
        mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Top Selling Report</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + (new Date()).format("MM/dd/yyyy hh:mm tt") + "</span><br/><span class='DateRangeCSS' style='float:center;margin-top: 0.5%; font-size: 9px; color: black; font-weight: bold;'>Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val() + "</span><br/></div>")

    }
    else {
 
        mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;height:40px'><img src='/Img/logo/" + image + "' style='float:left;' height='40px' width='140px'/><span style='float:center;margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;'>Top Selling Report</span><span style='float:right;margin-right:10px;line-height: 3; font-size: 12px; color: black; font-weight: bold;'>Date: " + (new Date()).format("MM/dd/yyyy hh:mm tt") + "</span></div>")
    }
    //mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
    mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/app-assets/css/custom.css"/></head><body>');
    $("#PrintDate").text("Print Date :" + (new Date()).format("MM/dd/yyyy hh:mm tt"));
    $("#PrintLogo").attr("src", "/Img/logo/" + image);
        //mywindow.document.write($(tblOrderList).clone().html());
    $("#tblOrderList").addClass("PrintMyTableHeader");
   
    //$('#tblOrderList tfoot tr:last').remove();
    mywindow.document.write(divToPrint.outerHTML);

    mywindow.document.write('</body></html>');
    setTimeout(function () {
        mywindow.print();
    }, 2000);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    $("#tblOrderList").removeClass("PrintMyTableHeader");

}
/*---------------Export To Excel---------------------------*/
function ExportReport() {
    if ($('#tblOrderList tbody tr').length > 0) {
    $("#tblOrderList").table2excel({
        exclude: ".noExl",
        name: "Excel Document Name",
        filename: "TopSellingReport",
        fileext: ".xls",
        exclude_img: true,
        exclude_links: true,
        exclude_inputs: true
    });
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
};