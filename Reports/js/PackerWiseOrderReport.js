﻿$(document).ready(function () {

    Bindpacker();
    $('#txtDateFrom').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    $('#txtDateTo').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    $("#txtDateFrom").val(month + '/' + day + '/' + year);
    $("#txtDateTo").val(month + '/' + day + '/' + year);
});
function Bindpacker() {
    $.ajax({
        type: "POST",
        url: "WebAPI/WPackerWiseOrderReport.asmx/Bindpacker",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var GetCategory = $(xmldoc).find("Table");
                    $("#ddlPackerName option:not(:first)").remove();
                    $.each(GetCategory, function () {
                        $("#ddlPackerName").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("PackerName").text() + "</option>");
                    });

                    $("#ddlPackerName").select2()
                }
            }
            else {
                location.href = "/";
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function Pagevalue(e) {
    GetPackerOrderReport(parseInt($(e).attr("page")));
}
$("#ddlPageSize").change(function () {
    GetPackerOrderReport(1);
})
$("#btnSearch").click(function () {
    GetPackerOrderReport(1);
});
function GetPackerOrderReport(PageIndex) {
    var data = {
        FromDate: $("#txtDateFrom").val(),
        ToDate: $("#txtDateTo").val(),
        PackerAutoId: $("#ddlPackerName").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/WPackerWiseOrderReport.asmx/GetProductWiseCommissionList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: successGetRouteList,
        error: function (result) {
            alert(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function successGetRouteList(response) {
    if (response.d != "Session Expired") {
        if (response.d != "false") {

            var xmldoc = $.parseXML(response.d);
            var RouteList = $(xmldoc).find("Table1");
            var OrderTotal = $(xmldoc).find("Table2");

            $("#tblProductCatelogReport tbody tr").remove();
            var row = $("#tblProductCatelogReport thead tr").clone(true);
            if (RouteList.length > 0) {
                $("#EmptyTable").hide();
                var html = "", PackerAutoId = 0, PackerName = '';
                var NoofItems = 0, NoofPackedItems = 0, ManuallyScanned = 0;
                var NoofItemss = 0, NoofPackedItemss = 0, ManuallyScanneds = 0;
                $("#tblProductCatelogReport thead tr").show();
                $.each(RouteList, function (index) {
                    if (PackerAutoId != '0' && (PackerAutoId != $(this).find("PackerAutoId").text())) {
                        html = "<tr style='font-weight: 700; background: oldlace;'><td style='text-align:center;color:#dd0b3c;background:#faf2f2;' colspan='7'><b>" + PackerName + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                            + NoofItems + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + NoofPackedItems + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + ManuallyScanned + "</b></td></tr>";
                        $("#tblProductCatelogReport tbody").append(html);
                        NoofItems = 0;
                        NoofPackedItems = 0;
                        ManuallyScanned = 0;
                    }
                    $(".WarehouseManager", row).text($(this).find("WarehouseManager").text());
                    $(".PackerName", row).text($(this).find("PackerName").text());
                    $(".CustomerName", row).text($(this).find("CustomerName").text());
                    $(".OrderNo", row).text($(this).find("OrderNo").text());
                    $(".OrderDate", row).text($(this).find("OrderDate").text());
                    $(".PackerAssignDate", row).text($(this).find("PackerAssignDate").text());
                    $(".PackingDate", row).text($(this).find("PackingDate").text());
                    $(".NoofItems", row).text($(this).find("NoofItems").text());
                    $(".NoofPackedItems", row).text($(this).find("NoofPackedItems").text());
                    $(".ManuallyScanned", row).text($(this).find("ManuallyScanned").text());

                    NoofItems += parseInt($(this).find("NoofItems").text());
                    NoofPackedItems += parseInt($(this).find("NoofPackedItems").text());
                    ManuallyScanned += parseInt($(this).find("ManuallyScanned").text());
                    $("#tblProductCatelogReport tbody").append(row);
                    row = $("#tblProductCatelogReport tbody tr:last").clone(true);
                    PackerAutoId = $(this).find("PackerAutoId").text();
                    PackerName = $(this).find("PackerName").text();

                    NoofItemss += parseInt($(this).find("NoofItems").text());
                    NoofPackedItemss += parseInt($(this).find("NoofPackedItems").text());
                    ManuallyScanneds += parseInt($(this).find("ManuallyScanned").text());

                });

               
                if (PackerAutoId > 0) {
                    html = "<tr style='font-weight: 700; background: oldlace;'><td style='text-align:center;color:#dd0b3c;background:#faf2f2;' colspan='7'><b>" + PackerName + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                        + NoofItems + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + NoofPackedItems + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + ManuallyScanned + "</b></td></tr>";
                    $("#tblProductCatelogReport tbody").append(html);
                    NoofItems = 0;
                    NoofPackedItems = 0;
                    ManuallyScanned = 0;
                }
                $('#NoofItemsa').html(parseInt(NoofItemss));
                $('#NoofPackedItemsa').html(parseInt(NoofPackedItemss));
                $('#ManuallyScanneda').html(parseInt(ManuallyScanneds));
            } else {
                $('#TotalQty').html('0');
                $('#AmtDue').html('0.00');
                $('#NoofItemsa').html('0');
                $('#NoofPackedItemsa').html('0');
                $('#ManuallyScanneda').html('0');
                //  $("#EmptyTable").show();
                $('#NoofItemsas').html('0');
                $('#NoofPackedItemsas').html('0');
                $('#ManuallyScannedas').html('0');
            }

            $(OrderTotal).each(function () {
                $('#NoofItemsas').html(parseInt($(this).find('NoofItems').text()));
                $('#NoofPackedItemsas').html(parseInt($(this).find('NoofPackedItems').text()));
                $('#ManuallyScannedas').html(parseInt($(this).find('ManuallyScanned').text()));
            });

            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        }
    }
    else {
        location.href = "/";
    }
}
function CreateTable(us) {
    row1 = "";
    var image = $("#imgName").val();
    var data = {
        FromDate: $("#txtDateFrom").val(),
        ToDate: $("#txtDateTo").val(),
        PackerAutoId: $("#ddlPackerName").val(),
        PageSize: 0,
        PageIndex: 1
    };
    $.ajax({
        type: "POST",
        url: "WebAPI/WPackerWiseOrderReport.asmx/GetProductWiseCommissionList",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var PrintDate = $(xmldoc).find("Table");
                    var RouteList = $(xmldoc).find("Table1");
                    var OrderTotal = $(xmldoc).find("Table2");
                    $("#PrintTable tbody tr").remove();
                    var row = $("#PrintTable thead tr").clone(true);
                    if (RouteList.length > 0) {
                        var html = "", PackerAutoId = 0, PackerName = '';
                        var NoofItems = 0, NoofPackedItems = 0, ManuallyScanned = 0;
                        $.each(RouteList, function () {
                            if (PackerAutoId != '0' && (PackerAutoId != $(this).find("PackerAutoId").text())) {
                                html = "<tr style='font-weight: 700; background: oldlace;'><td style='text-align:center;color:#dd0b3c;background:#faf2f2;' colspan='7'><b>" + PackerName + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                    + NoofItems + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + NoofPackedItems + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + ManuallyScanned + "</b></td></tr>";
                                $("#PrintTable tbody").append(html);
                                NoofItems = 0;
                                NoofPackedItems = 0;
                                ManuallyScanned = 0;
                            }
                            $(".PWarehouseManager", row).text($(this).find("WarehouseManager").text());
                            $(".PPackerName", row).text($(this).find("PackerName").text());
                            $(".PCustomerName", row).text($(this).find("CustomerName").text());
                            $(".POrderNo", row).text($(this).find("OrderNo").text());
                            $(".POrderDate", row).text($(this).find("OrderDate").text());
                            $(".PPackerAssignDate", row).text($(this).find("PackerAssignDate").text());
                            $(".PPackingDate", row).text($(this).find("PackingDate").text());
                            $(".PNoofItems", row).text($(this).find("NoofItems").text());
                            $(".PNoofPackedItems", row).text($(this).find("NoofPackedItems").text());
                            $(".PManuallyScanned", row).text($(this).find("ManuallyScanned").text());
                            NoofItems += parseInt($(this).find("NoofItems").text());
                            NoofPackedItems += parseInt($(this).find("NoofPackedItems").text());
                            ManuallyScanned += parseInt($(this).find("ManuallyScanned").text());
                            $("#PrintTable tbody").append(row);
                            row = $("#PrintTable tbody tr:last").clone(true);
                            PackerAutoId = $(this).find("PackerAutoId").text();
                            PackerName = $(this).find("PackerName").text();
                            
                        });
                        if (PackerAutoId > 0) {
                            html = "<tr style='font-weight: 700; background: oldlace;'><td style='text-align:center;color:#dd0b3c;background:#faf2f2;' colspan='7'><b>" + PackerName + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>"
                                + NoofItems + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + NoofPackedItems + "</b></td><td style='text-align:center;color:#dd0b3c;background:#faf2f2;'><b>" + ManuallyScanned + "</b></td></tr>";
                            $("#PrintTable tbody").append(html);
                            NoofItems = 0;
                            NoofPackedItems = 0;
                            ManuallyScanned = 0;
                        }

                    }
                    $(OrderTotal).each(function () {
                        $('#NoofItemsass').html(parseInt($(this).find('NoofItems').text()));
                        $('#NoofPackedItemsass').html(parseInt($(this).find('NoofPackedItems').text()));
                        $('#ManuallyScannedass').html(parseInt($(this).find('ManuallyScanned').text()));
                    });
                    if (us == 1) {
                        //var image = $("#imgName").val();
                        //var divToPrint = document.getElementById("PrintTable");
                        //var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        //mywindow.document.write('<html><head><style>#PrintTable tbody tr .right{text-align:right;} #PrintTable tbody tr .left{text-align:left;} #PrintTable tbody tr .price{text-align:right;}#PrintTable {border-collapse: collapse;width: 100%;}#PrintTable td, #PrintTable th {border: 1px solid black;padding: 8px;}#PrintTable tr:nth-child(even){background-color: #f2f2f2;}#PrintTable thead {padding-top: 12px;padding-bottom: 12px;background-color:#C8E8F9;color:black;}.text-right{text-align:right;}.text-left{text-align:left;}.text-center{text-align:center;}</style>');
                        //mywindow.document.write('</head><body>');
                        //if ($("#txtDateFrom").val() != "") {
                        //    mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;'><img src='/Img/logo/" + image + "' style='float:left;' height='70px' width='140px'/><h3 style='float:center;'>Packer Report By Order</h3><h4 style='float:right;margin-right:10px;'>Date: " + $("#txtDateFrom").val() + " To " + $("#txtDateTo").val() + "</h4></div>")

                        //}
                        //else {
                        //    debugger
                        //    mywindow.document.write("<div style='width:100%;padding:10px;text-align:center;'><img src='/Img/logo/" + image + "' style='float:left;' height='70px' width='140px'/><h3 style='float:center;'>Packer Report By Order</h3><h4 style='float:right;margin-right:10px;'>Date: " + (new Date()).format("MM/dd/yyyy") + "</h4></div>")
                        //}
                        //mywindow.document.write(divToPrint.outerHTML);
                        //mywindow.document.write('</body></html>');
                        //mywindow.print();
                        var image = $("#imgName").val();
                        var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                        mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                        $("#PrintDate").text("Print Date :" + PrintDate.find("PrintDate").text());
                        $("#PrintLogo").attr("src", "/Img/logo/" + image);
                        if ($("#txtDateFrom").val() != "") {
                            $("#DateRange").text("Date Range: " + $("#txtDateFrom").val() + " To " + $("#txtDateTo").val());
                        }
                        mywindow.document.write($(PrintTable1).clone().html());
                        //mywindow.document.write(divToPrint.outerHTML);

                        mywindow.document.write('</body></html>');
                        setTimeout(function () {
                            mywindow.print();
                        }, 2000);
                    }
                    if (us == 2) {
                        $("#PrintTable").table2excel({
                            exclude: ".noExl",
                            name: "Excel Document Name",
                            filename: "PackerReportByOrder",
                            fileext: ".xls",
                            exclude_img: true,
                            exclude_links: true,
                            exclude_inputs: true
                        });
                    }
                }
            }
            else {
                location.href = "/";
            }
        }

    });
}

/*---------------Print Code------------------*/
function PrintElem() {
    if ($('#tblProductCatelogReport tbody tr').length > 0) {
    CreateTable(1);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
/*---------------Export To Excel---------------------------*/
$("#btnExport").click(function () {
    if ($('#tblProductCatelogReport tbody tr').length > 0) {
    CreateTable(2);
    }
    else {
        toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
