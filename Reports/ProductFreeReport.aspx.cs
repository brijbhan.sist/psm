﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLProductFreeReport;

public partial class Reports_ProductFreeReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    public static string ProductFreeReport(string dataValue)
    {
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                PL_ProductFreeReport pobj = new PL_ProductFreeReport();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);

                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPerson"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["Customer"]);
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubCategoryAutoId = Convert.ToInt32(jdv["SubCategoryAutoId"]);

                pobj.Status = Convert.ToInt16(jdv["Status"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

                BL_ProductFreeReport.getReport(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch
        {
            return "false";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string BindDropdownlist()
    {
        try
        {
            PL_ProductFreeReport pobj = new PL_ProductFreeReport();
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                BL_ProductFreeReport.bindDropdownList(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindCustomer(string SalesPersonAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                //var jss = new JavaScriptSerializer();
                //var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ProductFreeReport pobj = new PL_ProductFreeReport();
                pobj.SalesPersonAutoId = Convert.ToInt32(SalesPersonAutoId);
                BL_ProductFreeReport.BindCustomer(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }


    [WebMethod(EnableSession = true)]
    public static string BindSubcategory(string dataValue)
    {
        try
        {
            PL_ProductFreeReport pobj = new PL_ProductFreeReport();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                BL_ProductFreeReport.BindSubcategory(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch
        {
            return "false";
        }
    }
}