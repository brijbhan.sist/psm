﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="SalesReportByBrand.aspx.cs" Inherits="Reports_SalesReportByBrand" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Sales By Brand</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Sales Report</a></li>
                        <li class="breadcrumb-item">Sales By Brand</li>
                          <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10052)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" onclick="ExportReport()" id="btnExport">Export</button>
                </div>
            </div>
        </div>
    </div>
     <div class="content-body">
                <section id="drag-area">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            From Order Date <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm date border-primary" placeholder="From Order Date" id="txtSFromDate" onfocus="this.select()" />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            To Order Date <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm date border-primary" placeholder="To Order Date" id="txtSToDate" onfocus="this.select()" />
                                                </div>
                                            </div>
                                             <div class="col-md-4">
                                                <select class="form-control input-sm border-primary" id="ddlSalesPerson" multiple="multiple">
                                                    <option value="0">All Sales Person</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1 btn-sm animated undefined" id="btnSearch" style="margin-top: 10px;" onclick="getReport();">Search</button>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </section>
        
    <div class="content-body">
        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblProductSalesByBrand">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Sales tblwth" rowspan="2">Sales Person</td>
                                                        <td class="Flair tblwth" colspan="2">Flair</td>
                                                        <td class="Hemping tblwth" colspan="2">Hemping</td>
                                                        <td class="Herbsens tblwth" colspan="2">Herbsens</td>
                                                        <td class="Juul tblwth" colspan="2">Juul</td>
                                                        <td class="Reclex tblwth" colspan="2">Reclex</td>
                                                        <td class="Special tblwth" colspan="2">Special Reserve</td>
                                                        <td class="Startek tblwth" colspan="2">Startek</td>
                                                        <td class="TotalSales tblwth" colspan="2">GM</td>
                                                        <td class="TotalSales tblwth"  rowspan="2">Total Sales</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="flairRevenue  price right">Revenue</td>
                                                        <td class="flairPer  price right">%</td>
                                                        <td class="hempRevenue  price right">Revenue</td>
                                                        <td class="hempPer  price right">%</td>
                                                        <td class="herbRevenue  price right">Revenue</td>
                                                        <td class="herbPer  price right">%</td>
                                                        <td class="juulRevenue  price right">Revenue</td>
                                                        <td class="juulPer  price right">%</td>
                                                        <td class="recRevenue  price right">Revenue</td>
                                                        <td class="recPer  price right">%</td>
                                                        <td class="specialRevenue  price right">Revenue</td>
                                                        <td class="specialPer  price right">%</td>
                                                        <td class="startekRevenue  price right">Revenue</td>
                                                        <td class="startekPer  price right">%</td>
                                                        <td class="gmRevenue  price right">Revenue</td>
                                                        <td class="gmPer  price right">%</td>
                                                    </tr>
                                                     <tr style="display: none">
                                                        <td class="Sales tblwth left">Sales Person</td>
                                                        <td class="flairRevenue price right">Revenue</td>
                                                        <td class="flairPer price right">%</td>
                                                        <td class="hempRevenue price right">Revenue</td>
                                                        <td class="hempPer price right">%</td>
                                                        <td class="herbRevenue price right">Revenue</td>
                                                        <td class="herbPer price right">%</td>
                                                        <td class="juulRevenue price right">Revenue</td>
                                                        <td class="juulPer price right">%</td>
                                                        <td class="recRevenue price right">Revenue</td>
                                                        <td class="recPer price right">%</td>
                                                        <td class="specialRevenue price right">Revenue</td>
                                                        <td class="specialPer price right">%</td>
                                                        <td class="startekRevenue price right">Revenue</td>
                                                        <td class="startekPer price right">%</td>
                                                        <td class="gmRevenue price right">Revenue</td>
                                                        <td class="gmPer price right">%</td>
                                                         <td class="TotalSales tblwth price right">Total Sales</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight:bold">
                                                        <td>Total</td>
                                                        <td id="flairRevenue" class="price right">0.00</td>
                                                        <td id="flairPer" class="price right">0.00</td>
                                                        <td id="hempRevenue" class="price right">0.00</td>
                                                        <td id="hempPer" class="price right">0.00</td>
                                                        <td id="herbRevenue" class="price right">0.00</td>
                                                        <td id="herbPer" class="price right">0.00</td>
                                                        <td id="juulRevenue" class="price right">0.00</td>
                                                        <td id="juulPer" class="price right">0.00</td>
                                                        <td id="recRevenue" class="price right">0.00</td>
                                                        <td id="recPer" class="price right">0.00</td>
                                                        <td id="specialRevenue" class="price right">0.00</td>
                                                        <td id="specialPer" class="price right">0.00</td>
                                                        <td id="startekRevenue" class="price right">0.00</td>
                                                        <td id="startekPer" class="price right">0.00</td>
                                                        <td id="gmRevenue" class="price right">0.00</td>
                                                        <td id="gmPer" class="price right">0.00</td>
                                                        <td id="TotalSales" class="price right">0.00</td>
                                                    </tr>
                                                
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Sales Report By Brand
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate1" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
             <div style="display: none;" id="ExcelDiv"></div>
            <%--<table class="MyTableHeader PrintMyTableHeader" id="PrintTable1">
                <thead class="bg-blue white">
                    <tr>
                        <td class="pSales tblwth" rowspan="2">Sales Person</td>
                        <td class="pFlair tblwth" colspan="2">Flair</td>
                        <td class="pHemping tblwth" colspan="2">Hemping</td>
                        <td class="pHerbsens tblwth" colspan="2">Herbsens</td>
                        <td class="pJuul tblwth" colspan="2">Juul</td>
                        <td class="pReclex tblwth" colspan="2">Reclex</td>
                        <td class="pSpecial tblwth" colspan="2">Special Reserve</td>
                        <td class="pStartek tblwth" colspan="2">Startek</td>
                        <td class="pTotalSales tblwth" colspan="2">GM</td>
                        <td class="pTotalSales tblwth" rowspan="2">Total Sales</td>
                    </tr>
                    <tr>
                        <td class="pflairRevenue">Revenue</td>
                        <td class="pflairPer">%</td>
                        <td class="phempRevenue">Revenue</td>
                        <td class="phempPer">%</td>
                        <td class="pherbRevenue">Revenue</td>
                        <td class="pherbPer">%</td>
                        <td class="pjuulRevenue">Revenue</td>
                        <td class="pjuulPer">%</td>
                        <td class="precRevenue">Revenue</td>
                        <td class="precPer">%</td>
                        <td class="pspecialRevenue">Revenue</td>
                        <td class="pspecialPer">%</td>
                        <td class="pstartekRevenue">Revenue</td>
                        <td class="pstartekPer">%</td>
                        <td class="pgmRevenue">Revenue</td>
                        <td class="pgmPer">%</td>
                    </tr>
                    <tr style="display: none">
                        <td class="pSales tblwth">Sales Person</td>
                        <td class="pflairRevenue">Revenue</td>
                        <td class="pflairPer">%</td>
                        <td class="phempRevenue">Revenue</td>
                        <td class="phempPer">%</td>
                        <td class="pherbRevenue">Revenue</td>
                        <td class="pherbPer">%</td>
                        <td class="pjuulRevenue">Revenue</td>
                        <td class="pjuulPer">%</td>
                        <td class="precRevenue">Revenue</td>
                        <td class="precPer">%</td>
                        <td class="pspecialRevenue">Revenue</td>
                        <td class="pspecialPer">%</td>
                        <td class="pstartekRevenue">Revenue</td>
                        <td class="pstartekPer">%</td>
                        <td class="pgmRevenue">Revenue</td>
                        <td class="pgmPer">%</td>
                        <td class="pTotalSales tblwth">Total Sales</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr style="font-weight: bold">
                        <td>Total</td>
                        <td id="pflairRevenue">0.00</td>
                        <td id="pflairPer">0.00</td>
                        <td id="phempRevenue">0.00</td>
                        <td id="phempPer">0.00</td>
                        <td id="pherbRevenue">0.00</td>
                        <td id="pherbPer">0.00</td>
                        <td id="pjuulRevenue">0.00</td>
                        <td id="pjuulPer">0.00</td>
                        <td id="precRevenue">0.00</td>
                        <td id="precPer">0.00</td>
                        <td id="pspecialRevenue">0.00</td>
                        <td id="pspecialPer">0.00</td>
                        <td id="pstartekRevenue">0.00</td>
                        <td id="pstartekPer">0.00</td>
                        <td id="pgmRevenue">0.00</td>
                        <td id="pgmPer">0.00</td>
                        <td id="pTotalSales">0.00</td>
                    </tr>

                </tfoot>
            </table>--%>
        </div>
    </div>
 <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

