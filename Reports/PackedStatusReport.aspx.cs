﻿using DllPackedStatusReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_PackedStatusReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string text = File.ReadAllText(Server.MapPath("/Reports/JS/PackedStatusReport.js"));
        Page.Header.Controls.Add(
             new LiteralControl(
                "<script id='checksdrivRequiredField'>" + text + "</script>"
            ));
    }
    [WebMethod(EnableSession = true)]
    public static string GetProfitReport(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                Pl_PackedStatusReport pobj = new Pl_PackedStatusReport();
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.SalesPerson = jdv["SalesAutoId"].ToString();
                pobj.ShippingAutoId = jdv["ShippingAutoId"].ToString();
                pobj.OrderNo= jdv["OrderNo"].ToString();
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_PackedStatusReport.GetProfitReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindDropDown()
    {

        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                Pl_PackedStatusReport pobj = new Pl_PackedStatusReport();
                BL_PackedStatusReport.bindDropDown(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetCustomerBySalesPerson(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                Pl_PackedStatusReport pobj = new Pl_PackedStatusReport();
                pobj.SalesPerson = jdv["SalesAutoId"].ToString();
                BL_PackedStatusReport.GetCustomerBySalesPerson(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}