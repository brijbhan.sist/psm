﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="BankReportLogView.aspx.cs" Inherits="Reports_BankReportLogView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   <style>
       table thead td{text-align:center !important}
   </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">View Bank Report Log</h3>            
        </div>
        <div class="content-header-right col-md-6 col-12 mb-2">
             <a class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" href="/Reports/BankReportLog.aspx" id="addProduct" runat="server">Back</a>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 id="divReportDate" style="margin-bottom: 0 !important;"></h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label"><b>Total Customer :</b></label>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <div class="form-group">
                                            <span id="TotalNoOfCustomer"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <label class="control-label">
                                            <b>Total Order : </b>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <div class="form-group">
                                            <span id="TotalNoOfOrder"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <label class="control-label">
                                            <b>Cash :  </b>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <span id="TotalCash"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">

                                    <div class="col-md-2 col-sm-12">
                                        <label class="control-label">
                                            <b>Check :</b>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <span id="TotalCheck"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <label class="control-label">
                                            <b>Credit Card	 :</b>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <span id="TotalCreditCard"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <label class="control-label">
                                            <b>Electronic Transfer	 :</b>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <span id="ElectronicTransfesr"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="row form-group">
                                    <div class="col-md-2 col-sm-12">
                                        <label class="control-label">
                                            <b>Money Order	 :</b>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <span id="TotalMoneyOrder">0.00</span>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <label class="control-label">
                                            <b>Store Credit Applied	 :</b>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <span id="TotalStoreCredit">0.00</span>
                                        </div>
                                    </div>
                                     <div class="col-md-2 col-sm-12">
                                        <label class="control-label">
                                            <b>Store Credit Generated	 :</b>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <span id="StoreCreditGenerated">0.00</span>
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2 col-sm-12">
                                        <label class="control-label">
                                            <b>Total Transaction :</b>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <span id="TotalPaid"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <label class="control-label">
                                            <b>Short :</b>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <span id="Short"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <label class="control-label">
                                            <b>Expense :</b>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <span id="Expense"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2 col-sm-12">
                                        <label class="control-label">
                                            <b>Deposit Amount :</b>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div class="form-group">
                                            <span id="DepositeAmount"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" id="CashDetailHeader">
                            <h4 style="margin-bottom: 0 !important;"><b>Cash Detail</b></h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblCashDetai">
                                                <thead>
                                                    <tr>
                                                        <td class="CurrencyName text-center">CASH</td>
                                                        <td class="NoOfValue text-center">Total Count</td>
                                                        <td class="TotalAmount price right">Total Amount</td>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="2">Total</td>
                                                        <td id="CashTotal"  style="text-align:right"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <p style='page-break-before: always'></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" id="OrderDetailHeader">
                            <h4 style="margin-bottom: 0 !important;"><b>Order Details</b></h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblPaymentDailyReportLog">
                                                <thead>
                                                    <tr>
                                                        <td class="CustomerName left">Customer Name</td>
                                                        <td class="OrderNo text-center">Order No</td>
                                                        <td class="Stime text-center">Settlement Time</td>
                                                        <td class="Cash  price right">Cash</td>
                                                        <td class="tCheck  price right">Check</td>
                                                        <td class="ChequeNo text-center">Check No</td>
                                                        <td class="CreditCard  price right">Credit Card</td>
                                                        <td class="ElectronicTransfer  price right" >Electronic Transfer</td>
                                                        <td class="MoneyOrder  price right">Money Order</td>
                                                        <td class="StoreCredit  price right">Store Credit</td>
                                                        <td class="TotalAmount  price right">Total Amount</td>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="3">Total</td>
                                                        <td id="Cash"  style="text-align:right"></td>
                                                        <td id="Check"  style="text-align:right"></td>
                                                        <td></td>
                                                        <td id="CreditCard"  style="text-align:right"></td>
                                                        <td id="ElectronicTransfer"  style="text-align:right"></td>
                                                        <td id="MoneyOrder"  style="text-align:right"></td>
                                                        <td id="StoreCredit"  style="text-align:right"></td>
                                                        <td id="TotalAmount"  style="text-align:right"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="expDetail" style="display:none">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" id="tblExpenseDetailHeader">
                            <h4 style="margin-bottom: 0 !important;"><b>Expense - Details</b></h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                    <table class="MyTableHeader" id="tblExpenseDescription">
                                        <thead>
                                            <tr>

                                                <td class="ePaymentMode text-center">Payment Mode</td>
                                                <td class="ExpenseAmount  price right">Expense Amount</td>
                                                <td class="ExpenseBy">Expense By</td>
                                                <td class="ExpenseDescription">Expense Description</td>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="1" class="text-right">Total</td>
                                                <td id="ExpAmount"  style="text-align:right"></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row" id="expBillDetail" style="display:none">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 style="margin-bottom: 0 !important;"><b>Expense Cash - Breakdown</b></h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="MyTableHeader" id="tblExpenseCashDetail">
                                    <thead>
                                        <tr>
                                            <td class="ExpCurrencyValue text-center">Currency</td>
                                            <td class="ExpTotalCount text-center">Total Count</td>
                                            <td class="ExpTotalAmount  price right">Total Amount</td>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2">Total</td>
                                            <td id="ExpCashTotal"  style="text-align:right"></td>
                                        </tr>
                                    </tfoot>
                                </table>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


           

         <%--   <div class="row" style="display: none">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Generate Store Credit - Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">

                                        <table class="MyTableHeader" id="ExpenseCashDetailHeader">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        <h4>Expense Cash - Breakdown</h4>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                        <table class="MyTableHeader" id="tblExpenseCashDetail">
                                            <thead>
                                                <tr>
                                                    <td class="ExpCurrencyValue text-center">Currency</td>
                                                    <td class="ExpTotalCount text-center">Total Count</td>
                                                    <td class="ExpTotalAmount text-right">Total Amount</td>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="2">Total</td>
                                                    <td id="ExpCashTotal"></td>
                                                </tr>
                                            </tfoot>
                                        </table>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>

            <div class="row" id="tblSortDetailHeader" style="display: none">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 style="margin-bottom: 0 !important;"><b>Short Amount - Details</b></h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="MyTableHeader" id="tblSortDetail" style="display: none">
                                            <thead>
                                                <tr>
                                                    <td class="PaymentID text-center">Settlement ID</td>
                                                    <td class="SettlementDate text-center">Settlement Date</td>
                                                    <td class="CustomerId text-center">Customer ID</td>
                                                    <td class="CustomerName">Customer Name</td>
                                                    <td class="ReceivedBy">Received By</td>
                                                    <td class="SettlementBy">Settlement By</td>
                                                    <td class="sPaymentMode text-center">Payment Mode</td>
                                                    <td class="ReceivedAmount  price right">Received Amount</td>
                                                    <td class="SortAmount  price right">Short Amount</td>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="7" class="=text-right">Total</td>
                                                    <td id="ReceivedAllAmount"  style="text-align:right"></td>
                                                    <td id="SortAllAmount"  style="text-align:right"></td>

                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="tblCheckDetailsHeader" style="display: none">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 style="margin-bottom: 0 !important;"><b>Check Cancelled [Settled] - Details</b></h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">

                                        <table class="MyTableHeader" id="tblCheckDetails" style="display: none">
                                            <thead>
                                                <tr>
                                                    <td class="SettlementID text-center">Settlement Id</td>
                                                    <td class="SettlementDate text-center">Settlement Date</td>
                                                    <td class="CustemerName">Customer Name</td>
                                                    <td class="RefOrderNo text-center">Reference Order No</td>
                                                    <td class="CheckNo text-center">Check No</td>
                                                    <td class="CheckDate text-center">Check Date</td>
                                                    <td class="CheckAmount  price right">Check Amount</td>
                                                    <td class="CancelDate text-center">Cancel Date</td>
                                                    <td class="CancelBy">Cancel By</td>
                                                    <td class="CancelRemark">Remark</td>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="6" class="text-right">Total</td>
                                                    <td id="CheckAmount"  style="text-align:right"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <!--<td id="SortAllAmount"></td>-->
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="tblGENERATESTORECREDITHeader" style="display:none">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 style="margin-bottom: 0 !important;"><b>Generate Store Credit - Details</b></h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">

                                        <table class="MyTableHeader" id="tblGENERATESTORECREDIT" style="display: none">
                                            <thead>
                                                <tr>
                                                    <td class="PaymentId text-center">Settlement Id</td>
                                                    <td class="PaymentDate text-center">Settlement Date</td>
                                                    <td class="CustomerId text-center">Customer Id</td>
                                                    <td class="CustomerName">Customer Name</td>
                                                    <td class="PaymentMode text-center">Payment Mode</td>
                                                    <td class="StoreCredit  price right">Store Credit</td>
                                                    <td class="ReceivedBy">Received By</td>
                                                    <td class="SettlementBy">Settlement By</td>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="5" class="text-right">Total</td>
                                                    <td id="G_Credit"  style="text-align:right"></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="tblPaymentSettelWithoutOrderHeader" style="display: none">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 style="margin-bottom: 0 !important;"><b>Payment Settled Without Order - Details</b></h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">

                                        <table class="MyTableHeader" id="tblPaymentSettelWithoutOrder" style="display: none">
                                            <thead>
                                                <tr>
                                                    <td class="PaymentId text-center">Settlement Id</td>
                                                    <td class="PaymentDate text-center">Settlement Date</td>
                                                    <td class="CustomerId text-center">Customer Id</td>
                                                    <td class="CustomerName">Customer Name</td>
                                                    <td class="PaymentMode text-center">Payment Mode</td>
                                                    <td class="ReceivedAmount  price right">Received Amount</td>
                                                    <td class="ReceivedBy">Received By</td>
                                                    <td class="SettlementBy">Settlement By</td>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="5" class="text-right">Total</td>
                                                    <td id="G_ReceivedAmount"  style="text-align:right"></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">

                                        <table>
                                            <tr>
                                                <td><b style="color: red">Note : -</b></td>
                                            </tr>
                                            <tr>
                                                <td><b>Transaction Total :</b> Cash <b>+</b> Check <b>+</b> Credit Card <b>+</b> Electronic Transfer <b>+</b> Money Order <b>+</b> Store Credit Applied <b>+</b> Store Credit Generated </td>
                                            </tr>
                                            <tr>
                                                <td><b>Deposit Amount :</b>Total Transaction -(Short <b>+</b> expense <b>+</b> Store Credit Applied <b>+</b> Electronic Transfer)</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Reports/JS/BankReportLogView.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

