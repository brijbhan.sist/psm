﻿using DLLOrderAverageReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_OrderAverageReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/OrderAverageReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }

    [WebMethod(EnableSession = true)]
    public static string bindEmployeeType()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_OrderAverageReport pobj = new PL_OrderAverageReport();
                BL_OrderAverageReport.GetEmployeeType(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;

            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindEmployee(string dataValues)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                PL_OrderAverageReport pobj = new PL_OrderAverageReport();
                pobj.EmpType = Convert.ToInt32(jdv["AutoID"]);
                BL_OrderAverageReport.GetEmployee(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;

            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string GetOrderAverageReport(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderAverageReport pobj = new PL_OrderAverageReport();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);

                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                pobj.Employee = Convert.ToInt32(jdv["Employee"]);
                pobj.EmpType = Convert.ToInt32(jdv["EmpType"]);
                BL_OrderAverageReport.bindOrderAverageReport(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }

        }
        catch
        {
            return "false";
        }

    }
}