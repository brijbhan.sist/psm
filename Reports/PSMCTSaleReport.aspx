﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="PSMCTSaleReport.aspx.cs" Inherits="Reports_PSMCTSaleReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">PSMCT Product Sold By POS</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li> 
                         <li class="breadcrumb-item"><a href="#">Other Location</a>
                        </li> 
                        <li class="breadcrumb-item">PSMCT Product Sold By POS</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10028)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height:400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                      Order From Date  <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" id="txtFromDate" value="" placeholder="From Order Date" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                     Order To Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" id="txtToDate" value="" placeholder="To Order Date" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-1">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" onclick="OrderSaleReport(1)" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
  
        <section id="drag-area3">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblOrderSaleReport">
                                                <thead class="bg-blue white">
                                                    <tr> 
                                                        <td class="CategoryName text-center" style="width:90px;white-space:nowrap">Category</td>
                                                        <td class="ProductId left" style="width:72px;white-space:nowrap">Product Id</td>
                                                        <td class="ProductName" style="text-align: center">Product Name</td>
                                                        <td class="UnitType" style="text-align: center;width:50px;white-space:nowrap">Unit </td>
                                                        <td class="PSMCT_UNIT_COST price" style="text-align: right;width:75px;white-space:nowrap">PSMCT <br />Unit Cost</td>
                                                        <td class="Sold_Default_Qty price" style="text-align: right;width:75px;white-space:nowrap">Sold <br />Default Qty</td> 
                                                        <td class="Net_Sold_Default_Qty price" style="text-align: right;width:75px;white-space:nowrap">Net Sold <br />Default Qty</td>
                                                        <td class="PSMCT_NET_COST" style="text-align: right;width:75px;white-space:nowrap">PSMCT<br /> Net Cost</td>
                                                        <td class="Net_Sale_Rev" style="text-align: right;width:75px;white-space:nowrap">Net Sale<br /> Revenue</td> 
                                                        <td class="Net_Total_Sales" style="text-align: right;width:75px;white-space:nowrap">Net Total <br />Sale</td>
                                                        <td class="Profit" style="text-align: right;width:69px;white-space:nowrap">Profit</td>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="5">Total</td> 
                                                        <td id="TSold_Default_Qty" class="right">0.00</td> 
                                                        <td id="TNet_Sold_Default_Qty" class="right">0.00</td>
                                                        <td id="TPSMCT_NET_COST" class="right">0.00</td>
                                                        <td id="TNet_Sale_Rev" class="right">0.00</td> 
                                                         <td id="TNet_Total_Sales" class="right">0.00</td>
                                                        <td id="TProfit" class="right">0.00</td>
                                                    </tr>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="5">Overall Total</td> 
                                                        <td id="OTSold_Default_Qty" class="right">0.00</td> 
                                                        <td id="OTNet_Sold_Default_Qty" class="right">0.00</td>
                                                        <td id="OTPSMCT_NET_COST" class="right">0.00</td>
                                                        <td id="OTNet_Sale_Rev" class="right">0.00</td> 
                                                        <td id="OTNet_Total_Sales" class="right">0.00</td>
                                                        <td id="OTProfit" class="right">0.00</td>
                                                    </tr>

                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                  <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="PageSize" onchange="OrderSaleReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    PSMCT Product Sold
                    <br />
                    <span class="text-center" style="font-size: 8px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
             
        </div>
        <div style="display: none;" id="ExcelDiv"></div>
    </div>
   
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

