﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllPLCustomerWiseReport;
public partial class Reports_PLCustomerWiseReport : System.Web.UI.Page
{
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/PLCustomerWiseReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindCategory()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PLCustomerWiseReport pobj = new PL_PLCustomerWiseReport();
                BL_PLCustomerWiseReport.AllddlList(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindSubCategory( string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_PLCustomerWiseReport pobj = new PL_PLCustomerWiseReport();
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                BL_PLCustomerWiseReport.SubCategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindProduct(string SubCategoryAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PLCustomerWiseReport pobj = new PL_PLCustomerWiseReport();
                pobj.SubCategoryAutoId = Convert.ToInt32(SubCategoryAutoId);
                BL_PLCustomerWiseReport.BindProduct(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindCustomer(string SalesPersonAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PLCustomerWiseReport pobj = new PL_PLCustomerWiseReport();
                pobj.SalesPerson = Convert.ToInt32(SalesPersonAutoId);
                BL_PLCustomerWiseReport.BindCustomer(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }


    [WebMethod(EnableSession = true)]
    public static string GetCustomerReport(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_PLCustomerWiseReport pobj = new PL_PLCustomerWiseReport();
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.SalesPerson = Convert.ToInt32(jdv["SalesAutoId"]);
                pobj.SubCategoryAutoId = Convert.ToInt32(jdv["SubCategoryAutoId"]);
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_PLCustomerWiseReport.CustomerWiseReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}