﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="Daily_Order_Payment_Status.aspx.cs" Inherits="Reports_Daily_Order_Payment_Status" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Daily Order Payment</h3>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Account Report  </a></li>
                        <li class="breadcrumb-item">Daily Order Payment</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10010)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body" style="min-height: 400px">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">From Order Date<span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" onchange="setdatevalidation(1)" placeholder="From Order Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" onchange="setdatevalidation(2)" placeholder="To Order Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlStatus" runat="server">
                                            <option value="0">All Status</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlPaymentStatus" runat="server">
                                            <option value="0">All Payment Status</option>
                                            <option value="N/A">N/A</option>
                                            <option value="NOT PAID">NOT PAID</option>
                                            <option value="PAID">PAID</option>
                                            <option value="PARTIAL PAID">PARTIAL PAID</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSalesPerson" runat="server">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlDriver" runat="server">
                                            <option value="0">All Driver</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">From Closed Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date"  onchange="setdatevalidation(3)" placeholder="From Closed Order Date" id="txtOrderCloseFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">To Closed Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date " onchange="setdatevalidation(4)" placeholder="To Closed Order Date" id="txtOrderCloseToDate" onfocus="this.select()" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group">
                                        <button type="button" class="btn btn-info pull-right buttonAnimation round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick=" BindReport(1);">Search</button>

                                    </div>
                                </div>

                                <div class="row">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="OrderDate text-center wth7">Order Date</td>
                                                        <td class="OrderNo text-center wth8">Order No</td>
                                                        <td class="CustomerName left">Customer Name</td>
                                                        <td class="OrderStatus text-center">Order Status</td>
                                                        <td class="PaymentStatus text-center wth5">Payment Status</td>
                                                        <td class="OrderTotal price right wth6">Order Total</td>
                                                        <td class="PaidAmount price right wth6">Paid Amount</td>
                                                        <td class="DueAmount price right wth6">Due Amount</td>
                                                        <td class="SalesPerson wth7">Sales Person</td>
                                                        <td class="WarehouseManager wth7">Warehouse Manager</td>
                                                        <td class="Packer wth7">Packer</td>
                                                        <td class="SalesManager wth7">Sales Manager</td>
                                                        <td class="Driver wth7">Driver</td>
                                                        <td class="Account wth7">Account</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold;" id="trTotal">
                                                        <td colspan="5" class="text-right">Total</td>
                                                        <td id="TGrandAmount" class="text-right">0.00</td>
                                                        <td id="TPaidAmount" class="text-right">0.00</td>
                                                        <td id="TDueAmount" class="text-right">0.00</td>
                                                        <td colspan="6"></td>
                                                    </tr>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="5" class="text-right">Overall Total</td>
                                                        <td id="TGrandAmounts" class="text-right">0.00</td>
                                                        <td id="TPaidAmounts" class="text-right">0.00</td>
                                                        <td id="TDueAmounts" class="text-right">0.00</td>
                                                        <td colspan="6"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                  <div class="d-flex">
                                    <div class="form-group">
                                        <select id="ddlPaging" class="form-control border-primary input-sm pagesize" onchange=" BindReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                     </div>
                                    <div class="ml-auto">
                                        <div class="Pager text-right"></div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">

            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" style="padding: 5px" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Daily Order Payment
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; line-height: 1; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="P_OrderDate text-center wth7">Order Date</td>
                        <td class="P_OrderNo text-center wth8">Order No</td>
                        <td class="P_CustomerName left">Customer Name</td>
                        <td class="P_OrderStatus text-center" style="text-align:center">Order Status</td>
                        <td class="P_PaymentStatus text-center wth5" style="text-align:center">Payment Status</td>
                        <td class="P_OrderTotal right price wth6">Order Total</td>
                        <td class="P_PaidAmount right price wth6">Paid Amount</td>
                        <td class="P_DueAmount right price wth6">Due Amount</td>
                        <td class="P_SalesPerson text-center wth7">Sales Person</td>
                        <td class="P_WarehouseManager text-center wth7">Warehouse Manager</td>
                        <td class="P_Packer text-center wth7">Packer</td>
                        <td class="P_SalesManager text-center wth7">Sales Manager</td>
                        <td class="P_Driver text-center wth7">Driver</td>
                        <td class="P_Account text-center wth7">Account</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5" class="text-right" style="font-weight: bold;">Total</td>
                        <td id="P_TGrandAmount" class="right" style="font-weight: bold;">0.00</td>
                        <td id="P_TPaidAmount" class="right" style="font-weight: bold;">0.00</td>
                        <td id="P_TDueAmount" class="right" style="font-weight: bold;">0.00</td>
                        <td colspan="6"></td>
                    </tr>
                </tfoot>
            </table>
        </div>

    </div>

    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

