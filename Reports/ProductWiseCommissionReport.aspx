﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ProductWiseCommissionReport.aspx.cs" Inherits="Admin_ProductWiseCommissionReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 5%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Details</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Employee Commission</a>
                        </li>
                        <li class="breadcrumb-item active">Details
                        </li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10014)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()" id="Print" runat="server">Print</button>

                    <button type="button" class="dropdown-item" id="Export" runat="server" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body" style="min-height: 400px;">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-4 col-sm-12">
                                        <select class="form-control input-sm border-primary" id="ddlSalesPersonName">
                                            <option selected="selected" value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <input type="text" id="txtProductId" class="form-control input-sm border-primary" placeholder="Product ID" />
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <input type="text" id="txtProductName" class="form-control input-sm border-primary" placeholder="Product Name" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4 col-sm-12">
                                        <select class="form-control input-sm" id="ddlCommissionCode">
                                            <option selected="selected" value="0">All Commission Code</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtDateFrom" onchange="setdatevalidation(1)" class="form-control input-sm border-primary date" placeholder="From Order Date" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtDateTo" onchange="setdatevalidation(2)" class="form-control input-sm border-primary date" placeholder="To Order Date" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group text-right">
                                    <div class="col-md-12 col-sm-12">
                                        <button id="btnSearch" type="button" value="Search" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" onclick=" GetProductWiseCommissionReport(1);">Search</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblProductCatelogReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SalesPerson left" style="width: 8%;">Sales Person</td>
                                                        <td class="ProductId text-center tblwth">Product ID</td>
                                                        <td class="ProductName left" style="width: 13%;">Product</td>
                                                        <td class="UnitType text-center tblwth">Unit</td>
                                                        <td class="CommCode text-center tblwth">Comm.<br />
                                                            Code</td>
                                                        <td class="SoldQty text-center tblwth" style="text-align: right">Sold Qty</td>
                                                        <td class="SoldAmount tblwth" style="text-align: right">Total Sale</td>
                                                        <td class="SPCommAmt  tblwth" style="text-align: right">SP Com.<br />
                                                            Amt</td>
                                                        <td class="CreditQty tblwth">Cr. Memo<br />
                                                            Qty</td>
                                                        <td class="CreditAmount tblwth" style="text-align: right">Cr. Memo<br />
                                                            Amount</td>
                                                        <td class="CreditCommissionAmount  tblwth" style="text-align: right">Cr. Com.<br />
                                                            Amount</td>
                                                        <td class="ActualSoldQty text-center tblwth">Act. Sold<br />
                                                            Qty</td>
                                                        <td class="ActualSoldAmount tblwth" style="text-align: right">Act. Sold<br />
                                                            Amount</td>
                                                        <td class="ActualCommissionAmount tblwth" style="text-align: right">Act. Com.<br />
                                                            Amount</td>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>

                                                    <tr style="font-weight: bold;">
                                                        <td colspan="5" class="text-center">Total</td>
                                                        <td id="T_NoofProduct" style="text-align: center">0</td>
                                                        <td id="T_TotalSale" style="text-align: right">0.00</td>
                                                        <td id="T_SPCommAmt" style="text-align: right">0.00</td>
                                                        <td id="T_CreditQty" style="text-align: center">0</td>
                                                        <td id="T_CreditAmount" style="text-align: right">0.00</td>
                                                        <td id="T_CreditCommissionAmount" style="text-align: right">0.00</td>
                                                        <td id="T_ActualSoldQty" style="text-align: center">0</td>
                                                        <td id="T_ActualSoldAmount" style="text-align: right">0.00</td>
                                                        <td id="T_ActualCommissionAmount" style="text-align: right">0.00</td>
                                                    </tr>
                                                    <tr style="font-weight: bold;" id="trTotal">
                                                        <td colspan="5" class="text-center">Overall Total</td>
                                                        <td id="T_NoofProducts" style="text-align: center">0</td>
                                                        <td id="T_TotalSales" style="text-align: right">0.00</td>
                                                        <td id="T_SPCommAmts" style="text-align: right">0.00</td>
                                                        <td id="T_CreditQtys" style="text-align: center">0</td>
                                                        <td id="T_CreditAmounts" style="text-align: right">0.00</td>
                                                        <td id="T_CreditCommissionAmounts" style="text-align: right">0.00</td>
                                                        <td id="T_ActualSoldQtys" style="text-align: center">0</td>
                                                        <td id="T_ActualSoldAmounts" style="text-align: right">0.00</td>
                                                        <td id="T_ActualCommissionAmounts" style="text-align: right">0.00</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select class="form-control border-primary input-sm pagesize" id="ddlPageSize" onchange=" GetProductWiseCommissionReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Employee Commission Details Report
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="PSalesPerson left text-center" style="width: 8%;">Sales Person</td>
                        <td class="PProductId tblwth text-center" style="width: 6%;">Product ID</td>
                        <td class="PProductName tblwth left" style="width: 13%;">Product</td>
                        <td class="PUnitType tblwth text-center" style="width: 6%;">Unit</td>
                        <td class="PCommCode tblwth text-center" style="width: 6%;">Comm. Code</td>
                        <td class="PSoldQty tblwth text-center" style="width: 6%;">Sold Qty</td>
                        <td class="PSoldAmount tblwth right" style="width: 6%; text-align: right">Total Sale</td>
                        <td class="PSPCommAmt tblwth right" style="width: 6%; text-align: right">SP Com. Amt</td>
                        <td class="PCreditQty tblwth text-center" style="width: 6%;">Cr. Memo Qty</td>
                        <td class="PCreditAmount tblwth right tblwth" style="width: 6%; text-align: right">Cr. Memo Amount</td>
                        <td class="PCreditCommissionAmount tblwth right" style="width: 6%; text-align: right">Cr. Com. Amount</td>
                        <td class="PActualSoldQty tblwth text-center" style="width: 6%;">Act. Sold Qty</td>
                        <td class="PActualSoldAmount tblwth right" style="width: 6%; text-align: right">Act. Sold Amount</td>
                        <td class="PActualCommissionAmount tblwth right" style="width: 6%; text-align: right">Act. Com. Amount</td>

                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr style="font-weight: bold;">
                        <td colspan="5" class="text-center" style="font-weight: bold;">Overall Total</td>
                        <td id="PT_NoofProduct" class="text-center" style="font-weight: bold;">0</td>
                        <td id="PT_TotalSale" class="text-right" style="font-weight: bold;">0.00</td>
                        <td id="PT_SPCommAmt" class="text-right" style="font-weight: bold;">0.00</td>
                        <td id="PT_CreditQty" class="text-center" style="font-weight: bold;">0</td>
                        <td id="PT_CreditAmount" class="text-right" style="font-weight: bold;">0.00</td>
                        <td id="PT_CreditCommissionAmount" class="text-right" style="font-weight: bold;">0.00</td>
                        <td id="PT_ActualSoldQty" class="text-center" style="font-weight: bold;">0</td>
                        <td id="PT_ActualSoldAmount" class="text-right" style="font-weight: bold;">0.00</td>
                        <td id="PT_ActualCommissionAmount" class="text-right" style="font-weight: bold;">0.00</td>
                    </tr>

                </tfoot>
            </table>

        </div>
    </div>

    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

