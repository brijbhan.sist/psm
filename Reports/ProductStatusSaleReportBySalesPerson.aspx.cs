﻿using DLLProductSaleStatusReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_ProductStatusSaleReportBySalesPerson : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/ProductStatusSaleReportBySalesPerson.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindReport(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ProductSaleStatusReport pobj = new PL_ProductSaleStatusReport();
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.CustomerAutoId= Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.SalesPersonAutoId = jdv["SalesPersonAutoId"].ToString();
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_ProductSaleStatusReport.BindReport(pobj);
                return pobj.Ds.GetXml();


            }
            catch (Exception ex)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string bindDropDown()
    {
        PL_ProductSaleStatusReport pobj = new PL_ProductSaleStatusReport();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                BL_ProductSaleStatusReport.bindDropDown(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            else
            {
                return "Session Expired";
            }
        }
        catch
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindCustomer(string SalesPersons)
    {
        PL_ProductSaleStatusReport pobj = new PL_ProductSaleStatusReport();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.SalesPersonAutoId = SalesPersons;
                BL_ProductSaleStatusReport.bindCustomer(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            else
            {
                return "Session Expired";
            }
        }
        catch
        {
            return "false";
        }
    }

}