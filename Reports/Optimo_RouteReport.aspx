﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="Optimo_RouteReport.aspx.cs" Inherits="Reports_Optimo_RouteReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style> 
        .Pager {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Driver Log</h3>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a></li>
                        
                        <li class="breadcrumb-item">Driver Log</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height:400px">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                   Planning From Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="From Date" onchange="setdatevalidation(1);" id="txtFromDate" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                   Planning To Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="To Date" onchange="setdatevalidation(2);" id="txtToDate" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick="GetRouteReport(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblReportPaymentDailyReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="PlanBy wth7" style="text-align: left">Plan By</td>
                                                        <td class="PlanningDate text-center wth7">Planning Date</td>
                                                        <td class="PlanningId text-center wth7">Planning Id</td>
                                                        <td class="Drivers text-center wth7">Drivers</td>
                                                        <td class="Stops text-center wth7">Stops</td>
                                                        <td class="Orders text-center wth7">Orders</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>

                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select id="ddlPageSize" class="form-control input-sm border-primary pagesize" onchange=" GetRouteReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Optimo Route Report
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>

                        <td class="P_PlanBy text-left wth7">Plan By</td>
                        <td class="P_PlanningId text-center wth7">Planning Id</td>
                        <td class="P_PlanningDate text-center wth7">Planning Date</td>
                        <td class="P_Drivers text-center wth7">Drivers</td>
                        <td class="P_Stops text-center wth7">Stops</td>
                        <td class="P_Orders text-center wth7">Orders</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
               
            </table>
        </div>
    </div>
      <div id="ShowPlanningDetails" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" id="hdnPlanId"/>
                            <h4 class="modal-title">Planning Details  [<span id="PlanningIDName" style="font-size: 14px; font-weight: bold"></span>]</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="MyTableHeader" id="tblPlanningDetails">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="Action" style="text-align:center">Action</td>
                                    <td class="DriverName"  style="text-align:center">DriverName</td>
                                    <td class="StartTime text-center">Start Time</td>
                                    <td class="EndTime text-center">End Time</td>
                                    <td class="TotalStops text-center">Total Stops</td>
                                    <td class="TotalOrders text-center">Total Orders</td>
                                    <td class="CarName">Car Name</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                          <h5 class="well text-center" id="EmptyTable33" style="display: none">No data available.</h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" onclick="PrintAllDriver_Order();">All Print</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
     <div id="ShowDriverDetails" class="modal fade" role="dialog">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Order Details   [<span id="spOrdDtls" style="font-size: 14px; font-weight: bold"></span>]</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="MyTableHeader" id="tblDriverDetails">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SalesPerson text-center wt4">Sales Person</td>
                                    <td class="StopNo text-center wt4" style="width:47px">Stop No.</td>
                                    <td class="CustomerName wt25" style="text-align:left;word-wrap:break-word;width:253px">Customer Name/ Ship to Address</td>
                                    <td class="OrderNo"  style="text-align:left;width: 90px;">Order No(s)</td>
                                    <td class="Status"  style="text-align:left;width: 90px;">Status</td>
                                    <td class="StoreOpenTime wt5" style="width:102px">Store Open Hours</td>
                                    <td class="SuggestedTime wt5" style="width:102px">Requested Delivery Hours</td>
                                    <td class="AssignDeliveryTime wt5" style="width:102px">Scheduled Delivery Time</td>
                                    <td class="DeliveryTime wt5" style="width:102px">Actual Delivery Time</td>
                                    <td class="Remark wt7">Driver Remarks<br />[Sales Person/Sales Manager]</td>
                                    <td class="AcctRemarks wt7">Account Remarks</td>

                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                          <h5 class="well text-center" id="EmptyTable34" style="display: none">No data available.</h5>
                    </div>
                </div>
                <div class="modal-footer">
                      <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnDriver_OrderPrint" onclick="PrintDriverOrd(0,0);">Print</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModalorderBy" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Select Order By</h4>
                        <button type="button" class="close pull-right" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="container">
                                    <div class="row form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="NameType" id="Templ1" value="1" checked="checked"/>
                                            Driver
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="NameType" value="2" id="Templ2" />
                                            Sales Person
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="Print_AllDriversOrder()">Print</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

