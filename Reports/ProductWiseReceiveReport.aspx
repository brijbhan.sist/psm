﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ProductWiseReceiveReport.aspx.cs" Inherits="Reports_WebAPI_ProductWiseReciveReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
      <style>
        .tblwth{
            width:7%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Product Receive</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Inventory Report</a>
                        </li>
                        <li class="breadcrumb-item active">Product Receive
                        </li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10018)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport()">Export</button>
                </div>             
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlAllCustomer">
                                            <option value="0">All Vendor</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlAllCategory" onchange="BindProductSubCategory()">
                                            <option value="0">All Category</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="AllSubCategory" onchange="BindProduct()">
                                            <option value="0">All Subcategory</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Bill No" id="txtSBillNo" />
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    From Billing Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="From Billing Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    To Billing Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="To Billing Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlProduct">
                                            <option value="0">All Product</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick="getReport(1);">Search</button>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <div class="content-body">
        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="BillDate text-center">Date</td>
                                                        <td class="BillNo text-center tblwth">Bill No</td>
                                                        <td class="VendorName left">Vendor</td>
                                                        <td class="ProductId text-center">Product ID</td>
                                                        <td class="ProductName left">Product Name</td>
                                                        <td class="TotalPieces text-center tblwth">Piece</td>
                                                        <td class="TotalBox text-center tblwth">Box</td>
                                                        <td class="TotalCase text-center tblwth">Case</td>
                                                        <td class="UnitPrice right price wth8">Unit Purchase Price</td>
                                                        <td class="SellingPrice right pric tblwth">Default Selling Price</td>
                                                        <td class="PricePerPiece right price tblwth">Price Per Piece</td>
                                                        <td class="PayableAmount right price tblwth">Item Total Amount</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight:bold">
                                                        <td colspan="5" class="text-right">Total</td>
                                                        <td id="TotalPieceQty" class="text-center">0</td>
                                                        <td id="TotalBoxQty" class="text-center">0</td>
                                                        <td id="TotalCaseQty" class="text-center">0</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td id="AmtDue" class="text-right">0.00</td>


                                                    </tr>
                                                    <tr style="font-weight: bold">
                                                        <td colspan="5" class="text-right">Overall Total</td>
                                                        <td id="TotalPieceQtys" class="text-center">0</td>
                                                        <td id="TotalBoxQtys" class="text-center">0</td>
                                                        <td id="TotalCaseQtys" class="text-center">0</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td id="AmtDues" class="text-right">0.00</td>


                                                    </tr>
                                                    <tr style="font-weight:bold">
                                                        <td colspan="5" class="text-right">AVG</td>
                                                        <td id="TotalPieceAVG" class="text-center"></td>
                                                        <td id="TotalBoxAVG" class="text-center"></td>
                                                        <td id="TotalCaseAVG" class="text-center"></td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        <td class="text-center">-</td>
                                                        

                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="getReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display:none;"  id="PrintTable1">
              <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                Inventory Report By Product Receive
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="P_BillDate text-center wth7">Date</td>
                        <td class="P_BillNo text-center wth7">Bill No</td>
                        <td class="P_VendorName left">Vendor</td>
                        <td class="P_ProductId wth7"style="text-align:center">Product ID</td>
                        <td class="P_ProductName left">Product Name</td>
                        <td class="P_TotalPieces wth7"  style="text-align:center">Piece</td>
                        <td class="P_TotalBox wth7"  style="text-align:center">Box</td>
                        <td class="P_TotalCase wth7"  style="text-align:center">Case</td>
                        <td class="P_UnitPrice right wth7">Unit Purchase Price</td>
                        <td class="P_SellingPrice right wth7">Default Selling Price</td>
                        <td class="P_PricePerPiece right wth7">Price Per Piece</td>
                        <td class="P_PayableAmount right wth7">Item Total Amount</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr >
                        <td colspan="5" class="text-right" style="text-align:center;font-weight:bold">Overall Total</td>
                        <td id="P_TotalPieceQty" style="text-align:center;font-weight:bold">0</td>
                        <td id="P_TotalBoxQty" style="text-align:center;font-weight:bold">0</td>
                        <td id="P_TotalCaseQty" style="text-align:center;font-weight:bold">0</td>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                        <td id="P_AmtDue" class="right" style="font-weight:bold">0.00</td>


                    </tr>
                    <tr>
                        <td colspan="5" class="text-right" style="text-align:center;font-weight:bold">AVG</td>
                        <td id="P_TotalPieceAVG" style="text-align:center;font-weight:bold"></td>
                        <td id="P_TotalBoxAVG" style="text-align:center;font-weight:bold"></td>
                        <td id="P_TotalCaseAVG" style="text-align:center;font-weight:bold"></td>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                        

                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
  
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

