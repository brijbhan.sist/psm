﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="customerStatementReport.aspx.cs" Inherits="customerStatementReport" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Customer Statement</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Account Report</a>
                        </li>
                        <li class="breadcrumb-item active">Customer Statement</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10009)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport()">Export</button>
                </div>
               
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height:400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-3 form-group">
                                        <select class="form-control border-primary input-sm ddlreq" id="ddlCustomer" onchange="CustomerChange()">
                                            <option value="-1">Choose Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                    From Transaction Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="From Transaction Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">
                                                    To Transaction Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="To Transaction Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-1">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick="SearchReport()">Search</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <label class="control-label">Balance Forward Amount :</label>
                                        <label class="control-label" id="lblBalanceForwardAmount"></label>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblCustomerStatementReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="TransactionDate text-center" style="width:13%;">Date</td>
                                                        <td class="TransactionType left">Transaction Type</td>
                                                        <td class="TotalOrderAmount right price wth6">Total Order<br /> Amount</td>
                                                        <td class="TotalPaidAmount right price wth6">Total Paid<br /> Amount</td>
                                                        <td class="TotalBalanceAmount right price wth10">Total Open <br />Balance Amount</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="4" style="font-weight:bold;">Open Balance</td>
                                                        <td style="font-weight:bold;text-align: right">
                                                            <label id="lblOpenBalance"></label>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="BindReport(1)">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
       
        <div class="table-responsive" style="display: none;width:98% !important;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                 Customer Statement Report
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
             <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                        <thead class="bg-blue white">
                            <tr>
                                <th colspan="5" style="background: #fff; border: none; text-align: left;">
                                    <label>Balance Forward Amount :</label>
                                    <label id="P_lblBalanceForwardAmount">0.00</label>
                                </th>
                            </tr>
                            <tr>
                                <td class="P_TransactionDate text-center width9per">Date</td>
                                <td class="P_TransactionType left">Transaction Type</td>
                                <td class="P_TotalOrderAmount right wth5">Total Order<br />
                                    Amount</td>
                                <td class="P_TotalPaidAmount right wth5">Total Paid<br />
                                    Amount</td>
                                <td class="P_TotalBalanceAmount right" style="width: 8%;">Total Open<br />
                                    Balance Amount</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="text-center" colspan="4" style="font-weight: bold;">Open Balance</td>
                                <td id="P_lblOpenBalance" class="text-right" style="font-weight: bold;"></td>
                            </tr>
                        </tfoot>
                    </table>
               
        </div>
           
    </div>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

