﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="NotShippedReport.aspx.cs" Inherits="Reports_NotShippedReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 6%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Order</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Not Shipped Report</a></li>
                        <li class="breadcrumb-item">By Order</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10021)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>


    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">From Packing Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" onchange="setdatevalidation(1)" placeholder="From Packing Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">To Packing Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" onchange="setdatevalidation(2)" placeholder="To Packing Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlStatus">
                                            <option value="0">All Status</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlSalesPerson">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick="getNotShippedReport(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblNotShippedReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Location" style="width: 6%">Location</td>
                                                        <td class="CustomerId text-center" style="width: 7%">Customer ID</td>
                                                        <td class="CustomerName left">Customer</td>
                                                        <td class="Orderno  text-center" style="width: 5%">Order No</td>
                                                        <td class="OrderDate  text-center" style="width: 6%">Order Date</td>
                                                        <td class="PackingDate  text-center" style="width: 6%">Packing Date</td>
                                                        <td class="ProductId text-center" style="width: 6%">Product ID</td>
                                                        <td class="ProductName left">Product</td>
                                                        <td class="Unit text-center" style="width: 5%">Unit</td>
                                                        <td class="RequiredQty text-center" style="width: 4%">Order Qty</td>
                                                        <td class="QtyShip text-center" style="width: 5%">Shipped Qty</td>
                                                        <td class="RemainQty text-center" style="width: 7%">Not Shipped Qty</td>
                                                        <td class="Amount price  tblwth" style="text-align: right; width: 7%">Not Shipped Amount</td>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="9">Total</td>
                                                        <td id="ordqty">0</td>
                                                        <td id="shipqty" class="center">0</td>
                                                        <td id="nitshipqty" class="center">0</td>
                                                        <td id="notshipamt" class="right">0.00</td>
                                                    </tr>
                                                    <tr style="font-weight: bold;" id="trOTotal">
                                                        <td colspan="9">Overall Total</td>
                                                        <td id="ordqtys">0</td>
                                                        <td id="shipqtys" class="center">0</td>
                                                        <td id="nitshipqtys" class="center">0</td>
                                                        <td id="notshipamts" class="right">0.00</td>
                                                    </tr>

                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPagesize" onchange="getNotShippedReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Not Shipped Report- By Order
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>

            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable1">
                <thead class="bg-blue white">
                    <tr>
                        <td class="P_Location" style="width: 4%">Location</td>
                        <td class="P_CustomerId center" style="width: 4%">Customer ID</td>
                        <td class="P_CustomerName left">Customer</td>
                        <td class="P_Orderno center " style="width: 6%">Order No</td>
                        <td class="P_OrderDate center " style="width: 6%">Order Date</td>
                        <td class="P_PackingDate  text-center" style="width: 6%; text-align: center">Packing Date</td>
                        <td class="P_ProductId text-center" style="width: 6%; text-align: center">Product ID</td>
                        <td class="P_ProductName left">Product</td>
                        <td class="P_Unit center" style="width: 4%">Unit</td>
                        <td class="P_RequiredQty text-center" style="width: 6%; text-align: center">Order Qty</td>
                        <td class="P_QtyShip text-center" style="width: 6%; text-align: center">Shipped Qty</td>
                        <td class="P_RemainQty text-center" style="width: 6%; text-align: center">Not Shipped Qty</td>
                        <td class="P_Amount" style="text-align: right; width: 6%;">Not Shipped Amount</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>

                    <tr>
                        <td colspan="9" style="font-weight: bold;">Overall Total</td>
                        <td id="ordqtyss" style="font-weight: bold; text-align: center">0</td>
                        <td id="shipqtyss" class="center" style="font-weight: bold; text-align: center">0</td>
                        <td id="nitshipqtyss" class="center" style="font-weight: bold; text-align: center">0</td>
                        <td id="notshipamtss" class="right" style="font-weight: bold;">0.00</td>
                    </tr>

                </tfoot>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="js/NotShippedReport.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

