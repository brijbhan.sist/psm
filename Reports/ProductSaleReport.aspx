﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ProductSaleReport.aspx.cs" Inherits="Reports_ProductSaleReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style>
        .tblwth {
            width: 11%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Product Sales</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Sales Report</a></li>
                        <li class="breadcrumb-item active">By Product Sales</li> 
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body" style="min-height:400px">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm date border-primary" placeholder="From Order Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm date border-primary" placeholder="To Order Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlProduct">
                                            <option value="0">All Product</option>
                                        </select>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                     <div class="col-md-4 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlPriceLevel">
                                            <option value="0">All Price Level</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlAllPerson">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                   
                                    <div class="col-md-1 form-group" style="top:10px;">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnSearch" onclick="getReport(1);">Search</button>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>

                                                        <td class="ProductId text-center tblwth6">Product ID</td>
                                                        <td class="ProductName left">Product Name</td>
                                                        <td class="Unit text-center tblwth8">Unit Type</td>
                                                        <td class="Pcs text-center tblwth8">Pcs Per<br /> Unit</td>
                                                        <td class="CustomPrice right tblwth8">Custom <br />Price</td>
                                                        <td class="SoldDefaultQty right tblwth8">Sold Default<br /> Qty</td>
                                                        <td class="SoldPcs text-center tblwth8">Sold Pcs</td>
                                                        <td class="SoldRev right tblwth8">Sale Rev</td>
                                                        <td class="SoldDefaultQty right tblwth8">Net Sold<br /> Default Qty</td>
                                                        <td class="NetSoldPcs right tblwth8">Net Sold<br /> Pcs</td>
                                                        <td class="NetSaleRev right tblwth8">Net Sale <br />Rev</td>
                                                        <td class="CostPrice price right tblwth8">Cost Price</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="8" class="text-right">Total</td>
                                                        <td id="TotalSoldDefaultQty" class="right">0.00</td>
                                                        <td id="TotalNetSoldP" class="text-right">0.00</td>
                                                        <td id="TotalNetSaleRev" class="text-right">0.00</td>
                                                        <td id="TotalcostP" class="text-right">0.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="8" class="text-right">Overall Total</td>
                                                        <td id="T_TotalSoldDefaultQty" class="right">0.00</td>
                                                        <td id="T_TotalNetSoldP" class="right">0.00</td>
                                                        <td id="T_TotalNetSaleRev" class="right">0.00</td>
                                                        <td id="T_TotalcostP" class="right">0.00</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="getReport(1);">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display: none;" id="ExcelDiv"></div>
        </section>
    </div>
   
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

