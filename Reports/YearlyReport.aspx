﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="YearlyReport.aspx.cs" Inherits="Reports_YearlyReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Yearly Report</h3>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                         <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Sales Report</a></li>
                        <%--  <li class="breadcrumb-item"><a href="#">Account Report </a></li>--%>
                        <li class="breadcrumb-item">Yearly Report</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10051)"><i class="la la-question-circle"></i><%--<i class="la la-question"></i>--%></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                    <input type="hidden" id="hfLiveDate" />
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label for="cus">Customer Name</label>
                                        <div class="form-group">
                                            <select name="Year" class="form-control border-primary input-sm" id="dllCustomer">
                                                <option value="0">All Customer</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="cus">Year</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <select name="Year" class="form-control border-primary input-sm" id="dllSelectYear">
                                                <%-- <option value="year">Current Year</option>--%>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" style="position: relative; top: 26px;" data-animation="pulse" id="btnSearch" onclick=" YearlyReport(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblYearlyReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="CustomerName text-left width13per" style="white-space: nowrap" rowspan="2">Customer Name</td>
                                                        <td class="SalesRep text-left width2per" rowspan="2">Sales Person</td>
                                                        <td class="JanOrd text-center width2per" colspan="2">January</td>
                                                        <td class="FebOrd text-center width2per" colspan="2">February</td>
                                                        <td class="MarOrd text-center width2per" colspan="2">March</td>
                                                        <td class="AprOrd text-center width2per" colspan="2">April</td>
                                                        <td class="MayOrd text-center width2per" colspan="2">May</td>
                                                        <td class="JunOrd text-center width2per" colspan="2">June</td>
                                                        <td class="JulOrd text-center width2per" colspan="2">July</td>
                                                        <td class="AugTot text-center width2per" colspan="2">August</td>
                                                        <td class="SepOrd text-center width2per" colspan="2">September</td>
                                                        <td class="OctOrd text-center width2per" colspan="2">October</td>
                                                        <td class="NovOrd text-center width2per" colspan="2">November</td>
                                                        <td class="DecOrd text-center width2per" colspan="2">December</td>
                                                        <td class="YearlyOrd text-center width2per" colspan="2">Year</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="JanOrd text-center width2per">Order</td>
                                                        <td class="JanTot text-center width2per">Total</td>
                                                        <td class="FebOrd text-center width2per">Order</td>
                                                        <td class="FebTot text-center width2per">Total</td>
                                                        <td class="MarOrd text-center width2per">Order</td>
                                                        <td class="MarTot text-center width2per">Total </td>
                                                        <td class="AprOrd text-center width2per">Order</td>
                                                        <td class="AprTot text-center width2per">Total</td>
                                                        <td class="MayOrd text-center width2per">Order</td>
                                                        <td class="MayTot text-center width2per">Total</td>
                                                        <td class="JunOrd text-center width2per">Order</td>
                                                        <td class="JunTot text-center width2per">Total</td>
                                                        <td class="JulOrd text-center width2per">Order</td>
                                                        <td class="JulTot text-center width2per">Total</td>
                                                        <td class="AugOrd text-center width2per">Order</td>
                                                        <td class="AugTot text-center width2per">Total</td>
                                                        <td class="SepOrd text-center width2per">Order</td>
                                                        <td class="SepTot text-center width2per">Total</td>
                                                        <td class="OctOrd text-center width2per">Order</td>
                                                        <td class="OctTot text-center width2per">Total</td>
                                                        <td class="NovOrd text-center width2per">Order</td>
                                                        <td class="NovTot text-center width2per">Total</td>
                                                        <td class="DecOrd text-center width2per">Order</td>
                                                        <td class="DecTot text-center width2per">Total</td>
                                                        <td class="DecOrd text-center width2per">Order</td>
                                                        <td class="DecTot text-center width2per">Total</td>
                                                    </tr>
                                                    <tr style="display: none">
                                                        <td class="CustomerName text-left width13per" style="white-space: nowrap">Customer Name</td>
                                                        <td class="SalesRep text-left width2per">Sales Person</td>
                                                        <td class="JanOrd text-center width2per">January Order</td>
                                                        <td class="JanTot text-center width2per">January Total</td>
                                                        <td class="FebOrd text-center width2per">February Order</td>
                                                        <td class="FebTot text-center width2per">February Total</td>
                                                        <td class="MarOrd text-center width2per">March Order</td>
                                                        <td class="MarTot text-center width2per">March Total </td>
                                                        <td class="AprOrd text-center width2per">April Order</td>
                                                        <td class="AprTot text-center width2per">April Total</td>
                                                        <td class="MayOrd text-center width2per">May Order</td>
                                                        <td class="MayTot text-center width2per">May Total</td>
                                                        <td class="JunOrd text-center width2per">June Order</td>
                                                        <td class="JunTot text-center width2per">June Total</td>
                                                        <td class="JulOrd text-center width2per">July Order</td>
                                                        <td class="JulTot text-center width2per">July Total</td>
                                                        <td class="AugOrd text-center width2per">August Order</td>
                                                        <td class="AugTot text-center width2per">August Total</td>
                                                        <td class="SepOrd text-center width2per">September Order</td>
                                                        <td class="SepTot text-center width2per">September Total</td>
                                                        <td class="OctOrd text-center width2per">October Order</td>
                                                        <td class="OctTot text-center width2per">October Total</td>
                                                        <td class="NovOrd text-center width2per">November Order</td>
                                                        <td class="NovTot text-center width2per">November Total</td>
                                                        <td class="DecOrd text-center width2per">December Order</td>
                                                        <td class="DecTot text-center width2per">December Total</td>
                                                        <td class="YearlyOrd text-center width2per">Yearly Order</td>
                                                        <td class="YearlyTot text-center width2per">Yearly Total</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="2">Total</td>
                                                        <td id="tJanOrd">0.00</td>
                                                        <td id="tJanTot">0.00</td>
                                                        <td id="tFebOrd">0.00</td>
                                                        <td id="tFebTot">0.00</td>
                                                        <td id="tMarOrd">0.00</td>
                                                        <td id="tMarTot">0.00</td>
                                                        <td id="tAprOrd">0.00</td>
                                                        <td id="tAprTot">0.00</td>
                                                        <td id="tMayOrd">0.00</td>
                                                        <td id="tMayTot">0.00</td>
                                                        <td id="tJunOrd">0.00</td>
                                                        <td id="tJunTot">0.00</td>
                                                        <td id="tJulOrd">0.00</td>
                                                        <td id="tJulTot">0.00</td>
                                                        <td id="tAugOrd">0.00</td>
                                                        <td id="tAugTot">0.00</td>
                                                        <td id="tSepOrd">0.00</td>
                                                        <td id="tSepTot">0.00</td>
                                                        <td id="tOctOrd">0.00</td>
                                                        <td id="tOctTot">0.00</td>
                                                        <td id="tNovOrd">0.00</td>
                                                        <td id="tNovTot">0.00</td>
                                                        <td id="tDecOrd">0.00</td>
                                                        <td id="tDecTot">0.00</td>
                                                        <td id="tYearlyOrd">0.00</td>
                                                        <td id="tYearlyTot">0.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">Overall Total</td>
                                                        <td id="otJanOrd">0.00</td>
                                                        <td id="otJanTot">0.00</td>
                                                        <td id="otFebOrd">0.00</td>
                                                        <td id="otFebTot">0.00</td>
                                                        <td id="otMarOrd">0.00</td>
                                                        <td id="otMarTot">0.00</td>
                                                        <td id="otAprOrd">0.00</td>
                                                        <td id="otAprTot">0.00</td>
                                                        <td id="otMayOrd">0.00</td>
                                                        <td id="otMayTot">0.00</td>
                                                        <td id="otJunOrd">0.00</td>
                                                        <td id="otJunTot">0.00</td>
                                                        <td id="otJulOrd">0.00</td>
                                                        <td id="otJulTot">0.00</td>
                                                        <td id="otAugOrd">0.00</td>
                                                        <td id="otAugTot">0.00</td>
                                                        <td id="otSepOrd">0.00</td>
                                                        <td id="otSepTot">0.00</td>
                                                        <td id="otOctOrd">0.00</td>
                                                        <td id="otOctTot">0.00</td>
                                                        <td id="otNovOrd">0.00</td>
                                                        <td id="otNovTot">0.00</td>
                                                        <td id="otDecOrd">0.00</td>
                                                        <td id="otDecTot">0.00</td>
                                                        <td id="otYearlyOrd">0.00</td>
                                                        <td id="otYearlyTot">0.00</td>
                                                    </tr>

                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="ddlPageSize" class="form-control input-sm border-primary pagesize" onchange="YearlyReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Yearly Report
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate1" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <div style="display: none;" id="ExcelDiv"></div>

        </div>
    </div>

    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <style>
        .ui-datepicker-calendar {
            display: none;
        }
    </style>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

