﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllDailyOrderPaymentStatus;
public partial class Reports_Daily_Order_Payment_Status : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/Daily_Order_Payment_Status.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }

    [WebMethod(EnableSession = true)]
    public static string bindStatus()
    {
        PL_DailyOrderPaymentStatus pobj = new PL_DailyOrderPaymentStatus();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {

                BL_DailyOrderPaymentStatus.bindStatus(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;


            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string OrderPaymentStatusReport(string dataValue)
    {
        try
        {
            PL_DailyOrderPaymentStatus pobj = new PL_DailyOrderPaymentStatus();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            if (jdv["CloseOrderFromDate"] != "")
                pobj.CloseOrderFromDate = Convert.ToDateTime(jdv["CloseOrderFromDate"]);
            if (jdv["CloseOrderToDate"] != "")
                pobj.CloseOrderToDate = Convert.ToDateTime(jdv["CloseOrderToDate"]);
            pobj.EmpAutoId = Convert.ToInt32(jdv["SalesPerson"]);
            pobj.DriverAutoId = Convert.ToInt32(jdv["DriverAutoId"]);
            pobj.OrderStatus = Convert.ToInt32(jdv["OrderStatus"]);
            pobj.PaymentStatus = jdv["PaymentStatus"];
            pobj.PageIndex = Convert.ToInt16(jdv["Index"]);
            pobj.PageSize = Convert.ToInt16(jdv["PageSize"]);
            BL_DailyOrderPaymentStatus.OrderPaymentStatusReport(pobj);
            return pobj.Ds.GetXml();
        }
        catch (Exception)
        {
            return "false";
        }
    }
}