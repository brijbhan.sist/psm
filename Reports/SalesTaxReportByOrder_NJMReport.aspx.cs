﻿using DLLSalesTaxReportByOrder_NJMReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_SalesTaxReportByOrder_NJMReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/SalesTaxReportByOrder_NJMReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindCustomer()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_DetailProductSalesRepor pobj = new PL_DetailProductSalesRepor();
                BL_DetailProductSalesRepor.BindCustomer(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindProductCategory()
    {
        PL_DetailProductSalesRepor pobj = new PL_DetailProductSalesRepor();
        BL_DetailProductSalesRepor.BindProductCategory(pobj);
        string json = "";
        foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
        {
            json += dr[0].ToString();
        }
        if (json == "")
        {
            json = "[]";
        }
        return json;
    }
    [WebMethod(EnableSession = true)]
    public static string BindProductSubCategory(string CategoryAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_DetailProductSalesRepor pobj = new PL_DetailProductSalesRepor();
                pobj.CategoryAutoId = Convert.ToInt32(CategoryAutoId);
                BL_DetailProductSalesRepor.BindProductSubCategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetReportDetail(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_DetailProductSalesRepor pobj = new PL_DetailProductSalesRepor();
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SalesPerson = Convert.ToInt32(jdv["SalesAutoId"]);
                pobj.SubCategoryId = Convert.ToInt32(jdv["SubCategoryAutoId"]);
                pobj.BrandId = jdv["BrandId"];
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                //pobj.SortBySoldQty = Convert.ToInt32(jdv["SortBySoldQty"]);
                BL_DetailProductSalesRepor.BindProductSaleReport(pobj);
                return pobj.Ds.GetXml();


            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindProductByBrand(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_DetailProductSalesRepor pobj = new PL_DetailProductSalesRepor();
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubCategoryId = Convert.ToInt32(jdv["SubCategoryAutoId"]);
                pobj.BrandId = jdv["BrandId"];
                BL_DetailProductSalesRepor.BindProductByBrand(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CustomerDropdown(string SalesPersonAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_DetailProductSalesRepor pobj = new PL_DetailProductSalesRepor();
                pobj.SalesPerson = Convert.ToInt32(SalesPersonAutoId);
                BL_DetailProductSalesRepor.CustomerDropdown(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}