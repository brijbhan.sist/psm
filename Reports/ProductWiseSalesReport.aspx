﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="ProductWiseSalesReport.aspx.cs" Inherits="Reports_OrderWiseOpenBalanceReport_" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 5%;
        }
.picker__day {
    padding: 2px !important;
}
.select2-search__field
{
    padding-top:0 !important;
}
.select2-search .select2-search--inline {
    padding: 0 0 5px 5px !IMPORTANT;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Product Detail</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Sales Report</a></li>
                        <li class="breadcrumb-item active">By Product Detail</li>
                          <li class="breadcrumb-item active"><a href="#" onclick="GetPageInformation(10046)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" onclick="exportToExcel()" id="btnExport">Export</button>
                </div>
                <%--<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" id="btnExport">Export</button>
                </div>--%>
            </div>
        </div>
    </div>

    <div>
        <div class="alert alert-success alert-dismissable fade in" id="alertSuccessDelete" style="display: none;">
            <a aria-label="close" id="successDeleteClose" class="close" style="cursor: pointer;">&times;</a>
            <span></span>
        </div>
        <div>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="content-body" style="min-height: 500px;">
                <section id="drag-area">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body" style="padding-bottom: 0 !important;">
                                        <div class="row form-group">
                                            <div class="col-md-3 form-group">
                                                <select class="form-control input-sm border-primary" id="ddlAllPerson" onchange="BindCustomer()">
                                                    <option value="0">All Sales Person</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3 form-group">
                                                <select class="form-control input-sm border-primary" id="ddlCustomerType" onchange="BindCustomer()">
                                                    <option value="0">All Customer Type</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3 form-group">
                                                <select class="form-control input-sm border-primary" id="ddlAllCustomer">
                                                    <option value="0">All Customer</option>
                                                </select>
                                            </div>
                                            
                                            <div class="col-md-3 form-group">
                                                <select class="form-control input-sm border-primary" id="ddlAllCategory" onchange="BindProductSubCategory()">
                                                    <option value="0">All Category</option>
                                                </select>
                                            </div>
                                             <div class="col-md-3 form-group">
                                                <select class="form-control border-primary input-sm" id="ddlSearchBy">
                                                    <option value="1">By Order Date</option>
                                                    <option value="2">By Close Date</option>
                                                </select>
                                            </div>
                                             <div class="col-md-3 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            From Date
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm date border-primary" placeholder="From Date" id="txtOrdCloseFromDate" onchange="setdatevalidation(1);" onfocus="this.select()" />
                                                </div>
                                            </div>
                                            <div class="col-md-3 form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            To Date
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm date border-primary" placeholder="To Date" id="txtOrdCloseToDate" onchange="setdatevalidation(2);" onfocus="this.select()" />
                                                </div>
                                            </div>
                                            <div class="col-md-3 form-group">
                                                <select class="form-control input-sm border-primary" id="AllSubCategory" onchange="BindProduct()">
                                                    <option value="0">All Subcategory</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <select class="form-control input-sm border-primary"  onchange="BindProduct()" style="width: 100% !important;padding:5px !important" id="ddlBrand">
                                                   
                                                </select>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <select class="form-control input-sm border-primary" style="width: 100% !important;padding:5px !important" id="ddlProduct">
                                                   
                                                </select>
                                            </div>
                                          
                                            
                                           <div class="col-md-12 form-group">
                                                <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnSearch" onclick=" getReport(1);">Search</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="MyTableHeader" id="tblOrderList">
                                                        <thead class="bg-blue white">
                                                             <tr>
                                                                <td rowspan="2" class="CustomerName left" style="width: 7%;">Customer</td>
                                                                <td rowspan="2" class="SalesPerson left" style="width: 8%;">Sales Person</td>
                                                                <td rowspan="2" class="ProductId text-center" style="width: 6%;">Product ID</td>
                                                                <td rowspan="2" class="ProductName left">Product Name</td>
                                                                <td rowspan="2" class="OrderDate text-center" style="width: 7%;">Order Date</td>
                                                                <td rowspan="2" class="OrderNo text-center" style="width: 7%;">Order No</td>
                                                                <td rowspan="2" class="CreditMemoDate text-center" style="width: 7%;">Credit Memo<br /> Date</td>
                                                                <td rowspan="2" class="CreditMemoNo text-center" style="width: 7%;">Credit<br /> Memo No</td>
                                                                   <td colspan="2">Piece</td>
                                                                <td colspan="2">Box</td>
                                                                <td colspan="2">Case</td>
                                                                <td rowspan="2" class="UnitPrice tblwth" style="text-align: right">Unit<br />
                                                                    Price ($)</td>
                                                                <td rowspan="2" class="PayableAmount" style="text-align: right; width: 6%;">Item Total Amount ($)</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="TotalPieces text-center tblwth">Sold</td>
                                                                <td class="R_TotalPieces text-center tblwth">Return</td>
                                                                <td class="TotalBox text-center tblwth">Sold</td>
                                                                <td class="R_TotalBox text-center tblwth">Return</td>
                                                                <td class="TotalCase text-center tblwth">Sold</td>
                                                                <td class="R_TotalCase text-center tblwth">Return</td>
                                                            </tr>
                                                            <tr style="display:none">
                                                                <td class="CustomerName left" style="width: 7%;">Customer</td>
                                                                <td class="SalesPerson left" style="width: 8%;">Sales Person</td>
                                                                <td class="ProductId text-center" style="width: 6%;">Product ID</td>
                                                                <td class="ProductName left">Product Name</td>
                                                                <td class="OrderDate text-center" style="width: 7%;">Order Date</td>
                                                                <td class="OrderNo text-center" style="width: 7%;">Order No</td>
                                                                <td class="CreditMemoDate text-center" style="width: 7%;">Credit Memo<br /> Date</td>
                                                                <td class="CreditMemoNo text-center" style="width: 7%;">Credit<br /> Memo No</td>
                                                                <td class="TotalPieces text-center tblwth">Sold</td>
                                                                <td class="R_TotalPieces text-center tblwth">Return</td>
                                                                <td class="TotalBox text-center tblwth">Sold</td>
                                                                <td class="R_TotalBox text-center tblwth">Return</td>
                                                                <td class="TotalCase text-center tblwth">Sold</td>
                                                                <td class="R_TotalCase text-center tblwth">Return</td>
                                                                <td class="UnitPrice tblwth" style="text-align: right">Unit<br />
                                                                    Price ($)</td>
                                                                <td class="PayableAmount" style="text-align: right; width: 6%;">Item Total Amount ($)</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="8" class="text-right">Total</td>
                                                                <td id="TotalPieceQty" style="font-weight: bold;text-align: center">0</td>
                                                                <td id="RTotalPieceQty" style="font-weight: bold;text-align: center">0</td>
                                                                <td id="TotalBoxQty" style="font-weight: bold;text-align: center">0</td>
                                                                <td id="RTotalBoxQty" style="font-weight: bold;text-align: center">0</td>
                                                                <td id="TotalCaseQty" style="font-weight: bold;text-align: center">0</td>
                                                                <td id="RTotalCaseQty" style="font-weight: bold;text-align: center">0</td>
                                                                <td></td>
                                                                <td id="AmtDue" style="text-align: right">0.00</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="8" class="text-right">Overall Total</td>
                                                                <td id="OverAllTotalPieceQty" style="font-weight: bold;text-align: center">0</td>
                                                                <td id="R_OverAllTotalPieceQty" style="font-weight: bold;text-align: center">0</td>
                                                                <td id="OverAllTotalBoxQty" style="font-weight: bold;text-align: center">0</td>
                                                                <td id="R_OverAllTotalBoxQty" style="font-weight: bold;text-align: center">0</td>
                                                                <td id="OverAllTotalCaseQty" style="font-weight: bold;text-align: center">0</td>
                                                                <td id="R_OverAllTotalCaseQty" style="font-weight: bold;text-align: center">0</td>
                                                                <td></td>
                                                                <td id="AmtDuea" style="text-align: right">0.00</td>
                                                            </tr>
                                                          <%--  <tr style="font-weight: bold;">
                                                                <td colspan="8" class="text-right">AVG by Unit</td>
                                                                <td id="TotalPieceAVG" colspan="2" style="text-align: center"></td>
                                                                <td id="TotalBoxAVG" colspan="2" style="text-align: center"></td>
                                                                <td id="TotalCaseAVG" colspan="2" style="text-align: center"></td>
                                                                <td></td>
                                                                <td id="AVGAmount" style="text-align: right"></td>
                                                            </tr>--%>
                                                        </tfoot>
                                                    </table>
                                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="getReport(1);">
                                                    <option selected="selected" value="10">10</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                    <option value="500">500</option>
                                                    <option value="1000">1000</option>
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="Pager text-right"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div style="display: none;" id="ExcelDiv"></div>
                <div class="table-responsive" style="display: none;" id="PrintTable1">
                    <div class="row" style="margin-bottom: 5px;">
                        <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                            <img src="" id="PrintLogo" height="40" />
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                            Sales Report By Product Detail
                    <br />
                            <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
                    </div>
                    <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                        <thead>
                            <tr>
                               
                                <td class="CustomerNames left" style="width: 7%;">Customer</td> 
                                <td class="SalesPersons left" style="width: 8%;">Sales Person</td>
                                <td class="ProductIds text-center" style="width: 6%;">Product ID</td>
                                <td class="ProductNames left">Product Name</td> 
                                <td class="OrderDates text-center" style="width: 7%;">Order Date</td>
                                <td class="OrderNos text-center" style="width: 7%;">Order No</td>
                                <td class="CreditMemoDates text-center" style="width: 7%;">Credit Memo Date</td>
                                <td class="CreditMemoNos text-center" style="width: 7%;">Credit Memo No</td>
                                <td class="TotalPiecess text-center" style="width: 5%;">Piece</td>
                                <td class="R_TotalPiecess text-center" style="width: 5%;">Return Piece</td>
                                <td class="TotalBoxs text-center" style="width: 5%;">Box</td>
                                <td class="R_TotalBoxs text-center" style="width: 5%;">Return Box</td>
                                <td class="TotalCases text-center" style="width: 5%;">Case</td>
                                <td class="R_TotalCases text-center" style="width: 5%;">Return Case</td>
                                <td class="UnitPrices" style="text-align: right; width: 5%;">Unit Price</td>
                                <td class="PayableAmounts" style="text-align: right; width: 5%;">Item Total Amount</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot> 
                            <tr style="font-weight: bold;">
                                <td colspan="8" class="text-right">Overall Total</td>
                                <td id="PrintTotalPieceQty" style="font-weight: bold;text-align: center">0</td>
                                <td id="R_PrintTotalPieceQty" style="font-weight: bold;text-align: center">0</td>
                                <td id="PrintTotalBoxQty" style="font-weight: bold;text-align: center">0</td>
                                <td id="R_PrintTotalBoxQty" style="font-weight: bold;text-align: center">0</td>
                                <td id="PrintTotalCaseQty" style="font-weight: bold;text-align: center">0</td>
                                <td id="R_PrintTotalCaseQty" style="font-weight: bold;text-align: center">0</td>
                                <td></td>
                                <td id="AmtDueaa" style="font-weight: bold;text-align: right">0.00</td>
                            </tr>
                         
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>


