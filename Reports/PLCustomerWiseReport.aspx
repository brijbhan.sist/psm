﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="PLCustomerWiseReport.aspx.cs" Inherits="Reports_PLCustomerWiseReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 8%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Customer</h3>

            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">P & L Report</a>

                        </li>
                        <li class="breadcrumb-item active">By Customer
                        </li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10031)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>

            </div>
        </div>
    </div>

    <div class="content-body" style="min-height:400px;">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlSalesPerson" runat="server" onchange="BindCustomer()">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlCustomerName" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlCategory" runat="server" onchange="BindSubCategory();">
                                            <option value="0">All Category</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">

                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlSubCategory" runat="server" onchange="BindProduct();">
                                            <option value="0">All Subcategory</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlProductName" runat="server">
                                            <option value="0">All Product</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="From Order Date" id="txtDateFrom" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="To Order Date" id="txtDateTo" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnCustomerSearch" onclick=" GetCustomerReport(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblCustomerWiseResport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="CustomerId tblwth text-center">Customer ID</td>
                                                        <td class="CustomerName left">Customer</td>
                                                        <td class="FirstOrderDate text-center tblwth">First Order Date</td>
                                                        <td class="LastOrderDate text-center tblwth">Last Order Date</td>
                                                        <td class="totalNoOfOrders tblwth text-center">Total No of Orders</td>
                                                        <td class="TotalSoldQty tblwth text-center">Total Sold Qty</td>
                                                        <td class="TotalCostPrice tblwth right">Total Cost Amount</td>
                                                        <td class="TotalSoldAmount tblwth right">Total Sold Amount</td>
                                                        <td class="TotalProfit tblwth right">Profit Amount</td>
                                                        <td class="ProfitPer right tblwth">Profit(%)</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="4" class="text-right">Total</td>
                                                        <td id="totalNoOfOrders" class="text-center">0</td>
                                                        <td id="TotalSoldQty" class="text-center">0</td>
                                                        <td id="TotalCostPrice" class="text-right">0.00</td>
                                                        <td id="TotalSoldAmount" class="text-right">0.00</td>
                                                        <td id="TotalProfit" class="text-right">0.00</td>
                                                        <td id="TotalProfitPer" class="text-right">0.00</td>

                                                    </tr>
                                                    <tr style="font-weight: bold;">
                                                        <td  colspan="4" class="text-right" style="font-weight:bold">Overall Total</td>
                                                        <td id="totalNoOfOrdersa" class="text-center">0</td>
                                                        <td id="TotalSoldQtya" class="text-center">0</td>
                                                        <td id="TotalCostPricea" class="text-right">0.00</td>
                                                        <td id="TotalSoldAmounta" class="text-right">0.00</td>
                                                        <td id="TotalProfita" class="text-right">0.00</td>
                                                        <td id="TotalProfitPera" class="text-right">0.00</td>

                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlCustomerPageSize" onchange=" GetCustomerReport(1);" style="width:90px">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    P&L Report By Customer
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="P_CustomerId tblwth" style="width: 8%;">Customer ID</td>
                        <td class="P_CustomerName left">Customer</td>
                        <td class="P_FirstOrderDate tblwth" style="width: 8%;">First Order Date</td>
                        <td class="P_LastOrderDate tblwth" style="width: 8%;">Last Order Date</td>
                        <td class="P_totalNoOfOrders tblwth" style="width: 8%;">Total No of Orders</td>
                        <td class="P_TotalSoldQty tblwth" style="width: 8%;">Total Sold Qty</td>
                        <td class="P_TotalCostPrice right tblwth" style="width: 8%;">Total Cost Amount</td>
                        <td class="P_TotalSoldAmount right tblwth" style="width: 8%;">Total Sold Amount</td>
                        <td class="P_TotalProfit right tblwth" style="width: 8%;">Profit Amount</td>
                        <td class="P_ProfitPer right tblwth" style="width: 8%;">Profit(%)</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr style="font-weight: bold;">
                        <td id="lb" colspan="4" class="text-center">Overall Total</td>
                        <td id="P_totalNoOfOrders" class="text-center"></td>
                        <td id="P_TotalSoldQty" class="text-center"></td>
                        <td id="P_TotalCostPrice" class="text-right"></td>
                        <td id="P_TotalSoldAmount" class="text-right"></td>
                        <td id="P_TotalProfit" class="text-right"></td>
                        <td id="P_TotalProfitPer" class="text-right"></td>

                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

