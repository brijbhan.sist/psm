﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="OrderSubmitTimeReport.aspx.cs" Inherits="Reports_OrderSubmitTimeReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
      <style>
        .tblwth{
            width:11%;
        }
     
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Date And Time</h3>
            <div class="row breadcrumbs-top">


                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Order Submit</a></li>
                        <li class="breadcrumb-item active">By Date And Time</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10027)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
               
            </div>
        </div>
    </div>


    <div class="content-body" style="min-height:400px;">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlCustomerName" runat="server">
                                            <option value="0">All Sale Person</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlRouteStatus">
                                            <option value="2">All Route Status</option>
                                            <option value="1">ON</option>
                                            <option value="0">OFF</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                   From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm  date border-primary" placeholder="From Order Date" id="txtDateFrom" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm date border-primary" placeholder="To Order Date" id="txtDateTo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnSearch" onclick="OrderSubmitReport(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblOrderSubmitTimeReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="OrderDate left" style="width:130px">Date</td>
                                                        <td class="CustomerName left">Customer Name</td>
                                                        <td class="SalePerson left text-center">Sales Person</td>
                                                        <td class="RouteStatus text-center" style="width:80px;">Route Status</td>
                                                        <td class="OrderNo text-center tblwth" style="width:70px">Order No</td>
                                                        <td class="GrandTotal price tblwth" style="text-align:right;width:90px">Grand Total</td>
                                                        <td class="status text-center tblwth" style="width:55px">Status</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight:bold;">
                                                        <td colspan="5" style="text-align:right" class="text-center">Total</td>
                                                        <td id="TotalAmt" style="text-align:right" class="text-right">0.00</td>
                                                        <td></td>
                                                    </tr>
                                                     <tr style="font-weight:bold;">
                                                        <td colspan="5" style="text-align:right" class="text-center">Overall Total</td>
                                                        <td id="TotalAmts" style="text-align:right" class="text-right">0.00</td>
                                                        <td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div><br />
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="OrderSubmitReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display:none;"  id="PrintTable1">
             <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                 Order Submit Report By Date And Time
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <%-- <tr style="background: #fff; font-weight: bold; border: none;">
                        <td colspan="3" id="PrintDate" style="background: #fff; font-weight: bold; border: none;"></td>
                        <td colspan="4" id="Date" style="background: #fff; font-weight: bold;text-align:right; border: none;"></td>
                    </tr>--%>
                    <tr>
                        <td class="P_OrderDate text-center">Date</td>
                        <td class="P_CustomerName left">Customer Name</td>
                        <td class="P_SalePerson left" style="width:14%;">Sales Person</td>
                        <td class="P_RouteStatus text-center" style="width:10%;text-align:center !important">Route Status</td>
                        <td class="P_OrderNo text-center tblwth">Order No</td>
                        <td class="P_GrandTotal right tblwth" style="text-align:right">Grand Total</td>
                        <td class="P_status text-center tblwth" style="text-align:center !important">Status</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5" class="text-center" style="font-weight:bold;text-align:right">Overall Total</td>
                        <td id="P_TotalAmt" class="text-right" style="font-weight:bold;text-align:right"></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>


    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

