﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="CustomerLocationReport.aspx.cs" Inherits="Reports_OP_SalesBySalesPerson" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../LocationCss/customcss.css" rel="stylesheet" />
    <script src="../LocationCss/jquery-ui.js"></script>
    <script src="../LocationCss/jquery.min.js"></script>
    <%--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8Y7FRVyPLlIEvF11qdFiD-ZWsf5OVIjs&callback=initMap&libraries=&v=weekly" defer></script>--%>
    <style>
        b {
            font-weight: bold !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Customer Location Report</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Customer Report</a></li>
                        <li class="breadcrumb-item active">Customer Location Report</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(12222)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <select class="form-control input-sm border-primary" id="ddlSalesPerson">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control input-sm border-primary" id="ddlStatus">
                                            <option value="2">All Status</option>
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1 btn-sm animated undefined" id="btnSearch" onclick="getmap();">Search</button>
                                    </div>
                                    <div class="col-md-2">
                                        <b>Search Result:- </b><span id="searchcount" style="font-size: 19px; font-weight: 700;">0</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id='map'></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        var latcenter, longcenter;
        function initMap() {
        }
        function setMarkers(map, beaches) {
            var infowindow = new google.maps.InfoWindow({});
            var marker, count;
            for (count = 0; count < beaches.length; count++) {
                const beach = beaches[count];
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(Number(beach[2]), Number(beach[3])),
                    map: map,
                    title: (beach[1]),
                    animation: google.maps.Animation.DROP,
                });

                google.maps.event.addListener(marker, 'click', (function (marker, count) {
                    return function () {
                        var content = "<div style='line-height:1.7;text-transform:capitalize'> <b>" + beach[0] + "</b><br><b>" + beach[1] + "</b><br>Sales Person:- " + beach[4] + "<br>Route Name:- " + beach[6];
                        content = content + "<br>Last Order Date:- " + beach[5];
                        infowindow.setContent(content);
                        infowindow.open(map, marker);
                    }
                })(marker, count));
            }
        }
        $(document).ready(function () {
            bindSalesPerson(); 
        });
        function bindSalesPerson() {
            $.ajax({
                type: "POST",
                async: false,
                url: "/Reports/CustomerLocationReport.aspx/BindSalesPerson",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {

                },
                complete: function () {
                },
                async: false,
                success: function (response) {
                    var responseData = JSON.parse(response.d);                    
                    $.each(responseData[0].SalesPerson, function (index, item) {
                        $('#ddlSalesPerson').append('<option value=' + item.AutoId + '>' + item.Name + '</option>')
                    })
                    latcenter = responseData[0].CompanyDetails[0].optimoStart_Lat;
                    longcenter = responseData[0].CompanyDetails[0].optimoStart_Long;
                    
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        function getmap() {
            pobj = {
                SalesPerson: $('#ddlSalesPerson').val(),
                Status: $('#ddlStatus').val()
            }

            $.ajax({
                type: "POST",
                async: false,
                url: "/Reports/CustomerLocationReport.aspx/getCustomerDetails",
                data: JSON.stringify({ pobj }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {

                },
                complete: function () {
                },
                async: false,
                success: function (response) {
                    var responseData = JSON.parse(response.d);
                    var locations = [];
                    $("#searchcount").html('0');
                    $.each(responseData, function (index) {
                        var location = [responseData[index].CustomerId, responseData[index].CustomerName, responseData[index].SA_Lat,
                            responseData[index].SA_Long, responseData[index].Name, responseData[index].LastOrderDate, responseData[index].RouteName];
                        locations.push(location)
                       
                    })
                    animateValue(1, responseData.length, 100);
                    const map = new google.maps.Map(document.getElementById("map"), {
                        zoom: 6,
                        center: { lat: Number(latcenter), lng: Number(longcenter) },
                    });
                    setMarkers(map, locations);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }

        function animateValue(start, end, duration) {
            if (start === end) {
                $("#searchcount").html('1');
                return;
            }
            var range = end - start;
            var current = start;
            var increment = end > start ? 1 : -1;
            var stepTime = Math.abs(Math.floor(duration / range)); 
            var timer = setInterval(function ()
            {
                current += increment;
                $("#searchcount").html(current);
                if (current == end) {
                    clearInterval(timer);
                }
            }, stepTime);
        }
    </script>
</asp:Content>

