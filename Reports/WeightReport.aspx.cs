﻿using DLLWeightReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_WeightReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/WeightReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetBrand()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_WeightReport pobj = new PL_WeightReport();
            BL_WeightReport.GetBrand(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0];
            }
            return json;
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string GetProductOnChange(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_WeightReport pobj = new PL_WeightReport();
            pobj.Brand = dataValue;
            BL_WeightReport.GetProductOnChange(pobj);

            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0];
            }
            return json;
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string GetReport(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_WeightReport pobj = new PL_WeightReport();
            pobj.Brand = jdv["BrandName"];
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
            pobj.State = Convert.ToInt32(jdv["StateAutoId"]);
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

            BL_WeightReport.GetWeightReport(pobj);

            return pobj.Ds.GetXml();

        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string WeightReportExport(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_WeightReport pobj = new PL_WeightReport();
            pobj.Brand = jdv["BrandName"];
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
            pobj.State = Convert.ToInt32(jdv["StateAutoId"]);
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);


            BL_WeightReport.ExportWeightReport(pobj);

            return pobj.Ds.GetXml();

        }
        else
        {
            return "SessionExpired";
        }
    }
}