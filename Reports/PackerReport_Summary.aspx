﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="PackerReport_Summary.aspx.cs" Inherits="Reports_PackerReport_Summary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .table th, .table td {
            padding: 3px;
        }

        .tblwth {
            width: 13%;
        }

        table thead tr td {
            text-align: center !important;
        }

        .Pager {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Summary</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Packer Report</a>
                        </li>
                        <li class="breadcrumb-item active">By Summary
                        </li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10035)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height: 400px;">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlPackerName" runat="server">
                                            <option value="0">All Packer</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">From Packing Assign Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="From Packing Assign Date" id="txtDateFrom" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">To Packing Assign Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="To Packing Assign Date" id="txtDateTo" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-1">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblPackerReportSummary">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="PackerAssignDate text-center width9per">Date</td>
                                                        <td class="warehouse left" style="width: 30%">Warehouse Manager</td>
                                                        <td class="PackerName left" style="width: 30%">Packer</td>
                                                        <td class="TotalAssignOrder text-center width10per">Total Assign Order</td>
                                                        <td class="TotalPackedOrder text-center width10per">Total Packed Order</td>
                                                        <td class="TotalPendingOrder text-center width11per">Total Pending Order</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="3" class="text-right">Total</td>
                                                        <td id="TotalAssignOrder" class="text-center">0</td>
                                                        <td id="TotalPackedOrder" class="text-center">0</td>
                                                        <td id="TotalPendingOrder" class="text-center">0</td>
                                                    </tr>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="3" class="text-right">Overall Total</td>
                                                        <td id="TotalAssignOrders" class="text-center">0</td>
                                                        <td id="TotalPackedOrders" class="text-center">0</td>
                                                        <td id="TotalPendingOrders" class="text-center">0</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2" >
                                        <select class="form-control border-primary input-sm pagesize" id="ddlPageSize">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Packer Report By Summary
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="PPackerAssignDate text-center width4per">Date</td>
                        <td class="Pwarehouse left">Warehouse Manager</td>
                        <td class="PPackerName left">Packer</td>
                        <td class="PTotalAssignOrder width8per" style="text-align: center !important">Total Assign Order</td>
                        <td class="PTotalPackedOrder width8per" style="text-align: center !important">Total Packed Order</td>
                        <td class="PTotalPendingOrder width9per" style="text-align: center !important">Total Pending Order</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" style="text-align: center !important; font-weight: bold;">Overall Total</td>
                        <td id="PTotalAssignOrder" style="text-align: center !important; font-weight: bold;"></td>
                        <td id="PTotalPackedOrder" style="text-align: center !important; font-weight: bold;"></td>
                        <td id="PTotalPendingOrder" style="text-align: center !important; font-weight: bold;"></td>
                    </tr>

                </tfoot>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="js/PackerReportSummary.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

