﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="PackerWiseOrderReport.aspx.cs" Inherits="Admin_ProductWiseCommissionReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
      <style>
        .tblwth{
            width:12%;
        }
         .Pager{
          text-align:right;
      }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Order</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Packer Report</a></li>
                        <li class="breadcrumb-item active">By Order</li>
                         <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10033)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                 <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport">Export</button>
                </div>
                <%--<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" id="btnExport">Export</button>
                </div>--%>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height:400px;">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlPackerName" style="width:100%;">
                                            <option selected="selected" value="0">All Packer</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                   Packer Assign From Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtDateFrom" class="form-control input-sm date border-primary" placeholder="Packer Assign From Date" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    Packer Assign To Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtDateTo" class="form-control input-sm date border-primary" placeholder="Packer Assign To Date" />
                                        </div>
                                    </div>
                                    <div class="col-md-1 form-group">
                                        <input type="button" id="btnSearch" value="Search" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblProductCatelogReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="WarehouseManager left" style="width:10%;">Warehouse Manager</td>
                                                        <td class="PackerName left tblwth">Packer</td>
                                                        <td class="CustomerName left">Customer</td>
                                                        <td class="OrderNo text-center tblwth" style="width:6%;">Order No</td>
                                                        <td class="OrderDate text-center tblwth">Order Date</td>
                                                        <td class="PackerAssignDate text-center tblwth">Packer Assign Date</td>
                                                        <td class="PackingDate text-center tblwth">Packing Date</td>
                                                        <td class="NoofItems text-center tblwth" style="width:4%;">No of<br /> Items</td>
                                                        <td class="NoofPackedItems text-center tblwth" style="width:4%;">No of Packed Items</td>
                                                        <td class="ManuallyScanned text-center tblwth" style="width:4%;">Manually <br />Added</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                  <tfoot>
                                                        <tr style="font-weight:bold;">
                                                            <td class="text-center" colspan="7"> Total</td>
                                                            <td id="NoofItemsa" style="text-align: center">0</td>
                                                            <td id="NoofPackedItemsa" style="text-align: center">0</td>
                                                            <td id="ManuallyScanneda" style="text-align: center">0</td>
                                                        </tr>
                                                       <tr style="font-weight:bold;">
                                                            <td class="text-center" colspan="7">Overall Total</td>
                                                            <td id="NoofItemsas" style="text-align: center">0</td>
                                                            <td id="NoofPackedItemsas" style="text-align: center">0</td>
                                                            <td id="ManuallyScannedas" style="text-align: center">0</td>
                                                        </tr>
                                                    </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>

                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right" id="pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Packer Report By Order
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="PWarehouseManager left">Warehouse Manager</td>
                        <td class="PPackerName left tblwth">Packer</td>
                        <td class="PCustomerName left">Customer</td>
                        <td class="POrderNo text-center" style="width:5%;">Order No</td>
                        <td class="POrderDate text-center tblwth">Order Date</td>
                        <td class="PPackerAssignDate text-center tblwth">Packer Assign Date</td>
                        <td class="PPackingDate text-center tblwth">Packing Date</td>
                        <td class="PNoofItems text-center tblwth" style="width:4%;">No of
                            <br />
                            Items</td>
                        <td class="PNoofPackedItems text-center tblwth" style="width:4%;">No of Packed
                            Items</td>
                        <td class="PManuallyScanned text-center tblwth" style="width:4%;">Manually
                            Added</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>

                    <tr >
                        <td class="text-center" style="font-weight:bold;" colspan="7">Overall Total</td>
                        <td id="NoofItemsass" style="font-weight:bold;text-align:center">0</td>
                        <td id="NoofPackedItemsass" style="font-weight:bold;text-align:center">0</td>
                        <td id="ManuallyScannedass" style="font-weight:bold;text-align:center">0</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/PackerWiseOrderReport.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
        </script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

