﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ProductListReport.aspx.cs" Inherits="Reports_ProductListReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Website Product List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Product List </a></li>
                    </ol>
                </div>
            </div>
        </div>

    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlCategory" onchange="bindSubcategory()">
                                            <option value="0">All Category</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSubCategory">
                                            <option value="0">All Sub Category</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product Id" id="txtProductId" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product Name" id="txtSProductName" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlbrand">
                                            <option value="0">All Brand</option>
                                        </select>
                                    </div>


                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm ddlreq" id="ddlWDisplay">
                                            <option value="">Show on website </option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                            <option value="2">Both </option>
                                        </select>
                                    </div>
                                    <div class="col-md-1 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" onclick="getProductList(1);" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12 ">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblProductList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center" style="width: 10px">Action</td>
                                                        <td class="ProductId text-center">Product ID</td>
                                                        <td class="Category">Category</td>
                                                        <td class="Subcategory">Subcategory</td>
                                                        <td class="ProductName">Product Name</td>
                                                        <td class="BrandName">Brand</td>
                                                        <td class="ImageUrl text-center">Image</td>
                                                        <td class="Status text-center" style="display: none;">Show on website</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select class="form-control border-primary input-sm pagesize" id="ddlPageSize" onchange="getProductList(1)">
                                            <option value="10">10 </option>
                                            <option value="50">50</option>
                                            <option value="100">100 </option>
                                            <option value="500">500</option>
                                            <option value="1000">1000 </option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager" id="ProductPager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

