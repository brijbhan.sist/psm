﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="BankReportLog.aspx.cs" Inherits="Reports_Bank_Report_Log" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Bank Report Log</h3>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>

                        <li class="breadcrumb-item">Bank Report Log</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10006)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height: 400px">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">Settled From Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Settled From  Date" id="txtFromDate" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">Settled To Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Settled To Date" id="txtToDate" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick="PaymentDailyReport(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblReportPaymentDailyReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center wth6">Action</td>
                                                        <td class="Date text-center wth7">Date</td>
                                                        <td class="TotalNoOfCustomer text-center wth7">Total Customer</td>
                                                        <td class="TotalNoOfOrder text-center wth7">Total Order</td>
                                                        <td class="TotalCash right wth7">Cash</td>
                                                        <td class="TotalCheck right wth7">Check</td>
                                                        <td class="TotalCreditCard right wth7">Credit Card</td>
                                                        <td class="ElectronicTransfer right wth7" style="background-color: #e0707d">Electronic Transfer</td>
                                                        <td class="TotalMoneyOrder right wth7">Money Order</td>
                                                        <td class="TotalStoreCredit right wth7" style="background-color: #e0707d">Store Credit Applied</td>
                                                        <td class="CreditAmount right wth7">Store Credit Generated</td>
                                                        <td class="TotalPaid right wth7">Total Transaction </td>
                                                        <td class="Short right wth7" style="background-color: #e0707d">Short</td>
                                                        <td class="Expense right wth7" style="background-color: #e0707d">Expense</td>
                                                        <td class="Deposite right wth7">Deposit Amount</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="2" class="text-center">Total</td>
                                                        <td id="TotalCustomer" class="text-center">0</td>
                                                        <td id="TotalOrder" class="text-center">0</td>
                                                        <td id="Totalcase" style="text-align: right;">0.00</td>
                                                        <td id="TotalCheck" style="text-align: right;">0.00</td>
                                                        <td id="TotalCreditcard" style="text-align: right;">0.00</td>
                                                        <td id="TotalstoredElectricTransfer" style="text-align: right;">0.00</td>
                                                        <td id="TotalMoneyorder" style="text-align: right;">0.00</td>
                                                        <td id="TotalstorecreditApply" style="text-align: right;">0.00</td>
                                                        <td id="TotalstoredCreditGenerated" style="text-align: right;">0.00</td>
                                                        <td id="TotalGrand" style="text-align: right;">0.00</td>
                                                        <td id="TotalShort" style="text-align: right;">0.00</td>
                                                        <td id="TotalExpense" style="text-align: right;">0.00</td>
                                                        <td id="TotalDepositAmount" style="text-align: right;">0.00</td>
                                                    </tr>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="2" class="text-center">Overall Total</td>
                                                        <td id="TotalCustomers" class="text-center">0</td>
                                                        <td id="TotalOrdesr" class="text-center">0</td>
                                                        <td id="Totalcases" style="text-align: right;">0.00</td>
                                                        <td id="TotalChecks" style="text-align: right;">0.00</td>
                                                        <td id="TotalCreditcards" style="text-align: right;">0.00</td>
                                                        <td id="TotalstoredElectricTransfers" style="text-align: right;">0.00</td>
                                                        <td id="TotalMoneyorders" style="text-align: right;">0.00</td>
                                                        <td id="TotalstorecreditApplys" style="text-align: right;">0.00</td>
                                                        <td id="TotalstoredCreditGenerateds" style="text-align: right;">0.00</td>
                                                        <td id="TotalGrands" style="text-align: right;">0.00</td>
                                                        <td id="TotalShorts" style="text-align: right;">0.00</td>
                                                        <td id="TotalExpenses" style="text-align: right;">0.00</td>
                                                        <td id="TotalDepositAmounts" style="text-align: right;">0.00</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-group">
                                        <select id="ddlPageSize" class="form-control input-sm border-primary pagesize" onchange=" PaymentDailyReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Bank Report Log
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="P_Date text-center wth7">Date</td>
                        <td class="P_TotalNoOfCustomer text-center wth7">Total Customer</td>
                        <td class="P_TotalNoOfOrder text-center wth7">Total Order</td>
                        <td class="P_TotalCash right wth7">Cash</td>
                        <td class="P_TotalCheck right wth7">Check</td>
                        <td class="P_TotalCreditCard right wth7">Credit Card</td>
                        <td class="P_ElectronicTransfer right wth7">Electronic Transfer</td>
                        <td class="P_TotalMoneyOrder right wth7">Money Order</td>
                        <td class="P_TotalStoreCredit right wth7">Store Credit Applied</td>
                        <td class="P_CreditAmount right wth7">Store Credit Generated</td>
                        <td class="P_TotalPaid right wth7">Total Transacton </td>
                        <td class="P_Short right wth7">Short</td>
                        <td class="P_Expense right wth7">Expense</td>
                        <td class="P_Deposite right wth7">Deposite Amount</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr style="font-weight: bold;">
                        <td colspan="1" class="text-center">Overall Total</td>
                        <td id="OTotalCustomers" class="text-center">0</td>
                        <td id="OTotalOrdesr" class="text-center">0</td>
                        <td id="OTotalcases" style="text-align: right;">0.00</td>
                        <td id="OTotalChecks" style="text-align: right;">0.00</td>
                        <td id="OTotalCreditcards" style="text-align: right;">0.00</td>
                        <td id="OElectronicTransfer" style="text-align: right;">0.00</td>
                        <td id="OTotalMoneyorders" style="text-align: right;">0.00</td>
                        <td id="OTotalstorecreditApplys" style="text-align: right;">0.00</td>
                        <td id="OTotalstoredCreditGenerateds" style="text-align: right;">0.00</td>
                        <td id="OTotalGrands" style="text-align: right;">0.00</td>
                        <td id="OTotalShorts" style="text-align: right;">0.00</td>
                        <td id="OTotalExpenses" style="text-align: right;">0.00</td>
                        <td id="OTotalDepositAmounts" style="text-align: right;">0.00</td>
                    </tr>
                </tfoot>

            </table>
        </div>
    </div>


    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

