﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="SalesBySalesPerson.aspx.cs" Inherits="Reports_SalesBySalesPerson" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 10%;
        }

        .select2-container--classic .select2-selection--multiple, .select2-container--default .select2-selection--multiple {
            border: 1px solid #666EE8 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Sales Person</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Sales Report</a></li>
                        <li class="breadcrumb-item active">By Sales Person</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10058)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>

            </div>
        </div>
    </div>


    <div>
        <div class="alert alert-success alert-dismissable fade in" id="alertSuccessDelete" style="display: none;">
            <a aria-label="close" id="successDeleteClose" class="close" style="cursor: pointer;">&times;</a>
            <span></span>
        </div>
        <div>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="content-body" style="min-height:400px;">
                <section id="drag-area">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">

                                        <div class="row form-group">
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                           From Order Date <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm date border-primary" placeholder="From Order Date" id="txtSFromDate" onfocus="this.select()" />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">
                                                            To Order Date <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm date border-primary" placeholder="To Order Date" id="txtSToDate" onfocus="this.select()" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control input-sm border-primary" id="ddlSalesPerson" multiple="multiple">
                                                    <option value="0">All Sales Person</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1 btn-sm animated undefined" id="btnSearch" style="margin-top: 10px;" onclick="BindReport(1);">Search</button>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row container-fluid">
                                            <div class="table-responsive">
                                                <table class="MyTableHeader" id="tblOrderList">
                                                    <thead class="bg-blue white">
                                                        <tr>
                                                            <td class="EmployeeName left">Sales Person</td>
                                                            <td class="NoOfOrders text-center tblwth">Sales Person Orders</td>
                                                            <td class="TotalOrderAmount price tblwth" style="text-align: right;">Sales Person Amount</td>
                                                            <td class="NoofPOSOrders text-center tblwth">POS Sales Order</td>
                                                            <td class="POSTotalAmount price tblwth" style="text-align: right;">POS Sale Amount</td>
                                                            <td class="TotalOrders text-center tblwth">Total Orders</td>
                                                            <td class="OrderTotal price tblwth" style="text-align: right;">Total Sale Amount</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td style="font-weight: bold;" class="text-right">Total</td>
                                                            <td id="NoOfOrders" style="text-align: center; font-weight: bold;">0</td>
                                                            <td id="GrandAmount" style="text-align: right; font-weight: bold;">0.00</td>
                                                            <td id="NoofPOSOrders" style="text-align: center; font-weight: bold;">0</td>
                                                            <td id="POSGrandAmount" style="text-align: right; font-weight: bold;">0.00</td>
                                                            <td id="AllOrders" style="text-align: center; font-weight: bold;">0</td>
                                                            <td id="AllGrandAmount" style="text-align: right; font-weight: bold;">0.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-weight: bold;" class="text-right">Overall Total</td>
                                                            <td id="NoOfOrdersa" style="text-align: center; font-weight: bold;">0</td>
                                                            <td id="GrandAmounta" style="text-align: right; font-weight: bold;">0.00</td>
                                                            <td id="NoofPOSOrdersa" style="text-align: center; font-weight: bold;">0</td>
                                                            <td id="POSGrandAmounta" style="text-align: right; font-weight: bold;">0.00</td>
                                                            <td id="AllOrdersa" style="text-align: center; font-weight: bold;">0</td>
                                                            <td id="AllGrandAmounta" style="text-align: right; font-weight: bold;">0.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-weight: bold;" class="text-right">AVG Price</td>
                                                            <td></td>
                                                            <td id="avgSales" style="text-align:right;font-weight: bold;"></td>
                                                            <td></td>
                                                            <td id="avgPOS" style="text-align:right;font-weight: bold;"></td>
                                                            <td></td>
                                                            <td id="AVGPiece" style="text-align:right;font-weight: bold;"></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                                <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-2">
                                                <select id="ddlPaging" class="form-control border-primary input-sm pagesize" onchange="BindReport(1);">
                                                    <option selected="selected" value="10">10</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                    <option value="500">500</option>
                                                    <option value="1000">1000</option>
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="Pager text-right"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
                <div class="table-responsive" style="display: none;" id="PrintTable1">
                    <div class="row" style="margin-bottom: 5px;">
                        <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                            <img src="" id="PrintLogo" height="40" />
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                            Sale Report By Sales Person
                    <br />
                            <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
                    </div>
                    <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                        <thead>
                            <tr>
                                <td class="EmployeeNames left">Sales Person</td>
                                <td class="NoOfOrderss text-center tblwth">Sales Person Orders</td>
                                <td class="TotalOrderAmounts price tblwth" style="text-align: right;">Sales Person Amount</td>
                                <td class="NoofPOSOrderss text-center tblwth">POS Sales Order</td>
                                <td class="POSTotalAmounts price tblwth" style="text-align: right;">POS Sale Amount</td>
                                <td class="TotalOrderss text-center tblwth">Total Orders</td>
                                <td class="OrderTotals price tblwth" style="text-align: right;">Total Sale Amount</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td style="font-weight: bold;" class="text-right">Total</td>
                                <td id="NoOfOrdersd" style="text-align: center; font-weight: bold;">0</td>
                                <td id="GrandAmountd" style="text-align: right; font-weight: bold;">0.00</td>
                                <td id="NoofPOSOrdersd" style="text-align: center; font-weight: bold;">0</td>
                                <td id="POSGrandAmountd" style="text-align: right; font-weight: bold;">0.00</td>
                                <td id="AllOrdersd" style="text-align: center; font-weight: bold;">0</td>
                                <td id="AllGrandAmountd" style="text-align: right; font-weight: bold;">0.00</td>
                            </tr>
                            <tr>
                                <td style="font-weight:bold;" class="text-right">AVG Price</td>
                                <td></td>
                                <td id="avgSalesP" style="text-align:right;font-weight:bold;"></td>
                                <td></td>
                                <td id="avgPOSP" style="text-align:right;font-weight:bold;"></td>
                                <td></td>
                                <td id="AVGPieceP" style="text-align:right;font-weight:bold;"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div style="display: none;" id="ExcelDiv"></div>
            </div>
        </div>
    </div>

    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

