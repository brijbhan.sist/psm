﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="Commission_Summary_Report.aspx.cs" Inherits="Reports_OrderWiseOpenBalanceReport_" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 16%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Summary</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Employee Commission </a></li>
                        <li class="breadcrumb-item active">Summary</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10005)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>

            </div>
        </div>
    </div>


    <div>
        <div class="alert alert-success alert-dismissable fade in" id="alertSuccessDelete" style="display: none;">
            <a aria-label="close" id="successDeleteClose" class="close" style="cursor: pointer;">&times;</a>
            <span></span>
        </div>
        <div>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="content-body" style="min-height: 400px">
                <section id="drag-area">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row  form-group">
                                            <div class="col-md-3">
                                                <select class="form-control input-sm border-primary" id="ddlAllPerson">
                                                    <option value="0">All Sales Person</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 0.5rem;">From Order Date <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm date border-primary" onchange="setdatevalidation(1)" placeholder="From Order Date" id="txtSFromDate" onfocus="this.select()" />
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" style="padding: 0rem 0.5rem;">To Order Date <span class="la la-calendar-o"></span>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control input-sm date border-primary" onchange="setdatevalidation(2)" placeholder="To Order Date" id="txtSToDate" onfocus="this.select()" />
                                                </div>
                                            </div>
                                            <div class="col-md-1 ">
                                                <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" id="btnSearch" onclick="getReport(1);">Search</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="MyTableHeader" id="tblOrderList">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="CommCode text-center tblwth">Commission Category</td>
                                                                <td class="NoofProduct tblwth" style="text-align: center">No of Products Sold</td>
                                                                <td class="TotalSale tblwth" style="text-align: right">Total Sales</td>
                                                                <td class="Commission tblwth" style="text-align: right">Commission</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr style="font-weight: bold;">
                                                                <td class="text-right">Total</td>
                                                                <td id="T_NoofProduct" style="text-align: center">0</td>
                                                                <td id="T_TotalSale" style="text-align: right">0.00</td>
                                                                <td id="T_SPCommAmt" style="text-align: right">0.00</td>
                                                            </tr>
                                                            <tr style="font-weight: bold;" id="trTotal">
                                                                <td class="text-right">Overall Total</td>
                                                                <td id="T_NoofProducts" style="text-align: center">0</td>
                                                                <td id="T_TotalSales" style="text-align: right">0.00</td>
                                                                <td id="T_SPCommAmts" style="text-align: right">0.00</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex">
                                            <div class="form-group">
                                                <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="getReport(1);">
                                                    <option selected="selected" value="10">10</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                    <option value="500">500</option>
                                                    <option value="1000">1000</option>
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                            <div class="ml-auto">
                                                <div class="Pager text-right"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="table-responsive" style="display: none;" id="PrintTable1">
                    <div class="row" style="margin-bottom: 5px;">
                        <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                            <img src="" id="PrintLogo" height="40" />
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                            Employee Commission Summary Report
                    <br />
                            <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
                    </div>
                    <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                        <thead class="bg-blue white">
                            <tr>
                                <td class="PCommCode tblwth" style="text-align: center;">Commission Category</td>
                                <td class="PNoofProduct " style="text-align: center;">No of Products Sold</td>
                                <td class="PTotalSale right tblwth" style="text-align: right;">Total Sales</td>
                                <td class="PCommission right tblwth" style="text-align: right;">Commission</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td style="font-weight: bold; text-align: right;">Overall Total</td>
                                <td id="PT_NoofProduct" style="font-weight: bold; text-align: center;">0</td>
                                <td id="PT_TotalSale" style="font-weight: bold; text-align: right;">0.00</td>
                                <td id="PT_SPCommAmt" style="font-weight: bold; text-align: right;">0.00</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>


