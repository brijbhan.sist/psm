﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" AutoEventWireup="true" CodeFile="OrderWiseOpenBalanceReport.aspx.cs" Inherits="Reports_OrderWiseOpenBalanceReport_" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 13%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Order</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Open Balance Report</a>
                        </li>
                        <li class="breadcrumb-item active">By Order 
                        </li>
                        <li class="breadcrumb-item active"><a href="#" onclick="GetPageInformation(10004)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>




    <div class="content-body" style="min-height: 400px">
        <section id="drag-area">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSalesPerson" onchange="BindCustomer();">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlCustomertype" onchange=" BindCustomer();">
                                            <option value="0">All Customer Type</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlCustomer">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="From Order Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="To Order Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlShippingType">
                                            <option value="0">All Shipping Type</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlShortBy">
                                            <option value="1">Sort By Sales Person</option>
                                            <option value="2">Sort By Customer</option>
                                            <option value="3">Sort By Order No</option>
                                            <option value="4">Sort By Due </option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlOrderBy">
                                            <option value="ASC">Ascending</option>
                                            <option value="DESC">Descending </option>
                                        </select>
                                    </div>
                                    <div class="col-md-4   form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" id="btnSearch" onclick=" BindReport(1);">Search</button>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SalesPerson  text-center">Sales Person</td>
                                                        <td class="CustomerId  text-center">Customer Id</td>
                                                        <td class="CustomerName left">Customer</td>
                                                        <td class="ShippingType  text-center">Shipping Type</td>
                                                        <td class="OrderDate text-center tblwth" style="width: 80px">Order Date</td>
                                                        <td class="OrderNo text-center tblwth" style="width: 70px">Order No</td>
                                                        <td class="TotalOrderAmount tblwth" style="text-align: right">Total Order Amount</td>
                                                        <td class="TotalPaid tblwth" style="text-align: right; width: 80px">Total Paid</td>
                                                        <td class="TotalDue tblwth" style="text-align: right; width: 80px">Total Due</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold">
                                                        <td colspan="6" class="text-center">Total</td>
                                                        <td id="TotalOrderAmount" class="text-right">0.00</td>
                                                        <td id="TotalPaid" class="text-right">0.00</td>
                                                        <td id="AmtDue" class="text-right">0.00</td>
                                                    </tr>
                                                    <tr style="font-weight: bold">
                                                        <td colspan="6" class="text-center">Overall Total</td>
                                                        <td id="TotalOrderAmounts" class="text-right">0.00</td>
                                                        <td id="TotalPaids" class="text-right">0.00</td>
                                                        <td id="AmtDues" class="text-right">0.00</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="ddlPaging" class="form-control input-sm border-primary pagesize" onchange="BindReport(1);">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Open Balance Report By Order
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead>
                    <tr>
                        <td class="PSalesPerson  text-center">Sales Person</td>
                        <td class="PCustomerId  text-center">Customer Id</td>
                        <td class="PCustomerName left">Customer</td>
                        <td class="PShippingType  text-center">Shipping Type</td>
                        <td class="POrderDate" style="width: 12%;">Order Date</td>
                        <td class="POrderNo" style="width: 12%;">Order No</td>
                        <td class="PTotalOrderAmount right" style="width: 12%;">Total Order Amount</td>
                        <td class="PTotalPaid right" style="width: 12%;">Total Paid</td>
                        <td class="PTotalDue right" style="width: 12%;">Total Due</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr style="font-weight: bold">
                        <td colspan="6" class="text-center" style="font-weight: bold !important; text-align: center">Overall Total</td>
                        <td id="PTotalOrderAmount" class="text-right" style="font-weight: bold !important;">0.00</td>
                        <td id="PTotalPaid" class="text-right" style="font-weight: bold !important;">0.00</td>
                        <td id="PAmtDue" class="text-right" style="font-weight: bold !important;">0.00</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>


