﻿using DllProfitReportByOrder;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_ProfitReportByOrder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string text = File.ReadAllText(Server.MapPath("/Reports/JS/ProfitReportByOrder.js"));
        Page.Header.Controls.Add(
             new LiteralControl(
                "<script id='checksdrivRequiredField'>" + text + "</script>"
            ));
    }
    [WebMethod(EnableSession = true)]
    public static string GetProfitReport(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                Pl_ProfitReportByOrder pobj = new Pl_ProfitReportByOrder();
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.SalesPerson = jdv["SalesAutoId"].ToString();
                pobj.ShippingAutoId = jdv["ShippingAutoId"].ToString();
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_ProfitReportByOrder.GetProfitReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    
    [WebMethod(EnableSession = true)]
    public static string bindDropDown()
    {

        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                Pl_ProfitReportByOrder pobj = new Pl_ProfitReportByOrder();
                BL_ProfitReportByOrder.bindDropDown(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}