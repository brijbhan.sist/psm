﻿using DllOtpReportSummary;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_OtpReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/OtpReportSummary.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetOtpReport(string dataValue)
    {
        try
        {
            PL_OtpReportSummary pobj = new PL_OtpReportSummary();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            if (jdv["DateFrom"] != null && jdv["DateFrom"] != "")
            {
                pobj.FromDate = Convert.ToDateTime(jdv["DateFrom"]);
            }
            if (jdv["DateTo"] != null && jdv["DateTo"] != "")
            {
                pobj.ToDate = Convert.ToDateTime(jdv["DateTo"]);
            }
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            BL_OtpReportSummary.GetOtpResport(pobj);

            return pobj.Ds.GetXml();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindTaxType()
    {
        try
        {
            PL_OtpReportSummary pobj = new PL_OtpReportSummary();
            BL_OtpReportSummary.BindTaxType(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0];
            }
            return json;
        }
        catch
        {
            return "false";
        }
    }
}