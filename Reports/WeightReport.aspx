﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="WeightReport.aspx.cs" Inherits="Reports_WeightReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <style>
        .tblwth{
            width:6%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Weight</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Report</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">OPT Report</a>
                        </li>
                        <li class="breadcrumb-item active">By Weight
                        </li>
                             <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10026)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height:400px;">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-4 col-sm-12">
                                        <select class="form-control input-sm border-primary" id="ddlBrand" onchange="Brandchange()">
                                            <option value="0">All Brand</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <select class="form-control input-sm border-primary" id="ddlProduct">
                                            <option value="0">All Product</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <select class="form-control input-sm border-primary" id="ddlState">
                                            <option value="0">All State</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="From Order Date" id="txtFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="To Order Date" id="txtToDate" onfocus="this.select()" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 col-sm-12">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1 pull-right btn-sm" id="btnSearch" onclick=" getRecords(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblWeightReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="InvoiceDate text-center" style="width:11%;">Invoice Date</td>
                                                        <td class="InvoiceNo tblwth text-center" style="width:8%;">Invoice No</td>
                                                        <td class="BrandName text-center" style="width:8%;">Brand Name</td>
                                                        <td class="Customer left">Customer</td>
                                                        <td class="Address left">Address</td>
                                                        <td class="City text-center" style="width:8%;">City</td>
                                                        <td class="StateName  text-center" style="width:8%;">State</td>
                                                        <td class="ZipCode tblwth text-center">Zip Code</td>
                                                        <td class="OTPLicence text-center tblwth">OPT Licence</td>
                                                        <td class="ProductDescription" style="width:8%;">Product Description</td>
                                                        <td class="TotalOzSold tblwth" style="text-align:right">Total Oz Sold</td>
                                                        <td class="TotalOzTax tblwth" style="text-align:right">Total Oz Tax</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight:bold;">
                                                        <td colspan="10" class="text-right text-bold-400" style="font-weight:bold;"><b>Total</b></td>
                                                        <td id="OZSold" class=" text-right text-bold-800" style="font-weight:bold;"><b>0.00</b></td>
                                                        <td id="OZTax" class=" text-right text-bold-800" style="font-weight:bold;"><b>0.00</b></td>
                                                    </tr>
                                                    <tr  style="font-weight:bold;">
                                                        <td colspan="10" class="text-right text-bold-400" style="font-weight:bold;"><b>Overall Total</b></td>
                                                        <td id="TotalOzSolds" class=" text-right text-bold-800" style="font-weight:bold;"><b>0.00</b></td>
                                                        <td id="TotalOzTaxs" class=" text-right text-bold-800" style="font-weight:bold;"><b>0.00</b></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="getRecords(1)">
                                            <option value="10" selected="selected">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" style="padding:5px" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top:0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Weight Tax Report
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; line-height:1; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>

            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="InvoiceDate text-center">Invoice Date</td>
                        <td class="InvoiceNo text-center">Invoice No</td>
                        <td class="BrandName text-center">Brand Name</td>
                        <td class="Customer left">Customer</td>
                        <td class="Address left">Address</td>
                        <td class="City text-center">City</td>
                        <td class="StateName text-center">State</td>
                        <td class="ZipCode text-center">Zip Code</td>
                        <td class="OTPLicence text-center">OPT Licence</td>
                        <td class="ProductDescription text-center">Product Description</td>
                        <td class="TotalOzSold right" style="text-align:right">Total Oz Sold</td>
                        <td class="TotalOzTax right" style="text-align:right">Total Oz Tax</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr style="font-weight:bold;">
                        <td colspan="10" style="text-align:right;font-weight:bold;"><b>Overall Total</b></td>
                        <td id="PrintTotalOzSolds" class=" text-right text-bold-800" style="font-weight:bold;"><b>0.00</b></td>
                        <td id="PrintTotalOzTaxs" class=" text-right text-bold-800" style="font-weight:bold;"><b>0.00</b></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
   
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
</asp:Content>

