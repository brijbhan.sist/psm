﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLUpcomingMigrationReport;

public partial class Reports_UpcomingMigrationReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/Js/UpcomingMigrationReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }

     [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static string GetReport(PL_UpcomingMigrationReport pobj)
    {
        try
        {                      
            BL_UpcomingMigrationReport.Getreport(pobj);
            return pobj.Ds.GetXml();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string Migration(string OrderAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_UpcomingMigrationReport pobj = new PL_UpcomingMigrationReport();
            pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
            BL_UpcomingMigrationReport.Migration(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}