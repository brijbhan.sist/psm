﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="LogoutTrackingReport.aspx.cs" Inherits="Reports_LogoutTrackingReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Logout Tracking</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Logout Tracking</a>
                        </li>
                            <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10056)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div> 
    </div>

    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">

                                        <input type="text" class="form-control border-primary input-sm date" id="txtUsername" placeholder="User Name" />

                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" id="txtFromDate" placeholder="From Date" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" id="txtToDate" placeholder="To Date" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-1">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" onclick="LogtrackingReport(1)" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content-body">
        <section id="drag-area3">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblLogtrackingReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SN text-center" style="width: 4%;">SN</td>
                                                        <td class="userName text-center tblwth">User Name</td>
                                                        <td class="accessToken tblwth">Access Token</td>
                                                        <td class="appversion tblwth">App Version</td>
                                                        <td class="deviceId price tblwth">Device Id</td>
                                                        <td class="LogoutTime tblwth">Logout Time</td>
                                                        <td class="Created tblwth">Created Date</td>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="PageSize" onchange="LogtrackingReport(1);">
                                            <option value="10" selected="selected">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> 
</asp:Content>

