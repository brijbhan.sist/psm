﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="WrongPackingDetailsReport.aspx.cs" Inherits="Reports_WrongPackingDetailsReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Wrong Packing Details Report</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Developer Help</a>
                        </li>
                        <li class="breadcrumb-item active">Wrong Packing Details Report
                        </li>
                    </ol>
                </div>

            </div>
        </div>
    </div>

     <div class="content-body">
        <section id="drag-area4">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-4 col-sm-12">
                                        <select class="form-control border-primary input-sm" id="ddlProduct">
                                            <option value="0">All Product</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" onclick="getWrongPackingDetailsReport(1)" data-animation="pulse">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </section>
        </div>

    <div class="content-body">
        <section id="drag-area3">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblWrongPackingDetailsReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td rowspan="2" class="text-center">Product Id</td>
                                                        <td rowspan="2" class="text-center">UnitType</td>
                                                        <td colspan="5" class="text-center">Quantity</td>
                                                    </tr>
                                                     <tr>
                                                        <td class="NJQty text-center">NJ Qty</td>
                                                        <td class="PAQty text-center">PA Qty</td>
                                                        <td class="NPAQty text-center">NPA Qty</td>
                                                        <td class="WPAQty text-center">WPA Qty</td>
                                                        <td class="CTQty text-center">CT Qty</td>
                                                    </tr>
                                                    <tr style="display:none">
                                                        <td class="ProductId text-center">Product Id</td>
                                                        <td class="UnitType text-center">UnitType</td>
                                                        <td class="NJQty text-center">NJ Qty</td>
                                                        <td class="PAQty text-center">PA Qty</td>
                                                        <td class="NPAQty text-center">NPA Qty</td>
                                                        <td class="WPAQty text-center">WPA Qty</td>
                                                        <td class="CTQty text-center">CT Qty</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" onchange="getWrongPackingDetailsReport(1)" id="ddlPageSize">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

