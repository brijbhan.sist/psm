﻿using DllLogin;
using DLLProductWiseCommissionReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ProductWiseCommissionReport : System.Web.UI.Page
{ 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/ProductWiseCommissionReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }

    [WebMethod(EnableSession = true)]
    public static string BindSalesPerson()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {

            try
            {
                PL_ProductWiseCommissionReport pobj = new PL_ProductWiseCommissionReport();
                BL_ProductWiseCommissionReport.selectSalesPerson(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;

            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string GetProductWiseCommissionList(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {

            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ProductWiseCommissionReport pobj = new PL_ProductWiseCommissionReport();
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.CommissionCode = jdv["CommissionCode"];
                if (jdv["CommissionCode"] == "-All Commission Code-")
                {
                    pobj.CommissionCode = "";
                }
                pobj.ProductId = jdv["ProductId"];
                pobj.ProductName = jdv["ProductName2"];
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);

                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_ProductWiseCommissionReport.GetReportDetail(pobj);

                return pobj.Ds.GetXml();

            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}