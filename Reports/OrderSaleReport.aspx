﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="OrderSaleReport.aspx.cs" Inherits="Reports_OrderSaleReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 9%;
        }

        .Pager {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Customer</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Sales Report</a>
                        </li>
                        <li class="breadcrumb-item">By Customer</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10045)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height: 400px;">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlAllSales" onchange="BindCustomer()">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlCustomerType" onchange="BindCustomer()">
                                            <option value="0">All Customer Type</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlAllCustomer">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlAllCategory" onchange="BindProductSubCategory()">
                                            <option value="0">All Category</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlAllSubCategory" onchange="BindProductDropdown()">
                                            <option value="0">All Subcategory</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control border-primary input-sm" id="ddlAllProduct">
                                            <option value="0">All Product</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" id="txtFromDate" placeholder="From Order Date" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" id="txtToDate" placeholder="To Order Date" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-1">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" onclick="OrderSaleReport(1)" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblOrderSaleReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="SN text-center" style="width: 3% !important">SN</td>
                                                        <td class="CustomerId text-center" style="width: 6% !important">Customer ID</td>
                                                        <td class="CustomerName left">Customer</td>
                                                        <td class="LastOrderDate" style="text-align: right; width: 6% !important">Last Order<br />
                                                            Date</td>
                                                        <td class="TotalOrder" style="text-align: center; width: 5% !important">Total Order</td>
                                                        <td class="TotalNetSales price" style="text-align: right; width: 6% !important">Total
                                                            <br />
                                                            Net Sales</td>
                                                        <td class="Tax price" style="text-align: right; width: 4% !important">Tax</td>
                                                        <td class="Shipping price" style="text-align: right; width: 5% !important">Shipping</td>
                                                        <td class="GrossSale price" style="text-align: right;">Gross Sales</td>
                                                        <td class="TotalDue" style="text-align: right; width: 5% !important">Total Due</td>
                                                        <td class="AOV" style="text-align: right; width: 4% !important">AOV</td>

                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="4">Total</td>
                                                        <td id="TTotalOrder" class="text-center">0</td>
                                                        <td id="TNetSale" class="right">0.00</td>
                                                        <td id="TTAxPrice" class="right">0.00</td>
                                                        <td id="TShippingPrice" class="right">0.00</td>
                                                        <td id="TGrossSale" class="right">0.00</td>
                                                        <td id="TTotalDue" class="right">0.00</td>
                                                        <td id="TAOV" class="right">0.00</td>
                                                    </tr>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="4">Overall Total</td>
                                                        <td id="TotalOrder" class="text-center">0</td>
                                                        <td id="TNetSales" class="right">0.00</td>
                                                        <td id="TTAxPrices" class="right">0.00</td>
                                                        <td id="TShippingPrices" class="right">0.00</td>
                                                        <td id="TGrossSales" class="right">0.00</td>
                                                        <td id="TotalDue" class="right">0.00</td>
                                                        <td id="AOV" class="right">0.00</td>
                                                    </tr>

                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="PageSize" onchange="OrderSaleReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Order Sale Report
                    <br />
                    <span class="text-center" style="font-size: 8px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>

        </div>
        <div style="display: none;" id="ExcelDiv"></div>
    </div>

    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

