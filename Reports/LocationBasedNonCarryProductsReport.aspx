﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="LocationBasedNonCarryProductsReport.aspx.cs" Inherits="Reports_NonCarryProductsReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Not Sold Report</h3>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Products</a></li>
                        <li class="breadcrumb-item">Manage Not Sold Report</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10066)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height: 400px">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSCategory" onchange="bindSubcategory()">
                                            <option value="0">All Category</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSSubcategory">
                                            <option value="0">All Subcategory</option>
                                        </select>
                                    </div>  
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" style="width:100% !important" id="ddlBrand">
                                            <option value="0">All Brand</option>
                                        </select>
                                    </div>
                                    <%--<div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control border-primary input-sm"  id="txtProductId" placeholder="Product ID/ Product Name" />                                          
                                        </div>
                                    </div>--%>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                             <input type="text" class="form-control border-primary input-sm" id="txtProductName" placeholder="Product ID/ Product Name"/>                                          
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <div class="input-group">
                                            <select class="form-control border-primary input-sm" id="ddlMonth">
                                                <option value="0">All Duration</option>
                                                <option value="1">1 Month</option>
                                                <option value="3">3 Month</option>
                                                <option value="6">6 Month</option>
                                                <option value="9">9 Month</option>
                                                <option value="12">12 Month</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" style="width:100% !important" id="ddlLocation">
                                            <option value="0">All Location</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick="getReport(1);">Search</button>
                                        <button type="button" class="btn btn-warning buttonAnimation round box-shadow-1  btn-sm" onclick="InactiveProduct()" data-animation="pulse" id="btnInactiveProduct" onclick="getReport(1);">Inactive Product</button>
                                 </div>
                                    <div class="col-md-3 form-group text-right">
                                        <span style="font-size:18px;color:red">Total Records : <b id="totalCount">0</b></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center width3per"><input type="checkbox" onclick="checkAll()" id="checkall" /></td>
                                                        <td class="ProductId text-center  width2per">Product ID</td>
                                                        <td class="Category left wth6">Category</td>
                                                        <td class="SubCategory left wth5">Subcategory</td>
                                                        <td class="ProductName left width-15-per">Product Name</td>
                                                        <td class="Brand left wth4">Brand</td>
                                                        <td class="CreateDate text-center  width2per">Create Date</td>
                                                        <td class="Date text-center  width4per">Last Order Date</td>
                                                        <td class="Location text-center  width4per">Location</td>
                                                        <td class="Status text-center  width2per" style="display:none">Status</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>

                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange=" getReport(1);">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <div id="ExcelDiv" style="display:none"></div>
    </div>

    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

