﻿using DLL_CustomerCreditReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_Customer_Credit_Report : System.Web.UI.Page
{
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/CustomerCreditReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindCustomer()
    {
        try
        {
            PL_CustomerCreditReport pobj = new PL_CustomerCreditReport();
            BL_CustomerCreditReport.BindCustomer(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0];
            }
            return json;
        }
        catch (Exception )
        {
            return "false";
        }
    }


    [WebMethod(EnableSession = true)]
    public static string GetCustomerCreditReport(string dataValue)
    {
        try
        {
            PL_CustomerCreditReport pobj = new PL_CustomerCreditReport();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            BL_CustomerCreditReport.GetCustomerCreditReport(pobj);
            return pobj.Ds.GetXml();
        }
        catch (Exception )
        {
            return "false";
        }
    }
}