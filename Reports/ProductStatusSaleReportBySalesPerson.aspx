﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/MasterPage.master" CodeFile="ProductStatusSaleReportBySalesPerson.aspx.cs" Inherits="Reports_ProductStatusSaleReportBySalesPerson" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .table th, .table td {
            padding: 0 5px 0 5px !important;
        }

        .Pager {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Product Status Sale Report By Sales Person</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                         <li class="breadcrumb-item"><a href="#">Product Report</a>
                        </li>
                        <li class="breadcrumb-item active">Product Status Sale Report By Sales Person
                        </li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10044)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12 col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                   Order From Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm req" onchange="setdatevalidation(1)" placeholder="From Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                  Order To Date  <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm req" onchange="setdatevalidation(2)" placeholder="To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-4 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlSalesPerson" onchange="bindCustomer()">
                                            <option value="0" selected="selected">All Sales Person</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-4 form-group">
                                        <select class="form-control input-sm border-primary" id="ddlCustomer">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-4 form-group">
                                        <select class="form-control input-sm border-primary ddlreq" id="ddlProduct">
                                            <option value="0">All Product</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-4 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick="getSearchdata();">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="content-body">
        <section id="drag-area3">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblProductStatusSaleReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="T_SalesRep text-center">Sales Person</td>
                                                        <td class="T_CustomerName">Customer Name</td>
                                                        <td class="T_ProductId text-center" style="width: 4% !important">Product Id</td>
                                                        <td class="T_ProductName">Product Name</td>
                                                        <td class="T_New text-center" style="text-align: center; width: 3% !important">New</td>
                                                        <td class="T_Processed text-center" style="text-align: center; width: 3% !important">Processed</td>
                                                        <td class="T_AddOn text-center" style="text-align: center; width: 3% !important">Add On</td>
                                                        <td class="T_Packed text-center" style="text-align: center; width: 3% !important">Packed</td>
                                                        <td class="T_AddOnPacked text-center" style="text-align: center; width: 3% !important">Add On<br />
                                                            Packed</td>
                                                        <td class="T_ReadyToShip text-center" style="text-align: center;">Ready
                                                            <br />
                                                            To Ship</td>
                                                        <td class="T_Shipped text-center" style="text-align: center; width: 3% !important">Shipped</td>
                                                        <td class="T_Delivered text-center" style="text-align: center; width: 3% !important">Delivered</td>
                                                        <td class="T_Closed text-center" style="text-align: center; width: 3% !important">Close</td>
                                                        <td class="T_Total text-center" style="text-align: center; width: 3% !important">Total</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="4" style="text-align: right; font-weight: bold"><b>Total</b></td>
                                                        <td style="text-align: center; font-weight: bold" id="tNew">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="tProcessed">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="tAddOn">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="tPacked">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="tAddOnPacked">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="tReadyToShip">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="tShipped">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="tDelivered">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="tClose">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="tTotal">0</td>
                                                    </tr>
                                                    <tr id="trTotal">
                                                        <td style="text-align: right; font-weight: bold" colspan="4"><b>Overall Total</b></td>
                                                        <td style="text-align: center; font-weight: bold" id="oNew">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="oProcessed">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="oAddOn">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="oPacked">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="oAddOnPacked">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="oReadyToShip">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="oShipped">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="oDelivered">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="oClose">0</td>
                                                        <td style="text-align: center; font-weight: bold" id="oTotal">0</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-2" >
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="BindReport(1);">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Product Status Sale Report By Sales Person
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead>
                    <tr>
                        <td class="T_SalesRep text-left">Sales Person</td>
                        <td class="T_CustomerName text-left">Customer Name</td>
                        <td class="T_ProductId text-center" style="width: 4% !important">Product Id</td>
                        <td class="T_ProductName text-left">Product Name</td>
                        <td class="T_New text-center" style="text-align: center; width: 3% !important">New</td>
                        <td class="T_Processed text-center" style="text-align: center; width: 3% !important">Processed</td>
                        <td class="T_AddOn text-center" style="text-align: center; width: 3% !important">Add On</td>
                        <td class="T_Packed text-center" style="text-align: center; width: 3% !important">Packed</td>
                        <td class="T_AddOnPacked text-center" style="text-align: center; width: 4% !important">Add On Packed</td>
                        <td class="T_ReadyToShip text-center" style="text-align: center; width: 4% !important">Ready To Ship</td>
                        <td class="T_Shipped text-center" style="text-align: center; width: 3% !important">Shipped</td>
                        <td class="T_Delivered text-center" style="text-align: center; width: 3% !important">Delivered</td>
                        <td class="T_Closed text-center" style="text-align: center; width: 3% !important">Close</td>
                        <td class="T_Total text-center" style="text-align: center; width: 3% !important">Total</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" style="text-align: right; font-weight: bold"><b>Total</b></td>
                        <td style="text-align: center; font-weight: bold" id="ptNew">0</td>
                        <td style="text-align: center; font-weight: bold" id="ptProcessed">0</td>
                        <td style="text-align: center; font-weight: bold" id="ptAddOn">0</td>
                        <td style="text-align: center; font-weight: bold" id="ptPacked">0</td>
                        <td style="text-align: center; font-weight: bold" id="ptAddOnPacked">0</td>
                        <td style="text-align: center; font-weight: bold" id="ptReadyToShip">0</td>
                        <td style="text-align: center; font-weight: bold" id="ptShipped">0</td>
                        <td style="text-align: center; font-weight: bold" id="ptDelivered">0</td>
                        <td style="text-align: center; font-weight: bold" id="ptClose">0</td>
                        <td style="text-align: center; font-weight: bold" id="ptTotal">0</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>


