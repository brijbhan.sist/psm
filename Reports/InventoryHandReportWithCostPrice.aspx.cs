﻿using DllInventoryHandReportWithCostPrice;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_InventoryHandReport : System.Web.UI.Page
{

    string Body = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/InventoryHandReportWithCostPrice.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }

    [WebMethod(EnableSession = true)]
    public static string bindCategory()
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            PL_Product pobj = new PL_Product();
            try
            {
                BL_Product.bindCategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindSubcategory(int categoryAutoId)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            PL_Product pobj = new PL_Product();
            try
            {
                pobj.CategoryAutoId = categoryAutoId;
                BL_Product.bindSubcategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if(string.IsNullOrEmpty(json))
                {
                    json = "[]";
                }
                return json;
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getProductList(string dataValue)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {


            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Product pobj = new PL_Product();
            try
            {
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                pobj.ProductId = jdv["ProductId"];
                pobj.Barcode = jdv["BarCode"];
                pobj.ProductName = jdv["ProductName"];
                pobj.BrandAutoId = Convert.ToInt32(jdv["BrandName"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_Product.select(pobj);

                return pobj.Ds.GetXml();

            }
            catch (Exception )
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}