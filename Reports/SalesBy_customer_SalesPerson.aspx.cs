﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllSalesByCustomerSalesPerson;
public partial class Reports_SalesBySalesPerson : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/SalesByCustomerSalesPerson.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindSalesPerson()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SalesByCustomerSalesPerson pobj = new PL_SalesByCustomerSalesPerson();
                BL_SalesByCustomerSalesPerson.BindSalesPerson(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string SaleByCustomerSalesPersonDetail(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SalesByCustomerSalesPerson pobj = new PL_SalesByCustomerSalesPerson();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.SalesPerson = jdv["SalesPerson"].ToString();
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                pobj.PageSize = Convert.ToInt16(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt16(jdv["PageIndex"]);
                BL_SalesByCustomerSalesPerson.SaleByCustomerSalesPersonDetail(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}