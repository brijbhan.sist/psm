﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLOrderSubmitTimeReport;

public partial class Reports_OrderSubmitTimeReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/OrderSubmitTimeReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetOrderSubmitReport(string dataValue)
    {
        try
        {
            PL_OrderSubmitTimeReport pobj = new PL_OrderSubmitTimeReport();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            if (jdv["DateFrom"] != null && jdv["DateFrom"] != "")
            {
                pobj.FromDate = Convert.ToDateTime(jdv["DateFrom"]);
            }
            if (jdv["DateTo"] != null && jdv["DateTo"] != "")
            {
                pobj.ToDate = Convert.ToDateTime(jdv["DateTo"]);
            }
            pobj.SalePersonAutoId = Convert.ToInt32(jdv["PackerAutoId"]);
            pobj.RouteStatus = Convert.ToInt32(jdv["RouteStatus"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            BL_OrderSubmitTimeReport.GetOrderSubmit(pobj);

            return pobj.Ds.GetXml();

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public static string CutomerName()
    {
        try
        {
            PL_OrderSubmitTimeReport pobj = new PL_OrderSubmitTimeReport();

            BL_OrderSubmitTimeReport.GetCustomer(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0];
            }
            return json;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}