﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllProductListReport;
public partial class Reports_ProductListReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/ProductListReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindCategory()
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            PL_ProductListReport pobj = new PL_ProductListReport();
            try
            {
                BL_ProductListReport.bindCategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindSubCategory(string categoryAutoId)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            PL_ProductListReport pobj = new PL_ProductListReport();
            pobj.CategoryAutoId = Convert.ToInt32(categoryAutoId);
            try
            {
                BL_ProductListReport.bindSubCategory(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string getProductList(string dataValue)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {


            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ProductListReport pobj = new PL_ProductListReport();
            try
            {
                pobj.Websitedispaly = Convert.ToInt32(jdv["WebsiteDisplay"]);                           
                pobj.CategoryAutoId = Convert.ToInt32(jdv["Category"]);                           
                pobj.SubCategoryAutoId = Convert.ToInt32(jdv["SubCategory"]);                           
                pobj.BrandAutoId = Convert.ToInt32(jdv["Brand"]);                           
                pobj.ProductId = Convert.ToInt32(jdv["Brand"]);                           
                pobj.ProductName = jdv["ProductName"];
                if(jdv["ProductId"]!="")
                {
                    pobj.ProductId = Convert.ToInt32(jdv["ProductId"]);
                }               
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);             
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_ProductListReport.getProductList(pobj);
                if (!pobj.isException)
                {
                   
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string UpdateWebsiteStatus(string dataValue)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {


            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ProductListReport pobj = new PL_ProductListReport();
            try
            {
                pobj.ShowOnWebsite = Convert.ToInt32(jdv["ShowOnWebsite"]);
                pobj.ProductId = Convert.ToInt32(jdv["ProductId"]);
                pobj.EmpAutoId =Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_ProductListReport.updateStatus(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}