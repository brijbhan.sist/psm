﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="OrderSummaryReport.aspx.cs" Inherits="Reports_OrderSummaryReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        table tbody tr th {
            background: #286090;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="alert alert-success alert-dismissable fade in" id="alertSuccessDelete" style="display: none;">
        <a aria-label="close" id="successDeleteClose" class="close" style="cursor: pointer;">&times;</a>
        <span></span>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" style="text-align: center;">
            <b>Order Summary Report</b>
        </div>
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="table-responsive">
                        <div id="tableorder" runat="server" class="table table-striped table-bordered" style="width:100%">
                        </div>
                    </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>

