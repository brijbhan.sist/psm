﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="OrderAverageReport.aspx.cs" Inherits="Reports_OrderAverageReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 8.5%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order Average</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a> </li>

                        <li class="breadcrumb-item"><a href="#">Reports</a> </li>
                        <li class="breadcrumb-item"><a href="#">Sale Report</a> </li>
                        <li class="breadcrumb-item">Order Average</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10049)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>


    <div class="content-body" style="min-height: 400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">

                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="From Order Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="To Order Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <select id="ddlEmpType" onchange="Employee()" class="form-control border-primary input-sm">
                                                <option value="0">All User Type</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="form-group">
                                            <select id="ddlEmployee" class="form-control border-primary input-sm">
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-1">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch" onclick="GetOrderAverageReport();">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblOrderAverageReport">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td colspan="2" class="text-center">Sales Person : New Orders	</td>
                                                        <td colspan="2" class="text-center">Packer : Packed Orders	</td>
                                                        <td colspan="2" class="text-center">Delivered Orders</td>
                                                        <td colspan="2" class="text-center">Account : Closed Orders</td>
                                                        <td colspan="2" class="text-center">Cancel Orders</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="WDay">Week Day</td>
                                                        <td class="WDCount text-center tblwth">Week Day Count</td>
                                                        <td class="TOrders text-center tblwth">Total Orders</td>
                                                        <td class="AWOrders text-center tblwth">Avg. Weekly Orders</td>
                                                        <td class="PTotal text-center tblwth">Total Orders</td>
                                                        <td class="PAvg text-center tblwth">Avg. Weekly Orders</td>
                                                        <td class="DTotal text-center tblwth">Total Orders</td>
                                                        <td class="DAvg text-center tblwth">Avg. Weekly Orders</td>
                                                        <td class="ATotal text-center tblwth">Total Orders</td>
                                                        <td class="AAvg text-center tblwth">Avg. Weekly Orders</td>
                                                        <td class="CTotal text-center tblwth">Total Orders</td>
                                                        <td class="CAvg text-center tblwth">Avg. Weekly Orders</td>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                            <table id="tblOrderAverageExportReport" class="table table-striped table-bordered well" style="display: none;">

                                                <thead>
                                                    <tr>
                                                        <td class="WDay"></td>
                                                        <td class="WDCount"></td>
                                                        <td class="TOrders"></td>
                                                        <td class="AWOrders"></td>

                                                    </tr>
                                                    <tr style="background-color: gray">
                                                        <td colspan="5" style="text-align: center">
                                                            <b>Report Name: </b>Order Average Report<br />
                                                            <b>Download Date:</b><span id="LblDate"></span><br />
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="color: #fff; background-color: #009688 !important;" class="WDay">Week Day</td>
                                                        <td style="color: #fff; background-color: #009688 !important;" class="WDCount">Week Day Count</td>
                                                        <td style="color: #fff; background-color: #009688 !important;" class="TOrders">Total Orders</td>
                                                        <td style="color: #fff; background-color: #009688 !important;" class="AWOrders">Avg. Weekly Orders</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="js/OrderAverageReport.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

