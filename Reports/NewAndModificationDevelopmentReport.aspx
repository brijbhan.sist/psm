﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="NewAndModificationDevelopmentReport.aspx.cs" Inherits="Reports_NewAndModificationDevelopmentReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">New And Modification Development Report</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" id="NewAndModification">
                            <h4 style="margin-bottom: 0 !important;"><b>New And Modification Development </b></h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblPaymentDailyReportLog">
                                                <thead>
                                                    <tr>
                                                        <td class="Date text-left">Date</td>
                                                        <td class="Priority text-center">Priority</td>
                                                        <td class="Type text-center">Type</td>
                                                        <td class="Task  price right">Task Description</td>
                                                        <td class="Status  price right">Status</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>9/22/2020</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>Invoice print document</td>
                                                        <td></td>
                                                    </tr> 
                                                    <tr>
                                                        <td>9/30/2020</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>Driver log discuss</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>9/30/2020</td>
                                                        <td></td>
                                                        <td>City Master</td>
                                                        <td>Auto city Name add</td>
                                                        <td></td>
                                                    </tr>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>

</asp:Content>

