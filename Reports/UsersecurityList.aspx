﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="UsersecurityList.aspx.cs" Inherits="Reports_UsersecurityList" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .zoom:hover {
            -ms-transform: scale(3); /* IE 9 */
            -webkit-transform: scale(3); /* Safari 3-8 */
            transform: scale(3);
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">User Security List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Developer</a></li>
                        <li class="breadcrumb-item active">User Security List</li>

                    </ol>
                </div>
            </div>
        </div>
        <input type="hidden" id="HDDomain" runat="server" />
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12 ">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblUsersecurityList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="TypeName text-center">Type Name</td>
                                                        <td class="SecurityValue text-center">Security Value</td>
                                                        <td class="Description">Description</td>
                                                        <td class="InteralRemark text-right">Internal Remark</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    </div>
    <input type="hidden" id="hiddenForPacker" runat="server" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Reports/JS/UsersecurityList.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />

</asp:Content>

