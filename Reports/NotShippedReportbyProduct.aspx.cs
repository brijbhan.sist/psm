﻿using DLLNotShippedReportbyProduct;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_NotShippedReportbyProduct : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/NotShippedReportbyProduct.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }

    [WebMethod(EnableSession = true)]
    public static string BindStatussalesperson()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {

                PL_NotShippedReportbyProduct pobj = new PL_NotShippedReportbyProduct();
                BL_NotShippedReportbyProduct.BindStatussalesperson(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Timeout";
        }
    }
    
    [WebMethod(EnableSession = true)]
    public static string getNotShippedReport(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_NotShippedReportbyProduct pobj = new PL_NotShippedReportbyProduct();
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.SortBy = Convert.ToInt32(jdv["SortBy"]);
                pobj.SortOrder = Convert.ToInt32(jdv["SortOrder"]);
                pobj.SalsePersonAutoid = Convert.ToInt32(jdv["SalespersonAutoId"]);
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_NotShippedReportbyProduct.BindNotShipedReport(pobj);

                return pobj.Ds.GetXml();

            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Timeout";
        }
    }


    [WebMethod(EnableSession = true)]
    public static string getNotShipedReportExport(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_NotShippedReportbyProduct pobj = new PL_NotShippedReportbyProduct();

                pobj.Status = Convert.ToInt32(jdv["Status"]);
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.SalsePersonAutoid = Convert.ToInt32(jdv["SalespersonAutoId"]);

                BL_NotShippedReportbyProduct.NotShipedReportExport(pobj);

                return pobj.Ds.GetXml();

            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Timeout";
        }
    }
}