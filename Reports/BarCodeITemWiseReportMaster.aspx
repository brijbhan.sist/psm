﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="BarCodeITemWiseReportMaster.aspx.cs" Inherits="Admin_BarCodeITemWiseReportMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #BarCodeITemWiseReportMaster tfoot{text-align:center}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Barcode Item Wise Report</h3>
             <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Barcode</a></li>
                        <li class="breadcrumb-item">Barcode Item Wise Report</li>
                            <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10055)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Barcode Details</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row  form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="BarCodeITemWiseReportMaster">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Barcode">Barcode</td>
                                                        <td class="PSMNJUnit">PSMNJ Unit</td>
                                                        <td class="PSMNJProductId">Product ID</td>
                                                        <td class="PSMPAUnit">PSMPA Unit</td>
                                                        <td class="PSMPAProductId">PSMPA Product ID</td>
                                                        <td class="PSMCTUnit">PSMCT Unit</td>
                                                        <td class="PSMCTProductId">PSMCT  Product ID</td>
                                                        <td class="PSMNPAUnit">PSMNPA Unit</td>
                                                        <td class="PSMNPAProductId">PSMNPA Product ID</td>
                                                        <td class="PSMWPAUnit">PSMWPA Unit</td>
                                                        <td class="PSMWPAProductId">PSMWPA Product ID</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td><b>Total</b></td>
                                                        <td colspan="2">
                                                            <b id="njtotalbarcode"></b>
                                                        </td>
                                                        <td colspan="2">
                                                             <b id="patotalbarcode"></b>
                                                        </td>
                                                        <td colspan="2">
                                                             <b id="cttotalbarcode"></b>
                                                        </td>
                                                        <td colspan="2">
                                                             <b id="npatotalbarcode"></b>
                                                        </td>
                                                        <td colspan="2">
                                                             <b id="wpatotalbarcode"></b>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm pagesize" id="ddlPagesize">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="10000">1000</option>                                           
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="js/BarCodeITemWiseReportMaster.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

