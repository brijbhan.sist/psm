﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLBarCodeITemWiseReportMaster;

public partial class Admin_BarCodeITemWiseReportMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    public static string LocationwiseBarCodeReport(string dataValue)
    {
        PL_BarCodeITemWiseReportMaster pobj = new PL_BarCodeITemWiseReportMaster();
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        pobj.Opcode = 41;
        pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
        pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
        DL_BarCodeITemWiseReportMaster.ReturnTable(pobj);
        return pobj.Ds.GetXml();
    }
}