﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ItemWiseReport.aspx.cs" Inherits="Reports_ItemWiseReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblProductSalesStock tbody .ProductId, .Peice, .Box, .Case, .AvailableQty {
            text-align: center;
        }

        .tblwth {
            width: 13%;
        }

        .Pager {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Pick Product</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Reports</a></li>
                        <li class="breadcrumb-item"><a href="#">Packer Report</a></li>
                        <li class="breadcrumb-item">By Pick Product</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10034)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport">Export</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body" style="min-height: 400px;">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">From Order Date <span class="la la-calendar-o"></span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm  date" placeholder="From Order Date" id="txtDateFrom" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">To Order Date <span class="la la-calendar-o"></span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm date" placeholder="To Order Date" id="txtDateTo" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm" id="ddlProduct">
                                                <option value="0">All Product</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control border-primary input-sm" id="ddlStatus">
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblProductSalesStock">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="ProductId tblwth">Product ID</td>
                                                        <td class="ProductName left">Product Name</td>
                                                        <td class="Peice tblwth">Piece</td>
                                                        <td class="Box tblwth">Box</td>
                                                        <td class="Case tblwth">Case</td>
                                                        <td class="AvailableQty tblwth">Available Qty</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="2" class="text-center">Total</td>
                                                        <td id="Totalpeice" class="text-center">0</td>
                                                        <td id="TotalBox" class="text-center">0</td>
                                                        <td id="Totalcase" class="text-center">0</td>
                                                        <td id="TotalAvlQty" class="text-center">0</td>
                                                    </tr>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="2" class="text-center">Overall Total</td>
                                                        <td id="Totalpeices" class="text-center">0</td>
                                                        <td id="TotalBoxs" class="text-center">0</td>
                                                        <td id="Totalcases" class="text-center">0</td>
                                                        <td id="TotalAvlQtys" class="text-center">0</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control border-primary input-sm pagesize" id="ddlPageSize" onchange="ProductWiseSalesStockReport(1)">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    By Pick Product
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="P_ProductId text-center tblwth">Product ID</td>
                        <td class="P_ProductName left">Product Name</td>
                        <td class="P_Peice text-center tblwth">Piece</td>
                        <td class="P_Box text-center tblwth">Box</td>
                        <td class="P_Case text-center tblwth">Case</td>
                        <td class="P_AvailableQty text-center tblwth">Available Qty</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" style="font-weight: bold;" class="text-center">Overall Total</td>
                        <td id="Totalpeicess" style="font-weight: bold;" class="text-center">0</td>
                        <td id="TotalBoxss" style="font-weight: bold;" class="text-center">0</td>
                        <td id="Totalcasess" style="font-weight: bold;" class="text-center">0</td>
                        <td id="TotalAvlQtyss" style="font-weight: bold;" class="text-center">0</td>
                    </tr>

                </tfoot>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="js/ItemWiseReport.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

