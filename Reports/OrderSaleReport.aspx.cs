﻿using DLLOrderSaleReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_OrderSaleReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/OrderSaleReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindSalesPerson()
    {
        try
        {
            PL_OrderSaleReport pobj = new PL_OrderSaleReport();
            BL_OrderSaleReport.bindDropdown(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0];
            }
            return json;
        }
        catch
        {
            return "false";
        }

    }
    [WebMethod(EnableSession = true)]
    public static string BindCustomer(string Customertype, string SalesPersonAutoId)
    {

        try
        {
            PL_OrderSaleReport pobj = new PL_OrderSaleReport();
            pobj.CustomerType = Convert.ToInt32(Customertype);
            pobj.SalesPerson = Convert.ToInt32(SalesPersonAutoId);
            BL_OrderSaleReport.bindCustomer(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0].ToString();
            }
            if (json == "")
            {
                json = "[]";
            }
            return json;

        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindProductSubCategory(string CategoryAutoId)
    {

        try
        {
            PL_OrderSaleReport pobj = new PL_OrderSaleReport();
            pobj.Category = Convert.ToInt32(CategoryAutoId);
            BL_OrderSaleReport.BindProductSubCategory(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0].ToString();
            }
            if (json == "")
            {
                json = "[]";
            }
            return json;

        }
        catch (Exception)
        {
            return "false";
        }

    }
    [WebMethod(EnableSession = true)]
    public static string BindProduct(string CategoryAutoId, string SubcategoryAutoId)
    {

        try
        {
            PL_OrderSaleReport pobj = new PL_OrderSaleReport();
            pobj.SubCategory = Convert.ToInt32(SubcategoryAutoId);
            pobj.Category = Convert.ToInt32(CategoryAutoId);
            BL_OrderSaleReport.BindProduct(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0].ToString();
            }
            if (json == "")
            {
                json = "[]";
            }
            return json;

        }
        catch (Exception)
        {
            return "false";
        }
    }


    [WebMethod(EnableSession = true)]
    public static string OrderSaleReport(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_OrderSaleReport pobj = new PL_OrderSaleReport();
        try
        {
            pobj.CustomerType = Convert.ToInt32(jdv["CustomerType"]);
            pobj.Customer = Convert.ToInt32(jdv["Customer"]);
            pobj.SalesPerson = Convert.ToInt32(jdv["SalesPerson"]);
            pobj.Category = Convert.ToInt32(jdv["Category"]);
            pobj.SubCategory = Convert.ToInt32(jdv["SubCategory"]);
            pobj.Product = Convert.ToInt32(jdv["Product"]);
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

            BL_OrderSaleReport.OrderSaleReport(pobj);
            return pobj.Ds.GetXml();
        }
        catch (Exception ex)
        {
            return "false";
        }
    }
}