﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using DllUtility;
public partial class Reports_OrderSummaryReport : System.Web.UI.Page
{
    Config connect = new Config();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            try
            {
                DataTable dt = this.GetReport();
                StringBuilder html = new StringBuilder();
                html.Append("<table class='table table-striped table-bordered'>");
                html.Append("<tr>");
                foreach (DataColumn column in dt.Columns)
                {
                    html.Append("<th>");
                    html.Append(column.ColumnName);
                    html.Append("</th>");
                }
                html.Append("</tr>");
                foreach (DataRow row in dt.Rows)
                {
                    html.Append("<tr>");
                    foreach (DataColumn column in dt.Columns)
                    {
                        html.Append("<td>");
                        html.Append(row[column.ColumnName]);
                        html.Append("</td>");
                    }
                    html.Append("</tr>");
                }
                html.Append("</table>");
                tableorder.Controls.Add(new Literal { Text = html.ToString() });
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }
    }
    private DataTable GetReport()
    {
        DataTable dt = new DataTable();
        try
        {
            SqlCommand cmd = new SqlCommand("procOrderSummaryReport", connect.con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            dt = new DataTable();
            sda.Fill(dt);
        }
        catch (Exception ex)
        {
            ex.Message.ToString();
        }
        return dt;
    }
}