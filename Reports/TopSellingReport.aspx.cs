﻿using DLL_TopSellingReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_TopSellingReport : System.Web.UI.Page
{
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/TopSellingReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }

    [WebMethod(EnableSession = true)]
    public static string BindSalesPerson()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_TopSaleReport pobj = new PL_TopSaleReport();
            BL_TopSaleReport.BindSalesPerson(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0];
            }
            return json;
        }
        else
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string TopsellingReport(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_TopSaleReport pobj = new PL_TopSaleReport();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.SalesPersons = jdv["SalesPersons"];
                pobj.CustomerType = Convert.ToInt32(jdv["CustomerType"]);
                pobj.Type = Convert.ToInt32(jdv["Type"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                BL_TopSaleReport.BindTopSellingReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}