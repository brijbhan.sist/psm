﻿<%@ Page Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ProductFreeReport.aspx.cs" Inherits="Reports_ProductFreeReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <style>
        .tblwth{
            width:5%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Free</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Product Report</a>
                        </li>
                        <li class="breadcrumb-item active">By Free
                        </li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10042)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport">Export</button>
                </div>
                <%--<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item">Export</button>                  
                </div>--%>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area2">
            <div class="row form-group">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                      <div class="col-sm-12 col-md-4">
                                        <select class="form-control input-sm" id="ddlSalesPerson" onchange="BindCustomer()">
                                            <option value="0">All Sales Person</option>
                                        </select>
                                    </div>
                                      <div class="col-sm-12 col-md-4">
                                        <select class="form-control input-sm" id="ddlCustomer">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                   
                                        <div class="col-sm-12 col-md-4">
                                        <select class="form-control input-sm" id="ddlStatus">
                                            <option value="0">All Status</option>
                                        </select>
                                    </div>
                                   
                                  
                                </div>
                                <div class="row form-group">
                                     <div class="col-sm-12 col-md-4">
                                        <select class="form-control input-sm" id="ddlCategory" onchange="BindSubcategory();">
                                            <option value="0">All Category</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <select class="form-control input-sm" id="ddlSubcategory" >
                                            <option value="0">All Subcategory</option>
                                        </select>
                                    </div>
                                 <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary date" onchange="setdatevalidation(1)" placeholder="From Order Date" id="txtFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    </div>
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                 <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary date" onchange="setdatevalidation(2)" placeholder="To Order Date" id="txtToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-1">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1 pull-left  btn-sm" id="btnSearch">Search</button>
                                    </div>
                                </div>
                                <%--<div class="row form-group">

                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <table class="MyTableHeader" id="tblProductExchangeReport">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="CustomerId" style="width:8%;">Customer ID</td>
                                                <td class="CustomerName left">Customer Name</td>
                                                <td class="OrderNo" style="width:8%;">Order No</td>
                                                <td class="OrderDate" style="width:8%;">Order Date</td>
                                                <td class="SalesPerson left" style="width:10%;">Sales Person</td>
                                                <td class="ProductId text-center" style="width:7%;">Product ID</td>
                                                <td class="ProductName left">Product Name</td>
                                                <td class="Unit text-center tblwth">Unit</td>
                                                <td class="OrderQty text-center tblwth">Order Qty</td>
                                                <td class="ProductPrice price tblwth" style="text-align:right;">Product Price</td>
                                                <td class="TotalAmmount price tblwth" style="text-align:right;">Total Amount</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr style="font-weight:bold">
                                                <td colspan="10"  class="text-right">Total Amount</td>                                              
                                                <td id="TotalAmount" class="right">0.00</td>
                                            </tr>
                                            <tr style="font-weight:bold">
                                                <td colspan="10" class="text-right">Overall Amount</td>
                                                <td id="OverAllAmount" class="right">0.00</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                        </div>
                                    <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>

                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="table-responsive" style="display: none;" id="PrintTable1">
              <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
               Product Report By Free
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="P_CustomerId text-center">Customer ID</td>
                        <td class="P_CustomerName left">Customer Name</td>
                        <td class="P_OrderNo text-center">Order No</td>
                        <td class="P_OrderDate text-center">Order Date</td>
                        <td class="P_SalesPerson left">Sales Person</td>
                        <td class="P_ProductId text-center">Product ID</td>
                        <td class="P_ProductName left">Product Name</td>
                        <td class="P_Unit text-center">Unit</td>
                        <td class="P_OrderQty text-center">Order Qty</td>
                        <td class="P_ProductPrice" style="text-align:right;">Unit Price</td>
                        <td class="P_TotalAmmount" style="text-align:right;">Total Amount</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10" style="font-weight:bold" class="text-right">Total Amount</td>
                        <td id="TotalAmountPrint" style="font-weight:bold" class="right">0.00</td>
                    </tr>                    
                </tfoot>

            </table>
        </div>

    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="js/ProductFreeReport.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

