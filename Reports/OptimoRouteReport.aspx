﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="OptimoRouteReport.aspx.cs" Inherits="Reports_OptimoRouteReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 17%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Optimo Route Report</h3>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>

                        <li class="breadcrumb-item"><a href="#">Reports</a></li>

                        <li class="breadcrumb-item">Optimo Route Report</li>

                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport">Export</button>
                </div>
                <%--   <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport">Export</button>
                </div>--%>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="row container-fluid">
                                        <div class="col-md-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class="la la-calendar-o"></span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm date" placeholder="From Date" id="txtSFromDate" onfocus="this.select()" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="padding: 0rem 1rem;">
                                                        <span class="la la-calendar-o"></span>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control border-primary input-sm date" placeholder="To Date" id="txtSToDate" onfocus="this.select()" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 form-group">
                                            <select class="form-control border-primary input-sm" id="ddlSalesPerson">
                                                <option value="0">All Sales Person</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 form-group">
                                            <select class="form-control border-primary input-sm" id="ddlDriver" style="width: 100%;">
                                                <option value="0">All Driver</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3 form-group">
                                            <select class="form-control border-primary input-sm" id="ddlStatus" style="width: 100%;">
                                                <option value="0">All Status</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3 form-group">
                                            <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch">Search</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">

                                <div class="row form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="OrderNo center">OrderNo</td>
                                                        <td class="DeliveryDate text-center">Delivery Date</td>
                                                        <td class="Addres left">Address</td>
                                                        <td class="PayableAmount price right">Payable Amount</td>
                                                        <td class="PackedBoxes center">Packed Boxes</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>

                                                <tfoot>
                                                    <tr>
                                                        <td colspan="3">Total</td>
                                                        <td id="TotalPaidd" class="right">0.00</td>
                                                        <td id="AmtDued" class="ceter">0.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">Overall Total</td>
                                                        <td id="PayableAmount" class="right">0.00</td>
                                                        <td id="PackedBoxes" class="center">0.00</td>
                                                    </tr>

                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="ddlPaging" class="form-control input-sm border-primary pagesize">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Optimo Route Report
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable1">
                <thead>
                    <tr>
                        <td class="POrderNo center">OrderNo</td>
                        <td class="PDeliveryDate text-center">Delivery Date</td>
                        <td class="PAddres left">Address</td>
                        <td class="PPayableAmount price right">Payable Amount</td>
                        <td class="PPackedBoxes center">Packed Boxes</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3">Total</td>
                        <td id="PTotalPaidd" class="right">0.00</td>
                        <td id="PAmtDued" class="ceter">0.00</td>
                    </tr>
                </tfoot>
            </table>
        </div>

    </div>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="JS/OptimoRouteReport.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</asp:Content>

