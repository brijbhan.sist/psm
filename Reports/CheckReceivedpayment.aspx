﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="CheckReceivedpayment.aspx.cs" Inherits="Reports_CheckReceivedpayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">By Received Check</h3>
            <input type="hidden" id="hiddenPackerAutoId" runat="server" />
            <input type="hidden" id="hiddenEmpType" runat="server" />
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>

                        <li class="breadcrumb-item"><a href="#">Reports</a></li>

                        <li class="breadcrumb-item"><a href="#">Account Report</a></li>

                        <li class="breadcrumb-item active">By Received Check</li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10013)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height:400px">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;"> From Check Received Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" onchange="setdatevalidation(1)" placeholder="Check Received From Date" id="txtFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">To Check Received Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" onchange="setdatevalidation(2)" placeholder="Check Received To Date" id="txtToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlCustomer" onclick=" BindCustomer();">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>


                                </div>
                                <div class="row">
                                  
                                    <div class="col-md-4 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;"> From Check Deposit Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date"  onchange="setdatevalidation(3)" placeholder="Check Deposit From Date" id="txtDepositFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">To Check Deposit Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date"  onchange="setdatevalidation(4)" placeholder="Check Deposit To Date" id="txtDepositToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                      <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlStatus">
                                            <option value="0">All Status</option>
                                            <option value="1">New</option>
                                            <option value="2">Settled</option>
                                            <option value="3">Cancelled</option>
                                            <option value="4">Hold</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlReceivedBy">
                                            <option value="0">All Received By</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-left round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick="BindReport(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">

                                <div class="row form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblCheckReceivedPayment">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="PaymentId text-center wth5">Payment ID</td>
                                                        <td class="ReceivedDate text-center  wth5">Received Date</td>
                                                        <td class="CustomerId text-center  wth5">Customer ID</td>
                                                        <td class="CustomerName left">Customer Name</td>
                                                        <td class="CheckAmount right  wth5">Check Amount</td>
                                                        <td class="CheckNoRefNo text-center wth8">Check No / Ref.No</td>
                                                        <td class="ReceivedBy left">Received By</td>
                                                        <td class="SettelledBy left">Settled By</td>
                                                        <td class="DepositDate text-center wth6">Deposit Date</td>
                                                        <td class="SettelledDate text-center wth5">Settled Date</td>
                                                        <td class="StatusType text-center wth5">Status</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                    <tr style="font-weight: bold" id="trTotal">
                                                        <td class="right" colspan="4">Total Amount</td>
                                                        <td class="right" id="TotalAmount"></td>
                                                        <td colspan="6"></td>
                                                    </tr>
                                                    <tr style="font-weight: bold">
                                                        <td class="right" colspan="4">Overall Amount</td>
                                                        <td class="right" id="TotalAmounts"></td>
                                                        <td colspan="6"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="ddlPaging" class="form-control input-sm border-primary pagesize" onchange="BindReport(1);">
                                            <option selected="selected" value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" style="padding: 5px" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    By Received Check
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; line-height: 1; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead>
                    <tr>
                        <td class="P_PaymentId text-center wth5">Payment ID</td>
                        <td class="P_ReceivedDate text-center  wth5">Received Date</td>
                        <td class="P_CustomerId text-center  wth5">Customer ID</td>
                        <td class="P_CustomerName left">Customer Name</td>
                        <td class="P_CheckAmount right  wth5">Check Amount</td>
                        <td class="P_CheckNoRefNo text-center wth8">Check No / Ref.No</td>
                        <td class="P_ReceivedBy left">Received By</td>
                        <td class="P_SettelledBy left">Settled By</td>
                        <td class="P_DepositDate text-center wth5">Deposit Date</td>
                        <td class="P_SettelledDate text-center wth5">Settled Date</td>
                        <td class="P_StatusType text-center wth5">Status</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" style="text-align: center; font-weight: bold">Amount</td>
                        <td class="text-right" id="P_TotalAmount" style="font-weight: bold"></td>
                        <td colspan="6"></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

</asp:Content>

