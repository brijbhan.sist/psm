﻿using DllCustomerStatementReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class customerStatementReport : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Reports/JS/CustomerStatementReport.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }

    [WebMethod(EnableSession = true)]
    public static string BindCustomerList()
    {
        try
        {
            PL_CustomerStatementReport pobj = new PL_CustomerStatementReport();
            BL_CustomerStatementReport.BindCustomerList(pobj);
            string json = "";
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                json += dr[0];
            }
            return json;
        }
       catch(Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindCustomerStatementReport(string dataValue)
    {
        try
        {
            PL_CustomerStatementReport pobj = new PL_CustomerStatementReport();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            pobj.CustomerList = jdv["CustomerList"].ToString();
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            BL_CustomerStatementReport.SelectCustomerStatementReport(pobj);
            return pobj.Ds.GetXml();
        }
        catch(Exception)
        {
            return "false";
        }


    }

}