﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="InventoryHandReport.aspx.cs" Inherits="Admin_InventoryHandReport" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .zoom:hover {
            -ms-transform: scale(3); /* IE 9 */
            -webkit-transform: scale(3); /* Safari 3-8 */
            transform: scale(3);
        }
        .table tbody td {
            padding: 3px !important;
            vertical-align: middle !important;
        }
         .table td {
            padding: 8px !important;
            vertical-align: middle !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Inventory Onhand</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Inventory Report</a></li>
                        <li class="breadcrumb-item active">Inventory Onhand</li>
                      <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10017)"><i class="la la-question-circle"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>  <input type="hidden" id="HDDomain" runat="server" />
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport" onclick="ExportReport();">Export</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSCategory" onchange="BindSubCategory();">
                                            <option value="0">All Category</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlSSubcategory">
                                            <option value="0">All Subcategory</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product ID" id="txtSProductId" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Product Name" id="txtSProductName" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlBrand">
                                            <option value="0">All Brand</option>
                                        </select>
                                       
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Barcode" id="txtBarCode" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select class="form-control border-primary input-sm" id="ddlStatus">
                                            <option value="2">All Status</option>
                                            <option value="1" selected="selected">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>

                                    <div class="col-md-1 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch" onclick="getProductList(1);">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12 ">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblProductList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Category text-center width4per">Category</td>
                                                        <td class="Subcategory text-center width4per">Subcategory</td>
                                                        <td class="BrandName text-center width3per">Brand</td>
                                                        <td class="ProductId text-center width3per">Product ID</td>
                                                        <td class="ProductName">Product Name</td>
                                                        <td class="Stock text-center width3per">Stock</td>
                                                        <td class="ReOrderMark text-center width4per">Re Order Mark</td>
                                                        <td class="ImageUrl text-center width3per">Image</td>
                                                        <td class="Vendor text-center width4per">Prefer Vendor</td>
                                                        <td class="Status text-center width3per">Status</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control border-primary input-sm pagesize" id="ddlPageSize" onchange="getProductList(1)">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                    Inventory Onhand
                    <br />
                    <span class="text-center" style="font-size: 8px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead class="bg-blue white">
                    <tr>
                        <td class="P_CategoryName text-center" style="width: 13%">Category</td>
                        <td class="P_SubcategoryName text-center" style="width: 10%;">Subcategory</td>
                        <td class="P_BrandName text-center tblwth" style="width: 4%">Brand</td>
                        <td class="P_ProductId text-center" style="width: 4%">Product ID</td>
                        <td class="P_ProductName left">Product Name</td>
                        <td class="P_Stock text-center" style="width: 5%">Stock</td>
                        <td class="P_ReOrderMark tblwth" style="text-align: center; width: 5%">Re Order Mark</td>
                        <td class="P_Vendor right tblwth" style="text-align: left; width: 6%">Prefer Vendor</td>
                        <td class="P_Status right tblwth" style="text-align: center; width: 4%">Status</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <input type="hidden" id="hiddenForPacker" runat="server" />
    
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

