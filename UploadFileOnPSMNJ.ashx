﻿<%@ WebHandler Language="C#" Class="FileUploadHandler" %>

using System;
using System.Web;
using System.IO;
using System.Data;
using Newtonsoft.Json;

using System.Drawing;
using System.Drawing.Drawing2D;
public class FileUploadHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        DataTable dt = new DataTable();
        dt.Clear();
        dt.Columns.Add("URL");
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                string fname;
                if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1
                        ];
                }
                else
                {
                    fname = file.FileName;
                }
                string[] getextentension = fname.Split('.');
                string[] GtUrl = HttpContext.Current.Request.Url.Host.ToString().Split('.');
                string dirFullPath = "", returnurThumnailUrl = "";
                dirFullPath = HttpContext.Current.Server.MapPath("~/DraftProduct/");
                string timeStamp = context.Request.QueryString["timeStamp"].ToString();
                string fnamefull = dirFullPath + timeStamp + "." + getextentension[getextentension.Length - 1];
                    

                //Code for watermark functionality
                System.Drawing.Image upImage = System.Drawing.Image.FromStream(file.InputStream);
                System.Drawing.Image logoImage = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath("img/logo/watermark.png"));
                var width = (upImage.Width - logoImage.Width) / 2;
                var height = (upImage.Height - logoImage.Height) / 2;
                using (Graphics g = Graphics.FromImage(upImage))
                {
                    g.DrawImage(logoImage, new Point(width, height));
                }
                //End

                upImage.Save(fnamefull);


                returnurThumnailUrl = CreateThumbnail(100, 100, fnamefull, "DraftProduct", timeStamp);
                returnurThumnailUrl = CreateThumbnail(400, 400, fnamefull, "DraftProduct", timeStamp);

                DataRow row = dt.NewRow();
                row["URL"] = timeStamp + "_" + fname;
                dt.Rows.Add(row);
            }
        }
        context.Response.ContentType = "text/plain";
        context.Response.Write(JsonConvert.SerializeObject(dt));
    }
    public string CreateThumbnail(int maxWidth, int maxHeight, string path, string folderName, string timeStamp)
    {
        var image = System.Drawing.Image.FromFile(path);
        var ratioX = (double)maxWidth / image.Width;
        var ratioY = (double)maxHeight / image.Height;
        var ratio = Math.Min(ratioX, ratioY);
        var newWidth = (int)(image.Width * ratio);
        var newHeight = (int)(image.Height * ratio);
        var newImage = new Bitmap(newWidth, newHeight);
        Graphics thumbGraph = Graphics.FromImage(newImage);

        thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
        thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
        thumbGraph.DrawImage(image, 0, 0, newWidth, newHeight);
        image.Dispose();

        string fileRelativePath = "~/" + folderName + "/" + maxHeight + "_" + maxHeight + "_Thumbnail_" + Path.GetFileName(path);
        newImage.Save(HttpContext.Current.Server.MapPath(fileRelativePath), newImage.RawFormat);

        return fileRelativePath;
    }
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}