﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using DLLProductWiseCommissionReport;

/// <summary>
/// Summary description for ProductWiseCommissionReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class ProductWiseCommissionReport : System.Web.Services.WebService {
    [WebMethod(EnableSession = true)]
    public string BindSalesPerson()
    {
        if (Session["EmpAutoId"] != null)
        {

            try
            {
                PL_ProductWiseCommissionReport pobj = new PL_ProductWiseCommissionReport();
                BL_ProductWiseCommissionReport.selectSalesPerson(pobj);
                //return pobj.Ds.GetXml();
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }   
    }

    [WebMethod(EnableSession = true)]
    public string GetProductWiseCommissionList(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {

        try
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ProductWiseCommissionReport pobj = new PL_ProductWiseCommissionReport();
            if (jdv["FromDate"] != null && jdv["FromDate"] != "")
            {
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            }
            if (jdv["ToDate"] != null && jdv["ToDate"] != "")
            {
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            }
            pobj.CommissionCode = jdv["CommissionCode"];
            if (jdv["CommissionCode"] == "-All Commission Code-")
            {
                pobj.CommissionCode = "";
            }
            pobj.ProductId = jdv["ProductId"];
            pobj.ProductName = jdv["ProductName2"];
            pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);          

            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            BL_ProductWiseCommissionReport.GetReportDetail(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        catch
        {
            return "false";
        }
        }
        else
        {
            return "Session Expired";
        }  
    }  
}
