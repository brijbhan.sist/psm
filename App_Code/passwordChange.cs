﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllLogin;
using System.Web.Script.Serialization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class passwordChange : System.Web.Services.WebService
{
    [WebMethod(EnableSession = true)]
    public string changePassword(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Login pobj = new PL_Login();
        try
        {
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]); ;
            pobj.Password = jdv["OldPassword"].Trim();
            pobj.NewPassword = jdv["NewPassword"].Trim();
            BL_Login.changePassword(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.exceptionMessage;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}
