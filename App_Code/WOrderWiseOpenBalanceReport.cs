﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLSaleReport;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for WOrderWiseOpenBalanceRepor
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WOrderWiseOpenBalanceReport : System.Web.Services.WebService
{

    public WOrderWiseOpenBalanceReport()
    {


    }
    [WebMethod(EnableSession = true)]
    public string BindCustomer(string Customertype, string SalesPersonAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                pobj.CustomerType = Convert.ToInt32(Customertype);
                pobj.SalesPersonAutoId = Convert.ToInt32(SalesPersonAutoId);
                BL_SaleReport.BindCustomer(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string BindCustomerType()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                BL_SaleReport.BindCustomerType(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string OrderWiseOpenBalanceReport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerType = Convert.ToInt32(jdv["CustomerType"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                BL_SaleReport.OrderWiseOpenBalanceReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string OpenBalanceReportSummary(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerType = Convert.ToInt32(jdv["CustomerType"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_SaleReport.OpenBalanceReportSummary(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string OpenBalanceReportDetail(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerType = Convert.ToInt32(jdv["CustomerType"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_SaleReport.OpenBalanceReportDetail(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
