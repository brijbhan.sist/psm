﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLDeviceMaster;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for WDeviceMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WDeviceMaster : System.Web.Services.WebService
{

    public WDeviceMaster()
    {

    }

    [WebMethod(EnableSession = true)]
    public string insertDevice(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            Pl_DeviceMaster pobj = new Pl_DeviceMaster();
            try
            {
                pobj.DeviceId = jdv["DeviceId"];
                pobj.DeviceName = jdv["DeviceName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.IsLocationRequired = Convert.ToInt32(jdv["IsLocationRequired"]);
                pobj.Createdby = Convert.ToInt32(Session["EmpAutoId"]);
                BL_DeviceMaster.insertDevice(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getDeviceDetail(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            Pl_DeviceMaster pobj = new Pl_DeviceMaster();
            try
            {
                pobj.DeviceId = jdv["DeviceId"];
                pobj.DeviceName = jdv["DeviceName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_DeviceMaster.GetDeviceDetail(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string editDevice(int DeviceAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            Pl_DeviceMaster pobj = new Pl_DeviceMaster();
            pobj.Autoid = Convert.ToInt32(DeviceAutoId);
            BL_DeviceMaster.editDevice(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateDevice(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            Pl_DeviceMaster pobj = new Pl_DeviceMaster();
            try
            {
                pobj.DeviceId = jdv["DeviceId"];
                pobj.DeviceName = jdv["DeviceName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.IsLocationRequired = Convert.ToInt32(jdv["IsLocationRequired"]);
                pobj.Autoid = Convert.ToInt32(jdv["DeviceAutoId"]);
                pobj.Createdby = Convert.ToInt32(Session["EmpAutoId"]);
                BL_DeviceMaster.updateDevice(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteDevice(int DeviceAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            Pl_DeviceMaster pobj = new Pl_DeviceMaster();
            try
            {
                pobj.Autoid = Convert.ToInt32(DeviceAutoId);
                BL_DeviceMaster.DeleteDevice(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
}
