﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using DllUtility;
using DllPageTrackerMaster;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class PageTracker : System.Web.Services.WebService
{

    public PageTracker()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string BindUserTypeName()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PageTrackerMaster pobj = new PL_PageTrackerMaster();
                BL_PageTrackerMaster.BindUserTypeName(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }


    [WebMethod(EnableSession = true)]
    public string BindEmployee(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_PageTrackerMaster pobj = new PL_PageTrackerMaster();
                pobj.EmpTypeAutoId = Convert.ToInt32(jdv["User"]);
                BL_PageTrackerMaster.BindEmployee(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetPageTrackerList(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_PageTrackerMaster pobj = new PL_PageTrackerMaster();
                pobj.UserAutoId = Convert.ToInt32(jdv["UserName"]);
                if (jdv["FromDate"] != "")
                {
                    pobj.AccessFromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.AccessToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                if (jdv["TypeName"] != "0")
                {
                    pobj.EmpTypeAutoId = Convert.ToInt32(jdv["TypeName"]);
                }
                pobj.OrderBy = Convert.ToInt32(jdv["OrderBy"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_PageTrackerMaster.GetPageTrackerReportData(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}
