﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using DLLPattyCashTransaction;
using DllUtility;

/// <summary>
/// Summary description for PattyCash
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class PattyCash : System.Web.Services.WebService
{
    public PattyCash()
    {
        gZipCompression.fn_gZipCompression();
    }
   [WebMethod(EnableSession = true)]
    public string getTransactionBalance()
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            PL_PattyCashTransaction pobj = new PL_PattyCashTransaction();
            try
            {
                BL_PattyCashTransaction.GetCurrentBlc(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string CheckSecurity(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_PattyCashTransaction pobj = new PL_PattyCashTransaction();
        try
        {
            if (Session["EmpTypeNo"] != null)
            {
                pobj.EmpType = Convert.ToInt32(Session["EmpTypeNo"].ToString());
                pobj.SecurityKey = jdv["Security"];
                BL_PattyCashTransaction.checkSecurity(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string updateTransaction(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_PattyCashTransaction pobj = new PL_PattyCashTransaction();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.Who = Convert.ToInt32(Session["EmpAutoId"].ToString());
                pobj.Amount = Convert.ToDecimal(jdv["transactionAmount"]);
                pobj.Category = Convert.ToInt32(jdv["Category"]);
                pobj.TransactionType = jdv["transactionType"];
                pobj.Remark = jdv["remark"];
                if (jdv["TransactionDate"] != null && jdv["TransactionDate"] != "")
                {
                    pobj.TransactionDate = Convert.ToDateTime(jdv["TransactionDate"]);
                }
                BL_PattyCashTransaction.insert(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string Update(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_PattyCashTransaction pobj = new PL_PattyCashTransaction();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.Who = Convert.ToInt32(Session["EmpAutoId"].ToString());
                pobj.Amount = Convert.ToDecimal(jdv["transactionAmount"]);
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.TransactionType = jdv["transactionType"];
                pobj.Category = Convert.ToInt32(jdv["Category"]);
                pobj.Remark = jdv["remark"];
                if (jdv["TransactionDate"] != null && jdv["TransactionDate"] != "")
                {
                    pobj.TransactionDate = Convert.ToDateTime(jdv["TransactionDate"]);
                }
                BL_PattyCashTransaction.Update(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getPattyCashTransactionList(string datavalue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(datavalue);
            PL_PattyCashTransaction pobj = new PL_PattyCashTransaction();
            try
            {
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.Amount = Convert.ToDecimal(jdv["Amount"]);
                pobj.TransactionType= jdv["Trtype"];
                pobj.SearchBy= jdv["SearchBy"];
                pobj.Category=Convert.ToInt32(jdv["Category"]);


                BL_PattyCashTransaction.GetPattyCashList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getPattyCashTransactionListExport(string datavalue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(datavalue);
            PL_PattyCashTransaction pobj = new PL_PattyCashTransaction();
            try
            {
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }

                BL_PattyCashTransaction.GetPattyCashListExport(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string EditPattyBalance(string AutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_PattyCashTransaction pobj = new PL_PattyCashTransaction();
            try
            {
                pobj.AutoId = Convert.ToInt32(AutoId);
                BL_PattyCashTransaction.EditPattyBalance(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

}
