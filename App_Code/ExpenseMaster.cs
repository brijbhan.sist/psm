﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLExpenseMaster;
using System.Data;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class ExpenseMaster : System.Web.Services.WebService
{
    //Testing comment
    [WebMethod(EnableSession = true)]
    public string bindPaymentType()
    {
        PL_ExpenseMaster pobj = new PL_ExpenseMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_ExpenseMaster.GetPaymentType(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "SessionExpired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetExpense(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ExpenseMaster pobj = new PL_ExpenseMaster();
        try
        {
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            pobj.SearchBy = jdv["SearchBy"];
            pobj.ExpenseAmount = Convert.ToDecimal(jdv["ExpenseAmount"]);
            pobj.PageIndex = jdv["PageIndex"];
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            pobj.Category = Convert.ToInt32(jdv["Category"]);
            if (Session["EmpAutoId"] != null)
            {
                BL_ExpenseMaster.GetExpense(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "SessionExpired";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string EditExpense(string AutoId)
    {
        PL_ExpenseMaster pobj = new PL_ExpenseMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(AutoId);
                BL_ExpenseMaster.EditExpense(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "SessionExpired";
            }

        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string AddExpense(string dataValue,string XmlExpensiveCurrency)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ExpenseMaster pobj = new PL_ExpenseMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {              
                pobj.ExpenseDescription = jdv["ExpenseDescription"];
                pobj.PaymentSource = Convert.ToInt32(jdv["PaymentSource"]);
                pobj.ExpenseDate = Convert.ToDateTime(jdv["ExpenseDate"]);
                pobj.ExpenseAmount = Convert.ToDecimal(jdv["ExpenseAmount"]);
                pobj.CreateBy = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.PaymentTypeAutoId = Convert.ToInt32(jdv["PaymentTypeAutoId"]);
                pobj.Category = Convert.ToInt32(jdv["Category"]);
                pobj.XmlCurrency = XmlExpensiveCurrency;
                BL_ExpenseMaster.AddExpense(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    this.Context.Response.Clear();
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "false";
                }
            }
            else
            {
                return "SessionExpired";
            }

        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdateExpense(string dataValue, string XmlExpensiveCurrency)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ExpenseMaster pobj = new PL_ExpenseMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.ExpenseDescription = jdv["ExpenseDescription"];
                pobj.ExpenseDate = Convert.ToDateTime(jdv["ExpenseDate"]);
                pobj.ExpenseAmount = Convert.ToDecimal(jdv["ExpenseAmount"]);
                pobj.UpdateBy = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.PaymentTypeAutoId = Convert.ToInt32(jdv["PaymentTypeAutoId"]);
                pobj.PaymentSource = Convert.ToInt32(jdv["PaymentSource"]);
                pobj.Category = Convert.ToInt32(jdv["Category"]);
                pobj.XmlCurrency = XmlExpensiveCurrency;
                BL_ExpenseMaster.UpdateExpense(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    this.Context.Response.Clear();
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "false";
                }
            }
            else
            {
                return "SessionExpired";
            }

        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string DeleteExpense(string AutoId)
    {
        PL_ExpenseMaster pobj = new PL_ExpenseMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(AutoId);
                BL_ExpenseMaster.DeleteExpense(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "SessionExpired";
            }

        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
}
