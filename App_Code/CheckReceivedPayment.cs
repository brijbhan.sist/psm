﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLCheckReceivedPayment;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for CheckReceivedPayment
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class CheckReceivedPayment : System.Web.Services.WebService {

    public CheckReceivedPayment () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string GetReport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_CheckReceivedPayment pobj = new PL_CheckReceivedPayment();
            pobj.Customer = Convert.ToInt32(jdv["CustomerStatus"]);
            pobj.ReceivedBy = Convert.ToInt32(jdv["ReceivedBy"]);
            pobj.Status = Convert.ToInt32(jdv["Status"]);
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            if (jdv["DepositFromDate"] != "")
                pobj.DepositFromDate = Convert.ToDateTime(jdv["DepositFromDate"]);
            if (jdv["DepositToDate"] != "")
                pobj.DepositToDate = Convert.ToDateTime(jdv["DepositToDate"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

            BL_CheckReceivedPayment.GetReport(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetCustomer()
    {
        if (Session["EmpAutoId"] != null)
        {          
            PL_CheckReceivedPayment pobj = new PL_CheckReceivedPayment();
            BL_CheckReceivedPayment.GetCustomer(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }    
}
