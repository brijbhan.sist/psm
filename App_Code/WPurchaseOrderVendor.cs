﻿using DLLPOVendorMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Data;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class WPurchaseOrderVendor : System.Web.Services.WebService {
    public WPurchaseOrderVendor () {
    }
    [WebMethod(EnableSession = true)]
    public string bindDropDowns()
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            BL_POVendorMaster.BindDropDown(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindProductDropDown()
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            BL_POVendorMaster.BindProductDropDown(pobj);
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return json;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindProductDetails(int productAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            try
            {
                pobj.ProductAutoId = productAutoId;
                BL_POVendorMaster.BindProductDetails(pobj);
                return pobj.Ds.GetXml();

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string AddPOVItem(string data)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(data);
        PL_POVendorMaster pobj = new PL_POVendorMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.Remark = jdv["Remark"];
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.RequiredQty = Convert.ToInt32(jdv["RequiredQty"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.QtyPerUnit = Convert.ToInt32(jdv["QtyPerUnit"]);
                if (jdv["VendorId"] != "")
                {
                    pobj.VendorId = Convert.ToInt32(jdv["VendorId"]);
                }
                if (jdv["DraftAutoId"] != "")
                {
                    pobj.POVDraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                }
                pobj.Price = Convert.ToDecimal(jdv["CostPrice"]);
                BL_POVendorMaster.AddProductItem(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    var msg="";
                    if (pobj.exceptionMessage == "ambiguity")
                    {
                        msg = "ambiguity";
                    }
                    else
                    {
                        msg = "false";
                    }
                    return msg;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string AddPOVItemByBarcode(string data)
    {
        PL_POVendorMaster pobj = new PL_POVendorMaster();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(data);
                pobj.Barcode = jdv["Barcode"];
                pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.RequiredQty = Convert.ToInt32(jdv["ReqQty"]);
                if (jdv["DraftAutoId"] != "")
                {
                    pobj.POVDraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                }
                if (jdv["Remark"] != "")
                {
                    pobj.Remark = jdv["Remark"];
                }
                //pobj.VendorId = Convert.ToInt32(jdv["VenderId"]);
                BL_POVendorMaster.AddProductItemByBarcode(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    var msg = "";
                    if (pobj.exceptionMessage == "ambiguity")
                    {
                        msg = "ambiguity";
                    }
                    else
                    {
                        msg = "false";
                    }
                    return msg;
                }
            }
            catch (Exception)
            {
                return pobj.exceptionMessage; ;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string DeleteProductItem(string DataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(DataValue);
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            try
            {
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.POVDraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                BL_POVendorMaster.DeleteProductItem(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string SaveAsDraft(string data, string XMLData)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(data);
        PL_POVendorMaster pobj = new PL_POVendorMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.Remark = jdv["Remark"];
                pobj.VendorId = Convert.ToInt32(jdv["VendorId"]);
                pobj.Draftitems = Convert.ToString(XMLData);
                pobj.POVDraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                BL_POVendorMaster.SaveAsDraft(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GeneratePO(string data, string XMLData)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(data);
        PL_POVendorMaster pobj = new PL_POVendorMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.Remark = jdv["Remark"];
                //pobj.VendorId = Convert.ToInt32(jdv["VendorId"]);
                pobj.Draftitems = Convert.ToString(XMLData);
                pobj.POVDraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                BL_POVendorMaster.GeneratePO(pobj);
                if (!pobj.isException)
                {                   
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string POList(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            pobj.POVNumber = jdv["PONo"];
            if (jdv["POFromDate"] != "")
            {
                pobj.POFromDate = Convert.ToDateTime(jdv["POFromDate"]);
            }
            if (jdv["POToDate"] != "")
            {
                pobj.POToDate = Convert.ToDateTime(jdv["POToDate"]);
            }
            pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.VendorId = Convert.ToInt32(jdv["VendorId"]); 
            pobj.Status = Convert.ToInt32(jdv["Status"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            BL_POVendorMaster.PoList(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string DraftList(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            if (jdv["POFromDate"] != "")
            {
                pobj.POFromDate = Convert.ToDateTime(jdv["POFromDate"]);
            }
            if (jdv["POToDate"] != "")
            {
                pobj.POToDate = Convert.ToDateTime(jdv["POToDate"]);
            }
            pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.VendorId = Convert.ToInt32(jdv["VendorId"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PODraftId = jdv["PODraftId"];
            BL_POVendorMaster.DraftList(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string EditDraft(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.POVDraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
            BL_POVendorMaster.EditDraft(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string ReceivePo(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            pobj.PoAutoId = Convert.ToInt32(jdv["AutoId"]);
            if (jdv["RecAutoId"] != "")
            {
                pobj.RecAutoId = Convert.ToInt32(jdv["RecAutoId"]);
            }
           
            BL_POVendorMaster.ReceivePO(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string AutoUpdatePo(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            pobj.POVNumber = jdv["POVNumber"]; 
            pobj.Remark = jdv["Remark"];
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
            pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
            pobj.RecUnitAutoId = Convert.ToInt32(jdv["RecUnitAutoId"]);
            pobj.ReveivedQty = Convert.ToInt32(jdv["ReveivedQty"]);
            pobj.QtyPerUnit = Convert.ToInt32(jdv["QtyUnit"]);
            pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
            if (jdv["ReceiveAutoId"] != "")
            {
                pobj.RecAutoId = Convert.ToInt32(jdv["ReceiveAutoId"]);
            }
            //pobj.BillNo = jdv["BillNo"];
            //if (jdv["BillDate"] != "")
            //{
            //    pobj.BillDate = Convert.ToDateTime(jdv["BillDate"]);
            //}
            //if (jdv["RecDate"] != "")
            //{
            //    pobj.RecDate = Convert.ToDateTime(jdv["RecDate"]);
            //}
            BL_POVendorMaster.AutoUpdatePO(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string deleteDraftOrder(string DraftAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            try
            {
                pobj.POVDraftAutoId = Convert.ToInt32(DraftAutoId);
                BL_POVendorMaster.deleteDraft(pobj);
                if (!pobj.isException)
                {
                    return "deleted";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string ReceiveByStatus(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            pobj.Status = Convert.ToInt32(jdv["Status"]);
            pobj.POVNumber = jdv["PONumber"];
            pobj.ReceiverRemark = jdv["ReceiverRemark"];
            //pobj.Draftitems = Convert.ToString(XMLData);
            if (jdv["RecAutoId"] != "")
            {
                pobj.RecAutoId = Convert.ToInt32(jdv["RecAutoId"]);
            }
            pobj.BillNo = jdv["BillNo"];
            if (jdv["BillDate"] != "")
            {
                pobj.BillDate = Convert.ToDateTime(jdv["BillDate"]);
            }
            if (jdv["RecDate"] != "")
            {
                pobj.RecDate = Convert.ToDateTime(jdv["RecDate"]);
            }
            pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
            BL_POVendorMaster.ReceiveByStatus(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string SavePODraft(string DraftAutoId,string Remark)
    {
        if (Session["EmpAutoId"] != null)
        {
            
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            pobj.POVDraftAutoId = Convert.ToInt32(DraftAutoId);
            pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.Remark = (Remark);
            BL_POVendorMaster.SavePODraft(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string ForwardDraftList(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            if (jdv["POFromDate"] != "")
            {
                pobj.POFromDate = Convert.ToDateTime(jdv["POFromDate"]);
            }
            if (jdv["POToDate"] != "")
            {
                pobj.POToDate = Convert.ToDateTime(jdv["POToDate"]);
            }
            pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.VendorId = Convert.ToInt32(jdv["VendorId"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PODraftId = jdv["PODraftId"];
            BL_POVendorMaster.ForwardDraftList(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string RevertPODraft(string DraftAutoId, string Remark)
    {
        if (Session["EmpAutoId"] != null)
        {

            PL_POVendorMaster pobj = new PL_POVendorMaster();
            pobj.POVDraftAutoId = Convert.ToInt32(DraftAutoId);
            pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.Remark = (Remark);
            BL_POVendorMaster.RevertPODraft(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string CancelPO(string POAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {

            PL_POVendorMaster pobj = new PL_POVendorMaster();
            pobj.PoAutoId = Convert.ToInt32(POAutoId);
            pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
            BL_POVendorMaster.CancelPO(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string RevertPOS(string POAutoId,string Remark,string RecAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {

            PL_POVendorMaster pobj = new PL_POVendorMaster();
            pobj.PoAutoId = Convert.ToInt32(POAutoId);
            pobj.RecAutoId = Convert.ToInt32(RecAutoId);
            pobj.Remark = (Remark);
            pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
            BL_POVendorMaster.RevertPO(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string DraftLog(int DraftAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            try
            {
                pobj.POVDraftAutoId = DraftAutoId;
                BL_POVendorMaster.DraftLog(pobj);
                return pobj.Ds.GetXml();

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string POLog(int POAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            try
            {
                pobj.PoAutoId = POAutoId;
                BL_POVendorMaster.POLog(pobj);
                return pobj.Ds.GetXml();

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string AutoUpdatePODraftProduct(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            pobj.POVDraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
            pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]); 
            pobj.QtyPerUnit = Convert.ToInt32(jdv["QtyPerUnit"]); 
            pobj.RequiredQty = Convert.ToInt32(jdv["ReqQty"]); 
            pobj.Price = Convert.ToDecimal(jdv["Price"]);
            if (jdv["Vendor"] != "")
            {
                pobj.VendorId = Convert.ToInt32(jdv["Vendor"]);
            }
            BL_POVendorMaster.AutoUpdatePODraftProduct(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string AutoUpdateRemarks(string DraftAutoId, string Remark)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            pobj.POVDraftAutoId = Convert.ToInt32(DraftAutoId);
            pobj.Remark = Remark;
          
            BL_POVendorMaster.AutoUpdateRemarks(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string PODetails(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_POVendorMaster pobj = new PL_POVendorMaster();
            pobj.PoAutoId = Convert.ToInt32(jdv["AutoId"]);
            BL_POVendorMaster.PODetails(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
