﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllOrderListAccount;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class UndeliveredOrders : System.Web.Services.WebService {
    [WebMethod(EnableSession = true)]
    public string getUndelOrderList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderListAccount pobj = new PL_OrderListAccount();
        try
        {
            pobj.OrderNo = jdv["OrderNo"];
            if (jdv["FromDate"] != null && jdv["FromDate"] != "")
            {
                pobj.Fromdate = Convert.ToDateTime(jdv["FromDate"]);
            }
            if (jdv["ToDate"] != null && jdv["ToDate"] != "")
            {
                pobj.Todate = Convert.ToDateTime(jdv["ToDate"]);
            }
            pobj.PageIndex = jdv["PageIndex"];

            pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
            pobj.OrderStatus = Convert.ToInt32(jdv["StatusAutoId"]);
            BL_OrderListAccount.getUndelOrderList(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }    
}
