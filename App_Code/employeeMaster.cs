﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllEmployee;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;

/// <summary>
/// Summary description for employeeMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class employeeMaster : System.Web.Services.WebService
{
    string Body = "";
    [WebMethod(EnableSession = true)]
    public string bindEmpTypeAndStatus()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Employee pobj = new PL_Employee();
            try
            {
                BL_Employee.selectEmpTypeAndStatus(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Unauthorized access.";

        }
    }
    [WebMethod(EnableSession = true)]
    public string insertEmpRecord(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);

            PL_Employee pobj = new PL_Employee();
            try
            {
                pobj.EmpCode = jdv["EmpCode"];
                pobj.ProfileName = jdv["ProfileName"];
                pobj.EmpType = Convert.ToInt32(jdv["EmpType"]);
                pobj.FirstName = jdv["FirstName"];
                pobj.LastName = jdv["LastName"];
                pobj.Address = jdv["Address"];
                pobj.State = jdv["State"];
                pobj.City = jdv["City"];
                pobj.Zipcode = jdv["Zipcode"];
                pobj.Email = jdv["Email"];
                pobj.Contact = jdv["Contact"];
                pobj.ImageURL = "/Attachments/" + jdv["ImageURL"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                //pobj.AutoId = Convert.ToInt32(jdv["AssignDevice"]);
                pobj.UserName = jdv["UserName"];
                pobj.Password = jdv["Password"];
                if (Session["EmpTypeNo"].ToString() == "1")
                {
                    pobj.IsLoginIp = jdv["IsLoginIp"];
                    //pobj.IpAddress = jdv["IpAddress"];
                    pobj.IsAppLogin = Convert.ToInt32(jdv["IsAppLogin"]);
                }
                else if (Session["EmpTypeNo"].ToString() == "13")
                {
                    pobj.IsLoginIp = jdv["IsLoginIp"];
                }
                pobj.DeliveryStartTime = jdv["DeliveryStartTime"];
                pobj.DeliveryCloseTime = jdv["DeliveryCloseTime"];
                string xyz = "";
                string ASD = "";
                int i = 0;
                int j = 0;
                if (jdv["AssignDevice"] != null)
                {
                    foreach (string abc in jdv["AssignDevice"])
                    {
                        if (i == 0)
                        {
                            xyz = abc;
                        }
                        else
                        {
                            xyz = xyz + "," + abc;

                        }
                        i = i + 1;
                    }

                    pobj.AssignDeviceAutoId = xyz;
                }
                if (jdv["IpAddressAutoId"] != null)
                {
                    foreach (string adc1 in jdv["IpAddressAutoId"])
                    {
                        if (j == 0)
                        {
                            ASD = adc1;
                        }
                        else
                        {
                            ASD = ASD + "," + adc1;

                        }
                        j = j + 1;
                    }

                    pobj.IpAddressAutoId = ASD;
                }
                BL_Employee.insert(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getEmpRecord(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Employee pobj = new PL_Employee();
            try
            {
                pobj.EmpType = Convert.ToInt32(jdv["EmpType"]);
                pobj.EmpCode = jdv["EmpId"];
                pobj.FirstName = jdv["Name"];
                pobj.LastName = jdv["Name"];
                pobj.Email = jdv["Email"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = jdv["pageIndex"];

                BL_Employee.selectEmpRecord(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Unauthorized access.";
        }

    }
    [WebMethod(EnableSession = true)]
    public string editEmpRecord(string AutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Employee pobj = new PL_Employee();
            try
            {
                pobj.AutoId = Convert.ToInt16(AutoId);
                BL_Employee.editEmpRecord(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string deleteEmpRecord(string AutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Employee pobj = new PL_Employee();
            try
            {
                pobj.AutoId = Convert.ToInt16(AutoId);
                BL_Employee.deleteEmpRecord(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string ViewProfile()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Employee pobj = new PL_Employee();
            try
            {
                pobj.AutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_Employee.BindViewProfile(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateEmpRecord(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            int PasswordChange = 0;
            PL_Employee pobj = new PL_Employee();
            try
            {
                pobj.AutoId = Convert.ToInt16(jdv["AutoId"]);
                pobj.EmpCode = jdv["EmpCode"];
                pobj.ProfileName = jdv["ProfileName"];
                pobj.EmpType = Convert.ToInt32(jdv["EmpType"]);
                pobj.FirstName = jdv["FirstName"];
                pobj.LastName = jdv["LastName"];
                pobj.Address = jdv["Address"];
                pobj.State = jdv["State"];
                pobj.City = jdv["City"];
                pobj.Zipcode = jdv["Zipcode"];
                pobj.Email = jdv["Email"];
                pobj.Contact = jdv["Contact"];
                pobj.ImageURL = "/Attachments/" + jdv["ImageURL"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.UserName = jdv["UserName"];
                pobj.Password = jdv["Password"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                var username = Session["UserName"].ToString().Remove(5);
                var abc = Session["EmpType"].ToString();
                if (Session["EmpTypeNo"].ToString() == "1")
                {
                    pobj.IsLoginIp = jdv["IsLoginIp"];
                    //pobj.IpAddress = jdv["IpAddress"];
                    pobj.IsAppLogin = Convert.ToInt32(jdv["IsAppLogin"]);
                }
                else if (Session["EmpTypeNo"].ToString() == "13")
                {
                    pobj.IsLoginIp = jdv["IsLoginIp"];
                }
                pobj.DeliveryStartTime = jdv["DeliveryStartTime"];
                pobj.DeliveryCloseTime = jdv["DeliveryCloseTime"];
                PasswordChange = Convert.ToInt32(jdv["PasswordChange"]);
                //pobj.AutoId = Convert.ToInt32(jdv["AssignDevice"]);
                string xyz1 = "";
                string ASD1 = "";
                int i1 = 0;
                int j1 = 0;
                if (jdv["AssignDevice"] != null)
                {
                    foreach (string abc1 in jdv["AssignDevice"])
                    {
                        if (i1 == 0)
                        {
                            xyz1 = abc1;
                        }
                        else
                        {
                            xyz1 = xyz1 + "," + abc1;

                        }
                        i1 = i1 + 1;
                    }

                    pobj.AssignDeviceAutoId = xyz1;
                }
                if (jdv["IpAddressAutoId"] != null)
                {
                    foreach (string adc1 in jdv["IpAddressAutoId"])
                    {
                        if (j1 == 0)
                        {
                            ASD1 = adc1;
                        }
                        else
                        {
                            ASD1 = ASD1 + "," + adc1;

                        }
                        j1 = j1 + 1;
                    }

                    pobj.IpAddressAutoId = ASD1;
                }
                BL_Employee.update(pobj);
                if (!pobj.isException)
                {
                    if (PasswordChange == 1)
                    {
                        if (pobj.Ds != null)
                        {
                            getMailBody(pobj.Ds);
                        }
                    }
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }

    [WebMethod(EnableSession = true)]
    public  string updatepass(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            int PasswordChange = 0;
            PL_Employee pobj = new PL_Employee();
            try
            {
                pobj.AutoId = Convert.ToInt16(jdv["AutoId"]);
                pobj.Password = jdv["Password"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                PasswordChange = Convert.ToInt32(jdv["PasswordChange"]);
                pobj.IsLoginIp = Convert.ToInt32(jdv["IsLoginIp"]);
                BL_Employee.updatepass(pobj);
                if (!pobj.isException)
                {
                    if (PasswordChange == 1)
                    {
                        if (pobj.Ds != null)
                        {
                            getMailBody(pobj.Ds);
                        }
                    }
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    //Send Mail when password change
    public string getMailBody(DataSet Ds)
    {
        if (Session["EmpAutoId"] != null)
        {
            string OperationType = "", ReceiverEmailId = "", location = "";
            try
            {
                if (Ds != null && Ds.Tables.Count > 0)
                {
                    Body = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'>";
                    Body += "<head> <style>table{border-collapse:collapse;}td {padding: 3px 8px;}.text-center{text-align:center}";
                    Body += "table {width: 100%;}body {font-size: 14px;font-family: monospace;}";
                    Body += ".center{text-align: center;}.right {text-align: right;}thead > tr {background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a;}";
                    Body += "thead > tr > td {text-align: center !important;}.InvContent {width: 100%;}.tdLabel {font-weight: 700;}";
                    Body += "@media print {#btnPrint {display: none;}}legend {border-bottom: 1px solid #5f7c8a;}";
                    Body += ".tableCSS > tbody > tr > td,.tableCSS > thead > tr > td,.tableCSS > tfoot > tr > td {border: 1px solid #5f7c8a;}pre{border:none !important;}";
                    Body += "</style>";
                    Body += "</head>";
                    Body += "<body style='font-family: Arial; font-size: 12px'>";
                    if (Ds.Tables[0].Rows.Count > 0)
                    {
                        Body += "<table class='table tableCSS'>";
                        Body += "<tbody>";
                        Body += "<tr><td><b>Employee Id</b></td><td>" + Ds.Tables[0].Rows[0]["EmpId"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Employee Name</b></td><td>" + Ds.Tables[0].Rows[0]["EmployeeName"].ToString() + " (" + Ds.Tables[0].Rows[0]["EmpType"].ToString() + ")</td></tr>";
                        Body += "<tr><td><b>User Name</b></td>" + Ds.Tables[0].Rows[0]["UserName"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Password</b></td>" + Ds.Tables[0].Rows[0]["Password"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Update Date</b></td><td>" + Ds.Tables[0].Rows[0]["UpdateDate"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Update By</b></td><td>" + Ds.Tables[0].Rows[0]["UpdateBy"].ToString() + "</td></tr>";
                        location = Ds.Tables[0].Rows[0]["Location"].ToString();
                    }
                    Body += "</tbody>";
                    Body += "</table>";
                    Body += "<br>";
                    OperationType = location + " Password Changed";
                    int e = 0;
                    for (e = 0; e <= Ds.Tables[1].Rows.Count - 1; e++)
                    {
                        ReceiverEmailId += Ds.Tables[1].Rows[e]["Email"].ToString() + ",";
                    }
                    //}
                    if (ReceiverEmailId != null && ReceiverEmailId != string.Empty && ReceiverEmailId.Length > 0)
                    {
                        string emailmsg = MailSending.SendMailResponse(ReceiverEmailId, "", "", OperationType, Body.ToString(), "Developer");
                        if (emailmsg == "Successful")
                        {
                            return "true";
                        }
                        else
                        {
                            return "EmailNotSend";
                        }
                    }
                    else
                    {
                        return "receiverMail";
                    }
                }
                else
                {
                    return "EmailNotSend";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string loginWith(string EmpAutoId)
    {
        PL_Employee pobj = new PL_Employee();
        try
        {
            pobj.EmpAutoId = Convert.ToInt32(EmpAutoId);
            pobj.IPAddress = HttpContext.Current.Request.UserHostAddress;

            BL_Employee.loginwith(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                if (pobj.Ds.Tables[0].Rows.Count > 0)
                {

                    string AutoId = pobj.Ds.Tables[0].Rows[0]["AutoId"].ToString();
                    string emptype = pobj.Ds.Tables[0].Rows[0]["EmpType"].ToString();
                    string username = pobj.Ds.Tables[0].Rows[0]["UserName"].ToString();
                    string firstname = pobj.Ds.Tables[0].Rows[0]["Name"].ToString();
                    string empTypeno = pobj.Ds.Tables[0].Rows[0]["EmpTypeNo"].ToString();
                    string ProfileName = pobj.Ds.Tables[0].Rows[0]["ProfileName"].ToString();
                    string Email = pobj.Ds.Tables[0].Rows[0]["Email"].ToString();
                    HttpContext.Current.Session.Add("UserName", username);
                    HttpContext.Current.Session.Add("EmpFirstName", firstname);
                    HttpContext.Current.Session.Add("EmpAutoId", AutoId);
                    HttpContext.Current.Session.Add("EmpType", emptype);
                    HttpContext.Current.Session.Add("EmpTypeNo", empTypeno);
                    HttpContext.Current.Session.Add("ProfileName", ProfileName);
                    HttpContext.Current.Session.Add("ImageURL", pobj.Ds.Tables[0].Rows[0]["ImageURL"].ToString());
                    HttpContext.Current.Session.Add("UserEmail", Email);
                    HttpContext.Current.Session.Add("DBLocation", pobj.Ds.Tables[0].Rows[0]["DBLocation"].ToString());


                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.Ds.GetXml();
                }
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }



}
