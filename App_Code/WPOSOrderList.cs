﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllSalesManMaster;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for WPOSOrderList
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WPOSOrderList : System.Web.Services.WebService {

    public WPOSOrderList () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string getPOSOrderList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_SalesManMaster pobj = new PL_SalesManMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderNo = jdv["OrderNo"];
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoid"]);
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.Fromdate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.Todate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageIndex = jdv["pageIndex"];
                pobj.PageSize =Convert.ToInt32(jdv["PageSize"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.OrderStatus = Convert.ToInt32(jdv["OrderStatus"]);
                pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
                pobj.Opcode = 430;
                BL_SalesManMaster.getPOSOrderList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    
}
