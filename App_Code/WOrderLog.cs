﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;
using DllOrderLog;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WOrderLog : System.Web.Services.WebService {

    [WebMethod(EnableSession = true)]
    public string viewOrderLog(int OrderAutoId)
    {
        PL_OrderLog pobj = new PL_OrderLog();
        pobj.OrderAutoId = OrderAutoId;

        BL_OrderLog.getOrderLog(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }
    
}
