﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using DLLBrandMaster;

/// <summary>
/// Summary description for BrandMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class BrandMaster : System.Web.Services.WebService
{
    public BrandMaster()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string insertBrand(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_BrandMaster pobj = new PL_BrandMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.Who = Convert.ToInt16(Session["EmpAutoId"]);
                pobj.BrandName = jdv["BrandName"];
                pobj.Description = jdv["Description"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_BrandMaster.insert(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getBrandDetail(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_BrandMaster pobj = new PL_BrandMaster();
        try
        {
            pobj.BrandId = jdv["BrandId"];
            pobj.BrandName = jdv["BrandName"];
            pobj.Status = Convert.ToInt32(jdv["Status"]);          
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);          
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);          
            BL_BrandMaster.select(pobj);
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

 [WebMethod(EnableSession = true)]
    public string editBrand(string AutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_BrandMaster pobj = new PL_BrandMaster();
            pobj.AutoId = Convert.ToInt32(AutoId);
            BL_BrandMaster.editBrand(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string deleteBrand(int AutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_BrandMaster pobj = new PL_BrandMaster();
            try
            {
                pobj.AutoId = AutoId;
                BL_BrandMaster.delete(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateBrand(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_BrandMaster pobj = new PL_BrandMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.Who = Convert.ToInt16(Session["EmpAutoId"]);
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.BrandId = jdv["BrandId"];
                pobj.BrandName = jdv["BrandName"];
                pobj.Description = jdv["Description"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_BrandMaster.update(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
}
