﻿using DLLManagePage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class WManagePage : System.Web.Services.WebService
{

    public WManagePage()
    {

    }

    [WebMethod(EnableSession = true)]
    public string insert(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ManagePage pobj = new PL_ManagePage();
            try
            {
                pobj.PageName = jdv["PageName"];
                pobj.PageUrl = jdv["PageUrl"];
                pobj.Description = jdv["Description"];
                pobj.PageType = Convert.ToInt32(jdv["PageType"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.UserType = Convert.ToInt32(jdv["UserType"]);
                pobj.ModuleAutoId = Convert.ToInt32(jdv["ModuleAutoId"]);

                BL_ManagePage.insert(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        return "Hello World";
    }

    [WebMethod(EnableSession = true)]
    public string getPageList(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ManagePage pobj = new PL_ManagePage();
        pobj.PageName = jdv["SearchPageName"];
        pobj.PageId = jdv["SearchPageId"];
        pobj.Status = Convert.ToInt32(jdv["Status"]);
        BL_ManagePage.select(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }
    [WebMethod(EnableSession = true)]
    public string deletePage(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ManagePage pobj = new PL_ManagePage();
        pobj.PageAutoId = Convert.ToInt32(jdv["AutoId"]);
        BL_ManagePage.delete(pobj);

        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return "Success";
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }

    [WebMethod(EnableSession = true)]
    public string update(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ManagePage pobj = new PL_ManagePage();
            try
            {
                pobj.PageAutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.PageName = jdv["PageName"];
                pobj.PageUrl = jdv["PageUrl"];
                pobj.Description = jdv["Description"];
                pobj.PageType = Convert.ToInt32(jdv["PageType"]);
                pobj.UserType = Convert.ToInt32(jdv["UserType"]);
                pobj.ModuleAutoId = Convert.ToInt32(jdv["ModuleAutoId"]);

                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_ManagePage.update(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "false";
        }
        
    }

    [WebMethod(EnableSession = true)]
    public string selectUserType()
    {

        PL_ManagePage pobj = new PL_ManagePage();
        BL_ManagePage.selectUserType(pobj);

        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }
    [WebMethod(EnableSession = true)]
    public string getParentModule()
    {

        PL_ManagePage pobj = new PL_ManagePage();
        BL_ManagePage.getParentModule(pobj);

        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }


    [WebMethod(EnableSession = true)]
    public string checkActionBeforeRemove(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ManagePage pobj = new PL_ManagePage();
            try
            {
                pobj.PageAutoId = Convert.ToInt32(jdv["PageAutoId"]);
                pobj.PageAction = jdv["PageAction"];
                BL_ManagePage.checkActionBeforeRemove(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "false";
        }

    }

    [WebMethod(EnableSession = true)]
    public string editPage(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ManagePage pobj = new PL_ManagePage();
        pobj.PageAutoId = Convert.ToInt32(jdv["PageAutoId"]);
        BL_ManagePage.getPageByPageId(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }

    [WebMethod(EnableSession = true)]
    public string saveAction(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ManagePage pobj = new PL_ManagePage();
            try
            {
                pobj.PageAutoId = Convert.ToInt32(jdv["PageAutoId"]);
                pobj.EmpTypeNo = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.PageAction = jdv["ActionName"];
                pobj.ActionType = Convert.ToInt32(jdv["ActionType"]);
                pobj.UserType = Convert.ToInt32(jdv["UserType"]);

                BL_ManagePage.saveAction(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        return "Hello World";
    }

    [WebMethod(EnableSession = true)]
    public string getActionList(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ManagePage pobj = new PL_ManagePage();
        pobj.PageAutoId = Convert.ToInt32(jdv["PageAutoId"]);
        BL_ManagePage.getActionList(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }

    [WebMethod(EnableSession = true)]
    public string updateAction(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ManagePage pobj = new PL_ManagePage();
            try
            {
                pobj.PageAutoId = Convert.ToInt32(jdv["PageAutoId"]);
                pobj.EmpTypeNo = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.PageAction = jdv["ActionName"];
                pobj.ActionType = Convert.ToInt32(jdv["ActionType"]);
                pobj.UserType = Convert.ToInt32(jdv["UserType"]);
                pobj.ActionAutoId = Convert.ToInt32(jdv["ActionAutoId"]);

                BL_ManagePage.updateAction(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        return "Hello World";
    }

}
