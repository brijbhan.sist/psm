﻿using DLLPrintAllOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for WmPrintAllOrder
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WmPrintAllOrder : System.Web.Services.WebService {

    [WebMethod(EnableSession = true)]
    public string GetAllOrderID(int EndAutoID)
    {
        PL_PrintAllOrder pobj = new PL_PrintAllOrder();
        pobj.EndAutoId = EndAutoID;
        BL_PrintAllOrder.GetAllOrderID(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetOrderDetails(int OrderAutoId)
    {
        PL_PrintAllOrder pobj = new PL_PrintAllOrder();
        pobj.OrderAutoId = OrderAutoId;
        BL_PrintAllOrder.PrintAllOrder(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }
}
