﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using DLLProductCatalogueReport;
/// <summary>
/// Summary description for ProductCatalogueReportasmx
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class ProductCatalogueReportasmx : System.Web.Services.WebService {

    public ProductCatalogueReportasmx () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string Category()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_ProductCatalogueReport pobj = new PL_ProductCatalogueReport();
                BL_ProductCatalogueReport.selectCategory(pobj);
                //return pobj.Ds.GetXml();
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    
    [WebMethod (EnableSession = true)]
    public string SubCategory(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ProductCatalogueReport pobj = new PL_ProductCatalogueReport();
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                BL_ProductCatalogueReport.selectSubCategory(pobj);
                //return pobj.Ds.GetXml();
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

        [WebMethod (EnableSession = true)]
    public string GetProductCatelogReportList(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ProductCatalogueReport pobj = new PL_ProductCatalogueReport();
                pobj.ProductId = jdv["ProductId"];
                pobj.ProductName = jdv["ProductName2"];
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubCategoryAutoId = Convert.ToInt32(jdv["SubCategoryName"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_ProductCatalogueReport.GetProductCatelogReportList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
