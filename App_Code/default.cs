﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllLogin;
using System.Web.Script.Serialization;
using System.Net;
using System.Net.Sockets;
using System.Data;
using System.Configuration;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class _default : System.Web.Services.WebService
{

    [WebMethod(EnableSession = true)]
    public string loginUser(string UserName, string Password)
    {
        PL_Login pobj = new PL_Login();
        try
        {
            pobj.UserName = UserName.Trim();
            pobj.Password = Password.Trim();
            pobj.IPAddress = HttpContext.Current.Request.UserHostAddress;
            HttpContext.Current.Session["UserName"] = null; 
            if (string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password))
            {
                return "Username And / Or Password Incorrect";
            }
            if (ConfigurationManager.AppSettings["Mode"] == "TestMode")
            {
                Session["Location"] = UserName.Split('@')[1];
            }
            else
            {
                Session["Location"] = Session["Location"] = (HttpContext.Current.Request.Url.Host.ToString().Split('.')[0]).Replace("1", "");
            }
            string host = HttpContext.Current.Request.Url.Host;
            string WebUrl= (host.Split('.').First()).Replace("1", "");
            String strlist = UserName.Split('@').Last();
            if(WebUrl.ToLower() == strlist.ToLower() || WebUrl == "localhost")
            {
               BL_Login.login(pobj);
            }else
            {
                return "Username And / Or Password Incorrect";
            }
           
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                if (pobj.Ds.Tables[0].Rows.Count > 0)
                {

                    string AutoId = pobj.Ds.Tables[0].Rows[0]["AutoId"].ToString();
                    string emptype = pobj.Ds.Tables[0].Rows[0]["EmpType"].ToString();
                    string username = pobj.Ds.Tables[0].Rows[0]["UserName"].ToString();
                    string firstname = pobj.Ds.Tables[0].Rows[0]["Name"].ToString();
                    string empTypeno = pobj.Ds.Tables[0].Rows[0]["EmpTypeNo"].ToString();
                    string ProfileName = pobj.Ds.Tables[0].Rows[0]["ProfileName"].ToString();
                    string Email = pobj.Ds.Tables[0].Rows[0]["Email"].ToString();
                    HttpContext.Current.Session.Add("UserName", username);
                    HttpContext.Current.Session.Add("EmpFirstName", firstname);
                    HttpContext.Current.Session.Add("EmpAutoId", AutoId);
                    HttpContext.Current.Session.Add("EmpType", emptype);
                    HttpContext.Current.Session.Add("EmpTypeNo", empTypeno);
                    HttpContext.Current.Session.Add("ProfileName", ProfileName);
                    HttpContext.Current.Session.Add("ImageURL", pobj.Ds.Tables[0].Rows[0]["ImageURL"].ToString());
                    HttpContext.Current.Session.Add("UserEmail", Email);
                    HttpContext.Current.Session.Add("DBLocation", pobj.Ds.Tables[0].Rows[0]["DBLocation"].ToString());


                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.Ds.GetXml();
                }
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception)
        {
            return "Authorised Access Only";
        }
    }

    [WebMethod(EnableSession = true)]
    public string Bindlogo()
    {
        PL_Login pobj = new PL_Login();

        BL_Login.Bindlogo(pobj);

        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }

    public static string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }
        throw new Exception("No network adapters with an IPv4 address in the system!");
    }
}
