﻿using DLLOptimoOrder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class OptimoRoute : System.Web.Services.WebService
{
    public OptimoRoute()
    {
    }

    [WebMethod(EnableSession = true)]
    public string getOrderAndDriver(string TableValues)
    {
        DataTable dtOrderAutoId = new DataTable();
        dtOrderAutoId = JsonConvert.DeserializeObject<DataTable>(TableValues);

        PL_OptimoRoute pobj = new PL_OptimoRoute();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtOrderAutoId.Rows.Count > 0)
                {
                    pobj.OrderAutoIds = dtOrderAutoId;
                }
                BL_OptimoRoute.BindOrderandDriver(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string SaveRouteDetails(string TableValues, string driverLog)
    {
        DataTable dtOptimoRoute = new DataTable();
        DataTable dtDriverLog = new DataTable();
        dtOptimoRoute = JsonConvert.DeserializeObject<DataTable>(TableValues);
        dtDriverLog = JsonConvert.DeserializeObject<DataTable>(driverLog);

        PL_OptimoRoute pobj = new PL_OptimoRoute();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtOptimoRoute.Rows.Count > 0)
                {
                    pobj.RouteDetails = dtOptimoRoute;
                    pobj.DriverLog = dtDriverLog;
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_OptimoRoute.SaveRoute(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string clickonSecurity(string CheckSecurity)
    {
        PL_OptimoRoute pobj = new PL_OptimoRoute();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                BL_OptimoRoute.CheckSecurity(pobj);
                if (!pobj.isException)
                {
                    if (pobj.Ds.Tables.Count > 0 && pobj.Ds.Tables[0].Rows.Count > 0)
                    {
                        return pobj.Ds.GetXml();
                    }
                    else
                    {
                        return "false";
                    }
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getShipDetails(string TableValues)
    {
        DataTable dtShipAutoId = new DataTable();
        dtShipAutoId = JsonConvert.DeserializeObject<DataTable>(TableValues);

        PL_OptimoRoute pobj = new PL_OptimoRoute();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtShipAutoId.Rows.Count > 0)
                {
                    pobj.ShipAutoIds = dtShipAutoId;
                }
                BL_OptimoRoute.GetShipList(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string getDateDifference()
    {
        PL_OptimoRoute pobj = new PL_OptimoRoute();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_OptimoRoute.GetDateDifference(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}
