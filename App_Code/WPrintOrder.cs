﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllPrintOrderMaster;
using DllUtility;
using System.Data;
/// <summary>
/// Summary description for WPrintOrder
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WPrintOrder : System.Web.Services.WebService
{

    public WPrintOrder()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string getOrderData(string OrderAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_PrintOrderMaster pobj = new PL_PrintOrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_PrintOrderMaster.PrintOrder(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetOrderPrint(string OrderAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_PrintOrderMaster pobj = new PL_PrintOrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_PrintOrderMaster.GetOrderPrint(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetPackingOrderPrint(string OrderAutoId)//For category wise packing slip
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_PrintOrderMaster pobj = new PL_PrintOrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_PrintOrderMaster.GetPackingOrderPrint(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getPrintLabel(string OrderAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_PrintOrderMaster pobj = new PL_PrintOrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_PrintOrderMaster.getPrintLabel(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    //[WebMethod(EnableSession = true)]
    //public string getBulkOrderData1(string OrderAutoId)
    //{
    //    if (Session["EmpAutoId"] != null)
    //    {
    //        PL_PrintOrderMaster pobj = new PL_PrintOrderMaster();
    //        try
    //        {
                
    //            pobj.BulkOrderAutoId = OrderAutoId;
    //            pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
    //            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
    //            BL_PrintOrderMaster.getBulkOrderData1(pobj);
    //            if (!pobj.isException)
    //            {
    //                this.Context.Response.ContentType = "application/json; charset=utf-8";
    //                this.Context.Response.StatusCode = 200;
    //                return pobj.Ds.GetXml();
    //            }
    //            else
    //            {
    //                return pobj.exceptionMessage;
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            return ex.Message;
    //        }
    //    }
    //    else
    //    {
    //        return "Session Expired";
    //    }
    //}
    [WebMethod(EnableSession = true)]
    public string getBulkOrderData(string OrderAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_PrintOrderMaster pobj = new PL_PrintOrderMaster();
            try
            {
                //pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.BulkOrderAutoId = OrderAutoId;
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_PrintOrderMaster.getBulkOrderData(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return json;
                    //return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string PrintOrderItem(string OrderAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_PrintOrderMaster pobj = new PL_PrintOrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_PrintOrderMaster.PrintOrderItem(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string PrintOrderTemplate1(string OrderAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_PrintOrderMaster pobj = new PL_PrintOrderMaster();
            try
            {
                pobj.BulkOrderAutoId = OrderAutoId;
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_PrintOrderMaster.PrintOrderTemplate1(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string PrintOrderItemTemplate2(string OrderAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_PrintOrderMaster pobj = new PL_PrintOrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_PrintOrderMaster.PrintOrderItemTemplate2(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetOrderPrint_OrderPrintCC(string OrderAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_PrintOrderMaster pobj = new PL_PrintOrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_PrintOrderMaster.GetOrderPrint_OrderPrintCC(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetOrderPrintNew(string OrderAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_PrintOrderMaster pobj = new PL_PrintOrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_PrintOrderMaster.GetOrderPrintNew(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string PrintBulkOrderItemTemplate2(string OrderAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_PrintOrderMaster pobj = new PL_PrintOrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_PrintOrderMaster.PrintBulkOrderItemTemplate2(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string PrintBulkOrderItemTemplate3(string OrderAutoId)
    {
        
        if (Session["EmpAutoId"] != null)
        {

            PL_PrintOrderMaster pobj = new PL_PrintOrderMaster();
            try
            {
              
                //pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.BulkOrderAutoId = OrderAutoId;
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_PrintOrderMaster.getBulkOrderData(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return json;
                    //return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
