﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllCustomer;
using System.Web.Script.Serialization;
using DllUtility;
using System.Data;
using Newtonsoft.Json;
using System.Web.Script.Services;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class customerMaster : System.Web.Services.WebService
{
    public customerMaster()
    {
        gZipCompression.fn_gZipCompression();
    }
    [WebMethod(EnableSession = true)]
    public string bindDropDownsList()
    {
        PL_Customer pobj = new PL_Customer();

        BL_Customer.bindDropDownsList(pobj);
        if (Session["EmpAutoId"] != null)
        {
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return json;
                //return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string EditBankDetails(string AutoId)
    {
        PL_Customer pobj = new PL_Customer();
        pobj.AutoId = Convert.ToInt32(AutoId);
        BL_Customer.EditBankDetails(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }

    }

    [WebMethod(EnableSession = true)]
    public string BindpriceLevel(string DataValues)
    {
        PL_Customer pobj = new PL_Customer();
        pobj.CustType = Convert.ToInt32(DataValues);
        BL_Customer.bindPriceLevel(pobj);
        if (Session["EmpAutoId"] != null)
        {
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                if (json == "")
                {
                    json = "[]";
                }
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return json;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string BindLocation(string DataValues)
    {
        PL_Customer pobj = new PL_Customer();
        pobj.LocationName = DataValues;
        BL_Customer.GetLocation(pobj);
        if (Session["EmpAutoId"] != null)
        {
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string BindCity(string StateAutoId)
    {
        PL_Customer pobj = new PL_Customer();
        pobj.State1 = Convert.ToInt32(StateAutoId);
        BL_Customer.BindCity(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }

    }
    [WebMethod(EnableSession = true)]
    public List<string> BindPinCode(string ZipCode)
    {
        PL_Customer pobj = new PL_Customer();
        pobj.Zipcode1 = ZipCode;
        BL_Customer.BindPinCode(pobj);
        List<string> lstCountries = new List<string>();
        if (pobj.Ds != null && pobj.Ds.Tables.Count > 0)
        {

            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {

                lstCountries.Add(dr["Zipcode"].ToString());

            }

        }
        return lstCountries;

    }
    [WebMethod(EnableSession = true)]
    public string BindPinCodeForCity(string ZipCode)
    {
        PL_Customer pobj = new PL_Customer();
        pobj.Zipcode1 = ZipCode;
        BL_Customer.BindPinCodeForCity(pobj);
        return pobj.Ds.GetXml();

    }
    [WebMethod(EnableSession = true)]
    public string BindStateCity(string ZipCode, string CityId)
    {
        PL_Customer pobj = new PL_Customer();
        pobj.Zipcode1 = ZipCode;
        pobj.CityAutoId = Convert.ToInt32(CityId);
        BL_Customer.BindStateCity(pobj);
        return pobj.Ds.GetXml();

    }
    [WebMethod(EnableSession = true)]
    public string insertCustomer(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.XmlContactPerson = jdv["Xmlcontperson"];
                pobj.LocationAutoId = Convert.ToInt32(jdv["LocatioAutoId"]);
                pobj.CustomerName = jdv["CustomerName"];
                pobj.BusinessName = jdv["BusinessName"];
                pobj.OPTLicence = jdv["OPTLicence"];
                pobj.CustType = jdv["CustType"]; 
                pobj.BillAdd = jdv["BillAdd"];
                pobj.Zipcode1 = jdv["Zipcode1"];
                pobj.ShipAdd = jdv["ShipAdd"];
                pobj.Zipcode2 = jdv["Zipcode2"]; 
                pobj.BillAdd2 = jdv["BillAdd2"];
                pobj.ShipAdd2 = jdv["ShipAdd2"];
                pobj.City1 = jdv["CityName1"];
                pobj.City2 = jdv["CityName2"];
                pobj.StateName = jdv["State1"];
                pobj.StateName2 = jdv["State2"]; 
                pobj.TaxId = jdv["TaxId"];
                pobj.Terms = jdv["Terms"]; 
                pobj.PriceLevelAutoId = jdv["PriceLevelAutoId"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.Status = jdv["Status"];
                pobj.latitude = jdv["latitude"];
                pobj.longitude = jdv["longitude"];
                pobj.latitude1 = jdv["latitude1"];
                pobj.longitude1 = jdv["longitude1"];
                pobj.StoreOpenTime = jdv["StoreOpenTime"];
                pobj.StoreCloseTime = jdv["StoreCloseTime"];
                pobj.CustomerRemarks = jdv["CustomerRemarks"];
                

                if (Session["EmpTypeNo"].ToString() != "2")
                {
                    pobj.SalesPersonAutoId = jdv["SalesPersonAutoId"];
                }
                else
                {
                    pobj.SalesPersonAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                }

                BL_Customer.insert(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    this.Context.Response.Clear();
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string editCustomer(string CustomerId)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(CustomerId);
                BL_Customer.edit(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return json;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateCustomer(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.LocationAutoId = Convert.ToInt32(jdv["LocationAutoId"]);
                pobj.CustomerId = jdv["CustomerId"];
                pobj.CustomerName = jdv["CustomerName"];
                pobj.BusinessName = jdv["BusinessName"];
                pobj.OPTLicence = jdv["OPTLicence"];
                pobj.CustType = jdv["CustType"];
                pobj.TaxId = jdv["TaxId"];
                pobj.Terms = jdv["Terms"];
                pobj.PriceLevelAutoId = jdv["PriceLevelAutoId"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.Status = jdv["Status"];
                pobj.CustomerRemarks = jdv["CustomerRemarks"];

                pobj.StoreOpenTime = jdv["StoreOpenTime"];
                pobj.StoreCloseTime = jdv["StoreCloseTime"];
                if (Session["EmpTypeNo"].ToString() != "2")
                {
                    if(jdv["SalesPersonAutoId"]==null)
                    {
                        return "Sales Person required";
                    }
                    pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                }
                else
                {
                    pobj.SalesPersonAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                }
                BL_Customer.update(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    this.Context.Response.Clear();
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string editCustomerMain(string CustomerId)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CustomerId = CustomerId;

                BL_Customer.editMain(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string CustomerOrderList(string CustomerId, string PageIndex, string PageSize)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CustomerId = CustomerId;
                pobj.PageIndex = Convert.ToInt32(PageIndex);
                pobj.PageSize = Convert.ToInt32(PageSize);
                BL_Customer.CustomerOrderList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string CustomerDuePayment(string dataValue)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerId = jdv["CustomerId"];
                pobj.OrderNo = jdv["OrderNo"];
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);

                }
                BL_Customer.CustomerDuePayment(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string CustomerPaymentList(string dataValue)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerId = jdv["CustomerId"];
                pobj.PaymentId = jdv["PaymentId"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);

                }
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_Customer.CustomerPaymentList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string CustomerPaymentDetails(string PaymentAutoId)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {


                pobj.PaymentAutoId = Convert.ToInt32(PaymentAutoId);
                BL_Customer.CustomerPaymentDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string CustomerLogReport(string dataValue)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerId = jdv["CustomerId"];
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_Customer.CustomerLogReport(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string EditPaymentDetails(string PaymentAutoId)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {


                pobj.PaymentAutoId = Convert.ToInt32(PaymentAutoId);
                BL_Customer.EditPaymentDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string DocumentSave(string dataValue)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerId = jdv["CustomerId"];
                pobj.DocumentName = jdv["DocumentName"];
                pobj.DocumentURL = jdv["DocumentURL"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_Customer.DocumentSave(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string CustomerDocument(string dataValue)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerId = jdv["CustomerId"];
                BL_Customer.CustomerDocument(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdateDocument(string dataValue)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.FileAutoId = Convert.ToInt32(jdv["FileAutoId"]);
                pobj.DocumentName = jdv["DocumentName"];
                pobj.DocumentURL = jdv["DocumentURL"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_Customer.UpdateDocument(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string DeleteDocument(string FileAutoId)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.FileAutoId = Convert.ToInt32(FileAutoId);
                BL_Customer.DeleteDocument(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string CollectionDetails(string dataValue)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerId = jdv["CustomerId"];
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);

                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_Customer.CollectionDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string PaymentDetails(string dataValue)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerId = jdv["CustomerId"];
                pobj.PaymentAutoId = Convert.ToInt32(jdv["PaymentAutoId"]);
                BL_Customer.PaymentDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string Savecheck(string dataValue)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CheckNo = jdv["CheckNo"];
                if (jdv["CheckDate"] != "")
                    pobj.CheckDate = Convert.ToDateTime(jdv["CheckDate"]);
                pobj.PaymentRemarks = jdv["Remarks"];
                pobj.PaymentAutoId = Convert.ToInt32(jdv["PaymentAutoId"]);
                BL_Customer.Savecheck(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string CancelledPayment(string dataValue)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerId = jdv["CustomerId"];
                pobj.EmployeeRemarks = jdv["EmployeeRemarks"];
                pobj.PaymentAutoId = Convert.ToInt32(jdv["PaymentAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_Customer.CancelledPayment(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string StoreCreditDetails(string dataValue)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerId = jdv["CustomerId"];
                BL_Customer.StoreCreditDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string StoreCreditLog(string dataValue)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.CustomerId = jdv["CustomerId"];
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_Customer.StoreCreditLog(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string orderList(string ReferenceNo, string CustomerId)
    {
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.ReferenceNo = ReferenceNo;
                pobj.CustomerId = CustomerId;
                BL_Customer.orderList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string CheckSecurity(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpTypeNo"] != null)
            {
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpTypeNo"].ToString());
                pobj.SecurityKey = jdv["Security"];
                BL_Customer.checkSecurity(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string CancelPayment(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpTypeNo"] != null)
            {
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                pobj.PaymentAutoId = Convert.ToInt32(jdv["PaymentAutoId"]);
                pobj.CancelRemark = jdv["Remark"];
                BL_Customer.CancelPayment(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindCardType()
    {
        PL_Customer pobj = new PL_Customer();

        BL_Customer.bindCardType(pobj);
        if (Session["EmpAutoId"] != null)
        {
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string SaveBankDetails(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_Customer pobj = new PL_Customer();
                if (Convert.ToInt32(jdv["Status"]) == 1)
                {
                    pobj.BankName = jdv["BankName"];
                    pobj.BankAcc = jdv["ACName"];
                    pobj.RoutingNo = jdv["RoutingNo"];
                }
                else
                {
                    pobj.CardType = Convert.ToInt32(jdv["CardType"]);

                    pobj.CardNo = jdv["CardNo"];
                    if (jdv["ExpiryDate"] != "")
                        pobj.ExpiryDate = jdv["ExpiryDate"];
                    pobj.CVV = jdv["CVV"];
                }

                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.CustomerId = jdv["CustomerAutoId"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                BL_Customer.SaveBankDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetBankDetailsList(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Customer pobj = new PL_Customer();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            pobj.CustomerId = jdv["CustomerAutoId"];
            BL_Customer.GetBankDetailsList(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdateBankDetails(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_Customer pobj = new PL_Customer();
                if (Convert.ToInt32(jdv["Status"]) == 1)
                {
                    pobj.BankName = jdv["BankName"];
                    pobj.BankAcc = jdv["ACName"];
                    pobj.RoutingNo = jdv["RoutingNo"];
                    pobj.PaymentRemarks = jdv["Remarks"];
                }
                else
                {
                    pobj.CardType = Convert.ToInt32(jdv["CardType"]);

                    pobj.CardNo = jdv["CardNo"];
                    if (jdv["ExpiryDate"] != "")
                        pobj.ExpiryDate = jdv["ExpiryDate"];
                    pobj.CVV = jdv["CVV"];
                    pobj.Zipcode1 = jdv["Zipcode"];
                    pobj.PaymentRemarks = jdv["Remarks"];
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                BL_Customer.UpdateBankDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string DeleteBankDetails(string AutoId)
    {
        PL_Customer pobj = new PL_Customer();
        pobj.AutoId = Convert.ToInt32(AutoId);
        BL_Customer.DeleteBankDetails(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }

    }
    [WebMethod(EnableSession = true)]
    public string checkState(string state,string city,string zcode)
    {
        PL_Customer pobj = new PL_Customer();
        pobj.StateName = state;
        pobj.City1 = city;
        pobj.Zipcode1 = zcode;
        BL_Customer.checkZipcode(pobj);
        if (!pobj.isException)
        {
            return pobj.exceptionMessage;
        }
        else
        {
            return "false";
        }

    }

    [WebMethod(EnableSession = true)]
    public string bindContactPersonType()
    {
        PL_Customer pobj = new PL_Customer();

        BL_Customer.bindContactPersonType(pobj);
        if (Session["EmpTypeNo"] != null)
        {
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetContactPersonList(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(jdv["AutoId"]);
                BL_Customer.GetContactPersonList(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string saveContactDetail(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.MobileNo = jdv["Mobile"];
                pobj.FaxNo = jdv["Fax"];
                pobj.Email = jdv["Email"];
                pobj.AltEmail = jdv["AlternateEmail"];
                pobj.ContactPersonName = jdv["ContactPersonName"];
                pobj.Type = Convert.ToInt32(jdv["Type"]);
                pobj.Landline = jdv["Landline"];
                pobj.Landline2 = jdv["Landline2"];
                pobj.IsDefault = jdv["IsDefault"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                BL_Customer.saveContactDetail(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod(EnableSession = true)]
    public string updateContactDetail(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.MobileNo = jdv["Mobile"];
                pobj.FaxNo = jdv["Fax"];
                pobj.Email = jdv["Email"];
                pobj.AltEmail = jdv["AlternateEmail"];
                pobj.ContactPersonName = jdv["ContactPersonName"];
                pobj.Type = Convert.ToInt32(jdv["Type"]);
                pobj.Landline = jdv["Landline"];
                pobj.Landline2 = jdv["Landline2"];
                pobj.IsDefault = jdv["IsDefault"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                BL_Customer.updateContactDetail(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteCustomerContactPerson(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_Customer pobj = new PL_Customer();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                BL_Customer.deleteCustomerContactPerson(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}
