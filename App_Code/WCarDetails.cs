﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using DLLCarDetails;
/// <summary>
/// Summary description for WCarDetails
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WCarDetails : System.Web.Services.WebService
{

    public WCarDetails()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string insert(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_CarDetails pobj = new PL_CarDetails();
                pobj.CarName = jdv["CarName"];
                pobj.Year = Convert.ToInt32(jdv["Year"]);
                pobj.Make = jdv["Make"];
                pobj.Model = jdv["Model"];
                pobj.VINNumber = jdv["VINNumber"];
                pobj.TAGNumber = jdv["TAGNumber"];
                if (jdv["InspectionExpDate"] != "")
                    pobj.InspectionExpDate = Convert.ToDateTime(jdv["InspectionExpDate"]);
                pobj.StartMilage = jdv["StartMilage"];
                pobj.Description = jdv["Description"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_CarDetails.Insert(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
     [WebMethod(EnableSession = true)]
    public string update(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_CarDetails pobj = new PL_CarDetails();
                pobj.CarAutoId = Convert.ToInt32(jdv["CarAutoId"]);
                pobj.CarName = jdv["CarName"];
                pobj.Year = Convert.ToInt32(jdv["Year"]);
                pobj.Make = jdv["Make"];
                pobj.Model = jdv["Model"];
                pobj.VINNumber = jdv["VINNumber"];
                pobj.TAGNumber = jdv["TAGNumber"];
                if (jdv["InspectionExpDate"] != "")
                    pobj.InspectionExpDate = Convert.ToDateTime(jdv["InspectionExpDate"]);
                pobj.StartMilage = jdv["StartMilage"];
                pobj.Description = jdv["Description"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_CarDetails.update(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
     [WebMethod(EnableSession = true)]
    public string getCarList(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_CarDetails pobj = new PL_CarDetails();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            pobj.CarName = jdv["CarName"];
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            BL_CarDetails.CarList(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string editCar(string CarAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_CarDetails pobj = new PL_CarDetails();
            pobj.CarAutoId = Convert.ToInt32(CarAutoId);
            BL_CarDetails.EditCar(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteCar(string CarAutoId)
    {
        PL_CarDetails pobj = new PL_CarDetails();
        if (Session["EmpAutoId"] != null)
        {
            if (CarAutoId != "")
            {
                pobj.CarAutoId = Convert.ToInt32(CarAutoId);
                BL_CarDetails.DeleteCar(pobj);
            }
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
}

