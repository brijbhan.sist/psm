﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLCompanyProfile;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for CompanyProfile
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class CompanyProfile : System.Web.Services.WebService
{



    [WebMethod(EnableSession = true)]
    public string EditCompany()
    {
        PL_CompanyDetails pobj = new PL_CompanyDetails();

        try
        {
            BL_CompanyDetails.select(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            pobj.isException = true;
            pobj.exceptionMessage = ex.Message;
            return "false";
        }

    }
    [WebMethod(EnableSession = true)]
    public string UpdateCompanyDetails(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_CompanyDetails pobj = new PL_CompanyDetails();
        try
        {
            pobj.CompanyId = jdv["CompanyId"];
            pobj.CompanyName = jdv["CompanyName"];
            pobj.Address = jdv["Address"];
            pobj.EmailAddress = jdv["EmailAddress"];
            pobj.Website = jdv["Website"];
            pobj.StartLatitude = jdv["StartLatitude"];
            pobj.StartLongitude = jdv["StartLongitude"];

            pobj.MobileNo = jdv["MobileNo"];
            pobj.DateDifference = jdv["DateDifference"];
            pobj.FaxNo = jdv["FaxNo"];
            pobj.TermsCondition = jdv["TermsCondition"];
            pobj.Logo = jdv["Logo"];
            pobj.currentVersion = jdv["currentVersion"];
            if (jdv["StartDate"] != null && jdv["StartDate"] != "")
            {
                pobj.StartDate = Convert.ToDateTime(jdv["StartDate"]);
            }
            if (jdv["appLogoutTime"] != null && jdv["appLogoutTime"] != "")
            {
                pobj.appLogoutTime = Convert.ToDateTime(jdv["appLogoutTime"]);
            }

            if (jdv["SubscriptionAlertDate"] != null && jdv["SubscriptionAlertDate"] != "")
            {
                pobj.SubscriptionAlertDate = Convert.ToDateTime(jdv["SubscriptionAlertDate"]);
            }
            if (jdv["SubscriptionExpryDate"] != null && jdv["SubscriptionExpryDate"] != "")
            {
                pobj.SubscriptionExpiryDate = Convert.ToDateTime(jdv["SubscriptionExpryDate"]);
            }
            pobj.OptimoDriverLimit = Convert.ToInt32(jdv["OptimoDriverLimit"]);

            BL_CompanyDetails.update(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            pobj.isException = true;
            pobj.exceptionMessage = ex.Message;
            return "Oops,something wents wrongs.!!!";
        }
    }

}
