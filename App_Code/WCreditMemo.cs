﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllUtility;
using DLLCreditMemoMaster;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Data;
/// <summary>
/// Summary description for WCreditMemo
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WCreditMemo : System.Web.Services.WebService
{

    public WCreditMemo()
    {
        gZipCompression.fn_gZipCompression();
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string BarcodeReader(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        if (Session["EmpAutoId"] != null)
        {
            PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);     
            pobj.barcodeNo = jdv["BarcodeNo"];
            pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
            BL_CreditMemoMaster.ReadBarcode(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }


    [WebMethod(EnableSession = true)]
    public string bindDropdown()
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            BL_CreditMemoMaster.bindDropdown(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindAllDropdown()
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            BL_CreditMemoMaster.bindAllDropdown(pobj);
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return json;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string BindProduct(string CustomerAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
            pobj.CustomerAutoId = Convert.ToInt32(CustomerAutoId);
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            BL_CreditMemoMaster.BindProduct(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindUnitType(string ProductAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.ProductAutoId = Convert.ToInt32(ProductAutoId);
            BL_CreditMemoMaster.SelectUnit(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string ValidateCreditMemo(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
            pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
            pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
            pobj.Qty = Convert.ToInt32(jdv["Qty"]);
            BL_CreditMemoMaster.ValidateCreditMemo(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string insertOrderData(string TableValues, string dataValue)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
                pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
                pobj.TaxValue = Convert.ToDecimal(jdv["TaxValue"]);
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.MLTax = Convert.ToDecimal(jdv["MLTax"]);
                pobj.AdjustmentAmt = Convert.ToDecimal(jdv["AdjustmentAmt"]);
                pobj.Remarks = jdv["Remarks"];
                pobj.CreditMemoType = Convert.ToInt32(jdv["CreditMemoType"]);
                if (Convert.ToInt32(jdv["ReferenceOrderAutoId"]) != 0)
                {
                    pobj.ReferenceOrderAutoId = Convert.ToInt32(jdv["ReferenceOrderAutoId"]);
                }
                BL_CreditMemoMaster.insert(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string editCredit(string CreditAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
            BL_CreditMemoMaster.EditCredit(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateOrderData(string TableValues, string dataValue)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }

                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
                pobj.TaxValue = Convert.ToDecimal(jdv["TaxValue"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
               
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.MLTax = Convert.ToDecimal(jdv["MLTax"]);
                pobj.AdjustmentAmt = Convert.ToDecimal(jdv["AdjustmentAmt"]);
                pobj.Remarks = jdv["Remarks"];
                pobj.CreditAutoId = Convert.ToInt32(jdv["CreditAutoId"]);
                pobj.CreditMemoType = Convert.ToInt32(jdv["CreditMemoType"]);
                if (Convert.ToInt32(jdv["ReferenceOrderAutoId"]) != 0)
                {
                    pobj.ReferenceOrderAutoId = Convert.ToInt32(jdv["ReferenceOrderAutoId"]);
                }
                BL_CreditMemoMaster.updateCredit(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success!!";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateOrderData1(string TableValues, string CreditAutoId)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }

                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
                BL_CreditMemoMaster.updateCredit1(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success!!";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string ApprovedCredit(string CreditAutoId, string Status)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
            pobj.Status = Convert.ToInt32(Status);
            BL_CreditMemoMaster.ApprovedCredit(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return "Success!!";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string CompleteOrder(string CreditAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
            BL_CreditMemoMaster.CreditComplete(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return "Success!!";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string PrintCredit(string CreditAutoId)
    {
        PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();        
        pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
        BL_CreditMemoMaster.PrintCredit(pobj);
        return pobj.Ds.GetXml();

    }

    [WebMethod(EnableSession = true)]
    public string selectQtyPrice(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                BL_CreditMemoMaster.UnitDetails(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string PrintBulkCredit(string CreditAutoId)
    {
        PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
        pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
        BL_CreditMemoMaster.PrintBulkCredit(pobj);
        string json = "";
        foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
        {
            json += dr[0];
        }
        return json;

    }
}
