﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using DLLCreditMemoMaster;
/// <summary>
/// Summary description for WCreditMemoList
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WCreditMemoList : System.Web.Services.WebService
{

    public WCreditMemoList()
    {
        gZipCompression.fn_gZipCompression();
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string getCreditList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CreditNo = jdv["CreditNo"];
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]); 
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.CreditType = Convert.ToInt32(jdv["CreditType"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_CreditMemoMaster.select(pobj);
                return pobj.Ds.GetXml();

            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception)
        {
            return "false";
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindStatus()
    {
        if (Session["EmpAutoId"] != null)
        {

            PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
            BL_CreditMemoMaster.bindStatus(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteOrder(string CreditAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
                pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
                BL_CreditMemoMaster.delete(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                
                 return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string viewOrderLog(string CreditAutoId,int sortInCode)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CreditMemoMaster pobj = new PL_CreditMemoMaster();
            pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
            pobj.sortInCode = Convert.ToInt32(sortInCode);
            BL_CreditMemoMaster.BindCreditLog(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }

    }
}
