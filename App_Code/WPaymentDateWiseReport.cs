﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLPaymentDateWiseReport;
using System.Web.Script.Serialization;
using DllUtility;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WPaymentDateWiseReport : System.Web.Services.WebService {

    public WPaymentDateWiseReport()
    {
        gZipCompression.fn_gZipCompression();        
    }
    [WebMethod (EnableSession = true)]
    public string GetPaymentResport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PaymentDateWiseReport pobj = new PL_PaymentDateWiseReport();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.payemnetMode = Convert.ToInt32(jdv["Type"]);
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                BL_PaymentDateWiseReport.GetPaymentResport(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod (EnableSession = true)]
    public string GetPaymentMode()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PaymentDateWiseReport pobj = new PL_PaymentDateWiseReport();
                BL_PaymentDateWiseReport.BindPaymentMode(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }      
  }
}
