﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using DLLStatusMaster;
/// <summary>
/// Summary description for WStatusMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WStatusMaster : System.Web.Services.WebService
{

    public WStatusMaster()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string getStatusList(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_StatusMaster pobj = new PL_StatusMaster();
            //var jss = new JavaScriptSerializer();
            //var jdv = jss.Deserialize<dynamic>(dataValue);
            //pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            //pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            BL_StatusMaster.StatusList(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }

}
