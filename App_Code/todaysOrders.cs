﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using DllOrderMaster;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class todaysOrders : System.Web.Services.WebService
{

    [WebMethod(EnableSession = true)]
    public string getTodayOrders(string dataValue)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {

            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue); ;
            if (Session["EmpAutoId"] != null)
            {
                pobj.DrvAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.OrderNo = jdv["OrderNo"];
                if (jdv["FromDate"] != "")
                {
                    pobj.Fromdate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.Todate = Convert.ToDateTime(jdv["ToDate"]);
                }
                if (jdv["CustomerAutoId"] != "")
                {
                    pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                }
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                pobj.OrderStatus = Convert.ToInt32(jdv["StatusAutoId"]);
                BL_OrderMaster.getDrvAsgnOrder(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "session";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string Pick_Order(string OrderAutoId)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            BL_OrderMaster.Pick_Order(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.exceptionMessage;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string viewOrder(string OrderAutoId)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);            
            BL_OrderMaster.viewOrder(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string PaymentHistory(string CustomerAutoId)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            pobj.CustomerAutoId = Convert.ToInt32(CustomerAutoId);
            BL_OrderMaster.PaymentHistory(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}
