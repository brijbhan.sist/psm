﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using DLLoptimoRoute;

/// <summary>
/// Summary description for OptimoRouteReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class OptimoRouteReport : System.Web.Services.WebService
{

    public OptimoRouteReport()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string BindDropDown()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_OptimoRoute pobj = new PL_OptimoRoute();                
                BL_OptimoRoute.BindDropDown(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string BindList(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_OptimoRoute pobj = new PL_OptimoRoute();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.status = Convert.ToInt32(jdv["Status"]);
                pobj.AutoIdSalesPerson = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                pobj.AutoIdDriver = Convert.ToInt32(jdv["DriverAutoId"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                BL_OptimoRoute.BindReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception exp)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
