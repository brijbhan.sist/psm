﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using DLLInvReceiverPO;

/// <summary>
/// Summary description for WInvReceiverPO
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WInvReceiverPO : System.Web.Services.WebService
{

    public WInvReceiverPO()
    {
    }

    [WebMethod(EnableSession = true)]
    public string ReceiveStockList(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_RecPO pobj = new PL_RecPO();
            if (jdv["POFromDate"] != "")
            {
                pobj.POFromDate = Convert.ToDateTime(jdv["POFromDate"]);
            }
            if (jdv["POToDate"] != "")
            {
                pobj.POToDate = Convert.ToDateTime(jdv["POToDate"]);
            }
            pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.VendorId = Convert.ToInt32(jdv["VendorId"]);
            pobj.Status = Convert.ToInt32(jdv["Status"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            if (jdv["PONo"] != "")
            {
              pobj.PODraftId = jdv["PONo"];
            } 
            if (jdv["PORecNo"] != "")
            {
              pobj.RecId = jdv["PORecNo"];
            }
           
            BL_RecPO.ReceiveStockList(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string ReceieveDetails(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_RecPO pobj = new PL_RecPO();
            pobj.PoAutoId = Convert.ToInt32(jdv["AutoId"]);
            BL_RecPO.ReceieveDetails(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string ReceiveStockListInvManager(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_RecPO pobj = new PL_RecPO();
            if (jdv["POFromDate"] != "")
            {
                pobj.POFromDate = Convert.ToDateTime(jdv["POFromDate"]);
            }
            if (jdv["POToDate"] != "")
            {
                pobj.POToDate = Convert.ToDateTime(jdv["POToDate"]);
            }
            pobj.Status = Convert.ToInt32(jdv["Status"]);
            pobj.EmployeeId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.VendorId = Convert.ToInt32(jdv["VendorId"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            if (jdv["PONo"] != "")
            {
                pobj.PODraftId = jdv["PONo"];
            }
            if (jdv["PORecNo"] != "")
            {
                pobj.RecId = jdv["PORecNo"];
            }
            BL_RecPO.ReceiveStockListInvManager(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string ReceiveListByPO(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_RecPO pobj = new PL_RecPO();
            pobj.PoAutoId = Convert.ToInt32(jdv["PONo"]);
            BL_RecPO.ReceiveListByPO(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
