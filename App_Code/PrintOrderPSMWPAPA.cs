﻿using DllPrintOrderPSMWPAPA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for PrintOrderPSMWPAPA
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class PrintOrderPSMWPAPA : System.Web.Services.WebService
{

    public PrintOrderPSMWPAPA()
    {
    }

    [WebMethod(EnableSession = true)]
    public string GetPackingOrderPrint(string OrderAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_PrintOrderPSMWPAPA pobj = new PL_PrintOrderPSMWPAPA();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_PrintOrderPSMWPAPA.GetPackingOrderPrint(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}
