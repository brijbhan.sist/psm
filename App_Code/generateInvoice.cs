﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllInvoiceMaster;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
 //To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class generateInvoice : System.Web.Services.WebService {

    [WebMethod(EnableSession = true)]
    public string insertInvoice(string TableValues, string InvData)
    {
        DataTable dtInv = new DataTable();
        dtInv = JsonConvert.DeserializeObject<DataTable>(TableValues);

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(InvData);

        PL_InvoiceMaster pobj = new PL_InvoiceMaster();
        try
        {
            if (dtInv.Rows.Count > 0)
            {
                pobj.TableValue = dtInv;
            }
            pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
            pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
            pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
            pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
            pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharges"]);
            pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
            pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);           

            BL_InvoiceMaster.insert(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.exceptionMessage;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    
}
