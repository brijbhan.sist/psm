﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllUtility;
using DLLVendorPayMaster;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
/// <summary>
/// Summary description for WvendorPayMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WvendorPayMaster : System.Web.Services.WebService {

    public WvendorPayMaster () {

        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string getPayList(string dataValue)
    {
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_VendorPayMaster pobj = new PL_VendorPayMaster();
                pobj.PayId = jdv["PayId"];
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.PageIndex = jdv["pageIndex"];
                BL_VendorPayMaster.SelectList(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindDdlList()
    {
        PL_VendorPayMaster pobj = new PL_VendorPayMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_VendorPayMaster.BidnDdlList(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    //[WebMethod(EnableSession = true)]
    //public string InserPaymentDetails(string dataValue)
    //{
    //    try
    //    {
    //        if (Session["EmpAutoId"] != null)
    //        {
    //            var jss = new JavaScriptSerializer();
    //            var jdv = jss.Deserialize<dynamic>(dataValue);
    //            PL_VendorPayMaster pobj = new PL_VendorPayMaster();
    //            pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);              
    //            if (jdv["PaymentDate"] != "")
    //                pobj.PaymentDate = Convert.ToDateTime(jdv["PaymentDate"]);
    //            if (jdv["PaymentAmount"] != "")
    //                pobj.PaymentAmount = Convert.ToDecimal(jdv["PaymentAmount"]);
    //            pobj.PaymentMode = Convert.ToInt32(jdv["PaymentMode"]);
    //            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
    //            pobj.ReferenceId = jdv["ReferenceId"];
    //            pobj.Remark = jdv["Remark"];
    //            BL_VendorPayMaster.InsertPayment(pobj);
    //            if (!pobj.isException)
    //            {
    //                return "true";
    //            }
    //            else
    //            {
    //                return pobj.exceptionMessage;
    //            }
    //        }
    //        else
    //        {
    //            return "Session Expired";
    //        }
    //    }
    //    catch (Exception)
    //    {

    //        return "Oops! Something went wrong.Please try later.";
    //    }
    //}



    [WebMethod(EnableSession = true)]
    public string Editpay(string PaymentAutoid)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_VendorPayMaster pobj = new PL_VendorPayMaster();
                pobj.PayAutoId = Convert.ToInt32(PaymentAutoid);
                BL_VendorPayMaster.editPay(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updatePayment(string dataValue)
    {
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_VendorPayMaster pobj = new PL_VendorPayMaster();             
                pobj.PayAutoId = Convert.ToInt32(jdv["PaymentAutoId"]);
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                if (jdv["PaymentDate"] != "")
                    pobj.PaymentDate = Convert.ToDateTime(jdv["PaymentDate"]);
                if (jdv["PaymentAmount"] != "")
                    pobj.PaymentAmount = Convert.ToDecimal(jdv["PaymentAmount"]);
                pobj.PaymentMode = Convert.ToInt32(jdv["PaymentMode"]);
               
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.ReferenceId = jdv["ReferenceId"];
                pobj.Remark = jdv["Remark"];
                BL_VendorPayMaster.UpdatePay(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {

            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string DeletePay(string PaymentAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_VendorPayMaster pobj = new PL_VendorPayMaster();
                pobj.PayAutoId = Convert.ToInt32(PaymentAutoId);
                BL_VendorPayMaster.deletePay(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            catch (Exception)
            {

                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindPOSLists(string VendorAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_VendorPayMaster pobj = new PL_VendorPayMaster();
                pobj.VendorAutoId = Convert.ToInt32(VendorAutoId);
                BL_VendorPayMaster.BindPOSList(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string savePayment(string TableValues, string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        PL_VendorPayMaster pobj = new PL_VendorPayMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                if (jdv["PaymentAmount"] != "")
                {
                    pobj.PaymentAmount = Convert.ToDecimal(jdv["PaymentAmount"]);
                }
                if (jdv["PaymentDate"] != "")
                pobj.PaymentDate = Convert.ToDateTime(jdv["PaymentDate"]);
                pobj.PaymentMode = Convert.ToInt32(jdv["PaymentMethod"]);
                pobj.PaymentType = Convert.ToInt32(jdv["PaymentType"]);
                pobj.CheckAutoId = Convert.ToInt32(jdv["CheckAutoId"]);
                pobj.ReferenceId = jdv["ReferenceNo"];
                pobj.Remark = jdv["PaymentRemarks"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.SortAmount = Convert.ToDecimal(jdv["SortAmount"]);
                pobj.TotalCurrencyAmount = Convert.ToDecimal(jdv["TotalCurrencyAmount"]);
                pobj.PaymentCurrencyXml = jdv["PaymentCurrencyXml"];
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }
                BL_VendorPayMaster.InsertPayment(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindCheckNo(string VenderAutoId, string PayAutoId)
    {
        PL_VendorPayMaster pobj = new PL_VendorPayMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.VendorAutoId = Convert.ToInt32(VenderAutoId);
                pobj.PayAutoId = Convert.ToInt32(PayAutoId);
                BL_VendorPayMaster.bindCheckNo(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdatePaymentsettle(string TableValues, string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        PL_VendorPayMaster pobj = new PL_VendorPayMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                if (jdv["PaymentAmount"] != "")
                {
                    pobj.PaymentAmount = Convert.ToDecimal(jdv["PaymentAmount"]);
                }
                if (jdv["PaymentDate"] != "")
                    pobj.PaymentDate = Convert.ToDateTime(jdv["PaymentDate"]);
                pobj.PaymentMode = Convert.ToInt32(jdv["PaymentMethod"]);
                pobj.PaymentType = Convert.ToInt32(jdv["PaymentType"]);
                pobj.CheckAutoId = Convert.ToInt32(jdv["CheckAutoId"]);
                pobj.ReferenceId = jdv["ReferenceNo"];
                pobj.Remark = jdv["PaymentRemarks"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.SortAmount = Convert.ToDecimal(jdv["SortAmount"]);
                pobj.TotalCurrencyAmount = Convert.ToDecimal(jdv["TotalCurrencyAmount"]);
                pobj.PaymentCurrencyXml = jdv["PaymentCurrencyXml"];
                pobj.PayAutoId= Convert.ToInt32(jdv["PaymentAutoId"]);
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }
                BL_VendorPayMaster.UpdatePaymentsettle(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
}
