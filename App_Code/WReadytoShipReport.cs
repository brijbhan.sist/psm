﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using DLLSaleReport;
/// <summary>
/// Summary description for WReadytoShipReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WReadytoShipReport : System.Web.Services.WebService
{

    public WReadytoShipReport()
    {

    }


    [WebMethod (EnableSession = true)]
    public string ReadytoShipReport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                //pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                BL_SaleReport.ReadytoShipReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }


}
