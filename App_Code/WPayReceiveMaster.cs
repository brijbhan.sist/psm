﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLPayReceiveMaster;
using DllUtility;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for WPayReceiveMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WPayReceiveMaster : System.Web.Services.WebService
{

    public WPayReceiveMaster()
    {

        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string savePayment(string dataValue)
    {
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_PayReceiveMaster pobj = new PL_PayReceiveMaster();
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                if (jdv["ReceiveDate"] != "")
                    pobj.ReceiveDate = Convert.ToDateTime(jdv["ReceiveDate"]);
                if (jdv["ReceiveAmount"] != "")
                    pobj.ReceiveAmount = Convert.ToDecimal(jdv["ReceiveAmount"]);
                pobj.PaymentMode = Convert.ToInt32(jdv["PaymentMode"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.ReferenceId = jdv["ReferenceId"];
                pobj.OrderAutoId = jdv["OrderAutoId"];
                pobj.Remarks = jdv["Remarks"];
                BL_PayReceiveMaster.insert(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getPayList(string dataValue)
    {
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_PayReceiveMaster pobj = new PL_PayReceiveMaster();
                pobj.PayId = jdv["PayId"];
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);

                pobj.Status = Convert.ToInt32(jdv["Status"]);

                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.PageIndex = jdv["pageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_PayReceiveMaster.select(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }

            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {

            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindStatus()
    {
        PL_PayReceiveMaster pobj = new PL_PayReceiveMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_PayReceiveMaster.bindStatus(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string Editpay(string PaymentAutoid)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {


                PL_PayReceiveMaster pobj = new PL_PayReceiveMaster();
                pobj.PayAutoId = Convert.ToInt32(PaymentAutoid);
                BL_PayReceiveMaster.editPay(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }

            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updatePayment(string dataValue)
    {
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_PayReceiveMaster pobj = new PL_PayReceiveMaster();
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.PayAutoId = Convert.ToInt32(jdv["PaymentAutoId"]);
                if (jdv["ReceiveDate"] != "")
                    pobj.ReceiveDate = Convert.ToDateTime(jdv["ReceiveDate"]);
                if (jdv["ReceiveAmount"] != "")
                    pobj.ReceiveAmount = Convert.ToDecimal(jdv["ReceiveAmount"]);
                pobj.PaymentMode = Convert.ToInt32(jdv["PaymentMode"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.ReferenceId = jdv["ReferenceId"];
                 pobj.OrderAutoId = jdv["OrderAutoId"];
                pobj.Remarks = jdv["Remarks"];
                BL_PayReceiveMaster.update(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {

            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string DeletePay(string PaymentAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {


                PL_PayReceiveMaster pobj = new PL_PayReceiveMaster();
                pobj.PayAutoId = Convert.ToInt32(PaymentAutoId);
                BL_PayReceiveMaster.delete(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            catch (Exception)
            {

                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string ViewDueAmount(string CustomerAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {


                PL_PayReceiveMaster pobj = new PL_PayReceiveMaster();
                pobj.CustomerAutoId = Convert.ToInt32(CustomerAutoId);
                BL_PayReceiveMaster.ViewDueAmount(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }

            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
