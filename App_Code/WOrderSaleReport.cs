﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLOrderSaleReport;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for WOrderSaleReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WOrderSaleReport : System.Web.Services.WebService {

    [WebMethod(EnableSession = true)]
    public string BindSalesPerson()
    {
        try
        {
            PL_OrderSaleReport pobj = new PL_OrderSaleReport();
            BL_OrderSaleReport.bindDropdown(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string OrderSaleReport(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_OrderSaleReport pobj = new PL_OrderSaleReport();
        try
        {
            pobj.CustomerType = Convert.ToInt32(jdv["CustomerType"]);
            pobj.Customer = Convert.ToInt32(jdv["Customer"]);
            pobj.SalesPerson = Convert.ToInt32(jdv["SalesPerson"]);
            pobj.Category = Convert.ToInt32(jdv["Category"]);
            pobj.SubCategory = Convert.ToInt32(jdv["SubCategory"]);
            pobj.Product = Convert.ToInt32(jdv["Product"]);
            if (jdv["FromDate"]!="")
            pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
            pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            
            BL_OrderSaleReport.OrderSaleReport(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}
