﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

public class OrderEmailTemplete
{
    public OrderEmailTemplete()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string getEmailTemplete(DataSet ds)
    {
        string html = "<html>";
        html += "<head>";
        html += "<style>";
        html += @"td {
            width: 50%;
            padding: 3px 8px;
        }

        table {
            width: 100%;
        }

        body {
            font-size: 14px;
            font-family: monospace;
        }

        thead > tr {
            background: #fff;
            color: #000;
            font-weight: 700;
            border-top: 1px solid #5f7c8a;
        }

        .InvContent {
            width: 800px;
            border: 2px solid #e5e5e5;
            border-radius: 10px;
        }

        .tdLabel {
            font-weight: 700;
        }

        #tblProduct > thead > tr > td, #tblProduct > tbody > tr > td {
            width: 50px;
        }

        @media print {
            #btnPrint {
                display: none;
            }
        }

        legend {
            border-bottom: 1px solid #5f7c8a;
        }

        .tableCSS > tbody > tr > td,
        .tableCSS > thead > tr > td,
        .tableCSS > tfoot > tr > td {
            border: 1px solid #5f7c8a;
        }
    </style>
    <title></title>
</head>
<body>
";
        html += "<div>";
        html += "<table><tbody>";
        html += @"<tr>
                    <td colspan='2'>                       
                        <center>
                            <img src='../images/LOGO_transparent.png' id='logo' class='img-responsive' style='width:20%;'><br />
                            <span style='font-size: 12px;text-align:center;'>";
        html += "<span>" + ds.Tables[0].Rows[0]["Address"].ToString() + "</span>&nbsp;&nbsp;|&nbsp;&nbsp;Phone:<span>" + ds.Tables[0].Rows[0]["MobileNo"].ToString() + "</span>,Fax:<span>" + ds.Tables[0].Rows[0]["FaxNo"].ToString() + "</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span>" + ds.Tables[0].Rows[0]["Website"].ToString() + "</span>";
        html += @"</span>
                        </center>
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        <legend style='text-align: center; font-weight: 700;'></legend>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class='table tableCSS'>
                            <tbody>
                                <tr>
                                    <td>A/C #</td>
                                 ";
html +="<td>"+ds.Tables[0].Rows[0]["CustomerId"].ToString() +"</td></tr>";
html +="<tr><td>Store Name</td><td>"+ds.Tables[0].Rows[0]["CustomerName"].ToString() +"</td></tr>";
html +="<tr><td>Contact</td><td>"+ds.Tables[0].Rows[0]["Contact"].ToString() +"</td></tr>";
html += @"  <tr>
                                    <td>Terms</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>";

        html += @"<table class='table tableCSS'>
                            <tbody>
                                <tr>
                                    <td rowspan='2'><span>Shipping Address:</span>&nbsp;<span></span></td>
                                    <td><span>Billing Address:</span>&nbsp;<span></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style='vertical-align: top;'>
                        <table class='table tableCSS'>
                            <tbody>
                                <tr>
                                    <td>Sales Rep</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=>S.O.# </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>S.O. Date</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Ship Via</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Cust Type</td>
                                    <td></td>
                                </tr>                             
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>";
        html += @"
        <fieldset>
            <table class='table tableCSS'>
                <thead>
                    <tr>
                        <td style='width:10px'>Qty (Nos.)</td>
                        <td style='width:10px'>Barcode</td>
                        <td style='width:10px'>Product ID</td>
                        <td>Product Desc.</td>
                        <td style='text-align: right;width:10px'>Unit Price($)</td>
                        <td style='text-align: right;width:10px'>Tax(%)</td>
                        <td style='text-align: right;width:10px'>SRP($)</td>
                        <td style='text-align: right;width:10px'>GP(%)</td>
                        <td style='text-align: right;width:10px'>Total Price($)</td>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </fieldset>";
        html += @"
        <fieldset>
            <table style='width: 100%;'>
                <tbody>
                    <tr>
                        <td style='width: 33%;'></td>
                        <td style='width: 33%;'></td>
                        <td style='width: 33%;'>
                            <table class='table tableCSS'>
                                <tbody>
                                    <tr>
                                        <td>Total Qty(Nos.)</td>
                                        <td style='text-align: right;'></td>
                                    </tr>
                                    <tr>
                                        <td>Total without Tax & Shipping</td>
                                        <td style='text-align: right;'>$&nbsp;<span>0</span></td>
                                    </tr>
                                    <tr>
                                        <td>Disc.(%)</td>
                                        <td style='text-align: right;'><span id='disc'>0.00</span>&nbsp;%</td>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <td style='text-align: right;'>$&nbsp;<span>0.00</span></td>
                                    </tr>
                                    <tr>
                                        <td>Shipping</td>
                                        <td style='text-align: right;'>$&nbsp;<span>0.00</span></td>
                                    </tr>
                                    <tr>
                                        <td>ML Tax</td>
                                        <td style='text-align: right;'>$&nbsp;<span>0.00</span></td>
                                    </tr>
                                    <tr>
                                        <td>Grand Total</td>
                                        <td style='text-align: right;'>$&nbsp;<span>0.00</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
        <br />
        <fieldset>
            <legend></legend>
            <center>
                <pre>
               
                </pre>
            </center>
        </fieldset>
    </div>
    
</body>
</html>
";

        return html;
    }
}