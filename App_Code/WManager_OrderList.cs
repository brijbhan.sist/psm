﻿using DLL_Manager_OrderMaster;
using DllUtility;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Script.Serialization;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using System.Web;
/// <summary>
/// Summary description for WManager_OrderList
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WManager_OrderList : System.Web.Services.WebService
{

    public WManager_OrderList()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string bindStatus()
    {
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_Manager_OrderMaster.bindStatus(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string getOrderList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderNo = jdv["OrderNo"];
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoid"]);
                pobj.SalesPerson = jdv["SalesPerson"];
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.Fromdate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.Todate = Convert.ToDateTime(jdv["ToDate"]);
                }
                if (jdv["DeliveryFromDate"] != null && jdv["DeliveryFromDate"] != "")
                {
                    pobj.DeliveryFromDate = Convert.ToDateTime(jdv["DeliveryFromDate"]);
                }
                if (jdv["DeliveryToDate"] != null && jdv["DeliveryToDate"] != "")
                {
                    pobj.DeliveryToDate = Convert.ToDateTime(jdv["DeliveryToDate"]);
                }
                pobj.OrdStatus = jdv["Status"];
                pobj.OrderType = Convert.ToInt32(jdv["OrderType"]);
                pobj.PageIndex = jdv["pageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.TypeShipping = jdv["ShippingType"];
                pobj.OrderBy = Convert.ToInt32(jdv["OrderBy"]);
                pobj.StateFilter= jdv["State"];
                pobj.DriverAutoId = Convert.ToInt32(jdv["Driver"]);
                BL_Manager_OrderMaster.getOrderList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string getOrderData(string OrderAutoId)
    {
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
            BL_Manager_OrderMaster.getOrderData(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string getDriverList(string OrderAutoId, int LoginEmpType)
    {
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.LoginEmpType = LoginEmpType;
                BL_Manager_OrderMaster.getDriverList(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string SetAsProcess(string OrderAutoId)
    {
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_Manager_OrderMaster.SetAsProcess(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string AssignDriver(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.DriverAutoId = Convert.ToInt32(jdv["DriverAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                if (jdv["AssignDate"] != "")
                {
                    pobj.AsgnDate = Convert.ToDateTime(jdv["AssignDate"]);
                }
                BL_Manager_OrderMaster.AssignDriver(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string cancelOrder(string OrderAutoId, string Remarks)
    {
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.Remarks = Remarks;
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.IPAddress = HttpContext.Current.Request.UserHostAddress;
                BL_Manager_OrderMaster.cancelOrder(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string clickonSecurity(string CheckSecurity, string OrderAutoId)
    {
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                BL_Manager_OrderMaster.clickonSecurity(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getMailBody(int OrderId)
    {
        if (Session["EmpAutoId"] != null)
        {
            string Body = "";
            Body = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'>";
            Body += "<head> <style>table{border-collapse:collapse;}td {padding: 3px 8px;}.text-center{text-align:center}";
            Body += "table {width: 100%;}body {font-size: 14px;font-family: monospace;}";
            Body += ".center{text-align: center;}.right {text-align: right;}thead > tr {background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a;}";
            Body += "thead > tr > td {text-align: center !important;}.InvContent {width: 100%;}.tdLabel {font-weight: 700;}";
            Body += "@media print {#btnPrint {display: none;}}legend {border-bottom: 1px solid #5f7c8a;}";
            Body += ".tableCSS > tbody > tr > td,.tableCSS > thead > tr > td,.tableCSS > tfoot > tr > td {border: 1px solid #5f7c8a;}pre{border:none !important;}";
            Body += "</style>";
            Body += "</head>";
            Body += "<body style='font-family: Arial; font-size: 12px'>";
            Body += "<div class='InvContent'>";
            Body += "<fieldset>";
            Body += "<table class='table tableCSS'>";
            Body += "<tbody>";
            Body += "<tr>";
            Body += "<td colspan='6' style='text-align: center'>";
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPrintOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", 41);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", OrderId);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                DataSet Ds = new DataSet();
                sqlAdp.Fill(Ds);

                Body += "<span style='font-size: 20px;' id='PageHeading'>" + Ds.Tables[1].Rows[0]["CustomerName"].ToString() + " - " + Ds.Tables[1].Rows[0]["OrderNo"].ToString() + "</span>";
                Body += "<span style='font-size: 20px;float:right;font-weight:600'><i>SALES ORDER</i></span>";
                Body += "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td rowspan='2' style='width: 80px !important;'>";
                Body += "<img src='" + Ds.Tables[0].Rows[0]["Logo"].ToString() + "' id='logo' class='img-responsive' style='width: 100%;'/></td>";
                Body += "<td><span id='CompanyName'>" + Ds.Tables[0].Rows[0]["CompanyName"].ToString() + "</span></td>";
                Body += "<td class='tdLabel' style='white-space: nowrap'>Customer Id</td>";
                Body += "<td id='acNo' style='white-space: nowrap'>" + Ds.Tables[1].Rows[0]["CustomerId"].ToString() + "</td>";
                Body += "<td class='tdLabel' style='white-space: nowrap'>Sales Rep</td>";
                Body += "<td id='salesRep' style='white-space: nowrap'>" + Ds.Tables[1].Rows[0]["SalesPerson"].ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span id='Address'>" + Ds.Tables[0].Rows[0]["Address"].ToString() + "</span></td>";
                Body += "<td class='tdLabel' style='white-space: nowrap'>Customer Contact</td>";
                Body += "<td id='storeName' style='white-space: nowrap'>" + Ds.Tables[1].Rows[0]["ContactPersonName"].ToString() + "</td>";
                Body += "<td class='tdLabel'>Order No</td>";
                Body += "<td id='so'>" + Ds.Tables[1].Rows[0]["OrderNo"].ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span id='Website'>" + Ds.Tables[0].Rows[0]["Website"].ToString() + "</span></td>";
                Body += "<td>Phone : <span id='Phone'> Phone : " + Ds.Tables[0].Rows[0]["MobileNo"].ToString() + "</span></td>";
                Body += "<td class='tdLabel'>Contact No</td>";
                Body += "<td id='ContPerson'>" + Ds.Tables[1].Rows[0]["Contact"].ToString() + "</td>";
                Body += "<td class='tdLabel' style='white-space: nowrap'>Order Date</td>";
                Body += "<td id='soDate' style='white-space: nowrap'>" + Ds.Tables[1].Rows[0]["OrderDate"].ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span id='EmailAddress'>" + Ds.Tables[0].Rows[0]["EmailAddress"].ToString() + "</span></td>";
                Body += "<td>Fax:<span id='FaxNo'>" + Ds.Tables[0].Rows[0]["FaxNo"].ToString() + "</span></td>";
                Body += "<td class='tdLabel'>Terms</td>";
                Body += "<td style='font-size: 13px'><b><span id='terms'>" + Ds.Tables[1].Rows[0]["TermsDesc"].ToString() + "</span></b></td>";
                Body += "<td class='tdLabel'>Ship Via</td>";
                Body += "<td id='shipVia'>" + Ds.Tables[1].Rows[0]["ShippingType"].ToString() + "</td>";
                Body += "</tr>";

                string Bn = Ds.Tables[1].Rows[0]["BusinessName"].ToString();
                if (Bn != "" && Bn != null)
                {
                    Body += "<tr>";
                    Body += "<td><span class='tdLabel'>Business Name:</span></td>";
                    Body += "<td colspan='5'><span id='BusinessName'>" + Bn + "</span></td>";
                    Body += "</tr>";
                }
                Body += "<tr>";
                Body += "<td><span class='tdLabel'>Shipping Address:</span></td>";
                Body += "<td colspan='5'><span id='shipAddr' style='font-size: 13px'>" + Ds.Tables[1].Rows[0]["ShipAddr"].ToString() + "</span></td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span class='tdLabel'>Billing Address:</span></td>";
                Body += "<td colspan='5'><span id='billAddr' style='font-size: 13px'>" + Ds.Tables[1].Rows[0]["BillAddr"].ToString() + "</span></td>";
                Body += "</tr>";
                Body += "</tbody>";
                Body += "</table>";
                Body += "</fieldset><fieldset>";
                //Repeat Table
                Body += "<table class='table tableCSS' id='tblProduct'>";
                Body += "<thead>";
                Body += "<tr>";
                Body += "<td class='text-center'>Qty</td><td style='width: 10px;white-space:nowrap' class='text-center'>Unit Type</td><td style='white-space: nowrap'>Product ID</td>" +
                "<td style='white-space: nowrap'>Product Description</td><td class='SRP' style='text-align: right; width: 10px'>SRP</td><td class='GP' style='text-align: right; width: 10px'>GP(%)</td>" +
                "<td class='UnitPrice' style='text-align: right; width: 10px'>Price</td><td class='totalPrice' style='text-align: right; width: 10px; white-space: nowrap'>Total Price</td>";
                Body += "</tr>";
                Body += "</thead>";
                Body += "<tbody>";
                if (Ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow dr in Ds.Tables[2].Rows)
                    {
                        Body += "<tr>";
                        Body += "<td class='text-center'>" + dr["RequiredQty"] + "</td><td style='width: 10px'>" + dr["UnitType"] + "</td>" +
                        "<td style='white-space: nowrap' class='text-center'>" + dr["ProductId"] + "</td>" +
                        "<td style='white-space: nowrap'>" + dr["ProductName"] + "</td>" +
                        "<td class='SRP' style='text-align: right; width: 10px'>" + dr["SRP"] + "</td>" +
                        "<td class='GP' style='text-align: right; width: 10px'>" + dr["GP"] + "</td>" +
                        "<td class='UnitPrice' style='text-align: right; width: 10px'>" + dr["UnitPrice"] + "</td>" +
                        "<td class='totalPrice' style='text-align: right; width: 10px; white-space: nowrap'>" + dr["NetPrice"] + "</td>";
                        Body += "</tr>";
                    }
                }

                Body += "</tbody>";
                Body += " <tbody></tbody>";
                Body += "</table></field>";
                Body += "<field>";
                Body += "<table style='width: 100%;'>";
                Body += "<tbody>";
                Body += "<tr>";
                Body += "<td style='width: 66%' colspan='2'><center><div id='TC'>" + Ds.Tables[0].Rows[0]["TermsCondition"].ToString() + "</div></center></td>";
                Body += "<td style='width: 33%;'>";
                Body += "<table class='table tableCSS' style='width:80%;float:right'>";
                Body += "<tbody>";
                Body += "<tr>";
                Body += "<td class='tdLabel'>Total Items</td>";
                Body += "<td id='totalQty' style='text-align: right;'>" + Ds.Tables[2].Rows.Count.ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td class='tdLabel'>Sub Total</td>";
                Body += "<td style='text-align: right;'><span id='totalwoTax'>" + Ds.Tables[1].Rows[0]["TotalAmount"].ToString() + "</span></td>";
                Body += "</tr>";
                string dis = Ds.Tables[1].Rows[0]["OverallDiscAmt"].ToString();
                if (dis != "0.00" && dis != null)
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Discount</td>";
                    Body += "<td style='text-align: right;'><span id='disc'>" + dis + "</span></td>"; ;
                    Body += "</tr>";
                }
                string tax = Ds.Tables[1].Rows[0]["TotalTax"].ToString();
                if (tax != "0.00" && tax != null)
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Tax</td>";
                    Body += "<td style='text-align: right;'><span id='tax'>" + tax + "</span></td>";
                    Body += "</tr>";
                }
                string SC = Ds.Tables[1].Rows[0]["ShippingCharges"].ToString();
                if (SC != "0.00" && SC != null)
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Shipping</td>";
                    Body += "<td style='text-align: right;'><span id='shipCharges'>" + SC + "</span></td>";
                    Body += "</tr>";
                }
                string NTax = Ds.Tables[1].Rows[0]["MLTax"].ToString();
                if (NTax != null && NTax != "0.00")
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>NJ ECig/Nicotine Tax</td>";
                    Body += "<td style='text-align: right;'><span id='MLTax'>" + NTax + "</span></td>";
                    Body += "</tr>";
                }
                string WTax = Ds.Tables[1].Rows[0]["WeightTax"].ToString();
                if (WTax != "" && WTax != null)
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Weight Tax</td>";
                    Body += "<td style='text-align: right;'><span id='WeightTax'>" + WTax + "</span></td>";
                    Body += "</tr>";
                }
                string AAmt = Ds.Tables[1].Rows[0]["AdjustmentAmt"].ToString();
                if (AAmt != null && AAmt != "0.00")
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Adjustment</td>";
                    Body += "<td style='text-align: right;'><span id='spAdjustment'>" + AAmt + "</span></td>";
                    Body += "</tr>";
                }
                Body += "<tr>";
                Body += "<td class='tdLabel'>Grand Total</td>";
                Body += "<td style='text-align: right;'><span id='grandTotal'>" + Ds.Tables[1].Rows[0]["GrandTotal"].ToString() + "</span></td>";
                Body += "</tr>";

                string DAmmt = Ds.Tables[1].Rows[0]["DeductionAmount"].ToString();
                if (DAmmt != "0.00" && DAmmt != null)
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Credit memo</td>";
                    Body += "<td style='text-align: right;'><span id='spnCreditmemo'>" + DAmmt + "</span></td>";
                    Body += "</tr>";
                }
                string CAmt = Ds.Tables[1].Rows[0]["CreditAmount"].ToString();
                if (CAmt != null && CAmt != "0.00")
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Store Credit</td>";
                    Body += "<td style='text-align: right;'><span id='spnStoreCredit'>" + CAmt + "</span></td>";
                    Body += "</tr>";
                }
                Body += "<tr>";
                Body += "<td class='tdLabel'>Payable Amount</td>";
                Body += "<td style='text-align: right;'><span id='spnPayableAmount'>" + Ds.Tables[1].Rows[0]["PayableAmount"].ToString() + "</span></td>";
                Body += "</tr>";
                Body += "</tbody>";
                Body += "</table>";
                Body += "</td>";
                Body += "</tr>";
                Body += "</tbody>";
                Body += "</table>";
                Body += "</field>";
                Body += "</div>";
                Body += "</body>";
                Body += "</html>";
                string ReceiverEmailId = Ds.Tables[1].Rows[0]["ReceiverEmail"].ToString();
                if (ReceiverEmailId != null && ReceiverEmailId != string.Empty && ReceiverEmailId.Length > 0)
                {
                    string emailmsg = MailSending.SendMailResponse(ReceiverEmailId, "", "", "New order generated", Body.ToString(), "Developer");

                    if (emailmsg == "Successful")
                    {
                        return "true";
                    }
                    else
                    {
                        return "EmailNotSend";
                    }
                }
                else
                {
                    return "receiverMail";
                }
            }
            catch (Exception)
            {
                return "false";
            }
            //return Body;
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string ViewOrderDetails(string OrderAutoId)
    {
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_Manager_OrderMaster.ViewOrderDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string getMailBoddy11(string dataValue)
    {

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);

        if (Session["EmpAutoId"] != null)
        {
            string CSSStyle = "table{border-collapse:collapse;}td {padding: 5px 7px;}.text-center{text-align:center}";
            CSSStyle += "table {width: 100%;}body {font-size: 13px;font-family: monospace;}";
            CSSStyle += ".center{text-align: center;}.right {text-align: right;}thead > tr {background: #fff;color: #000;font-weight: 600;border-top: 1px solid #5f7c8a;}";
            CSSStyle += "thead > tr > td {text-align: center !important;}.InvContent {width: 100%;}.tdLabel {font-weight: 600;}";
            CSSStyle += "@media print {#btnPrint {display: none;}}legend {border-bottom: 1px solid #5f7c8a;}";
            CSSStyle += ".tableCSS > tbody > tr > td,.tableCSS > thead > tr > td,.tableCSS > tfoot > tr > td {border: 1px solid #5f7c8a;}pre{border:none !important;}";
            CSSStyle += "";
            string Body = "";
            Body = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'>";
            Body += "<head> <title></title> <style  type='text/css'></style>";
            Body += "</head>";
            Body += "<body style='font-family: Arial; font-size: 13px;width:1000px'>";
            Body += "<div class='InvContent'>";
            Body += "<fieldset>";
            Body += "<table class='table tableCSS'>";
            Body += "<tbody>";
            Body += "<tr style='text-align:center'>";
            Body += "<td colspan='6' style='padding:8px 8px;'>";
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPrintOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", 41);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", Convert.ToInt32(jdv["OrderId"]));
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                DataSet Ds = new DataSet();
                sqlAdp.Fill(Ds);

                Body += "<div><span style='font-size: 20px;text-align:center;font-weight:600' id='PageHeading'>" + Ds.Tables[1].Rows[0]["CustomerName"].ToString() + " - " + Ds.Tables[1].Rows[0]["OrderNo"].ToString() + "</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                Body += "<span style='font-size: 20px;text-align:right;font-weight:600;float:right'><i>SALES ORDER</i></span></div>";
                Body += "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td rowspan='2' style='width: 21% !important;'>";
                Body += "<img src='" + Ds.Tables[0].Rows[0]["LogoforEmail"].ToString() + "' id='logo' class='img-responsive' style='width: 100%;'/>";
                Body += "</td>";
                Body += "<td style='width: 26% !important;'><span id='CompanyName'>" + Ds.Tables[0].Rows[0]["CompanyName"].ToString() + "</span></td>";
                Body += "<td class='tdLabel' style='white-space: nowrap;width:18% !important'>Customer Id</td>";
                Body += "<td id='acNo' style='white-space: nowrap;width:23% !important'>" + Ds.Tables[1].Rows[0]["CustomerId"].ToString() + "</td>";
                Body += "<td class='tdLabel' style='white-space: nowrap;width:13% !important'>Sales Rep</td>";
                Body += "<td id='salesRep' style='white-space: nowrap;width:19% !important'>" + Ds.Tables[1].Rows[0]["SalesPerson"].ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span id='Address'>" + Ds.Tables[0].Rows[0]["Address"].ToString() + "</span></td>";
                Body += "<td class='tdLabel' style='white-space: nowrap'>Customer Contact</td>";
                Body += "<td id='storeName' style='white-space: nowrap'>" + Ds.Tables[1].Rows[0]["ContactPersonName"].ToString() + "</td>";
                Body += "<td class='tdLabel' style='white-space: nowrap'>Order No</td>";
                Body += "<td id='so'>" + Ds.Tables[1].Rows[0]["OrderNo"].ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span id='Website' style='white-space: nowrap'>" + Ds.Tables[0].Rows[0]["Website"].ToString() + "</span></td>";
                Body += "<td style='white-space: nowrap'>Phone : <span id='Phone'> Phone : " + Ds.Tables[0].Rows[0]["MobileNo"].ToString() + "</span></td>";
                Body += "<td class='tdLabel'>Contact No</td>";
                Body += "<td id='ContPerson'>" + Ds.Tables[1].Rows[0]["Contact"].ToString() + "</td>";
                Body += "<td class='tdLabel' style='white-space: nowrap'>Order Date</td>";
                Body += "<td id='soDate' style='white-space: nowrap'>" + Ds.Tables[1].Rows[0]["OrderDate"].ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span id='EmailAddress'>" + Ds.Tables[0].Rows[0]["EmailAddress"].ToString() + "</span></td>";
                Body += "<td>Fax:<span id='FaxNo'>" + Ds.Tables[0].Rows[0]["FaxNo"].ToString() + "</span></td>";
                Body += "<td class='tdLabel'>Terms</td>";
                Body += "<td style='font-size: 12px'><b><span id='terms'>" + Ds.Tables[1].Rows[0]["TermsDesc"].ToString() + "</span></b></td>";
                Body += "<td class='tdLabel'>Ship Via</td>";
                Body += "<td id='shipVia'>" + Ds.Tables[1].Rows[0]["ShippingType"].ToString() + "</td>";
                Body += "</tr>";

                string Bn = Ds.Tables[1].Rows[0]["BusinessName"].ToString();
                if (Bn != "" && Bn != null)
                {
                    Body += "<tr>";
                    Body += "<td><span class='tdLabel'>Business Name:</span></td>";
                    Body += "<td colspan='5'><span id='BusinessName'>" + Bn + "</span></td>";
                    Body += "</tr>";
                }
                Body += "<tr>";
                Body += "<td><span class='tdLabel'>Shipping Address:</span></td>";
                Body += "<td colspan='5'><span id='shipAddr' style='font-size: 15px'>" + Ds.Tables[1].Rows[0]["ShipAddr"].ToString() + " " + Ds.Tables[1].Rows[0]["City2"].ToString() + " " + Ds.Tables[1].Rows[0]["State2"].ToString() + " " + Ds.Tables[1].Rows[0]["Zipcode2"].ToString() + "</span></td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span class='tdLabel'>Billing Address:</span></td>";
                Body += "<td colspan='5'><span id='billAddr' style='font-size: 15px'>" + Ds.Tables[1].Rows[0]["BillAddr"].ToString() + " " + Ds.Tables[1].Rows[0]["City1"].ToString() + " " + Ds.Tables[1].Rows[0]["State1"].ToString() + " " + Ds.Tables[1].Rows[0]["Zipcode1"].ToString() + "</span></td>";
                Body += "</tr>";
                Body += "</tbody>";
                Body += "</table>";
                Body += "</fieldset><br/><fieldset>";
                //Repeat Table
                Body += "<table class='table tableCSS' id='tblProduct'>";
                Body += "<thead>";
                Body += "<tr>";
                Body += "<td class='text-center' style='width:8px;'>Qty</td><td style='width: 10px;white-space:nowrap' class='text-center'>Unit Type</td><td style='white-space: nowrap;width:12px;'>Product ID</td>" +
                "<td style='white-space: nowrap;width:30px;'>Product Description</td><td class='SRP' style='text-align: center; width: 10px'>SRP</td><td class='GP' style='text-align: center; width: 10px'>GP(%)</td>" +
                "<td class='UnitPrice' style='text-align: center; width: 10px'>Price</td><td class='totalPrice' style='text-align: center; width: 10px; white-space: nowrap'>Total Price</td>";
                Body += "</tr>";
                Body += "</thead>";
                Body += "<tbody>";
                if (Ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow dr in Ds.Tables[2].Rows)
                    {
                        Body += "<tr>";
                        Body += "<td class='text-center'>" + dr["QtyShip"] + "</td><td style='width: 25px'>" + dr["UnitType"] + "</td>" +
                        "<td style='white-space: nowrap' class='text-center'>" + dr["ProductId"] + "</td>" +
                        "<td style='white-space: nowrap'>" + dr["ProductName"] + "</td>" +
                        "<td class='SRP' style='text-align: right; width: 28px'>" + dr["SRP"] + "</td>" +
                        "<td class='GP' style='text-align: right; width: 28px'>" + dr["GP"] + "</td>" +
                        "<td class='UnitPrice' style='text-align: right; width: 28px'>" + dr["UnitPrice"] + "</td>" +
                        "<td class='totalPrice' style='text-align: right; width: 28px; white-space: nowrap'>" + dr["NetPrice"] + "</td>";
                        Body += "</tr>";
                    }
                }

                Body += "</tbody>";
                Body += "</table></fieldset><br/>";
                Body += "<fieldset>";
                Body += "<table style='width: 100%;'>";
                Body += "<tbody>";
                Body += "<tr>";
                Body += "<td style='width: 67%' colspan='2'>" + "<center><div id='TC'>" + Ds.Tables[0].Rows[0]["tc"].ToString() + "</div></center>" + "</td>";
                Body += "<td style='width: 33%;'>";
                Body += "<table class='table tableCSS' style='width:100%;'>";
                Body += "<tbody>";
                Body += "<tr>";
                Body += "<td class='tdLabel'>Total Items</td>";
                Body += "<td id='totalQty' style='text-align: right;'>" + Ds.Tables[2].Rows.Count.ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td class='tdLabel'>Sub Total</td>";
                Body += "<td style='text-align: right;'><span id='totalwoTax'>" + Ds.Tables[1].Rows[0]["TotalAmount"].ToString() + "</span></td>";
                Body += "</tr>";
                string dis = Ds.Tables[1].Rows[0]["OverallDiscAmt"].ToString();
                if (dis != "0.00" && dis != null)
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Discount</td>";
                    Body += "<td style='text-align: right;'><span id='disc'>" + dis + "</span></td>"; ;
                    Body += "</tr>";
                }
                string tax = Ds.Tables[1].Rows[0]["TotalTax"].ToString();
                if (tax != "0.00" && tax != null)
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Tax</td>";
                    Body += "<td style='text-align: right;'><span id='tax'>" + tax + "</span></td>";
                    Body += "</tr>";
                }
                string SC = Ds.Tables[1].Rows[0]["ShippingCharges"].ToString();
                if (SC != "0.00" && SC != null)
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Shipping</td>";
                    Body += "<td style='text-align: right;'><span id='shipCharges'>" + SC + "</span></td>";
                    Body += "</tr>";
                }
                string NTax = Ds.Tables[1].Rows[0]["MLTax"].ToString();
                if (NTax != null && NTax != "0.00")
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>" + jdv["PrintLabel"] + "</td>";
                    Body += "<td style='text-align: right;'><span id='MLTax'>" + NTax + "</span></td>";
                    Body += "</tr>";
                }
                string WTax = Ds.Tables[1].Rows[0]["WeightTax"].ToString();
                if (WTax != "" && WTax != null)
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Weight Tax</td>";
                    Body += "<td style='text-align: right;'><span id='WeightTax'>" + WTax + "</span></td>";
                    Body += "</tr>";
                }
                string AAmt = Ds.Tables[1].Rows[0]["AdjustmentAmt"].ToString();
                if (AAmt != null && AAmt != "0.00")
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Adjustment</td>";
                    Body += "<td style='text-align: right;'><span id='spAdjustment'>" + AAmt + "</span></td>";
                    Body += "</tr>";
                }
                Body += "<tr>";
                Body += "<td class='tdLabel'>Grand Total</td>";
                Body += "<td style='text-align: right;'><span id='grandTotal'>" + Ds.Tables[1].Rows[0]["GrandTotal"].ToString() + "</span></td>";
                Body += "</tr>";

                string DAmmt = Ds.Tables[1].Rows[0]["DeductionAmount"].ToString();
                if (DAmmt != "0.00" && DAmmt != null)
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Credit memo</td>";
                    Body += "<td style='text-align: right;'><span id='spnCreditmemo'>" + DAmmt + "</span></td>";
                    Body += "</tr>";
                }
                string CAmt = Ds.Tables[1].Rows[0]["CreditAmount"].ToString();
                if (CAmt != null && CAmt != "0.00")
                {
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Store Credit</td>";
                    Body += "<td style='text-align: right;'><span id='spnStoreCredit'>" + CAmt + "</span></td>";
                    Body += "</tr>";
                }
                Body += "<tr>";
                Body += "<td class='tdLabel'>Payable Amount</td>";
                Body += "<td style='text-align: right;'><span id='spnPayableAmount'>" + Ds.Tables[1].Rows[0]["PayableAmount"].ToString() + "</span></td>";
                Body += "</tr>";
                Body += "</tbody>";
                Body += "</table>";
                Body += "</td>";
                Body += "</tr>";
                Body += "</tbody>";
                Body += "</table>";
                Body += "</fieldset><br/>";
                //string DueAmount = Ds.Tables[3].Rows[0]["AmtDue"].ToString();
                //if (DueAmount != null && DueAmount != "0.00") { }
                //---------------------------------------------------------Open Balance Report-----------------------------------------------------

                //Repeat Table
                if (Ds.Tables[3].Rows.Count > 0)
                {
                    Body += "<P style='page-break-before: always'></p><fieldset>";
                    Body += "<table class='table tableCSS' id='tblProduct'>";
                    Body += "<thead>";
                    Body += "<tr style='text-align:center'>";
                    Body += "<td colspan='6' style='padding:8px 8px;'>";
                    Body += "<div><span style='font-size: 20px;text-align:right;font-weight:600;float:right'><i>Open Balance</i></span></div>";
                    Body += "</td>";
                    Body += "</tr>";
                    Body += "<tr style='text-align:center'>";
                    Body += "<td colspan='6' style='padding:8px 8px;'>";
                    Body += "<div><span style='font-size: 20px;text-align:center;font-weight:600;' id='PageHeading'>" + Ds.Tables[1].Rows[0]["CustomerName"].ToString() + "</span></div>";
                    Body += "</td>";
                    Body += "</tr>";
                    Body += "<tr>";
                    Body += "<td style='width: 10px;white-space:nowrap' class='text-center'>Order No.</td><td style='white-space: nowrap;width:12px;'>Order Date</td>" +
                    "<td style='white-space: nowrap;width:30px;'>Order Amount</td><td class='PaidAmount' style='text-align: center; width: 10px'>Paid amount</td><td class='OpenAmount' style='text-align: center; width: 10px'>Open Amount</td>" +
                    "<td class='Aging' style='text-align: center; width: 10px'>Aging (Days)</td>";
                    Body += "</tr>";
                    Body += "</thead>";
                    Body += "<tbody>";

                    if (Ds.Tables[3].Rows.Count > 0)
                    {

                        foreach (DataRow dr in Ds.Tables[3].Rows)
                        {
                            Body += "<tr>";
                            Body += "<td class='text-center'>" + dr["OrderNo"] + "</td><td class='text-center' style='width: 25px'>" + dr["OrderDate"] + "</td>" +
                            "<td style='white-space: nowrap;text-align: right;'>" + dr["AmtDue"] + "</td>" +
                            "<td style='white-space: nowrap;text-align: right;'>" + dr["GrandTotal"] + "</td>" +
                            "<td class='SRP' style='text-align: right; width: 28px'>" + dr["AmtPaid"] + "</td>" +
                            //  "<td class='GP' style='text-align: right; width: 28px'>" + dr["Aging"] + "</td>" +
                            //"<td class='UnitPrice' style='text-align: right; width: 28px'>" + dr["UnitPrice"] + "</td>" +
                            "<td class='totalPrice' style='text-align: center; width: 28px; white-space: nowrap'>" + dr["Aging"] + "</td>";
                            Body += "</tr>";
                        }
                    }

                    Body += "</tbody>";
                    Body += "</table></fieldset><br/><br/>";
                }
                //string Credit = Ds.Tables[5].Rows[0]["CreditNo"].ToString();
                //if (Credit != "" && Credit != null)
                if (Ds.Tables[5].Rows.Count > 0)
                {

                    //---------------------------------------------------------Credit Memo-----------------------------------------------------
                    Body += "<P style='page-break-before: always'></p><fieldset>";
                    Body += "<table class='table tableCSS' id='tblProductss'>";
                    Body += "<thead>";
                    Body += "<tr style='text-align:center'>";
                    Body += "<td colspan='6' style='padding:8px 8px;'>";
                    Body += "<div><span style='font-size: 20px;text-align:center;font-weight:600' id='PageHeadings'>" + Ds.Tables[1].Rows[0]["CustomerName"].ToString() + " - " + Ds.Tables[5].Rows[0]["CreditNo"].ToString() + "</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    Body += "<span style='font-size: 20px;text-align:right;font-weight:600;float:right'><i>Credit Memo</i></span></div>";

                    Body += "</td>";
                    Body += "</tr>";
                    Body += "<tr>";
                    Body += "<td rowspan='2' style='width: 21% !important;'>";
                    Body += "<img src='" + Ds.Tables[0].Rows[0]["LogoforEmail"].ToString() + "' id='logo' class='img-responsive' style='width: 100%;'/>";
                    Body += "</td>";
                    Body += "<td style='width: 26% !important;text-align:left;font-weight:100'><span id='CompanyName'>" + Ds.Tables[0].Rows[0]["CompanyName"].ToString() + "</span></td>";
                    Body += "<td class='tdLabel' style='white-space: nowrap;width:18% !important;text-align:left;'>Customer Id</td>";
                    Body += "<td id='acNo' style='white-space: nowrap;width:23% !important;text-align:left;font-weight:100'>" + Ds.Tables[1].Rows[0]["CustomerId"].ToString() + "</td>";
                    Body += "<td class='tdLabel' style='white-space: nowrap;width:13% !important;text-align:left;'>Sales Rep</td>";
                    Body += "<td id='salesRep' style='white-space: nowrap;width:19% !important;text-align:left;font-weight:100'>" + Ds.Tables[1].Rows[0]["SalesPerson"].ToString() + "</td>";
                    Body += "</tr>";
                    Body += "<tr>";
                    Body += "<td style='text-align:left;font-weight:100'><span id='Address'>" + Ds.Tables[0].Rows[0]["Address"].ToString() + "</span></td>";
                    Body += "<td class='tdLabel' style='white-space: nowrap;text-align:left;'>Customer Contact</td>";
                    Body += "<td id='storeName' style='white-space: nowrap;text-align:left;font-weight:100'>" + Ds.Tables[1].Rows[0]["ContactPersonName"].ToString() + "</td>";
                    Body += "<td class='tdLabel' style='white-space: nowrap;text-align:left;'>Credit No</td>";
                    Body += "<td id='so' style='text-align:left;font-weight:100'>" + Ds.Tables[5].Rows[0]["CreditNo"].ToString() + "</td>";
                    Body += "</tr>";
                    Body += "<tr>";
                    Body += "<td style='text-align:left;font-weight:100'><span id='Website' style='white-space: nowrap;'>" + Ds.Tables[0].Rows[0]["Website"].ToString() + "</span></td>";
                    Body += "<td style='white-space: nowrap;text-align:left;font-weight:100'>Phone : <span id='Phone'> Phone : " + Ds.Tables[0].Rows[0]["MobileNo"].ToString() + "</span></td>";
                    Body += "<td class='tdLabel' style='text-align:left';>Contact No</td>";
                    Body += "<td id='ContPerson' style='text-align:left;font-weight:100'>" + Ds.Tables[1].Rows[0]["Contact"].ToString() + "</td>";
                    Body += "<td class='tdLabel' style='white-space: nowrap;text-align:left;'>Credit Date</td>";
                    Body += "<td id='soDate' style='white-space: nowrap;text-align:left;font-weight:100'>" + Ds.Tables[5].Rows[0]["CreditDate"].ToString() + "</td>";
                    Body += "</tr>";
                    Body += "<tr>";
                    Body += "<td style='text-align:left;font-weight:100'><span id='EmailAddress'>" + Ds.Tables[0].Rows[0]["EmailAddress"].ToString() + "</span></td>";
                    Body += "<td style='text-align:left;font-weight:100'>Fax:<span id='FaxNo'>" + Ds.Tables[0].Rows[0]["FaxNo"].ToString() + "</span></td>";
                    Body += "<td style='text-align:left;' class='tdLabel'>Terms</td>";
                    Body += "<td style='font-size: 12px;text-align:left;font-weight:100'><b><span id='terms'>" + Ds.Tables[1].Rows[0]["TermsDesc"].ToString() + "</span></b></td>";
                    Body += "<td class='tdLabel' style='text-align:left;'>Ship Via</td>";
                    Body += "<td id='shipVia' style='text-align:left;font-weight:100'>" + Ds.Tables[1].Rows[0]["ShippingType"].ToString() + "</td>";
                    Body += "</tr>";

                    if (Bn != "" && Bn != null)
                    {
                        Body += "<tr>";
                        Body += "<td><span class='tdLabel'>Business Name:</span></td>";
                        Body += "<td colspan='5'><span id='BusinessName'>" + Bn + "</span></td>";
                        Body += "</tr>";
                    }
                    Body += "<tr>";
                    Body += "<td style='text-align:left;'><span class='tdLabel'>Shipping Address:</span></td>";
                    Body += "<td colspan='5' style='text-align:left;font-weight:100'><span id='shipAddr' style='font-size: 15px'>" + Ds.Tables[1].Rows[0]["ShipAddr"].ToString() + "</span></td>";
                    Body += "</tr>";
                    Body += "<tr>";
                    Body += "<td style='text-align:left;'><span class='tdLabel'>Billing Address:</span></td>";
                    Body += "<td colspan='5' style='text-align:left;font-weight:100'><span id='billAddr' style='font-size: 15px'>" + Ds.Tables[1].Rows[0]["BillAddr"].ToString() + "</span></td>";
                    Body += "</tr>";
                    Body += "</thead>";
                    Body += "</table></fieldset><br/><fieldset>";

                    Body += "<table class='table tableCSS' id='tblProductw'>";
                    Body += "<thead>";
                    Body += "<tr>";
                    Body += "<td class='text-center' style='width:8px;'>Qty</td><td style='width: 8px;white-space:nowrap' class='text-center'>Unit Type</td><td style='white-space: nowrap;width:10px;'>Product ID</td>" +
                    "<td style='white-space: nowrap;width:40px;'>Item</td><td class='SRP' style='text-align: center; width: 8px'>UPC</td>" +
                    "<td class='UnitPrice' style='text-align: center; width: 8px'>Price</td><td class='totalPrice' style='text-align: center; width: 8px; white-space: nowrap'>Total Price</td>";
                    Body += "</tr>";
                    Body += "</thead>";
                    Body += "<tbody>";
                    if (Ds.Tables[7].Rows.Count > 0)
                    {
                        foreach (DataRow dr in Ds.Tables[7].Rows)
                        {
                            Body += "<tr>";
                            Body += "<td class='text-center'>" + dr["QtyShip"] + "</td><td style='width: 25px'>" + dr["UnitType"] + "</td>" +
                            "<td style='white-space: nowrap' class='text-center'>" + dr["ProductId"] + "</td>" +
                            "<td style='white-space: nowrap'>" + dr["ProductName"] + "</td>" +
                            "<td class='SRP' style='text-align: center; width: 28px'>" + dr["Barcode"] + "</td>" +
                            "<td class='UnitPrice' style='text-align: right; width: 28px'>" + dr["UnitPrice"] + "</td>" +
                            "<td class='totalPrice' style='text-align: right; width: 28px; white-space: nowrap'>" + dr["NetPrice"] + "</td>";
                            Body += "</tr>";
                        }
                    }
                    Body += "</tbody>";
                    Body += "</table></fieldset><br/>";


                    Body += "<fieldset>";
                    Body += "<table style='width: 100%;'>";
                    Body += "<tbody>";
                    Body += "<tr>";
                    Body += "<td style='width: 67%' colspan='2'>" + "<center></center>" + "</td>";
                    Body += "<td style='width: 33%;'>";
                    Body += "<table class='table tableCSS' style='width:100%;'>";
                    Body += "<tbody>";
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Total Qty</td>";
                    Body += "<td id='totalQty' style='text-align: right;'>" + Ds.Tables[2].Rows.Count.ToString() + "</td>";
                    Body += "</tr>";
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Sub Total</td>";
                    Body += "<td style='text-align: right;'><span id='totalwoTax'>" + Ds.Tables[6].Rows[0]["TotalAmount"].ToString() + "</span></td>";
                    Body += "</tr>";
                    string diss = Ds.Tables[6].Rows[0]["OverallDisc"].ToString();
                    if (diss != "0.00" && diss != null)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Discount</td>";
                        Body += "<td style='text-align: right;'><span id='disc'>" + dis + "</span></td>"; ;
                        Body += "</tr>";
                    }
                    string taxs = Ds.Tables[6].Rows[0]["TotalTax"].ToString();
                    if (taxs != "0.00" && taxs != null)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Tax</td>";
                        Body += "<td style='text-align: right;'><span id='tax'>" + tax + "</span></td>";
                        Body += "</tr>";
                    }
                    string MLQnty = Ds.Tables[6].Rows[0]["MLQty"].ToString();
                    if (MLQnty != null && MLQnty != "0.00")
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>ML Qty</td>";
                        Body += "<td style='text-align: right;'><span id='MLTax'>" + MLQnty + "</span></td>";
                        Body += "</tr>";
                    }
                    string NTaxs = Ds.Tables[6].Rows[0]["MLTax"].ToString();
                    if (NTaxs != null && NTaxs != "0.00")
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>" + jdv["PrintLabel"] + "</td>";
                        Body += "<td style='text-align: right;'><span id='MLTax'>" + NTax + "</span></td>";
                        Body += "</tr>";
                    }
                    string WTaxs = Ds.Tables[6].Rows[0]["WeightTaxAmount"].ToString();
                    if (WTaxs != "" && WTaxs != null)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Weight Tax</td>";
                        Body += "<td style='text-align: right;'><span id='WeightTax'>" + WTax + "</span></td>";
                        Body += "</tr>";
                    }
                    string AAmts = Ds.Tables[6].Rows[0]["AdjustmentAmt"].ToString();
                    if (AAmts != null && AAmts != "0.00")
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Adjustment</td>";
                        Body += "<td style='text-align: right;'><span id='spAdjustment'>" + AAmt + "</span></td>";
                        Body += "</tr>";
                    }
                    Body += "<tr>";
                    Body += "<td class='tdLabel'>Grand Total</td>";
                    Body += "<td style='text-align: right;'><span id='grandTotal'>" + Ds.Tables[1].Rows[0]["GrandTotal"].ToString() + "</span></td>";
                    Body += "</tr>";


                    //Body += "<tr>";
                    //Body += "<td class='tdLabel'>Payable Amount</td>";
                    //Body += "<td style='text-align: right;'><span id='spnPayableAmount'>" + Ds.Tables[6].Rows[0]["TotalAmount"].ToString() + "</span></td>";
                    //Body += "</tr>";
                    Body += "</tbody>";
                    Body += "</table>";
                    Body += "</td>";
                    Body += "</tr>";
                    Body += "</tbody>";
                    Body += "</table>";
                    Body += "</fieldset><br/>";
                }
                //-------------------------------------------------------------------------------------------------------------------------
                Body += "</div>";
                Body += "</body>";
                Body += "</html>";


                string ReceiverEmailId = jdv["To"];
                if (ReceiverEmailId != null && ReceiverEmailId != string.Empty && ReceiverEmailId.Length > 0)
                {
                    string emailmsg = MailSending.SendMailResponsewithAttachment(ReceiverEmailId, "", "", jdv["Subject"], jdv["EmailBody"], "Developer", Body.ToString(), CSSStyle);

                    if (emailmsg == "Successful")
                    {
                        return "true";
                    }
                    else
                    {
                        return "EmailNotSend";
                    }
                }
                else
                {
                    return "receiverMail";
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
            //return Body;
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getMailBoddy(string dataValue)
    {

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);

        if (Session["EmpAutoId"] != null)
        {
            string CSSStyle = "table{border-collapse:collapse;}td {padding: 5px 7px;}.text-center{text-align:center}";
            CSSStyle += "table {width: 100%;}body {font-size: 13px;font-family: monospace;}";
            CSSStyle += ".center{text-align: center;}.right {text-align: right;}thead > tr {background: #fff;color: #000;font-weight: 600;border-top: 1px solid #5f7c8a;}";
            CSSStyle += "thead > tr > td {text-align: center !important;}.InvContent {width: 100%;}.tdLabel {font-weight: 600;}";
            CSSStyle += "@media print {#btnPrint {display: none;}}legend {border-bottom: 1px solid #5f7c8a;}";
            CSSStyle += ".tableCSS > tbody > tr > td,.tableCSS > thead > tr > td,.tableCSS > tfoot > tr > td {border: 1px solid #5f7c8a;}pre{border:none !important;}";
            CSSStyle += "";
            string Body = "";
            Body = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'>";
            Body += "<head> <title></title> <style  type='text/css'></style>";
            Body += "</head>";
            Body += "<body style='font-family: Arial; font-size: 13px;width:1000px'>";
            Body += "<div class='InvContent'>";
            Body += "<fieldset>";
            Body += "<table class='table tableCSS'>";
            Body += "<tbody>";
            Body += "<tr style='text-align:center'>";
            Body += "<td colspan='6' style='padding:8px 8px;'>";
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPrintOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", 522);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", Convert.ToInt32(jdv["OrderId"]));
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                DataSet Ds = new DataSet();
                sqlAdp.Fill(Ds);
                string Orders = "";
                string CompanyDetails = "";
                foreach (DataRow dr in Ds.Tables[0].Rows)
                {
                    Orders += dr[0];
                }
                CompanyDetails = Orders;

                JArray CompanyList = JArray.Parse(CompanyDetails);
                JObject CompanyDetail = (JObject)CompanyList[0];
                JArray OrderList = (JArray)CompanyDetail["OrderDetails"];
                JObject OrderDetail = (JObject)OrderList[0];

                Body += "<div><span style='font-size: 20px;text-align:center;font-weight:600' id='PageHeading'>" + Ds.Tables[1].Rows[0]["CustomerName"].ToString() + " - " + Ds.Tables[1].Rows[0]["OrderNo"].ToString() + "</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                Body += "<span style='font-size: 20px;text-align:right;font-weight:600;'><i>Sales Order</i></span></div>";
                Body += "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td rowspan='2' style='width: 21% !important;'>";
                Body += "<img src='" + CompanyDetail["LogoforEmail"].ToString() + "' id='logo' class='img-responsive' style='width: 100%;'/>";
                Body += "</td>";
                Body += "<td style='width: 26% !important;'><span id='CompanyName'>" + CompanyDetail["CompanyName"].ToString() + "</span></td>";
                Body += "<td class='tdLabel' style='white-space: nowrap;width:18% !important'>Customer ID</td>";
                Body += "<td id='acNo' style='white-space: nowrap;width:23% !important'>" + OrderDetail["CustomerId"].ToString() + "</td>";
                Body += "<td class='tdLabel' style='white-space: nowrap;width:13% !important'>Sales Rep</td>";
                Body += "<td id='salesRep' style='white-space: nowrap;width:19% !important'>" + OrderDetail["SalesPerson"].ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span id='Address'>" + CompanyDetail["Address"].ToString() + "</span></td>";
                Body += "<td class='tdLabel' style='white-space: nowrap'>Customer Contact</td>";
                Body += "<td id='storeName' style='white-space: nowrap'>" + OrderDetail["ContactPersonName"].ToString() + "</td>";
                Body += "<td class='tdLabel' style='white-space: nowrap'>Order No</td>";
                Body += "<td id='so'>" + OrderDetail["OrderNo"].ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span id='Website' style='white-space: nowrap'>" + CompanyDetail["Website"].ToString() + "</span></td>";
                Body += "<td style='white-space: nowrap'><span id='Phone'> Phone : " + CompanyDetail["MobileNo"].ToString() + "</span></td>";
                Body += "<td class='tdLabel'>Contact No</td>";
                Body += "<td id='ContPerson'>" + OrderDetail["Contact"].ToString() + "</td>";
                Body += "<td class='tdLabel' style='white-space: nowrap'>Order Date</td>";
                Body += "<td id='soDate' style='white-space: nowrap'>" + OrderDetail["OrderDate"].ToString() + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span id='EmailAddress'>" + CompanyDetail["EmailAddress"].ToString() + "</span></td>";
                Body += "<td>Fax:<span id='FaxNo'>" + CompanyDetail["FaxNo"].ToString() + "</span></td>";
                Body += "<td class='tdLabel'>Terms</td>";
                Body += "<td style='font-size: 12px'><b><span id='terms'>" + OrderDetail["TermsDesc"].ToString() + "</span></b></td>";
                Body += "<td class='tdLabel'>Ship Via</td>";
                Body += "<td id='shipVia'>" + OrderDetail["ShippingType"].ToString() + "</td>";
                Body += "</tr>";

                string Bn = OrderDetail["BusinessName"].ToString();
                if (Bn != "" && Bn != null)
                {
                    Body += "<tr>";
                    Body += "<td><span class='tdLabel'>Business Name:</span></td>";
                    Body += "<td colspan='5'><span id='BusinessName'>" + Bn + "</span></td>";
                    Body += "</tr>";
                }
                Body += "<tr>";
                Body += "<td><span class='tdLabel'>Shipping Address</span></td>";
                Body += "<td colspan='5'><span id='shipAddr' style='font-size: 15px'>" + OrderDetail["ShipAddr"].ToString() + " " + OrderDetail["City2"].ToString() + " " + OrderDetail["State2"].ToString() + " " + OrderDetail["Zipcode2"].ToString() + "</span></td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td><span class='tdLabel'>Billing Address</span></td>";
                Body += "<td colspan='5'><span id='billAddr' style='font-size: 15px'>" + OrderDetail["BillAddr"].ToString() + " " + OrderDetail["City1"].ToString() + " " + OrderDetail["State1"].ToString() + " " + OrderDetail["Zipcode1"].ToString() + "</span></td>";
                Body += "</tr>";
                Body += "</tbody>";
                Body += "</table>";
                Body += "</fieldset><br/><fieldset>";
                Body += "<table class='table tableCSS' id='tblProduct'>";
                Body += "<thead>";
                Body += "<tr>";
                Body += "<td class='text-center' style='width:4px;'>Qty</td><td style='width: 6px;white-space:nowrap' class='text-center'>Unit Type</td><td style='white-space: nowrap;width:8px;'>Product ID</td>" +
                "<td style='white-space: nowrap;width:30px;'>Product Description</td><td style='text-align: center;width:12px'>UPC</td><td class='SRP' style='text-align: center; width: 6px'>SRP</td><td class='GP' style='text-align: center; width: 6px'>GP(%)</td>" +
                "<td class='UnitPrice' style='text-align: center; width: 6px'>Price</td><td class='totalPrice' style='text-align: center; width: 10px; white-space: nowrap'>Total Price</td>";
                Body += "</tr>";
                Body += "</thead>";
                Body += "<tbody>";
                JArray ItemList = (JArray)OrderDetail["Item2"];
                if (((JArray)OrderDetail["Item1"]).Count > 0)
                {

                    ItemList = (JArray)OrderDetail["Item1"];
                }
                //JObject Items = (JObject)ItemList[0];
                int CountOrderItem = 0;
                for (var i = 0; i < ItemList.Count; i++)
                {

                    JObject Items = (JObject)ItemList[i];

                    if (Items.Count > 0)
                    {

                        CountOrderItem += Convert.ToInt32(Items["QtyShip"].ToString());
                        Body += "<tr>";
                        Body += "<td class='text-center'>" + Items["QtyShip"].ToString() + "</td><td style='width: 25px;text-align: center;'>" + Items["UnitType"].ToString() + "</td>" +
                        "<td style='white-space: nowrap;text-align: left;'>" + Items["ProductId"].ToString() + "</td>";
                        if (Items["IsExchange"].ToString() == "1")
                        {
                            Body += "<td style='white-space: nowrap'>" + Items["ProductName"] + " <i style='font-size:10px'>Exchange</i>" + "</td>";
                        }
                        else if (Items["isFreeItem"].ToString() == "1")
                        {
                            Body += "<td style='white-space: nowrap'>" + Items["ProductName"] + " <i style='font-size:10px'>Free</i>" + "</td>";
                        }
                        else
                        {
                            Body += "<td style='white-space: nowrap'>" + Items["ProductName"] + "</td>";
                        }
                        Body += "<td style='text-align: center; width: 28px'>" + Items["Barcode"] + "</td>" +
                        "<td class='SRP' style='text-align: right; width: 28px'>" + Items["SRP"].ToString() + "</td>" +
                        "<td class='GP' style='text-align: right; width: 28px'>" + Items["GP"] + "</td>" +
                        "<td class='UnitPrice' style='text-align: right; width: 28px'>" + Items["UnitPrice"].ToString() + "</td>" +
                        "<td class='totalPrice' style='text-align: right; width: 28px; white-space: nowrap'>" + Items["NetPrice"].ToString() + "</td>";
                        Body += "</tr>";
                    }
                }

                Body += "</tbody>";
                Body += "</table></fieldset><br/>";
                Body += "<fieldset>";
                Body += "<table style='width: 100%;'>";
                Body += "<tbody>";
                Body += "<tr>";
                Body += "<td style='width: 67%' colspan='2'>" + "<center><div id='TC'>" + CompanyDetail["tc"].ToString() + "</div></center>" + "</td>";
                Body += "<td style='width: 33%;'>";
                Body += "<table class='table tableCSS' style='width:100%;'>";
                Body += "<tbody>";
                Body += "<tr>";
                Body += "<td class='tdLabel'>Total Items</td>";
                Body += "<td id='totalQty' style='text-align: right;'>" + CountOrderItem + "</td>";
                Body += "</tr>";
                Body += "<tr>";
                Body += "<td class='tdLabel'>Sub Total</td>";
                Body += "<td style='text-align: right;'><span id='totalwoTax'>" + OrderDetail["TotalAmount"].ToString() + "</span></td>";
                Body += "</tr>";
                string dis = OrderDetail["OverallDiscAmt"].ToString();
                if (dis != "0.00" && dis != null)
                {
                    if (Convert.ToDecimal(dis) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Discount</td>";
                        Body += "<td style='text-align: right;'><span id='disc'>-" + dis + "</span></td>"; ;
                        Body += "</tr>";
                    }
                }
                string tax = OrderDetail["TotalTax"].ToString();
                if (tax != "0.00" && tax != null)
                {
                    if (Convert.ToDecimal(tax) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Tax</td>";
                        Body += "<td style='text-align: right;'><span id='tax'>" + tax + "</span></td>";
                        Body += "</tr>";
                    }
                }
                string SC = OrderDetail["ShippingCharges"].ToString();
                if (SC != "0.00" && SC != null)
                {
                    if (Convert.ToDecimal(SC) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Shipping</td>";
                        Body += "<td style='text-align: right;'><span id='shipCharges'>" + SC + "</span></td>";
                        Body += "</tr>";
                    }
                }
                string NTax = OrderDetail["MLTax"].ToString();
                if (NTax != null && NTax != "0.00")
                {
                    if (Convert.ToDecimal(NTax) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>" + OrderDetail["MLTaxPrintLabel"].ToString() + "</td>";
                        Body += "<td style='text-align: right;'><span id='MLTax'>" + NTax + "</span></td>";
                        Body += "</tr>";
                    }
                }
                string WTax = OrderDetail["WeightTax"].ToString();
                if (WTax != "" && WTax != null && WTax != "0.00")
                {
                    if (Convert.ToDecimal(WTax) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Weight Tax</td>";
                        Body += "<td style='text-align: right;'><span id='WeightTax'>" + WTax + "</span></td>";
                        Body += "</tr>";
                    }
                }
                string AAmt = OrderDetail["AdjustmentAmt"].ToString();
                if (AAmt != null && AAmt != "0.00")
                {
                    if (Convert.ToDecimal(AAmt) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Adjustment</td>";
                        Body += "<td style='text-align: right;'><span id='spAdjustment'>" + AAmt + "</span></td>";
                        Body += "</tr>";
                    }
                }
                Body += "<tr>";
                Body += "<td class='tdLabel'>Grand Total</td>";
                Body += "<td style='text-align: right;'><span id='grandTotal'>" + OrderDetail["GrandTotal"].ToString() + "</span></td>";
                Body += "</tr>";

                string DAmmt = OrderDetail["DeductionAmount"].ToString();
                if (DAmmt != "0.00" && DAmmt != null)
                {
                    if (Convert.ToDecimal(DAmmt) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Credit Memo</td>";
                        Body += "<td style='text-align: right;'><span id='spnCreditmemo'>-" + DAmmt + "</span></td>";
                        Body += "</tr>";
                    }
                }
                string CAmt = OrderDetail["CreditAmount"].ToString();
                if (CAmt != null && CAmt != "0.00")
                {
                    if (Convert.ToDecimal(CAmt) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Store Credit</td>";
                        Body += "<td style='text-align: right;'><span id='spnStoreCredit'>-" + CAmt + "</span></td>";
                        Body += "</tr>";
                    }
                }
                try
                {


                    if (Convert.ToDecimal(CAmt) > 0 || Convert.ToDecimal(DAmmt) > 0)
                    {
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Payable Amount</td>";
                        Body += "<td style='text-align: right;'><span id='spnPayableAmount'>" + OrderDetail["PayableAmount"].ToString() + "</span></td>";
                        Body += "</tr>";
                    }

                }
                catch (Exception)
                {

                }

                Body += "</tbody>";
                Body += "</table>";
                Body += "</td>";
                Body += "</tr>";
                Body += "</tbody>";
                Body += "</table>";
                Body += "</fieldset><br/>";
                //---------------------------------------------------------Open Balance Report-----------------------------------------------------
                JArray DueOrder = (JArray)OrderDetail["DueOrderList"];
                if (DueOrder.Count > 0)
                {
                    Body += "<P style='page-break-before: always'></p><fieldset>";
                    Body += "<table class='table tableCSS' id='tblProduct'>";
                    Body += "<thead>";
                    Body += "<tr style='text-align:center'>";
                    Body += "<td colspan='6' style='padding:8px 8px;'>";
                    Body += "<div><span style='font-size: 20px;text-align:right;font-weight:600;float:right'><i>Open Balance</i></span></div>";
                    Body += "</td>";
                    Body += "</tr>";
                    Body += "<tr style='text-align:center'>";
                    Body += "<td colspan='6' style='padding:8px 8px;'>";
                    Body += "<div><span style='font-size: 20px;text-align:center;font-weight:600;' id='PageHeading'>" + OrderDetail["CustomerName"].ToString() + "</span></div>";
                    Body += "</td>";
                    Body += "</tr>";
                    Body += "<tr>";
                    Body += "<td style='width: 10px;white-space:nowrap' class='text-center'>Order No.</td><td style='white-space: nowrap;width:12px;'>Order Date</td>" +
                    "<td style='white-space: nowrap;width:30px;'>Order Amount</td><td class='PaidAmount' style='text-align: center; width: 10px'>Paid Amount</td><td class='OpenAmount' style='text-align: center; width: 10px'>Open Amount</td>" +
                    "<td class='Aging' style='text-align: center; width: 10px'>Aging (Days)</td>";
                    Body += "</tr>";
                    Body += "</thead>";
                    Body += "<tbody>";
                    for (var j = 0; j < DueOrder.Count; j++)
                    {
                        JObject DueOrderList = (JObject)DueOrder[j];
                        if (DueOrderList.Count > 0)
                        {
                            Body += "<tr>";
                            Body += "<td class='text-center'>" + DueOrderList["OrderNo"].ToString() + "</td><td class='text-center' style='width: 25px'>" + DueOrderList["OrderDate"].ToString() + "</td>" +
                            "<td style='white-space: nowrap;text-align: right;'>" + DueOrderList["GrandTotal"].ToString() + "</td>" +
                            "<td style='white-space: nowrap;text-align: right;'>" + DueOrderList["AmtPaid"].ToString() + "</td>" +
                            "<td class='SRP' style='text-align: right; width: 28px'>" + DueOrderList["AmtDue"].ToString() + "</td>" +
                            "<td class='totalPrice' style='text-align: center; width: 28px; white-space: nowrap'>" + DueOrderList["Aging"].ToString() + "</td>";
                            Body += "</tr>";
                        }
                    }


                    Body += "</tbody>";
                    Body += "</table></fieldset><br/><br/>";
                }
                JArray CreditDetails = (JArray)OrderDetail["CreditDetails"];
                if (CreditDetails.Count > 0)
                {
                    for (var k = 0; k < CreditDetails.Count; k++)
                    {
                        JObject Credit = (JObject)CreditDetails[k];

                        //---------------------------------------------------------Credit Memo-----------------------------------------------------
                        Body += "<P style='page-break-before: always'></p><fieldset>";
                        Body += "<table class='table tableCSS' id='tblProductss'>";
                        Body += "<thead>";
                        Body += "<tr style='text-align:center'>";
                        Body += "<td colspan='6' style='padding:8px 8px;'>";
                        Body += "<div><span style='font-size: 20px;text-align:center;font-weight:600' id='PageHeadings'>" + Credit["CustomerName"].ToString() + " - " + Credit["CreditNo"].ToString() + "</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        Body += "<span style='font-size: 20px;text-align:right;font-weight:600;float:right'><i>Credit Memo</i></span></div>";

                        Body += "</td>";
                        Body += "</tr>";
                        Body += "<tr>";
                        Body += "<td rowspan='2' style='width: 21% !important;'>";
                        Body += "<img src='" + CompanyDetail["LogoforEmail"].ToString() + "' id='logo' class='img-responsive' style='width: 100%;'/>";
                        Body += "</td>";
                        Body += "<td style='width: 26% !important;text-align:left;font-weight:100'><span id='CompanyName'>" + CompanyDetail["CompanyName"].ToString() + "</span></td>";
                        Body += "<td class='tdLabel' style='white-space: nowrap;width:18% !important;text-align:left;'>Customer Id</td>";
                        Body += "<td id='acNo' style='white-space: nowrap;width:23% !important;text-align:left;font-weight:100'>" + Credit["CustomerId"].ToString() + "</td>";
                        Body += "<td class='tdLabel' style='white-space: nowrap;width:13% !important;text-align:left;'>Sales Rep</td>";
                        Body += "<td id='salesRep' style='white-space: nowrap;width:19% !important;text-align:left;font-weight:100'>" + Credit["SalesPerson"].ToString() + "</td>";
                        Body += "</tr>";
                        Body += "<tr>";
                        Body += "<td style='text-align:left;font-weight:100'><span id='Address'>" + CompanyDetail["Address"].ToString() + "</span></td>";
                        Body += "<td class='tdLabel' style='white-space: nowrap;text-align:left;'>Customer Contact</td>";
                        Body += "<td id='storeName' style='white-space: nowrap;text-align:left;font-weight:100'>" + Credit["ContactPersonName"].ToString() + "</td>";
                        Body += "<td class='tdLabel' style='white-space: nowrap;text-align:left;'>Credit No</td>";
                        Body += "<td id='so' style='text-align:left;font-weight:100'>" + Credit["CreditNo"].ToString() + "</td>";
                        Body += "</tr>";
                        Body += "<tr>";
                        Body += "<td style='text-align:left;font-weight:100'><span id='Website' style='white-space: nowrap;'>" + CompanyDetail["Website"].ToString() + "</span></td>";
                        Body += "<td style='white-space: nowrap;text-align:left;font-weight:100'>Phone : <span id='Phone'> Phone : " + CompanyDetail["MobileNo"].ToString() + "</span></td>";
                        Body += "<td class='tdLabel' style='text-align:left';>Contact No</td>";
                        Body += "<td id='ContPerson' style='text-align:left;font-weight:100'>" + Credit["Contact"].ToString() + "</td>";
                        Body += "<td class='tdLabel' style='white-space: nowrap;text-align:left;'>Credit Date</td>";
                        Body += "<td id='soDate' style='white-space: nowrap;text-align:left;font-weight:100'>" + Credit["CreditDate"].ToString() + "</td>";
                        Body += "</tr>";
                        Body += "<tr>";
                        Body += "<td style='text-align:left;font-weight:100'><span id='EmailAddress'>" + CompanyDetail["EmailAddress"].ToString() + "</span></td>";
                        Body += "<td style='text-align:left;font-weight:100'>Fax:<span id='FaxNo'>" + CompanyDetail["FaxNo"].ToString() + "</span></td>";
                        Body += "<td style='text-align:left;' class='tdLabel'>Terms</td>";
                        Body += "<td style='font-size: 12px;text-align:left;font-weight:100'><b><span id='terms'>" + Credit["TermsDesc"].ToString() + "</span></b></td>";
                        Body += "<td class='tdLabel' style='text-align:left;' colspan='2'></td>";
                        Body += "</tr>";

                        if (Bn != "" && Bn != null)
                        {
                            Body += "<tr>";
                            Body += "<td><span class='tdLabel'>Business Name:</span></td>";
                            Body += "<td colspan='5'><span id='BusinessName'>" + Bn + "</span></td>";
                            Body += "</tr>";
                        }
                        Body += "<tr>";
                        Body += "<td style='text-align:left;'><span class='tdLabel'>Shipping Address:</span></td>";
                        Body += "<td colspan='5' style='text-align:left;font-weight:100'><span id='shipAddr' style='font-size: 15px'>" + Credit["ShipAddr"].ToString() + "</span></td>";
                        Body += "</tr>";
                        Body += "<tr>";
                        Body += "<td style='text-align:left;'><span class='tdLabel'>Billing Address:</span></td>";
                        Body += "<td colspan='5' style='text-align:left;font-weight:100'><span id='billAddr' style='font-size: 15px'>" + Credit["BillAddr"].ToString() + "</span></td>";
                        Body += "</tr>";
                        Body += "</thead>";
                        Body += "</table></fieldset><br/><fieldset>";

                        Body += "<table class='table tableCSS' id='tblProductw'>";
                        Body += "<thead>";
                        Body += "<tr>";
                        Body += "<td class='text-center' style='width:8px;'>Qty</td><td style='width: 8px;white-space:nowrap' class='text-center'>Unit Type</td><td style='white-space: nowrap;width:10px;'>Product ID</td>" +
                        "<td style='white-space: nowrap;width:40px;'>Item</td>" +
                        "<td class='UnitPrice' style='text-align: center; width: 8px'>Price</td><td class='totalPrice' style='text-align: center; width: 8px; white-space: nowrap'>Total Price</td>";
                        Body += "</tr>";
                        Body += "</thead>";
                        Body += "<tbody>";
                        JArray CreditItemList = (JArray)Credit["CreditItems"];
                        //JObject Items = (JObject)ItemList[0];
                        int CountCreditItem = 0;
                        for (var l = 0; l < CreditItemList.Count; l++)
                        {
                            JObject CreditItems = (JObject)CreditItemList[l];


                            if (CreditItems.Count > 0)
                            {
                                CountCreditItem += Convert.ToInt32(CreditItems["QtyShip"].ToString());
                                Body += "<tr>";

                                Body += "<td class='text-center'>" + CreditItems["QtyShip"].ToString() + "</td><td style='width: 25px'>" + CreditItems["UnitType"].ToString() + "</td>" +
                                "<td style='white-space: nowrap' class='text-center'>" + CreditItems["ProductId"].ToString() + "</td>" +
                                "<td style='white-space: nowrap'>" + CreditItems["ProductName"].ToString() + "</td>" +
                                "<td class='UnitPrice' style='text-align: right; width: 28px'>" + CreditItems["UnitPrice"].ToString() + "</td>" +
                                "<td class='totalPrice' style='text-align: right; width: 28px; white-space: nowrap'>" + CreditItems["NetPrice"].ToString() + "</td>";
                                Body += "</tr>";
                            }
                        }
                        Body += "</tbody>";
                        Body += "</table></fieldset><br/>";


                        Body += "<fieldset>";
                        Body += "<table style='width: 100%;'>";
                        Body += "<tbody>";
                        Body += "<tr>";
                        Body += "<td style='width: 67%' colspan='2'>" + "<center></center>" + "</td>";
                        Body += "<td style='width: 33%;'>";
                        Body += "<table class='table tableCSS' style='width:100%;'>";
                        Body += "<tbody>";
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Total Qty</td>";
                        Body += "<td id='totalQty' style='text-align: right;'>" + CountCreditItem.ToString() + "</td>";
                        Body += "</tr>";
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Sub Total</td>";
                        Body += "<td style='text-align: right;'><span id='totalwoTax'>" + Credit["TotalAmount"].ToString() + "</span></td>";
                        Body += "</tr>";
                        string diss = Credit["OverallDisc"].ToString();
                        if (diss != "0.00" && diss != null)
                        {
                            if (Convert.ToDecimal(diss) > 0)
                            {
                                Body += "<tr>";
                                Body += "<td class='tdLabel'>Discount</td>";
                                Body += "<td style='text-align: right;'><span id='disc'>" + dis + "</span></td>"; ;
                                Body += "</tr>";
                            }
                        }
                        string taxs = Credit["TotalTax"].ToString();
                        if (taxs != "0.00" && taxs != null)
                        {
                            if (Convert.ToDecimal(taxs) > 0)
                            {
                                Body += "<tr>";
                                Body += "<td class='tdLabel'>Tax</td>";
                                Body += "<td style='text-align: right;'><span id='tax'>" + tax + "</span></td>";
                                Body += "</tr>";
                            }
                        }

                        string NTaxs = Credit["MLTax"].ToString();

                        if (NTaxs != null)
                        {
                            if (Convert.ToDecimal(NTaxs) > 0)
                            {
                                Body += "<tr>";
                                Body += "<td class='tdLabel'>" + OrderDetail["MLTaxPrintLabel"].ToString() + "</td>";
                                Body += "<td style='text-align: right;'><span id='MLTax'>" + NTax + "</span></td>";
                                Body += "</tr>";
                            }
                        }
                        string WTaxs = Credit["WeightTaxAmount"].ToString();
                        if (WTaxs != null)
                        {
                            if (Convert.ToDecimal(WTax) > 0)
                            {
                                Body += "<tr>";
                                Body += "<td class='tdLabel'>Weight Tax</td>";
                                Body += "<td style='text-align: right;'><span id='WeightTax'>" + WTax + "</span></td>";
                                Body += "</tr>";
                            }
                        }
                        string AAmts = Credit["AdjustmentAmt"].ToString();
                        if (AAmts != null && AAmts != "0.00")
                        {
                            if (Convert.ToDecimal(AAmt) > 0)
                            {
                                Body += "<tr>";
                                Body += "<td class='tdLabel'>Adjustment</td>";
                                Body += "<td style='text-align: right;'><span id='spAdjustment'>" + AAmt + "</span></td>";
                                Body += "</tr>";
                            }
                        }
                        Body += "<tr>";
                        Body += "<td class='tdLabel'>Grand Total</td>";
                        Body += "<td style='text-align: right;'><span id='grandTotal'>" + Credit["GrandTotal"].ToString() + "</span></td>";
                        Body += "</tr>";


                        //Body += "<tr>";
                        //Body += "<td class='tdLabel'>Payable Amount</td>";
                        //Body += "<td style='text-align: right;'><span id='spnPayableAmount'>" + Ds.Tables[6].Rows[0]["TotalAmount"].ToString() + "</span></td>";
                        //Body += "</tr>";
                        Body += "</tbody>";
                        Body += "</table>";
                        Body += "</td>";
                        Body += "</tr>";
                        Body += "</tbody>";
                        Body += "</table>";
                        Body += "</fieldset><br/>";
                    }
                }
                //-------------------------------------------------------------------------------------------------------------------------
                Body += "</div>";
                Body += "</body>";
                Body += "</html>";


                string ReceiverEmailId = jdv["To"];
                if (ReceiverEmailId != null && ReceiverEmailId != string.Empty && ReceiverEmailId.Length > 0)
                {
                    string emailmsg = MailSending.SendMailResponsewithAttachment(ReceiverEmailId, "", "", jdv["Subject"], jdv["EmailBody"], "Developer", Body.ToString(), CSSStyle);

                    if (emailmsg == "Successful")
                    {
                        return "true";
                    }
                    else
                    {
                        return "EmailNotSend";
                    }
                }
                else
                {
                    return "receiverMail";
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
            //return Body;
        }
        else
        {
            return "Session Expired";
        }
    }
    private MemoryStream PDFGenerate(string message, string ImagePath)
    {

        MemoryStream output = new MemoryStream();

        Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, output);

        pdfDoc.Open();
        Paragraph Text = new Paragraph(message);
        pdfDoc.Add(Text);

        byte[] file;
        file = System.IO.File.ReadAllBytes(ImagePath);

        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(file);
        jpg.ScaleToFit(550F, 200F);
        pdfDoc.Add(jpg);

        pdfWriter.CloseStream = false;
        pdfDoc.Close();
        output.Position = 0;

        return output;
    }
    [WebMethod(EnableSession = true)]
    public string BindDriver()
    {
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_Manager_OrderMaster.BindDriver(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string ProductSalesHistory(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                BL_Manager_OrderMaster.ProductSaleHistory(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string ConfirmSecurity(string CheckSecurity, string OrderAutoId, string MLTaxRemark)
    {
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                pobj.MLTaxRemark = MLTaxRemark;
                BL_Manager_OrderMaster.ConfirmSecurity(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdateShippingAddress(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.TypeShipping = jdv["ShippingAddress"];
                pobj.ShippingAddress2 = jdv["ShippingAddress2"];
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.OrdLat= jdv["lat"];
                pobj.OrdLong= jdv["long"];
                pobj.OrdState = jdv["state"];
                pobj.OrdCity = jdv["city"];
                pobj.OrdZipcode = jdv["zipcode"];
                pobj.OrdAddress = jdv["address"];
                BL_Manager_OrderMaster.UpdateShippingAddress(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindshippingAddress(string OrderAutoId)
    {
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_Manager_OrderMaster.BindshippingAddress(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindSalesperson()
    {
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_Manager_OrderMaster.bindSalesperson(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string changeSalesPerson(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesAutoId"]);
                pobj.Remarks = jdv["Remarks"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_Manager_OrderMaster.changeSalesPerson(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdateDriverRemark(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.Remarks = jdv["DriverRemark"];
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_Manager_OrderMaster.UpdateDriverRemark(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string changeOrderStatus(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.OrderStatus = Convert.ToInt32(jdv["Status"]);
                pobj.Remarks = jdv["Remarks"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_Manager_OrderMaster.changeOrderStatus(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string getDriverRemark(string OrderAutoId)
    {
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_Manager_OrderMaster.getDriverRemark(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}

