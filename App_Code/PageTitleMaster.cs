﻿using DllPageTitleMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for PageTitleMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class PageTitleMaster : System.Web.Services.WebService
{
    [WebMethod(EnableSession = true)]
    public string getPageTitleMaster(string dataValue)
    {

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PageTitleMaster pobj = new PL_PageTitleMaster();
                //pobj.Opcode = 101;
                pobj.PageId = Convert.ToInt32(jdv["PageId"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_PageTitleMaster.getPageInformation(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "false";
        }
    }


    [WebMethod(EnableSession = true)]
    public string savePageTitleMasterDetail(string dataValue)
    {


        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            try
            {
                PL_PageTitleMaster pobj = new PL_PageTitleMaster();
                //pobj.Opcode = 101;
                pobj.PageId = Convert.ToInt32(jdv["PageId"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.PageUrl = jdv["PageUrl"];
                pobj.PageTitle = jdv["PageTitle"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.LocationId = Convert.ToInt32(jdv["Location"]);
                pobj.UserDescription = jdv["UserDescription"];
                pobj.AdminDescription = jdv["AdminDescription"];
                BL_PageTitleMaster.savePageTitleDetial(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string updatePageTitleMasterDetail(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            try
            {
                PL_PageTitleMaster pobj = new PL_PageTitleMaster();
                pobj.PageId = Convert.ToInt32(jdv["PageId"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.PageUrl = jdv["PageUrl"];
                pobj.PageTitle = jdv["PageTitle"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.LocationId = Convert.ToInt32(jdv["Location"]);
                pobj.LocationName = Session["DBLocation"].ToString().ToLower();
                pobj.UserDescription = jdv["UserDescription"];
                pobj.AdminDescription = jdv["AdminDescription"];
                BL_PageTitleMaster.updatePageTitleDetial(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }


    [WebMethod(EnableSession = true)]
    public string getPageTitleMasterDetail(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            try
            {
                PL_PageTitleMaster pobj = new PL_PageTitleMaster();
                if (jdv["SPageId"] != null && jdv["SPageId"] != "")
                {
                    pobj.PageId = Convert.ToInt32(jdv["SPageId"]);
                }
                pobj.LocationName = Session["DBLocation"].ToString().ToLower();
                pobj.PageUrl = jdv["SPageUrl"];
                pobj.PageTitle = jdv["SPageTitle"];
                pobj.Status = Convert.ToInt32(jdv["SStatus"]);
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_PageTitleMaster.getPageTitleDetail(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getData(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            try
            {
                PL_PageTitleMaster pobj = new PL_PageTitleMaster();
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                BL_PageTitleMaster.getdata(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
