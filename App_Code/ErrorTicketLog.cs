﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLErrorTicketLog;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for ErrorTicketLog
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class ErrorTicketLog : System.Web.Services.WebService {

    public ErrorTicketLog () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession =true)]
    public string ddl()
    {
        PL_ErrorTicketLog pobj = new PL_ErrorTicketLog();
        BL_ErrorTicketLog.bindDll(pobj);
        return pobj.Ds.GetXml();
    }

    [WebMethod(EnableSession = true)]
    public string GetErrorTicketList(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ErrorTicketLog pobj = new PL_ErrorTicketLog();
        try
        {
            if (Session["EmpType"].ToString() != "Admin")
            {
                pobj.EmployeeType = Session["EmpTypeNo"].ToString();
                pobj.ByEmplyoyee = Session["EmpAutoId"].ToString();
            }
            else
            {
                pobj.EmployeeType = jdv["UserType"];
                pobj.ByEmplyoyee = jdv["Employee"];
            }
           
            pobj.Status = jdv["ClientStatus"];
            pobj.DevoloperStatus = jdv["DevoloperStatus"];
            pobj.Type = jdv["AllType"];
            pobj.TicketID = jdv["TicketId"];
            pobj.Subject = jdv["Subject"];
            if (jdv["FromDate"] != "")
            {
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            }
            if (jdv["ToDate"] != "")
            {
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            }
          
            pobj.PageSize = Convert.ToInt32(jdv["pageSize"]);
            pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
            BL_ErrorTicketLog.bingridall(pobj);
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetErrorTicketListExport(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ErrorTicketLog pobj = new PL_ErrorTicketLog();
        try
        {
            if(Session["EmpType"].ToString()!= "Admin")
            {
                pobj.EmployeeType = Session["EmpTypeNo"].ToString();
                pobj.ByEmplyoyee = Session["EmpAutoId"].ToString();
            }
            else
            {                
                pobj.EmployeeType = jdv["UserType"];
                pobj.ByEmplyoyee = jdv["Employee"];
            }
           
            pobj.Status = jdv["ClientStatus"];
            pobj.DevoloperStatus = jdv["DevoloperStatus"];
            pobj.Type = jdv["AllType"];
            pobj.TicketID = jdv["TicketId"];
            pobj.Subject = jdv["Subject"];
            if (jdv["FromDate"] != "")
            {
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            }
            if (jdv["ToDate"] != "")
            {
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            }
            pobj.PageSize = 0;
            pobj.PageIndex = 1;

            BL_ErrorTicketLog.bingridall(pobj);
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    
}
