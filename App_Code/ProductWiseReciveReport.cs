﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using DLLProductWiseReciveReport;
using DllUtility;

/// <summary>
/// Summary description for ProductWiseReciveReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ProductWiseReciveReport : System.Web.Services.WebService
{

    public ProductWiseReciveReport()
    {
        gZipCompression.fn_gZipCompression();
    }

   
    [WebMethod(EnableSession = true)]
    public string BindProductSubCategory(string CategoryAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_ProuctWiseReciveReport pobj = new PL_ProuctWiseReciveReport();
                pobj.CategoryAutoId = Convert.ToInt32(CategoryAutoId);
                BL_ProuctWiseReciveReport.BindProductSubCategory(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindProduct(string SubCategoryId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_ProuctWiseReciveReport pobj = new PL_ProuctWiseReciveReport();
                pobj.SubCategoryId = Convert.ToInt32(SubCategoryId);
                BL_ProuctWiseReciveReport.BindProduct(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetReportDetail(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ProuctWiseReciveReport pobj = new PL_ProuctWiseReciveReport();
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.CustomerTypeAutoId = Convert.ToInt32(jdv["CustomerTypeAutoId"]);
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(jdv["SalesAutoId"]);
                pobj.SubCategoryId = Convert.ToInt32(jdv["SubCategoryAutoId"]);
                pobj.OrderStatus = Convert.ToInt32(jdv["Status"]);
                pobj.BillNo = jdv["BillNo"];
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_ProuctWiseReciveReport.BindProductSaleReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {

                return "false";
            }
        }
        return "Session Expired";
    }
    [WebMethod(EnableSession = true)]
    public string BindCustomer()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_ProuctWiseReciveReport pobj = new PL_ProuctWiseReciveReport();
                BL_ProuctWiseReciveReport.BindCustomer(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}
