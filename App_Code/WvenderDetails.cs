﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLViewVenderDetails;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for WvenderDetails
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WvenderDetails : System.Web.Services.WebService {

    public WvenderDetails () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string ViewVenderDetails(string VendorId)
    {
        PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.VenderId = VendorId;

                BL_ViewVenderDetails.SelectVenderDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string PoList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.VenderId = jdv["VendorId"];
                pobj.PageSize = Convert.ToInt16(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt16(jdv["pageIndex"]);

                BL_ViewVenderDetails.GetPoList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getReceivedBill(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.VenderId = jdv["VendorId"];
                pobj.PageSize = Convert.ToInt16(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt16(jdv["pageIndex"]);

                BL_ViewVenderDetails.getReceivedBill(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string PaymentHistry(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
        try
        {
            if (Session["EmpAutoId"] != null)
            {             
                pobj.VenderId = jdv["VendorId"];            
                pobj.PageSize = Convert.ToInt16(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt16(jdv["pageIndex"]);

                BL_ViewVenderDetails.GetPayHitry(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getPayList(string dataValue)
    {
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
                //pobj.PayId = jdv["PayId"];
                pobj.VenderId = jdv["VendorId"];
                //if (jdv["FromDate"] != "")
                //    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                //if (jdv["ToDate"] != "")
                //    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                //pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                //pobj.PageIndex = jdv["pageIndex"];
                BL_ViewVenderDetails.GetPayList(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string DocumentSave(string dataValue)
    {
        PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.VenderId = jdv["VendorId"];
                pobj.DocumentName = jdv["DocumentName"];
                pobj.DocumentURL = jdv["DocumentURL"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_ViewVenderDetails.DocumentSave(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string CustomerDocument(string dataValue)
    {
        PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.VenderId = jdv["VendorId"];
                BL_ViewVenderDetails.CustomerDocument(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdateDocument(string dataValue)
    {
        PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.FileAutoId = Convert.ToInt32(jdv["FileAutoId"]);
                pobj.DocumentName = jdv["DocumentName"];
                pobj.DocumentURL = jdv["DocumentURL"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_ViewVenderDetails.UpdateDocument(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string DeleteDocument(string FileAutoId)
    {
        PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.FileAutoId = Convert.ToInt32(FileAutoId);
                BL_ViewVenderDetails.DeleteDocument(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindCardType()
    {
        PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();

        BL_ViewVenderDetails.bindCardType(pobj);
        if (Session["EmpAutoId"] != null)
        {
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string SaveBankDetails(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
                if (Convert.ToInt32(jdv["Status"]) == 1)
                {
                    pobj.BankName = jdv["BankName"];
                    pobj.BankAcc = jdv["ACName"];
                    pobj.RoutingNo= jdv["RoutingNo"];
                    pobj.Remarks = jdv["Remarks"];

                }
                else
                {
                    pobj.CardType = Convert.ToInt32(jdv["CardType"]);

                    pobj.CardNo = jdv["CardNo"];
                    if (jdv["ExpiryDate"] != "")
                        pobj.ExpiryDate = jdv["ExpiryDate"];
                    pobj.CVV = jdv["CVV"];
                    pobj.Zipcode = jdv["Zipcode"];
                    pobj.Remarks = jdv["Remarks"];
                }

                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.VenderId = jdv["VendorId"];
                BL_ViewVenderDetails.SaveBankDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetBankDetailsList(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            pobj.VenderId = jdv["VendorId"];
            BL_ViewVenderDetails.GetBankDetailsList(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string EditBankDetails(string AutoId)
    {
        PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
        pobj.AutoId = Convert.ToInt32(AutoId);
        BL_ViewVenderDetails.EditBankDetails(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }

    }
    [WebMethod(EnableSession = true)]
    public string UpdateBankDetails(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();

                if (Convert.ToInt32(jdv["Status"]) == 1)
                {
                    pobj.BankName = jdv["BankName"];
                    pobj.BankAcc = jdv["ACName"];
                    pobj.RoutingNo = jdv["RoutingNo"];
                    pobj.Remarks = jdv["Remarks"];
                }
                else
                {
                    pobj.CardType = Convert.ToInt32(jdv["CardType"]);

                    pobj.CardNo = jdv["CardNo"];
                    if (jdv["ExpiryDate"] != "")
                        pobj.ExpiryDate = jdv["ExpiryDate"];
                    pobj.CVV = jdv["CVV"];
                    pobj.Zipcode = jdv["Zipcode"];
                    pobj.Remarks = jdv["Remarks"];
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                BL_ViewVenderDetails.UpdateBankDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string DeleteBankDetails(string AutoId)
    {
        PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
        pobj.AutoId = Convert.ToInt32(AutoId);
        BL_ViewVenderDetails.DeleteBankDetails(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }

    }
    [WebMethod(EnableSession = true)]
    public string deleteVendor(string VendorId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
            try
            {
                pobj.VenderId = VendorId;
                BL_ViewVenderDetails.Vendersdelete(pobj);

                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getPayLists(string dataValue)
    {
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
                //pobj.PayId = jdv["PayId"];
                pobj.VenderId = jdv["VendorAutoId"];
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.PageIndex = jdv["pageIndex"];
                BL_ViewVenderDetails.getPayLists(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }
    //[WebMethod(EnableSession = true)]
    //public string SaveBankDetails1(string dataValue)
    //{
    //    if (Session["EmpTypeNo"] != null)
    //    {
    //        try
    //        {
    //            var jss = new JavaScriptSerializer();
    //            var jdv = jss.Deserialize<dynamic>(dataValue);
    //            PL_ViewVenderDetails pobj = new PL_ViewVenderDetails();
    //            //pobj.BankName = jdv["BankName"];
    //            pobj.CardType = Convert.ToInt32(jdv["CardType"]);
    //            //pobj.BankAcc = jdv["ACName"];
    //            pobj.CardNo = jdv["CardNo"];
    //            if (jdv["ExpiryDate"] != "")
    //                pobj.ExpiryDate = Convert.ToDateTime(jdv["ExpiryDate"]);
    //            pobj.CVV = jdv["CVV"];
    //            pobj.VenderId = jdv["VendorId"];
    //            pobj.Status = 1;
    //            BL_ViewVenderDetails.SaveBankDetails(pobj);
    //            if (!pobj.isException)
    //            {
    //                return pobj.Ds.GetXml();
    //            }
    //            else
    //            {
    //                return "false";
    //            }
    //        }
    //        catch (Exception)
    //        {
    //            return "false";
    //        }
    //    }
    //    else
    //    {
    //        return "Unauthorized access.";
    //    }
    //}
}
