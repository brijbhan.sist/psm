﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllUtility;
using DllStockReceiverMaster;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
/// <summary>
/// Summary description for WStockReceiverMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WStockReceiverMaster : System.Web.Services.WebService
{

    public WStockReceiverMaster()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string bindVendor()
    {
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                BL_StockReceiverMaster.GetVendornStatus(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindProduct()
    {
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                BL_StockReceiverMaster.bindProduct(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindUnitType(int productAutoId)
    {
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        if (Session["EmpAutoId"] != null)
        {

            try
            {
                pobj.ProductAutoId = productAutoId;
                BL_StockReceiverMaster.bindUnitType(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string insertStockEntry(string TableValues, string billData)
    {
        DataTable dtBill = new DataTable();
        dtBill = JsonConvert.DeserializeObject<DataTable>(TableValues);

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(billData);
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        if (Session["EmpAutoId"] != null)
        {

            try
            {
                if (dtBill.Rows.Count > 0)
                {
                    pobj.TableValue = dtBill;
                }

                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                pobj.BillNo = jdv["billNo"];
                if (jdv["billDate"] != null && jdv["billDate"] != "")
                {
                    pobj.BillDate = Convert.ToDateTime(jdv["billDate"]);
                }
                pobj.Remarks = (jdv["remarks"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_StockReceiverMaster.insert(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
   
    [WebMethod(EnableSession = true)]
    public string editStockEntry(int BillAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
            try
            {
                pobj.BillAutoId = BillAutoId;
                BL_StockReceiverMaster.editStockEntry(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string updateStockEntry(string TableValues, string billData)
    {
        if (Session["EmpAutoId"] != null)
        {
            DataTable dtQty = new DataTable();
            dtQty = JsonConvert.DeserializeObject<DataTable>(TableValues);
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(billData);
            PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
            try
            {
                if (dtQty.Rows.Count > 0)
                {
                    pobj.TableValue = dtQty;
                }
                pobj.BillAutoId = Convert.ToInt32(jdv["billAutoId"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.BillNo = jdv["billNo"];
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                if (jdv["billDate"] != null && jdv["billDate"] != "")
                {
                    pobj.BillDate = Convert.ToDateTime(jdv["billDate"]);
                }
                pobj.Remarks = (jdv["remarks"]);
                pobj.UserAutoId = Convert.ToInt32(Session["EmpAutoId"]);

                BL_StockReceiverMaster.update(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {

                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getProductThruBarcode(string barcode, string DraftAutoId,string vendorid,string billDate)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
            try
            {
                if (DraftAutoId != "")
                    pobj.DraftAutoId = Convert.ToInt32(DraftAutoId);
                if (billDate != null && billDate != "")
                {
                    pobj.BillDate = Convert.ToDateTime(billDate);
                }
                pobj.VendorAutoId = Convert.ToInt32(vendorid);
                pobj.Barcode = barcode;
                BL_StockReceiverMaster.getProductThruBarcode(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string assignBarcode(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        try
        {
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
            pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
            pobj.Barcode = jdv["Barcode"];
            BL_StockReceiverMaster.assignBarcode(pobj);
            return pobj.Ds.GetXml();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod(EnableSession = true)]
    public string countBarcode(string dataValues)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
            try
            {
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);

                BL_StockReceiverMaster.countBarcode(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetProductDetails(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        try
        {
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
            pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);

            BL_StockReceiverMaster.GetProductDetails(pobj);
            return pobj.Ds.GetXml();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateBasePrice(string dataValues)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
            try
            {
                pobj.ItemAutoId = Convert.ToInt32(jdv["ItemAutoId"]);
                if (jdv["BasePrice"] != "")
                {
                    pobj.BasePrice = Convert.ToDecimal(jdv["BasePrice"]);
                }
                if (jdv["RetailMIN"] != "")
                {
                    pobj.RetailMIN = Convert.ToDecimal(jdv["RetailMIN"]);
                }
                if (jdv["CostPrice"] != "")
                {
                    pobj.CostPrice = Convert.ToDecimal(jdv["CostPrice"]);
                }
                if (jdv["SRP"] != "")
                {
                    pobj.SRP = Convert.ToDecimal(jdv["SRP"]);
                }
                if (jdv["WholesaleMinPrice"] != "")
                {
                    pobj.WholesaleMinPrice = Convert.ToDecimal(jdv["WholesaleMinPrice"]);
                }
                pobj.UserAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_StockReceiverMaster.updateBasePrice(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    if (pobj.exceptionMessage == "EXISTS")
                    {
                        return pobj.Ds.GetXml();
                    }
                    else
                        return "false";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getStockEntryList(string dataValues)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
            try
            {
                pobj.BillNo = jdv["BillNo"];
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                if (jdv["FromBillDate"] != null && jdv["FromBillDate"] != "")
                {
                    pobj.FromBillDate = Convert.ToDateTime(jdv["FromBillDate"]);
                }
                if (jdv["ToBillDate"] != null && jdv["ToBillDate"] != "")
                {
                    pobj.ToBillDate = Convert.ToDateTime(jdv["ToBillDate"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["Index"]);
                BL_StockReceiverMaster.stockEntryList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string InvgetStockEntryList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_StockReceiverMaster pobj = new PL_StockReceiverMaster();
        try
        {
            pobj.BillNo = jdv["BillNo"];
            pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
            pobj.Status = Convert.ToInt32(jdv["Status"]);
            if (jdv["FromBillDate"] != null && jdv["FromBillDate"] != "")
            {
                pobj.FromBillDate = Convert.ToDateTime(jdv["FromBillDate"]);
            }
            if (jdv["ToBillDate"] != null && jdv["ToBillDate"] != "")
            {
                pobj.ToBillDate = Convert.ToDateTime(jdv["ToBillDate"]);
            }
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

            BL_StockReceiverMaster.InvstockEntryList(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}
