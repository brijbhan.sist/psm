﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllRequestProductList;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Data;

/// <summary>
/// Summary description for ProductList
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class RequestProductList : System.Web.Services.WebService
{
    string Body = "";
    [WebMethod(EnableSession = true)]
    public string bindCategory()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Product pobj = new PL_Product();
            try
            {
                BL_Product.bindCategory(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindSubcategory(int categoryAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Product pobj = new PL_Product();
            try
            {
                pobj.CategoryAutoId = categoryAutoId;
                BL_Product.bindSubcategory(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getProductList(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {


            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Product pobj = new PL_Product();
            try
            {
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                pobj.ProductId = jdv["ProductId"];
                pobj.ProductName = jdv["ProductName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                if (jdv["Location"] != "localhost" && jdv["Location"] != "psmnj")
                {
                    pobj.ProductLocation = jdv["Location"];

                }
                pobj.PreDefinedBarcode = jdv["BarCode"];
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);               
                //pobj.SearchBy = jdv["SearchBy"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_Product.select(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string deleteProduct(string ProductId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_Product pobj = new PL_Product();
            try
            {
                pobj.ProductId = ProductId;
                pobj.EmpAutoId = Convert.ToString(Session["EmpAutoId"]);
                BL_Product.deleteProduct(pobj);
                if (!pobj.isException)
                {
                    getMailBody(pobj.Ds, "8");
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    this.Context.Response.Clear();
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getBarcodeReport(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Product pobj = new PL_Product();
            try
            {
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                pobj.ProductId = jdv["ProductId"];
                pobj.ProductName = jdv["ProductName"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                BL_Product.BarcodeReport(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getBarcodeReportExport(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Product pobj = new PL_Product();
            try
            {
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                pobj.ProductId = jdv["ProductId"];
                pobj.ProductName = jdv["ProductName"];
                BL_Product.BarcodeReportExport(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getBarcodePrint(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Product pobj = new PL_Product();
            try
            {
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                pobj.ProductId = jdv["ProductId"];
                pobj.ProductName = jdv["ProductName"];
                BL_Product.BarcodeReportExport(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return JsonConvert.SerializeObject(pobj.Ds);
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    public string getMailBody(DataSet Ds, string Type)
    {
        if (Session["EmpAutoId"] != null)
        {



            try
            {
                if (Ds != null && Ds.Tables.Count > 0)
                {
                    Body = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'>";
                    Body += "<head> <style>table{border-collapse:collapse;}td {padding: 3px 8px;}.text-center{text-align:center}";
                    Body += "table {width: 100%;}body {font-size: 14px;font-family: monospace;}";
                    Body += ".center{text-align: center;}.right {text-align: right;}thead > tr {background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a;}";
                    Body += "thead > tr > td {text-align: center !important;}.InvContent {width: 100%;}.tdLabel {font-weight: 700;}";
                    Body += "@media print {#btnPrint {display: none;}}legend {border-bottom: 1px solid #5f7c8a;}";
                    Body += ".tableCSS > tbody > tr > td,.tableCSS > thead > tr > td,.tableCSS > tfoot > tr > td {border: 1px solid #5f7c8a;}pre{border:none !important;}";
                    Body += "</style>";
                    Body += "</head>";
                    Body += "<body style='font-family: Arial; font-size: 12px'>";
                    string OperationType = "", ReceiverEmailId = "";
                    if (Ds.Tables[0].Rows.Count > 0)
                    {
                        Body += "<table class='table tableCSS'>";
                        Body += "<tbody>";
                        Body += "<tr><td><b>Product Id</b></td><td>" + Ds.Tables[0].Rows[0]["Prdtid"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Product Name</b></td><td>" + Ds.Tables[0].Rows[0]["ProductName"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Product Image</b></td><td><img src=" + Ds.Tables[0].Rows[0]["ImageUrl"].ToString() + " alt='Product Image' style='width: 50px;height: 50px;'></td></tr>";
                        Body += "<tr><td><b>Deleted By</b></td><td>" + Ds.Tables[0].Rows[0]["EmpName"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Date</b></td><td>" + Ds.Tables[0].Rows[0]["DeleteDate"].ToString() + "</td></tr>";
                        Body += "</tbody>";
                        Body += "</table>";
                        Body += "</tbody>";
                        Body += "</table>";
                        Body += "</body>";
                        Body += "</html>";
                        OperationType = Ds.Tables[0].Rows[0]["Location"].ToString() + " - Product deleted";
                    }
                    int e = 0;
                    for (e = 0; e <= Ds.Tables[1].Rows.Count - 1; e++)
                    {
                        ReceiverEmailId += Ds.Tables[1].Rows[e]["Email"].ToString() + ",";
                    }
                    if (ReceiverEmailId != null && ReceiverEmailId != string.Empty && ReceiverEmailId.Length > 0)
                    {
                        string emailmsg = MailSending.SendMailResponse(ReceiverEmailId, "", "", OperationType, Body.ToString(), "Developer");
                        if (emailmsg == "Successful")
                        {
                            return "true";
                        }
                        else
                        {
                            return "EmailNotSend";
                        }
                    }
                    else
                    {
                        return "receiverMail";
                    }
                }
                else
                {
                    return "EmailNotSend";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetProductBarcode(int ProductAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Product pobj = new PL_Product();
            try
            {
                pobj.AutoId = ProductAutoId;
                BL_Product.GetProductBarcode(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetProductAllBarcode(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Product pobj = new PL_Product();
            try
            {
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                pobj.ProductId = jdv["ProductId"];
                pobj.ProductName = jdv["ProductName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.ProductLocation = jdv["Location"];
                pobj.PreDefinedBarcode = jdv["BarCode"];
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                if (jdv["Stock"] != "")
                    pobj.Qty = Convert.ToInt32(jdv["Stock"]);
                pobj.SearchBy = jdv["SearchBy"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_Product.GetProductAllBarcode(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
