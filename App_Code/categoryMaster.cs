﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllCategory;
using System.Web.Script.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class categoryMaster : System.Web.Services.WebService
{
    [WebMethod(EnableSession = true)]
    public string insertCategory(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Category pobj = new PL_Category();
            try
            {
                pobj.CategoryName = jdv["CategoryName"];
                pobj.Description = jdv["Description"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.IsShow = Convert.ToInt32(jdv["IsShowOnWeb"]);
                if (jdv["SequenceNo"] != "")
                    pobj.SeqNo = Convert.ToInt32(jdv["SequenceNo"]);
                BL_Category.insert(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getCategoryDetail(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Category pobj = new PL_Category();
            try
            {
                pobj.CategoryId = jdv["CategoryId"];
                pobj.CategoryName = jdv["CategoryName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_Category.select(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string editCategory(string CategoryId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Category pobj = new PL_Category();
            pobj.CategoryId = CategoryId;
            BL_Category.editCategory(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateCategory(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Category pobj = new PL_Category();
            try
            {
                pobj.CategoryId = jdv["CategoryId"];
                pobj.CategoryName = jdv["CategoryName"];
                pobj.Description = jdv["Description"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.IsShow = Convert.ToInt32(jdv["IsShowOnWeb"]);
                if (jdv["SequenceNo"] != "")
                    pobj.SeqNo = Convert.ToInt32(jdv["SequenceNo"]);
                BL_Category.update(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteCategory(string CategoryId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Category pobj = new PL_Category();
            try
            {
                pobj.CategoryId = CategoryId;
                BL_Category.delete(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
}
