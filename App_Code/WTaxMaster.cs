﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllTaxMaster;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for WTaxMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WTaxMaster : System.Web.Services.WebService {

    public WTaxMaster () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string insertTax(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {

            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_TaxMaster pobj = new PL_TaxMaster();
            try
            {
                pobj.TaxName = jdv["TaxName"];
                if (jdv["Value"] != "")
                {
                    pobj.Value = Convert.ToDecimal(jdv["Value"]);
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.State = Convert.ToInt32(jdv["State"]);
                pobj.PrintLabel = jdv["PrintLabel"];
                BL_TaxMaster.insert(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getTaxDetail(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_TaxMaster pobj = new PL_TaxMaster();
            try
            {
                pobj.TaxId = jdv["TaxId"];
                pobj.TaxName = jdv["TaxName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_TaxMaster.select(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
   [WebMethod(EnableSession = true)]
    public string editTax(string TaxId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_TaxMaster pobj = new PL_TaxMaster();
            pobj.TaxId = TaxId;
            BL_TaxMaster.editTax(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
     [WebMethod(EnableSession = true)]
    public string updateTax(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_TaxMaster pobj = new PL_TaxMaster();
            try
            {
                pobj.TaxId = jdv["TaxId"];
                pobj.TaxName = jdv["TaxName"];
                if (jdv["Value"] != "")
                {
                    pobj.Value = Convert.ToDecimal(jdv["Value"]);
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.State = Convert.ToInt32(jdv["State"]);
                pobj.PrintLabel = jdv["PrintLabel"];
                BL_TaxMaster.update(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later."; 
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
   [WebMethod(EnableSession = true)]
    public string deleteTax(string TaxId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_TaxMaster pobj = new PL_TaxMaster();
            try
            {
                pobj.TaxId = TaxId;
                BL_TaxMaster.delete(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
     [WebMethod(EnableSession = true)]
    public string BindState()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_TaxMaster pobj = new PL_TaxMaster();
            try
            {
                BL_TaxMaster.BindState(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    
}
