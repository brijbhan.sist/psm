﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using DllOtpReportSummary;
using DllUtility;
/// <summary>
/// Summary description for OtpReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class OtpReport : System.Web.Services.WebService {

    public OtpReport () {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string GetOtpReport(string dataValue)
    {
        try
        {
            PL_OtpReportSummary pobj = new PL_OtpReportSummary();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            if (jdv["DateFrom"] != null && jdv["DateFrom"] != "")
            {
                pobj.FromDate = Convert.ToDateTime(jdv["DateFrom"]);
            }
            if (jdv["DateTo"] != null && jdv["DateTo"] != "")
            {
                pobj.ToDate = Convert.ToDateTime(jdv["DateTo"]);
            }
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            BL_OtpReportSummary.GetOtpResport(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindTaxType()
    {
        PL_OtpReportSummary pobj = new PL_OtpReportSummary();
        BL_OtpReportSummary.BindTaxType(pobj);
        return pobj.Ds.GetXml();
    }
}
