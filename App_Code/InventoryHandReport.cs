﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllInventoryHandReport;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Data;

/// <summary>
/// Summary description for ProductList
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class InventoryHandReport : System.Web.Services.WebService
{
    string Body = "";
    [WebMethod(EnableSession = true)]
    public string bindCategory()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Product pobj = new PL_Product();
            try
            {
                BL_Product.bindCategory(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindSubcategory(int categoryAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Product pobj = new PL_Product();
            try
            {
                pobj.CategoryAutoId = categoryAutoId;
                BL_Product.bindSubcategory(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getProductList(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {


            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Product pobj = new PL_Product();
            try
            {
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                pobj.ProductId = jdv["ProductId"];
                pobj.Barcode = jdv["BarCode"];
                pobj.ProductName = jdv["ProductName"];
                pobj.BrandAutoId = Convert.ToInt32(jdv["BrandName"]); 
                pobj.Status = Convert.ToInt32(jdv["Status"]); 
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_Product.select(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}
