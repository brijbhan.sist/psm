﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllVendorMaster;
using System.Web.Script.Serialization;
using System.Data;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class vendorMaster : System.Web.Services.WebService
{

    [WebMethod(EnableSession = true)]
    public string LocationBind(string LocationName)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            //var jdv = jss.Deserialize<dynamic>(LocationName);

            PL_VendorMaster pobj = new PL_VendorMaster();
            pobj.Location = LocationName;
            BL_VendorMaster.bindLocation(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Unauthorized";
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindDropDown()
    {
        if (Session["EmpTypeNo"] != null)
        {

            PL_VendorMaster pobj = new PL_VendorMaster();
            BL_VendorMaster.bindDropDown(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Unauthorized";
        }
    }

    [WebMethod(EnableSession = true)]
    public string insertVendor(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_VendorMaster pobj = new PL_VendorMaster();
            try
            {
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.VendorName = jdv["VendorName"];              
                pobj.LocationAutoId = Convert.ToInt32(jdv["Location"]);
                pobj.LocationTypeAutoId = Convert.ToInt32(jdv["Type"]);
                pobj.Address = jdv["Address"];
                pobj.Country = Convert.ToInt32(jdv["Country"]);
                pobj.Zipcode = jdv["Zipcode"];
                pobj.ContactPerson = jdv["ContactPerson"];
                pobj.Cell = jdv["Cell"];
                pobj.Office1 = jdv["Office1"];
                pobj.Office2 = jdv["Office2"];
                pobj.Email = jdv["Email"];
                pobj.City = jdv["City"];
                pobj.State = Convert.ToInt32(jdv["State"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_VendorMaster.insert(pobj);
                if (!pobj.isException)
                {
                    return "success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getVendorRecord(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_VendorMaster pobj = new PL_VendorMaster();
            try
            {
                pobj.VendorName = jdv["VendorName"];
                pobj.Cell = jdv["VendorCell"];
                pobj.PageIndex = jdv["pageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_VendorMaster.selectVendorRecord(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized";
        }
    }

    [WebMethod(EnableSession = true)]
    public string editVendor(string VendorId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_VendorMaster pobj = new PL_VendorMaster();
            try
            {
                pobj.VendorId = VendorId;
                BL_VendorMaster.editVendorRecord(pobj);

                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized";
        }
    }

    [WebMethod(EnableSession = true)]
    public string updateVendor(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_VendorMaster pobj = new PL_VendorMaster();
            try
            {
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.VendorId = jdv["VendorId"];
                pobj.LocationAutoId = Convert.ToInt32(jdv["Location"]);
                pobj.LocationTypeAutoId = Convert.ToInt32(jdv["Type"]);
                pobj.VendorName = jdv["VendorName"];
                pobj.Address = jdv["Address"];
                pobj.Country = Convert.ToInt32(jdv["Country"]);
                pobj.State = Convert.ToInt32(jdv["State"]);
                pobj.City =jdv["City"];
                pobj.Zipcode = jdv["Zipcode"];
                pobj.ContactPerson = jdv["ContactPerson"];
                pobj.Cell = jdv["Cell"];
                pobj.Office1 = jdv["Office1"];
                pobj.Office2 = jdv["Office2"];
                pobj.Email = jdv["Email"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);

                BL_VendorMaster.update(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage.ToString();
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized";
        }
    }

    [WebMethod(EnableSession = true)]
    public string deleteVendor(string VendorId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_VendorMaster pobj = new PL_VendorMaster();
            try
            {
                pobj.VendorId = VendorId;
                BL_VendorMaster.delete(pobj);

                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized";
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindVendor()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_VendorMaster pobj = new PL_VendorMaster();
            BL_VendorMaster.bindVendor(pobj);
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return json;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Unauthorized";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetStateCity(string Zipcode)
    {

        if (Session["EmpTypeNo"] != null)
        {
            PL_VendorMaster pobj = new PL_VendorMaster();
            try
            {
                pobj.Zipcode = Zipcode;
                BL_VendorMaster.SelectStateCity(pobj);

                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized";
        }
    }
}
