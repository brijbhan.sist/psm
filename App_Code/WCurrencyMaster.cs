﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLCurrencyMaster;
using System.Web.Script.Serialization;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WCurrencyMaster : System.Web.Services.WebService {

    public WCurrencyMaster () {
      
    }

    [WebMethod(EnableSession = true)]
    public string InsertCurrency(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            String CurrencyMaster = dataValue;
            Pl_CurrencyMaster pobj = new Pl_CurrencyMaster();
            try
            {
                pobj.CurrencyXml = CurrencyMaster;
                pobj.Createdby = Convert.ToInt32(Session["EmpAutoId"]);
                BL_CurrencyMaster.insertCurrency(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getCurrencyDetail(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            Pl_CurrencyMaster pobj = new Pl_CurrencyMaster();
            try
            {

                pobj.CurrencyName = jdv["CurrencyName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_CurrencyMaster.GetCurrencyDetail(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string editCurrency(int CurrencyId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            Pl_CurrencyMaster pobj = new Pl_CurrencyMaster();
            pobj.Autoid = CurrencyId;
            BL_CurrencyMaster.editCurrency(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
     [WebMethod(EnableSession = true)]
    public string updateCurrency(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            String CurrencyMaster = dataValue;
            Pl_CurrencyMaster pobj = new Pl_CurrencyMaster();
            try
            {
                pobj.CurrencyXml = CurrencyMaster;
                pobj.Createdby = Convert.ToInt32(Session["EmpAutoId"]);
                BL_CurrencyMaster.updateCurreny(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
     [WebMethod(EnableSession = true)]
     public string deleteCurrency(int CurrencyId)
     {
         if (Session["EmpTypeNo"] != null)
         {
             Pl_CurrencyMaster pobj = new Pl_CurrencyMaster();
             try
             {
                 pobj.Autoid = CurrencyId;
                 BL_CurrencyMaster.DeleteCurrency(pobj);
                 if (!pobj.isException)
                 {
                     this.Context.Response.ContentType = "application/json; charset=utf-8";
                     this.Context.Response.StatusCode = 200;
                     return "Success";
                 }
                 else
                 {
                     return pobj.exceptionMessage;
                 }
             }
             catch (Exception ex)
             {
                 return "Oops! Something went wrong.Please try later.";
             }
         }
         else
         {
             return "Unauthorized access.";
         }
     }
}
