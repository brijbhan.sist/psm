﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllUtility;
using System.Web.Script.Serialization;
using DLLPaymentLogReport;
/// <summary>
/// Summary description for WPaymentLogReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WPaymentLogReport : System.Web.Services.WebService
{

    public WPaymentLogReport()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string getPaymentReport(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_PaymentLogReport pobj = new PL_PaymentLogReport();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(jdv["CollectionBy"]);
                pobj.PayId = jdv["PayId"];
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);

                if (jdv["SettledFromDate"] != "")
                    pobj.SettledFromDate = Convert.ToDateTime(jdv["SettledFromDate"]);
                if (jdv["SettledToDate"] != "")
                    pobj.SettledToDate = Convert.ToDateTime(jdv["SettledToDate"]);

                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.PaymentMode = Convert.ToInt32(jdv["PaymentMode"]);
                pobj.Range = Convert.ToInt32(jdv["Range"]);
                if (jdv["PaymentAmount"] != "")
                { 
                    pobj.PaymentAmount = Convert.ToDecimal(jdv["PaymentAmount"]);
                }
                else
                {
                    pobj.PaymentAmount = Convert.ToDecimal("0.00");
                }
                   
                BL_PaymentLogReport.BindPaymentLogReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindDropDown()
    {
        PL_PaymentLogReport pobj = new PL_PaymentLogReport();
        BL_PaymentLogReport.BindDropDown(pobj);
        return pobj.Ds.GetXml();
    }
    [WebMethod(EnableSession = true)]
    public string getReceivedPrint(string PaymentAutoId)
    {
        PL_PaymentLogReport pobj = new PL_PaymentLogReport();
        pobj.PaymentAutoId = Convert.ToInt32(PaymentAutoId);
        BL_PaymentLogReport.getReceivedPrint(pobj);
        return pobj.Ds.GetXml();
    }
}

