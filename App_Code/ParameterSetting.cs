﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllParameterSetting;
using System.Web.Script.Serialization;


/// <summary>
/// Summary description for ParameterSetting
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ParameterSetting : System.Web.Services.WebService
{

    public ParameterSetting()
    {
    }
    ///
    [WebMethod (EnableSession=true)]
    public string insertData(string datavalue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(datavalue);
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            try
            {
                pobj.State = jdv["SName"];
                pobj.Abbreviation = jdv["Abbreviation"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_ParameterSetting.Insert(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdateStateMaster(string datavalue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(datavalue);
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            try
            {
                pobj.Stateid = Convert.ToInt32(jdv["SId"]);
                pobj.State = jdv["SName"];
                pobj.Abbreviation = jdv["Abbreviation"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_ParameterSetting.update(pobj);
                if (!pobj.isException)
                {
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getStateDetail(string dataValue)  
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            try
            {
                pobj.State = jdv["StateSearch"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                BL_ParameterSetting.select(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string EditStateMaster(int AutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            pobj.Stateid = AutoId;
            BL_ParameterSetting.edit(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string DeleteStateMaster(int AutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                PL_ParameterSetting pobj = new PL_ParameterSetting();
                pobj.Stateid = AutoId;
                BL_ParameterSetting.delete(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string selectData(string datavalue)
    {
        return datavalue;
    }
    //here start services to city
    [WebMethod(EnableSession = true)]
    public string bindActiveState()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            BL_ParameterSetting.bindActiveState(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindSearchState_City()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            BL_ParameterSetting.bindSearchState_City(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string insertData_City(string datavalue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(datavalue);
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            try
            {
                //pobj.id= jdv["CId"];
                pobj.Stateid = Convert.ToInt32(jdv["CState"]);
                pobj.City = jdv["CCity"];
                pobj.Status = Convert.ToInt32(jdv["CStatus"]);
                BL_ParameterSetting.insertData_City(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getCityDetail(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            try
            {
                pobj.Stateid = Convert.ToInt32(jdv["StateSearch"]);
                pobj.City = jdv["CitySeach"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                BL_ParameterSetting.tblbindActiveState_City(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string DeleteCityMaster(int AutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                PL_ParameterSetting pobj = new PL_ParameterSetting();
                pobj.Stateid = AutoId;
                BL_ParameterSetting.deleteCity(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string EditCityMaster(int AutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            pobj.Stateid = AutoId;
            BL_ParameterSetting.editCity(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdateCityMaster(string datavalue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(datavalue);
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            try
            {
                pobj.CityId = Convert.ToInt32(jdv["CCityId"]);
                pobj.Stateid = Convert.ToInt32(jdv["CState"]);
                pobj.City = jdv["CCity"];
                pobj.Status = Convert.ToInt32(jdv["CStatus"]);
                BL_ParameterSetting.updateCity(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    //here start services to Zip
    [WebMethod(EnableSession = true)]
    public string bindActiveCity_Zip()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            BL_ParameterSetting.bindActiveCity_Zip(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getZipDetail(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            try
            {
                pobj.Stateid = Convert.ToInt32(jdv["StateSearch"]);
                pobj.CityId = Convert.ToInt32(jdv["CitySearch"]);
                pobj.Zip = jdv["ZipSearch"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                BL_ParameterSetting.tblbind_Zip(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string insertData_Zip(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            try
            {
                //pobj.id= jdv["CId"];
                pobj.Stateid = Convert.ToInt32(jdv["ZState"]);
                pobj.CityId = Convert.ToInt32(jdv["ZCity"]);
                pobj.Zip = jdv["ZZip"];
                pobj.Status = Convert.ToInt32(jdv["ZStatus"]);
                BL_ParameterSetting.insertData_Zip(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteZip(int AutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                PL_ParameterSetting pobj = new PL_ParameterSetting();
                pobj.Stateid = AutoId;
                BL_ParameterSetting.deleteZip(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string EditZipMaster(int AutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            pobj.Stateid = AutoId;
            BL_ParameterSetting.editZip(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdateZipMaster(string datavalue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(datavalue);
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            try
            {
                pobj.ZipId = Convert.ToInt32(jdv["ZZipId"]);
                pobj.Stateid = Convert.ToInt32(jdv["ZStateId"]);
                pobj.CityId = Convert.ToInt32(jdv["ZCityId"]);
                pobj.Zip = jdv["ZZipCode"];
                pobj.Status = Convert.ToInt32(jdv["ZStatus"]);
                BL_ParameterSetting.updateZip(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string binedState_Zip(string categoryAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ParameterSetting pobj = new PL_ParameterSetting();
            pobj.Stateid = Convert.ToInt32(categoryAutoId);
            BL_ParameterSetting.binedState_Zip(pobj);

            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
}

