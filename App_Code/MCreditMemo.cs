﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllUtility;
using DLLManagerSalesMemo;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Data;
using DLLCreditMemoMaster;

/// <summary>
/// Summary description for MCreditMemo
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]


public class MCreditMemo : System.Web.Services.WebService
{

    public MCreditMemo()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string bindDropdown()
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            BL_ManagerSalesMemo.bindDropdown(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string BindProduct(string CustomerAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
            pobj.CustomerAutoId = Convert.ToInt32(CustomerAutoId);
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            BL_ManagerSalesMemo.BindProduct(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindUnitType(string ProductAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.ProductAutoId = Convert.ToInt32(ProductAutoId);
            BL_ManagerSalesMemo.SelectUnit(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string ValidateCreditMemo(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
            pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
            pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
            pobj.Qty = Convert.ToInt32(jdv["Qty"]);
            BL_ManagerSalesMemo.ValidateCreditMemo(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string insertOrderData(string TableValues, string dataValue)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }

                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
                pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
                pobj.TaxValue = Convert.ToDecimal(jdv["TaxValue"]);
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.MLTax = Convert.ToDecimal(jdv["MLTax"]);
                pobj.AdjustmentAmt = Convert.ToDecimal(jdv["AdjustmentAmt"]);
                pobj.Remarks = jdv["Remarks"];
                BL_ManagerSalesMemo.insert(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success!!";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string OrderRemarkDeatil(string CreditAutoId)
    {
        PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
        pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
        BL_ManagerSalesMemo.BindCreditLog(pobj);
        return pobj.Ds.GetXml();
    }
    [WebMethod(EnableSession = true)]
    public string editCredit(string CreditAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
            BL_ManagerSalesMemo.EditCredit(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateOrderData(string TableValues, string dataValue, string CreditAutoId)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();

        PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }

                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
                pobj.TaxValue = Convert.ToDecimal(jdv["TaxValue"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);

                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.MLTax = Convert.ToDecimal(jdv["MLTax"]);
                pobj.AdjustmentAmt = Convert.ToDecimal(jdv["AdjustmentAmt"]);
                pobj.Remarks = jdv["Remarks"];
                pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
                BL_ManagerSalesMemo.updateCredit(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success!!";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateOrderData1(string TableValues,string dataValue)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.CreditAutoId = Convert.ToInt32(jdv["CreditAutoId"]);
                pobj.ManagerRemark = jdv["ManagerRemark"];
                pobj.CreditMemoType = Convert.ToInt32(jdv["CreditMemoType"]);
                if (Convert.ToInt32(jdv["ReferenceOrderAutoId"]) != 0)
                {
                    pobj.ReferenceOrderAutoId = Convert.ToInt32(jdv["ReferenceOrderAutoId"]);
                }
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                BL_ManagerSalesMemo.updateCredit1(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success!!";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string ApprovedCredit(string CreditData, string TableValues)
    {
        if (Session["EmpAutoId"] != null)
        {
            DataTable dtCredit = new DataTable();
            dtCredit = JsonConvert.DeserializeObject<DataTable>(TableValues);
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(CreditData);
            PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.CreditAutoId = Convert.ToInt32(jdv["CreditAutoId"]);
            pobj.Status = Convert.ToInt32(jdv["Status"]);
            pobj.ManagerRemark = jdv["ManagerRemark"];
            pobj.TableValue = dtCredit;
            pobj.IPAddress = HttpContext.Current.Request.UserHostAddress;
            BL_ManagerSalesMemo.ApprovedCredit(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return "Success!!";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string CompleteOrder(string CreditAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
            BL_ManagerSalesMemo.CreditComplete(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return "Success!!";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string PrintCredit(string CreditAutoId)
    {
        PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
        pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
        BL_ManagerSalesMemo.PrintCredit(pobj);
        return pobj.Ds.GetXml();

    }

    [WebMethod(EnableSession = true)]
    public string CheckSecurity(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
        try
        {
            if (Session["EmpTypeNo"] != null)
            {
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpTypeNo"].ToString());
                pobj.SecurityKey = jdv["Security"];
                BL_ManagerSalesMemo.checkSecurity(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string CancelCreditMomo(string datavalue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(datavalue);
            PL_ManagerSalesMemo pobj = new PL_ManagerSalesMemo();
            pobj.EmpAutoId = Convert.ToInt32((Session["EmpAutoId"]).ToString());
            pobj.CreditAutoId = Convert.ToInt32(jdv["CreditAutoId"]);
            pobj.CancelRemarks = jdv["CancelRemark"];
            pobj.IPAddress = HttpContext.Current.Request.UserHostAddress;
            BL_ManagerSalesMemo.CancelCreditMemo(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}
