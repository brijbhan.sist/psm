﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllPurchaseOrder;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
using DllUtility;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class PurchaseOrder : System.Web.Services.WebService
{
    public PurchaseOrder()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string bindDropdown()
    {
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_PurchaseOrder.bindDropdown(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string selectQtyPrice(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                BL_PurchaseOrder.selectQtyPrice(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindUnitType(int productAutoId)
    {
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.ProductAutoId = productAutoId;
                BL_PurchaseOrder.bindUnitType(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }


    [WebMethod(EnableSession = true)]
    public string InsUpdData(string data, string xml)
    {
        //DataTable dtOrder = new DataTable();
        //dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(data);
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                string t = xml;
                pobj.EmpId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.NOofItems = Convert.ToInt32(jdv["NoofItems"]);
                if (jdv["DraftAutoId"] != "")
                {
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                }
                if (jdv["POAutoId"] != "")
                {
                    pobj.POAutoId = Convert.ToInt32(jdv["POAutoId"]);
                }
                if (jdv["PORemark"] != "")
                {
                    pobj.PORemarks = jdv["PORemark"];
                }
                pobj.VenderAutoId = Convert.ToInt32(jdv["VenderAutoId"]);
                pobj.TableData = t;

                BL_PurchaseOrder.insertandupdate(pobj);
                if (!pobj.isException)
                {

                    return pobj.exceptionMessage;

                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {

            }
            return "Session Expired";
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string DraftData(string data)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(data);
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.EmpId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["unitAutoId"]);
                pobj.QtyPerUnit = Convert.ToInt32(jdv["QtyPerUnit"]);
                if (jdv["DraftAutoId"] != "")
                {
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                }
                if (jdv["PORemark"] != "")
                {
                    pobj.PORemarks = jdv["PORemark"];
                }
                if (jdv["VenderAutoId"] != "")
                {
                    pobj.VenderAutoId = Convert.ToInt32(jdv["VenderAutoId"]);
                }
                BL_PurchaseOrder.DraftSaveData(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
           
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string DeleteDraftItem(string DataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(DataValue);
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                BL_PurchaseOrder.DeleteDraftItem(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string EditDraftOrder(string DraftAutoId)
    {
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.DraftAutoId = Convert.ToInt32(DraftAutoId);
                BL_PurchaseOrder.editDraftDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetBarDetails(string dataValues)
    {
        string msg = "";
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.Barcode = jdv["Barcode"];
                pobj.EmpId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                if (jdv["DraftAutoId"] != "")
                {
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                }
                if (jdv["PORemark"] != "")
                {
                    pobj.PORemarks = jdv["PORemark"];
                }
                if (jdv["VenderAutoId"] != "")
                {
                    pobj.VenderAutoId = Convert.ToInt32(jdv["VenderAutoId"]);
                }
                BL_PurchaseOrder.GetBarDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    if(pobj.exceptionMessage== "BarcodeDoesNotExist")
                    {
                        msg = "BarcodeDoesNotExist";
                    }
                    else if(pobj.exceptionMessage == "ProductisInactive")
                    {
                        msg = "ProductisInactive";
                    }
                    else
                    {
                        msg = pobj.exceptionMessage;
                    }
                    return msg;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getDraftOrderList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                
                if (jdv["VenderName"] != "")
                {
                    pobj.VendorName = jdv["VenderName"];
                }
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = jdv["pageIndex"];
                pobj.EmpId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_PurchaseOrder.DraftOrderList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string editOrder(string AutoId)
    {
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.POAutoId = Convert.ToInt32(AutoId);
                pobj.EmpId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_PurchaseOrder.editOrderDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdateDraftReq(string dataValues)
    {
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["unitAutoId"]);
                pobj.QtyPerUnit= Convert.ToInt32(jdv["QtyPerUnit"]);
                if (jdv["ReqQty"] != "")
                    pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                BL_PurchaseOrder.UpdateDraftReq(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
}
