﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllbulkPrintOrder;

/// <summary>
/// Summary description for WCBulkOrdePrint
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WCBulkOrdePrint : System.Web.Services.WebService
{

    public WCBulkOrdePrint()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string getBulkOrderData()
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_bulkPrintOrder pobj = new PL_bulkPrintOrder();
            try
            {
                //pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                //pobj.BulkOrderAutoId = OrderAutoId;
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_bulkPrintOrder.PrintOrder(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return json;
                    //return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
