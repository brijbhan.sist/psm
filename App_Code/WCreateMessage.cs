﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using DllNotificationMessage;
using System.Data;
using Newtonsoft.Json;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WCreateMessage : System.Web.Services.WebService
{

    [WebMethod(EnableSession = true)]
    public string BindRecipient(int TeamId)
    {
        PL_NotificationMessage pobj = new PL_NotificationMessage();
        if (Session["EmpAutoId"] != null)
        {
            pobj.TeamId = TeamId;
            BL_NotificationMessage.SelectRecipient(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string SaveMessage(string MessageData)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(MessageData);
        try

        {
            PL_NotificationMessage pobj = new PL_NotificationMessage();
            if (Session["EmpAutoId"] != null)
            {
                pobj.Message = jdv["Message"];
                pobj.Title = jdv["Title"];
                DataTable dt = JsonConvert.DeserializeObject<DataTable>(jdv["messageImageUrl"]);
                if (dt.Rows.Count > 0)
                    pobj.dtbulkmessageImageUrl = dt;
                if (jdv["Sequence"] != "")
                {
                    pobj.Sequence = Convert.ToInt32(jdv["Sequence"]);
                }

                if (jdv["ExpiryDate"] != "" && jdv["ExpiryTime"] != "")
                {
                    pobj.ExpiryDate = Convert.ToDateTime(jdv["ExpiryDate"]);
                    pobj.ExpiryTime = jdv["ExpiryTime"];
                }
                if (jdv["StartDate"] != "" && jdv["StartDate"] != "")
                {
                    pobj.StartDate = Convert.ToDateTime(jdv["StartDate"]);
                    pobj.StartTime = jdv["StartTime"];
                }
                pobj.Recipient = jdv["Recipient"];
                pobj.TeamId = jdv["TeamId"];
                pobj.IsRepeat = Convert.ToInt32(jdv["IsRepeat"]);
                BL_NotificationMessage.SaveMessage(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string BindMessage(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_NotificationMessage pobj = new PL_NotificationMessage();
        if (Session["EmpAutoId"] != null)
        {
            if (jdv["ExpiryDate"] != "")
            {
                pobj.ExpiryDate = Convert.ToDateTime(jdv["ExpiryDate"]);
            }
            pobj.PageIndex = jdv["PageIndex"];
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            BL_NotificationMessage.GetMessages(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string EditMessage(string MessageAutoId)
    {

        PL_NotificationMessage pobj = new PL_NotificationMessage();
        if (Session["EmpAutoId"] != null)
        {

            pobj.MessageAutoId = Convert.ToInt32(MessageAutoId);
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            BL_NotificationMessage.EditMessage(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdateMessage(string MessageData)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(MessageData);
        try
        {
            PL_NotificationMessage pobj = new PL_NotificationMessage();
            if (Session["EmpAutoId"] != null)
            {
                pobj.Message = jdv["Message"];
                pobj.Title = jdv["Title"];
                if (jdv["StartDate"] != "" && jdv["StartDate"] != "")
                {
                    pobj.StartDate = Convert.ToDateTime(jdv["StartDate"]);
                    pobj.StartTime = jdv["StartTime"];
                }
                pobj.IsRepeat = Convert.ToInt32(jdv["IsRepeat"]);
                if (jdv["Sequence"] != "")
                    pobj.Sequence = Convert.ToInt32(jdv["Sequence"]);

                if (jdv["ExpiryDate"] != "" && jdv["ExpiryTime"] != "")
                {
                    pobj.ExpiryDate = Convert.ToDateTime(jdv["ExpiryDate"]);
                    pobj.ExpiryTime = jdv["ExpiryTime"];
                }
                if (jdv["messageImageUrl"] != "[]")
                {
                    DataTable dt = JsonConvert.DeserializeObject<DataTable>(jdv["messageImageUrl"]);
                    if (dt.Rows.Count > 0)
                        pobj.dtbulkmessageImageUrl = dt;
                    pobj.Noblank = 1;
                }


                pobj.MessageAutoId = Convert.ToInt32(jdv["MessageAutoId"]);
                pobj.Recipient = jdv["Recipient"];
                pobj.TeamId = jdv["TeamId"];

                BL_NotificationMessage.updateMessage(pobj);
                if (!pobj.isException)
                {
                    return "updated";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteMessage(string MessageAutoId)
    {

        PL_NotificationMessage pobj = new PL_NotificationMessage();
        if (Session["EmpAutoId"] != null)
        {

            pobj.MessageAutoId = Convert.ToInt32(MessageAutoId);
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            BL_NotificationMessage.deleteMessage(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
