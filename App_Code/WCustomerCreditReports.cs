﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLL_CustomerCreditReport;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for WCustomerCreditReports
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WCustomerCreditReports : System.Web.Services.WebService {

    public WCustomerCreditReports () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string BindCustomer()
    {
        try
        {
            PL_CustomerCreditReport pobj = new PL_CustomerCreditReport();
            BL_CustomerCreditReport.BindCustomer(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod(EnableSession = true)]
    public string GetCustomerCreditReport(string dataValue)
    {
        try
        {
            PL_CustomerCreditReport pobj = new PL_CustomerCreditReport();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            BL_CustomerCreditReport.GetCustomerCreditReport(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}
