﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllCustomerList;
using System.Web.Script.Serialization;
using DllUtility;
/// <summary>
/// Summary description for WBindSalesPersonReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class CustomerStatementReport : System.Web.Services.WebService
{

    public CustomerStatementReport()
    {

        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string BindCustomerList()
    {
        PL_CustomerList pobj = new PL_CustomerList();
        BL_CustomerList.BindCustomerList(pobj);
        return pobj.Ds.GetXml();
    }
    [WebMethod(EnableSession = true)]
    public string BindCustomerStatementReport(string dataValue)
    {
        PL_CustomerList pobj = new PL_CustomerList();
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        pobj.CustomerList = jdv["CustomerList"].ToString();
        pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
        pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
        if (jdv["FromDate"] != "")
            pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
        if (jdv["ToDate"] != "")
            pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
        BL_CustomerList.SelectCustomerStatementReport(pobj);
        return pobj.Ds.GetXml();
    }
   
   

}
