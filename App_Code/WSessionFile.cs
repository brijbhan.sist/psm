﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for WSessionFile
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WSessionFile : System.Web.Services.WebService
{

    public WSessionFile()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string SessionFile()
    {
        if (Session["UserName"] == null || Session["EmpFirstName"] == null || Session["EmpAutoId"] == null || Session["EmpType"] == null || Session["EmpTypeNo"] == null)
        {
            return "Session Expired";
        }
        else
        {
            Session["UserName"] = Session["UserName"];
            Session["EmpFirstName"] = Session["EmpFirstName"];
            Session["EmpAutoId"] = Session["EmpAutoId"];
            Session["EmpType"] = Session["EmpType"];
            Session["EmpTypeNo"] = Session["EmpTypeNo"];
            return "true";
        }

    }

}
