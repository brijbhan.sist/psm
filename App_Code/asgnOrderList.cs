﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllOrderMaster;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class asgnOrderList : System.Web.Services.WebService
{

    [WebMethod(EnableSession = true)]
    public string bindDriver()
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            BL_OrderMaster.bindDriver(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {

            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string getAssignedOrdersList(string searchValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(searchValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);

            if (pobj.LoginEmpType == 5)
            {
                pobj.DrvAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            }
            else if (pobj.LoginEmpType == 4 || pobj.LoginEmpType == 7 || pobj.LoginEmpType == 8)
            {
                pobj.DrvAutoId = Convert.ToInt32(jdv["DrvAutoId"]);
                pobj.PageIndex = jdv["PageIndex"];
            }

            if (jdv["FromDate"] != null && jdv["FromDate"] != "")
            {
                pobj.Fromdate = Convert.ToDateTime(jdv["FromDate"]);
            }
            if (jdv["ToDate"] != null && jdv["ToDate"] != "")
            {
                pobj.Todate = Convert.ToDateTime(jdv["ToDate"]);
            }

            BL_OrderMaster.getAssignedOrdersList(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {

            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string getDrvAsgnOrder(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);

            if (pobj.LoginEmpType == 5)
            {
                pobj.DrvAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            }
            else if (pobj.LoginEmpType == 4 || pobj.LoginEmpType == 7 || pobj.LoginEmpType == 8)
            {
                pobj.DrvAutoId = Convert.ToInt32(jdv["DrvAutoId"]);
            }

            pobj.AsgnDate = Convert.ToDateTime(jdv["AsgnDate"]);

            BL_OrderMaster.getDrvAsgnOrder(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string saveStoppage(string dataValues, string orders)
    {
        DataTable Dt = new DataTable();
        Dt = JsonConvert.DeserializeObject<DataTable>(orders);
        PL_OrderMaster pobj = new PL_OrderMaster();
        if (Dt.Rows.Count > 0)
        {
            pobj.AsgnOrder = Dt;
        }
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        try
        {
            pobj.DrvAutoId = Convert.ToInt32(jdv["DrvAutoId"]);
            pobj.AsgnDate = Convert.ToDateTime(jdv["AsgnDate"]);
            pobj.Root = jdv["Root"];

            BL_OrderMaster.saveStoppage(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string PrintReadytoShipOrder(string DriverAutoId, string AssignDate)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();

        pobj.DrvAutoId = Convert.ToInt32(DriverAutoId);
        pobj.AsgnDate = Convert.ToDateTime(AssignDate);
        BL_OrderMaster.PrintReadytoShipOrder(pobj);
        return pobj.Ds.GetXml();
    }

    //------------------------------------------------------------------------

    [WebMethod(EnableSession = true)]
    public string getDriverList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (jdv["date"] != null)
                {
                    pobj.AsgnDate = Convert.ToDateTime(jdv["date"]);
                }
                pobj.DrvAutoId = Convert.ToInt32(jdv["DriverAutoId"]);

                BL_OrderMaster.getDriverListToChange(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod(EnableSession = true)]
    public string AssignDriver(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.DrvAutoId = Convert.ToInt32(jdv["DriverAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.DraftAutoId = Convert.ToInt32(jdv["OldDriverAutoId"]);
                if (jdv["AssignDate"] != "")
                {
                    pobj.AsgnDate = Convert.ToDateTime(jdv["AssignDate"]);
                }
               
                BL_OrderMaster.AssignDriverChange(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string DriverPackagePrintOption(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            pobj.DrvAutoId = Convert.ToInt32(jdv["DrvAutoId"]);
            pobj.AsgnDate = Convert.ToDateTime(jdv["AsgnDate"]);
            BL_OrderMaster.DriverPrintOption(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}


