﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLNotShippedReportbyProduct;
using System.Web.Script.Serialization;
using DllUtility;
/// <summary>
/// Summary description for WNotShippedReportbyProduct
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WNotShippedReportbyProduct : System.Web.Services.WebService
{

    public WNotShippedReportbyProduct()
    {
        gZipCompression.fn_gZipCompression();
    }

     [WebMethod(EnableSession = true)]
    public string BindStatussalesperson()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {

                PL_NotShippedReportbyProduct pobj = new PL_NotShippedReportbyProduct();
                BL_NotShippedReportbyProduct.BindStatussalesperson(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                { 
                    return "false";
                }
            }
            catch 
            { 
                return "false";
            }
        }
        else
        {
            return "Session Timeout";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getNotShippedReport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_NotShippedReportbyProduct pobj = new PL_NotShippedReportbyProduct();
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.SortBy = Convert.ToInt32(jdv["SortBy"]);
                pobj.SortOrder = Convert.ToInt32(jdv["SortOrder"]);
                pobj.SalsePersonAutoid = Convert.ToInt32(jdv["SalespersonAutoId"]);
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_NotShippedReportbyProduct.BindNotShipedReport(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch 
            {
                return "false";
            }
        }
        else
        {
            return "Session Timeout";
        }
    }


    [WebMethod(EnableSession = true)]
    public string getNotShipedReportExport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_NotShippedReportbyProduct pobj = new PL_NotShippedReportbyProduct();

                pobj.Status = Convert.ToInt32(jdv["Status"]);
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.SalsePersonAutoid = Convert.ToInt32(jdv["SalespersonAutoId"]);

                BL_NotShippedReportbyProduct.NotShipedReportExport(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Timeout";
        }
    }

}
