﻿using DLLAllowQtyInPieces;
using DllUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for AllowQtyInPiece
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class AllowQtyInPiece : System.Web.Services.WebService
{

    public AllowQtyInPiece()
    {
        gZipCompression.fn_gZipCompression();
    }
    

    [WebMethod(EnableSession = true)]
    public string InsertAndUpdate(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_AllowQtyInPieces pobj = new PL_AllowQtyInPieces();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (jdv["AutoId"] != null && jdv["AutoId"] != "")
                {
                    pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                }
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonId"]);
                pobj.AllowDate = Convert.ToDateTime(jdv["AllowDate"]);
                pobj.AllowQtyInPieces = Convert.ToInt32(jdv["AllowQty"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductId"]);
                pobj.EmpId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_AllowQtyInPieces.InsertAndUpdate(pobj);
                if (!pobj.isException)
                {
                    return pobj.exceptionMessage; ;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindProduct()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_AllowQtyInPieces pobj = new PL_AllowQtyInPieces();
                BL_AllowQtyInPieces.BindProduct(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getList(string dataValues)
    {

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_AllowQtyInPieces pobj = new PL_AllowQtyInPieces();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                if (jdv["SalesPersonAutoId"] != "0")
                {
                    pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                }
                if (jdv["ProductId"] != "0")
                {
                    pobj.ProductAutoId = Convert.ToInt32(jdv["ProductId"]);
                }

                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageIndex = jdv["pageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_AllowQtyInPieces.getList(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetProductUnit(string ProductId)
    {

        PL_AllowQtyInPieces pobj = new PL_AllowQtyInPieces();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.ProductAutoId = Convert.ToInt32(ProductId);
                BL_AllowQtyInPieces.getProductUnit(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string Delete(string AutoId)
    {

        PL_AllowQtyInPieces pobj = new PL_AllowQtyInPieces();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.AutoId = Convert.ToInt32(AutoId);
                BL_AllowQtyInPieces.Delete(pobj);
                return pobj.exceptionMessage;
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetEditData(string AutoId)
    {

        PL_AllowQtyInPieces pobj = new PL_AllowQtyInPieces();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.AutoId = Convert.ToInt32(AutoId);
                BL_AllowQtyInPieces.getData(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}
