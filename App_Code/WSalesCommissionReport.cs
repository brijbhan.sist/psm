﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllUtility;
using DLLSalesCommission;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for WSalesCommissionReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WSalesCommissionReport : System.Web.Services.WebService
{

    public WSalesCommissionReport()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string BindSalesPerson()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SalesCommission pobj = new PL_SalesCommission();
                BL_SalesCommission.BindSalesPerson(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetReportDetail(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_SalesCommission pobj = new PL_SalesCommission();
                pobj.SalesPerson = Convert.ToInt32(jdv["SalesAutoId"]);
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_SalesCommission.BindSaleReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
