﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllProductBulkUpload;
using System.Data.OleDb;
using System.Data;
using DllUtility;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ProductBulkUpload : System.Web.Services.WebService {
    public ProductBulkUpload() {
        gZipCompression.fn_gZipCompression();
    }
    [WebMethod(EnableSession=true)]
    public string BulkUpload(string FileName)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_ProductBulkUpload obj = new PL_ProductBulkUpload();
            int i = FileName.LastIndexOf('.');
            string fileExtention = FileName.Substring(i);
            //if (fileExtention == ".xls" || fileExtention == ".xlsx")
            //if (fileExtention == ".xls")
            //{
                string Message = Insert(FileName, fileExtention);
                if (Message == "false")
                {
                    BL_ProductBulkUpload.selectExcelData(obj);
                    if (!obj.isException)
                    {
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.StatusCode = 200;
                        return obj.Ds.GetXml();     
                    }
                    else 
                    {
                        return obj.exceptionMessage;
                    }                                   
                }
                else
                {
                    return Message;
                }
            //}
            //else
            //{
            //    return "Invalid Extension";
            //}
        }
        else 
        {
            return "Session Expired";
        }
    }
    
    public string Insert(string FileName, string fileExtention)
    {
        PL_ProductBulkUpload obj = new PL_ProductBulkUpload();
        string connectionString = "";
        string FilePath = Server.MapPath("~/Attachments/" + FileName);
        if (fileExtention == ".xlsx")
            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\"";
        if (fileExtention == ".xls")
            connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FilePath + ";Extended Properties=Excel 8.0;";
        OleDbConnection oconn = new OleDbConnection(@connectionString);

        try
        {
            OleDbCommand ocmd = new OleDbCommand("select * from [" + GetSheetsNames(@connectionString) + "$]", oconn);
            OleDbDataAdapter da = new OleDbDataAdapter(ocmd);
            DataSet dt = new DataSet();
            da.Fill(dt);
            obj.TableValue = new DataTable();
            obj.TableValue.Columns.Add("ProductId", typeof(string));
            obj.TableValue.Columns.Add("ProductName", typeof(string));
            obj.TableValue.Columns.Add("Category", typeof(string));
            obj.TableValue.Columns.Add("Subcategory", typeof(string));
            obj.TableValue.Columns.Add("PreferVendor", typeof(string));
            obj.TableValue.Columns.Add("ReOrderMarkBox", typeof(string)); 
            obj.TableValue.Columns.Add("P_Qty", typeof(string)); 
            obj.TableValue.Columns.Add("P_MinPrice", typeof(string));
            obj.TableValue.Columns.Add("P_WholeSale", typeof(string));
            obj.TableValue.Columns.Add("P_BasePrice", typeof(string));
            obj.TableValue.Columns.Add("P_CostPrice", typeof(string));
            obj.TableValue.Columns.Add("P_SRP", typeof(string));
            obj.TableValue.Columns.Add("P_CommCode", typeof(string));
            obj.TableValue.Columns.Add("P_Location", typeof(string));
            obj.TableValue.Columns.Add("P_Barcode", typeof(string));
            obj.TableValue.Columns.Add("B_Qty", typeof(string));
            obj.TableValue.Columns.Add("B_MinPrice", typeof(string));
            obj.TableValue.Columns.Add("B_WholeSale", typeof(string));
            obj.TableValue.Columns.Add("B_BasePrice", typeof(string));
            obj.TableValue.Columns.Add("B_CostPrice", typeof(string));
            obj.TableValue.Columns.Add("B_SRP", typeof(string));
            obj.TableValue.Columns.Add("B_CommCode", typeof(string));
            obj.TableValue.Columns.Add("B_Location", typeof(string));
            obj.TableValue.Columns.Add("B_Barcode", typeof(string));
            obj.TableValue.Columns.Add("C_Qty", typeof(string));
            obj.TableValue.Columns.Add("C_MinPrice", typeof(string));
            obj.TableValue.Columns.Add("C_WholeSale", typeof(string));
            obj.TableValue.Columns.Add("C_BasePrice", typeof(string));
            obj.TableValue.Columns.Add("C_CostPrice", typeof(string));
            obj.TableValue.Columns.Add("C_SRP", typeof(string));
            obj.TableValue.Columns.Add("C_CommCode", typeof(string));
            obj.TableValue.Columns.Add("C_Location", typeof(string));
            obj.TableValue.Columns.Add("C_Barcode", typeof(string));
            obj.TableValue.Columns.Add("D_Selling", typeof(string));

            foreach (DataRow row in dt.Tables[0].Rows)
            {
                // Here we add five DataRows.
                obj.TableValue.Rows.Add(
                Convert.ToString(row[1]),
                Convert.ToString(row[2]),
                Convert.ToString(row[3]),
                Convert.ToString(row[4]),
                Convert.ToString(row[5]),
                Convert.ToString(row[6]),
                Convert.ToString(row[8]),
                Convert.ToString(row[9]),
                Convert.ToString(row[10]),
                Convert.ToString(row[11]),
                Convert.ToString(row[12]),
                Convert.ToString(row[13]),
                Convert.ToString(row[14]),            
                Convert.ToString(row[15]),
                Convert.ToString(row[16]),
                Convert.ToString(row[18]),
                Convert.ToString(row[19]),
                Convert.ToString(row[20]),
                Convert.ToString(row[21]),
                Convert.ToString(row[22]),                
                Convert.ToString(row[23]),
                Convert.ToString(row[24]),
                Convert.ToString(row[25]),                
                Convert.ToString(row[26]),
                Convert.ToString(row[28]),  
                Convert.ToString(row[29]),
                Convert.ToString(row[30]),               
                Convert.ToString(row[31]),
                Convert.ToString(row[32]),
                Convert.ToString(row[33]),              
                Convert.ToString(row[34]),
                Convert.ToString(row[35]),
                Convert.ToString(row[36]),
                Convert.ToString(row[37])                
                );            
            }
            BL_ProductBulkUpload.insertExcelData(obj);
            if (!obj.isException)
            {
                return "false";
            }
            else 
            {
                return obj.exceptionMessage;
            }            
        }
        catch (Exception ex)
        {
            return ex.Message;
        }        
    }

    public static string GetSheetsNames(string connectionString)
    {
        try
        {            
            OleDbConnection connection = new OleDbConnection(connectionString);           
            connection.Open();
            DataTable tbl = connection.GetSchema("Tables");
            connection.Close();
            foreach (DataRow row in tbl.Rows)
            {
                string sheetName = Convert.ToString(row["TABLE_NAME"]);
                if (sheetName.EndsWith("$"))
                {
                    sheetName = sheetName.Substring(0, sheetName.Length - 1);
                    return sheetName;                   
                }
                if (sheetName.EndsWith("'"))
                {
                    sheetName = sheetName.Substring(0, sheetName.Length - 2);
                    return sheetName;
                }
            }
            return "Sheet1";
        }
        catch (Exception)
        {
            return "Sheet1";
        }
    }

    [WebMethod(EnableSession = true)]
    public string FinalUpload()
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_ProductBulkUpload obj = new PL_ProductBulkUpload();
            BL_ProductBulkUpload.FinalUpload(obj);
            if (!obj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return "success";
            }
            else
            {
                return obj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
