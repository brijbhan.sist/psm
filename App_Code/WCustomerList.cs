﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;
using DllCustomerList;
using DllUtility;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WCustomerList : System.Web.Services.WebService
{
    public WCustomerList()
    {
        gZipCompression.fn_gZipCompression();
    }
    [WebMethod(EnableSession = true)]
    public string bindDropDowns()
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CustomerList pobj = new PL_CustomerList();

            BL_CustomerList.bindDropDowns(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getCustomerList(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_CustomerList pobj = new PL_CustomerList();
            try
            {
                pobj.CustomerId = jdv["CustomerId"];
                pobj.CustomerName = jdv["CustomerName"];
                pobj.PageIndex = jdv["pageIndex"];

                if (Session["EmpType"].ToString() != "Admin" && Session["EmpType"].ToString() != "Account")
                {
                    pobj.SalesPersonAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                }
                else
                {
                    pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                }

                BL_CustomerList.select(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getNewCustomerList(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_CustomerList pobj = new PL_CustomerList();
            try
            {
                pobj.CustomerId = jdv["CustomerId"];
                pobj.CustomerName = jdv["CustomerName"];
                pobj.CustemerTypeAutoId = Convert.ToInt32(jdv["CustomerType"]);
                pobj.StateAutoId = Convert.ToInt32(jdv["StateName"]);
                pobj.CityAutoId = Convert.ToInt32(jdv["CityName"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.OrderNo = jdv["OrderNo"];
                pobj.PageIndex = jdv["pageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

                if (Session["EmpType"].ToString() == "Sales Person")
                {
                    pobj.SalesPersonAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                }
                else
                {
                    pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                }

                BL_CustomerList.Newselect(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string StateChagedBindCity(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_CustomerList pobj = new PL_CustomerList();
            try
            {
                pobj.StateAutoId = Convert.ToInt32(jdv["StateName"]);

                BL_CustomerList.StateChagedBindCity(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteCustomer(int AutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CustomerList pobj = new PL_CustomerList();
            try
            {
                pobj.CustomerAutoId = AutoId;
                BL_CustomerList.deleteCustomer(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}




