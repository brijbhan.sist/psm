﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllStockEntryMaster;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class stockEntry : System.Web.Services.WebService
{
    [WebMethod(EnableSession = true)]
    public string bindProduct()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_StockEntryMaster pobj = new PL_StockEntryMaster();
            try
            {
                BL_StockEntryMaster.bindProduct(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpire";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindUnitType(int productAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {

            PL_StockEntryMaster pobj = new PL_StockEntryMaster();
            try
            {
                pobj.ProductAutoId = productAutoId;
                BL_StockEntryMaster.bindUnitType(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpire";
        }
    }

    [WebMethod(EnableSession = true)]
    public string insertStockEntry(string TableValues, string billData)
    {
        if (Session["EmpTypeNo"] != null)
        {
            DataTable dtBill = new DataTable();
            dtBill = JsonConvert.DeserializeObject<DataTable>(TableValues);
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(billData);
            PL_StockEntryMaster pobj = new PL_StockEntryMaster();
            try
            {
                if (dtBill.Rows.Count > 0)
                {
                    pobj.TableValue = dtBill;
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                pobj.BillNo = jdv["billNo"];
                if (jdv["billDate"] != null && jdv["billDate"] != "")
                {
                    //DateTime dt = DateTime.ParseExact(jdv["billDate"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    pobj.BillDate = Convert.ToDateTime(jdv["billDate"]);
                }
                pobj.Remarks = (jdv["remarks"]);
                BL_StockEntryMaster.insert(pobj);
                if (!pobj.isException)
                {                   
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception e)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpire";
        }
    }

    [WebMethod(EnableSession = true)]
    public string editStockEntry(int BillAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_StockEntryMaster pobj = new PL_StockEntryMaster();
            try
            {
                pobj.BillAutoId = BillAutoId;
                BL_StockEntryMaster.editStockEntry(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpire";
        }
    }

    [WebMethod(EnableSession = true)]
    public string updateStockEntry(string TableValues, string billData)
    {
        if (Session["EmpTypeNo"] != null)
        {
            DataTable dtQty = new DataTable();
            dtQty = JsonConvert.DeserializeObject<DataTable>(TableValues);
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(billData);
            PL_StockEntryMaster pobj = new PL_StockEntryMaster();
            try
            {
                if (dtQty.Rows.Count > 0)
                {
                    pobj.TableValue = dtQty;
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.BillAutoId = Convert.ToInt32(jdv["billAutoId"]);
                pobj.BillNo = jdv["billNo"];
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                if (jdv["billDate"] != null && jdv["billDate"] != "")
                {
                    pobj.BillDate = Convert.ToDateTime(jdv["billDate"]);
                }
                pobj.Remarks = (jdv["remarks"]);

                BL_StockEntryMaster.update(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpire";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getProductThruBarcode(string barcode)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_StockEntryMaster pobj = new PL_StockEntryMaster();
            try
            {
                pobj.Barcode = barcode;
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_StockEntryMaster.getProductThruBarcode(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpire";
        }
    }

    [WebMethod(EnableSession = true)]
    public string assignBarcode(string dataValues)
    {

        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            PL_StockEntryMaster pobj = new PL_StockEntryMaster();
            try
            {
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.Barcode = jdv["Barcode"];
                BL_StockEntryMaster.assignBarcode(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpire";
        }
    }

    [WebMethod(EnableSession = true)]
    public string countBarcode(string dataValues)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            PL_StockEntryMaster pobj = new PL_StockEntryMaster();
            try
            {
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);

                BL_StockEntryMaster.countBarcode(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpire";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetProductDetails(string dataValues)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            PL_StockEntryMaster pobj = new PL_StockEntryMaster();
            try
            {
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);

                BL_StockEntryMaster.GetProductDetails(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpire";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateBasePrice(string dataValues)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            PL_StockEntryMaster pobj = new PL_StockEntryMaster();
            try
            {
                pobj.ItemAutoId = Convert.ToInt32(jdv["ItemAutoId"]);
                if (jdv["BasePrice"] != "")
                {
                    pobj.BasePrice = Convert.ToDecimal(jdv["BasePrice"]);
                }
                if (jdv["RetailMIN"] != "")
                {
                    pobj.RetailMIN = Convert.ToDecimal(jdv["RetailMIN"]);
                }
                if (jdv["CostPrice"] != "")
                {
                    pobj.CostPrice = Convert.ToDecimal(jdv["CostPrice"]);
                }
                if (jdv["SRP"] != "")
                {
                    pobj.SRP = Convert.ToDecimal(jdv["SRP"]);
                }
                if (jdv["WholesaleMinPrice"] != "")
                {
                    pobj.WholesaleMinPrice = Convert.ToDecimal(jdv["WholesaleMinPrice"]);
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_StockEntryMaster.updateBasePrice(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    if (pobj.exceptionMessage == "EXISTS")
                    {
                        return pobj.Ds.GetXml();
                    }
                    else
                    {
                        return "false";
                    }

                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpire";
        }
    }
}
