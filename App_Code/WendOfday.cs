﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using DLLEndOfDay;
using DllUtility;

/// <summary>
/// Summary description for WendOfday
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WendOfday : System.Web.Services.WebService
{

    public WendOfday()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string BindSalesPerson()
    {
        PL_EndOfDay pobj = new PL_EndOfDay();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_EndOfDay.bindSalesPerson(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetList(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_EndOfDay pobj = new PL_EndOfDay();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["Index"]);
                BL_EndOfDay.getList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string InsertRecord(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_EndOfDay pobj = new PL_EndOfDay();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                if (jdv["EndDate"] != null && jdv["EndDate"] != "")
                {
                    pobj.EndDate = Convert.ToDateTime(jdv["EndDate"]);
                }
                if (jdv["DateTime"] != null && jdv["DateTime"] != "")
                {
                    pobj.EndTime = Convert.ToDateTime(jdv["DateTime"]);
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                BL_EndOfDay.InsertRecord(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

}
