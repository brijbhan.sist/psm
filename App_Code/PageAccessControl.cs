﻿using DllLogin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for PageAccessControl
/// </summary>
public class PageAccessControl
{
	public PageAccessControl()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataSet AccessControl(string PageUrl, int EmpType)
    {
        PL_Login pobj = new PL_Login();
        pobj.PageUrl = PageUrl;

        pobj.EmpType = EmpType;        
        BL_Login.getPageAction(pobj);
        return pobj.Ds;
    }
    public void usercontrol(DataSet ds, HtmlControl btn)
    {
        btn.Visible = false;
        String strCommaSepList = ds.Tables[0].Rows[0]["Action"].ToString();
        String[] strCommanSepArray = strCommaSepList.Split(',');

        foreach (String m in strCommanSepArray)
        {
            if (m == btn.ID.ToString())
            {
                btn.Visible = true;
            }
        }
    }
}