﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLL_TopSellingReport;
using System.Web.Script.Serialization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class TopSellingReport : System.Web.Services.WebService {




    [WebMethod(EnableSession = true)]
    public string BindSalesPerson()
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_TopSaleReport pobj = new PL_TopSaleReport();
            BL_TopSaleReport.BindSalesPerson(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string TopsellingReport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_TopSaleReport pobj = new PL_TopSaleReport();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.SalesPersons = jdv["SalesPersons"];
                pobj.CustomerType = Convert.ToInt32(jdv["CustomerType"]);
                pobj.Type = Convert.ToInt32(jdv["Type"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                BL_TopSaleReport.BindTopSellingReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
