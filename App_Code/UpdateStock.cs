﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllUpdateStock;
using System.Web.Script.Serialization;
using DllUtility;

/// <summary>
/// Summary description for UpdateStock
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class UpdateStock : System.Web.Services.WebService
{

    public UpdateStock()
    {

        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string StockList()
    {
        if (Session["EmpAutoId"] != null)
        {
            Pl_UpdateStock pobj = new Pl_UpdateStock();
            Bl_UpdateStock.bindStock(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string StockQty(int productAutoId, int PageIndex)
    {
        if (Session["EmpAutoId"] != null)
        {
            Pl_UpdateStock pobj = new Pl_UpdateStock();
            pobj.productId = productAutoId;
            pobj.PageIndex = PageIndex;
            Bl_UpdateStock.selectProductId(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string Update(string datavalue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(datavalue);
            Pl_UpdateStock pobj = new Pl_UpdateStock();
            try
            {
                pobj.UserAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.productId = Convert.ToInt32(jdv["ProductId"]);
                pobj.Quantity = Convert.ToInt32(jdv["Qty"]);

                Bl_UpdateStock.updateStock(pobj);
                if (!pobj.isException)
                {

                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "fail";
                }
            }
            catch (Exception ex)
            {
                return "fail";
            }
        }
        else
        {
            return "Session Expired";
        }

    }
    [WebMethod(EnableSession = true)]
    public string GetBarDetails(string Barcode)
    {
        if (Session["EmpAutoId"] != null)
        {
            Pl_UpdateStock pobj = new Pl_UpdateStock();
            pobj.Barcode = Barcode;
            Bl_UpdateStock.GetBarDetails(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
}


