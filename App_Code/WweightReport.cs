﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLWeightReport;
using System.Web.Script.Serialization;
using DllUtility;

/// <summary>
/// Summary description for WweightReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WweightReport : System.Web.Services.WebService {

    public WweightReport () {

        gZipCompression.fn_gZipCompression();
    }  
    [WebMethod(EnableSession = true)]
    public string GetBrand()
    {
        if (Session["EmpAutoId"] != null)
        {                 
            PL_WeightReport pobj = new PL_WeightReport();          
            BL_WeightReport.GetBrand(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetProductOnChange(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {           
            PL_WeightReport pobj = new PL_WeightReport();
            pobj.Brand = dataValue;
            BL_WeightReport.GetProductOnChange(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetReport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_WeightReport pobj = new PL_WeightReport();
            pobj.Brand = jdv["BrandName"];
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);          
            pobj.State = Convert.ToInt32(jdv["StateAutoId"]);
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

            BL_WeightReport.GetWeightReport(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string WeightReportExport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_WeightReport pobj = new PL_WeightReport();
            pobj.Brand = jdv["BrandName"];
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
            pobj.State = Convert.ToInt32(jdv["StateAutoId"]);
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            //pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
           // pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

            BL_WeightReport.ExportWeightReport(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }    
    
}
