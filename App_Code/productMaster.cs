﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllProduct;
using System.Web.Script.Serialization;
using DllUtility;
using System.Data;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.IO;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class productMaster : System.Web.Services.WebService
{
    string Body = "";
    public productMaster()
    {
        gZipCompression.fn_gZipCompression();
    }
    [WebMethod(EnableSession = true)]
    public string bindCategory()
    {
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_Product.bindCategory(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindSubcategory(string categoryAutoId)
    {
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CategoryAutoId = Convert.ToInt32(categoryAutoId);
                BL_Product.bindSubcategory(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string insertProduct(string dataValue, string Locationstatus)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        DataTable dtLocationstatus = new DataTable();
        dtLocationstatus = JsonConvert.DeserializeObject<DataTable>(Locationstatus);
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.ProductName = jdv["ProductName"];
                pobj.BrandAutoId = Convert.ToInt32(jdv["BrandAutoId"]);
                pobj.IsAppyMLQty = Convert.ToInt32(jdv["IsApplyMlQty"]);
                pobj.IsAppyWeightQty = Convert.ToInt32(jdv["IsApplyWeightQty"]);
                pobj.ProductId = jdv["ProductId"];
                pobj.ImageUrl = "/productThumb1000/1000_1000_" + (jdv["ThumbnailImageUrl1000"]);
                pobj.ThumbnailImageUrl = "/productThumbnailImage/100_100_" + (jdv["ThumbnailImageUrl"]);
                pobj.FourImageUrl = "/productThumbnailImage/400_400_" + (jdv["ThumbnailImageUrl"]);
                pobj.OriginalImageUrl = "/Attachments/" + (jdv["ImageUrl"]);
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                pobj.WeightOz = Convert.ToDecimal(jdv["Weight"]);
                if (jdv["ReOrderMark"] != "")
                    pobj.ReOrderMark = Convert.ToInt32(jdv["ReOrderMark"]);
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.EmpAutoId = Session["EmpAutoId"].ToString();
                if (jdv["CommCode"] != "")
                {
                    pobj.CommCode = Convert.ToDecimal(jdv["CommCode"]);
                }
                if (jdv["SRP"] != "")
                {
                    pobj.SRP = Convert.ToDecimal(jdv["SRP"]);
                }
                pobj.LocationStatus = dtLocationstatus;
                string[] GtUrl = HttpContext.Current.Request.Url.Host.ToString().Split('.');
                string dirFullPath = "";
                dirFullPath = HttpContext.Current.Server.MapPath("~/Attachments/" + jdv["ImageUrl"]);
                if (jdv["ImageUrl"] == "default_pic.png")
                {
                    return "ImageIssue";
                }
                else
                {
                    if (File.Exists(dirFullPath))
                    {
                        BL_Product.insert(pobj);
                        if (!pobj.isException)
                        {

                            return pobj.Ds.GetXml();
                        }
                        else
                        {
                            return pobj.exceptionMessage;
                        }
                    }
                    else
                    {
                        return "ImageIssue";
                    }
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateProduct(string dataValue, string Locationstatus)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        DataTable dtLocationstatus = new DataTable();
        dtLocationstatus = JsonConvert.DeserializeObject<DataTable>(Locationstatus);
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.IsAppyMLQty = Convert.ToInt32(jdv["IsApplyMlQty"]);
                pobj.IsAppyWeightQty = Convert.ToInt32(jdv["IsApplyWeightQty"]);
                if (jdv["BrandAutoId"] == null)
                {
                    return "Brand Required";
                }
                pobj.BrandAutoId = Convert.ToInt32(jdv["BrandAutoId"]);
                pobj.ProductId = jdv["ProductId"];
                pobj.ProductName = jdv["ProductName"];
                pobj.ImageUrl = "/productThumb1000/1000_1000_" + (jdv["ThumbnailImageUrl1000"]);
                pobj.ThumbnailImageUrl = "/productThumbnailImage/100_100_" + (jdv["ThumbnailImageUrl"]);
                pobj.FourImageUrl = "/productThumbnailImage/400_400_" + (jdv["ThumbnailImageUrl"]);
                pobj.OriginalImageUrl = "/Attachments/" + (jdv["ImageUrl"]);
                if (jdv["CategoryAutoId"] == null)
                {
                    return "Category Required";
                }
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                if (jdv["SubcategoryAutoId"] == null)
                {
                    return "Subcategory Required";
                }
                pobj.SubcategoryAutoId = Convert.ToInt32(jdv["SubcategoryAutoId"]);
                if (jdv["VendorAutoId"] == null)
                {
                    return "Vendor Required";
                }
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                if (jdv["ReOrderMark"] != "")
                    pobj.ReOrderMark = Convert.ToInt32(jdv["ReOrderMark"]);
                if(jdv["QtyPerPiece"]!="")
                    pobj.Qty = Convert.ToInt32(jdv["QtyPerPiece"]);
                if (jdv["MLQty"] != "")
                    pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                if (jdv["WeightOz"] != "")
                    pobj.WeightOz = Convert.ToDecimal(jdv["WeightOz"]);
                pobj.EmpAutoId = Session["EmpAutoId"].ToString();
                if (jdv["CommCode"] != "")
                {
                    pobj.CommCode = Convert.ToDecimal(jdv["CommCode"]);
                }
                if (jdv["SRP"] != "")
                {
                    pobj.SRP = Convert.ToDecimal(jdv["SRP"]);
                }
                pobj.LocationStatus = dtLocationstatus;
                string[] GtUrl = HttpContext.Current.Request.Url.Host.ToString().Split('.');
                string dirFullPath = "";
                dirFullPath = HttpContext.Current.Server.MapPath("~/Attachments/" + jdv["ImageUrl"]);
                if (!File.Exists(dirFullPath) && HttpContext.Current.Session["Location"].ToString().ToLower() == "psmnj")
                {
                    return "ImageIssue";
                }
                else
                {
                    BL_Product.updateProduct(pobj);
                    if (!pobj.isException)
                    {
                        return "Success";
                    }
                    else
                    {
                        return pobj.exceptionMessage;
                    }
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string insertPackingDetails(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.UnitType = Convert.ToInt32(jdv["UnitType"]);
                pobj.Qty = Convert.ToInt32(jdv["UnitQty"]);
                pobj.MinPrice = Convert.ToDecimal(jdv["MinPrice"]);
                pobj.CostPrice = Convert.ToDecimal(jdv["CostPrice"]);
                pobj.ProductLocation = jdv["ProductLocation"];
                pobj.EligibleforFree = Convert.ToInt32(jdv["EligibleforFree"]);
                if (jdv["WHminPrice"] != "")
                    pobj.WHminPrice = Convert.ToDecimal(jdv["WHminPrice"]);
                pobj.Price = Convert.ToDecimal(jdv["Price"]);
                pobj.PreDefinedBarcode = jdv["PreDefinedBarcode"];
                pobj.EmpAutoId = Session["EmpAutoId"].ToString();
                pobj.AutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                BL_Product.insertPacking(pobj);
                if (!pobj.isException)
                {
                    getMailBody(pobj.Ds, "3");
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindUnitType()
    {
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_Product.bindUnitType(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getPackingDetails(string ProductAutoId)
    {
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(ProductAutoId);
                BL_Product.selectPackingDetails(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string editProduct(string ProductId)
    {
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(ProductId);
                BL_Product.editProduct(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string editPacking(int PackingDetailAutoId)
    {
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.PackingDetailAutoId = PackingDetailAutoId;
                BL_Product.editPacking(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updatePacking(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.UnitType = Convert.ToInt32(jdv["UnitType"]);
                pobj.Qty = Convert.ToInt32(jdv["UnitQty"]);
                pobj.MinPrice = Convert.ToDecimal(jdv["MinPrice"]);
                pobj.CostPrice = Convert.ToDecimal(jdv["CostPrice"]);
                pobj.Price = Convert.ToDecimal(jdv["Price"]);
                pobj.EligibleforFree = Convert.ToInt32(jdv["EligibleforFree"]);
                pobj.ProductLocation = jdv["ProductLocation"];
                pobj.EmpAutoId = Session["EmpAutoId"].ToString();
                if (jdv["WHminPrice"] != "")
                    pobj.WHminPrice = Convert.ToDecimal(jdv["WHminPrice"]);
                pobj.AutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.PackingDetailAutoId = Convert.ToInt32(jdv["PackingDetailAutoId"]);
                BL_Product.updatePacking(pobj);
                if (!pobj.isException)
                {
                    getMailBody(pobj.Ds, "4");
                    return "Success";
                }
                else
                {
                    if (pobj.exceptionMessage == "EXISTS")
                    {
                        return pobj.Ds.GetXml();
                    }
                    else
                    {
                        return "false";
                    }

                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string DefaultPacking(string PackingAutoId)
    {

        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(PackingAutoId);
                BL_Product.DefaultPacking(pobj);
                if (!pobj.isException)
                {
                    return "Success!!";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deletePacking(int PackingDetailAutoId)
    {
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.PackingDetailAutoId = PackingDetailAutoId;
                pobj.EmpAutoId = Session["EmpAutoId"].ToString();
                BL_Product.deletePacking(pobj);
                if (!pobj.isException)
                {
                    getMailBody(pobj.Ds, "5");
                    return "Success!!";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getBarcode(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.PackingDetailAutoId = Convert.ToInt32(jdv["PackingDetailAutoId"]);
                BL_Product.getBarcode(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateBarcode(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.BarcodeAutoId = Convert.ToInt32(jdv["barcodeAutoId"]);
                pobj.PreDefinedBarcode = jdv["barcode"];
                pobj.EmpAutoId = Session["EmpAutoId"].ToString();
                BL_Product.updateBarcode(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteBarcode(string barcodeAutoId)
    {
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.BarcodeAutoId = Convert.ToInt32(barcodeAutoId);
                pobj.EmpAutoId = Session["EmpAutoId"].ToString();
                BL_Product.deleteBarcode(pobj);
                if (!pobj.isException)
                {
                    getMailBody(pobj.Ds, "7");
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string saveBarcode(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.PackingDetailAutoId = Convert.ToInt32(jdv["PackingAutoId"]);
                pobj.AutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.PreDefinedBarcode = jdv["NewBarcode"];
                pobj.EmpAutoId = Session["EmpAutoId"].ToString();
                BL_Product.insertBarcode(pobj);
                if (!pobj.isException)
                {
                    getMailBody(pobj.Ds, "6");
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getManagePrice(string ProductAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_Product pobj = new PL_Product();
            pobj.AutoId = Convert.ToInt32(ProductAutoId); ;
            BL_Product.getUnitMaster(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updatebulk(string dataTable)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_Product pobj = new PL_Product();
                DataTable dtOrder = new DataTable();
                dtOrder = JsonConvert.DeserializeObject<DataTable>(dataTable);
                pobj.EmpAutoId = Session["EmpAutoId"].ToString();
                if (dtOrder.Rows.Count > 0)
                    pobj.dtbulkUnit = dtOrder;
                BL_Product.updatebulk(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {

                return ex.Message;//"Oops,some thing went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";

        }
    }
    public string getMailBody(DataSet Ds, string Type)
    {
        if (Session["EmpAutoId"] != null)
        {
            string OperationType = "", ReceiverEmailId = "", location = "";
            try
            {
                if (Ds != null && Ds.Tables.Count > 0)
                {
                    Body = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'>";
                    Body += "<head> <style>table{border-collapse:collapse;}td {padding: 3px 8px;}.text-center{text-align:center}";
                    Body += "table {width: 100%;}body {font-size: 14px;font-family: monospace;}";
                    Body += ".center{text-align: center;}.right {text-align: right;}thead > tr {background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a;}";
                    Body += "thead > tr > td {text-align: center !important;}.InvContent {width: 100%;}.tdLabel {font-weight: 700;}";
                    Body += "@media print {#btnPrint {display: none;}}legend {border-bottom: 1px solid #5f7c8a;}";
                    Body += ".tableCSS > tbody > tr > td,.tableCSS > thead > tr > td,.tableCSS > tfoot > tr > td {border: 1px solid #5f7c8a;}pre{border:none !important;}";
                    Body += "</style>";
                    Body += "</head>";
                    Body += "<body style='font-family: Arial; font-size: 12px'>";
                    if (Ds.Tables[0].Rows.Count > 0)
                    {
                        Body += "<table class='table tableCSS'>";
                        Body += "<tbody>";
                        Body += "<tr><td><b>Product Id</b></td><td>" + Ds.Tables[0].Rows[0]["Prdtid"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Product Name</b></td><td>" + Ds.Tables[0].Rows[0]["ProductName"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Product Image</b></td><td><img   onerror='this.src='/images/default_pic.png'' src=" + Ds.Tables[0].Rows[0]["ImageUrl"].ToString() + " alt='' style='width: 50px;height: 50px;'></td></tr>";
                        if (Type == "1" || Type == "2")
                        {
                            Body += "<tr><td><b>Category</b></td><td>" + Ds.Tables[0].Rows[0]["CategoryName"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Subcategory</b></td><td>" + Ds.Tables[0].Rows[0]["SubcategoryName"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Prefered Vendor</b></td><td>" + Ds.Tables[0].Rows[0]["VendorName"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Brand</b></td><td>" + Ds.Tables[0].Rows[0]["BrandName"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Re Order Mark (Pieces)</b></td><td>" + Ds.Tables[0].Rows[0]["ReOrderMark"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Is Apply ML Quantity</b></td><td>" + Ds.Tables[0].Rows[0]["IsApply_ML"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>ML Quantity</b></td><td>" + Ds.Tables[0].Rows[0]["MLQty"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Is Apply Weight (OZ)</b></td><td>" + Ds.Tables[0].Rows[0]["IsApply_Oz"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Weight (Oz)</b></td><td>" + Ds.Tables[0].Rows[0]["WeightOz"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Status</b></td><td>" + Ds.Tables[0].Rows[0]["ProductStatus"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Stock(Pieces)</b></td><td>" + Ds.Tables[0].Rows[0]["Stock"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Commission Code</b></td><td>" + Ds.Tables[0].Rows[0]["P_CommCode"].ToString() + "</td></tr>";
                        }
                        if (Type != "6" && Type != "7" && Type != "4" && Type != "5")
                        {
                            Body += "<tr><td><b>SRP</b></td><td>" + Ds.Tables[0].Rows[0]["SRP"].ToString() + "</td></tr>";
                        }
                        if (Type == "6")
                        {
                            Body += "<tr><td><b>Unit Type</b></td><td>" + Ds.Tables[2].Rows[0]["UnitType"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Barcode</b></td><td>" + Ds.Tables[0].Rows[0]["Barcode"].ToString() + "</td></tr>";
                        }
                        if (Type == "7")
                        {
                            Body += "<tr><td><b>Unit Type</b></td><td>" + Ds.Tables[0].Rows[0]["UnitName"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Barcode</b></td><td>" + Ds.Tables[0].Rows[0]["Barcode"].ToString() + "</td></tr>";
                        }
                        if (Type == "1" || Type == "3" || Type == "6")
                        {
                            Body += "<tr><td><b>Create Date & Time</b></td><td>" + Ds.Tables[0].Rows[0]["CreateDate"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Created By</b></td><td>" + Ds.Tables[0].Rows[0]["EmpName"].ToString() + "</td></tr>";
                        }
                        if (Type == "2" || Type == "4")
                        {
                            Body += "<tr><td><b>Update Date & Time</b></td><td>" + Ds.Tables[0].Rows[0]["UpdateDate"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Updated By</b></td><td>" + Ds.Tables[0].Rows[0]["EmpName"].ToString() + "</td></tr>";
                        }
                        if (Type == "5" || Type == "7")
                        {
                            Body += "<tr><td><b>Delete Date & Time</b></td><td>" + Ds.Tables[0].Rows[0]["DeleteDate"].ToString() + "</td></tr>";
                            Body += "<tr><td><b>Deleted By</b></td><td>" + Ds.Tables[0].Rows[0]["EmpName"].ToString() + "</td></tr>";
                        }
                        location = Ds.Tables[0].Rows[0]["Location"].ToString();
                    }
                    Body += "</tbody>";
                    Body += "</table>";
                    Body += "<br>";
                    #region
                    //Product Table
                    if (Type == "3" || Type == "4" || Type == "5")
                    {
                        Body += "<table class='table tableCSS'>";
                        if (Ds.Tables[1].Rows.Count != 0 && Ds.Tables[1].Rows.Count > 0)
                        {
                            int i = 0, j = 0;
                            int NoOfUnit = 1;// Convert.ToInt32(Ds.Tables[0].Rows[0]["TotalUnit"].ToString());
                            DataTable dt = null; DataSet ds1 = null;
                            for (i = 0; i <= NoOfUnit - 1; i++)
                            {

                                String UnitId = Ds.Tables[2].Rows[i]["UnitId"].ToString();
                                ds1 = Ds.Copy();
                                DataRow[] foundRows = null;
                                var expr = "";
                                if (UnitId == "1")
                                {
                                    expr = "UnitId = 1";
                                    foundRows = ds1.Tables[2].Select(expr);
                                }
                                else if (UnitId == "2")
                                {
                                    expr = "UnitId = 2";
                                    foundRows = ds1.Tables[2].Select(expr);
                                }
                                else if (UnitId == "3")
                                {
                                    expr = "UnitId = 3";
                                    foundRows = ds1.Tables[2].Select(expr);
                                }
                                Body += "<thead>";
                                Body += "<tr>";
                                Body += "<td><b>Unit Type</b></td><td><b>Quantity</b></td><td><b>Cost Price</b></td><td><b>Wholesale Minimum Price</b></td><td><b>Retail Minimum Price</b></td><td><b>Base Price</b></td><td><b>Free</b></td><td><b>Barcode</b></td>";
                                Body += "</tr>";
                                Body += "</thead>";
                                Body += "<tbody>";
                                Body += "<tr>";
                                Body += "<td style='text-align:center' rowspan='" + foundRows.Length + "'>" + foundRows[i]["UnitType"].ToString() + "</td>";
                                Body += "<td style='text-align:center' rowspan='" + foundRows.Length + "'>" + foundRows[i]["Qty"].ToString() + "</td>";
                                Body += "<td class='right'  rowspan='" + foundRows.Length + "'>" + foundRows[i]["CostPrice"].ToString() + "</td>";
                                Body += "<td class='right'  rowspan='" + foundRows.Length + "'>" + foundRows[i]["WHminPrice"].ToString() + "</td>";
                                Body += "<td class='right'  rowspan='" + foundRows.Length + "'>" + foundRows[i]["RetailMinPrice"].ToString() + "</td>";
                                Body += "<td class='right' rowspan='" + foundRows.Length + "'>" + foundRows[i]["Price"].ToString() + "</td>";
                                Body += "<td class='left' rowspan='" + foundRows.Length + "'>" + foundRows[i]["EligibleforFree"].ToString() + "</td>";
                                for (j = 0; j <= foundRows.Length - 1; j++)
                                {
                                    if (j == 0)
                                    {
                                        Body += "<td class='center'>" + foundRows[j]["Barcode"].ToString() + "</td></tr>";
                                    }
                                    else
                                    {
                                        Body += "<tr><td class='center'>" + foundRows[j]["Barcode"].ToString() + "</td></tr>";
                                    }
                                }
                            }
                        }
                        Body += "</tbody>";
                        Body += "</table>";
                    }
                    //Product Table End
                    Body += "<br>";
                    #endregion
                    Body += "<table class='table tableCSS'>";
                    Body += "<tbody>";

                    Body += "</tbody>";
                    Body += "</table>";
                    Body += "</body>";
                    Body += "</html>";
                    if (Type == "1")
                    {
                        OperationType = location + " - New product added";
                    }
                    else if (Type == "2")
                    {
                        OperationType = location + " - Product details updated";
                    }
                    else if (Type == "3")
                    {
                        OperationType = location + " - Packing details added";
                    }
                    else if (Type == "4")
                    {
                        OperationType = location + " - Packing details updated";
                    }
                    else if (Type == "5")
                    {
                        OperationType = location + " - Packing details deleted";
                    }
                    else if (Type == "6")
                    {
                        OperationType = location + " - Barcode added";
                    }
                    else if (Type == "7")
                    {
                        OperationType = location + " - Barcode deleted";
                    }
                    else if (Type == "8")
                    {
                        OperationType = location + " - Product deleted";
                    }
                    int e = 0;
                    //if (Type=="1" || Type == "2" || Type=="7")
                    //{
                    //    for (e = 0; e <= Ds.Tables[2].Rows.Count - 1; e++)
                    //    {
                    //        ReceiverEmailId += Ds.Tables[2].Rows[e]["Email"].ToString() + ",";
                    //    }
                    //}
                    //else
                    //{


                    for (e = 0; e <= Ds.Tables[3].Rows.Count - 1; e++)
                    {
                        ReceiverEmailId += Ds.Tables[3].Rows[e]["Email"].ToString() + ",";
                    }
                    //}
                    if (ReceiverEmailId != null && ReceiverEmailId != string.Empty && ReceiverEmailId.Length > 0)
                    {
                        string emailmsg = MailSending.SendMailResponse(ReceiverEmailId, "", "", OperationType, Body.ToString(), "Developer");
                        if (emailmsg == "Successful")
                        {
                            return "true";
                        }
                        else
                        {
                            return "EmailNotSend";
                        }
                    }
                    else
                    {
                        return "receiverMail";
                    }
                }
                else
                {
                    return "EmailNotSend";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    public string getMailBody1(DataSet Ds, string Type)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                if (Ds != null && Ds.Tables.Count > 0)
                {
                    Body = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'>";
                    Body += "<head> <style>table{border-collapse:collapse;}td {padding: 3px 8px;}.text-center{text-align:center}";
                    Body += "table {width: 100%;}body {font-size: 14px;font-family: monospace;}";
                    Body += ".center{text-align: center;}.right {text-align: right;}thead > tr {background: #fff;color: #000;font-weight: 700;border-top: 1px solid #5f7c8a;}";
                    Body += "thead > tr > td {text-align: center !important;}.InvContent {width: 100%;}.tdLabel {font-weight: 700;}";
                    Body += "@media print {#btnPrint {display: none;}}legend {border-bottom: 1px solid #5f7c8a;}";
                    Body += ".tableCSS > tbody > tr > td,.tableCSS > thead > tr > td,.tableCSS > tfoot > tr > td {border: 1px solid #5f7c8a;}pre{border:none !important;}";
                    Body += "</style>";
                    Body += "</head>";
                    Body += "<body style='font-family: Arial; font-size: 12px'>";
                    string location = "";
                    if (Ds.Tables[0].Rows.Count > 0)
                    {
                        Body += "<table class='table tableCSS'>";
                        Body += "<tbody>";
                        Body += "<tr><td><b>Product Id</b></td><td>" + Ds.Tables[0].Rows[0]["Prdtid"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Product Name</b></td><td>" + Ds.Tables[0].Rows[0]["ProductName"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Product Image</b></td><td><img src=" + Ds.Tables[0].Rows[0]["ImageUrl"].ToString() + " alt='' style='width: 50px;height: 50px;'></td></tr>";
                        Body += "<tr><td><b>Update Date & Time</b></td><td>" + Ds.Tables[0].Rows[0]["UpdateDate"].ToString() + "</td></tr>";
                        Body += "<tr><td><b>Updated By</b></td><td>" + Ds.Tables[0].Rows[0]["EmpName"].ToString() + "</td></tr>";
                        Body += "</tbody>";
                        Body += "</table>";
                        Body += "<br>";
                        location = Ds.Tables[0].Rows[0]["Location"].ToString();
                    }
                    #region
                    Body += "<table class='table tableCSS'>";
                    if (Ds.Tables[1].Rows.Count != 0 && Ds.Tables[1].Rows.Count > 0)
                    {
                        int i = 0, j = 0;
                        int NoOfUnit = 1;
                        DataTable dt = null; DataSet ds1 = null;
                        for (i = 0; i <= NoOfUnit - 1; i++)
                        {

                            String UnitId = Ds.Tables[2].Rows[i]["UnitId"].ToString();
                            ds1 = Ds.Copy();
                            DataRow[] foundRows = null;
                            var expr = "";

                            foundRows = ds1.Tables[2].Select(expr);

                            Body += "<thead>";
                            Body += "<tr>";
                            Body += "<td><b>Unit Type</b></td><td><b>Quantity</b></td><td><b>Cost Price</b></td><td><b>Wholesale Minimum Price</b></td><td><b>Retail Minimum Price</b></td><td><b>Base Price</b></td><td><b>Free</b></td><td><b>Location</b></td>";
                            Body += "</tr>";
                            Body += "</thead>";
                            Body += "<tbody>";
                            Body += "<tr>";

                            for (j = 0; j <= foundRows.Length - 1; j++)
                            {

                                var a = foundRows[j]["DefPacking"].ToString();
                                if (a != "")
                                {
                                    Body += "<td style='text-align:center'>" + foundRows[j]["UnitType"].ToString() + "(Default)</td>";
                                }
                                else
                                {
                                    Body += "<td style='text-align:center'>" + foundRows[j]["UnitType"].ToString() + "</td>";
                                }
                                Body += "<td style='text-align:center'>" + foundRows[j]["Qty"].ToString() + "</td>";
                                Body += "<td style='text-align:right'>" + foundRows[j]["CostPrice"].ToString() + "</td>";
                                Body += "<td style='text-align:right'>" + foundRows[j]["WHminPrice"].ToString() + "</td>";
                                Body += "<td style='text-align:right'>" + foundRows[j]["RetailMinPrice"].ToString() + "</td>";
                                Body += "<td style='text-align:right'>" + foundRows[j]["Price"].ToString() + "</td>";
                                Body += "<td style='text-align:left'>" + foundRows[j]["EligibleforFree"].ToString() + "</td>";
                                Body += "<td style='text-align:left'>" + foundRows[j]["Location"].ToString() + "</td><tr/>";
                            }
                        }
                    }
                    Body += "</tbody>";
                    Body += "</table>";

                    Body += "<br>";
                    #endregion
                    Body += "</tbody>";
                    Body += "</table>";
                    Body += "</body>";
                    Body += "</html>";
                    string OperationType = "", ReceiverEmailId = "";
                    OperationType = location + " - Price Updated";
                    int e = 0;
                    if (Type == "1" || Type == "2" || Type == "7")
                    {
                        for (e = 0; e <= Ds.Tables[2].Rows.Count - 1; e++)
                        {
                            ReceiverEmailId += Ds.Tables[2].Rows[e]["Email"].ToString() + ",";
                        }
                    }
                    else
                    {
                        for (e = 0; e <= Ds.Tables[3].Rows.Count - 1; e++)
                        {
                            ReceiverEmailId += Ds.Tables[3].Rows[e]["Email"].ToString() + ",";
                        }
                    }

                    if (ReceiverEmailId != null && ReceiverEmailId != string.Empty && ReceiverEmailId.Length > 0)
                    {
                        string emailmsg = MailSending.SendMailResponse(ReceiverEmailId, "", "", OperationType, Body.ToString(), "Developer");
                        if (emailmsg == "Successful")
                        {
                            return "true";
                        }
                        else
                        {
                            return "EmailNotSend";
                        }
                    }
                    else
                    {
                        return "receiverMail";
                    }
                }
                else
                {
                    return "EmailNotSend";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }

    }

    [WebMethod(EnableSession = true)]
    public string savewebsiteDetails(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.IsOutOfStock = Convert.ToInt32(jdv["CheckboxOOS"]);
                pobj.IsShowOnWebsite = Convert.ToInt32(jdv["IsShowOnWebsite"]);
                pobj.txtDescription = jdv["txtDescription"];
                pobj.EmpAutoId = Session["EmpAutoId"].ToString();
                BL_Product.savewebsiteDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string updateDefaultImage(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.defaultImg = jdv["defaultImg"];

                BL_Product.updateDefaultImage(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string removeProdutcImage(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.ImageAutoId = Convert.ToInt32(jdv["ImageAutoId"]);
                pobj.AutoId = Convert.ToInt32(jdv["ProductAutoId"]);

                BL_Product.removeProdutcImage(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string updateImageDetails(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.defaultImg = jdv["defaultImg"];
                DataTable dt = JsonConvert.DeserializeObject<DataTable>(jdv["ImageUrl"]);
                pobj.dtbulkImageUrl = dt;
                pobj.EmpAutoId = Session["EmpAutoId"].ToString();
                BL_Product.updateImageDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string clickonSecurity(string CheckSecurity)
    {
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                pobj.EmpAutoId = Session["EmpAutoId"].ToString();
                BL_Product.clickonSecurity(pobj);
                if (!pobj.isException)
                {
                    if (pobj.Ds.Tables.Count > 0 && pobj.Ds.Tables[0].Rows.Count > 0)
                    {
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.StatusCode = 200;
                        return "true";
                    }
                    else
                    {
                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.StatusCode = 200;
                        return "false";
                    }
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
}
