﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLMLTaxMaster;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for WMLTaxMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WMLTaxMaster : System.Web.Services.WebService {

    public WMLTaxMaster () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string insertTax(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {

            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_MLTaxMaster pobj = new PL_MLTaxMaster();
            try
            {
                if (jdv["Value"] != "")
                {
                    pobj.Rate = Convert.ToDecimal(jdv["Value"]);
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.State = Convert.ToInt32(jdv["State"]);
                pobj.PrintLabel = jdv["PrintLabel"];
                BL_MLTaxMaster.insert(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getTaxDetail(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_MLTaxMaster pobj = new PL_MLTaxMaster();
            try
            {
                pobj.TaxId = jdv["TaxId"];
                pobj.State = Convert.ToInt32(jdv["TaxState"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_MLTaxMaster.select(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string editTax(string TaxId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_MLTaxMaster pobj = new PL_MLTaxMaster();
            pobj.TaxId = TaxId;
            BL_MLTaxMaster.editTax(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateTax(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_MLTaxMaster pobj = new PL_MLTaxMaster();
            try
            {
                pobj.TaxId = jdv["TaxId"];
                if (jdv["Value"] != "")
                {
                    pobj.Rate = Convert.ToDecimal(jdv["Value"]);
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.State = Convert.ToInt32(jdv["State"]);
                pobj.PrintLabel = jdv["PrintLabel"];
                BL_MLTaxMaster.update(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteTax(string TaxId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_MLTaxMaster pobj = new PL_MLTaxMaster();
            try
            {
                pobj.TaxId = TaxId;
                BL_MLTaxMaster.delete(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindState()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_MLTaxMaster pobj = new PL_MLTaxMaster();
            try
            {
                BL_MLTaxMaster.BindState(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindTaxDetails()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_MLTaxMaster pobj = new PL_MLTaxMaster();
            try
            {
                BL_MLTaxMaster.BindTaxDetails(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateWeightTax(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_MLTaxMaster pobj = new PL_MLTaxMaster();
            try
            {
                pobj.TaxId = jdv["TaxId"];
                if (jdv["Value"] != "")
                {
                    pobj.Rate = Convert.ToDecimal(jdv["Value"]);
                }
                pobj.PrintLabel = jdv["PrintLabel"];
                BL_MLTaxMaster.updateWeightTax(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
}
