﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using DDLErrorTicketResponse;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for ErrorTicketResponse
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ErrorTicketResponse : System.Web.Services.WebService
{


    [WebMethod(EnableSession = true)]
    public string getErrorTicket(string TicketAutoId)
    {
        if (Session["UserName"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {
            PL_PropertyErrorTicketResponse pobj = new PL_PropertyErrorTicketResponse();
            try
            {
                pobj.TicketID = TicketAutoId;
                PL_BusinessErrorTicketResponse.selectallTicketDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public string insertErrorTicketResponse(string dataValue)
    {
        if (Session["UserName"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {
            PL_PropertyErrorTicketResponse pobj = new PL_PropertyErrorTicketResponse();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            try
            {
                pobj.TicketID = jdv["TicketID"];
                pobj.Action = jdv["Action"]; 
                pobj.ByEmployee = Session["EmpAutoId"].ToString();
                pobj.Status = jdv["Status"];
                pobj.DeveloperStatus = jdv["DeveloperStatus"];
                pobj.Subject = jdv["Subject"];
                pobj.Priority = jdv["Priority"];
                pobj.Description = jdv["Description"];
                PL_BusinessErrorTicketResponse.insert(pobj);
                if (!pobj.isException)
                {
                    sendMail(pobj);
                   return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }

    protected string sendMail(PL_PropertyErrorTicketResponse obj)
    {
        string byEmp = Session["EmpAutoId"].ToString();
        string Responseby = Session["EmpFirstName"].ToString(); 
        string Message = "";
        string Emailadmin = "";
        string bcc = "";
        PL_PropertyErrorTicketResponse pobj = new PL_PropertyErrorTicketResponse();
        pobj.ByEmployee = byEmp.Trim();
        PL_BusinessErrorTicketResponse.selectReceiver(pobj);
        if (pobj.Ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
            {
                Emailadmin += dr["EmailId"].ToString() + ",";
            }
        }
        if (Emailadmin.Trim() != "")
        {
            try
            {
                string MessageSubject = "A1WHM - " + pobj.Ds.Tables[0].Rows[0]["Companyid"].ToString() + " - There has been activity on support Ticket " + obj.TicketID + " (Subject : " + obj.Subject + ")";
                string body = "";
                body += "<html xmlns='http://www.w3.org/1999/xhtml'><head></head><body><form id='form1' runat='server'>";
                body += "  <table style='width: 812px; text-align: left; font-family: Helvetica, Arial, sans-serif;font-size: 13px;' cellpadding='2' cellspacing='2'>";
                body += " <tr><td colspan='3'>Dear Admin/Developers,</td> </tr> ";
                body += "<tr><td colspan='3'>There has been new activity on the your support ticket (<span style='color:Red;'>Ticket No.:</span><b>" + obj.TicketID + ")</b></td></tr>";
                body += "<tr style='height:10px'> <td colspan='3'></td></tr><tr><td colspan='3'><table width='100%' cellpadding='2' cellspacing='2'>";
                body += "<tr style='background-color: #ffefe4;'>";
                body += "<td style='width:20%'> <b>Response by</b></td><td style='width:20%'><b>Priority</b></td> <td style='width:20%'> <b>Client Status</b></td><td style='width:20%'> <b>Developer Status</b></td></tr>";
                body += "<tr> <td> " + Responseby + " </td><td> " + obj.Priority + " </td> <td> " + obj.Status + " </td><td> " + obj.DeveloperStatus + " </td> </tr>";
                body += "</table></td> </tr>";
                body += " <tr><td colspan='2'> <table width='100%' cellpadding='2' cellspacing='2'>";
                body += " <tr style='background-color: #ffefe4;'>";
                body += "<td colspan='2'><b>Description</b></td> </tr>";
                body += "<tr> <td colspan='2'> " + obj.Description + " </td> </tr>";
                body += " </table> </td> </tr> ";
                body += " <tr><td colspan='2'> <table width='100%' cellpadding='2' cellspacing='2'>";
                body += " <tr style='background-color: #ffefe4;'>";
                body += "<td colspan='2'><b>Response</b></td> </tr>";
                body += "<tr> <td colspan='2'> " + obj.Action + " </td> </tr>";
                body += " </table> </td> </tr> ";
                body += "<tr style='height:10px'> <td colspan='3'></td></tr>";
                body += "<tr> <td colspan='2'> To view the most recent activity, please view your ticket through the CRM  </td></tr>";
                body += "<tr style='height:20px'> <td colspan='3'></td></tr>";
                body += " <tr> <td colspan='2'> Thank You</td></tr> ";
                body += "<tr> <td colspan='2'> SIST</td></tr>";
                body += "<tr> <td colspan='2'> Development / Support™ </td> </tr>";
                body += "<tr> <td colspan='2' style='color:Red;'> **This is an automated email please do not reply** </td> </tr>";
                body += "</table></form></body></html>";
                string emailmsg = MailSending.SendMailResponse(Emailadmin.ToString(), "", bcc.ToString(), MessageSubject.ToString(), body.ToString(), "Developer");
                if (emailmsg == "Successful")
                {
                    Message = "true";
                }
                else
                {

                    Message = "false";
                }
            }
            catch (Exception ex)
            {
                Message = "false";
            }
        }
        return Message;
    }
}
