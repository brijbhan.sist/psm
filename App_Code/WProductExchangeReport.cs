﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLProductExchangeReport;
using System.Web.Script.Serialization;
using DllUtility;
/// <summary>
/// Summary description for WProductExchangeReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WProductExchangeReport : System.Web.Services.WebService
{
    public WProductExchangeReport()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string ProductExchangeReport(string dataValue)
    {
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                PL_ProductExchangeReport pobj = new PL_ProductExchangeReport();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
               
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPerson"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["Customer"]);

                pobj.DriverAutoId = Convert.ToInt32(jdv["DriverAutoId"]);
                pobj.Status = Convert.ToInt16(jdv["Status"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

                BL_ProductExchangeReport.getReport(pobj);           
                return pobj.Ds.GetXml();              
            }
            else
            {
                return "Session Expired";
            }
        }
        catch 
        {
            return "false";
        }       
    }

    [WebMethod (EnableSession = true)]
    public string BindDropdownlist()
    {
       try
       {
           PL_ProductExchangeReport pobj = new PL_ProductExchangeReport();
           if (Session["EmpAutoId"] != null)
           {
               BL_ProductExchangeReport.bindDropdownList(pobj);
               if (!pobj.isException)
               {
                   return pobj.Ds.GetXml();
               }
               else
               {
                   return "false";
               }
           }
           else
           {
               return "Session Expired";
           }
       }
        catch
       {
           return "false";
       }
    }
    [WebMethod(EnableSession = true)]
    public string BindCustomer(string SalesPersonAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                //var jss = new JavaScriptSerializer();
                //var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ProductExchangeReport pobj = new PL_ProductExchangeReport();
                pobj.SalesPersonAutoId = Convert.ToInt32(SalesPersonAutoId);
                BL_ProductExchangeReport.BindCustomer(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
