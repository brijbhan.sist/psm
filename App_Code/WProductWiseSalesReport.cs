﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLSaleReport;
using System.Web.Script.Serialization;
using DllUtility;
/// <summary>
/// Summary description for WProductWiseSalesReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WProductWiseSalesReport : System.Web.Services.WebService
{

    public WProductWiseSalesReport()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string BindProductCategory()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                BL_SaleReport.BindProductCategory(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindProductSubCategory(string CategoryAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                pobj.CategoryAutoId = Convert.ToInt32(CategoryAutoId);
                BL_SaleReport.BindProductSubCategory(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindProduct(string SubCategoryId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                pobj.SubCategoryId = Convert.ToInt32(SubCategoryId);
                BL_SaleReport.BindProduct(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetReportDetail(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_SaleReport pobj = new PL_SaleReport();
                pobj.BrandAutoIdStr = jdv["BrandAutoIdStr"];
                pobj.ProductAutoIdStr = jdv["ProductAutoId"];
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(jdv["SalesAutoId"]);
                pobj.SubCategoryId = Convert.ToInt32(jdv["SubCategoryAutoId"]); 
                pobj.CustomerType = Convert.ToInt32(jdv["CustomerType"]);
                if (jdv["FromDate"] != "")
                {
                    pobj.CloseOrderFromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.CloseOrderToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
              
                if (jdv["CreditFromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["CreditFromDate"]);
                }
                if (jdv["CreditToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["CreditToDate"]);
                }
                
                pobj.SearchBy = Convert.ToInt32(jdv["SearchBy"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_SaleReport.BindProductSaleReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch(Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindSalesPersonandStatus()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                BL_SaleReport.BindSalesPersonandStatus(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindCustomer(string CustomerType, string SalesPersonAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                pobj.CustomerType = Convert.ToInt32(CustomerType);
                pobj.SalesPersonAutoId = Convert.ToInt32(SalesPersonAutoId);
                BL_SaleReport.BindCustomer(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindProductDropdown(string SubcategoryAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                pobj.SubCategoryId = Convert.ToInt32(SubcategoryAutoId);
                BL_SaleReport.BindProductDropdown(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
