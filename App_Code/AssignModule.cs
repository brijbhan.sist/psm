﻿using DLLAssignModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for AssignModule
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class AssignModule : System.Web.Services.WebService {

    public AssignModule () {


    }

    [WebMethod(EnableSession = true)]
    public string HelloWorld() {
        return "Hello World";
    }
    [WebMethod(EnableSession = true)]
    public string bindDropdown()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_AssignModule pobj = new PL_AssignModule();
            try
            {
                BL_AssignModule.bindDropdown(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        return "true";
    }

    [WebMethod(EnableSession = true)]
    public string bindSubModule(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_AssignModule pobj = new PL_AssignModule();
        pobj.ModuleAutoId = Convert.ToInt32(jdv["ModuleAutoId"]);
        BL_AssignModule.getSubModuleByModule(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindPageName(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_AssignModule pobj = new PL_AssignModule();
        pobj.SubModuleAutoId = Convert.ToInt32(jdv["SubModuleAutoId"]);
        BL_AssignModule.bindPageName(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }


    [WebMethod(EnableSession = true)]
    public string assigedModule(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_AssignModule pobj = new PL_AssignModule();
        pobj.ModuleAutoId = Convert.ToInt32(jdv["Module"]);
        pobj.LocationAutoId = jdv["Location"];
        pobj.SubModuleAutoId = Convert.ToInt32(jdv["SubModule"]);
        BL_AssignModule.insert(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return "Success";
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }
    [WebMethod(EnableSession = true)]
    public string getAssignedModuleList(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_AssignModule pobj = new PL_AssignModule();

        if (jdv["searchLocation"] == "" || jdv["searchLocation"] == null)
        {
            pobj.LocationId = 0;
        }
        else
        {
            pobj.LocationId = Convert.ToInt32(jdv["searchLocation"]);
        }
        //if (jdv["searchPage"] == "" || jdv["searchPage"] == null)
        //{
        //    pobj.PageAutoId = 0;
        //}
        //else
        //{
        //    pobj.PageAutoId = Convert.ToInt32(jdv["searchPage"]);
        //}
        if (jdv["searchModule"] == "" || jdv["searchModule"] == null)
        {
            pobj.ModuleAutoId = 0;
        }
        else
        {
            pobj.ModuleAutoId = Convert.ToInt32(jdv["searchModule"]);
        }
        if (jdv["searchSubModule"] == "" || jdv["searchSubModule"] == null)
        {
            pobj.SubModuleAutoId = 0;
        }
        else
        {
            pobj.SubModuleAutoId = Convert.ToInt32(jdv["searchSubModule"]);
        }



        BL_AssignModule.select(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }
    [WebMethod(EnableSession = true)]
    public string update(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_AssignModule pobj = new PL_AssignModule();
        pobj.ModuleAutoId = Convert.ToInt32(jdv["Module"]);
        pobj.LocationAutoId = jdv["Location"];
        pobj.SubModuleAutoId = Convert.ToInt32(jdv["SubModule"]);
        pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
        BL_AssignModule.update(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return "Success";
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }
    [WebMethod(EnableSession = true)]
    public string delete(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_AssignModule pobj = new PL_AssignModule();
        pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
        BL_AssignModule.delete(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return "Success";
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }
}
