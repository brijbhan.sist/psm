﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using DllEmailServerMaster;

/// <summary>
/// Summary description for EmailServerMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class EmailServerMaster : System.Web.Services.WebService {

    [WebMethod(EnableSession = true)]
    public string insertEmailServer(string dataValue)
    {
        if (Session["UserName"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {

            DataUtility dobj = new DataUtility();
             Pl_EmailServer pobj = new Pl_EmailServer();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);                       
            try
            {
                pobj.CreatedBy = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.UserName = jdv["SendTo"];
                pobj.EmailID = jdv["Emailid"];
                pobj.Password = jdv["Password"];
                pobj.Server = jdv["Server"];
                pobj.Port = Convert.ToInt32(jdv["Port"]);
                pobj.SSL = Convert.ToBoolean(jdv["SSL"]);
                Bl_EmailServer.insert(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public string getEmailServer()
    {
        if (Session["UserName"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {
            Pl_EmailServer pobj = new Pl_EmailServer();
            try
            {
                Bl_EmailServer.select(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public string editEmailServer(string SendTo)
    {
        if (Session["UserName"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {
            Pl_EmailServer pobj = new Pl_EmailServer();
            try
            {
                pobj.UserName = SendTo;
                Bl_EmailServer.select(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public string updateEmailServer(string dataValue)
    {
        if (Session["EmpAutoId"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {
            DataUtility dobj = new DataUtility();
            Pl_EmailServer pobj = new Pl_EmailServer();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            if (jdv["SSL"]== 1)
            {
                pobj.SSL = true;
            }
            try
            {
                pobj.CreatedBy = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.UserName = jdv["SendTo"];
                pobj.EmailID = jdv["Emailid"];
                pobj.Password = jdv["Password"];
                pobj.Server = jdv["Server"];
                pobj.Port = Convert.ToInt32(jdv["Port"]);
                pobj.SSL = Convert.ToBoolean(jdv["SSL"]);
                Bl_EmailServer.update(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
    [WebMethod(EnableSession = true)]
    public string testEmailServer(string dataValue)
    {
        if (Session["UserName"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {
            Pl_EmailServer pobj = new Pl_EmailServer();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            try
            {
                pobj.UserName = jdv["SendTo"];
                pobj.EmailID = jdv["Emailid"];
                if (pobj.UserName!="" && pobj.EmailID!="")
                {
                    string body = "Congratulations!!! You have successfully configured Email Settings in the application. </br> Now you are ready to send email from this server.";
                        string emailmsg = MailSending.SendMailResponse(pobj.EmailID, "", "", "A1WHM: Testing Email setting", body, pobj.UserName); 
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
