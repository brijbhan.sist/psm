﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLproductReturnReport;
using System.Web.Script.Serialization;
using DllUtility;

/// <summary>
/// Summary description for WproductReturnReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WproductReturnReport : System.Web.Services.WebService {

    public WproductReturnReport()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string ProductExchangeReport(string dataValue)
    {
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                PL_productReturnReport pobj = new PL_productReturnReport();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);

                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                if (jdv["CloseFromDate"] != "")
                {
                    pobj.ClosedFromDate = Convert.ToDateTime(jdv["CloseFromDate"]);
                }
                if (jdv["CloseToDate"] != "")
                {
                    pobj.ClosedToDate = Convert.ToDateTime(jdv["CloseToDate"]);
                }
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPerson"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["Customer"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_productReturnReport.getReport(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch
        {
            return "false";
        }
    }

    [WebMethod (EnableSession = true)]
    public string BindDropdownlist()
    {
       try
       {
           PL_productReturnReport pobj = new PL_productReturnReport();
           if (Session["EmpAutoId"] != null)
           {
               BL_productReturnReport.bindDropdownList(pobj);
               if (!pobj.isException)
               {
                   return pobj.Ds.GetXml();
               }
               else
               {
                   return "false";
               }
           }
           else
           {
               return "Session Expired";
           }
       }
        catch
       {
           return "false";
       }
    }
    [WebMethod(EnableSession = true)]
    public string BindCustomer(string SalesPersonAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                //var jss = new JavaScriptSerializer();
                //var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_productReturnReport pobj = new PL_productReturnReport();
                pobj.SalesPersonAutoId = Convert.ToInt32(SalesPersonAutoId);
                BL_productReturnReport.BindCustomer(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    
}
