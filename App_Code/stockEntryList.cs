﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllStockEntryMaster;
using System.Web.Script.Serialization;
using System.Data;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class stockEntryList : System.Web.Services.WebService {

    [WebMethod(EnableSession = true)]
    public string getStockEntryList(string dataValues)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            PL_StockEntryMaster pobj = new PL_StockEntryMaster();
            try
            {
                pobj.BillNo = jdv["BillNo"];
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                if (jdv["FromBillDate"] != null && jdv["FromBillDate"] != "")
                {
                    pobj.FromBillDate = Convert.ToDateTime(jdv["FromBillDate"]);
                }
                if (jdv["ToBillDate"] != null && jdv["ToBillDate"] != "")
                {
                    pobj.ToBillDate = Convert.ToDateTime(jdv["ToBillDate"]);
                }
                pobj.PageSize = Convert.ToInt16(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt16(jdv["pageIndex"]);

                BL_StockEntryMaster.stockEntryList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session";
        }
    }
}
