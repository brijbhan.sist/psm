﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllUtility;
using DLLPLProductWiseReport;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for WPLProductWiseReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WPLProductWiseReport : System.Web.Services.WebService
{

    public WPLProductWiseReport()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string BindCategory()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PLProductWiseReport pobj = new PL_PLProductWiseReport();
                BL_PLProductWiseReport.AllddlList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string BindLSubCategory(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_PLProductWiseReport pobj = new PL_PLProductWiseReport();
                pobj.CategoryAutoId = Convert.ToInt32(jdv["subCategoryAutoId"]);
                BL_PLProductWiseReport.SubCategory(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetProductReport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_PLProductWiseReport pobj = new PL_PLProductWiseReport();
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.SalesPerson = Convert.ToInt32(jdv["SalesAutoId"]);
                pobj.SubCategoryAutoId = Convert.ToInt32(jdv["SubCategoryAutoId"]);
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_PLProductWiseReport.ProductWiseReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    //[WebMethod(EnableSession = true)]
    //public string GetCustomerReport(string dataValue)
    //{
    //    if (Session["EmpAutoId"] != null)
    //    {
    //        try
    //        {
    //            var jss = new JavaScriptSerializer();
    //            var jdv = jss.Deserialize<dynamic>(dataValue);
    //            PL_PLProductWiseReport pobj = new PL_PLProductWiseReport();
    //            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
    //            pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
    //            pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
    //            pobj.SalesPerson = Convert.ToInt32(jdv["SalesAutoId"]);
    //            pobj.SubCategoryAutoId = Convert.ToInt32(jdv["SubCategoryAutoId"]);
    //            if (jdv["FromDate"] != "")
    //            {
    //                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
    //            }
    //            if (jdv["ToDate"] != "")
    //            {
    //                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
    //            }
    //            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
    //            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
    //            BL_PLProductWiseReport.CustomerWiseReport(pobj);
    //            return pobj.Ds.GetXml();
    //        }
    //        catch
    //        {

    //            return "false";
    //        }
    //    }
    //    else
    //    {
    //        return "Session Expired";
    //    }
    //}

    [WebMethod(EnableSession = true)]
    public string BindCustomer(string SalesPersonAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                //var jss = new JavaScriptSerializer();
                //var jdv = jss.Deserialize<dynamic>(dataValue);

                PL_PLProductWiseReport pobj = new PL_PLProductWiseReport();
                pobj.SalesPerson = Convert.ToInt32(SalesPersonAutoId);
                BL_PLProductWiseReport.BindCustomer(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";

                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    //[WebMethod(EnableSession = true)]
    //public string BindProductBySubCat(string SubCategoryAutoId)
    //{
    //    if (Session["EmpAutoId"] != null)
    //    {
    //        try
    //        {
    //            PL_PLProductWiseReport pobj = new PL_PLProductWiseReport();
    //            pobj.SubCategoryAutoId = Convert.ToInt32(SubCategoryAutoId);
    //            BL_PLProductWiseReport.BindProductBySubCategory(pobj);
    //            if (!pobj.isException)
    //            {
    //                this.Context.Response.ContentType = "application/json; charset=utf-8";
    //                this.Context.Response.StatusCode = 200;

    //                return pobj.Ds.GetXml();
    //            }
    //            else
    //            {
    //                return "false";

    //            }
    //        }
    //        catch
    //        {
    //            return "false";
    //        }
    //    }
    //    else
    //    {
    //        return "Session Expired";
    //    }
    //}
}
