﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllSubcategory;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for subcategoryMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class subcategoryMaster : System.Web.Services.WebService
{
    [WebMethod(EnableSession = true)]
    public string bindCategory()
    {
        PL_Subcategory pobj = new PL_Subcategory();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                BL_Subcategory.selectCategory(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }

    }

    [WebMethod(EnableSession = true)]
    public string insertSubcategory(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Subcategory pobj = new PL_Subcategory();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.SubcategoryName = jdv["SubcategoryName"];
                pobj.Description = jdv["Description"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.SubcategoryTax = Convert.ToInt32(jdv["SubcategoryTax"]);
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.IsShow = Convert.ToInt32(jdv["IsShowOnWeb"]);
                if (jdv["SequenceNo"] != "")
                    pobj.SeqNo = Convert.ToInt32(jdv["SequenceNo"]);

                BL_Subcategory.insert(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.Clear();
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }

    }

    [WebMethod(EnableSession = true)]
    public string getSubcategory(String dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Subcategory pobj = new PL_Subcategory();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.SubcategoryId = jdv["SubcategoryId"];
                pobj.SubcategoryName = jdv["SubcategoryName"];
                pobj.SubcategoryTax = Convert.ToInt32(jdv["SubcategoryTax"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                BL_Subcategory.select(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string editSubcategory(string SubcategoryId)
    {
        PL_Subcategory pobj = new PL_Subcategory();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.SubcategoryId = SubcategoryId;
                BL_Subcategory.editSubcategory(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }

    }

    [WebMethod(EnableSession = true)]
    public string updateSubcategory(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Subcategory pobj = new PL_Subcategory();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.SubcategoryId = jdv["SubcategoryId"];
                pobj.SubcategoryName = jdv["SubcategoryName"];
                pobj.SubcategoryTax = Convert.ToInt32(jdv["SubcategoryTax"]);
                pobj.Description = jdv["Description"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.IsShow = Convert.ToInt32(jdv["IsShowOnWeb"]);
                if (jdv["SequenceNo"] != "")
                    pobj.SeqNo = Convert.ToInt32(jdv["SequenceNo"]);
                BL_Subcategory.update(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.Clear();
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }

    }

    [WebMethod(EnableSession = true)]
    public string deleteSubcategory(string SubcategoryId)
    {
        PL_Subcategory pobj = new PL_Subcategory();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.SubcategoryId = SubcategoryId;
                BL_Subcategory.delete(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    this.Context.Response.Clear();
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}

