﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using DLLProductSummaryReport;

/// <summary>
/// Summary description for WProductSummaryReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WProductSummaryReport : System.Web.Services.WebService {

    public WProductSummaryReport () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string ProductSummaryReport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_DLLProductSummaryReport pobj = new PL_DLLProductSummaryReport();
            pobj.ProductId = jdv["ProductId"];
            pobj.ProductName = jdv["ProductName"];
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

            BL_DLLProductSummaryReport.GetProductSummaryReport(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    
    [WebMethod(EnableSession = true)]
    public string ProductSummaryReportExport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_DLLProductSummaryReport pobj = new PL_DLLProductSummaryReport();
            pobj.ProductId = jdv["ProductId"];
            pobj.ProductName = jdv["ProductName"];
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            //pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            //pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

            BL_DLLProductSummaryReport.GetProductSummaryReportExport(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }    
}
