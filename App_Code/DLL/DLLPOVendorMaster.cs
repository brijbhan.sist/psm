﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DLLPOVendorMaster
/// </summary>
namespace DLLPOVendorMaster
{
    public class PL_POVendorMaster : Utility
    {
        public int PoAutoId { get; set; }
        public string POVNumber { get; set; }
        public int ProductId { get; set; }
        public int ProductAutoId { get; set; }
        public string Remark { get; set; }
        public string ReceiverRemark { get; set; }
        public int Status { get; set; }
        public int EmployeeId { get; set; }
        public int RequiredQty { get; set; }
        public int UnitAutoId { get; set; }
        public int QtyPerUnit { get; set; }
        public int POVDraftAutoId { get; set; }
        public int POVRemarks { get; set; }
        public int ReveivedQty { get; set; }
        public int VendorId { get; set; } 
        public int RecAutoId { get; set; }
        public int RecUnitAutoId { get; set; }
        public string Barcode { get; set; }
        public string Draftitems { get; set; }
        public DateTime POFromDate { get; set; }
        public DateTime POToDate { get; set; }
        public string PODraftId { get; set; }
        public decimal Price { get; set; }
        public DateTime BillDate { get; set; }
        public DateTime RecDate { get; set; }
        public string BillNo { get; set; }
    }
    public class DL_POVendorMaster
    {
        public static void ReturnTable(PL_POVendorMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPurchaseOrderVendorMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@EmpId", pobj.EmployeeId);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@POAutoId", pobj.PoAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@QtyPerUnit", pobj.QtyPerUnit);
                sqlCmd.Parameters.AddWithValue("@RequiredQty", pobj.RequiredQty);
                sqlCmd.Parameters.AddWithValue("@ReceivedQty", pobj.ReveivedQty);
                sqlCmd.Parameters.AddWithValue("@POVDraftAutoId", pobj.POVDraftAutoId);
                sqlCmd.Parameters.AddWithValue("@VendorId", pobj.VendorId);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);
                sqlCmd.Parameters.AddWithValue("@POVNumber", pobj.POVNumber);
                sqlCmd.Parameters.AddWithValue("@Price", pobj.Price);
                sqlCmd.Parameters.AddWithValue("@PODraftId", pobj.PODraftId);      
                sqlCmd.Parameters.AddWithValue("@RecAutoId", pobj.RecAutoId);
                sqlCmd.Parameters.AddWithValue("@RecUnitAutoId", pobj.RecUnitAutoId);
                sqlCmd.Parameters.AddWithValue("@BillNo", pobj.BillNo);
                if (pobj.POFromDate > DateTime.MinValue && pobj.POFromDate < DateTime.MaxValue)
                    sqlCmd.Parameters.AddWithValue("@POFromDate", pobj.POFromDate);
                if (pobj.BillDate > DateTime.MinValue && pobj.BillDate < DateTime.MaxValue)
                    sqlCmd.Parameters.AddWithValue("@BillDate", pobj.BillDate);
                if (pobj.RecDate > DateTime.MinValue && pobj.RecDate < DateTime.MaxValue)
                    sqlCmd.Parameters.AddWithValue("@RecDate", pobj.RecDate);


                if (pobj.POToDate > DateTime.MinValue && pobj.POToDate < DateTime.MaxValue)
                    sqlCmd.Parameters.AddWithValue("@POToDate", pobj.POToDate);

                sqlCmd.Parameters.AddWithValue("@Draftitems", pobj.Draftitems);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);//ReceiverRemark
                sqlCmd.Parameters.AddWithValue("@Remark", pobj.Remark);
                sqlCmd.Parameters.AddWithValue("@ReceiverRemark", pobj.ReceiverRemark);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                string Beforefill = System.DateTime.Now.ToString();
                sqlAdp.Fill(pobj.Ds);
                string Afterfill = System.DateTime.Now.ToString();
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_POVendorMaster
    {
        public static void BindDropDown(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 401;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void BindProductDropDown(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 4011;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void BindProductDetails(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 402;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void PoList(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 403;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void DraftList(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 404;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void EditDraft(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 405;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void ReceivePO(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 406;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void AddProductItem(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 101;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void AddProductItemByBarcode(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 102;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void DeleteProductItem(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 201;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void SaveAsDraft(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 301;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void GeneratePO(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 302;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void AutoUpdatePO(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 303;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void ReceiveByStatus(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 304;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void deleteDraft(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 409;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void SavePODraft(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 410;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void ForwardDraftList(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 411;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void RevertPODraft(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 412;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void CancelPO(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 413;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void RevertPO(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 414;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void DraftLog(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 415;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void POLog(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 416;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void AutoUpdatePODraftProduct(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 417;
            DL_POVendorMaster.ReturnTable(pobj);
        }
        public static void AutoUpdateRemarks(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 418;
            DL_POVendorMaster.ReturnTable(pobj);
        } 
        public static void PODetails(PL_POVendorMaster pobj)
        {
            pobj.Opcode = 419;
            DL_POVendorMaster.ReturnTable(pobj);
        }
    }
}