﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data;
using System.Data.SqlClient;

namespace DllManageMLQuantity
{
    public class PL_ManageMLQuantity : Utility
    {
        public int ProductAutoId { get; set; }
        public int CategoryAutoId { get; set; }
        public int SubcategoryAutoId { get; set; }
        public string ProductName { get; set; }
        public string Xml { get; set; }
        public int Updatedby { get; set; }
    }

    public class DL_ManageMLQuantity
    {
        public static void ReturnTable(PL_ManageMLQuantity pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcManageMLQuantity", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@SubcategoryAutoId", pobj.SubcategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductName", pobj.ProductName);
                sqlCmd.Parameters.AddWithValue("@Xml", pobj.Xml);
                sqlCmd.Parameters.AddWithValue("@UpdatedBy", pobj.Updatedby);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }         
    }

    public class BL_ManageMLQuantity
    {
        public static void getProductList(PL_ManageMLQuantity pobj)
        {
            pobj.Opcode = 41;
            DL_ManageMLQuantity.ReturnTable(pobj);
        }
        public static void UpdateMlQuantity(PL_ManageMLQuantity pobj)
        {
            pobj.Opcode = 42;
            DL_ManageMLQuantity.ReturnTable(pobj);
        }
    }
}
