﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllOrderMasterWH
{
    public class PL_OrderMasterWH : Utility
    {
        public int OrderAutoId { get; set; }
        public string OrderNo { get; set; }
        public string CheckSecurity { get; set; }
        public int CustomerAutoId { get; set; }
        public int LoginEmpType { get; set; }
        public int EmpAutoId { get; set; }
        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int ReqQty { get; set; }
        public int QtyPerUnit { get; set; }
        public decimal IsTaxable { get; set; }
        public decimal IsExchange { get; set; }
        public decimal IsFreeItem { get; set; }
        public string Barcode { get; set; }
        public DataTable TableValue { get; set; }
        public int CustomerTypeAutoId { get; set; }
        public int PackerAutoId { get; set; }
        public int Times { get; set; }
        public string Remarks { get; set; }
    }

    public class DL_OrderMasterWH
    {
        public static void ReturnTable(PL_OrderMasterWH pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcOrderMasterWH", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@IsFreeItem", pobj.IsFreeItem);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@ReqQty", pobj.ReqQty);
                sqlCmd.Parameters.AddWithValue("@QtyPerUnit", pobj.QtyPerUnit);
                sqlCmd.Parameters.AddWithValue("@IsExchange", pobj.IsExchange);
                sqlCmd.Parameters.AddWithValue("@IsTaxable", pobj.IsTaxable);
                sqlCmd.Parameters.AddWithValue("@Times", pobj.Times);
                sqlCmd.Parameters.AddWithValue("@PackerAutoId", pobj.PackerAutoId);
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);
                sqlCmd.Parameters.AddWithValue("@TableValue", pobj.TableValue);
                sqlCmd.Parameters.AddWithValue("@CheckSecurity", pobj.CheckSecurity);
                sqlCmd.Parameters.AddWithValue("@CustomerTypeAutoId", pobj.CustomerTypeAutoId);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_OrderMasterWH
    {
        public static void GetOrderdetails(PL_OrderMasterWH pobj)
        {
            pobj.Opcode = 41;
            DL_OrderMasterWH.ReturnTable(pobj);
        }
        public static void bindDropdown(PL_OrderMasterWH pobj)
        {
            pobj.Opcode = 42;
            DL_OrderMasterWH.ReturnTable(pobj);
        }

        public static void bindUnitType(PL_OrderMasterWH pobj)
        {
            pobj.Opcode = 43;
            DL_OrderMasterWH.ReturnTable(pobj);
        }

        public static void GetBarDetails(PL_OrderMasterWH pobj)
        {
            pobj.Opcode = 44;
            DL_OrderMasterWH.ReturnTable(pobj);
        }
        public static void updateOrder(PL_OrderMasterWH pobj)
        {
               pobj.Opcode = 21;
               DL_OrderMasterWH.ReturnTable(pobj);
        }
        public static void assignPacker(PL_OrderMasterWH pobj)
        {
            pobj.Opcode = 22;
            DL_OrderMasterWH.ReturnTable(pobj);
        }
        //public static void bindDropdowntest(PL_OrderMasterWH pobj)
        //{
        //    pobj.Opcode = 45;
        //    DL_OrderMasterWH.ReturnTable(pobj);
        //}
    }
}
