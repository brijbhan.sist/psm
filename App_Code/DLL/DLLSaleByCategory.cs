﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DLLDetailProductSalesReport
/// </summary>
namespace DLLSaleByCategory
{
    public class PL_DLLSaleByCategory : Utility
    {
        public int EmpAutoId { get; set; }
        public DateTime orderfrom { get; set; }
        public DateTime ToDate { get; set; }
        public int CategoryAutoId { get; set; }
        public int SubCategoryAutoId { get; set; }
    }
    public class DL_DLLSaleByCategory
    {
        public static void ReturnData(PL_DLLSaleByCategory pobj)
        {
            try

            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("ProcSale_By_Category", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                if (pobj.orderfrom > DateTime.MinValue && pobj.orderfrom < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@orderfrom", pobj.orderfrom);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                cmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                cmd.Parameters.AddWithValue("@SubCategoryAutoId", pobj.SubCategoryAutoId);
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                cmd.CommandTimeout = 1000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }

    public class BL_DLLSaleByCategory
    {
        public static void BindProductSaleReport(PL_DLLSaleByCategory pobj)
        {
            pobj.Opcode = 42;
            DL_DLLSaleByCategory.ReturnData(pobj);
        }
        public static void BindCategory(PL_DLLSaleByCategory pobj)
        {
            pobj.Opcode = 45;
            DL_DLLSaleByCategory.ReturnData(pobj);
        }
        public static void BindSubCategory(PL_DLLSaleByCategory pobj)
        {
            pobj.Opcode = 46;
            DL_DLLSaleByCategory.ReturnData(pobj);
        }
    }
}
