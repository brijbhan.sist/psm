﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DllUtility;

/// <summary>
/// Summary description for DataErrorTicketLog
/// </summary>
public class DataErrorTicketLog
{
    public static void returnTable(PropertyErrorTicketLog pobj)
    {
        try
        {
            Config connect = new Config();
            //DataBaseUtilitys dobj = new DataBaseUtilitys();
            SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("ProcErrorTicketLog", connect.con);

            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;
            sqlCmd.Parameters.Add("@Type", SqlDbType.NVarChar, 50).Value = pobj.Type;
            sqlCmd.Parameters.Add("@Status", SqlDbType.NVarChar, 50).Value = pobj.Status;
            sqlCmd.Parameters.Add("@TicketID", SqlDbType.NVarChar).Value = pobj.TicketID;
            sqlCmd.Parameters.Add("@ByEmployee", SqlDbType.NVarChar).Value = pobj.ByEmplyoyee;
            sqlCmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = pobj.FromDate;
            sqlCmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = pobj.ToDate;

            sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
            sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

            sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
            sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

            SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
            pobj.Dt = new DataTable();
            sqlAdp.Fill(pobj.Dt);
            pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
            pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
        }
        catch (Exception ex)
        {
            pobj.IsException = true;
            pobj.ExceptionMessage = ex.Message;
        }
    }
}