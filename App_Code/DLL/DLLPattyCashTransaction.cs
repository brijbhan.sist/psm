﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DllUtility;
using System.Data;


/// <summary>
/// Summary description for DLLPattyCashTransaction
/// </summary>
namespace DLLPattyCashTransaction
{
    public class PL_PattyCashTransaction : Utility
    {
        public int EmpType { get; set; }
        public int AutoId { get; set; }
         public decimal Amount { get; set; }
         public string TransactionType { get; set; }
         public string Remark { get; set; }
         public string SearchBy { get; set; }
         public string SecurityKey { get; set; }
         public int Category { get; set; }
         public DateTime FromDate { get; set; }
         public DateTime ToDate { get; set; }
         public DateTime TransactionDate { get; set; }

    }
    public class DL_PattyCashTransaction
    {
        public static void ReturnData(PL_PattyCashTransaction pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[ProcPattyCashTransaction]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@Who", pobj.Who);
                cmd.Parameters.AddWithValue("@Amount", pobj.Amount);
                cmd.Parameters.AddWithValue("@TransactionType", pobj.TransactionType);
                cmd.Parameters.AddWithValue("@SearchBy", pobj.SearchBy);
                cmd.Parameters.AddWithValue("@Remark", pobj.Remark);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@SecurityKey", pobj.SecurityKey);
                cmd.Parameters.AddWithValue("@Category", pobj.Category);
                cmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                cmd.Parameters.AddWithValue("@EmpType", pobj.EmpType);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                if (pobj.TransactionDate > DateTime.MinValue && pobj.TransactionDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@TransactionDate", pobj.TransactionDate);
                }
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
    public class BL_PattyCashTransaction
    {
        public static void insert(PL_PattyCashTransaction pobj)
        {
            pobj.Opcode = 11;
            DL_PattyCashTransaction.ReturnData(pobj);
        }
        public static void GetCurrentBlc(PL_PattyCashTransaction pobj)
        {
            pobj.Opcode = 41;
            DL_PattyCashTransaction.ReturnData(pobj);
        }
        public static void GetPattyCashList(PL_PattyCashTransaction pobj)
        {
            pobj.Opcode = 42;
            DL_PattyCashTransaction.ReturnData(pobj);
        }
        public static void checkSecurity(PL_PattyCashTransaction pobj)
        {
            pobj.Opcode = 43;
            DL_PattyCashTransaction.ReturnData(pobj);
        }
        public static void GetPattyCashListExport(PL_PattyCashTransaction pobj)
        {
            pobj.Opcode = 44;
            DL_PattyCashTransaction.ReturnData(pobj);
        }
        public static void EditPattyBalance(PL_PattyCashTransaction pobj)
        {
            pobj.Opcode = 45;
            DL_PattyCashTransaction.ReturnData(pobj);
        }
        public static void Binddropdown(PL_PattyCashTransaction pobj)
        {
            pobj.Opcode = 46;
            DL_PattyCashTransaction.ReturnData(pobj);
        }
        public static void Update(PL_PattyCashTransaction pobj)
        {
            pobj.Opcode = 21;
            DL_PattyCashTransaction.ReturnData(pobj);
        }
    }
}