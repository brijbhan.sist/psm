﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DLLSalesPerson_OrderView
{
    public class PL_SalesPerson_OrderMaster : Utility
    {           
        public int OrderAutoId { get; set; }       
        public string OrderNo { get; set; }
        public int EmpAutoId { get; set; }

    }
    public class DL_SalesPerson_OrderMaster
    {
        public static void ReturnTable(PL_SalesPerson_OrderMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("Proc_SalesPerson_OrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
               
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_SalesPerson_OrderMaster
    {
        public static void getOrderData(PL_SalesPerson_OrderMaster pobj)
        {
            pobj.Opcode = 42;
            DL_SalesPerson_OrderMaster.ReturnTable(pobj);
        }
    }
}