﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for DLL_CustomerCreditReport
/// </summary>
namespace DLL_CustomerCreditReport
{
    public class PL_CustomerCreditReport : Utility
    {
        public int CustomerAutoId { get; set; }
       

    }
    public class BL_CustomerCreditReport
    {

        public static void BindCustomer(PL_CustomerCreditReport pobj)
        {
            pobj.Opcode = 21;
            DL_CustomerCreditReport.ReturnData(pobj);
        }


        public static void GetCustomerCreditReport(PL_CustomerCreditReport pobj)
        {
            pobj.Opcode = 41;
            DL_CustomerCreditReport.ReturnData(pobj);
        } 

    }

    public class DL_CustomerCreditReport
    {
        public static void ReturnData(PL_CustomerCreditReport pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[ProcCustomer_Credit_Report]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
}