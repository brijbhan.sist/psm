﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DllUtility;
namespace DLLAccountCreditMemoView
{
    public class PL_AccountCreditMemoView:Utility
    {
        public string CancelRemarks { get; set; }
        public string SecurityKey { get; set; }
        public int CreditAutoId { get; set; }
        public int CustomerAutoId { get; set; }
        public int EmpAutoId { get; set; }
        public int Status { get; set; }
        public int CreditType { get; set; }
        public string Remarks { get; set; }
        public int EmpType { get; set; }       
    }
    public class DL_AccountCreditMemoView
    {
        public static void ReturnTable(PL_AccountCreditMemoView pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcAccountCreditMemoView", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CreditAutoId", pobj.CreditAutoId);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@SecurityKey", pobj.SecurityKey);
                sqlCmd.Parameters.AddWithValue("@CancelRemarks", pobj.CancelRemarks);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);

                sqlCmd.Parameters.AddWithValue("@Remark", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@EmpType", pobj.EmpType);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_AccountCreditMemoView
    {
       public static void EditCredit(PL_AccountCreditMemoView pobj)
        {
            pobj.Opcode = 46;
            DL_AccountCreditMemoView.ReturnTable(pobj);
        }
        public static void BindCreditLog(PL_AccountCreditMemoView pobj)
        {
            pobj.Opcode = 47;
            DL_AccountCreditMemoView.ReturnTable(pobj);
        }
       
        public static void PrintCredit(PL_AccountCreditMemoView pobj)
        {
            pobj.Opcode = 49;
            DL_AccountCreditMemoView.ReturnTable(pobj);
        }

        public static void CancelCreditMemo(PL_AccountCreditMemoView pobj)
        {
            pobj.Opcode = 50;
            DL_AccountCreditMemoView.ReturnTable(pobj);
        }

        public static void checkSecurity(PL_AccountCreditMemoView pobj)
        {
            pobj.Opcode = 51;
            DL_AccountCreditMemoView.ReturnTable(pobj);
        }
    }
}