﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllOrderMaster
{
    public class PL_OrderMaster : Utility
    {
        public int OrderAutoId { get; set; }
        public int DraftAutoId { get; set; }
        public string OrderNo { get; set; }
        public string CreditNo { get; set; }
        public decimal CreditAmount { get; set; }
        public decimal CreditMemoAmount { get; set; }
        public decimal PayableAmount { get; set; }
        public string CheckSecurity { get; set; }
        public string CustomerName { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int CustomerAutoId { get; set; }
        public int OrderType { get; set; }
        public string Terms { get; set; }
        public int OrderStatus { get; set; }
        public int SalesPersonAutoId { get; set; }
        public int LoginEmpType { get; set; }
        public int EmpAutoId { get; set; }
        public int LogAutoId { get; set; }
        public int PackerAutoId { get; set; }
        public int Times { get; set; }
        public int DrvAutoId { get; set; }
        public int AddrAutoId { get; set; }
        public int BillAddrAutoId { get; set; }
        public int ShipAddrAutoId { get; set; }
        public int AddrType { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string Long { get; set; }
        public string Lat { get; set; }
        public int State { get; set; }
        public string StateName { get; set; }
        public string City { get; set; }
        public int ZipAutoId { get; set; }
        public string Zipcode { get; set; }
        public int ShippingType { get; set; }

        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int ReqQty { get; set; }
        public int QtyPerUnit { get; set; }
        public int TaxType { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal minprice { get; set; }
        public decimal SRP { get; set; }
        public decimal GP { get; set; }
        public decimal MLQty { get; set; }
        public decimal MLTax { get; set; }
        public decimal AdjustmentAmt { get; set; }
        public decimal TaxRate { get; set; }
        public decimal IsTaxable { get; set; }
        public decimal IsExchange { get; set; }
        public decimal IsFreeItem { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal OverallDisc { get; set; }
        public decimal OverallDiscAmt { get; set; }
        public decimal ShippingCharges { get; set; }
        public decimal TotalTax { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal Oim_Discount { get; set; }
        public decimal Oim_DiscountAmount { get; set; }
        public int PackedBoxes { get; set; }

        public string Delivered { get; set; }
        public string PaymentRecev { get; set; }
        public string RecevAmt { get; set; }
        public decimal AmtValue { get; set; }
        public string ValueChanged { get; set; }
        public decimal DiffAmt { get; set; }
        public decimal NewTotal { get; set; }
        public int PayThru { get; set; }
        public string Remarks { get; set; }
        public string ManagerRemarks { get; set; }
        public string Barcode { get; set; }
        public string Root { get; set; }
        public string CheckNo { get; set; }

        public DataTable TableValue { get; set; }
        public DataTable TableAutoId { get; set; }
        public DataTable DTPackedItemsQty { get; set; }
        public DataTable CreditTable { get; set; }
        public DataTable AsgnOrder { get; set; }
        public DataTable DelItems { get; set; }
        public DataTable Payment { get; set; }
        public DateTime AsgnDate { get; set; }
        public bool FreeItem { get; set; }
        public DateTime Fromdate { get; set; }
        public DateTime Todate { get; set; }
        public DateTime FromDelDate { get; set; }
        public DateTime ToDelDate { get; set; }
        public string TypeShipping { get; set; }
        public int CustomerTypeAutoId { get; set; }
    }

    public class DL_OrderMaster
    {
        public static void ReturnTable(PL_OrderMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@IsFreeItem", pobj.IsFreeItem);
                sqlCmd.Parameters.AddWithValue("@OrderType", pobj.OrderType);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                sqlCmd.Parameters.AddWithValue("@CheckNo", pobj.CheckNo);
                sqlCmd.Parameters.AddWithValue("@ManagerRemarks", pobj.ManagerRemarks);
                sqlCmd.Parameters.AddWithValue("@Times", pobj.Times);
                if (pobj.OrderDate != DateTime.MinValue && pobj.OrderDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@OrderDate", pobj.OrderDate);
                }
                if (pobj.DeliveryDate != DateTime.MinValue && pobj.DeliveryDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@DeliveryDate", pobj.DeliveryDate);
                }
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@ReqQty", pobj.ReqQty);
                sqlCmd.Parameters.AddWithValue("@QtyPerUnit", pobj.QtyPerUnit);
                sqlCmd.Parameters.AddWithValue("@CreditNo", pobj.CreditNo);
                sqlCmd.Parameters.AddWithValue("@CreditAmount", pobj.CreditAmount);
                sqlCmd.Parameters.AddWithValue("@FreeItem", pobj.FreeItem);
                sqlCmd.Parameters.AddWithValue("@IsExchange", pobj.IsExchange);
                sqlCmd.Parameters.AddWithValue("@CreditMemoAmount", pobj.CreditMemoAmount);
                sqlCmd.Parameters.AddWithValue("@PayableAmount", pobj.PayableAmount);
                sqlCmd.Parameters.AddWithValue("@UnitPrice", pobj.UnitPrice);
                sqlCmd.Parameters.AddWithValue("@minprice", pobj.minprice);
                sqlCmd.Parameters.AddWithValue("@SRP", pobj.SRP);
                sqlCmd.Parameters.AddWithValue("@GP", pobj.GP);
                sqlCmd.Parameters.AddWithValue("@TaxRate", pobj.TaxRate);
                sqlCmd.Parameters.AddWithValue("@MLTax", pobj.MLTax);
                sqlCmd.Parameters.AddWithValue("@AdjustmentAmt", pobj.AdjustmentAmt);
                sqlCmd.Parameters.AddWithValue("@TaxType", pobj.TaxType);
                sqlCmd.Parameters.AddWithValue("@IsTaxable", pobj.IsTaxable);
                sqlCmd.Parameters.AddWithValue("@Terms", pobj.Terms);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
                sqlCmd.Parameters.AddWithValue("@PackerAutoId", pobj.PackerAutoId);
                sqlCmd.Parameters.AddWithValue("@DriverAutoId", pobj.DrvAutoId);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@AddressAutoId", pobj.AddrAutoId);
                sqlCmd.Parameters.AddWithValue("@BillAddrAutoId", pobj.BillAddrAutoId);
                sqlCmd.Parameters.AddWithValue("@ShipAddrAutoId", pobj.ShipAddrAutoId);
                sqlCmd.Parameters.AddWithValue("@AddressType", pobj.AddrType);
                sqlCmd.Parameters.AddWithValue("@Address", pobj.Address);
                sqlCmd.Parameters.AddWithValue("@Address2", pobj.Address2);
                sqlCmd.Parameters.AddWithValue("@State", pobj.State);
                sqlCmd.Parameters.AddWithValue("@City", pobj.City);
                sqlCmd.Parameters.AddWithValue("@ZipAutoId", pobj.ZipAutoId);
                sqlCmd.Parameters.AddWithValue("@Zipcode", pobj.Zipcode);
                sqlCmd.Parameters.AddWithValue("@ShippingType", pobj.ShippingType);
                sqlCmd.Parameters.AddWithValue("@LogAutoId", pobj.LogAutoId);
                sqlCmd.Parameters.AddWithValue("@MLQty", pobj.MLQty);
                sqlCmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                sqlCmd.Parameters.AddWithValue("@TotalAmount", pobj.TotalAmount);
                sqlCmd.Parameters.AddWithValue("@OverallDisc", pobj.OverallDisc);
                sqlCmd.Parameters.AddWithValue("@OverallDiscAmt", pobj.OverallDiscAmt);
                sqlCmd.Parameters.AddWithValue("@ShippingCharges", pobj.ShippingCharges);
                sqlCmd.Parameters.AddWithValue("@TotalTax", pobj.TotalTax);
                sqlCmd.Parameters.AddWithValue("@GrandTotal", pobj.GrandTotal);
                sqlCmd.Parameters.AddWithValue("@PackedBoxes", pobj.PackedBoxes);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);
                sqlCmd.Parameters.AddWithValue("@TableValue", pobj.TableValue); 
                sqlCmd.Parameters.AddWithValue("@TableAutoId", pobj.TableAutoId);
                sqlCmd.Parameters.AddWithValue("@AsgnOrder", pobj.AsgnOrder);
                sqlCmd.Parameters.AddWithValue("@DelItems", pobj.DelItems);
                sqlCmd.Parameters.AddWithValue("@StateName", pobj.StateName);
                sqlCmd.Parameters.AddWithValue("@Payment", pobj.Payment);
                sqlCmd.Parameters.AddWithValue("@CheckSecurity", pobj.CheckSecurity);
                sqlCmd.Parameters.AddWithValue("@Delivered", pobj.Delivered);
                sqlCmd.Parameters.AddWithValue("@PaymentRecev", pobj.PaymentRecev);
                sqlCmd.Parameters.AddWithValue("@RecevAmt", pobj.RecevAmt);
                sqlCmd.Parameters.AddWithValue("@AmtValue", pobj.AmtValue);
                sqlCmd.Parameters.AddWithValue("@ValueChanged", pobj.ValueChanged);
                sqlCmd.Parameters.AddWithValue("@DiffAmt", pobj.DiffAmt);
                sqlCmd.Parameters.AddWithValue("@NewTotal", pobj.NewTotal);
                sqlCmd.Parameters.AddWithValue("@PayThru", pobj.PayThru);
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@CustomerTypeAutoId", pobj.CustomerTypeAutoId);
                if (pobj.Fromdate != DateTime.MinValue && pobj.Fromdate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Fromdate", pobj.Fromdate);
                }
                if (pobj.Todate != DateTime.MinValue && pobj.Todate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Todate", pobj.Todate);
                }
                if (pobj.FromDelDate != DateTime.MinValue && pobj.FromDelDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDelDate", pobj.FromDelDate);
                }
                if (pobj.ToDelDate != DateTime.MinValue && pobj.ToDelDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDelDate", pobj.ToDelDate);
                }
                if (pobj.AsgnDate != DateTime.MinValue && pobj.AsgnDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@AsgnDate", pobj.AsgnDate);
                }
                sqlCmd.Parameters.AddWithValue("@Oim_Discount", pobj.Oim_Discount);
                sqlCmd.Parameters.AddWithValue("@Oim_DiscountAmount", pobj.Oim_DiscountAmount);
                
                sqlCmd.Parameters.AddWithValue("@Lat", pobj.Lat);
                sqlCmd.Parameters.AddWithValue("@Long", pobj.Long);
                sqlCmd.Parameters.AddWithValue("@TypeShipping", pobj.TypeShipping);
                sqlCmd.Parameters.AddWithValue("@CustomerName", pobj.CustomerName);
                sqlCmd.Parameters.AddWithValue("@Root", pobj.Root);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@DraftAutoId", SqlDbType.VarChar, 20).Value = pobj.DraftAutoId;
                sqlCmd.Parameters["@DraftAutoId"].Direction = ParameterDirection.InputOutput;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.DraftAutoId = Convert.ToInt32(sqlCmd.Parameters["@DraftAutoId"].Value);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later";
            }
        }
    }

    public class BL_OrderMaster
    {
        public static void insert(PL_OrderMaster pobj)
        {
            pobj.Opcode = 101;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void insertAddr(PL_OrderMaster pobj)
        {
            pobj.Opcode = 102;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void genPacking(PL_OrderMaster pobj)
        {
            pobj.Opcode = 103;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void saveStoppage(PL_OrderMaster pobj)
        {
            pobj.Opcode = 104;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void saveDelUpdates(PL_OrderMaster pobj)
        {
            pobj.Opcode = 105;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void saveDelStatus(PL_OrderMaster pobj)
        {
            pobj.Opcode = 107;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void payDueAmount(PL_OrderMaster pobj)
        {
            pobj.Opcode = 106;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void ApplyCreditMemo(PL_OrderMaster pobj)
        {
            pobj.Opcode = 109;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void deleteDeductionAmount(PL_OrderMaster pobj)
        {
            pobj.Opcode = 31;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void updateOrder(PL_OrderMaster pobj)
        {
            
                pobj.Opcode = 201;
            
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void AssignDriver(PL_OrderMaster pobj)
        {
            pobj.Opcode = 202;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void assignPacker(PL_OrderMaster pobj)
        {
            pobj.Opcode = 2021;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void updatePackedOrder(PL_OrderMaster pobj)
        {
            pobj.Opcode = 203;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void updatedeleverOrder(PL_OrderMaster pobj)
        {
            pobj.Opcode = 207;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void UpdatePayment(PL_OrderMaster pobj)
        {
            pobj.Opcode = 208;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void delete(PL_OrderMaster pobj)
        {
            pobj.Opcode = 301;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void deleteAddr(PL_OrderMaster pobj)
        {
            pobj.Opcode = 302;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void bindDropdown(PL_OrderMaster pobj)
        {
            pobj.Opcode = 401;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void bindAllDropdown(PL_OrderMaster pobj)
        {
            pobj.Opcode = 4011;
            DL_OrderMaster.ReturnTable(pobj);
        }

        public static void selectAddress(PL_OrderMaster pobj)
        {
            pobj.Opcode = 402;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void selectQtyPrice(PL_OrderMaster pobj)
        {
            pobj.Opcode = 403;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void bindUnitType(PL_OrderMaster pobj)
        {
            pobj.Opcode = 404;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void selectOrderList(PL_OrderMaster pobj)
        {
            pobj.Opcode = 405;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void editOrderDetails(PL_OrderMaster pobj)
        {
            pobj.Opcode = 406;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void bindStatus(PL_OrderMaster pobj)
        {
            pobj.Opcode = 407;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void getAddressList(PL_OrderMaster pobj)
        {
            pobj.Opcode = 408;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void addNewAddr(PL_OrderMaster pobj)
        {
            pobj.Opcode = 409;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void changeAddr(PL_OrderMaster pobj)
        {
            pobj.Opcode = 410;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void checkBarcode(PL_OrderMaster pobj)
        {
            pobj.Opcode = 411;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void AddProductQty(PL_OrderMaster pobj)
        {
            pobj.Opcode = 427;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void getDriverList(PL_OrderMaster pobj)
        {
            pobj.Opcode = 412;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void bindDriver(PL_OrderMaster pobj)
        {
            pobj.Opcode = 413;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void getAssignedOrdersList(PL_OrderMaster pobj)
        {
            if (pobj.LoginEmpType == 5)
            {
                pobj.Opcode = 420;
                DL_OrderMaster.ReturnTable(pobj);
            }
            else
            {
                pobj.Opcode = 414;
                DL_OrderMaster.ReturnTable(pobj);
            }
        }
        public static void getDrvAsgnOrder(PL_OrderMaster pobj)
        {
            pobj.Opcode = 415;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void Pick_Order(PL_OrderMaster pobj)
        {
            pobj.Opcode = 416;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void getOrderData(PL_OrderMaster pobj)
        {
            if (pobj.LoginEmpType == 3)
            {
                pobj.Opcode = 417;
                DL_OrderMaster.ReturnTable(pobj);
            }
            else
            {
                pobj.Opcode = 418;
                DL_OrderMaster.ReturnTable(pobj);
            }
        }
        public static void GetOrderPrint(PL_OrderMaster pobj)
        {
            pobj.Opcode = 51;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void GetOrderPrintNew(PL_OrderMaster pobj)
        {
            pobj.Opcode = 511;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void getDelOrderList(PL_OrderMaster pobj)
        {
            pobj.Opcode = 419;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void barcode_Print(PL_OrderMaster pobj)
        {
            pobj.Opcode = 421;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void cancelOrder(PL_OrderMaster pobj)
        {
            pobj.Opcode = 204;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void ShowTop25SellingProducts(PL_OrderMaster pobj)
        {
            pobj.Opcode = 422;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void GetBarDetails(PL_OrderMaster pobj)
        {
            pobj.Opcode = 423;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void SaveDraftOrder(PL_OrderMaster pobj)
        {
            pobj.Opcode = 108;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void UpdateDraftOrder(PL_OrderMaster pobj)
        {
            pobj.Opcode = 205;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void UpdateDraftReq(PL_OrderMaster pobj)
        {
            pobj.Opcode = 206;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void DraftOrderList(PL_OrderMaster pobj)
        {
            pobj.Opcode = 424;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void deleteDraft(PL_OrderMaster pobj)
        {
            pobj.Opcode = 303;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void editDraftDetails(PL_OrderMaster pobj)
        {
            pobj.Opcode = 425;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void selectShipViaList(PL_OrderMaster pobj)
        {
            pobj.Opcode = 426;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void viewOrder(PL_OrderMaster pobj)
        {
            pobj.Opcode = 428;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void PaymentHistory(PL_OrderMaster pobj)
        {
            pobj.Opcode = 429;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void PrintReadytoShipOrder(PL_OrderMaster pobj)
        {
            pobj.Opcode = 430;
            DL_OrderMaster.ReturnTable(pobj);
        }

        public static void SetAsProcess(PL_OrderMaster pobj)
        {
            pobj.Opcode = 211;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void DeleteDraftItem(PL_OrderMaster pobj)
        {
            pobj.Opcode = 212;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void clickonSecurity(PL_OrderMaster pobj)
        {
            pobj.Opcode = 431;
            DL_OrderMaster.ReturnTable(pobj);
        }

        public static void approveOrder(PL_OrderMaster pobj)
        {
            pobj.Opcode = 502;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void AddOngetOrderData(PL_OrderMaster pobj)
        {
            pobj.Opcode = 432;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void getDriverListToChange(PL_OrderMaster pobj)
        {
            pobj.Opcode = 433;
            DL_OrderMaster.ReturnTable(pobj);
        }

        public static void AssignDriverChange(PL_OrderMaster pobj)
        {
            pobj.Opcode = 434;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void BindCustomer(PL_OrderMaster pobj)
        {
            pobj.Opcode = 435;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void GetBarDetailsOrd(PL_OrderMaster pobj)
        {
            pobj.Opcode = 4231;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void bindZipcodeDropDowns(PL_OrderMaster pobj)
        {
            pobj.Opcode = 4001;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void ItemTypeUpdate(PL_OrderMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", 213);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId); 
                sqlCmd.Parameters.Add("@DraftAutoId", SqlDbType.VarChar, 20).Value = pobj.DraftAutoId;
                sqlCmd.Parameters["@DraftAutoId"].Direction = ParameterDirection.InputOutput;

                sqlCmd.Parameters.AddWithValue("@IsExchange", pobj.IsExchange);
                sqlCmd.Parameters.AddWithValue("@IsFreeItem", pobj.IsFreeItem);
                sqlCmd.Parameters.AddWithValue("@IsTaxable", pobj.IsTaxable);


                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.DraftAutoId = Convert.ToInt32(sqlCmd.Parameters["@DraftAutoId"].Value);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
        public static void BindPinCodeForCity(PL_OrderMaster pobj)
        {
            pobj.Opcode = 4002;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void DriverPrintOption(PL_OrderMaster pobj)
        {
            pobj.Opcode = 4003;
            DL_OrderMaster.ReturnTable(pobj);
        }
    }
}
