﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllLogin
{
    public class PL_Login : Utility
    {
        public string Name { get; set; }
        public int EmpAutoId { get; set; }
        public int EmpType { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string PageUrl { get; set; }

    }

    public class DL_Login
    {
        public static void ReturnTable(PL_Login pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcLogin", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@EmpType", pobj.EmpType);
                sqlCmd.Parameters.AddWithValue("@UserName", pobj.UserName);
                sqlCmd.Parameters.AddWithValue("@Password", pobj.Password);
                sqlCmd.Parameters.AddWithValue("@NewPassword", pobj.NewPassword);
                sqlCmd.Parameters.AddWithValue("@IPAddress", pobj.IPAddress);
                sqlCmd.Parameters.AddWithValue("@PageUrl", pobj.PageUrl);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_Login
    {
        public static void changePassword(PL_Login pobj)
        {
            pobj.Opcode = 21;
            DL_Login.ReturnTable(pobj);
        }
        //public static void changePasswordStore(PL_Login pobj)
        //{
        //    pobj.Opcode = 22;
        //    DAL_Login.ReturnTable(pobj);
        //}
        public static void login(PL_Login pobj)
        {
            pobj.Opcode = 41;
            DL_Login.ReturnTable(pobj);
        }
        public static void Bindlogo(PL_Login pobj)
        {
            pobj.Opcode = 42;
            DL_Login.ReturnTable(pobj);
        }
        public static void getPageAction(PL_Login pobj)
        {
            pobj.Opcode = 43;
            DL_Login.ReturnTable(pobj);
        }
        public static void getModuleList(PL_Login pobj)
        {
            pobj.Opcode = 44;
            DL_Login.ReturnTable(pobj);
        }
        //public static void selectProfileData(PL_Login pobj)
        //{
        //    pobj.Opcode = 42;
        //    DAL_Login.ReturnTable(pobj);
        //}
        //public static void selectProfilestore(PL_Login pobj)
        //{
        //    pobj.Opcode = 44;
        //    DAL_Login.ReturnTable(pobj);
        //}
        //public static void selectstoreData(PL_Login pobj)
        //{
        //    pobj.Opcode = 45;
        //    DAL_Login.ReturnTable(pobj);
        //}
        //public static void accessModule(PL_Login pobj)
        //{
        //    pobj.Opcode = 46;
        //    DAL_Login.ReturnTable(pobj);
        //}
        //public static void inspectorLogin(PL_Login pobj)
        //{
        //    pobj.Opcode = 47;
        //    DAL_Login.ReturnTable(pobj);
        //}
    }
}
