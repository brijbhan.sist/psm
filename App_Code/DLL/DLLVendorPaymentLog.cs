﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DllUtility;

/// <summary>
/// Summary description for DLLVendorPaymentLog
/// </summary>
namespace DLLVendorPaymentLog
{
    public class PL_VendorPaymentLog : Utility
    {
        public int PaymentType { get; set; }
        public string VendorName { get; set; }
        public int AutoId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int PayAutoId { get; set; }
    }
    public class DL_VendorPaymentLog
    {
        public static void Returntable(PL_VendorPaymentLog pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcVendorPaymentLog", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                sqlCmd.Parameters.AddWithValue("@PayAutoId", pobj.PayAutoId);
                sqlCmd.Parameters.AddWithValue("@PaymentType", pobj.PaymentType);
                sqlCmd.Parameters.AddWithValue("@VendorName", pobj.VendorName);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_VendorPaymentLog
    {
        public static void bindPaymentType(PL_VendorPaymentLog pobj)
        {
            pobj.Opcode = 41;
            DL_VendorPaymentLog.Returntable(pobj);
        }
        public static void GetVendorPaymentLog(PL_VendorPaymentLog pobj)
        {
            pobj.Opcode = 42;
            DL_VendorPaymentLog.Returntable(pobj);
        }
        public static void editPay(PL_VendorPaymentLog pobj)
        {
            pobj.Opcode = 43;
            DL_VendorPaymentLog.Returntable(pobj);
        }
    }
}