﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BusinessModificationBugMaster
/// </summary>
public class BusinessModificationBugMaster
{
    public static void insert(PropertyModificationBugMaster pobj)
    {
        pobj.OpCode = 11;
        DataModificationBugMaster.returnTable(pobj);
    }
    public static void selectreceiver(PropertyModificationBugMaster pobj)
    {
        pobj.OpCode = 41;
        DataModificationBugMaster.returnTable(pobj);
    }
    public static void selectEmployeedetails(PropertyModificationBugMaster pobj)
    {
        pobj.OpCode = 42;
        DataModificationBugMaster.returnTable(pobj);
    }
}