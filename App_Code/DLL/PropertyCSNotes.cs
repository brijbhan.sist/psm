﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropertyCSlog
/// </summary>
public class PropertyCSNotes : PropertyUtilityAbstract
{
    string cSLid, empId, serviceType, refrenceNo, notes, custId, comment, fromPage, orderNo, ordermemo;

    public string OrderNo
    {
        get { return orderNo; }
        set { orderNo = value; }
    }
    public string OrderMemo
    {
        get { return ordermemo; }
        set { ordermemo = value; }
    }
    DateTime noteTime;

    public DateTime NoteTime
    {
        get { return noteTime; }
        set { noteTime = value; }
    }

    public string FromPage
    {
        get { return fromPage; }
        set { fromPage = value; }
    }

    public string Comment
    {
        get { return comment; }
        set { comment = value; }
    }

    public string CustId
    {
        get { return custId; }
        set { custId = value; }
    }

    public string Notes
    {
        get { return notes; }
        set { notes = value; }
    }

    public string RefrenceNo
    {
        get { return refrenceNo; }
        set { refrenceNo = value; }
    }

    public string ServiceType
    {
        get { return serviceType; }
        set { serviceType = value; }
    }

    public string EmpId
    {
        get { return empId; }
        set { empId = value; }
    }

    public string CSLid
    {
        get { return cSLid; }
        set { cSLid = value; }
    }
}