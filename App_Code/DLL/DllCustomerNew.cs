﻿using System;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllCustomerNew
{
    public class PL_CustomerNew : Utility
    {
        public int CustomerAutoId { get; set; }
        public int DateSortIn { get; set; }
        public string PaymentId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int Status { get; set; }
        public string OrderNo { get; set; }
        public int PaymentAutoId { get; set; }
        public int EmpAutoId { get; set; }
        public decimal ReceivedAmount { get; set; }
        public int PaymentMethod { get; set; }
        public string ReferenceNo { get; set; }
        public string PaymentRemarks { get; set; }
        public int ReceivedPaymentBy { get; set; }
        public string EmployeeRemarks { get; set; }
        public decimal StoreCredit { get; set; }
        public DataTable TableValue { get; set; }
        public decimal SortAmount { get; set; }
        public decimal TotalCurrencyAmount { get; set; }
        public string PaymentCurrencyXml { get; set; }
        public int BillAddAutoId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentURL { get; set; }
        public int FileAutoId { get; set; }
        public string CancelRemark { get; set; }
        public string CheckNo { get; set; }
        public DateTime CheckDate { get; set; }
        public string BankName { get; set; }
        public string BankAcc { get; set; }
        public int CardType { get; set; }
        public string CardNo { get; set; }
        public string ExpiryDate { get; set; }
        public string CVV { get; set; }
        public string RoutingNo { get; set; }
        public int AutoId { get; set; }
        public string SecurityKey { get; set; }
        public int OrderAutoId { get; set; }
        public string Zipcode { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode1 { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public int IsDefault { get; set; }
    }
    public class DL_CustomerNew
    {
        public static void CustomerDetails(PL_CustomerNew pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("CustomerMasterProfile", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@Opcode", SqlDbType.Int).Value = pobj.Opcode;
                sqlCmd.Parameters.Add("@CustomerAutoId", SqlDbType.Int).Value = pobj.CustomerAutoId;
                sqlCmd.Parameters.Add("@FileAutoId",SqlDbType.Int).Value= pobj.FileAutoId;
                sqlCmd.Parameters.Add("@EmpAutoId", SqlDbType.Int).Value = pobj.EmpAutoId;
                sqlCmd.Parameters.Add("@DocumentName", SqlDbType.VarChar, 100).Value = pobj.DocumentName;
                sqlCmd.Parameters.Add("@DocumentURL", SqlDbType.VarChar, 100).Value = pobj.DocumentURL;
                sqlCmd.Parameters.Add("@BankName",SqlDbType.VarChar,100).Value= pobj.BankName;
                sqlCmd.Parameters.Add("@DateSortIn", SqlDbType.Int).Value= pobj.DateSortIn;
                sqlCmd.Parameters.Add("@BankAcc", SqlDbType.VarChar, 30).Value= pobj.BankAcc;
                sqlCmd.Parameters.Add("@CardType",SqlDbType.Int).Value= pobj.CardType;
                sqlCmd.Parameters.Add("@CardNo",SqlDbType.VarChar,30).Value= pobj.CardNo;
                sqlCmd.Parameters.Add("@CVV",SqlDbType.Int).Value= pobj.CVV;
                sqlCmd.Parameters.Add("@Zipcode", SqlDbType.VarChar).Value = pobj.Zipcode;

                sqlCmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = pobj.Address;
                sqlCmd.Parameters.Add("@Address2", SqlDbType.VarChar).Value = pobj.Address2;
                sqlCmd.Parameters.Add("@City", SqlDbType.VarChar).Value = pobj.City;
                sqlCmd.Parameters.Add("@State", SqlDbType.VarChar).Value = pobj.State;
                sqlCmd.Parameters.Add("@Zipcode1", SqlDbType.VarChar).Value = pobj.Zipcode1;
                sqlCmd.Parameters.Add("@latitude", SqlDbType.VarChar).Value = pobj.latitude;
                sqlCmd.Parameters.Add("@longitude", SqlDbType.VarChar).Value = pobj.longitude;

                sqlCmd.Parameters.Add("@Status",SqlDbType.Int).Value= pobj.Status;
                sqlCmd.Parameters.Add("@AutoId",SqlDbType.Int).Value= pobj.AutoId;
                sqlCmd.Parameters.Add("@StoreCredit", SqlDbType.Decimal, 10).Value= pobj.StoreCredit;
                sqlCmd.Parameters.Add("@PaymentRemarks", SqlDbType.VarChar, 500).Value= pobj.PaymentRemarks;
                sqlCmd.Parameters.Add("@ExpiryDate",SqlDbType.VarChar,10).Value= pobj.ExpiryDate;
                sqlCmd.Parameters.Add("@RoutingNo",SqlDbType.VarChar,30).Value= pobj.RoutingNo;
                sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.PageIndex;
                sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }

        public static void CustomerOrder(PL_CustomerNew pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("CustomerMaster_OrderDetails", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@Opcode", SqlDbType.Int).Value = pobj.Opcode;
                sqlCmd.Parameters.Add("@CustomerAutoId", SqlDbType.Int).Value = pobj.CustomerAutoId;
                sqlCmd.Parameters.Add("@ReferenceNo", SqlDbType.VarChar).Value = pobj.ReferenceNo;
                sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.PageIndex;
                sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }

        public static void CustomerPayment(PL_CustomerNew pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("CustomerMaster_PaymentDetails", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@Opcode", SqlDbType.Int).Value = pobj.Opcode;
                sqlCmd.Parameters.Add("@CustomerAutoId", SqlDbType.Int).Value = pobj.CustomerAutoId;
                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = pobj.FromDate;
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = pobj.ToDate;
                }
                sqlCmd.Parameters.Add("@EmpAutoId", SqlDbType.Int).Value = pobj.EmpAutoId;
                sqlCmd.Parameters.Add("@PaymentAutoId",SqlDbType.Int).Value= pobj.PaymentAutoId;
                sqlCmd.Parameters.Add("@OrderNo",SqlDbType.VarChar,15).Value= pobj.OrderNo;
                sqlCmd.Parameters.Add("@Status", SqlDbType.Int).Value = pobj.Status;
                sqlCmd.Parameters.Add("@PaymentId", SqlDbType.VarChar, 25).Value = pobj.PaymentId;
                sqlCmd.Parameters.Add("@ReceivedAmount", SqlDbType.Decimal,10).Value=pobj.ReceivedAmount;
                sqlCmd.Parameters.Add("@PaymentMethod", SqlDbType.Int).Value=pobj.PaymentMethod;
                sqlCmd.Parameters.Add("@ReferenceNo", SqlDbType.VarChar,25).Value= pobj.ReferenceNo;
                sqlCmd.Parameters.Add("@PaymentRemarks", SqlDbType.VarChar,500).Value= pobj.PaymentRemarks;
                sqlCmd.Parameters.Add("@ReceivedPaymentBy", SqlDbType.Int).Value= pobj.ReceivedPaymentBy;
                sqlCmd.Parameters.Add("@EmployeeRemarks", SqlDbType.VarChar,500).Value= pobj.EmployeeRemarks;
                sqlCmd.Parameters.Add("@TableValue", SqlDbType.Structured).Value= pobj.TableValue;
                sqlCmd.Parameters.Add("@SortAmount",SqlDbType.Decimal,10).Value= pobj.SortAmount;
                sqlCmd.Parameters.Add("@CancelRemark",SqlDbType.VarChar,500).Value= pobj.CancelRemark;
                sqlCmd.Parameters.Add("@CheckNo",SqlDbType.VarChar,100).Value= pobj.CheckNo;
                if (pobj.CheckDate > DateTime.MinValue && pobj.CheckDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.Add("@CheckDate",SqlDbType.DateTime).Value= pobj.CheckDate;
                }
                sqlCmd.Parameters.Add("@TotalCurrencyAmount", SqlDbType.Decimal, 10).Value = pobj.TotalCurrencyAmount;
                sqlCmd.Parameters.Add("@PaymentCurrencyXml", SqlDbType.Xml).Value = pobj.PaymentCurrencyXml;
                sqlCmd.Parameters.Add("@BillAddAutoId", SqlDbType.Int).Value = pobj.BillAddAutoId;
                sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.PageIndex;
                sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception EX)
            {
                pobj.isException = true;
                pobj.exceptionMessage = EX.Message;// "Oops! Something went wrong.Please try later.";
            }
        }
        public static void checkSecurity(PL_CustomerNew pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("CustomerMaster_PaymentDetails", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@Opcode", SqlDbType.Int).Value = pobj.Opcode; 
                sqlCmd.Parameters.Add("@EmpAutoId", SqlDbType.Int).Value = pobj.EmpAutoId;
                sqlCmd.Parameters.Add("@SecurityKey", SqlDbType.VarChar).Value= pobj.SecurityKey;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }
        public static void CreditMemoSettled(PL_CustomerNew pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("CustomerMaster_OrderDetails", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@Opcode", SqlDbType.Int).Value = pobj.Opcode;
                sqlCmd.Parameters.Add("@EmpAutoId", SqlDbType.Int).Value = pobj.EmpAutoId;
                sqlCmd.Parameters.Add("@OrderAutoId", SqlDbType.Int).Value = pobj.OrderAutoId;
                sqlCmd.Parameters.Add("@ReferenceNo", SqlDbType.VarChar).Value = pobj.ReferenceNo;
                sqlCmd.Parameters.Add("@EmployeeRemarks", SqlDbType.VarChar).Value = pobj.EmployeeRemarks;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }

        public static void CustomerCreditMemoList(PL_CustomerNew pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcCustomer_CreditMemoList", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@Opcode", SqlDbType.Int).Value = pobj.Opcode;
                sqlCmd.Parameters.Add("@CustomerAutoId", SqlDbType.Int).Value = pobj.CustomerAutoId;
                sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.PageIndex;
                sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }

        public static void CustomerPaymentLog(PL_CustomerNew pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("CustomerMaster_PaymentDetails", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@Opcode", SqlDbType.Int).Value = pobj.Opcode;
                sqlCmd.Parameters.Add("@PaymentAutoId", SqlDbType.Int).Value = pobj.PaymentAutoId;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }
        public static void CustomerorderPayment(PL_CustomerNew pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("CustomerMaster_OrderDetails", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@Opcode", SqlDbType.Int).Value = pobj.Opcode;
                sqlCmd.Parameters.Add("@OrderAutoId", SqlDbType.Int).Value = pobj.OrderAutoId;

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }

        }

        public static void checkHoldSecurity(PL_CustomerNew pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("CustomerMaster_PaymentDetails", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@Opcode", SqlDbType.Int).Value = pobj.Opcode;
                sqlCmd.Parameters.Add("@EmpAutoId", SqlDbType.Int).Value = pobj.EmpAutoId;
                sqlCmd.Parameters.Add("@SecurityKey", SqlDbType.VarChar).Value = pobj.SecurityKey;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }

        public static void BillingDetails(PL_CustomerNew pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("CustomerMasterProfile", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@Opcode", SqlDbType.Int).Value = pobj.Opcode;
                sqlCmd.Parameters.Add("@CustomerAutoId", SqlDbType.Int).Value = pobj.CustomerAutoId;
                sqlCmd.Parameters.Add("@AutoId", SqlDbType.Int).Value = pobj.AutoId;
                sqlCmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = pobj.Address;
                sqlCmd.Parameters.Add("@Address2", SqlDbType.VarChar).Value = pobj.Address2;
                sqlCmd.Parameters.Add("@City", SqlDbType.VarChar).Value = pobj.City;
                sqlCmd.Parameters.Add("@State", SqlDbType.VarChar).Value = pobj.State;
                sqlCmd.Parameters.Add("@Zipcode1", SqlDbType.VarChar).Value = pobj.Zipcode1;
                sqlCmd.Parameters.Add("@latitude", SqlDbType.VarChar).Value = pobj.latitude;
                sqlCmd.Parameters.Add("@longitude", SqlDbType.VarChar).Value = pobj.longitude;
                sqlCmd.Parameters.Add("@IsDefault", SqlDbType.VarChar).Value = pobj.IsDefault;
                sqlCmd.Parameters.Add("@EmpAutoId", SqlDbType.VarChar).Value = pobj.EmpAutoId;

                sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.PageIndex;
                sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }

        public static void ShippingDetails(PL_CustomerNew pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("CustomerMasterProfile", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@Opcode", SqlDbType.Int).Value = pobj.Opcode;
                sqlCmd.Parameters.Add("@CustomerAutoId", SqlDbType.Int).Value = pobj.CustomerAutoId;
                sqlCmd.Parameters.Add("@AutoId", SqlDbType.Int).Value = pobj.AutoId;
                sqlCmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = pobj.Address;
                sqlCmd.Parameters.Add("@Address2", SqlDbType.VarChar).Value = pobj.Address2;
                sqlCmd.Parameters.Add("@City", SqlDbType.VarChar).Value = pobj.City;
                sqlCmd.Parameters.Add("@State", SqlDbType.VarChar).Value = pobj.State;
                sqlCmd.Parameters.Add("@IsDefault", SqlDbType.VarChar).Value = pobj.IsDefault;
                sqlCmd.Parameters.Add("@latitude", SqlDbType.VarChar).Value = pobj.latitude;
                sqlCmd.Parameters.Add("@longitude", SqlDbType.VarChar).Value = pobj.longitude;
                sqlCmd.Parameters.Add("@Zipcode1", SqlDbType.VarChar).Value = pobj.Zipcode1;
                sqlCmd.Parameters.Add("@EmpAutoId", SqlDbType.VarChar).Value = pobj.EmpAutoId;

                sqlCmd.Parameters.Add("@PageIndex", SqlDbType.Int).Value = pobj.PageIndex;
                sqlCmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pobj.PageSize;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }
    }
}
