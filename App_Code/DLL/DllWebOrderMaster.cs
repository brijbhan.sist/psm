﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllWebOrderMaster
{
    public class PL_OrderMaster : Utility
    {
        public int OrderAutoId { get; set; }
        public int DraftAutoId { get; set; }
        public int ApproveStatus { get; set; }
        public string OrderNo { get; set; }
        public decimal CreditAmount { get; set; }
        public string CheckSecurity { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int CustomerAutoId { get; set; }
        public int OrderStatus { get; set; }
        public int SalesPersonAutoId { get; set; }
        public int LoginEmpType { get; set; }
        public int EmpAutoId { get; set; }
        public int DrvAutoId { get; set; }
        public int AddrAutoId { get; set; }
        public int BillAddrAutoId { get; set; }
        public int ShipAddrAutoId { get; set; }
        public int AddrType { get; set; }
        public string Address { get; set; }
        public int State { get; set; }
        public string City { get; set; }
        public int ZipAutoId { get; set; }
        public string Zipcode { get; set; }
        public int ShippingType { get; set; }

        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int ReqQty { get; set; }
        public int QtyPerUnit { get; set; }
        public int TaxType { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal minprice { get; set; }
        public decimal SRP { get; set; }
        public decimal GP { get; set; }
        public decimal MLQty { get; set; }
        public decimal TaxRate { get; set; }
        public decimal IsTaxable { get; set; }
        public decimal IsExchange { get; set; }
        public decimal IsFreeItem { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal OverallDisc { get; set; }
        public decimal OverallDiscAmt { get; set; }
        public decimal ShippingCharges { get; set; }
        public decimal TotalTax { get; set; }
        public decimal GrandTotal { get; set; }
        public string Remarks { get; set; }
        public string Barcode { get; set; }
        public DataTable TableValue { get; set; }
        public bool FreeItem { get; set; }
    }

    public class DL_OrderMaster
    {
        public static void ReturnTable(PL_OrderMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcOrderMasterWeb", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@ApproveStatus", pobj.ApproveStatus);
                sqlCmd.Parameters.AddWithValue("@IsFreeItem", pobj.IsFreeItem);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                if (pobj.DeliveryDate != DateTime.MinValue && pobj.DeliveryDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@DeliveryDate", pobj.DeliveryDate);
                }
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@ReqQty", pobj.ReqQty);
                sqlCmd.Parameters.AddWithValue("@QtyPerUnit", pobj.QtyPerUnit);
                sqlCmd.Parameters.AddWithValue("@CreditAmount", pobj.CreditAmount);
                sqlCmd.Parameters.AddWithValue("@FreeItem", pobj.FreeItem);
                sqlCmd.Parameters.AddWithValue("@IsExchange", pobj.IsExchange);
                sqlCmd.Parameters.AddWithValue("@UnitPrice", pobj.UnitPrice);
                sqlCmd.Parameters.AddWithValue("@minprice", pobj.minprice);
                sqlCmd.Parameters.AddWithValue("@SRP", pobj.SRP);
                sqlCmd.Parameters.AddWithValue("@GP", pobj.GP);
                sqlCmd.Parameters.AddWithValue("@TaxRate", pobj.TaxRate);
                sqlCmd.Parameters.AddWithValue("@TaxType", pobj.TaxType);
                sqlCmd.Parameters.AddWithValue("@IsTaxable", pobj.IsTaxable);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@AddressAutoId", pobj.AddrAutoId);
                sqlCmd.Parameters.AddWithValue("@BillAddrAutoId", pobj.BillAddrAutoId);
                sqlCmd.Parameters.AddWithValue("@ShipAddrAutoId", pobj.ShipAddrAutoId);
                sqlCmd.Parameters.AddWithValue("@AddressType", pobj.AddrType);
                sqlCmd.Parameters.AddWithValue("@Address", pobj.Address);
                sqlCmd.Parameters.AddWithValue("@State", pobj.State);
                sqlCmd.Parameters.AddWithValue("@City", pobj.City);
                sqlCmd.Parameters.AddWithValue("@ZipAutoId", pobj.ZipAutoId);
                sqlCmd.Parameters.AddWithValue("@Zipcode", pobj.Zipcode);
                sqlCmd.Parameters.AddWithValue("@ShippingType", pobj.ShippingType);
                sqlCmd.Parameters.AddWithValue("@MLQty", pobj.MLQty);
                sqlCmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                sqlCmd.Parameters.AddWithValue("@TotalAmount", pobj.TotalAmount);
                sqlCmd.Parameters.AddWithValue("@OverallDisc", pobj.OverallDisc);
                sqlCmd.Parameters.AddWithValue("@OverallDiscAmt", pobj.OverallDiscAmt);
                sqlCmd.Parameters.AddWithValue("@ShippingCharges", pobj.ShippingCharges);
                sqlCmd.Parameters.AddWithValue("@TotalTax", pobj.TotalTax);
                sqlCmd.Parameters.AddWithValue("@GrandTotal", pobj.GrandTotal);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);
                sqlCmd.Parameters.AddWithValue("@TableValue", pobj.TableValue); 
                sqlCmd.Parameters.AddWithValue("@CheckSecurity", pobj.CheckSecurity);
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@DraftAutoId", SqlDbType.VarChar, 20).Value = pobj.DraftAutoId;
                sqlCmd.Parameters["@DraftAutoId"].Direction = ParameterDirection.InputOutput;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.DraftAutoId = Convert.ToInt32(sqlCmd.Parameters["@DraftAutoId"].Value);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later";
            }
        }
    }

    public class BL_OrderMaster
    {
        public static void bindAllDropdown(PL_OrderMaster pobj)
        {
            pobj.Opcode = 4011;
            DL_OrderMaster.ReturnTable(pobj);
        }
      
        public static void editOrderDetails(PL_OrderMaster pobj)
        {
            pobj.Opcode = 406;
            DL_OrderMaster.ReturnTable(pobj);
        }
      
        public static void bindUnitType(PL_OrderMaster pobj)
        {
            pobj.Opcode = 404;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void selectQtyPrice(PL_OrderMaster pobj)
        {
            pobj.Opcode = 403;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void SaveDraftOrder(PL_OrderMaster pobj)
        {
            pobj.Opcode = 108;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void UpdateDraftReq(PL_OrderMaster pobj)
        {
            pobj.Opcode = 206;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void DeleteDraftItem(PL_OrderMaster pobj)
        {
            pobj.Opcode = 212;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void ItemTypeUpdate(PL_OrderMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcOrderMasterWeb", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", 213);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.Add("@DraftAutoId", SqlDbType.VarChar, 20).Value = pobj.DraftAutoId;
                sqlCmd.Parameters["@DraftAutoId"].Direction = ParameterDirection.InputOutput;

                sqlCmd.Parameters.AddWithValue("@IsExchange", pobj.IsExchange);
                sqlCmd.Parameters.AddWithValue("@IsFreeItem", pobj.IsFreeItem);
                sqlCmd.Parameters.AddWithValue("@IsTaxable", pobj.IsTaxable);


                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.DraftAutoId = Convert.ToInt32(sqlCmd.Parameters["@DraftAutoId"].Value);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
        public static void AddProductQty(PL_OrderMaster pobj)
        {
            pobj.Opcode = 427;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void updateOrder(PL_OrderMaster pobj)
        {
            pobj.Opcode = 201;
            DL_OrderMaster.ReturnTable(pobj);
        }
        
        public static void cancelOrder(PL_OrderMaster pobj)
        {
            pobj.Opcode = 204;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void GetBarDetails(PL_OrderMaster pobj)
        {
            pobj.Opcode = 423;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void clickonSecurity(PL_OrderMaster pobj)
        {
            pobj.Opcode = 431;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void approveOrder(PL_OrderMaster pobj)
        {
            pobj.Opcode = 502;
            DL_OrderMaster.ReturnTable(pobj);
        }
        public static void bindProductDropdown(PL_OrderMaster pobj)
        {
            pobj.Opcode = 503;
            DL_OrderMaster.ReturnTable(pobj);
        }
    }

}
