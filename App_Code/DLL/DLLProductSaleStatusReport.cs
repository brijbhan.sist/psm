﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DllUtility;
using System.Data;

namespace DLLProductSaleStatusReport
{
    public class PL_ProductSaleStatusReport : Utility
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int ProductAutoId { get; set; }
        public int CustomerAutoId { get; set; }
        public string SalesPersonAutoId { get; set; }
    }
    public class DL_ProductSaleStatusReport
    {
        public static void ReturnTable(PL_ProductSaleStatusReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("Product_Report_By_Status_SalesRep", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@fromdate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@todate", pobj.ToDate);
                }
                sqlCmd.Parameters.AddWithValue("@productid", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@salespersonautoid", pobj.SalesPersonAutoId);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);

                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_ProductSaleStatusReport
    {
        public static void BindReport(PL_ProductSaleStatusReport pobj)
        {
            pobj.Opcode = 41;
            DL_ProductSaleStatusReport.ReturnTable(pobj);
        }
        public static void bindDropDown(PL_ProductSaleStatusReport pobj)
        {
            pobj.Opcode = 42;
            DL_ProductSaleStatusReport.ReturnTable(pobj);
        }
        public static void bindCustomer(PL_ProductSaleStatusReport pobj)
        {
            pobj.Opcode = 43;
            DL_ProductSaleStatusReport.ReturnTable(pobj);
        }
    }
}