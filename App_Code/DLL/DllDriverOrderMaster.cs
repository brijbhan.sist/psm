﻿using System;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
namespace DllDriverOrderMaster
{
    public class PL_DriverOrderMaster : Utility
    {
        public int OrderAutoId { get; set; }
        public string OrderNo { get; set; }
        public int DrvAutoId { get; set; }
        public DateTime Fromdate { get; set; }
        public DateTime Todate { get; set; }
        public int CustomerAutoId { get; set; }
        public int SalesPersonAutoId { get; set; }
        public int OrderStatus { get; set; }
        public int EmpAutoId { get; set; }
        public string Remarks { get; set; }
        public string Delivered { get; set; }
        public int LoginEmpType { get; set; }
        public string DocumentDetails { get; set; }
        public string Barcode { get; set; }




    }
    public class DL_DriverOrderMaster
    {
        public static void ReturnTable(PL_DriverOrderMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcDriverOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@DriverAutoId", pobj.DrvAutoId);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@Delivered", pobj.Delivered);
                sqlCmd.Parameters.AddWithValue("@DocumentDetails", pobj.DocumentDetails);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);


                if (pobj.Fromdate != DateTime.MinValue && pobj.Fromdate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Fromdate", pobj.Fromdate);
                }
                if (pobj.Todate != DateTime.MinValue && pobj.Todate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Todate", pobj.Todate);
                }
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                sqlCmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_DriverOrderMaster
    {
        public static void getDrvAsgnOrder(PL_DriverOrderMaster pobj)
        {
            pobj.Opcode = 415;
            DL_DriverOrderMaster.ReturnTable(pobj);
        }
        public static void Pick_Order(PL_DriverOrderMaster pobj)
        {
            pobj.Opcode = 416;
            DL_DriverOrderMaster.ReturnTable(pobj);
        }
        public static void saveDelStatus(PL_DriverOrderMaster pobj)
        {
            pobj.Opcode = 107;
            DL_DriverOrderMaster.ReturnTable(pobj);
        }
        public static void loadbarcodelog(PL_DriverOrderMaster pobj)
        {
            pobj.Opcode = 108;
            DL_DriverOrderMaster.ReturnTable(pobj);
        }
        public static void unloadbarcodelog(PL_DriverOrderMaster pobj)
        {
            pobj.Opcode = 109;
            DL_DriverOrderMaster.ReturnTable(pobj);
        }
        public static void viewOrder(PL_DriverOrderMaster pobj)
        {
            pobj.Opcode = 428;
            DL_DriverOrderMaster.ReturnTable(pobj);
        }
        public static void bindDropdown(PL_DriverOrderMaster pobj)
        {
            pobj.Opcode = 407;
            DL_DriverOrderMaster.ReturnTable(pobj);
        }
        public static void bindCustomerSalesPersonWise(PL_DriverOrderMaster pobj)
        {
            pobj.Opcode = 408;
            DL_DriverOrderMaster.ReturnTable(pobj);
        }
    }
}