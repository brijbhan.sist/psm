﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using DllUtility;

/// <summary>
/// Summary description for DLLOrderSubmitTimeReport
/// </summary>
namespace DLLOrderSubmitTimeReport
{
  public class PL_OrderSubmitTimeReport : Utility
	{
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int CustomerAutoId { get; set; }
        public int SalePersonAutoId { get; set; }
        public int RouteStatus { get; set; }
	}
  public class DL_OrderSubmitTimeReport
    {
        public static void ReturnData(PL_OrderSubmitTimeReport pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[ProcOrderSubmitTimeReport]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                cmd.Parameters.AddWithValue("@SalePersonAutoId", pobj.SalePersonAutoId);
                cmd.Parameters.AddWithValue("@RouteStatus", pobj.RouteStatus);




                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@DateFrom", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@DateTo", pobj.ToDate);
                }

                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
  public  class BL_OrderSubmitTimeReport
    {
        public static void GetOrderSubmit(PL_OrderSubmitTimeReport pobj)
        {
            pobj.Opcode = 41;
            DL_OrderSubmitTimeReport.ReturnData(pobj);
        }
        public static void GetCustomer(PL_OrderSubmitTimeReport pobj)
        {
            pobj.Opcode = 42;
            DL_OrderSubmitTimeReport.ReturnData(pobj);
        }
    }
}