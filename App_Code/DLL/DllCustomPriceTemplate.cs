﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data;
using System.Data.SqlClient;

namespace DllCustomPriceTemplate
{
    public class PL_PriceLevel : Utility
    {
        public int EmpAutoId { get; set; }
        public string PriceLevelId { get; set; }
        public int PriceLevelAutoId { get; set; }
        public int ActionType { get; set; }
        public string PriceLevelName { get; set; }
        public int Status { get; set; }
        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public decimal CustomPrice { get; set; }
        public int CategoryAutoId { get; set; }
        public int SubcategoryAutoId { get; set; }
        public string ProductName { get; set; }
        public int CustomerType { get; set; }
    }

    public class DL_PriceLevel
    {
        public static void ReturnTable(PL_PriceLevel pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPriceLevelMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@PriceLevelId", pobj.PriceLevelId);
                sqlCmd.Parameters.AddWithValue("@PriceLevelAutoId", pobj.PriceLevelAutoId);
                sqlCmd.Parameters.AddWithValue("@PriceLevelName", pobj.PriceLevelName);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@CustomPrice", pobj.CustomPrice);  
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@SubcategoryAutoId", pobj.SubcategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductName", pobj.ProductName);
                sqlCmd.Parameters.AddWithValue("@ActionType", pobj.ActionType);
                sqlCmd.Parameters.AddWithValue("@CustomerType", pobj.CustomerType);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }         
    }

    public class BL_PriceLevel
    {
        public static void getProductList(PL_PriceLevel pobj)
        {
            pobj.Opcode = 41;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void BindStatus(PL_PriceLevel pobj)
        {
            pobj.Opcode = 43;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void SavePriceLevel(PL_PriceLevel pobj)
        {
            pobj.Opcode = 11;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void EditPriceLevel(PL_PriceLevel pobj)
        {
            pobj.Opcode = 42;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void AddProduct(PL_PriceLevel pobj)
        {
            pobj.Opcode = 12;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void RemoveProduct(PL_PriceLevel pobj)
        {
            pobj.Opcode = 31;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void checkMarkProduct(PL_PriceLevel pobj)
        {
            pobj.Opcode = 44;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void SavingProductPrice(PL_PriceLevel pobj)
        {
            pobj.Opcode = 21;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void UpdatePriceLevel(PL_PriceLevel pobj)
        {
            pobj.Opcode = 22;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void BindSubCategory(PL_PriceLevel pobj)
        {
            pobj.Opcode = 45;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void BindPriceLevel(PL_PriceLevel pobj)
        {
            pobj.Opcode = 46;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void AddAllProduct(PL_PriceLevel pobj)
        {
            pobj.Opcode = 14;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void PrintPriceLevel(PL_PriceLevel pobj)
        {
            pobj.Opcode = 47;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void ProductPriceLevelLog(PL_PriceLevel pobj)
        {
            pobj.Opcode = 48;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void PrintPriceLevel2(PL_PriceLevel pobj)
        {
            pobj.Opcode = 49;
            DL_PriceLevel.ReturnTable(pobj);
        }
    }
}
