﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DllOP_SalesBySalesPerson
/// </summary>
namespace DllOP_SalesBySalesPerson
{
    public class PL_OP_SalesBySalesPerson : Utility
    {
        public string SalesPerson { get; set; }
        public int SalesPersonAutoId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

    }
    public class DL_OP_SalesBySalesPerson
    {
        public static void ReturnData(PL_OP_SalesBySalesPerson pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[ProcOP_SalesBySalePerson]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                cmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);

                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }

                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }

        }
    }
    public class BL_OP_SalesBySalesPerson
    {
        public static void BindSalesPerson(PL_OP_SalesBySalesPerson pobj)
        {
            pobj.Opcode = 41;
            DL_OP_SalesBySalesPerson.ReturnData(pobj);
        }
        public static void SaleBySalesPersonDetail(PL_OP_SalesBySalesPerson pobj)
        {
            pobj.Opcode = 42;
            DL_OP_SalesBySalesPerson.ReturnData(pobj);
        }
    }
}