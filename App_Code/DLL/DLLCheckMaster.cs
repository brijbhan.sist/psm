﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllCheckMaster
{
    public class PL_CheckMaster : Utility
    {
        public int EmpTypeAutoId { get; set; }
        public int Type { get; set; }
        public string SecurityKey { get; set; }
        public int CheckAutoId { get; set; }
        public string CheckId { get; set; }
        public int VendorAutoId { get; set; }
        public int EmpAutoId { get; set; }
        public string VendorAddress { get; set; }
        public decimal CheckAmount { get; set; }
        public DateTime CheckDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Memo { get; set; }
        public int Status { get; set; }
    }
    public class DL_CheckMaster
    {
        public static void ReturnTable(PL_CheckMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcCheckMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@Type", pobj.Type);
                sqlCmd.Parameters.AddWithValue("@CheckAutoId", pobj.CheckAutoId);
                sqlCmd.Parameters.AddWithValue("@SecurityKey", pobj.SecurityKey);
                sqlCmd.Parameters.AddWithValue("@EmpType", pobj.EmpTypeAutoId);
                sqlCmd.Parameters.AddWithValue("@CheckId", pobj.CheckId);
                sqlCmd.Parameters.AddWithValue("@VendorAutoId", pobj.VendorAutoId);
                sqlCmd.Parameters.AddWithValue("@CheckAmount", pobj.CheckAmount);
                if (pobj.CheckDate > DateTime.MinValue && pobj.CheckDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@CheckDate", pobj.CheckDate);
                }
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                sqlCmd.Parameters.AddWithValue("@VendorAddress", pobj.VendorAddress);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@Memo", pobj.Memo);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_CheckMaster
    {
        public static void insert(PL_CheckMaster pobj)
        {
            pobj.Opcode = 11;
            DL_CheckMaster.ReturnTable(pobj);
        }
        public static void update(PL_CheckMaster pobj)
        {
            pobj.Opcode = 21;
            DL_CheckMaster.ReturnTable(pobj);
        }
        public static void delete(PL_CheckMaster pobj)
        {
            pobj.Opcode = 31;
            DL_CheckMaster.ReturnTable(pobj);
        }
        public static void select(PL_CheckMaster pobj)
        {
            pobj.Opcode = 41;
            DL_CheckMaster.ReturnTable(pobj);
        }
        public static void editCheckMaster(PL_CheckMaster pobj)
        {
            pobj.Opcode = 42;
            DL_CheckMaster.ReturnTable(pobj);
        }
        public static void Vendorselect(PL_CheckMaster pobj)
        {
            pobj.Opcode = 43;
            DL_CheckMaster.ReturnTable(pobj);
        }
        public static void getPrintDetails(PL_CheckMaster pobj)
        {
            pobj.Opcode = 44;
            DL_CheckMaster.ReturnTable(pobj);
        }
        public static void checkSecurity(PL_CheckMaster pobj)
        {
            pobj.Opcode = 45;
            DL_CheckMaster.ReturnTable(pobj);
        }
        public static void bindVendor(PL_CheckMaster pobj)
        {
            pobj.Opcode = 46;
            DL_CheckMaster.ReturnTable(pobj);
        }
        public static void getCheckAmount(PL_CheckMaster pobj)
        {
            pobj.Opcode = 47;
            DL_CheckMaster.ReturnTable(pobj);
        }
    }
}
