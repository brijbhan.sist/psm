﻿using System;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
namespace ManageIP
{
    public class PL_ManageIP : Utility
    {

        public int EmpAutoId { get; set; }
        public int AutoId { get; set; }
        public string userIpAddress { get; set; }
        public string description { get; set; }
        public int isDefault { get; set; }




    }
    public class DL_ManageIP
    {
        public static void ReturnTable(PL_ManageIP pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcIPMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
               
                sqlCmd.Parameters.AddWithValue("@userIpAddress", pobj.userIpAddress);
                sqlCmd.Parameters.AddWithValue("@description", pobj.description);

                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@isDefault", pobj.isDefault);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_ManageIP
    {
        public static void saveIpAddress(PL_ManageIP pobj)
        {
            pobj.Opcode = 11;
            DL_ManageIP.ReturnTable(pobj);
        }
        public static void select(PL_ManageIP pobj)
        {
            pobj.Opcode = 41;
            DL_ManageIP.ReturnTable(pobj);
        }
        public static void GetIPDetail(PL_ManageIP pobj)
        {
            pobj.Opcode = 42;
            DL_ManageIP.ReturnTable(pobj);
        }
        public static void UpdateIpAddress(PL_ManageIP pobj)
        {
            pobj.Opcode = 21;
            DL_ManageIP.ReturnTable(pobj);
        }
        public static void DeleteIp(PL_ManageIP pobj)
        {
            pobj.Opcode = 31;
            DL_ManageIP.ReturnTable(pobj);
        }

    }
}