﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DllUtility;
/// <summary>
/// Summary description for DLLIPAddressMaster
/// </summary>
namespace DLLIPAddressMaster
{
    public class PL_IPAddressMaster: Utility
    {
      
            public int AutoId { get; set; }
            public int EmpAutoId { get; set; }
            public string userIPAddress { get; set; }
            public string NameofLocation { get; set; }
            public string Description { get; set; }          
    }
    public class DL_IPAddressMaster
    {
        public static void ReturnTable(PL_IPAddressMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcIPAddressMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@IPAddress", pobj.userIPAddress);
                sqlCmd.Parameters.AddWithValue("@NameofLocation", pobj.NameofLocation);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@Description", pobj.Description);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_IPAddressMaster
    {
        public static void insert(PL_IPAddressMaster pobj)
        {
            pobj.Opcode = 11;
            DL_IPAddressMaster.ReturnTable(pobj);
        }
        public static void update(PL_IPAddressMaster pobj)
        {
            pobj.Opcode = 21;
            DL_IPAddressMaster.ReturnTable(pobj);
        }
        public static void delete(PL_IPAddressMaster pobj)
        {
            pobj.Opcode = 31;
            DL_IPAddressMaster.ReturnTable(pobj);
        }
        public static void select(PL_IPAddressMaster pobj)
        {
            pobj.Opcode = 41;
            DL_IPAddressMaster.ReturnTable(pobj);
        }
        public static void bindlist(PL_IPAddressMaster pobj)
        {
            pobj.Opcode = 42;
            DL_IPAddressMaster.ReturnTable(pobj);
        }
    }
}