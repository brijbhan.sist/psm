﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DLLSalesReportByBrand
/// </summary>
namespace DLLSalesReportByBrand
{
    public class PL_SalesReportByBrand : Utility
    {
        public string SalesPerson { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

    }
    public class BL_SalesReportByBrand
    {
        public static void getSalesPerson(PL_SalesReportByBrand pobj)
        {
            pobj.Opcode = 41;
            DL_ReadytoShipReport.ReturnData(pobj);
        }
        public static void SalesReportByBrand(PL_SalesReportByBrand pobj)
        {
            pobj.Opcode = 42;
            DL_ReadytoShipReport.ReturnData(pobj);
        }
    }

    public class DL_ReadytoShipReport
    {
        public static void ReturnData(PL_SalesReportByBrand pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[Proc_SalesReportByBrand]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@Fromdate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@Todate", pobj.ToDate);
                }
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
}