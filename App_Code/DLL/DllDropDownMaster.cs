﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace DllDropDownMaster
{
    public class PL_DropDownMaster:Utility
    {
        public string StatusCategory { get; set; }
    }
    public class DL_DropDownMaster
    {
        public static void ReturnTable(PL_DropDownMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcDropDownMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@StatusCategory", pobj.StatusCategory);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_DropDownMaster
    {
        public static void BindVendor(PL_DropDownMaster pobj)
        {
            pobj.Opcode = 401;
            DL_DropDownMaster.ReturnTable(pobj);
        }
        public static void BindProduct(PL_DropDownMaster pobj)
        {
            pobj.Opcode = 402;
            DL_DropDownMaster.ReturnTable(pobj);
        }
        public static void BindStatus(PL_DropDownMaster pobj)
        {
            pobj.Opcode = 403;
            DL_DropDownMaster.ReturnTable(pobj);
        }
        public static void BindAllOrderType(PL_DropDownMaster pobj)
        {
            pobj.Opcode = 404;
            DL_DropDownMaster.ReturnTable(pobj);
        }
        public static void BindBrand(PL_DropDownMaster pobj)
        {
            pobj.Opcode = 405;
            DL_DropDownMaster.ReturnTable(pobj);
        }
    }
}