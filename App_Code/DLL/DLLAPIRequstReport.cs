﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLSaleReport
/// </summary>
/// 
namespace DLLAPIRequstReport
{
    public class PL_APIRequstReport : Utility
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string User { get; set; }
        public int Method { get; set; }
        public string AppVer { get; set; }
        public string DeviceName { get; set; }

    }
    public class BL_APIRequstReport
    {

        public static void BindUsers(PL_APIRequstReport pobj)
        {
            pobj.Opcode = 41;
            DL_APIRequstReport.ReturnData(pobj);
        }
        public static void GetData(PL_APIRequstReport pobj)
        {
            pobj.Opcode = 42;
            DL_APIRequstReport.ReturnData(pobj);
        }
    }

    public class DL_APIRequstReport
    {
        public static void ReturnData(PL_APIRequstReport pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("ProcApiLogReport", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@User", pobj.User);
                cmd.Parameters.AddWithValue("@Method", pobj.Method);
                cmd.Parameters.AddWithValue("@AppV", pobj.AppVer);  
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.AddWithValue("@DeviceName", pobj.DeviceName);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
}