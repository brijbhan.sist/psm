﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllCustomer
{
    public class PL_Customer : Utility
    {
        public string XmlContactPerson { get; set; }
        public string CancelRemark { get; set; }
        public string SecurityKey { get; set; }
        public int LocationAutoId { get; set; }
        public string LocationName { get; set; }
        public int CustomerAutoId { get; set; }
        public int CityAutoId { get; set; }
        public string StateName { get; set; }
        public string StateName2 { get; set; }
        public int CityAutoId2 { get; set; }
        public int OrderAutoId { get; set; }
        public int PaymentAutoId { get; set; }
        public string CustomerId { get; set; }
        public string OrderNo { get; set; }
        public string CheckNo { get; set; }
        public DateTime CheckDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string StoreOpenTime { get; set; }
        public string StoreCloseTime { get; set; }
        public string CustomerName { get; set; }
        public string BusinessName { get; set; }
        public string OPTLicence { get; set; }
        public string DocumentName { get; set; }
        public string DocumentURL { get; set; }
        public int CustType { get; set; }
        public string Email { get; set; }
        public string AltEmail { get; set; }
        public string Contact1 { get; set; }
        public string Contact2 { get; set; }
        public int BillAddAutoId { get; set; }
        public int ShipAddAutoId { get; set; }

        public string BillAdd { get; set; }
        public string BillAdd2 { get; set; }
        public int State1 { get; set; }
        public string City1 { get; set; }
        public string Zipcode1 { get; set; }
        public int ZipAutoId { get; set; }
        public string ShipAdd { get; set; }
        public string ShipAdd2 { get; set; }
        public int State2 { get; set; }
        public string City2 { get; set; }
        public string Zipcode2 { get; set; }
        public int ZipAutoId2 { get; set; }
        public int Terms { get; set; }
        public string MobileNo { get; set; }
        public string FaxNo { get; set; }
        public string TaxId { get; set; }

        public int PriceLevelAutoId { get; set; }
        public int EmpAutoId { get; set; }
        public int FileAutoId { get; set; }

        public int Status { get; set; }
        public int SalesPersonAutoId { get; set; }
        public string ContactPersonName { get; set; }
        public string PaymentId { get; set; }
        public decimal ReceivedAmount { get; set; }
        public int PaymentMethod { get; set; }
        public string ReferenceNo { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string latitude1 { get; set; }
        public string longitude1 { get; set; }
        public string PaymentRemarks { get; set; }
        public int ReceivedPaymentBy { get; set; }
        public string EmployeeRemarks { get; set; }
        public decimal StoreCredit { get; set; }
        public DataTable TableValue { get; set; }
        public decimal SortAmount { get; set; }
        public decimal TotalCurrencyAmount { get; set; }
        public string PaymentCurrencyXml { get; set; }

        public string BankName { get; set; }
        public string BankAcc { get; set; }
        public int CardType { get; set; }
        public string CardNo { get; set; }
        public string ExpiryDate { get; set; }
        public string CVV { get; set; }
        public int AutoId { get; set; }
        public string RoutingNo { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int IsAppLogin { get; set; }
        public string ZipCode { get; set; }
        public int Type { get; set; }
        public string Landline { get; set; }
        public string Landline2 { get; set; }
        public string CustomerRemarks { get; set; }
        public int IsDefault { get; set; }


    }
    public class DL_Customer
    {
        public static void ReturnTable(PL_Customer pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcCustomerMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@XmlContactPerson", pobj.XmlContactPerson);
                sqlCmd.Parameters.AddWithValue("@LocationName", pobj.LocationName);
                sqlCmd.Parameters.AddWithValue("@LocationAutoId", pobj.LocationAutoId);
                sqlCmd.Parameters.Add("@CustomerAutoId",SqlDbType.Int).Value=pobj.CustomerAutoId;
                sqlCmd.Parameters.AddWithValue("@CityAutoId", pobj.CityAutoId);
                sqlCmd.Parameters.AddWithValue("@CityAutoId2", pobj.CityAutoId2);
                sqlCmd.Parameters.AddWithValue("@ZipAutoId", pobj.ZipAutoId);
                sqlCmd.Parameters.AddWithValue("@ZipAutoId2", pobj.ZipAutoId2);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@FileAutoId", pobj.FileAutoId);
                sqlCmd.Parameters.AddWithValue("@PaymentAutoId", pobj.PaymentAutoId);
                sqlCmd.Parameters.AddWithValue("@CancelRemark", pobj.CancelRemark);
                sqlCmd.Parameters.AddWithValue("@StoreCredit", pobj.StoreCredit);
                sqlCmd.Parameters.AddWithValue("@CustomerId", pobj.CustomerId);
                sqlCmd.Parameters.AddWithValue("@DocumentName", pobj.DocumentName);
                sqlCmd.Parameters.AddWithValue("@DocumentURL", pobj.DocumentURL);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                sqlCmd.Parameters.AddWithValue("@CustomerName", pobj.CustomerName);
                sqlCmd.Parameters.AddWithValue("@BusinessName", pobj.BusinessName);
                sqlCmd.Parameters.AddWithValue("@OPTLicence", pobj.OPTLicence);
                sqlCmd.Parameters.AddWithValue("@CustomerType", pobj.CustType);
                sqlCmd.Parameters.AddWithValue("@SecurityKey", pobj.SecurityKey);
                sqlCmd.Parameters.AddWithValue("@CheckNo", pobj.CheckNo);
                sqlCmd.Parameters.AddWithValue("@BankName", pobj.BankName);
                sqlCmd.Parameters.AddWithValue("@BankAcc", pobj.BankAcc);
                sqlCmd.Parameters.AddWithValue("@CardType", pobj.CardType);
                sqlCmd.Parameters.AddWithValue("@CardNo", pobj.CardNo);
                sqlCmd.Parameters.AddWithValue("@CVV", pobj.CVV);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@Latitude", pobj.latitude);
                sqlCmd.Parameters.AddWithValue("@Longitude", pobj.longitude);
                sqlCmd.Parameters.AddWithValue("@Zipcode", pobj.ZipCode);
                sqlCmd.Parameters.AddWithValue("@Latitude1", pobj.latitude1);
                sqlCmd.Parameters.AddWithValue("@Longitude1", pobj.longitude1);
                sqlCmd.Parameters.AddWithValue("@Type", pobj.Type);
                sqlCmd.Parameters.AddWithValue("@Landline", pobj.Landline);
                sqlCmd.Parameters.AddWithValue("@Landline2", pobj.Landline2);
                sqlCmd.Parameters.AddWithValue("@StateName", pobj.StateName);
                sqlCmd.Parameters.AddWithValue("@StateName2", pobj.StateName2);

                if (pobj.CheckDate > DateTime.MinValue && pobj.CheckDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@CheckDate", pobj.CheckDate);
                }

                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
        
                sqlCmd.Parameters.AddWithValue("@StoreOpenTime", pobj.StoreOpenTime);
                sqlCmd.Parameters.AddWithValue("@StoreCloseTime", pobj.StoreCloseTime);
                sqlCmd.Parameters.AddWithValue("@ExpiryDate", pobj.ExpiryDate);
                sqlCmd.Parameters.AddWithValue("@BillAddAutoId", pobj.BillAddAutoId);
                sqlCmd.Parameters.AddWithValue("@BillAdd", pobj.BillAdd);
                sqlCmd.Parameters.AddWithValue("@State1", pobj.State1);
                sqlCmd.Parameters.AddWithValue("@City1", pobj.City1);
                sqlCmd.Parameters.AddWithValue("@Zipcode1", pobj.Zipcode1);
                sqlCmd.Parameters.AddWithValue("@ShipAddAutoId", pobj.ShipAddAutoId);
                sqlCmd.Parameters.AddWithValue("@ShipAdd", pobj.ShipAdd);
                sqlCmd.Parameters.AddWithValue("@State2", pobj.State2);
                sqlCmd.Parameters.AddWithValue("@City2", pobj.City2);
                sqlCmd.Parameters.AddWithValue("@Zipcode2", pobj.Zipcode2);
                sqlCmd.Parameters.AddWithValue("@Terms", pobj.Terms);
                sqlCmd.Parameters.AddWithValue("@MobileNo", pobj.MobileNo);
                sqlCmd.Parameters.AddWithValue("@FaxNo", pobj.FaxNo);
                sqlCmd.Parameters.AddWithValue("@TaxId", pobj.TaxId);
                sqlCmd.Parameters.AddWithValue("@Email", pobj.Email);
                sqlCmd.Parameters.AddWithValue("@AltEmail", pobj.AltEmail);
                sqlCmd.Parameters.AddWithValue("@Contact1", pobj.Contact1);
                sqlCmd.Parameters.AddWithValue("@Contact2", pobj.Contact2);
                sqlCmd.Parameters.AddWithValue("@SortAmount", pobj.SortAmount);
                sqlCmd.Parameters.AddWithValue("@TotalCurrencyAmount", pobj.TotalCurrencyAmount);
                sqlCmd.Parameters.AddWithValue("@PaymentCurrencyXml", pobj.PaymentCurrencyXml);

                if (pobj.PriceLevelAutoId != 0)
                {
                    sqlCmd.Parameters.AddWithValue("@PriceLevelAutoId", pobj.PriceLevelAutoId);
                }

                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);

                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                if (pobj.SalesPersonAutoId != 0)
                {
                    sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                }
                sqlCmd.Parameters.AddWithValue("@ContactPersonName", pobj.ContactPersonName);
                sqlCmd.Parameters.AddWithValue("@PaymentId", pobj.PaymentId);
                sqlCmd.Parameters.AddWithValue("@ReceivedAmount", pobj.ReceivedAmount);
                sqlCmd.Parameters.AddWithValue("@PaymentMethod", pobj.PaymentMethod);
                sqlCmd.Parameters.AddWithValue("@ReferenceNo", pobj.ReferenceNo);
                sqlCmd.Parameters.AddWithValue("@PaymentRemarks", pobj.PaymentRemarks);
                sqlCmd.Parameters.AddWithValue("@ReceivedPaymentBy", pobj.ReceivedPaymentBy);
                sqlCmd.Parameters.AddWithValue("@EmployeeRemarks", pobj.EmployeeRemarks);
                sqlCmd.Parameters.AddWithValue("@TableValue", pobj.TableValue);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.AddWithValue("@RoutingNo", pobj.RoutingNo);
                sqlCmd.Parameters.AddWithValue("@UserName", pobj.UserName);
                sqlCmd.Parameters.AddWithValue("@Password", pobj.Password);
                sqlCmd.Parameters.AddWithValue("@IsAppLogin", pobj.IsAppLogin); 
                    sqlCmd.Parameters.AddWithValue("@CustomerRemark", pobj.CustomerRemarks);
                sqlCmd.Parameters.AddWithValue("@BillAdd2", pobj.BillAdd2);
                sqlCmd.Parameters.AddWithValue("@ShipAdd2", pobj.ShipAdd2);
                sqlCmd.Parameters.AddWithValue("@IsDefault", pobj.IsDefault);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }
    }
    public class BL_Customer
    {
        public static void insert(PL_Customer pobj)
        {
            pobj.Opcode = 11;
            DL_Customer.ReturnTable(pobj);
        }        
        public static void update(PL_Customer pobj)
        {
            pobj.Opcode = 21;
            DL_Customer.ReturnTable(pobj);
        }
       
        public static void delete(PL_Customer pobj)
        {
            pobj.Opcode = 31;
            DL_Customer.ReturnTable(pobj);
        }
        public static void DeleteDocument(PL_Customer pobj)
        {
            pobj.Opcode = 32;
            DL_Customer.ReturnTable(pobj);
        }      
        public static void edit(PL_Customer pobj)
        {
            pobj.Opcode = 42;
            DL_Customer.ReturnTable(pobj);
        }
        
        public static void bindDropDownsList(PL_Customer pobj)
        {
            pobj.Opcode = 43;
            DL_Customer.ReturnTable(pobj);
        }
        public static void editMain(PL_Customer pobj)
        {
            pobj.Opcode = 44;
            DL_Customer.ReturnTable(pobj);
        }
        public static void CustomerOrderList(PL_Customer pobj)
        {
            pobj.Opcode = 45;
            DL_Customer.ReturnTable(pobj);
        }
        public static void CustomerDuePayment(PL_Customer pobj)
        {
            pobj.Opcode = 46;
            DL_Customer.ReturnTable(pobj);
        }
        public static void CustomerPaymentList(PL_Customer pobj)
        {
            pobj.Opcode = 47;
            DL_Customer.ReturnTable(pobj);
        }
        public static void CustomerPaymentDetails(PL_Customer pobj)
        {
            pobj.Opcode = 48;
            DL_Customer.ReturnTable(pobj);
        }
        public static void CustomerLogReport(PL_Customer pobj)
        {
            pobj.Opcode = 49;
            DL_Customer.ReturnTable(pobj);
        }
        
        public static void DocumentSave(PL_Customer pobj)
        {
            pobj.Opcode = 23;
            DL_Customer.ReturnTable(pobj);
        }
        public static void UpdateDocument(PL_Customer pobj)
        {
            pobj.Opcode = 24;
            DL_Customer.ReturnTable(pobj);
        }
      
        public static void CancelledPayment(PL_Customer pobj)
        {
            pobj.Opcode = 25;
            DL_Customer.ReturnTable(pobj);
        }
        public static void Savecheck(PL_Customer pobj)
        {
            pobj.Opcode = 26;
            DL_Customer.ReturnTable(pobj);
        }       
      
        public static void EditPaymentDetails(PL_Customer pobj)
        {
            pobj.Opcode = 411;
            DL_Customer.ReturnTable(pobj);
        }
        public static void CustomerDocument(PL_Customer pobj)
        {
            pobj.Opcode = 412;
            DL_Customer.ReturnTable(pobj);
        }       
        public static void CollectionDetails(PL_Customer pobj)
        {
            pobj.Opcode = 413;
            DL_Customer.ReturnTable(pobj);
        }
        public static void PaymentDetails(PL_Customer pobj)
        {
            pobj.Opcode = 414;
            DL_Customer.ReturnTable(pobj);

        }
        public static void StoreCreditDetails(PL_Customer pobj)
        {
            pobj.Opcode = 415;
            DL_Customer.ReturnTable(pobj);
        }
        public static void StoreCreditLog(PL_Customer pobj)
        {
            pobj.Opcode = 416;
            DL_Customer.ReturnTable(pobj);
        }
        public static void orderList(PL_Customer pobj)
        {
            pobj.Opcode = 417;
            DL_Customer.ReturnTable(pobj);
        }
       
        public static void BindCity(PL_Customer pobj)
        {
            pobj.Opcode = 418;
            DL_Customer.ReturnTable(pobj);
        }
        public static void BindPinCode(PL_Customer pobj)
        {
            pobj.Opcode = 419;
            DL_Customer.ReturnTable(pobj);
        }
        public static void BindPinCodeForCity(PL_Customer pobj)
        {
            pobj.Opcode = 420;
            DL_Customer.ReturnTable(pobj);
        }

        public static void bindPriceLevel(PL_Customer pobj)
        {
            pobj.Opcode = 421;
            DL_Customer.ReturnTable(pobj);
        }
        public static void GetLocation(PL_Customer pobj)
        {
            pobj.Opcode = 422;
            DL_Customer.ReturnTable(pobj);
        }
        public static void checkSecurity(PL_Customer pobj)
        {
            pobj.Opcode = 423;
            DL_Customer.ReturnTable(pobj);
        }

        public static void CancelPayment(PL_Customer pobj)
        {
            pobj.Opcode = 424;
            DL_Customer.ReturnTable(pobj);
        }
        public static void bindCardType(PL_Customer pobj)
        {
            pobj.Opcode = 425;
            DL_Customer.ReturnTable(pobj);
        }
        public static void SaveBankDetails(PL_Customer pobj)
        {
            pobj.Opcode = 426;
            DL_Customer.ReturnTable(pobj);
        }
        public static void GetBankDetailsList(PL_Customer pobj)
        {
            pobj.Opcode = 427;
            DL_Customer.ReturnTable(pobj);
        }
        public static void EditBankDetails(PL_Customer pobj)
        {
            pobj.Opcode = 428;
            DL_Customer.ReturnTable(pobj);
        }
        public static void UpdateBankDetails(PL_Customer pobj)
        {
            pobj.Opcode = 429;
            DL_Customer.ReturnTable(pobj);
        }
        public static void DeleteBankDetails(PL_Customer pobj)
        {
            pobj.Opcode = 430;
            DL_Customer.ReturnTable(pobj);
        }
        public static void BindStateCity(PL_Customer pobj)//In case of multiple city
        {
            pobj.Opcode = 431;
            DL_Customer.ReturnTable(pobj);
        }
        public static void checkZipcode(PL_Customer pobj)
        {
            pobj.Opcode = 432;
            DL_Customer.ReturnTable(pobj);
        }
        public static void bindContactPersonType(PL_Customer pobj)
        {
            pobj.Opcode = 433;
            DL_Customer.ReturnTable(pobj);
        }
        public static void GetContactPersonList(PL_Customer pobj)
        {
            pobj.Opcode = 434;
            DL_Customer.ReturnTable(pobj);
        }
        public static void saveContactDetail(PL_Customer pobj)
        {
            pobj.Opcode = 435;
            DL_Customer.ReturnTable(pobj);
        }
        public static void updateContactDetail(PL_Customer pobj)
        {
            pobj.Opcode = 436;
            DL_Customer.ReturnTable(pobj);
        }
        public static void deleteCustomerContactPerson(PL_Customer pobj)
        {
            pobj.Opcode = 437;
            DL_Customer.ReturnTable(pobj);
        }
    }
}
