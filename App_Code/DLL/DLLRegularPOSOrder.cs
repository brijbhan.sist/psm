﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DllUtility;


namespace DLLRegularPOSOrder
{
    public class PL_RegularPOSOrder : Utility
    {
        public int OrderAutoId { get; set; }
        public string OrderNo { get; set; }
        public decimal Adjustment { get; set; }
        public DateTime OrderDate { get; set; }
        public int CustomerAutoId { get; set; }
        public int BillingAutoId { get; set; }
        public int DraftAutoId { get; set; }
        public int ShippingAutoId { get; set; }
        public int IsExchange { get; set; }
        public int IsTaxable { get; set; }
        public int IsFreeItem { get; set; }
        public int TaxType { get; set; }
        public decimal MLQty { get; set; }
        public int OrderStatus { get; set; }
        public int PaymentMenthod { get; set; }
        public string ReferenceNo { get; set; }
        public string CreditNo { get; set; }
        public int SalesPersonAutoId { get; set; }
        public int LoginEmpType { get; set; }
        public int EmpAutoId { get; set; }
        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int ReqQty { get; set; }
        public int QtyPerUnit { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal minprice { get; set; }
        public decimal SRP { get; set; }
        public decimal GP { get; set; }
        public decimal TaxRate { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal OverallDisc { get; set; }
        public decimal OverallDiscAmt { get; set; }
        public decimal ShippingCharges { get; set; }
        public decimal TotalTax { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal BalanceAmount { get; set; }
        public decimal CreditMemo { get; set; }
        public decimal StoreCredit { get; set; }
        public decimal PayableAmount { get; set; }
        public decimal PastDue { get; set; }
        public int PackedBoxes { get; set; }
        public string Remarks { get; set; }
        public DataTable TableValue { get; set; }
        public DataTable TableAutoId { get; set; }
        public DateTime Fromdate { get; set; }
        public DateTime Todate { get; set; }
        public int ShippingType { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string SecurityKey { get; set; }
        public string Barcode { get; set; }
        public decimal TotalCurrencyAmount { get; set; }
        public string PaymentCurrencyXml { get; set; }
        public int PaymentId { get; set; }
        public string CheckNo { get; set; }
        public DateTime CheckDate { get; set; }
        public string PaymentRemarks { get; set; }
    }

    public class BL_RegularPOSOrder
    {
        public static void bindDropdown(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {

                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }

        public static void selectQtyPrice(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {

                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
        public static void bindUnitType(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@Opcode", SqlDbType.Int).Value = pobj.Opcode;
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {

                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
        public static void SaveDraftOrder(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.Add("@DraftAutoId", SqlDbType.VarChar, 20).Value = pobj.DraftAutoId;
                sqlCmd.Parameters["@DraftAutoId"].Direction = ParameterDirection.InputOutput;
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@MLQty", pobj.MLQty);
                sqlCmd.Parameters.AddWithValue("@IsExchange", pobj.IsExchange);
                sqlCmd.Parameters.AddWithValue("@IsFreeItem", pobj.IsFreeItem);
                sqlCmd.Parameters.AddWithValue("@IsTaxable", pobj.IsTaxable);
                sqlCmd.Parameters.AddWithValue("@BillingAutoId", pobj.BillingAutoId);
                sqlCmd.Parameters.AddWithValue("@ShippingAutoId", pobj.ShippingAutoId);
                sqlCmd.Parameters.AddWithValue("@ShippingType", pobj.ShippingType);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@ReqQty", pobj.ReqQty);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
                pobj.DraftAutoId = Convert.ToInt32(sqlCmd.Parameters["@DraftAutoId"].Value);
            }
            catch (Exception ex)
            {

                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
        public static void insert(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@TaxType", pobj.TaxType);
                sqlCmd.Parameters.AddWithValue("@PaymentMenthod", pobj.PaymentMenthod);
                sqlCmd.Parameters.AddWithValue("@Adjustment", pobj.Adjustment);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                sqlCmd.Parameters.AddWithValue("@CreditNo", pobj.CreditNo);
                sqlCmd.Parameters.AddWithValue("@BillingAutoId", pobj.BillingAutoId);
                sqlCmd.Parameters.AddWithValue("@ShippingAutoId", pobj.ShippingAutoId);
                sqlCmd.Parameters.AddWithValue("@TotalAmount", pobj.TotalAmount);
                sqlCmd.Parameters.AddWithValue("@OverallDisc", pobj.OverallDisc);
                sqlCmd.Parameters.AddWithValue("@OverallDiscAmt", pobj.OverallDiscAmt);
                sqlCmd.Parameters.AddWithValue("@TableValue", pobj.TableValue);
                sqlCmd.Parameters.AddWithValue("@ShippingCharges", pobj.ShippingCharges);
                sqlCmd.Parameters.AddWithValue("@TotalTax", pobj.TotalTax);
                sqlCmd.Parameters.AddWithValue("@GrandTotal", pobj.GrandTotal);
                sqlCmd.Parameters.AddWithValue("@ReferenceNo", pobj.ReferenceNo);
                sqlCmd.Parameters.AddWithValue("@PaidAmount", pobj.PaidAmount);
                sqlCmd.Parameters.AddWithValue("@BalanceAmount", pobj.BalanceAmount);
                sqlCmd.Parameters.AddWithValue("@CreditMemo", pobj.CreditMemo);
                sqlCmd.Parameters.AddWithValue("@StoreCredit", pobj.StoreCredit);
                sqlCmd.Parameters.AddWithValue("@PayableAmount", pobj.PayableAmount);
                sqlCmd.Parameters.AddWithValue("@PastDue", pobj.PastDue);
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@DraftAutoId", pobj.DraftAutoId);
                sqlCmd.Parameters.AddWithValue("@ShippingType", pobj.ShippingType);
                sqlCmd.Parameters.AddWithValue("@TotalCurrencyAmount", pobj.TotalCurrencyAmount);
                sqlCmd.Parameters.AddWithValue("@PaymentCurrencyXml", pobj.PaymentCurrencyXml);
                sqlCmd.Parameters.AddWithValue("@CheckNo", pobj.CheckNo);
                sqlCmd.Parameters.AddWithValue("@PaymentRemarks", pobj.PaymentRemarks);
                if (pobj.CheckDate > DateTime.MinValue && pobj.CheckDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.Add("@CheckDate", SqlDbType.DateTime).Value = pobj.CheckDate;
                }
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {

                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
        public static void bindOrderDetails(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {

                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }

        public static void EditQty(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@IsExchange", pobj.IsExchange);
                sqlCmd.Parameters.AddWithValue("@IsFreeItem", pobj.IsFreeItem);
                sqlCmd.Parameters.AddWithValue("@ReqQty", pobj.ReqQty);
                sqlCmd.Parameters.AddWithValue("@QtyPerUnit", pobj.QtyPerUnit);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@UnitPrice", pobj.UnitPrice);
                sqlCmd.Parameters.Add("@DraftAutoId", SqlDbType.VarChar, 20).Value = pobj.DraftAutoId;
                sqlCmd.Parameters["@DraftAutoId"].Direction = ParameterDirection.InputOutput;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
                pobj.DraftAutoId = Convert.ToInt32(sqlCmd.Parameters["@DraftAutoId"].Value);
            }
            catch (Exception ex)
            {

                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }

        public static void EditDraftOrder(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", 425);
                sqlCmd.Parameters.AddWithValue("@DraftAutoId", pobj.DraftAutoId);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {

                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
        public static void DeleteItem(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", 501);
                sqlCmd.Parameters.AddWithValue("@DraftAutoId", pobj.DraftAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@IsFreeItem", pobj.IsFreeItem);
                sqlCmd.Parameters.AddWithValue("@IsExchange", pobj.IsExchange);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {

                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
        public static void UpdateDraftOrder(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", 205);
                sqlCmd.Parameters.AddWithValue("@DraftAutoId", pobj.DraftAutoId);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@ShippingType", pobj.ShippingType);
                //if (pobj.DeliveryDate > DateTime.MinValue && pobj.DeliveryDate < DateTime.MaxValue)
                //    sqlCmd.Parameters.AddWithValue("@DeliveryDate", pobj.DeliveryDate);
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@ShippingCharges", pobj.ShippingCharges);
                sqlCmd.Parameters.AddWithValue("@OverallDisc", pobj.OverallDisc);
                sqlCmd.Parameters.AddWithValue("@OverallDiscAmt", pobj.OverallDiscAmt);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {

                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }

        public static void checkallocationqty(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@ReqQty", pobj.ReqQty);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }

        public static void CheckMangerSecurity(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CheckSecurity", pobj.SecurityKey);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }

        public static void AddAllocatedQuantity(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@ReqQty", pobj.ReqQty);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }

        public static void readBarcode(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);
                sqlCmd.Parameters.Add("@DraftAutoId", SqlDbType.VarChar, 20).Value = pobj.DraftAutoId;
                sqlCmd.Parameters["@DraftAutoId"].Direction = ParameterDirection.InputOutput;
                sqlCmd.Parameters.AddWithValue("@ShippingType", pobj.ShippingType);
                sqlCmd.Parameters.AddWithValue("@ReqQty", pobj.ReqQty);
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@IsExchange", pobj.IsExchange);
                sqlCmd.Parameters.AddWithValue("@IsFreeItem", pobj.IsFreeItem);
                sqlCmd.Parameters.AddWithValue("@IsTaxable", pobj.IsTaxable);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
                pobj.DraftAutoId = Convert.ToInt32(sqlCmd.Parameters["@DraftAutoId"].Value);
            }
            catch (Exception ex)
            {

                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
        public static void readCurrencyDetails(PL_RegularPOSOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRegularPOSOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {

                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
}
