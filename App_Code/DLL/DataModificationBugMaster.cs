﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;  
using System.Threading.Tasks;
using DllUtility;



/// <summary>
/// Summary description for DataModificationBugMaster
/// </summary>
public class DataModificationBugMaster
{
    public static void returnTable(PropertyModificationBugMaster pobj)
    {
        try
        {
            //DataBaseUtilitys dobj = new DataBaseUtilitys();
            Config connect = new Config();
            SqlCommand sqlCmd = new SqlCommand("ProcErrorTicketMaster", connect.con);
           // SqlCommand sqlCmd = new SqlCommand("ProcErrorTicketMaster", dobj.SqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;
            sqlCmd.Parameters.Add("@TicketID", SqlDbType.Int).Value = pobj.TicketID;
            //sqlCmd.Parameters["@TicketID"].Direction = ParameterDirection.InputOutput;
            sqlCmd.Parameters.Add("@PageUrl", SqlDbType.NVarChar).Value = pobj.PageUrl;
            sqlCmd.Parameters.Add("@Priority", SqlDbType.NVarChar).Value = pobj.Priority;
            sqlCmd.Parameters.Add("@Type", SqlDbType.NVarChar).Value = pobj.Type;
            sqlCmd.Parameters.Add("@ByEmployee", SqlDbType.NVarChar).Value = pobj.ByEmployee;
            sqlCmd.Parameters.Add("@Subject", SqlDbType.NVarChar).Value = pobj.Subject;
            sqlCmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = pobj.Description;
            sqlCmd.Parameters.Add("@Status", SqlDbType.NVarChar).Value = pobj.Status;
            sqlCmd.Parameters.Add("@TicketCloseDate", SqlDbType.NVarChar).Value = pobj.TicketCloseDate;
            sqlCmd.Parameters.Add("@attach", SqlDbType.NVarChar).Value = pobj.Attach;
            sqlCmd.Parameters.Add("@TicketDate", SqlDbType.DateTime).Value = pobj.TicketDate;

            sqlCmd.Parameters.Add("@Who", SqlDbType.NVarChar).Value = pobj.Who;
            sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
            sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

            sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
            sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

            SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
            pobj.Dt = new DataTable();
            sqlAdp.Fill(pobj.Dt);

            //pobj.TicketID = sqlCmd.Parameters["@TicketID"].Value.ToString();
            pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
            pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
        }
        catch (Exception ex)
        {
            pobj.IsException = true;
            pobj.ExceptionMessage = ex.Message;
        }
    }
}