﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Threading.Tasks;
using DllUtility; 
/// <summary>
/// Summary description for DataErrorTicketForm
/// </summary>
public class DataErrorTicketForm
{
    public static void returnTable(PropertyErrorTicketForm pobj)
    {
        try
        {
            Config connect = new Config();
            SqlCommand sqlCmd = new SqlCommand("ProcErrorTicketMaster", connect.con);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;
            sqlCmd.Parameters.Add("@TicketID", SqlDbType.NVarChar, 50).Value = pobj.TicketID;
            sqlCmd.Parameters.AddWithValue("@DeveloperStatus", pobj.DeveloperStatus);
            sqlCmd.Parameters.AddWithValue("@CustomerType", pobj.CustomerType);
            sqlCmd.Parameters["@TicketID"].Direction = ParameterDirection.InputOutput;
            sqlCmd.Parameters.Add("@PageUrl", SqlDbType.NVarChar).Value = pobj.PageUrl;
            sqlCmd.Parameters.Add("@Priority", SqlDbType.NVarChar).Value = pobj.Priority;
            sqlCmd.Parameters.Add("@Type", SqlDbType.NVarChar).Value = pobj.Type;
            sqlCmd.Parameters.Add("@ByEmployee", SqlDbType.NVarChar).Value = pobj.ByEmployee;
            sqlCmd.Parameters.Add("@Subject", SqlDbType.NVarChar).Value = pobj.Subject;
            sqlCmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = pobj.Description;
            sqlCmd.Parameters.Add("@Status", SqlDbType.NVarChar).Value = pobj.Status;
            if (pobj.TicketCloseDate != DateTime.MinValue)
            {
                sqlCmd.Parameters.Add("@TicketCloseDate", SqlDbType.DateTime).Value = pobj.TicketCloseDate;
            }
            if (pobj.OpCode == 61)
            {
                sqlCmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = pobj.ToDate;
                sqlCmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = pobj.FromDate;
            }
            sqlCmd.Parameters.Add("@attach", SqlDbType.NVarChar).Value = pobj.Attach;
            sqlCmd.Parameters.Add("@TicketDate", SqlDbType.DateTime).Value = pobj.TicketDate;

            sqlCmd.Parameters.Add("@Who", SqlDbType.NVarChar).Value = pobj.Who;
            sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
            sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

            sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
            sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

            SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
            pobj.Dt = new DataTable();
            sqlAdp.Fill(pobj.Dt);

            pobj.TicketID = sqlCmd.Parameters["@TicketID"].Value.ToString();
            pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
            pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
        }
        catch (Exception ex)
        {
            pobj.IsException = true;
            pobj.ExceptionMessage = ex.Message;
        }
    }
}