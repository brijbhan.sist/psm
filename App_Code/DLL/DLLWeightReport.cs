﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using DllUtility;

/// <summary>
/// Summary description for DLLWeightReport
/// </summary>
namespace DLLWeightReport
{
	public class PL_WeightReport : Utility
	{
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int State { get; set; }
        public string Brand { get; set; }
        public int ProductAutoId { get; set; }
}
    public class DL_WeightReport
    {
        public static void ReturnTable(PL_WeightReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcWeightReport", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@StateAutoId", pobj.State);
                sqlCmd.Parameters.AddWithValue("@BrandName", pobj.Brand);

                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_WeightReport
    {
        public static void GetWeightReport(PL_WeightReport pobj)
        {
            pobj.Opcode = 41;
            DL_WeightReport.ReturnTable(pobj);
        }
        public static void ExportWeightReport(PL_WeightReport pobj)
        {
            pobj.Opcode = 42;
            DL_WeightReport.ReturnTable(pobj);
        }
        public static void GetBrand(PL_WeightReport pobj)
        {
            pobj.Opcode = 43;
            DL_WeightReport.ReturnTable(pobj);
        }
        public static void GetProductOnChange(PL_WeightReport pobj)
        {
            pobj.Opcode = 44;
            DL_WeightReport.ReturnTable(pobj);
        }
    }
}