﻿
using System;
using System.Net.Mail;
using System.IO;
using DllEmailServerMaster;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.pipeline.html;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.parser;
public class MailSending
{
    public MailSending()
    {

    }
    public static Stream GenerateStreamFromString(string s)
    {
        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(s);
        writer.Flush();
        stream.Position = 0;
        return stream;
    }
    public static string SendMailResponsewithAttachment(string ToMailID,string CC, string BCC, string subject, string body, string EmailUser,string Attachment,string CSS)
    {
        string fromMailID = "", port = "", server = "", pass = ""; bool ssl = false;
        string msg = string.Empty;
        try
        {
            DataUtility dobj = new DataUtility();
            Pl_EmailServer pobj = new Pl_EmailServer();
            pobj.UserName = EmailUser;
            Bl_EmailServer.selectEmailId(pobj);
            if (!pobj.isException && pobj.Ds.Tables[0].Rows.Count > 0)
            {
                fromMailID = pobj.Ds.Tables[0].Rows[0]["EmailId"].ToString().Trim();
                port = pobj.Ds.Tables[0].Rows[0]["port"].ToString().Trim();
                server = pobj.Ds.Tables[0].Rows[0]["server"].ToString().Trim();


                pass = pobj.Ds.Tables[0].Rows[0]["Pass"].ToString().Trim();
                ssl = Convert.ToBoolean(pobj.Ds.Tables[0].Rows[0]["ssl"].ToString());

                    MailMessage MailObj = new MailMessage();

                    StringReader sr = new StringReader(Attachment.ToString());

                    Document pdfDoc = new Document(PageSize.A4.Rotate(), 10f, 10f, 10f, 0f);
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                        pdfDoc.Open();
                        StyleAttrCSSResolver cssResolver = new StyleAttrCSSResolver();
                        var cssFile = XMLWorkerHelper.GetCSS(GenerateStreamFromString(CSS));
                        cssResolver.AddCss(cssFile);

                        // HTML  
                        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
                        htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());
                        // Pipelines  
                        PdfWriterPipeline pdfFile = new PdfWriterPipeline(pdfDoc, writer);
                        HtmlPipeline html = new HtmlPipeline(htmlContext, pdfFile);
                        CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
                        // XML Worker  
                        XMLWorker worker = new XMLWorker(css, true);
                        XMLParser p = new XMLParser(worker);
                        p.Parse(sr); 

                        //htmlparser.Parse(sr);
                        pdfDoc.Close();
                        byte[] bytes = memoryStream.ToArray();
                        memoryStream.Close();
                        MailObj.Attachments.Add(new Attachment(new MemoryStream(bytes), "OrderListPDF.pdf"));
                    }
                    SmtpClient smtpClient = new SmtpClient();
                    MailAddress fromAddress = new MailAddress(fromMailID);
                    MailObj.From = fromAddress;
                    if (ToMailID != "")
                    {
                        if (ToMailID.EndsWith(","))
                        {
                            ToMailID = ToMailID.Substring(0, ToMailID.Length - 1);
                        }
                        MailObj.To.Add(ToMailID);
                    }
                    if (BCC != "")
                    {
                        if (BCC.EndsWith(","))
                        {
                            BCC = BCC.Substring(0, BCC.Length - 1);
                        }
                        MailObj.Bcc.Add(BCC);
                    }
                    if (CC != "")
                    {
                        if (CC.EndsWith(","))
                        {
                            CC = CC.Substring(0, CC.Length - 1);
                        }
                        MailObj.CC.Add(CC);
                    }
                MailObj.Subject = subject;
                MailObj.IsBodyHtml = true;
                MailObj.Body = body;
                smtpClient.Host = server;
                smtpClient.Port = Convert.ToInt32(port);
                smtpClient.EnableSsl = ssl;
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new System.Net.NetworkCredential(fromMailID, pass);
                smtpClient.Send(MailObj);
                msg = "Successful";
            }
            else
            { msg = "No available email setting for " + EmailUser; }
            return msg;
        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }
        return msg;
    }
    public static string SendMailResponse(string ToMailID, string CC, string BCC, string subject, string body, string EmailUser)
    {
        //string fromMailID = "", port = "", server = "", pass = ""; bool ssl = false;
        string msg = string.Empty;
        try
        {
            DataUtility dobj = new DataUtility();
            Pl_EmailServer pobj = new Pl_EmailServer();
            pobj.UserName = EmailUser;
            pobj.body = body;
            pobj.Subject = subject;
            pobj.BCC = BCC;
            pobj.CC = CC;
            pobj.ToMailID = ToMailID;
            Bl_EmailServer.sendEmailDataDb(pobj);
            if (!pobj.isException)
            {
           
                msg = "Successful";
            }
            else
            { 
            msg = "No available email setting for " + EmailUser;
            }
            return msg;
        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }
        return msg;
    }

    //public static string SendMailResponse22(string ToMailID, string CC, string BCC, string subject, string body, string EmailUser,string Attachment,string CSS)
    //{
    //    string fromMailID = "", port = "", server = "", pass = ""; bool ssl = false;
    //    string msg = string.Empty;
    //    try
    //    {
    //        DataUtility dobj = new DataUtility();
    //        Pl_EmailServer pobj = new Pl_EmailServer();
    //        pobj.UserName = EmailUser;
    //        Bl_EmailServer.selectEmailId(pobj);
    //        if (!pobj.isException && pobj.Ds.Tables[0].Rows.Count > 0)
    //        {
    //            fromMailID = pobj.Ds.Tables[0].Rows[0]["EmailId"].ToString().Trim();
    //            port = pobj.Ds.Tables[0].Rows[0]["port"].ToString().Trim();
    //            server = pobj.Ds.Tables[0].Rows[0]["server"].ToString().Trim();


    //            pass = pobj.Ds.Tables[0].Rows[0]["Pass"].ToString().Trim();
    //            ssl = Convert.ToBoolean(pobj.Ds.Tables[0].Rows[0]["ssl"].ToString());

    //            MailMessage MailObj = new MailMessage();

    //            StringReader sr = new StringReader(Attachment.ToString());

    //            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
    //            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

    //            using (MemoryStream memoryStream = new MemoryStream())
    //            {
    //                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
    //                pdfDoc.Open();
    //              //  pdfDoc.Open();
    //                StyleAttrCSSResolver cssResolver = new StyleAttrCSSResolver();
    //                var cssFile = XMLWorkerHelper.GetCSS(GenerateStreamFromString(CSS));
    //                cssResolver.AddCss(cssFile);

    //                // HTML  
    //                HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
    //                htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());
    //                // Pipelines  
    //                PdfWriterPipeline pdfFile = new PdfWriterPipeline(pdfDoc, writer);
    //                HtmlPipeline html = new HtmlPipeline(htmlContext, pdfFile);
    //                CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
    //                // XML Worker  
    //                XMLWorker worker = new XMLWorker(css, true);
    //                XMLParser p = new XMLParser(worker);
    //                htmlparser.Parse(sr);
    //                pdfDoc.Close();
    //                byte[] bytes = memoryStream.ToArray();
    //                memoryStream.Close();
    //                MailObj.Attachments.Add(new Attachment(new MemoryStream(bytes), "OrderListPDF.pdf"));
    //            }
    //            SmtpClient smtpClient = new SmtpClient();
    //            MailAddress fromAddress = new MailAddress(fromMailID);
    //            MailObj.From = fromAddress;
    //            if (ToMailID != "")
    //            {
    //                if (ToMailID.EndsWith(","))
    //                {
    //                    ToMailID = ToMailID.Substring(0, ToMailID.Length - 1);
    //                }
    //                MailObj.To.Add(ToMailID);
    //            }
    //            if (BCC != "")
    //            {
    //                if (BCC.EndsWith(","))
    //                {
    //                    BCC = BCC.Substring(0, BCC.Length - 1);
    //                }
    //                MailObj.Bcc.Add(BCC);
    //            }
    //            if (CC != "")
    //            {
    //                if (CC.EndsWith(","))
    //                {
    //                    CC = CC.Substring(0, CC.Length - 1);
    //                }
    //                MailObj.CC.Add(CC);
    //            }
    //            MailObj.Subject = subject;
    //            MailObj.IsBodyHtml = true;
    //            MailObj.Body = body;
    //            smtpClient.Host = server;
    //            smtpClient.Port = Convert.ToInt32(port);
    //            smtpClient.EnableSsl = ssl;
    //            smtpClient.UseDefaultCredentials = true;
    //            smtpClient.Credentials = new System.Net.NetworkCredential(fromMailID, pass);
    //            smtpClient.Send(MailObj);
    //            msg = "Successful";
    //        }
    //        else
    //        { msg = "No available email setting for " + EmailUser; }
    //        return msg;
    //    }
    //    catch (Exception ex)
    //    {
    //        msg = ex.Message;
    //    }
    //    return msg;
    //}
}
