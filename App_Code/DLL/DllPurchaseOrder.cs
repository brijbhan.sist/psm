using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllPurchaseOrder
{
    public class PL_PurchaseOrder : Utility
    {
        public int POAutoId { get; set; }
        public string OrderNo { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int VenderAutoId { get; set; }
        public int OrderStatus { get; set; }
        public string PoStatus { get; set; }
        public int EmpId { get; set; }
        public int NOofItems { get; set; }
        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int ReqQty { get; set; }
        public string PORemarks { get; set; }
        public string ReciveStockRemark { get; set; }
        public string Barcode { get; set; }
        public string TableData { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int DraftAutoId { get; set; }
        public int QtyPerUnit { get; set; }
        public string VendorName { get; set; }
        public DataTable DtPackingDetails { get; set; }
        public int ProductId { get; set; }
    }

    public class DL_PurchaseOrder
    {
        public static void ReturnTable(PL_PurchaseOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPurchaseOrder", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@POAutoId", pobj.POAutoId);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                if (pobj.DeliveryDate != DateTime.MinValue && pobj.DeliveryDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@DeliveryDate", pobj.DeliveryDate);
                }
                sqlCmd.Parameters.AddWithValue("@dtbulkUnit", pobj.DtPackingDetails);
                sqlCmd.Parameters.AddWithValue("@VenderAutoId", pobj.VenderAutoId);
                sqlCmd.Parameters.AddWithValue("@VendorName", pobj.VendorName);
                sqlCmd.Parameters.AddWithValue("@ReqQty", pobj.ReqQty);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId); 
                sqlCmd.Parameters.AddWithValue("@ProductId", pobj.ProductId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@QtyPerUnit", pobj.QtyPerUnit);
                sqlCmd.Parameters.AddWithValue("@EmpId", pobj.EmpId);
                sqlCmd.Parameters.AddWithValue("@NoofItems", pobj.NOofItems);
                sqlCmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                sqlCmd.Parameters.AddWithValue("@PoStatus", pobj.PoStatus);
                sqlCmd.Parameters.AddWithValue("@RecieveStockRemark", pobj.ReciveStockRemark);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);
                sqlCmd.Parameters.AddWithValue("@TableValue", pobj.TableData);
                sqlCmd.Parameters.AddWithValue("@PORemarks", pobj.PORemarks);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Fromdate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Todate", pobj.ToDate);
                }
                sqlCmd.Parameters.Add("@DraftAutoId", SqlDbType.VarChar, 20).Value = pobj.DraftAutoId;
                sqlCmd.Parameters["@DraftAutoId"].Direction = ParameterDirection.InputOutput;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.DraftAutoId = Convert.ToInt32(sqlCmd.Parameters["@DraftAutoId"].Value);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_PurchaseOrder
    {
        public static void insertandupdate(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 40;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void bindUnitType(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 41;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void bindDropdown(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 42;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void GetBarDetails(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 43;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void selectQtyPrice(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 44;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void selectOrderList(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 45;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void editOrderDetails(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 46;
            DL_PurchaseOrder.ReturnTable(pobj);
        }

        public static void getDelOrderList(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 47;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void delete(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 48;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void UpdateStock(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 50;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void DraftSaveData(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 101;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void DraftOrderList(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 104;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void editDraftDetails(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 106;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void DeleteDraftItem(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 108;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void deleteDraft(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 203;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void UpdateDraftReq(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 206;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void getManagePrice(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 441;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void UpdateManagePrice(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 421;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void bindProduct(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 422;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void updateDraftPO(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 423;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
    }
}