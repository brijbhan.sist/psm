﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DataErrorTicketResponse
/// </summary>
namespace DDLErrorTicketResponse
{
    public class PL_PropertyErrorTicketResponse : Utility
    {
        public DateTime ActionDate { get; set; }
        public string ExceptionMessage { get; set; }
        public string DeveloperStatus{get;set;}
        public string TicketID{get;set;}
        public string ActionNo{ get;set;}
        public string Action {get;set;}
        public string ByEmployee{get;set;}
        public string Status{get;set;}
        public string Subject { get; set; }
        public string Priority { get;set; }
        public string Description { get; set; }
    }
    public class DL_ErrorTicketResponse
    {
        public static void returnTable(PL_PropertyErrorTicketResponse pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcErrorTicketResponseLog", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.Opcode;
                sqlCmd.Parameters.Add("@Action", SqlDbType.VarChar).Value = pobj.Action;
                sqlCmd.Parameters.Add("@TicketID", SqlDbType.VarChar).Value = pobj.TicketID;
                sqlCmd.Parameters.Add("@ActionNo", SqlDbType.VarChar).Value = pobj.ActionNo;
                sqlCmd.Parameters.Add("@ByEmployee", SqlDbType.VarChar).Value = pobj.ByEmployee;
                sqlCmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = pobj.Status;
                sqlCmd.Parameters.Add("@DeveloperStatus", SqlDbType.VarChar).Value = pobj.DeveloperStatus;
                sqlCmd.Parameters.Add("@Who", SqlDbType.NVarChar).Value = pobj.Who;

                if (pobj.ActionDate > DateTime.MinValue && pobj.ActionDate < DateTime.MaxValue)
                sqlCmd.Parameters.Add("@ActionDate", SqlDbType.DateTime).Value = pobj.ActionDate;

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.ExceptionMessage = ex.Message;
            }
        }
    }
    public class PL_BusinessErrorTicketResponse
    {
        public static void insert(PL_PropertyErrorTicketResponse pobj)
        {
            pobj.Opcode = 11;
            DL_ErrorTicketResponse.returnTable(pobj);
        }
        public static void bindgrid(PL_PropertyErrorTicketResponse pobj)
        {
            pobj.Opcode = 41;
            DL_ErrorTicketResponse.returnTable(pobj);
        }
        public static void selectAttach(PL_PropertyErrorTicketResponse pobj)
        {
            pobj.Opcode = 42;
            DL_ErrorTicketResponse.returnTable(pobj);
        }
        public static void selectallTicketDetails(PL_PropertyErrorTicketResponse pobj)
        {
            pobj.Opcode = 43;
            DL_ErrorTicketResponse.returnTable(pobj);
        }
        public static void selectReceiver(PL_PropertyErrorTicketResponse pobj)
        {
            pobj.Opcode = 44;
            DL_ErrorTicketResponse.returnTable(pobj);
        }
    }
}