﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllPageTrackerMaster
{
    public class PL_PageTrackerMaster : Utility
    {
        public int AutoId { get; set; }
        public int UserAutoId { get; set; }
        public int EmpTypeAutoId { get; set; }
        public DateTime AccessFromDate { get; set; }
        public DateTime AccessToDate { get; set; }
        public string PageUrl { get; set; }
        public string IpAddress { get; set; }
        public int OrderBy { get; set; }
    }
    public class DL_PageTrackerMaster
    {
        public static void ReturnTable(PL_PageTrackerMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPageTrackerMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@UserAutoId", pobj.UserAutoId);
                sqlCmd.Parameters.AddWithValue("@EmpTypeAutoId", pobj.EmpTypeAutoId);
                if (pobj.AccessFromDate > DateTime.MinValue && pobj.AccessFromDate < DateTime.MaxValue)
                 {
                    sqlCmd.Parameters.AddWithValue("@AccessFromDate", pobj.AccessFromDate);     
                }
                if (pobj.AccessToDate > DateTime.MinValue && pobj.AccessToDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@AccessToDate", pobj.AccessToDate);
                }
                sqlCmd.Parameters.AddWithValue("@PageUrl", pobj.PageUrl);
                sqlCmd.Parameters.AddWithValue("@IpAddress", pobj.IpAddress);
                sqlCmd.Parameters.AddWithValue("@Orderby", pobj.OrderBy);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_PageTrackerMaster
    {
        public static void Add(PL_PageTrackerMaster pobj)
        {
            pobj.Opcode = 11;
            DL_PageTrackerMaster.ReturnTable(pobj);
        }
        public static void BindUserTypeName(PL_PageTrackerMaster pobj)
        {
            pobj.Opcode = 41;
            DL_PageTrackerMaster.ReturnTable(pobj);
        }
        public static void BindEmployee(PL_PageTrackerMaster pobj)
        {
            pobj.Opcode = 42;
            DL_PageTrackerMaster.ReturnTable(pobj);
        }
        public static void GetPageTrackerReportData(PL_PageTrackerMaster pobj)
        {
            pobj.Opcode = 43;
            DL_PageTrackerMaster.ReturnTable(pobj);
        }
    }
}
