﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DLLCurrencyMaster
{
	public class Pl_CurrencyMaster:Utility
	{
        public int Autoid{ get; set; }
        public string CurrencyXml { get; set; }
        public int Createdby { get; set; }
        public string CurrencyName { get; set; }
        public int Status { get; set; }

      
	}

    public class DL_CurrencyMaster
    {
        public static void ReturnTable(Pl_CurrencyMaster pobj)
        {
        try
            { 
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcCurrencyMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.Autoid);
                sqlCmd.Parameters.AddWithValue("@Createdby", pobj.Createdby);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status); 
                sqlCmd.Parameters.AddWithValue("@CurrencyXml", pobj.CurrencyXml);
                sqlCmd.Parameters.AddWithValue("@CurrencyName", pobj.CurrencyName);
               
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch(Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    
    }

    public class BL_CurrencyMaster
    {
        public static void insertCurrency(Pl_CurrencyMaster pobj)
        {
            pobj.Opcode = 11;
            DL_CurrencyMaster.ReturnTable(pobj);
        }
        public static void updateCurreny(Pl_CurrencyMaster pobj)
        {
            pobj.Opcode = 21;
            DL_CurrencyMaster.ReturnTable(pobj);
        }
       public static void GetCurrencyDetail(Pl_CurrencyMaster pobj)
       {
           pobj.Opcode=31;
           DL_CurrencyMaster.ReturnTable(pobj);
       }
       public static void editCurrency(Pl_CurrencyMaster pobj)
       {
           pobj.Opcode = 41;
           DL_CurrencyMaster.ReturnTable(pobj);                     
       }
       public static void DeleteCurrency(Pl_CurrencyMaster pobj)
       {
           pobj.Opcode = 42;
           DL_CurrencyMaster.ReturnTable(pobj);
       }
    }
}