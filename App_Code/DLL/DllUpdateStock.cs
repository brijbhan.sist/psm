﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DllUpdateStock
/// </summary>
namespace DllUpdateStock
{
    public class Pl_UpdateStock : Utility
    {
        public int SessionAutoId { get; set; }
        public int productId { get; set; }
        public string producName { get; set; }
        public int Quantity { get; set; }
        public int OledQty { get; set; }
        public int UserAutoId { get; set; }
        public string Barcode { get; set; }
    }
    public class Dl_UpdateStock
    {
        public static void ReturnTable(Pl_UpdateStock pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcUpdateStock", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.AddWithValue("@opCode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@UserAutoId", pobj.UserAutoId);
                sqlCmd.Parameters.AddWithValue("@Productid",pobj.productId);
                sqlCmd.Parameters.AddWithValue("@ProductQuantity", pobj.Quantity);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);


                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }     
    }
    public class Bl_UpdateStock
    {
        public static void bindStock(Pl_UpdateStock pobj)
        {
            pobj.Opcode = 41;
            Dl_UpdateStock.ReturnTable(pobj);
        }
        public static void selectProductId(Pl_UpdateStock pobj)
        {
            pobj.Opcode = 42;
            Dl_UpdateStock.ReturnTable(pobj);
        }
        public static void selectBindGrid(Pl_UpdateStock pobj)
        {
            pobj.Opcode = 12;
            Dl_UpdateStock.ReturnTable(pobj);
        }

       
        public static void updateStock(Pl_UpdateStock pobj)
        {
            pobj.Opcode = 21;
            Dl_UpdateStock.ReturnTable(pobj);
        }
        public static void GetBarDetails(Pl_UpdateStock pobj)
        {
            pobj.Opcode = 43;
            Dl_UpdateStock.ReturnTable(pobj);
        }
    }
}

