﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DLLOptimoOrder
/// </summary>
namespace DLLOptimoOrder
{
    public class PL_OptimoRoute : Utility
    {
        public int OrderAutoId { get; set; }
        public DataTable OrderAutoIds { get; set; }
        public DataTable RouteDetails { get; set; }
        public DataTable DriverLog { get; set; }
        public int EmpAutoId { get; set; }
        public string CheckSecurity { get; set; }
        public DataTable ShipAutoIds { get; set; }
    }
    public class DL_OptimoRoute
    {
        public static void ReturnData(PL_OptimoRoute pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[Proc_OptimoRoute]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);

                cmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                cmd.Parameters.AddWithValue("@OptimoOrdersDt", pobj.OrderAutoIds);
                cmd.Parameters.AddWithValue("@SaveRuteDt", pobj.RouteDetails);
                cmd.Parameters.AddWithValue("@ShippingDt", pobj.ShipAutoIds);
                cmd.Parameters.AddWithValue("@DriverLog", pobj.DriverLog);
                cmd.Parameters.AddWithValue("@CheckSecurity", pobj.CheckSecurity);

                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_OptimoRoute
    {
        public static void BindOrderandDriver(PL_OptimoRoute pobj)
        {
            pobj.Opcode = 40;
            DL_OptimoRoute.ReturnData(pobj);
        }
        public static void SaveRoute(PL_OptimoRoute pobj)
        {
            pobj.Opcode = 41;
            DL_OptimoRoute.ReturnData(pobj);
        }
        public static void CheckSecurity(PL_OptimoRoute pobj)
        {
            pobj.Opcode = 42;
            DL_OptimoRoute.ReturnData(pobj);
        }
        public static void GetShipList(PL_OptimoRoute pobj)
        {
            pobj.Opcode = 43;
            DL_OptimoRoute.ReturnData(pobj);
        }
        public static void GetDateDifference(PL_OptimoRoute pobj)
        {
            pobj.Opcode = 44;
            DL_OptimoRoute.ReturnData(pobj);
        }
    }
}