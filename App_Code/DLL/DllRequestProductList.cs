﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;


namespace DllRequestProductList
{
    public class PL_Product : Utility
    {
        public int IsAppyMLQty { get; set; }
        public int IsAppyWeightQty { get; set; }
        public int AutoId { get; set; }
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public int BrandAutoId { get; set; }
        public string ProductLocation { get; set; }
        public string ImageUrl { get; set; }
        public int CategoryAutoId { get; set; }
        public int SubcategoryAutoId { get; set; }
        public int Status { get; set; }
        public int EligibleforFree { get; set; }
        public int VendorAutoId { get; set; }
        public int ReOrderMark { get; set; }        
        public int PackingDetailAutoId { get; set; }
        public int UnitType { get; set; }
        public int Qty { get; set; }
        public decimal MLQty { get; set; }
        public decimal WeightOz { get; set; }
        public decimal MinPrice { get; set; }
        public decimal CostPrice { get; set; }
        public decimal Price { get; set; }
        public decimal SRP { get; set; }
        public decimal WHminPrice { get; set; }
        public string PreDefinedBarcode { get; set; }
        public string SearchBy { get; set; }
        public decimal CommCode { get; set; }
        public int BarcodeAutoId { get; set; }
        public string EmpAutoId { get; set; }
        public DataTable dtbulkUnit { get; set; }
    }

    public class DL_Product 
    {
        public static void ReturnTable(PL_Product pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcDraftProductMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@SearchBy", pobj.SearchBy);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@ProductId", pobj.ProductId);
                sqlCmd.Parameters.AddWithValue("@ProductName", pobj.ProductName);
                sqlCmd.Parameters.AddWithValue("@ProductLocation", pobj.ProductLocation);
                sqlCmd.Parameters.AddWithValue("@ImageUrl", pobj.ImageUrl);
                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@SubcategoryAutoId", pobj.SubcategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@EligibleforFree", pobj.EligibleforFree);
                sqlCmd.Parameters.AddWithValue("@ReOrderMark", pobj.ReOrderMark);
                sqlCmd.Parameters.AddWithValue("@dtbulkUnit", pobj.dtbulkUnit); 
                sqlCmd.Parameters.AddWithValue("@PackingDetailAutoId", pobj.PackingDetailAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitType", pobj.UnitType);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@Qty", pobj.Qty);
                sqlCmd.Parameters.AddWithValue("@IsAppyMLQty", pobj.IsAppyMLQty);
                sqlCmd.Parameters.AddWithValue("@IsAppyWeightQty", pobj.IsAppyWeightQty);
                sqlCmd.Parameters.AddWithValue("@MLQty", pobj.MLQty);
                sqlCmd.Parameters.AddWithValue("@WeightOz", pobj.WeightOz);
                sqlCmd.Parameters.AddWithValue("@MinPrice", pobj.MinPrice);
                sqlCmd.Parameters.AddWithValue("@CostPrice", pobj.CostPrice);
                sqlCmd.Parameters.AddWithValue("@WHminPrice", pobj.WHminPrice);
                sqlCmd.Parameters.AddWithValue("@Price", pobj.Price);
                sqlCmd.Parameters.AddWithValue("@SRP", pobj.SRP);
                sqlCmd.Parameters.AddWithValue("@PreDefinedBarcode", pobj.PreDefinedBarcode);
                sqlCmd.Parameters.AddWithValue("@CommCode", pobj.CommCode);
                sqlCmd.Parameters.AddWithValue("@BarcodeAutoId", pobj.BarcodeAutoId);
                sqlCmd.Parameters.AddWithValue("@BrandAutoId", pobj.BrandAutoId);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                string Beforefill = System.DateTime.Now.ToString();
                sqlAdp.Fill(pobj.Ds);
                string Afterfill = System.DateTime.Now.ToString();
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }         
    }

    public class BL_Product
    {
        public static void insert(PL_Product pobj)
        {
            pobj.Opcode = 11;
            DL_Product.ReturnTable(pobj);
        }
        public static void insertPacking(PL_Product pobj)
        {
            pobj.Opcode = 12;
            DL_Product.ReturnTable(pobj);
        }
        public static void insertBarcode(PL_Product pobj)
        {
            pobj.Opcode = 13;
            DL_Product.ReturnTable(pobj);
        }
        public static void updateProduct(PL_Product pobj)
        {
            pobj.Opcode = 21;
            DL_Product.ReturnTable(pobj);
        }
        public static void updatePacking(PL_Product pobj)
        {
            pobj.Opcode = 22;
            DL_Product.ReturnTable(pobj);
        }
        public static void updateBarcode(PL_Product pobj)
        {
            pobj.Opcode = 23;
            DL_Product.ReturnTable(pobj);
        }
        public static void DefaultPacking(PL_Product pobj)
        {
            pobj.Opcode = 24;
            DL_Product.ReturnTable(pobj);
        }
        public static void deleteProduct(PL_Product pobj)
        {
            pobj.Opcode = 31;
            DL_Product.ReturnTable(pobj);
        }
        public static void deletePacking(PL_Product pobj)
        {
            pobj.Opcode = 32;
            DL_Product.ReturnTable(pobj);
        }
        public static void deleteBarcode(PL_Product pobj)
        {
            pobj.Opcode = 33;
            DL_Product.ReturnTable(pobj);
        }
        public static void bindCategory(PL_Product pobj)
        {
            pobj.Opcode = 41;
            DL_Product.ReturnTable(pobj);
        }
        public static void bindSubcategory(PL_Product pobj)
        {
            pobj.Opcode = 42;
            DL_Product.ReturnTable(pobj);
        }
        public static void bindUnitType(PL_Product pobj)
        {
            pobj.Opcode = 43;
            DL_Product.ReturnTable(pobj);
        }
        public static void select(PL_Product pobj)
        {
            pobj.Opcode = 44;
            DL_Product.ReturnTable(pobj);
        }
        public static void selectPackingDetails(PL_Product pobj)
        {
            pobj.Opcode = 45;
            DL_Product.ReturnTable(pobj);
        }
        public static void editProduct(PL_Product pobj)
        {
            pobj.Opcode = 46;
            DL_Product.ReturnTable(pobj);
        }
        public static void editPacking(PL_Product pobj)
        {
            pobj.Opcode = 47;
            DL_Product.ReturnTable(pobj);
        }
        public static void getBarcode(PL_Product pobj)
        {
            pobj.Opcode = 48;
            DL_Product.ReturnTable(pobj);
        }
        public static void BarcodeReport(PL_Product pobj)
        {
            pobj.Opcode = 49;
            DL_Product.ReturnTable(pobj);
        }
        public static void BarcodeReportExport(PL_Product pobj)
        {
            pobj.Opcode = 50;
            DL_Product.ReturnTable(pobj);
        }
        public static void getUnitMaster(PL_Product pobj)
        {
            pobj.Opcode = 51;
            DL_Product.ReturnTable(pobj);
        }
        public static void updatebulk(PL_Product pobj)
        {
            pobj.Opcode = 25;
            DL_Product.ReturnTable(pobj);
        }
        public static void GetProductBarcode(PL_Product pobj)
        {
            pobj.Opcode = 26;
            DL_Product.ReturnTable(pobj);
        }
        public static void GetProductAllBarcode(PL_Product pobj)
        {
            pobj.Opcode = 27;
            DL_Product.ReturnTable(pobj);
        }
    }
}
