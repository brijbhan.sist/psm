﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DllUtility;
using System.Data;
/// <summary>
/// Summary description for DLLProductExchangeReport
/// </summary>
namespace DLLProductExchangeReport
{
	class PL_ProductExchangeReport : Utility
    {      
        public int Status { get; set; }
        public int CustomerAutoId { get; set; }
        public int DriverAutoId { get; set; } 
        public int SalesPersonAutoId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }


    }
    class DL_ProductExchangeReport
    {
        public static void ReturnTable(PL_ProductExchangeReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcProductExchange_Report", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);               
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@DriverAutoId",pobj.DriverAutoId);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                if(pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        } 
    }
    class BL_ProductExchangeReport
    {
        public static void getReport(PL_ProductExchangeReport pobj)
        {
            pobj.Opcode = 41;
            DL_ProductExchangeReport.ReturnTable(pobj);
        }
        public static void bindDropdownList(PL_ProductExchangeReport pobj)
        {
            pobj.Opcode = 42;
            DL_ProductExchangeReport.ReturnTable(pobj);
        }
        public static void BindCustomer(PL_ProductExchangeReport pobj)
        {
            pobj.Opcode = 43;
            DL_ProductExchangeReport.ReturnTable(pobj);
        }
    }
}