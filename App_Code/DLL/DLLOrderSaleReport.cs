﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DLLOrderSaleReport
/// </summary>
namespace DLLOrderSaleReport
{
    public class PL_OrderSaleReport : Utility
	{
        public int CustomerType { get; set; }
        public int SalesPersonAutoId { get; set; }
        public int Customer { get; set; }
        public int SalesPerson { get; set; }
        public int Category { get; set; }
        public int SubCategory { get; set; }
        public int Product { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
	}
    public class DL_OrderSaleReport
    {
        public static void ReturnTable(PL_OrderSaleReport pobj)
        {
            Config con = new Config();
            SqlCommand cmd = new SqlCommand("ProcOrderSaleReport", con.con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 10000;
            cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
            cmd.Parameters.AddWithValue("@CutomerType", pobj.CustomerType);
            cmd.Parameters.AddWithValue("@Customer", pobj.Customer);
            cmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
            cmd.Parameters.AddWithValue("@Category", pobj.Category);
            cmd.Parameters.AddWithValue("@SubCategory", pobj.SubCategory);
            cmd.Parameters.AddWithValue("@Product", pobj.Product);
            cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
            cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
            cmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

             if (pobj.FromDate != DateTime.MaxValue && pobj.FromDate != DateTime.MinValue) 
            cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
             if (pobj.ToDate != DateTime.MaxValue && pobj.ToDate != DateTime.MinValue) 
            cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);

            cmd.Parameters.Add("@IsException", SqlDbType.Bit);
            cmd.Parameters["@IsException"].Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
            cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;

            SqlDataAdapter sqlAdp = new SqlDataAdapter(cmd);
            pobj.Ds = new DataSet();
            sqlAdp.Fill(pobj.Ds);

            pobj.isException = Convert.ToBoolean(cmd.Parameters["@IsException"].Value);
            pobj.exceptionMessage = cmd.Parameters["@ExceptionMessage"].Value.ToString();
        }
    }
    public class BL_OrderSaleReport
    {
        public static void bindDropdown(PL_OrderSaleReport pobj)
        {
            pobj.Opcode = 41;
            DL_OrderSaleReport.ReturnTable(pobj);
        }
        public static void bindCustomer(PL_OrderSaleReport pobj)
        {
            pobj.Opcode = 42;
            DL_OrderSaleReport.ReturnTable(pobj);
        }
        public static void BindProductSubCategory(PL_OrderSaleReport pobj)
        {
            pobj.Opcode = 43;
            DL_OrderSaleReport.ReturnTable(pobj);
        }
        public static void BindProduct(PL_OrderSaleReport pobj)
        {
            pobj.Opcode = 44;
             DL_OrderSaleReport.ReturnTable(pobj);
        }
        public static void OrderSaleReport(PL_OrderSaleReport pobj)
        {
            pobj.Opcode = 45;
            DL_OrderSaleReport.ReturnTable(pobj);
        }
    }
}