﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data;
using System.Data.SqlClient;

namespace DLLPaymentDateWiseReport
{
	public class PL_PaymentDateWiseReport:Utility
	{
        public int payemnetMode { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime FromDate { get; set; }
	}

    public class DL_PaymentDateWiseReport
    {
        public static void ReturnTable(PL_PaymentDateWiseReport pobj)
        {
            try
            {
                Config Connect = new Config();
                SqlCommand cmd = new SqlCommand("ProcPayementDatewiseReport", Connect.con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@PayMentMode", pobj.payemnetMode);
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);

                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@DateFrom", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@DateTo", pobj.ToDate);
                }

                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                cmd.CommandTimeout = 1000;
                SqlDataAdapter SqlAdp = new SqlDataAdapter(cmd);
                pobj.Ds = new DataSet();
                SqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = cmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch(Exception ex)
            {
                pobj.isException=true;
                pobj.exceptionMessage = ex.Message;
            }
        }        
    }
    public class BL_PaymentDateWiseReport
    {
        public static void BindPaymentMode(PL_PaymentDateWiseReport pobj)
        {
            pobj.Opcode = 401;
            DL_PaymentDateWiseReport.ReturnTable(pobj);
        }
        public static void GetPaymentResport(PL_PaymentDateWiseReport pobj)
        {
            pobj.Opcode = 402;
            DL_PaymentDateWiseReport.ReturnTable(pobj);
        }
    }
}