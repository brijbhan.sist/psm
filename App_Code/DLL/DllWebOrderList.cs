﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DllCopyOrderMaster
/// </summary>
namespace DllWebOrderList
{
    public class PL_COrderMaster : Utility
    {
        public string OrderNo { get; set; }

        public int CustomerAutoId { get; set; }
        public int SalesPersonAutoId { get; set; }

        public DateTime Fromdate { get; set; }
        public DateTime Todate { get; set; }
        public int LoginEmpType { get; set; }
        public string CheckSecurity { get; set; }
    }

    public class DL_COrderMaster
    {
        public static void ReturnTable(PL_COrderMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("[ProcWebOrderMaster]", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);

                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);

                if (pobj.Fromdate != DateTime.MinValue && pobj.Fromdate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Fromdate", pobj.Fromdate);
                }
                if (pobj.Todate != DateTime.MinValue && pobj.Todate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Todate", pobj.Todate);
                }

                sqlCmd.Parameters.AddWithValue("@CheckSecurity", pobj.CheckSecurity);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_COrderMaster
    {

        public static void BindSalesperson(PL_COrderMaster pobj)
        {
            pobj.Opcode = 41;
            DL_COrderMaster.ReturnTable(pobj);
        }public static void BindCustomer(PL_COrderMaster pobj)
        {
            pobj.Opcode = 42;
            DL_COrderMaster.ReturnTable(pobj);
        }
        public static void selectOrderList(PL_COrderMaster pobj)
        {
            pobj.Opcode = 43;
            DL_COrderMaster.ReturnTable(pobj);
        }
        public static void clickonSecurity(PL_COrderMaster pobj)
        {
            pobj.Opcode = 44;
            DL_COrderMaster.ReturnTable(pobj);
        }
    }
}