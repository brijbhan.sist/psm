﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DLLProductPacked_WithoutBarcode
/// </summary>
namespace DLLProductPacked_WithoutBarcode
{
    public class Pl_PackedStatusReport : Utility
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string SalesPerson { get; set; }
        public string ShippingAutoId { get; set; }
        public int CustomerAutoId { get; set; }
        public string OrderNo { get; set; }


    }

    public class DL_PackedStatusReport
    {
        public static void ReturnTable(Pl_PackedStatusReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("Proc_ProductPackedWithoutBarcodeReport", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@ShippingAutoId", pobj.ShippingAutoId);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }

    }

    public class BL_PackedStatusReport
    {
        public static void GetProfitReport(Pl_PackedStatusReport pobj)
        {
            pobj.Opcode = 41;
            DL_PackedStatusReport.ReturnTable(pobj);
        }
        public static void bindDropDown(Pl_PackedStatusReport pobj)
        {
            pobj.Opcode = 42;
            DL_PackedStatusReport.ReturnTable(pobj);
        }
        public static void GetCustomerBySalesPerson(Pl_PackedStatusReport pobj)
        {
            pobj.Opcode = 43;
            DL_PackedStatusReport.ReturnTable(pobj);
        }
    }
}