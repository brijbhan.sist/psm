﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;


namespace DLLCompanyProfile
{
    public class PL_CompanyDetails : Utility
    {
        public int AutoId { get; set; }
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public string MobileNo { get; set; }
        public string DateDifference { get; set; }
        public string FaxNo { get; set; }
        public string TermsCondition { get; set; }
        public string Logo { get; set; }
        public int OptimoDriverLimit { get; set; }
        
        public string currentVersion { get; set; }
        public DateTime SubscriptionAlertDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime SubscriptionExpiryDate { get; set; }
        public DateTime appLogoutTime { get; set; }
        public string StartLongitude { get; set; }
        public string StartLatitude { get; set; }

    }
    public class DL_CompanyDetails
    {
        public static void ReturnTable(PL_CompanyDetails pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcCompanyDetails", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@CompanyId", pobj.CompanyId);
                sqlCmd.Parameters.AddWithValue("@CompanyName", pobj.CompanyName);
                sqlCmd.Parameters.AddWithValue("@Address", pobj.Address);
                sqlCmd.Parameters.AddWithValue("@EmailAddress", pobj.EmailAddress);
                sqlCmd.Parameters.AddWithValue("@Website", pobj.Website);
                sqlCmd.Parameters.AddWithValue("@MobileNo", pobj.MobileNo);
                sqlCmd.Parameters.AddWithValue("@FaxNo", pobj.FaxNo);
                sqlCmd.Parameters.AddWithValue("@TermsCondition", pobj.TermsCondition);
                sqlCmd.Parameters.AddWithValue("@currentVersion", pobj.currentVersion);
                sqlCmd.Parameters.AddWithValue("@DateDifference", pobj.DateDifference);
                sqlCmd.Parameters.AddWithValue("@StartLongitude", pobj.StartLongitude);
                sqlCmd.Parameters.AddWithValue("@StartLatitude", pobj.StartLatitude); 
                sqlCmd.Parameters.AddWithValue("@OptimoDriverLimit", pobj.OptimoDriverLimit); 

                if (pobj.SubscriptionAlertDate > DateTime.MinValue && pobj.SubscriptionAlertDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@SubscriptionAlertDate", pobj.SubscriptionAlertDate);
                }
                if (pobj.SubscriptionAlertDate > DateTime.MinValue && pobj.SubscriptionAlertDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@StartDate", pobj.StartDate);
                }
                if (pobj.SubscriptionExpiryDate > DateTime.MinValue && pobj.SubscriptionExpiryDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@SubscriptionExpiryDate", pobj.SubscriptionExpiryDate);
                }
                if (pobj.appLogoutTime > DateTime.MinValue && pobj.appLogoutTime < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@appLogoutTime", pobj.appLogoutTime);
                }
                sqlCmd.Parameters.AddWithValue("@Logo", pobj.Logo);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }

        }
    }
    public class BL_CompanyDetails
    {


        public static void select(PL_CompanyDetails pobj)
        {
            pobj.Opcode = 41;
            DL_CompanyDetails.ReturnTable(pobj);
        }

        public static void update(PL_CompanyDetails pobj)
        {
            pobj.Opcode = 21;
            DL_CompanyDetails.ReturnTable(pobj);
        }
    }
}
