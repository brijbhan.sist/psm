﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;


/// <summary>
/// Summary description for DllProductImageList
/// </summary>
namespace DllProductImageList
{
    public class PL_ProductImgList : Utility
    {
        public string defaultImg { get; set; }
        public int ImageAutoId { get; set; }
        public int ProductAutoId { get; set; }
        public string ImageUrl { get; set; }
        public string ThumbnailImageUrl { get; set; }
        public string FourImageUrl { get; set; }
        public string OriginalImageUrl { get; set; }
        
        public int CategoryAutoId { get; set; }
        public int SubcategoryAutoId { get; set; }
        public string ProductId { get; set; }
        public int ImageType { get; set; }
        public int ProductStatus { get; set; }
        public string ProductName { get; set; }

    }

    public class DL_ProductImgList
    {
        public static void ReturnTable(PL_ProductImgList pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcProductImageList", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@ImageUrl", pobj.ImageUrl);
                sqlCmd.Parameters.AddWithValue("@ThumbnailImageUrl", pobj.ThumbnailImageUrl);
                sqlCmd.Parameters.AddWithValue("@FourImageUrl", pobj.FourImageUrl);
                sqlCmd.Parameters.AddWithValue("@OriginalImageUrl", pobj.OriginalImageUrl);
                sqlCmd.Parameters.AddWithValue("@defaultImg", pobj.defaultImg);
                sqlCmd.Parameters.AddWithValue("@ImageAutoId", pobj.ImageAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);

                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@SubcategoryAutoId", pobj.SubcategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductId", pobj.ProductId);
                sqlCmd.Parameters.AddWithValue("@ImageType", pobj.ImageType);
                sqlCmd.Parameters.AddWithValue("@ProductName", pobj.ProductName);
                sqlCmd.Parameters.AddWithValue("@ProductStatus", pobj.ProductStatus);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops,some thing went wrong.Please try later.";
            }
        }
    }

    public class BL_ProductImgList
    {
        public static void ProductList(PL_ProductImgList pobj)
        {
            pobj.Opcode = 41;
            DL_ProductImgList.ReturnTable(pobj);
        }
        public static void UploadImage(PL_ProductImgList pobj)
        {
            pobj.Opcode = 21;
            DL_ProductImgList.ReturnTable(pobj);
        }
        public static void getProductDetail(PL_ProductImgList pobj)
        {
            pobj.Opcode = 42;
            DL_ProductImgList.ReturnTable(pobj);
        }
        public static void AddWatermark(PL_ProductImgList pobj)
        {
            pobj.Opcode = 43;
            DL_ProductImgList.ReturnTable(pobj);
        }

    }
}