﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DLLAssignModule{
    public class PL_AssignModule : Utility
    {
        public int AutoId { get; set; }
        public int ModuleAutoId { get; set; }
        public int PageAutoId { get; set; }
        public int SubModuleAutoId { get; set; }
        public int Status { get; set; }
        public int LocationId { get; set; }
        public string LocationAutoId { get; set; }
    }
    public class DL_AssignModule
    {
        public static void ReturnTable(PL_AssignModule pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcAssignModule", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@OpCode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@ModuleAutoId", pobj.ModuleAutoId);
                sqlCmd.Parameters.AddWithValue("@PageAutoId", pobj.PageAutoId);
                sqlCmd.Parameters.AddWithValue("@AssignedLocation", pobj.LocationAutoId);
                sqlCmd.Parameters.AddWithValue("@SubModuleAutoId", pobj.SubModuleAutoId);
                sqlCmd.Parameters.AddWithValue("@LocationId", pobj.LocationId);


                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();

            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_AssignModule
    {
        public static void bindDropdown(PL_AssignModule pobj)
        {
            pobj.Opcode = 44;
            DL_AssignModule.ReturnTable(pobj);
        }
        public static void getSubModuleByModule(PL_AssignModule pobj)
        {
            pobj.Opcode = 43;
            DL_AssignModule.ReturnTable(pobj);
        }
        public static void bindPageName(PL_AssignModule pobj)
        {
            pobj.Opcode = 45;
            DL_AssignModule.ReturnTable(pobj);
        }
        public static void insert(PL_AssignModule pobj)
        {
            pobj.Opcode = 11;
            DL_AssignModule.ReturnTable(pobj);
        }
        public static void select(PL_AssignModule pobj)
        {
            pobj.Opcode = 41;
            DL_AssignModule.ReturnTable(pobj);
        }
        public static void update(PL_AssignModule pobj)
        {
            pobj.Opcode = 21;
            DL_AssignModule.ReturnTable(pobj);
        }
        public static void delete(PL_AssignModule pobj)
        {
            pobj.Opcode = 31;
            DL_AssignModule.ReturnTable(pobj);
        }
    }

}