﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
using System.Web;

namespace DLL_OM_OrderList
{
    public class PL_OM_OrderList : Utility
    {
        public int OrderAutoId { get; set; }
        public DateTime AsgnDate { get; set; }
        public int CustomerAutoId { get; set; }
        public int OrderStatus { get; set; }
        public string OrderNo { get; set; }
        public int SalesPersonAutoId { get; set; }
        public int EmpAutoId { get; set; }
        public string Remarks { get; set; }
        public DateTime Fromdate { get; set; }
        public DateTime Todate { get; set; }
        public string TicketNo { get; set; }
    }

    public class DL_OM_OrderList
    {
        public static void ReturnTable(PL_OM_OrderList pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("Proc_OM_OrderList", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
              
                sqlCmd.Parameters.AddWithValue("@TicketNo", pobj.TicketNo);
                if (pobj.AsgnDate != DateTime.MinValue && pobj.AsgnDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@AsgnDate", pobj.AsgnDate);
                }
                if (HttpContext.Current.Session["EmpAutoId"] != null)
                    sqlCmd.Parameters.AddWithValue("@EmpAutoId", HttpContext.Current.Session["EmpAutoId"].ToString());
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                if (pobj.Fromdate != DateTime.MinValue && pobj.Fromdate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Fromdate", pobj.Fromdate);
                }
                if (pobj.Todate != DateTime.MinValue && pobj.Todate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Todate", pobj.Todate);
                }
               
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_OM_OrderList
    {

        public static void getOrderList(PL_OM_OrderList pobj)
        {
            pobj.Opcode = 41;
            DL_OM_OrderList.ReturnTable(pobj);
        }

        public static void bindStatus(PL_OM_OrderList pobj)
        {
            pobj.Opcode = 43;
            DL_OM_OrderList.ReturnTable(pobj);
        }
        public static void deleteOrder(PL_OM_OrderList pobj)
        {
            pobj.Opcode = 31;
            DL_OM_OrderList.ReturnTable(pobj);
        }
        public static void getOrderData(PL_OM_OrderList pobj)
        {
            pobj.Opcode = 42;
            DL_OM_OrderList.ReturnTable(pobj);
        }
    }
}
