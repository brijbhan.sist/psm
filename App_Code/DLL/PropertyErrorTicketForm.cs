﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropertyErrorTicketForm
/// </summary>
public class PropertyErrorTicketForm:PropertyUtilityAbstract
{
    DateTime ticketDate, fromDate, toDate;

    public DateTime ToDate
    {
        get { return toDate; }
        set { toDate = value; }
    }

    public DateTime FromDate
    {
        get { return fromDate; }
        set { fromDate = value; }
    }

    public DateTime TicketDate
    {
        get { if (ticketDate == null || ticketDate == DateTime.MinValue) { ticketDate = Convert.ToDateTime("01/01/2010"); } return ticketDate; }
        set { if (ticketDate == null || ticketDate == DateTime.MinValue) { ticketDate = Convert.ToDateTime("01/01/2010"); } ticketDate = value; }
    }
    string ticketID, pageUrl, priority, type, byEmployee, subject, description, status, attach;
    DateTime ticketCloseDate;

    public DateTime TicketCloseDate
    {
        get { return ticketCloseDate; }
        set { ticketCloseDate = value; }
    }
    public string TicketID
    {
        get { return ticketID; }
        set { ticketID = value; }
    }

    public string PageUrl
    {
        get { return pageUrl; }
        set { pageUrl = value; }
    }

    public string Priority
    {
        get { return priority; }
        set { priority = value; }
    }

    public string Type
    {
        get { return type; }
        set { type = value; }
    }

    public string ByEmployee
    {
        get { return byEmployee; }
        set { byEmployee = value; }
    }

    public string Subject
    {
        get { return subject; }
        set { subject = value; }
    }

    public string Description
    {
        get { return description; }
        set { description = value; }
    }

    public string Status
    {
        get { return status; }
        set { status = value; }
    }

   
    public string Attach
    {
        get { return attach; }
        set { attach = value; }
    }

    public string CustomerType { get; set; }
    public string DeveloperStatus { get; set; }
}