﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DllDriverPackagePrint
/// </summary>
namespace DllPrintOrderPSMWPAPA
{
    public class PL_PrintOrderPSMWPAPA : Utility
    {
        public int OrderAutoId { get; set; }
        public string BulkOrderAutoId { get; set; }
        public string OrderNo { get; set; }
        public int LoginEmpType { get; set; }
        public int EmpAutoId { get; set; }
        public int DrvAutoId { get; set; }
        public DateTime AsgnDate { get; set; }
       
    }

    public class DL_PrintOrderPSMWPAPA
    {
        public static void ReturnTable(PL_PrintOrderPSMWPAPA pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPrintWPAPAOrder", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@BulkOrderAutoId", pobj.BulkOrderAutoId);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later";
            }
        }
    }

    public class BL_PrintOrderPSMWPAPA
    {
        public static void GetPackingOrderPrint(PL_PrintOrderPSMWPAPA pobj)
        {
            pobj.Opcode = 40;
            DL_PrintOrderPSMWPAPA.ReturnTable(pobj);
        }
    }
}




