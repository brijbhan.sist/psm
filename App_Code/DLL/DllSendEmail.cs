﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
using System.Web;


namespace DllSendEmail
{
    public class PL_SendEmail : Utility
    {
        public int OrderAutoId { get; set; }
        public int EmpAutoId { get; set; }
    }

    public class DL_SendEmail
    {
        public static void ReturnTable(PL_SendEmail pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("Proc_SendEmail", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
              
                if (HttpContext.Current.Session["EmpAutoId"] != null)
                    sqlCmd.Parameters.AddWithValue("@EmpAutoId", HttpContext.Current.Session["EmpAutoId"].ToString());

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_SendEmail
    {

        public static void ViewOrderDetails(PL_SendEmail pobj)
        {
            pobj.Opcode = 25;
            DL_SendEmail.ReturnTable(pobj);
        }
    }
}