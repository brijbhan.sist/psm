﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
namespace DLLCreditMemoMaster
{
    public class PL_CreditMemoMaster : Utility
    {
        public string barcodeNo { get; set; }
        public int CreditAutoId { get; set; }
        public int sortInCode { get; set; }
        public int CustomerAutoId { get; set; }
        public int EmpAutoId { get; set; }
        public string CheckSecurity { get; set; }
        public int OrderAutoId { get; set; }
        public string MLTaxRemark { get; set; }
        public int Qty { get; set; }
        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int Status { get; set; }
        public int CreditType { get; set; }
        public string CreditNo { get; set; }
        public string Remarks { get; set; }
        public string SecurityKey  { get; set; }
        public string CancelRemarks { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int EmpType { get; set; }
        public DataTable TableValue { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal OverallDisc { get; set; }
        public decimal OverallDiscAmt { get; set; }
        public decimal TotalTax { get; set; }
        public decimal GrandTotal { get; set; }
        public int TaxType { get; set; }
        public decimal TaxValue { get; set; }
        public decimal MLQty { get; set; }
        public decimal MLTax { get; set; }
        public decimal AdjustmentAmt { get; set; }
        public int SalesPerson { get; set; }
        public int CreditMemoType { get; set; }
        public int ReferenceOrderAutoId { get; set; }


    }
    public class DL_CreditMemoMaster
    {
        public static void ReturnTable(PL_CreditMemoMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcCreditMemo", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CreditAutoId", pobj.CreditAutoId);
                sqlCmd.Parameters.AddWithValue("@sortInCode", pobj.sortInCode);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@CreditNo", pobj.CreditNo);
                sqlCmd.Parameters.AddWithValue("@CreditType", pobj.CreditType);
                sqlCmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@IPAddress", pobj.IPAddress);
                sqlCmd.Parameters.AddWithValue("@Remark", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@Qty", pobj.Qty);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.barcodeNo);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@EmpType", pobj.EmpType);
                sqlCmd.Parameters.AddWithValue("@TableValue", pobj.TableValue);

                sqlCmd.Parameters.AddWithValue("@TotalAmount", pobj.TotalAmount);
                sqlCmd.Parameters.AddWithValue("@OverallDisc", pobj.OverallDisc);
                sqlCmd.Parameters.AddWithValue("@OverallDiscAmt", pobj.OverallDiscAmt);
                sqlCmd.Parameters.AddWithValue("@TotalTax", pobj.TotalTax);
                sqlCmd.Parameters.AddWithValue("@GrandTotal", pobj.GrandTotal);
                sqlCmd.Parameters.AddWithValue("@TaxType", pobj.TaxType);
                sqlCmd.Parameters.AddWithValue("@TaxValue", pobj.TaxValue);
                sqlCmd.Parameters.AddWithValue("@MLQty", pobj.MLQty);
                sqlCmd.Parameters.AddWithValue("@MLTax", pobj.MLTax);
                sqlCmd.Parameters.AddWithValue("@AdjustmentAmt", pobj.AdjustmentAmt);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.AddWithValue("@SecurityKey", pobj.SecurityKey);
                sqlCmd.Parameters.AddWithValue("@CancelRemarks", pobj.CancelRemarks);

                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@CheckSecurity", pobj.CheckSecurity);
                sqlCmd.Parameters.AddWithValue("@MLTaxRemark", pobj.MLTaxRemark);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);

                sqlCmd.Parameters.AddWithValue("@CreditMemoType", pobj.CreditMemoType);
                sqlCmd.Parameters.AddWithValue("@ReferenceOrderAutoId", pobj.ReferenceOrderAutoId);



                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }
    }

    public class BL_CreditMemoMaster
    {
        public static void insert(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 11;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void updateCredit(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 21;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void updateCredit1(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 24;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void delete(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 31;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void bindDropdown(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 41;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void bindAllDropdown(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 411;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void BindProduct(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 48;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void SelectUnit(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 42;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void ValidateCreditMemo(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 43;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void select(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 44;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void bindStatus(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 45;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void EditCredit(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 46;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void BindCreditLog(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 47;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void ApprovedCredit(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 22;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void CreditComplete(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 23;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void PrintCredit(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 49;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }

        public static void ReadBarcode(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 401;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }

        public static void UnitDetails(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 402;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void PrintBulkCredit(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 403;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void clickonSecurity(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 404;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void ConfirmSecurity(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 405;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void checkSecurity(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 51;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
        public static void CancelCreditMemo(PL_CreditMemoMaster pobj)
        {
            pobj.Opcode = 50;
            DL_CreditMemoMaster.ReturnTable(pobj);
        }
    }
}
