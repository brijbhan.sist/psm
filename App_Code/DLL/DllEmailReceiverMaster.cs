﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DllUtility;
using System.Data;

/// <summary>
/// Summary description for DllEmailReceiverMaster
/// </summary>
namespace DllEmailReceiverMaster
{
    public class Pl_EmailReciver : Utility
    {
        string employeeID, employeeName, emailID, category;
        public string Who { get; set; }
        public string EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }
        public string EmployeeName
        {
            get { return employeeName; }
            set { employeeName = value; }
        }
        public string EmailID
        {
            get { return emailID; }
            set { emailID = value; }
        }
        public string Category
        {
            get { return category; }
            set { category = value; }
        }
        public int CreatedBy { get; set; }
        public int UpDatedBy { get; set; }
    }

    class Dl_EmailReceiverMaster
    {
        public static void returnTable(Pl_EmailReciver pobj)
        {
            try
            {                
                Config connect = new Config();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("ProcEmailReceiverMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
               
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@EmpId", pobj.EmployeeID);
                sqlCmd.Parameters.AddWithValue("@Category", pobj.Category);
                sqlCmd.Parameters.AddWithValue("@EmpName", pobj.EmployeeName);
                sqlCmd.Parameters.AddWithValue("@EmailID", pobj.EmailID);

                sqlCmd.Parameters.AddWithValue("@CreatedBy", pobj.CreatedBy);
                sqlCmd.Parameters.AddWithValue("@UpDatedBy", pobj.UpDatedBy);
               
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
      

        public class Bl_EmailReceiverMaster
        {
            public static void insert(Pl_EmailReciver pobj)
            {
                pobj.Opcode = 11;
                Dl_EmailReceiverMaster.returnTable(pobj);
            }
            public static void update(Pl_EmailReciver pobj)
            {
                pobj.Opcode = 21;
                Dl_EmailReceiverMaster.returnTable(pobj);
            }
            public static void bindgrid(Pl_EmailReciver pobj)
            {
                pobj.Opcode = 41;
                Dl_EmailReceiverMaster.returnTable(pobj);
            }
            public static void bindallActivaAgent(Pl_EmailReciver pobj)
            {
                pobj.Opcode = 42;
                Dl_EmailReceiverMaster.returnTable(pobj);
            }
            public static void selectEmpdetails(Pl_EmailReciver pobj)
            {
                pobj.Opcode = 43;
                Dl_EmailReceiverMaster.returnTable(pobj);
            }
            public static void selectEmpdetailsById(Pl_EmailReciver pobj)
            {
                pobj.Opcode = 44;
                Dl_EmailReceiverMaster.returnTable(pobj);
            }
            public static void selectEmpdetailsbycategory(Pl_EmailReciver pobj)
            {
                //pobj.Opcode = 43;
                Dl_EmailReceiverMaster.returnTable(pobj);
            }
            public static void delete(Pl_EmailReciver pobj)
            {
                pobj.Opcode = 31;
                Dl_EmailReceiverMaster.returnTable(pobj);
            }
        }
    
}