﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllInvoiceTemplateMaster
{
    public class PL_InvoiceTemplateMaster : Utility
    {
        public int AutoId { get; set; }
        public int UserAutoId { get; set; }
        public int EmpTypeAutoId { get; set; }
        public string InvoiceTemplateName { get; set; }
        public string InvoiceDescription { get; set; }
    }
    public class DL_InvoiceTemplateMaster
    {
        public static void ReturnTable(PL_InvoiceTemplateMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcInvoiceTemplateListMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@UserAutoId", pobj.UserAutoId);
                sqlCmd.Parameters.AddWithValue("@InvoiceTemplateName", pobj.InvoiceTemplateName);
                sqlCmd.Parameters.AddWithValue("@InvoiceDescription", pobj.InvoiceDescription);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_InvoiceTemplateMaster
    {
        public static void getInvoiceTemplateList(PL_InvoiceTemplateMaster pobj)
        {
            pobj.Opcode = 41;
            DL_InvoiceTemplateMaster.ReturnTable(pobj);
        }
    }
}
