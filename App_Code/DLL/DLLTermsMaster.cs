﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DLLTermsMaster
{
    public class PL_Terms : Utility
    {
        public int TermsAutoId { get; set; }
        public string TermsId { get; set; }
        public string TermsName { get; set; }
        public string Description { get; set; }
        public int Status { get; set; } 
    }
    public class DL_Terms
    {
        public static void ReturnTable(PL_Terms pobj)
        {
            try
            { 
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcTermsMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@TermsAutoId", pobj.TermsAutoId);
                sqlCmd.Parameters.AddWithValue("@TermsId", pobj.TermsId);
                sqlCmd.Parameters.AddWithValue("@TermsName", pobj.TermsName);
                sqlCmd.Parameters.AddWithValue("@Description", pobj.Description);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch(Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_Terms
    {
        public static void insert(PL_Terms pobj)
        {
            pobj.Opcode = 11;
            DL_Terms.ReturnTable(pobj);
        }
        public static void update(PL_Terms pobj)
        {
            pobj.Opcode = 21;
            DL_Terms.ReturnTable(pobj);
        }
        public static void delete(PL_Terms pobj)
        {
            pobj.Opcode = 31;
            DL_Terms.ReturnTable(pobj);
        }  
        public static void select(PL_Terms pobj)
        {
            pobj.Opcode = 41;
            DL_Terms.ReturnTable(pobj);
        }
        public static void editTerms(PL_Terms pobj)
        {
            pobj.Opcode = 42;
            DL_Terms.ReturnTable(pobj);
        }
    }
}
