﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DllUtility;
using System.Data;

/// <summary>
/// Summary description for DLLoptimoRoute
/// </summary>
namespace DLLoptimoRoute
{
    public class PL_OptimoRoute : Utility
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int status { get; set; }
        public int AutoIdSalesPerson { get; set; }
        public int AutoIdDriver { get; set; }
    }
    public class DL_OptimoRoute
    {
        public static void ReturnData(PL_OptimoRoute pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[ProcOptimoRoute]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);

               
                cmd.Parameters.AddWithValue("@Status", pobj.status);
                cmd.Parameters.AddWithValue("@DriverAutoId", pobj.AutoIdDriver);
                cmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.AutoIdSalesPerson);

                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
               
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
    public class BL_OptimoRoute
    {
        public static void BindDropDown(PL_OptimoRoute pobj)
        {
            pobj.Opcode = 42;
            DL_OptimoRoute.ReturnData(pobj);
        }


        public static void BindReport(PL_OptimoRoute pobj)
        {
            pobj.Opcode = 41;
            DL_OptimoRoute.ReturnData(pobj);
        }
    }
}