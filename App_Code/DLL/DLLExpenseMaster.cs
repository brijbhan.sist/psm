﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for DLLExpenseMaster
/// </summary>
namespace DLLExpenseMaster
{
    public class PL_ExpenseMaster : Utility
	{
        public int AutoId { get; set; }
        public int Category { get; set; }
        public int PaymentTypeAutoId { get; set; }
        public string ExpenseDescription { get; set; }
        public string XmlCurrency { get; set; }
        public DateTime ExpenseDate { get; set; }
        public decimal ExpenseAmount { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public int UpdateBy { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string SearchBy { get; set; }
        public int PaymentSource { get; set; }
	}
    public class DL_ExpenseMaster
    {
        public static void Returntable(PL_ExpenseMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcExpenseMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@Category", pobj.Category);
                sqlCmd.Parameters.AddWithValue("@ExpenseDescription", pobj.ExpenseDescription);
                sqlCmd.Parameters.AddWithValue("@ExpenseAmount", pobj.ExpenseAmount);
                sqlCmd.Parameters.AddWithValue("@CreateBy", pobj.CreateBy);
                sqlCmd.Parameters.AddWithValue("@SearchBy", pobj.SearchBy);
                sqlCmd.Parameters.AddWithValue("@UpdateBy", pobj.UpdateBy);
                sqlCmd.Parameters.AddWithValue("@PaymentSource", pobj.PaymentSource);
                if (pobj.ExpenseDate > DateTime.MinValue && pobj.ExpenseDate < DateTime.MaxValue)
                    sqlCmd.Parameters.AddWithValue("@ExpenseDate", pobj.ExpenseDate);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@PaymetTypeAutoId", pobj.PaymentTypeAutoId);
                sqlCmd.Parameters.AddWithValue("@XmlCurrency", pobj.XmlCurrency);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);

                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_ExpenseMaster
    {
        public static void GetExpense(PL_ExpenseMaster pobj)
        {
            pobj.Opcode = 401;
            DL_ExpenseMaster.Returntable(pobj);
        }
        public static void GetPaymentType(PL_ExpenseMaster pobj)
        {
            pobj.Opcode = 41;
            DL_ExpenseMaster.Returntable(pobj);
        }
        public static void Binddropdown(PL_ExpenseMaster pobj)
        {
            pobj.Opcode = 42;
            DL_ExpenseMaster.Returntable(pobj);
        }
        public static void AddExpense(PL_ExpenseMaster pobj)
        {
            pobj.Opcode = 101;
            DL_ExpenseMaster.Returntable(pobj);
        }
        public static void EditExpense(PL_ExpenseMaster pobj)
        {
            pobj.Opcode = 402;
            DL_ExpenseMaster.Returntable(pobj);
        }
        public static void UpdateExpense(PL_ExpenseMaster pobj)
        {
            pobj.Opcode = 501;
            DL_ExpenseMaster.Returntable(pobj);
        }
        public static void DeleteExpense(PL_ExpenseMaster pobj)
        {
            pobj.Opcode = 301;
            DL_ExpenseMaster.Returntable(pobj);
        }
    }
}