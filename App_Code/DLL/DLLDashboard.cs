﻿using DllUtility;
using System;
using System.Data;
using System.Data.SqlClient;


namespace DLLDashboard
{
    public class PL_Dashboard : Utility
    {
        public int EmpAutoId { get; set; }
    }

    public class DL_Dashboard
    {
        public static void execute(PL_Dashboard pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcDashboard", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlCmd.CommandTimeout = 1000;
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_Dashboard
    {
        public static void bindSalesDashboard(PL_Dashboard obj)
        {
            obj.Opcode = 401;
            DL_Dashboard.execute(obj);
        }
        public static void bindDueList(PL_Dashboard obj)
        {
            obj.Opcode = 402;
            DL_Dashboard.execute(obj);
        }
        public static void bindCustomer(PL_Dashboard obj)
        {
            obj.Opcode = 403;
            DL_Dashboard.execute(obj);
        }
        public static void InvalidPacking(PL_Dashboard obj)
        {
            obj.Opcode = 404;
            DL_Dashboard.execute(obj);
        }
    }
}
