﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DllUtility;
namespace DLLPattyCashCategoryMaster
{
    public class PL_PattyCashCategoryMaster : Utility
    {
        public int CategoryAutoId { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
    }
    public class DL_PattyCashCategoryMaster
    {
        public static void ReturnTable(PL_PattyCashCategoryMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPattyCashCategoryMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@CategoryId", pobj.CategoryId);
                sqlCmd.Parameters.AddWithValue("@CategoryName", pobj.CategoryName);
                sqlCmd.Parameters.AddWithValue("@Description", pobj.Description);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_PattyCashCategoryMaster
    {
        public static void insert(PL_PattyCashCategoryMaster pobj)
        {
            pobj.Opcode = 11;
            DL_PattyCashCategoryMaster.ReturnTable(pobj);
        }
        public static void update(PL_PattyCashCategoryMaster pobj)
        {
            pobj.Opcode = 21;
            DL_PattyCashCategoryMaster.ReturnTable(pobj);
        }
        public static void delete(PL_PattyCashCategoryMaster pobj)
        {
            pobj.Opcode = 31;
            DL_PattyCashCategoryMaster.ReturnTable(pobj);
        }
        public static void select(PL_PattyCashCategoryMaster pobj)
        {
            pobj.Opcode = 41;
            DL_PattyCashCategoryMaster.ReturnTable(pobj);
        }
        public static void editCategory(PL_PattyCashCategoryMaster pobj)
        {
            pobj.Opcode = 42;
            DL_PattyCashCategoryMaster.ReturnTable(pobj);
        }
    }
}