﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLCheckReceivedPayment
/// </summary>
namespace DLLCheckReceivedPayment
{
	public class PL_CheckReceivedPayment : Utility
	{
        public int Customer { get; set; }
        
        public int ReceivedBy { get; set; }
        public int Status { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime DepositFromDate { get; set; }
        public DateTime DepositToDate { get; set; }
	}
    public class DL_CheckReceivedPayment
    {
        public static void ReturnTable(PL_CheckReceivedPayment pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcCheckReceivedPayment", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@Customer", pobj.Customer);
                sqlCmd.Parameters.AddWithValue("@ReceivedBy", pobj.ReceivedBy);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                
                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                if (pobj.DepositFromDate != DateTime.MinValue && pobj.DepositFromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@DepositFromDate", pobj.DepositFromDate);
                }
                if (pobj.DepositToDate != DateTime.MinValue && pobj.DepositToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@DepositToDate", pobj.DepositToDate);
                }

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);               

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_CheckReceivedPayment
    {
        public static void GetReport(PL_CheckReceivedPayment pobj)
        {
            pobj.Opcode = 41;
            DL_CheckReceivedPayment.ReturnTable(pobj);
        }
        
        
        public static void GetCustomer(PL_CheckReceivedPayment pobj)
        {
            pobj.Opcode = 42;
            DL_CheckReceivedPayment.ReturnTable(pobj);
        }
    }
}