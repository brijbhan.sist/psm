﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using DllUtility;


/// <summary>
/// Summary description for DLLPackerReportSummary
/// </summary>
namespace DLLPackerReportSummary
{
    public class PL_PackerReportSummary : Utility
	{
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int PackerAutoId { get; set; }
	}
    public class DL_PackerReportSummary
    {
        public static void ReturnData(PL_PackerReportSummary pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[ProcPackerReportSummary]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@PackerAutoId", pobj.PackerAutoId);
               




                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@DateFrom", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@DateTo", pobj.ToDate);
                }

                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                cmd.CommandTimeout = 1000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
    public class BL_PackerReportSummary
    {
        public static void GetPackerName(PL_PackerReportSummary pobj)
        {
            pobj.Opcode = 41;
            DL_PackerReportSummary.ReturnData(pobj);
        }
        public static void GetPackerResport(PL_PackerReportSummary pobj)
        {
            pobj.Opcode = 42;
           DL_PackerReportSummary.ReturnData(pobj);
        }
        public static void BindProductCategory(PL_PackerReportSummary pobj)
        {
            pobj.Opcode = 43;
           // DL_DetailProductSalesRepor.ReturnData(pobj);
        }
    }
}