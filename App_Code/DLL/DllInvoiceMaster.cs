﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllInvoiceMaster
{
    public class PL_InvoiceMaster : Utility
    {
        public int InvAutoId { get; set; }
        public int InvNo { get; set; }
        public DateTime InvDate { get; set; }
        public int OrderAutoId { get; set; }        
        public int EmpAutoId { get; set; }

        public int LoginEmpType { get; set; }
        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }           
        public decimal TotalAmount { get; set; }
        public decimal OverallDisc { get; set; }
        public decimal OverallDiscAmt { get; set; }
        public decimal ShippingCharges { get; set; }
        public decimal TotalTax { get; set; }
        public decimal GrandTotal { get; set; }

        public DataTable TableValue { get; set; }
        public DateTime FromInvDate { get; set; }
        public DateTime ToInvDate { get; set; }
        
        public int InvStatus { get; set; }
    }
    public class DL_InvoiceMaster 
    {
        public static void ReturnTable(PL_InvoiceMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcGenerateInvoice", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@InvAutoId", pobj.InvAutoId);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@InvNo", pobj.InvNo);
                if (pobj.InvDate != DateTime.MinValue && pobj.InvDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@InvDate", pobj.InvDate);
                }
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                //sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
                sqlCmd.Parameters.AddWithValue("@InvStatus", pobj.InvStatus);                
                //if (pobj.FromInvDate != DateTime.MinValue && pobj.FromInvDate != DateTime.MaxValue)
                //{
                //    sqlCmd.Parameters.AddWithValue("@FromInvDate", pobj.FromInvDate);
                //}
                //if (pobj.ToInvDate != DateTime.MinValue && pobj.ToInvDate != DateTime.MaxValue)
                //{
                //    sqlCmd.Parameters.AddWithValue("@ToInvDate", pobj.ToInvDate);
                //}
                //sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                //sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);                 
                sqlCmd.Parameters.AddWithValue("@TotalAmt", pobj.TotalAmount);
                sqlCmd.Parameters.AddWithValue("@OverallDisc", pobj.OverallDisc);
                sqlCmd.Parameters.AddWithValue("@OverallDiscAmt", pobj.OverallDiscAmt);
                sqlCmd.Parameters.AddWithValue("@Shipping", pobj.ShippingCharges);
                sqlCmd.Parameters.AddWithValue("@TotalTax", pobj.TotalTax);
                sqlCmd.Parameters.AddWithValue("@GrandTotal", pobj.GrandTotal);
                sqlCmd.Parameters.AddWithValue("@TableValue", pobj.TableValue);                

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_InvoiceMaster
    {
        public static void insert(PL_InvoiceMaster pobj)
        {
            pobj.Opcode = 11;
            DL_InvoiceMaster.ReturnTable(pobj);
        }
        //public static void updateOrder(PL_InvoiceMaster pobj)
        //{
        //    pobj.Opcode = 21;
        //    DL_InvoiceMaster.ReturnTable(pobj);
        //}
        //public static void delete(PL_InvoiceMaster pobj)
        //{
        //    pobj.Opcode = 31;
        //    DL_InvoiceMaster.ReturnTable(pobj);
        //}
        //public static void bindDropdown(PL_InvoiceMaster pobj)
        //{
        //    pobj.Opcode = 41;
        //    DL_InvoiceMaster.ReturnTable(pobj);
        //}
        //public static void selectAddress(PL_InvoiceMaster pobj)
        //{
        //    pobj.Opcode = 42;
        //    DL_InvoiceMaster.ReturnTable(pobj);
        //}
        //public static void selectQtyPrice(PL_InvoiceMaster pobj)
        //{
        //    pobj.Opcode = 43;
        //    DL_InvoiceMaster.ReturnTable(pobj);
        //}
        //public static void bindUnitType(PL_InvoiceMaster pobj)
        //{
        //    pobj.Opcode = 44;
        //    DL_InvoiceMaster.ReturnTable(pobj);
        //}
        //public static void selectOrderList(PL_InvoiceMaster pobj)
        //{
        //    pobj.Opcode = 45;
        //    DL_InvoiceMaster.ReturnTable(pobj);
        //}
        //public static void editOrderDetails(PL_InvoiceMaster pobj)
        //{
        //    pobj.Opcode = 46;
        //    DL_InvoiceMaster.ReturnTable(pobj);
        //}
        //public static void bindStatus(PL_InvoiceMaster pobj)
        //{
        //    pobj.Opcode = 47;
        //    DL_InvoiceMaster.ReturnTable(pobj);
        //}
    }
}
