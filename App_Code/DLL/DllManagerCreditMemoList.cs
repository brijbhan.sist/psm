﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DllManagerCreditMemoList
/// </summary>
namespace DllManagerCreditMemoList
{
    public class PL_ManagerCreditMemoList : Utility
    {
        public int CustomerAutoId { get; set; }
        public int EmpAutoId { get; set; }
        public int CreditAutoId { get; set; }
        public int sortInCode { get; set; }
        public int OrderAutoId { get; set; }
        public int ProductAutoId { get; set; }
        public int Status { get; set; }
        public int CreditType { get; set; }
        public string CreditNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int EmpType { get; set; }              
        public int SalesPerson { get; set; }



    }
    public class DL_ManagerCreditMemoList
    {
        public static void ReturnTable(PL_ManagerCreditMemoList pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcMangerCreditMemolist", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@CreditType", pobj.CreditType);
                sqlCmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                sqlCmd.Parameters.AddWithValue("@CreditNo", pobj.CreditNo);
                sqlCmd.Parameters.AddWithValue("@CreditAutoId", pobj.CreditAutoId);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@sortInCode", pobj.sortInCode);
                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
              
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }
    }

    public class BL_ManagerCreditMemoList
    {
        
        public static void bindDropdown(PL_ManagerCreditMemoList pobj)
        {
            pobj.Opcode = 41;
            DL_ManagerCreditMemoList.ReturnTable(pobj);
        }
        public static void bindCustomer(PL_ManagerCreditMemoList pobj)
        {
            pobj.Opcode = 42;
            DL_ManagerCreditMemoList.ReturnTable(pobj);
        }
        public static void select(PL_ManagerCreditMemoList pobj)
        {
            pobj.Opcode = 44;
            DL_ManagerCreditMemoList.ReturnTable(pobj);
        }
        public static void BindCreditLog(PL_ManagerCreditMemoList pobj)
        {
            pobj.Opcode = 47;
            DL_ManagerCreditMemoList.ReturnTable(pobj);
        }
    }
}