﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLDashbordMaster
/// </summary>
/// 
namespace DLLDashboardMaster
{
    public class PL_DashboardMaster : Utility
    {
        public int EmpAutoId { get; set; }
        public int Type { get; set; }
        public int AutoId { get; set; }
        public int Emptype { get; set; }

    }
    public class DL_DashboardMaster
    {
        public static void ReturnTable(PL_DashboardMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcDashboardMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@Emptype", pobj.Emptype);
                sqlCmd.Parameters.AddWithValue("@Type", pobj.Type);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }

        }
    }

    public class BL_DashboardMaster
    {
        public static void select(PL_DashboardMaster pobj)
        {
            pobj.Opcode = 41;
            DL_DashboardMaster.ReturnTable(pobj);
        }
        public static void getSalesRevenue(PL_DashboardMaster pobj)
        {
            pobj.Opcode = 42;
            DL_DashboardMaster.ReturnTable(pobj);
        }
        public static void CollectionDetails(PL_DashboardMaster pobj)
        {
            pobj.Opcode = 43;
            DL_DashboardMaster.ReturnTable(pobj);
        }
        public static void CreditMemoDetails(PL_DashboardMaster pobj)
        {
            pobj.Opcode = 44;
            DL_DashboardMaster.ReturnTable(pobj);
        }
        public static void getHoldPaymentList(PL_DashboardMaster pobj)
        {
            pobj.Opcode = 45;
            DL_DashboardMaster.ReturnTable(pobj);
        }
        public static void getTicketList(PL_DashboardMaster pobj)
        {
            pobj.Opcode = 46;
            DL_DashboardMaster.ReturnTable(pobj);
        }
        public static void getNotification(PL_DashboardMaster pobj)
        {
            pobj.Opcode = 47;
            DL_DashboardMaster.ReturnTable(pobj);
        } 
        public static void getNotification_Message(PL_DashboardMaster pobj)
        {
            pobj.Opcode = 48;
            DL_DashboardMaster.ReturnTable(pobj);
        }
        public static void getWebOrderNotification(PL_DashboardMaster pobj)
        {
            pobj.Opcode = 49;
            DL_DashboardMaster.ReturnTable(pobj);
        }
    }
}