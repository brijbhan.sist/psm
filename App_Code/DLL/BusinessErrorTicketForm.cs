﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BusinessErrorTicketForm
/// </summary>
public class BusinessErrorTicketForm
{
    public static void insert(PropertyErrorTicketForm pobj)
    {
        pobj.OpCode = 11;
        DataErrorTicketForm.returnTable(pobj);
    }
    public static void searchbyparameter(PropertyErrorTicketForm pobj)
    {
        pobj.OpCode = 61;
        DataErrorTicketForm.returnTable(pobj);
    }
    public static void search(PropertyErrorTicketForm pobj)
    {
        pobj.OpCode = 41;
        DataErrorTicketForm.returnTable(pobj);
    }
 }