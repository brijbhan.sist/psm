﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data;
using System.Data.SqlClient;
namespace DLLNotShippedReport
{
	public class PL_NotShippedReport:Utility
	{
        public int Status { get; set; }
        public int SalsePersonAutoid { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }       
	}

    public class DL_NotShippedReport
    {
        public static void ReturnData(PL_NotShippedReport pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[ProcNotShippedReport]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@Status", pobj.Status);
                cmd.Parameters.AddWithValue("@SalsePersonAutoid", pobj.SalsePersonAutoid);               
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
    public class BL_DLLNotShippedReport
    {
        public static void BindStatussalesperson(PL_NotShippedReport pobj)
        {
            pobj.Opcode = 41;
            DL_NotShippedReport.ReturnData(pobj);
        }
        public static void BindNotShipedReport(PL_NotShippedReport pobj)
        {
            pobj.Opcode = 42;
            DL_NotShippedReport.ReturnData(pobj);
        }
       public static void NotShipedReportExport(PL_NotShippedReport pobj)
       {
            pobj.Opcode = 42;
            DL_NotShippedReport.ReturnData(pobj);
       }
    }
}