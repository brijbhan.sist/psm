﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
using System.Web;

namespace DLL_Manager_OrderMaster
{
    public class PL_Manager_OrderMaster : Utility
    { 
        public PL_Manager_OrderMaster()
        {

        }
        public PL_Manager_OrderMaster(string ipAddress)
        {
            IPAddress = ipAddress;
        }
        public int OrderAutoId { get; set; }
        public int DriverAutoId { get; set; }
        public DateTime AsgnDate { get; set; }
        public int CustomerAutoId { get; set; }
        public int OrderStatus { get; set; }
        public string OrdStatus { get; set; }
        public int OrderType { get; set; }
        public string OrderNo { get; set; }
        public int SalesPersonAutoId { get; set; }
        public string SalesPerson { get; set; }
        public int LoginEmpType { get; set; }
        public int EmpAutoId { get; set; }
        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int ReqQty { get; set; }
        public int QtyPerUnit { get; set; }
        public string Remarks { get; set; }
        public DataTable DelItems { get; set; }
        public DateTime Fromdate { get; set; }
        public DateTime Todate { get; set; }
        public string CheckSecurity { get; set; }
        public string TypeShipping { get; set; }
        public string ShippingAddress2 { get; set; }
        public string MLTaxRemark { get; set; }
        public string Remark { get; set; }
        public string OrdLat { get; set; }
        public string OrdLong { get; set; }
        public DateTime DeliveryFromDate { get; set; }
        public DateTime DeliveryToDate { get; set; }
        public string OrdState { get; set; }
        public string OrdCity { get; set; }
        public string OrdZipcode { get; set; }
        public string OrdAddress { get; set; }
        public int OrderBy { get; set; }
        public string StateFilter { get; set; }
    }

    public class DL_Manager_OrderMaster
    {
        public static void ReturnTable(PL_Manager_OrderMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("Proc_Manager_OrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@IPAddress", pobj.IPAddress);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@DriverAutoId", pobj.DriverAutoId);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
                sqlCmd.Parameters.AddWithValue("@ReqQty", pobj.ReqQty);
                sqlCmd.Parameters.AddWithValue("@QtyPerUnit", pobj.QtyPerUnit);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                sqlCmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                sqlCmd.Parameters.AddWithValue("@OrdStatus", pobj.OrdStatus);
                sqlCmd.Parameters.AddWithValue("@OrderType", pobj.OrderType);
                sqlCmd.Parameters.AddWithValue("@OrderBy", pobj.OrderBy);
                sqlCmd.Parameters.AddWithValue("@CheckSecurity", pobj.CheckSecurity);
                sqlCmd.Parameters.AddWithValue("@TypeShipping", pobj.TypeShipping);
                if (pobj.AsgnDate != DateTime.MinValue && pobj.AsgnDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@AsgnDate", pobj.AsgnDate);
                }
                if (HttpContext.Current.Session["EmpAutoId"] != null)
                    sqlCmd.Parameters.AddWithValue("@EmpAutoId", HttpContext.Current.Session["EmpAutoId"].ToString());
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                if (pobj.Fromdate != DateTime.MinValue && pobj.Fromdate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Fromdate", pobj.Fromdate);
                }
                if (pobj.Todate != DateTime.MinValue && pobj.Todate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Todate", pobj.Todate);
                }
                if (pobj.DeliveryFromDate != DateTime.MinValue && pobj.DeliveryFromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@DeliveryFromdate", pobj.DeliveryFromDate);
                }
                if (pobj.DeliveryToDate != DateTime.MinValue && pobj.DeliveryToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@DeliveryToDate", pobj.DeliveryToDate);
                }
                sqlCmd.Parameters.AddWithValue("@MLTaxRemark", pobj.MLTaxRemark);
                sqlCmd.Parameters.AddWithValue("@OrdLat", pobj.OrdLat);
                sqlCmd.Parameters.AddWithValue("@OrdLong", pobj.OrdLong);

                sqlCmd.Parameters.AddWithValue("@OrdState", pobj.OrdState);
                sqlCmd.Parameters.AddWithValue("@OrdCity", pobj.OrdCity);
                sqlCmd.Parameters.AddWithValue("@OrdZipcode", pobj.OrdZipcode);
                sqlCmd.Parameters.AddWithValue("@OrdAddress", pobj.OrdAddress);
                sqlCmd.Parameters.AddWithValue("@ShippingAddress2", pobj.ShippingAddress2);
                sqlCmd.Parameters.AddWithValue("@StateFilter", pobj.StateFilter);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_Manager_OrderMaster
    {

        public static void getOrderList(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 41;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }


        public static void getOrderData(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 42;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void bindStatus(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 43;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }

        public static void getDriverList(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 44;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void ProductSaleHistory(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 45;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }

        public static void viewAllRemarks(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 46;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void SetAsProcess(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 21;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }

        public static void AssignDriver(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 22;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void cancelOrder(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 23;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void clickonSecurity(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 24;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void ViewOrderDetails(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 25;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void BindDriver(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 26;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void ConfirmSecurity(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 27;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void UpdateShippingAddress(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 28;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void BindshippingAddress(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 29;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void bindSalesperson(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 30;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void changeSalesPerson(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 31;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void UpdateDriverRemark(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 32;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void changeOrderStatus(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 33;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
        public static void getDriverRemark(PL_Manager_OrderMaster pobj)
        {
            pobj.Opcode = 34;
            DL_Manager_OrderMaster.ReturnTable(pobj);
        }
    }
}
