﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using DllUtility;


/// <summary>
/// Summary description for DLLPLProductWiseReport
/// </summary>
namespace DLLPLProductWiseReport
{
    public class PL_PLProductWiseReport : Utility
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int SalesPerson { get; set; }
        public int CustomerAutoId { get; set; }
        public int ProductAutoId { get; set; }
        public int CategoryAutoId { get; set; }
        public int SubCategoryAutoId { get; set; }
    }
    public class DL_PLProductWiseReport
    {
        public static void ReturnData(PL_PLProductWiseReport pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[ProcProductWisePLReport]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                cmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                cmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                cmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                cmd.Parameters.AddWithValue("@SubCategoryAutoId", pobj.SubCategoryAutoId);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }

                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                cmd.CommandTimeout = 1000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
    public class BL_PLProductWiseReport
    {
        public static void AllddlList(PL_PLProductWiseReport pobj)
        {
            pobj.Opcode = 41;
            DL_PLProductWiseReport.ReturnData(pobj);
        }

        public static void SubCategory(PL_PLProductWiseReport pobj)
        {
            pobj.Opcode = 42;
            DL_PLProductWiseReport.ReturnData(pobj);
        }
        public static void BindProduct(PL_PLProductWiseReport pobj)
        {
            pobj.Opcode = 43;
            DL_PLProductWiseReport.ReturnData(pobj);
        }
        public static void BindCustomer(PL_PLProductWiseReport pobj)
        {
            pobj.Opcode = 44;
            DL_PLProductWiseReport.ReturnData(pobj);
        }
        public static void ProductWiseReport(PL_PLProductWiseReport pobj)
        {
            pobj.Opcode = 45;
            DL_PLProductWiseReport.ReturnData(pobj);
        }

        
    }
}