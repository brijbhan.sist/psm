﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLBrandMaster
/// </summary>
namespace DLLBrandMaster
{
    public class PL_BrandMaster : Utility
    {
        public int AutoId { get; set; }
        public string BrandId { get; set; }
        public string BrandName { get; set; }
        public string Description { get; set; }
        public int Status { get; set; } 
    }
    public class DL_BrandMaster
    {
        public static void ReturnTable(PL_BrandMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcBrandMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@Who", pobj.Who);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@BrandId", pobj.BrandId);
                sqlCmd.Parameters.AddWithValue("@BrandName", pobj.BrandName);
                sqlCmd.Parameters.AddWithValue("@Description", pobj.Description);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_BrandMaster
    {
        public static void insert(PL_BrandMaster pobj)
        {
            pobj.Opcode = 11;
            DL_BrandMaster.ReturnTable(pobj);
        }
        public static void update(PL_BrandMaster pobj)
        {
            pobj.Opcode = 21;
            DL_BrandMaster.ReturnTable(pobj);
        }
        public static void delete(PL_BrandMaster pobj)
        {
            pobj.Opcode = 31;
            DL_BrandMaster.ReturnTable(pobj);
        }
        public static void select(PL_BrandMaster pobj)
        {
            pobj.Opcode = 41;
            DL_BrandMaster.ReturnTable(pobj);
        }
        public static void editBrand(PL_BrandMaster pobj)
        {
            pobj.Opcode = 42;
            DL_BrandMaster.ReturnTable(pobj);
        }
    }
}