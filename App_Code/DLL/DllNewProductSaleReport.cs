﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DLLDetailProductSalesReport
/// </summary>
namespace DLLNewProductSaleReport
{
    public class PL_NewProductSalesReport : Utility
    {
        public int OrderStatus { get; set; }
        public int SalesPerson { get; set; }
        public string SalesPersonString { get; set; }
        public int CategoryAutoId { get; set; }
        public int SubCategoryId { get; set; }
        public int EmpAutoId { get; set; }
        public int ProductAutoId { get; set; }
        public int PriceLevelAutoId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string BrandId { get; set; }
        public int CustomerAutoId { get; set; }
    }
    public class DL_NewProductSalesReport
    {
        public static void ReturnData(PL_NewProductSalesReport pobj)
        {
            try

            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("ProcNewProductSaleReport", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                cmd.Parameters.AddWithValue("@SalesPersonString", pobj.SalesPersonString);
                cmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                cmd.Parameters.AddWithValue("@SubCategoryId", pobj.SubCategoryId);
                cmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                cmd.Parameters.AddWithValue("@PriceLevelAutoId", pobj.PriceLevelAutoId);
                cmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                cmd.Parameters.AddWithValue("@BrandAutoIdSring", pobj.BrandId);
                cmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                cmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                cmd.CommandTimeout = 1000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }

    public class BL_NewProductSalesReport
    {
        public static void BindProductSaleReport(PL_NewProductSalesReport pobj)
        {
            pobj.Opcode = 42;
            DL_NewProductSalesReport.ReturnData(pobj);
        }
 
        public static void BindCustomer(PL_NewProductSalesReport pobj)
        {
            pobj.Opcode = 45;
            DL_NewProductSalesReport.ReturnData(pobj);
        }
      
    }
}