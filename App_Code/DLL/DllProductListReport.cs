﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DllUtility;
namespace DllProductListReport
{
    public class PL_ProductListReport:Utility
    {
        public int Websitedispaly { get; set; }
        public string ProductName { get; set; }
        public int CategoryAutoId { get; set; }
        public int SubCategoryAutoId { get; set; }
        public int BrandAutoId { get; set; }
        public int ProductId { get; set; }
        public int ShowOnWebsite { get; set; }
        public int EmpAutoId { get; set; }
    }
    public class DL_ProductListReport
    {
       public static void ReturnTable(PL_ProductListReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProProductListREport", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@WebsiteDisplay", pobj.Websitedispaly);
                sqlCmd.Parameters.AddWithValue("@ProductName", pobj.ProductName);
                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@SubCategoryAutoId", pobj.SubCategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@BrandAutoId", pobj.BrandAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductId", pobj.ProductId);
                sqlCmd.Parameters.AddWithValue("@ShowOnWebsite", pobj.ShowOnWebsite);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
              
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch ( Exception ex)
            {
                pobj.isException=true;
                pobj.exceptionMessage=ex.Message;
            }
        }
    }
    public class BL_ProductListReport
    {
        public static void updateStatus(PL_ProductListReport pobj)
        {
            pobj.Opcode = 21;
            DL_ProductListReport.ReturnTable(pobj);
        }
        public static void getProductList(PL_ProductListReport pobj)
        {
            pobj.Opcode = 41;
            DL_ProductListReport.ReturnTable(pobj);
        } 
        public static void bindCategory(PL_ProductListReport pobj)
        {
            pobj.Opcode = 42;
            DL_ProductListReport.ReturnTable(pobj);
        }
        public static void bindSubCategory(PL_ProductListReport pobj)
        {
            pobj.Opcode = 43;
            DL_ProductListReport.ReturnTable(pobj);
        }
    }
}