﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DllUtility;
namespace DLLDraftCustomerList
{
    public class PL_DraftCustomerList : Utility
    {
        public int CustomerAutoId { get; set; }
        public int StateAutoId { get; set; }
        public int CityAutoId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int CustomerType { get; set; } 
        public int EmpAutoId { get; set; }
        public int Status { get; set; }
        public string OrderNo { get; set; }
        public int SalesPersonAutoId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string CustomerList { get; set; }
        public string State { get; set; }
        public string City { get; set; }
    }

    public class DL_DraftCustomerList
    {
        public static void ReturnTable(PL_DraftCustomerList pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcDraftCustomerList", connect.con);
                sqlCmd.CommandTimeout = 100000;
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@CustomerList", pobj.CustomerList);
                sqlCmd.Parameters.AddWithValue("@StateAutoId", pobj.StateAutoId);
                sqlCmd.Parameters.AddWithValue("@CityAutoId", pobj.CityAutoId); 
                sqlCmd.Parameters.AddWithValue("@CustomerName", pobj.CustomerName);
                sqlCmd.Parameters.AddWithValue("@CustomerType", pobj.CustomerType);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                sqlCmd.Parameters.AddWithValue("@State", pobj.State);
                sqlCmd.Parameters.AddWithValue("@City", pobj.City);


                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_DraftCustomerList
    {
        public static void select(PL_DraftCustomerList pobj)
        {
            pobj.Opcode = 41;
            DL_DraftCustomerList.ReturnTable(pobj);
        }
        public static void bindDropDowns(PL_DraftCustomerList pobj)
        {
            pobj.Opcode = 42;
            DL_DraftCustomerList.ReturnTable(pobj);
        }
        public static void deleteCustomer(PL_DraftCustomerList pobj)
        {
            pobj.Opcode = 31;
            DL_DraftCustomerList.ReturnTable(pobj);
        }       
    }
}