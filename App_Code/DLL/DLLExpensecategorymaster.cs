﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DllUtility;

namespace DLLExpensecategorymaster
{
    public class PL_Expensecategorymaster:Utility
    {
        public int CategoryAutoId { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
       
    }
    public class DL_Expensecategorymaster
    {
        public static void ReturnTable(PL_Expensecategorymaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcExpensecategorymaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@CategoryId", pobj.CategoryId);
                sqlCmd.Parameters.AddWithValue("@CategoryName", pobj.CategoryName);
                sqlCmd.Parameters.AddWithValue("@Description", pobj.Description);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_Expensecategorymaster
    {
        public static void insert(PL_Expensecategorymaster pobj)
        {
            pobj.Opcode = 11;
            DL_Expensecategorymaster.ReturnTable(pobj);
        }
        public static void update(PL_Expensecategorymaster pobj)
        {
            pobj.Opcode = 21;
            DL_Expensecategorymaster.ReturnTable(pobj);
        }
        public static void delete(PL_Expensecategorymaster pobj)
        {
            pobj.Opcode = 31;
            DL_Expensecategorymaster.ReturnTable(pobj);
        }
        public static void select(PL_Expensecategorymaster pobj)
        {
            pobj.Opcode = 41;
            DL_Expensecategorymaster.ReturnTable(pobj);
        }
        public static void editCategory(PL_Expensecategorymaster pobj)
        {
            pobj.Opcode = 42;
            DL_Expensecategorymaster.ReturnTable(pobj);
        }
    }
}