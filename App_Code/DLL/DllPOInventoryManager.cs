using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllPOInventoryManager
{
    public class PL_PurchaseOrder : Utility
    {
        public int POAutoId { get; set; }
        public string OrderNo { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int VenderAutoId { get; set; }
        public int OrderStatus { get; set; }
        public string PoStatus { get; set; }
        public int EmpId { get; set; }
        public int NOofItems { get; set; }
        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int ReqQty { get; set; }
        public string PORemarks { get; set; }
        public string ReciveStockRemark { get; set; }
        public string Barcode { get; set; }
        public string TableData { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int QtyPerUnit { get; set; } 
        public int RecAutoId { get; set; }
        public string VendorName { get; set; }
        public DataTable DtPackingDetails { get; set; }
        public decimal CostPrice { get; set; }
        public string BillNo { get; set; }
        public DateTime BillDate { get; set; }
        public string ManagerRemark { get; set; }

    }

    public class DL_PurchaseOrder
    {
        public static void ReturnTable(PL_PurchaseOrder pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPOInventoryManager", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@POAutoId", pobj.POAutoId);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                if (pobj.DeliveryDate != DateTime.MinValue && pobj.DeliveryDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@DeliveryDate", pobj.DeliveryDate);
                }
                sqlCmd.Parameters.AddWithValue("@VenderAutoId", pobj.VenderAutoId);
                sqlCmd.Parameters.AddWithValue("@VendorName", pobj.VendorName);
                sqlCmd.Parameters.AddWithValue("@ReqQty", pobj.ReqQty);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@RecAutoId", pobj.RecAutoId);
                sqlCmd.Parameters.AddWithValue("@QtyPerUnit", pobj.QtyPerUnit);
                sqlCmd.Parameters.AddWithValue("@BillNo", pobj.BillNo);
                if (pobj.BillDate != DateTime.MinValue && pobj.BillDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@BillDate", pobj.BillDate);
                }
                sqlCmd.Parameters.AddWithValue("@ManagerRemark", pobj.ManagerRemark);
                sqlCmd.Parameters.AddWithValue("@EmpId", pobj.EmpId);
                sqlCmd.Parameters.AddWithValue("@CostPrice", pobj.CostPrice);
                sqlCmd.Parameters.AddWithValue("@NoofItems", pobj.NOofItems);
                sqlCmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                sqlCmd.Parameters.AddWithValue("@dtbulkUnit", pobj.DtPackingDetails);
                sqlCmd.Parameters.AddWithValue("@PoStatus", pobj.PoStatus);
                sqlCmd.Parameters.AddWithValue("@RecieveStockRemark", pobj.ReciveStockRemark);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);
                sqlCmd.Parameters.AddWithValue("@TableValue", pobj.TableData);
                sqlCmd.Parameters.AddWithValue("@PORemarks", pobj.PORemarks);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Fromdate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Todate", pobj.ToDate);
                }
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_PurchaseOrder
    {

        public static void GetPOList(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 41;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void bindStatus(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 42;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void bindVendor(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 43;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void BindPoData(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 44;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void getManagePrice(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 45;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void UpdateManagePrice(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 21;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
   
        public static void UpdateCostPrice(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 22;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void PartialRecievePO(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 23;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void UpdateCostPriceOnPO(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 24;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
        public static void BindRecStatus(PL_PurchaseOrder pobj)
        {
            pobj.Opcode = 25;
            DL_PurchaseOrder.ReturnTable(pobj);
        }
    }
}