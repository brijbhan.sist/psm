﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllStockEntryMaster
{
    public class PL_StockEntryMaster : Utility
    {
        public int BillAutoId { get; set; }
        public int VendorAutoId { get; set; }
        public string BillNo { get; set; }
        public DateTime BillDate { get; set; }
        public string Remarks { get; set; }
        public int ItemAutoId { get; set; }
        public int BillItemsAutoId { get; set; }
        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int Quantity { get; set; }
        public int TotalPieces { get; set; }
        public int QtyPerUnit { get; set; }
        public decimal BasePrice { get; set; }
        public decimal RetailMIN { get; set; }
        public decimal CostPrice { get; set; }
        public decimal SRP { get; set; }
        public decimal WholesaleMinPrice { get; set; }
        public DateTime FromBillDate { get; set; }
        public DateTime ToBillDate { get; set; }
        public DataTable TableValue { get; set; }
        public DataTable TblUpdateItems { get; set; }
        public string Barcode { get; set; }
        public int EmpAutoId { get; set; }
    }

    public class DL_StockEntryMaster
    {
        public static void ReturnTable(PL_StockEntryMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcStockEntryMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@BillAutoId", pobj.BillAutoId);
                sqlCmd.Parameters.AddWithValue("@ItemAutoId", pobj.ItemAutoId);
                sqlCmd.Parameters.AddWithValue("@BasePrice", pobj.BasePrice);
                sqlCmd.Parameters.AddWithValue("@RetailMIN", pobj.RetailMIN);
                sqlCmd.Parameters.AddWithValue("@CostPrice", pobj.CostPrice);
                sqlCmd.Parameters.AddWithValue("@SRP", pobj.SRP);
                sqlCmd.Parameters.AddWithValue("@WholesaleMinPrice", pobj.WholesaleMinPrice);
                sqlCmd.Parameters.AddWithValue("@VendorAutoId", pobj.VendorAutoId);
                sqlCmd.Parameters.AddWithValue("@BillNo", pobj.BillNo);
                if (pobj.BillDate != DateTime.MinValue && pobj.BillDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@BillDate", pobj.BillDate);
                }
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@BillItemsAutoId", pobj.BillItemsAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@Quantity", pobj.Quantity);
                sqlCmd.Parameters.AddWithValue("@TotalPieces", pobj.TotalPieces);
                sqlCmd.Parameters.AddWithValue("@QtyPerUnit", pobj.QtyPerUnit);
                if (pobj.FromBillDate != DateTime.MinValue && pobj.FromBillDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromBillDate", pobj.FromBillDate);
                }
                if (pobj.ToBillDate != DateTime.MinValue && pobj.ToBillDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToBillDate", pobj.ToBillDate);
                }
                sqlCmd.Parameters.AddWithValue("@TableValue", pobj.TableValue);
                sqlCmd.Parameters.AddWithValue("@TblUpdateItems", pobj.TblUpdateItems);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_StockEntryMaster
    {
        public static void insert(PL_StockEntryMaster pobj)
        {
            pobj.Opcode = 11;
            DL_StockEntryMaster.ReturnTable(pobj);
        }
        public static void update(PL_StockEntryMaster pobj)
        {
            pobj.Opcode = 21;
            DL_StockEntryMaster.ReturnTable(pobj);
        }
        //public static void delete(PL_StockEntryMaster pobj)
        //{
        //    pobj.Opcode = 31;
        //    DL_StockEntryMaster.ReturnTable(pobj);
        //}
        public static void bindProduct(PL_StockEntryMaster pobj)
        {
            pobj.Opcode = 41;
            DL_StockEntryMaster.ReturnTable(pobj);
        }
        public static void bindUnitType(PL_StockEntryMaster pobj)
        {
            pobj.Opcode = 42;
            DL_StockEntryMaster.ReturnTable(pobj);
        }
        public static void stockEntryList(PL_StockEntryMaster pobj)
        {
            pobj.Opcode = 43;
            DL_StockEntryMaster.ReturnTable(pobj);
        }
        public static void editStockEntry(PL_StockEntryMaster pobj)
        {
            pobj.Opcode = 44;
            DL_StockEntryMaster.ReturnTable(pobj);
        }
        public static void getProductThruBarcode(PL_StockEntryMaster pobj)
        {
            pobj.Opcode = 45;
            DL_StockEntryMaster.ReturnTable(pobj);
        }
        public static void assignBarcode(PL_StockEntryMaster pobj)
        {
            pobj.Opcode = 46;
            DL_StockEntryMaster.ReturnTable(pobj);
        }
        public static void countBarcode(PL_StockEntryMaster pobj)
        {
            pobj.Opcode = 47;
            DL_StockEntryMaster.ReturnTable(pobj);
        }
        public static void GetProductDetails(PL_StockEntryMaster pobj)
        {
            pobj.Opcode = 48;
            DL_StockEntryMaster.ReturnTable(pobj);
        }
        public static void updateBasePrice(PL_StockEntryMaster pobj)
        {
            pobj.Opcode = 23;
            DL_StockEntryMaster.ReturnTable(pobj);
        }
    }
}
