﻿using DllUtility;
using System;
using System.Data;
using System.Data.SqlClient;


namespace DLLCustomerLocation
{
    public class PL_CustomerLocation : Utility
    {
        public int SalesPerson { get; set; }
        public string Status { get; set; }
    }

    public class DL_CustomerLocation
    {
        public static void execute(PL_CustomerLocation pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("procGetCustomerLocation", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlCmd.CommandTimeout = 1000;
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_CustomerLocation
    {
        public static void bindCustomerDetails(PL_CustomerLocation obj)
        {
            obj.Opcode = 41;
            DL_CustomerLocation.execute(obj);
        }
        public static void bindSalesPerson(PL_CustomerLocation obj)
        {
            obj.Opcode = 42;
            DL_CustomerLocation.execute(obj);
        }
    }
}
