﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllPrintOrderMaster
{
    public class PL_PrintOrderMaster : Utility
    {
        public int OrderAutoId { get; set; }
        public string BulkOrderAutoId { get; set; }
        public int LoginEmpType { get; set; }
        public int EmpAutoId { get; set; }

    }

    public class DL_PrintOrderMaster
    {
        public static void ReturnTable(PL_PrintOrderMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPrintOrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@BulkOrderAutoId", pobj.BulkOrderAutoId);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_PrintOrderMaster
    {
        public static void PrintOrder(PL_PrintOrderMaster pobj)
        {
            pobj.Opcode = 41;
            DL_PrintOrderMaster.ReturnTable(pobj);
        }
        public static void GetOrderPrint(PL_PrintOrderMaster pobj)
        {
            pobj.Opcode = 42;
            DL_PrintOrderMaster.ReturnTable(pobj);
        }
        public static void GetPackingOrderPrint(PL_PrintOrderMaster pobj)
        {
            pobj.Opcode = 43;
            DL_PrintOrderMaster.ReturnTable(pobj);
        }
        public static void getPrintLabel(PL_PrintOrderMaster pobj)
        {
            pobj.Opcode = 44;
            DL_PrintOrderMaster.ReturnTable(pobj);
        }
        public static void getBulkOrderData(PL_PrintOrderMaster pobj)
        {
            pobj.Opcode = 45;
            DL_PrintOrderMaster.ReturnTable(pobj);
        }
        public static void getBulkOrderData1(PL_PrintOrderMaster pobj)
        {
            pobj.Opcode = 46;
            DL_PrintOrderMaster.ReturnTable(pobj);
        }
        public static void PrintOrderItem(PL_PrintOrderMaster pobj)
        {
            pobj.Opcode = 47;
            DL_PrintOrderMaster.ReturnTable(pobj);
        }
        public static void PrintOrderItemTemplate2(PL_PrintOrderMaster pobj)
        {
            pobj.Opcode = 48;
            DL_PrintOrderMaster.ReturnTable(pobj);
        }
        public static void GetOrderPrint_OrderPrintCC(PL_PrintOrderMaster pobj)
        {
            pobj.Opcode = 49;
            DL_PrintOrderMaster.ReturnTable(pobj);
        }
        public static void GetOrderPrintNew(PL_PrintOrderMaster pobj)
        {
            pobj.Opcode = 50;
            DL_PrintOrderMaster.ReturnTable(pobj);
        }
        public static void PrintBulkOrderItemTemplate2(PL_PrintOrderMaster pobj)
        {
            pobj.Opcode = 51;
            DL_PrintOrderMaster.ReturnTable(pobj);
        }
        public static void PrintOrderTemplate1(PL_PrintOrderMaster pobj)
        {
            pobj.Opcode = 52;
            DL_PrintOrderMaster.ReturnTable(pobj);
        }
    }
}
