﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DllUtility;

/// <summary>
/// Summary description for DLLRouteMaster
/// </summary>
namespace DLLRouteMaster
{
    public class PL_RouteMaster : Utility
	{
        public int UserAutoId { get; set; }
        public int AutoId { get; set; }
        public string RouteName { get; set; }
        public int SelesPersonId { get; set; }
        public int CustomerAutoId { get; set; }
        public int Status { get; set; }
	}

    public class DL_RouteMaster
	{
        public static void ReturnTable(PL_RouteMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcRouteMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);

                sqlCmd.Parameters.AddWithValue("@UserAutoId", pobj.UserAutoId);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@RouteName", pobj.RouteName);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SelesPersonId);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);                             

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
	}

    public class BL_RouteMaster
	{
		public static void GetSalesPerson(PL_RouteMaster pobj)
        {
            pobj.Opcode = 41;
            DL_RouteMaster.ReturnTable(pobj);
        }
        public static void GetRouteList(PL_RouteMaster pobj)
        {
            pobj.Opcode = 42;
            DL_RouteMaster.ReturnTable(pobj);
        }
        public static void EditRouteMaster(PL_RouteMaster pobj)
        {
            pobj.Opcode = 43;
            DL_RouteMaster.ReturnTable(pobj);
        }
        public static void GetCustomerBySalesPerson(PL_RouteMaster pobj)
        {
            pobj.Opcode = 44;
            DL_RouteMaster.ReturnTable(pobj);
        }
        public static void InsertRoute(PL_RouteMaster pobj)
        {
            pobj.Opcode = 11;
            DL_RouteMaster.ReturnTable(pobj);
        }
        public static void UpdateRoute(PL_RouteMaster pobj)
        {
            pobj.Opcode = 21;
            DL_RouteMaster.ReturnTable(pobj);
        }
        public static void InsertCheck(PL_RouteMaster pobj)
        {
            pobj.Opcode = 22;
            DL_RouteMaster.ReturnTable(pobj);
        }
        public static void DeleteCheck(PL_RouteMaster pobj)
        {
            pobj.Opcode = 31;
            DL_RouteMaster.ReturnTable(pobj);
        }
        public static void DeleteRoute(PL_RouteMaster pobj)
        {
            pobj.Opcode = 23;
            DL_RouteMaster.ReturnTable(pobj);
        }
    }
}