﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;


namespace DLLBarCodeITemWiseReportMaster
{
    public class PL_BarCodeITemWiseReportMaster: Utility
    {
        public string BarCode { get; set; }
    }
    public class DL_BarCodeITemWiseReportMaster
    {
        public static void ReturnTable(PL_BarCodeITemWiseReportMaster pobj)
        {
            try
            { 
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcBarCodeITemWiseReport", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandTimeout = 1000;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@Barcode",pobj.BarCode);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch(Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_BarCodeITemWiseReportMaster
    {
        public static void BindBarCodeITemWiseReportMaster(PL_BarCodeITemWiseReportMaster pobj)
        {
            pobj.Opcode = 21;
            DL_BarCodeITemWiseReportMaster.ReturnTable(pobj);
        }
       
    }
	
}