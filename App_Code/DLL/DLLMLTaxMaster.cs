﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLMLTaxMaster
/// </summary>
namespace DLLMLTaxMaster
{
    public class PL_MLTaxMaster : Utility
    {
        public int TaxAutoId { get; set; }
        public string TaxId { get; set; }
        public string TaxName { get; set; }
        public decimal Rate { get; set; }
        public int Status { get; set; }
        public int State { get; set; }
        public string PrintLabel { get; set; }
    }
     public class DL_MLTaxMaster
    {
        public static void ReturnTable(PL_MLTaxMaster pobj)
        {
            try
            { 
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcMLTaxMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@TaxAutoId", pobj.TaxAutoId);
                sqlCmd.Parameters.AddWithValue("@State", pobj.State);
                sqlCmd.Parameters.AddWithValue("@TaxId", pobj.TaxId);
                sqlCmd.Parameters.AddWithValue("@TaxName", pobj.TaxName);
                sqlCmd.Parameters.AddWithValue("@Rate", pobj.Rate);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@PrintLabel", pobj.PrintLabel);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch(Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
     public class BL_MLTaxMaster
     {
         public static void insert(PL_MLTaxMaster pobj)
         {
             pobj.Opcode = 11;
             DL_MLTaxMaster.ReturnTable(pobj);
         }
         public static void update(PL_MLTaxMaster pobj)
         {
             pobj.Opcode = 21;
             DL_MLTaxMaster.ReturnTable(pobj);
         }
         public static void delete(PL_MLTaxMaster pobj)
         {
             pobj.Opcode = 31;
             DL_MLTaxMaster.ReturnTable(pobj);
         }
         public static void select(PL_MLTaxMaster pobj)
         {
             pobj.Opcode = 61;
             DL_MLTaxMaster.ReturnTable(pobj);
         }
         public static void editTax(PL_MLTaxMaster pobj)
         {
             pobj.Opcode = 42;
             DL_MLTaxMaster.ReturnTable(pobj);
         }
         public static void BindState(PL_MLTaxMaster pobj)
         {
             pobj.Opcode = 43;
             DL_MLTaxMaster.ReturnTable(pobj);
         }
         public static void BindTaxDetails(PL_MLTaxMaster pobj)
         {
             pobj.Opcode = 44;
             DL_MLTaxMaster.ReturnTable(pobj);
         }
         public static void updateWeightTax(PL_MLTaxMaster pobj)
         {
             pobj.Opcode = 45;
             DL_MLTaxMaster.ReturnTable(pobj);
         }
     }
}