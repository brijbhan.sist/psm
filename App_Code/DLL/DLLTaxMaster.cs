﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllTaxMaster
{
    public class PL_TaxMaster : Utility
    {
        public int TaxAutoId { get; set; }
        public string TaxId { get; set; }
        public string TaxName { get; set; }
        public decimal Value { get; set; }
        public int Status { get; set; }
        public int State { get; set; }
        public string PrintLabel { get; set; }
    }
    public class DL_TaxMaster
    {
        public static void ReturnTable(PL_TaxMaster pobj)
        {
            try
            { 
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcTaxMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@TaxAutoId", pobj.TaxAutoId);
                sqlCmd.Parameters.AddWithValue("@State", pobj.State);
                sqlCmd.Parameters.AddWithValue("@TaxId", pobj.TaxId);
                sqlCmd.Parameters.AddWithValue("@TaxName", pobj.TaxName);
                sqlCmd.Parameters.AddWithValue("@Value", pobj.Value);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@PrintLabel", pobj.PrintLabel);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch(Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_TaxMaster
    {
        public static void insert(PL_TaxMaster pobj)
        {
            pobj.Opcode = 11;
            DL_TaxMaster.ReturnTable(pobj);
        }
        public static void update(PL_TaxMaster pobj)
        {
            pobj.Opcode = 21;
            DL_TaxMaster.ReturnTable(pobj);
        }
        public static void delete(PL_TaxMaster pobj)
        {
            pobj.Opcode = 31;
            DL_TaxMaster.ReturnTable(pobj);
        }  
        public static void select(PL_TaxMaster pobj)
        {
            pobj.Opcode = 41;
            DL_TaxMaster.ReturnTable(pobj);
        }
        public static void editTax(PL_TaxMaster pobj)
        {
            pobj.Opcode = 42;
            DL_TaxMaster.ReturnTable(pobj);
        }
        public static void BindState(PL_TaxMaster pobj)
        {
            pobj.Opcode = 43;
            DL_TaxMaster.ReturnTable(pobj);
        }
    }
}
