﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DllUtility;
namespace  DLLDraftCustomerMaster
{
    public class PL_DraftCustomerMaster:Utility
    {
        public string XmlContactPerson { get; set; }
        public int PriceLevelAutoId { get; set; }
        public int Terms { get; set; }
        public string TaxId { get; set; }
        public int EmpAutoId { get; set; }
        public int CustomerAutoId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string BusinessName { get; set; }
        public string OPTLicence { get; set; }
        public int CustType { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string latitude1 { get; set; }
        public string longitude1 { get; set; }
        public int BillAddAutoId { get; set; }
        public int ShipAddAutoId { get; set; }
        public int CityAutoId { get; set; }
        public int CityAutoId2 { get; set; }
        public string State1 { get; set; }
        public string State2 { get; set; }
        public string StoreOpenTime { get; set; }
        public string StoreCloseTime { get; set; }

        public string BillAdd { get; set; }
        public string BillAdd2 { get; set; }
        public string City1 { get; set; }
        public string Zipcode1 { get; set; }
        public int ZipAutoId { get; set; }
        public string ShipAdd { get; set; }
        public string ShipAdd2 { get; set; }

        public string City2 { get; set; }
        public string Zipcode2 { get; set; }
        public int ZipAutoId2 { get; set; }
        public int Status { get; set; }
        public int GenStatus { get; set; }
        public int SalesPersonAutoId { get; set; }
        public string ZipCode { get; set; }
        public string CustomerRemark { get; set; }

    }
    public class DL_DraftCustomerMaster
    {
        public static void ReturnTable(PL_DraftCustomerMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcDraftCustomerMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@XmlContactPerson", pobj.XmlContactPerson);
                sqlCmd.Parameters.Add("@CustomerAutoId", SqlDbType.Int).Value = pobj.CustomerAutoId;
                sqlCmd.Parameters.AddWithValue("@CityAutoId", pobj.CityAutoId);
                sqlCmd.Parameters.AddWithValue("@GenStatus", pobj.GenStatus);
                sqlCmd.Parameters.AddWithValue("@CityAutoId2", pobj.CityAutoId2);
                sqlCmd.Parameters.AddWithValue("@ZipAutoId", pobj.ZipAutoId);
                sqlCmd.Parameters.AddWithValue("@ZipAutoId2", pobj.ZipAutoId2);
                sqlCmd.Parameters.AddWithValue("@CustomerName", pobj.CustomerName);
                sqlCmd.Parameters.AddWithValue("@BusinessName", pobj.BusinessName);
                sqlCmd.Parameters.AddWithValue("@OPTLicence", pobj.OPTLicence);
                sqlCmd.Parameters.AddWithValue("@CustomerType", pobj.CustType);
                sqlCmd.Parameters.AddWithValue("@Latitude", pobj.latitude);
                sqlCmd.Parameters.AddWithValue("@Longitude", pobj.longitude);
                sqlCmd.Parameters.AddWithValue("@Zipcode", pobj.ZipCode);
                sqlCmd.Parameters.AddWithValue("@Latitude1", pobj.latitude1);
                sqlCmd.Parameters.AddWithValue("@Longitude1", pobj.longitude1);
                sqlCmd.Parameters.AddWithValue("@StoreOpenTime", pobj.StoreOpenTime);
                sqlCmd.Parameters.AddWithValue("@StoreCloseTime", pobj.StoreCloseTime);
                sqlCmd.Parameters.AddWithValue("@BillAddAutoId", pobj.BillAddAutoId);
                sqlCmd.Parameters.AddWithValue("@BillAdd", pobj.BillAdd);
                sqlCmd.Parameters.AddWithValue("@State1", pobj.State1);
                sqlCmd.Parameters.AddWithValue("@City1", pobj.City1);
                sqlCmd.Parameters.AddWithValue("@Zipcode1", pobj.Zipcode1);
                sqlCmd.Parameters.AddWithValue("@ShipAddAutoId", pobj.ShipAddAutoId);
                sqlCmd.Parameters.AddWithValue("@ShipAdd", pobj.ShipAdd);
                sqlCmd.Parameters.AddWithValue("@State2", pobj.State2);
                sqlCmd.Parameters.AddWithValue("@City2", pobj.City2);
                sqlCmd.Parameters.AddWithValue("@Zipcode2", pobj.Zipcode2);
                sqlCmd.Parameters.AddWithValue("@Terms", pobj.Terms);
                sqlCmd.Parameters.AddWithValue("@TaxId", pobj.TaxId);

                if (pobj.PriceLevelAutoId != 0)
                {
                    sqlCmd.Parameters.AddWithValue("@priceLevelId", pobj.PriceLevelAutoId);
                }

                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);

                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                if (pobj.SalesPersonAutoId != 0)
                {
                    sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                }
               
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.AddWithValue("@BillAdd2", pobj.BillAdd2);
                sqlCmd.Parameters.AddWithValue("@ShipAdd2", pobj.ShipAdd2);
                sqlCmd.Parameters.AddWithValue("@CustomerRemark", pobj.CustomerRemark);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }
    }
    public class BL_DraftCustomerMaster
    {
        public static void insert(PL_DraftCustomerMaster pobj)
        {
            pobj.Opcode = 11;
            DL_DraftCustomerMaster.ReturnTable(pobj);
        }
        public static void update(PL_DraftCustomerMaster pobj)
        {
            
            if(pobj.GenStatus==12)
            {
                pobj.Opcode = 12;
            }
            else
            {
                pobj.Opcode = 21;
            }
            DL_DraftCustomerMaster.ReturnTable(pobj);
        }

        public static void delete(PL_DraftCustomerMaster pobj)
        {
            pobj.Opcode = 31;
            DL_DraftCustomerMaster.ReturnTable(pobj);
        }
        public static void Select(PL_DraftCustomerMaster pobj)
        {
            pobj.Opcode = 41;
            DL_DraftCustomerMaster.ReturnTable(pobj);
        }
        public static void Edit(PL_DraftCustomerMaster pobj)
        {
            pobj.Opcode = 42;
            DL_DraftCustomerMaster.ReturnTable(pobj);
        }
        public static void EditContactPerson(PL_DraftCustomerMaster pobj)
        {
            pobj.Opcode = 46;
            DL_DraftCustomerMaster.ReturnTable(pobj);
        }
        public static void bindDropDownsList(PL_DraftCustomerMaster pobj)
        {
            pobj.Opcode = 43;
            DL_DraftCustomerMaster.ReturnTable(pobj);
        }
        public static void checkZipcode(PL_DraftCustomerMaster pobj)
        {
            pobj.Opcode = 44;
            DL_DraftCustomerMaster.ReturnTable(pobj);
        }
        public static void bindContactPersonType(PL_DraftCustomerMaster pobj)
        {
            pobj.Opcode = 45;
            DL_DraftCustomerMaster.ReturnTable(pobj);
        }
        public static void bindPriceLevel(PL_DraftCustomerMaster pobj)
        {
            pobj.Opcode = 47;
            DL_DraftCustomerMaster.ReturnTable(pobj);
        }
    }
}