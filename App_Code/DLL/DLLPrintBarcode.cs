﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DllUtility;
/// <summary>
/// Summary description for DLLPrintBarcode
/// </summary>
namespace DLLPrintBarcode
{
    public class PL_PrintBarcode:Utility
    {
        public int CategoryAutoId { get; set; }
        public int SubCategoryAutoId { get; set; }
        public int UnitType { get; set; }
        public int ProductAutoId { get; set; }
        public string Barcode { get; set; }
        public int PrintOption { get; set; }
    }
    public class DL_PrintBarcode
    {
        public static void ReturnTable(PL_PrintBarcode pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcBarcodePrint", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@SubCategoryAutoId", pobj.SubCategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitType", pobj.UnitType);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);
                sqlCmd.Parameters.AddWithValue("@PrintOption", pobj.PrintOption); 

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_PrintBarcode
    {
        public static void BindCategory(PL_PrintBarcode pobj)
        {
            pobj.Opcode = 401;
            DL_PrintBarcode.ReturnTable(pobj);
        }
        public static void BindSubCategory(PL_PrintBarcode pobj)
        {
            pobj.Opcode = 402;
            DL_PrintBarcode.ReturnTable(pobj);
        }
        public static void BindProduct(PL_PrintBarcode pobj)
        {
            pobj.Opcode = 403;
            DL_PrintBarcode.ReturnTable(pobj);
        }
        public static void BindUnitType(PL_PrintBarcode pobj)
        {
            pobj.Opcode = 404;
            DL_PrintBarcode.ReturnTable(pobj);
        }
        public static void getbarcodeDetails(PL_PrintBarcode pobj)
        {
            pobj.Opcode = 405;
            DL_PrintBarcode.ReturnTable(pobj);
        } 
        public static void AddinList(PL_PrintBarcode pobj)
        {
            pobj.Opcode = 406;
            DL_PrintBarcode.ReturnTable(pobj);
        }
    }
}