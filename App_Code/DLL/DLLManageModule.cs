﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DLLManageModule
{
    public class PL_ManageModule : Utility
    {
        public string Module { get; set; }
        public string ModuleId { get; set; }
        public string iconClass { get; set; }
        public int HasModule { get; set; }
        public int ParentModule { get; set; }
        public int Status { get; set; }
        public int EmpAutoId { get; set; }
        public int AutoId { get; set; }
        public int SequenceNo { get; set; }
        public int MenuType { get; set; }
    }
    public class DL_ManageModule
    {
        public static void ReturnTable(PL_ManageModule pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcManageModule", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@OpCode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@Module", pobj.Module);
                sqlCmd.Parameters.AddWithValue("@iconClass", pobj.iconClass);
                sqlCmd.Parameters.AddWithValue("@ModuleId", pobj.ModuleId);
                sqlCmd.Parameters.AddWithValue("@HasModule", pobj.HasModule);
                sqlCmd.Parameters.AddWithValue("@ParentModule", pobj.ParentModule);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@SequenceNo", pobj.SequenceNo);
                sqlCmd.Parameters.AddWithValue("@MenuType", pobj.MenuType);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();

            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }

    }
    public class BL_ManageModule
    {

        public static void insert(PL_ManageModule pobj)
        {
            pobj.Opcode = 11;
            DL_ManageModule.ReturnTable(pobj);
        }
        public static void update(PL_ManageModule pobj)
        {
            pobj.Opcode = 21;
            DL_ManageModule.ReturnTable(pobj);
        }
        public static void select(PL_ManageModule pobj)
        {
            pobj.Opcode = 41;
            DL_ManageModule.ReturnTable(pobj);
        }
        public static void getParentModule(PL_ManageModule pobj)
        {
            pobj.Opcode = 42;
            DL_ManageModule.ReturnTable(pobj);
        }
        public static void deleteModule(PL_ManageModule pobj)
        {
            pobj.Opcode = 31;
            DL_ManageModule.ReturnTable(pobj);
        }
 
    }

}