﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data;
using System.Data.SqlClient;

namespace DllPriceLevelList
{
    public class PL_PriceLevelList : Utility
    {        
        public string PriceLevelId { get; set; }
        public int PriceLevelAutoId { get; set; }
        public string PriceLevelName { get; set; }
        public int Status { get; set; }        
        public int CustomerType { get; set; }        
    }

    public class DL_PriceLevel
    {
        public static void ReturnTable(PL_PriceLevelList pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPriceLevelList", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@PriceLevelId", pobj.PriceLevelId);
                sqlCmd.Parameters.AddWithValue("@PriceLevelAutoId", pobj.PriceLevelAutoId);
                sqlCmd.Parameters.AddWithValue("@PriceLevelName", pobj.PriceLevelName);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@CustomerType", pobj.CustomerType);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_PriceLevelList
    {
        public static void BindPriceLevel(PL_PriceLevelList pobj)
        {
            pobj.Opcode = 41;
            DL_PriceLevel.ReturnTable(pobj);
        }
        public static void BindStatus(PL_PriceLevelList pobj)
        {
            pobj.Opcode = 42;
            DL_PriceLevel.ReturnTable(pobj);
        }
    }
}
