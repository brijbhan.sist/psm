﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DLLTaskStatusMaster
/// </summary>
namespace DLLToDo
{
    public class PL_ToDo: Utility
    {
        public int AutoId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Starred { get; set; }
        public int Important { get; set; }
        public int UserAutoId { get; set; }
        public int IsCompleted { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
    public class DL_ToDo
    {
        public static void ReturnTable(PL_ToDo pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcToDoMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@Title", pobj.Title);
                sqlCmd.Parameters.AddWithValue("@Description", pobj.Description);
                sqlCmd.Parameters.AddWithValue("@Starred", pobj.Starred);
                sqlCmd.Parameters.AddWithValue("@Important", pobj.Important);
                sqlCmd.Parameters.AddWithValue("@UserAutoId", pobj.UserAutoId);
                sqlCmd.Parameters.AddWithValue("@IsCompleted", pobj.IsCompleted);
                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                sqlCmd.Parameters.AddWithValue("@CreatedBy", pobj.Who);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_ToDo
    {
        public static void insert(PL_ToDo pobj)
        {
            pobj.Opcode = 11;
            DL_ToDo.ReturnTable(pobj);
        }
        public static void Update(PL_ToDo pobj)
        {
            pobj.Opcode = 21;
            DL_ToDo.ReturnTable(pobj);
        }
        public static void MoveTrash(PL_ToDo pobj)
        {
            pobj.Opcode = 22;
            DL_ToDo.ReturnTable(pobj);
        }
        public static void RevertToDo(PL_ToDo pobj)
        {
            pobj.Opcode = 23;
            DL_ToDo.ReturnTable(pobj);
        }
        public static void MarkAsCompleted(PL_ToDo pobj)
        {
            pobj.Opcode = 24;
            DL_ToDo.ReturnTable(pobj);
        }
        public static void Delete(PL_ToDo pobj)
        {
            pobj.Opcode = 31;
            DL_ToDo.ReturnTable(pobj);
        }

        public static void select(PL_ToDo pobj)
        {
            pobj.Opcode = 41;
            DL_ToDo.ReturnTable(pobj);
        }

        public static void Edit(PL_ToDo pobj)
        {
            pobj.Opcode = 42;
            DL_ToDo.ReturnTable(pobj);
        }

        public static void BindToDoTrashList(PL_ToDo pobj)
        {
            pobj.Opcode = 43;
            DL_ToDo.ReturnTable(pobj);
        } 
        public static void SelectStatus(PL_ToDo pobj)
        {
            pobj.Opcode = 44;
            DL_ToDo.ReturnTable(pobj);
        }
    }
}