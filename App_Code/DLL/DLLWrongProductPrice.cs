﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DLLWrongProductPrice
/// </summary>
namespace DLLWrongProductPrice {
    public class PL_WrongProductList : Utility
    {
        public string ProductName { get; set; }
        public string Unit { get; set; }
    }

        public class DL_WrongPrice
        {
            public static void ReturnTable(PL_WrongProductList pobj)
            {
                try
                {
                    Config connect = new Config();
                    SqlCommand sqlCmd = new SqlCommand("ProcWrongProductPrice", connect.con);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@OpCode", pobj.Opcode);
                    sqlCmd.Parameters.AddWithValue("@ProductName", pobj.ProductName);
                    sqlCmd.Parameters.AddWithValue("@Unit", pobj.Unit);

                    sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                    sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                    sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                    sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                    sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                    sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                    SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                    pobj.Ds = new DataSet();
                    sqlAdp.Fill(pobj.Ds);

                    pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                    pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();

                }
                catch (Exception ex)
                {
                    pobj.isException = true;
                    pobj.exceptionMessage = ex.Message;
                }
            }
        }
    public class BL_WrongProductPrice
    {
        public static void select(PL_WrongProductList pobj)
        {
            pobj.Opcode = 41;
            DL_WrongPrice.ReturnTable(pobj);
        }
    }
}