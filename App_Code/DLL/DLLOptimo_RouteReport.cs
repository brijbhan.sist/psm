﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DllUtility;

/// <summary>
/// Summary description for DLLOptimo_RouteReport
/// </summary>
namespace DLLOptimo_RouteReport
{
    public class PL_Optimo_RouteReport : Utility
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int ReportAutoId { get; set; }
        public int @PlanningId { get; set; }
        public int AutoId { get; set; }
    }
    public class DL_Optimo_RouteReport
    {
        public static void Returntable(PL_Optimo_RouteReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcOptimo_RouteReport", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);

                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }

                sqlCmd.Parameters.AddWithValue("@ReportAutoId", pobj.ReportAutoId);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@PlanningId", pobj.PlanningId);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_Optimo_RouteReport
    {
        public static void GetRouteReport(PL_Optimo_RouteReport pobj)
        {
            pobj.Opcode = 41;
            DL_Optimo_RouteReport.Returntable(pobj);
        }
        public static void GetPlanningDetails(PL_Optimo_RouteReport pobj)
        {
            pobj.Opcode = 42;
            DL_Optimo_RouteReport.Returntable(pobj);
        }
        public static void GetDriverDetails(PL_Optimo_RouteReport pobj)
        {
            pobj.Opcode = 43;
            DL_Optimo_RouteReport.Returntable(pobj);
        }
        public static void GetPrintDriverDetails(PL_Optimo_RouteReport pobj)
        {
            pobj.Opcode = 44;
            DL_Optimo_RouteReport.Returntable(pobj);
        } 

    }
}