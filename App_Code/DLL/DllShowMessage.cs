﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllShowMessage
{
    public class PL_ShowMessage : Utility
    {
        public int EmpAutoId { get; set; }
        public int EmpType { get; set; }
        public int AutoId { get; set; }
    }

    public class DL_ShowMessage
    {
        public static void ReturnTable(PL_ShowMessage pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcShowMessage", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@EmpType", pobj.EmpType);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_ShowMessage
    {
        public static void ShowMessage(PL_ShowMessage pobj)
        {
            pobj.Opcode = 41;
            DL_ShowMessage.ReturnTable(pobj);
        }
        public static void readmessage(PL_ShowMessage pobj)
        {
            pobj.Opcode = 11;
            DL_ShowMessage.ReturnTable(pobj);
        }
    }
}
