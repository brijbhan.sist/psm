﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllSubTaxMaster
{
    public class PL_SubTaxMaster : Utility
    {
        public int AutoId { get; set; }
        public string TaxName { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
        public int Status { get; set; }
        public int UserAutoId { get; set; }
    }
    public class DL_SubTaxMaster
    {
        public static void ReturnTable(PL_SubTaxMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcSubTaxMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@UserAutoId", pobj.UserAutoId);
                sqlCmd.Parameters.AddWithValue("@TaxName", pobj.TaxName);
                sqlCmd.Parameters.AddWithValue("@Value", pobj.Value);
                sqlCmd.Parameters.AddWithValue("@Description", pobj.Description);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_SubTaxMaster
    {
        public static void insert(PL_SubTaxMaster pobj)
        {
            pobj.Opcode = 11;
            DL_SubTaxMaster.ReturnTable(pobj);
        }
        public static void update(PL_SubTaxMaster pobj)
        {
            pobj.Opcode = 21;
            DL_SubTaxMaster.ReturnTable(pobj);
        }
        public static void delete(PL_SubTaxMaster pobj)
        {
            pobj.Opcode = 31;
            DL_SubTaxMaster.ReturnTable(pobj);
        }
        public static void select(PL_SubTaxMaster pobj)
        {
            pobj.Opcode = 41;
            DL_SubTaxMaster.ReturnTable(pobj);
        }
        public static void editTax(PL_SubTaxMaster pobj)
        {
            pobj.Opcode = 42;
            DL_SubTaxMaster.ReturnTable(pobj);
        }

    }
}
