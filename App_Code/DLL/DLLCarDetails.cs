﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLCarDetails
/// </summary>
/// 
namespace DLLCarDetails
{
    public class PL_CarDetails : Utility
    {
        public int CarAutoId { get; set; }
        public string CarName { get; set; }
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string VINNumber { get; set; }
        public string TAGNumber { get; set; }
        public DateTime InspectionExpDate { get; set; }
        public string StartMilage { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
    }
    public class DL_CarDetails
    {
        public static void Returntable(PL_CarDetails pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcCarDetails", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CarAutoId", pobj.CarAutoId);
                sqlCmd.Parameters.AddWithValue("@CarName", pobj.CarName);
                sqlCmd.Parameters.AddWithValue("@YearName", pobj.Year);
                sqlCmd.Parameters.AddWithValue("@Make", pobj.Make);
                sqlCmd.Parameters.AddWithValue("@Model", pobj.Model);
                sqlCmd.Parameters.AddWithValue("@VINNumber", pobj.VINNumber);
                sqlCmd.Parameters.AddWithValue("@TAGNumber", pobj.TAGNumber);
                if (pobj.InspectionExpDate > DateTime.MinValue && pobj.InspectionExpDate < DateTime.MaxValue)
                    sqlCmd.Parameters.AddWithValue("@InspectionExpDate", pobj.InspectionExpDate);                    
                sqlCmd.Parameters.AddWithValue("@StartMilage", pobj.StartMilage);
                sqlCmd.Parameters.AddWithValue("@Description", pobj.Description);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_CarDetails
    {
        public static void Insert(PL_CarDetails pobj)
        {
            pobj.Opcode = 11;
            DL_CarDetails.Returntable(pobj);
        }
        public static void update(PL_CarDetails pobj)
        {
            pobj.Opcode = 21;
            DL_CarDetails.Returntable(pobj);
        }
        public static void CarList(PL_CarDetails pobj)
        {
            pobj.Opcode = 41;
            DL_CarDetails.Returntable(pobj);
        }
        public static void EditCar(PL_CarDetails pobj)
        {
            pobj.Opcode = 42;
            DL_CarDetails.Returntable(pobj);
        }
        public static void DeleteCar(PL_CarDetails pobj)
        {
            pobj.Opcode = 43;
            DL_CarDetails.Returntable(pobj);
        }
    }
}