﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DLLproductReturnReport
/// </summary>
namespace DLLproductReturnReport
{
    public class PL_productReturnReport : Utility
    {
        public int CustomerAutoId { get; set; }
        public int SalesPersonAutoId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime ClosedFromDate { get; set; }
        public DateTime ClosedToDate { get; set; }
    }
    public class DL_productReturnReport
    {
        public static void ReturnTable(PL_productReturnReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcProductReturnReport", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);           
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);             
                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                if (pobj.ClosedFromDate != DateTime.MinValue && pobj.ClosedFromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ClosedFromDate", pobj.ClosedFromDate);
                }
                if (pobj.ClosedToDate != DateTime.MinValue && pobj.ClosedToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ClosedToDate", pobj.ClosedToDate);
                }
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);


                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_productReturnReport
    {
        public static void bindDropdownList(PL_productReturnReport pobj)
        {
            pobj.Opcode = 41;
            DL_productReturnReport.ReturnTable(pobj);
        }
        public static void getReport(PL_productReturnReport pobj)
        {
            pobj.Opcode = 42;
            DL_productReturnReport.ReturnTable(pobj);
        }
        public static void BindCustomer(PL_productReturnReport pobj)
        {
            pobj.Opcode = 43;
            DL_productReturnReport.ReturnTable(pobj);
        }
    }
}