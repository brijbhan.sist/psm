﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropertyErrorTicketLog
/// </summary>
public class PropertyErrorTicketLog : PropertyUtilityAbstract
{
    DateTime fromDate, toDate;
    string status, type, ticketID, byEmplyoyee;


    public string Status
    {
        get { return status; }
        set { status = value; }
    }

    public string Type
    {
        get { return type; }
        set { type = value; }
    }

    public string TicketID
    {
        get { return ticketID; }
        set { ticketID = value; }
    }

    public string ByEmplyoyee
    {
        get { return byEmplyoyee; }
        set { byEmplyoyee = value; }
    }
    public DateTime FromDate
    {
        get { if (fromDate == null || fromDate == DateTime.MinValue) { fromDate = Convert.ToDateTime("01/01/2010"); } return fromDate; }
        set { if (fromDate == null || fromDate == DateTime.MinValue) { fromDate = Convert.ToDateTime("01/01/2010"); } fromDate = value; }
    }

    public DateTime ToDate
    {
        get { if (toDate == null || toDate == DateTime.MinValue) { toDate = Convert.ToDateTime("01/01/2010"); } return toDate; }
        set { if (toDate == null || toDate == DateTime.MinValue) { toDate = Convert.ToDateTime("01/01/2010"); } toDate = value; }
    }
}