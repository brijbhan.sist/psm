﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data;
using System.Data.SqlClient;


/// <summary>
/// Summary description for DLLVendorPayMaster
/// </summary>
namespace DLLVendorPayMaster
{
	public class PL_VendorPayMaster : Utility
	{
        public int PayAutoId { get; set; }
        public string PayId { get; set; }
        public int VendorAutoId { get; set; }
        public int EmpAutoId { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal PaymentAmount { get; set; }
        public int PaymentMode { get; set; }
        public int PaymentType { get; set; }
        public string ReferenceId { get; set; }
        public string Remark { get; set; }
        public DataTable TableValue { get; set; }
        public decimal SortAmount { get; set; }
        public decimal TotalCurrencyAmount { get; set; }
        public string PaymentCurrencyXml { get; set; }
        public int CheckAutoId { get; set; }
    }
    public class DL_VendorPayMaster
    {
        public static void ReturnTable(PL_VendorPayMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcVendorPayMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@PayAutoId", pobj.PayAutoId);
                sqlCmd.Parameters.AddWithValue("@PayId", pobj.PayId);
                sqlCmd.Parameters.AddWithValue("@VendorAutoId", pobj.VendorAutoId);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@PaymentAmount", pobj.PaymentAmount);
                sqlCmd.Parameters.AddWithValue("@PaymentMode", pobj.PaymentMode);
                sqlCmd.Parameters.AddWithValue("@PaymentType", pobj.PaymentType);
                sqlCmd.Parameters.AddWithValue("@ReferenceId", pobj.ReferenceId);
                sqlCmd.Parameters.AddWithValue("@Remark", pobj.Remark);
                sqlCmd.Parameters.AddWithValue("@CheckAutoId", pobj.CheckAutoId);

                if (pobj.PaymentDate > DateTime.MinValue && pobj.PaymentDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@PaymentDate", pobj.PaymentDate);
                }
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }

                sqlCmd.Parameters.AddWithValue("@TableValue", pobj.TableValue);
                sqlCmd.Parameters.AddWithValue("@SortAmount", pobj.SortAmount);
                sqlCmd.Parameters.AddWithValue("@TotalCurrencyAmount", pobj.TotalCurrencyAmount);
                sqlCmd.Parameters.AddWithValue("@PaymentCurrencyXml", pobj.PaymentCurrencyXml);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_VendorPayMaster
    {
        public static void InsertPayment(PL_VendorPayMaster pobj)
        {
            pobj.Opcode = 11;
            DL_VendorPayMaster.ReturnTable(pobj);
        }
        public static void UpdatePay(PL_VendorPayMaster pobj)
        {
            pobj.Opcode = 21;
            DL_VendorPayMaster.ReturnTable(pobj);
        }
        public static void editPay(PL_VendorPayMaster pobj)
        {
            pobj.Opcode = 43;
            DL_VendorPayMaster.ReturnTable(pobj);
        }
        public static void binddropdown(PL_VendorPayMaster pobj)
        {
            pobj.Opcode = 44;
            DL_VendorPayMaster.ReturnTable(pobj);
        }
        public static void BidnDdlList(PL_VendorPayMaster pobj)
        {
            pobj.Opcode = 41;
            DL_VendorPayMaster.ReturnTable(pobj);
        }
        public static void SelectList(PL_VendorPayMaster pobj)
        {
            pobj.Opcode = 42;
            DL_VendorPayMaster.ReturnTable(pobj);
        }
        public static void deletePay(PL_VendorPayMaster pobj)
        {
            pobj.Opcode = 31;
            DL_VendorPayMaster.ReturnTable(pobj);
        }
        public static void BindPOSList(PL_VendorPayMaster pobj)
        {
            pobj.Opcode = 32;
            DL_VendorPayMaster.ReturnTable(pobj);
        }
        public static void bindCheckNo(PL_VendorPayMaster pobj)
        {
            pobj.Opcode = 33;
            DL_VendorPayMaster.ReturnTable(pobj);
        }
        public static void UpdatePaymentsettle(PL_VendorPayMaster pobj)
        {
            pobj.Opcode = 34;
            DL_VendorPayMaster.ReturnTable(pobj);
        }
    }
}