﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DllUtility;

/// <summary>
/// Summary description for DLLReportPaymentDailyReport
/// </summary>
namespace DLLYearlyReport
{
	public class PL_YearlyReport : Utility
	{
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int Customer { get; set; }
        public String SelectYear { get; set; }

    }
    public class DL_YearlyReport
    {
        public static void Returntable(PL_YearlyReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcYearlyReport", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                    sqlCmd.Parameters.AddWithValue("@SelectYear", pobj.SelectYear);
                sqlCmd.Parameters.AddWithValue("@Customer", pobj.Customer);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_YearlyReport
    {
        public static void BindCustomer(PL_YearlyReport pobj)
        {
            pobj.Opcode = 40;
            DL_YearlyReport.Returntable(pobj);
        }
        public static void GetYearlyReport(PL_YearlyReport pobj)
        {
            pobj.Opcode = 41;
            DL_YearlyReport.Returntable(pobj);
        }
	}
}