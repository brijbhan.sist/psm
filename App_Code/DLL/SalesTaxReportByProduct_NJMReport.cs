﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for SalesTaxReportByProduct_NJMReport
/// </summary>
namespace SalesTaxReportByProduct_NJMReport
{
    public class PL_DetailProductSalesRepor : Utility
    {
        public int OrderStatus { get; set; }
        public int SalesPerson { get; set; }
        public int CategoryAutoId { get; set; }
        public int SubCategoryId { get; set; }
        public int EmpAutoId { get; set; }
        public int ProductAutoId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string BrandId { get; set; }
        public int CustomerAutoId { get; set; }
        public int SortBySoldQty { get; set; }
    }
    public class DL_DetailProductSalesRepor
    {
        public static void ReturnData(PL_DetailProductSalesRepor pobj)
        {
            try

            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("ProcSalesReportByProduct_NJMReport", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                cmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                cmd.Parameters.AddWithValue("@SubCategoryId", pobj.SubCategoryId);
                cmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                cmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                cmd.Parameters.AddWithValue("@BrandAutoIdSring", pobj.BrandId);
                cmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                cmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                cmd.Parameters.AddWithValue("@SortBySoldQty", pobj.SortBySoldQty);
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                cmd.CommandTimeout = 1000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }

    public class BL_DetailProductSalesRepor
    {
        public static void BindProductByBrand(PL_DetailProductSalesRepor pobj)
        {
            pobj.Opcode = 41;
            DL_DetailProductSalesRepor.ReturnData(pobj);
        }
        public static void BindProductSaleReport(PL_DetailProductSalesRepor pobj)
        {
            pobj.Opcode = 42;
            DL_DetailProductSalesRepor.ReturnData(pobj);
        }
        public static void BindProductCategory(PL_DetailProductSalesRepor pobj)
        {
            pobj.Opcode = 43;
            DL_DetailProductSalesRepor.ReturnData(pobj);
        }
        public static void BindProductSubCategory(PL_DetailProductSalesRepor pobj)
        {
            pobj.Opcode = 44;
            DL_DetailProductSalesRepor.ReturnData(pobj);
        }
        public static void BindCustomer(PL_DetailProductSalesRepor pobj)
        {
            pobj.Opcode = 45;
            DL_DetailProductSalesRepor.ReturnData(pobj);
        }
        public static void CustomerDropdown(PL_DetailProductSalesRepor pobj)
        {
            pobj.Opcode = 46;
            DL_DetailProductSalesRepor.ReturnData(pobj);
        }
    }
}