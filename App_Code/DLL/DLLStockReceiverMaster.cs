﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllStockReceiverMaster
{
    public class PL_StockReceiverMaster : Utility
    {
        public int UserAutoId { get; set; }
        public int BillAutoId { get; set; }
        public int VendorAutoId { get; set; }
        public string BillNo { get; set; }
        public DateTime BillDate { get; set; }
        public string Remarks { get; set; }
        public int ItemAutoId { get; set; }
        public int BillItemsAutoId { get; set; }
        public int ProductAutoId { get; set; }
        public int DraftAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int Quantity { get; set; }
        public int TotalPieces { get; set; }
        public int QtyPerUnit { get; set; }
        public int Status { get; set; }
        public decimal BasePrice { get; set; }
        public decimal RetailMIN { get; set; }
        public decimal CostPrice { get; set; }
        public decimal SRP { get; set; }
        public decimal WholesaleMinPrice { get; set; }
        public DateTime FromBillDate { get; set; }
        public DateTime ToBillDate { get; set; }
        public DataTable TableValue { get; set; }
        public DataTable TblUpdateItems { get; set; }

        public string Barcode { get; set; }
    }

    public class DL_StockReceiverMaster
    {
        public static void ReturnTable(PL_StockReceiverMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcStockReceiverMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@UserAutoId", pobj.UserAutoId);
                sqlCmd.Parameters.AddWithValue("@BillAutoId", pobj.BillAutoId);
                sqlCmd.Parameters.AddWithValue("@ItemAutoId", pobj.ItemAutoId);
                sqlCmd.Parameters.AddWithValue("@BasePrice", pobj.BasePrice);
                sqlCmd.Parameters.AddWithValue("@RetailMIN", pobj.RetailMIN);
                sqlCmd.Parameters.AddWithValue("@CostPrice", pobj.CostPrice);
                sqlCmd.Parameters.AddWithValue("@SRP", pobj.SRP);
                sqlCmd.Parameters.AddWithValue("@WholesaleMinPrice", pobj.WholesaleMinPrice);
                sqlCmd.Parameters.AddWithValue("@VendorAutoId", pobj.VendorAutoId);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@BillNo", pobj.BillNo);
                if (pobj.BillDate != DateTime.MinValue && pobj.BillDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@BillDate", pobj.BillDate);
                }
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@BillItemsAutoId", pobj.BillItemsAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@Quantity", pobj.Quantity);
                sqlCmd.Parameters.AddWithValue("@TotalPieces", pobj.TotalPieces);
                sqlCmd.Parameters.AddWithValue("@QtyPerUnit", pobj.QtyPerUnit);
                if (pobj.FromBillDate != DateTime.MinValue && pobj.FromBillDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromBillDate", pobj.FromBillDate);
                }
                if (pobj.ToBillDate != DateTime.MinValue && pobj.ToBillDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToBillDate", pobj.ToBillDate);
                }
                sqlCmd.Parameters.AddWithValue("@TableValue", pobj.TableValue);
                sqlCmd.Parameters.AddWithValue("@TblUpdateItems", pobj.TblUpdateItems);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@DraftAutoId", SqlDbType.VarChar, 20).Value = pobj.DraftAutoId;
                sqlCmd.Parameters["@DraftAutoId"].Direction = ParameterDirection.InputOutput;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.DraftAutoId = Convert.ToInt32(sqlCmd.Parameters["@DraftAutoId"].Value);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_StockReceiverMaster
    {
        public static void insert(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 11;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void Saveasdraft(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 12;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        
        public static void update(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 21;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void UpdateDraftReq(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 22;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void updateDraftOrder(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 23;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void deletestock(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 32;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void deleteDraftitem(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 33;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void bindProduct(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 41;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void bindUnitType(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 42;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void stockEntryList(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 43;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void editStockEntry(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 44;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void getProductThruBarcode(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 45;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void assignBarcode(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 46;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void countBarcode(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 47;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void GetProductDetails(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 48;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void InvstockEntryList(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 49;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void GetVendornStatus(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 50;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
        public static void updateBasePrice(PL_StockReceiverMaster pobj)
        {
            pobj.Opcode = 23;
            DL_StockReceiverMaster.ReturnTable(pobj);
        }
       
    }
}
