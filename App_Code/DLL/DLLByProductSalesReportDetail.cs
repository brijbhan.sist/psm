﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DLLByProductSalesReportDetail
/// </summary>
namespace DLLByProductSalesReportDetail
{
    public class PL_ProductSalesReportDetail : Utility
    {
        public int CustomerAutoId { get; set; }
        public string SalesPerson { get; set; }
        public int CategoryAutoId { get; set; }
        public int SubCategoryId { get; set; }
        public int EmpAutoId { get; set; }
        public int DriverAutoId { get; set; }
        public int OrderStatus { get; set; }
        public string PaymentStatus { get; set; }
        public int ProductAutoId { get; set; }
        public string ProductAutoIdStr { get; set; }
        public string BrandAutoIdStr { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int CustomerType { get; set; }
        public int SalesPersonAutoId { get; set; }
        public DateTime CloseOrderFromDate { get; set; }
        public DateTime CloseOrderToDate { get; set; }

        public int SearchBy { get; set; }
    }
    public class BL_ProductSalesReportDetail
    {

        public static void BindCustomer(PL_ProductSalesReportDetail pobj)
        {
            pobj.Opcode = 40;
            DL_ProductSalesReportDetail.ReturnData(pobj);
        }
        public static void BindProductCategory(PL_ProductSalesReportDetail pobj)
        {
            pobj.Opcode = 41;
            DL_ProductSalesReportDetail.ReturnData(pobj);
        }
        public static void BindProductSubCategory(PL_ProductSalesReportDetail pobj)
        {
            pobj.Opcode = 42;
            DL_ProductSalesReportDetail.ReturnData(pobj);
        }
        public static void BindProduct(PL_ProductSalesReportDetail pobj)
        {
            pobj.Opcode = 43;
            DL_ProductSalesReportDetail.ReturnData(pobj);
        }
        public static void BindSalesPersonandStatus(PL_ProductSalesReportDetail pobj)
        {
            pobj.Opcode = 44;
            DL_ProductSalesReportDetail.ReturnData(pobj);
        }
        public static void BindProductSaleReport(PL_ProductSalesReportDetail pobj)
        {
            pobj.Opcode = 45;
            DL_ProductSalesReportDetail.ReturnData(pobj);
        }

    }

    public class DL_ProductSalesReportDetail
    {
        public static void ReturnData(PL_ProductSalesReportDetail pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("ProcSalesByProductDetailsReport", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                cmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                cmd.Parameters.AddWithValue("@DriverAutoId", pobj.DriverAutoId);
                cmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                cmd.Parameters.AddWithValue("@SubCategoryId", pobj.SubCategoryId);
                cmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                cmd.Parameters.AddWithValue("@BrandAutoIdSring", pobj.BrandAutoIdStr);
                cmd.Parameters.AddWithValue("@ProductAutoIdSring", pobj.ProductAutoIdStr);
                cmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                cmd.Parameters.AddWithValue("@PaymentStatus", pobj.PaymentStatus);
                cmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                cmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                cmd.Parameters.AddWithValue("@CustomerType", pobj.CustomerType);
                cmd.Parameters.AddWithValue("@SearchBy", pobj.SearchBy);

                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }

                if (pobj.CloseOrderFromDate > DateTime.MinValue && pobj.CloseOrderFromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@CloseOrderFromDate", pobj.CloseOrderFromDate);
                }
                if (pobj.CloseOrderToDate > DateTime.MinValue && pobj.CloseOrderToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@CloseOrderToDate", pobj.CloseOrderToDate);
                }

                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
}