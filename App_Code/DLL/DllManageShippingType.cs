﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLDeviceMaster
/// </summary>
namespace DLLManageShippingType
{
    public class Pl_ManageShippingType : Utility
    {
        public int Autoid { get; set; }
        public string ShippingType { get; set; }
        public int EnableTax { get; set; }
        public int Status { get; set; }


    }

    public class DL_ManageShippingType
    {
        public static void ReturnTable(Pl_ManageShippingType pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcManageShippingType", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.Autoid);
                sqlCmd.Parameters.AddWithValue("@ShippingType", pobj.ShippingType);
                sqlCmd.Parameters.AddWithValue("@EnableTax", pobj.EnableTax);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }

    }

    public class BL_ManageShippingType
    {
        public static void insertShippingType(Pl_ManageShippingType pobj)
        {
            pobj.Opcode = 11;
            DL_ManageShippingType.ReturnTable(pobj);
        }
        public static void updateShippingType(Pl_ManageShippingType pobj)
        {
            pobj.Opcode = 21;
            DL_ManageShippingType.ReturnTable(pobj);
        }
        public static void GetShippingType(Pl_ManageShippingType pobj)
        {
            pobj.Opcode = 41;
            DL_ManageShippingType.ReturnTable(pobj);
        }
        public static void EditShippingType(Pl_ManageShippingType pobj)
        {
            pobj.Opcode = 31;
            DL_ManageShippingType.ReturnTable(pobj);
        }
        //public static void deleteShippingType(Pl_ManageShippingType pobj)
        //{
        //    pobj.Opcode = 42;
        //    DL_ManageShippingType.ReturnTable(pobj);
        //}
    }
}