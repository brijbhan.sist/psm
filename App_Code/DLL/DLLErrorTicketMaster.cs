﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLErrorTicketMaster
/// </summary>
namespace DLLErrorTicketMaster
{
	public class PL_ErrorTicketMaster:Utility
    {
     
        public DateTime TicketDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime FromDate { get; set; }
        public string TicketID { get; set; }
        public int ByEmployee { get; set; }
        public string Type { get; set; }
        public string PageUrl { get; set; }
        public string Priority { get; set; }              
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string TicketCloseDate { get; set; }
        public string Attach { get; set; }

	}
    public class DL_ErrorTicketMaster
    {
        public static void ReturnTable(PL_ErrorTicketMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcErrorTicketMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode); 
                sqlCmd.Parameters.Add("@TicketID", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@TicketID"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.AddWithValue("@PageUrl", pobj.PageUrl);
                sqlCmd.Parameters.AddWithValue("@Priority", pobj.Priority);
                sqlCmd.Parameters.AddWithValue("@Type", pobj.Type);
                sqlCmd.Parameters.AddWithValue("@ByEmployee", pobj.ByEmployee);
                sqlCmd.Parameters.AddWithValue("@Subject", pobj.Subject);
                sqlCmd.Parameters.AddWithValue("@Description", pobj.Description);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);                           
                if (pobj.TicketDate != DateTime.MinValue && pobj.TicketDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@TicketDate", pobj.TicketDate);
                }

                if (pobj.Opcode == 61)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                sqlCmd.Parameters.AddWithValue("@Attach", pobj.Attach);
                sqlCmd.Parameters.AddWithValue("@TicketCloseDate", pobj.TicketCloseDate);

                sqlCmd.Parameters.AddWithValue("@Who", pobj.Who);
               
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.TicketID = sqlCmd.Parameters["@TicketID"].Value.ToString();
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_ErrorTicketMaster
    {
        public static void Insert(PL_ErrorTicketMaster pobj)
        {
            pobj.Opcode = 11;
            DL_ErrorTicketMaster.ReturnTable(pobj);
        }
        public static void selectreceiver(PL_ErrorTicketMaster pobj)
        {
            pobj.Opcode = 41;
            DL_ErrorTicketMaster.ReturnTable(pobj);
        }
        public static void selectEmployeedetails(PL_ErrorTicketMaster pobj)
        {
            pobj.Opcode = 42;
            DL_ErrorTicketMaster.ReturnTable(pobj);
        }
    }
}