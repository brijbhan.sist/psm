﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DllUtility;

/// <summary>
/// Summary description for DllDailyOrderPaymentStatus
/// </summary>
namespace DllDailyOrderPaymentStatus
{
    public class PL_DailyOrderPaymentStatus : Utility
    {

        public int EmpAutoId { get; set; }
        public int DriverAutoId { get; set; }
        public int OrderStatus { get; set; }
        public string PaymentStatus { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime CloseOrderFromDate { get; set; }
        public DateTime CloseOrderToDate { get; set; }
    }
    public class DL_DailyOrderPaymentStatus
    {
        public static void ReturnData(PL_DailyOrderPaymentStatus pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[ProcDailyOrderPaymentStatus]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                cmd.Parameters.AddWithValue("@DriverAutoId", pobj.DriverAutoId);
                cmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                cmd.Parameters.AddWithValue("@PaymentStatus", pobj.PaymentStatus);

                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }

                if (pobj.CloseOrderFromDate > DateTime.MinValue && pobj.CloseOrderFromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@CloseOrderFromDate", pobj.CloseOrderFromDate);
                }
                if (pobj.CloseOrderToDate > DateTime.MinValue && pobj.CloseOrderToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@CloseOrderToDate", pobj.CloseOrderToDate);
                }

                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
    public class BL_DailyOrderPaymentStatus
    {
        public static void bindStatus(PL_DailyOrderPaymentStatus pobj)
        {
            pobj.Opcode =41;
            DL_DailyOrderPaymentStatus.ReturnData(pobj);
        }
        public static void OrderPaymentStatusReport(PL_DailyOrderPaymentStatus pobj)
        {
            pobj.Opcode =42;
            DL_DailyOrderPaymentStatus.ReturnData(pobj);
        }
    }
}