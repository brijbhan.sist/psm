﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DLLCommissionMaster
/// </summary>
namespace DLLCommissionMaster
{
    public class PL_CommissionMaster : Utility
    {
        public int Autoid { get; set; }
        public string CommissionXml { get; set; }
        public int Createdby { get; set; }
        public int @CommissionCode { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }

    }

    public class DL_CommissionMaster
    {
        public static void ReturnTable(PL_CommissionMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcCommissionMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.Autoid);
                sqlCmd.Parameters.AddWithValue("@Createdby", pobj.Createdby);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@CommissionXml", pobj.CommissionXml);
                sqlCmd.Parameters.AddWithValue("@CommissionCode", pobj.CommissionCode);
                sqlCmd.Parameters.AddWithValue("@Description", pobj.Description);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_CommissionMaster
    {
        public static void insertCommission(PL_CommissionMaster pobj)
        {
            pobj.Opcode = 11;
            DL_CommissionMaster.ReturnTable(pobj);
        }
        public static void updateCommission(PL_CommissionMaster pobj)
        {
            pobj.Opcode = 21;
            DL_CommissionMaster.ReturnTable(pobj);
        }
        public static void GetCommissionDetails(PL_CommissionMaster pobj)
        {
            pobj.Opcode = 41;
            DL_CommissionMaster.ReturnTable(pobj);
        }
        public static void editCommission(PL_CommissionMaster pobj)
        {
            pobj.Opcode = 42;
            DL_CommissionMaster.ReturnTable(pobj);
        }
        public static void DeleteCommission(PL_CommissionMaster pobj)
        {
            pobj.Opcode = 31;
            DL_CommissionMaster.ReturnTable(pobj);
        }
    }
}