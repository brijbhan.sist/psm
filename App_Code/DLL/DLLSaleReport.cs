﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLSaleReport
/// </summary>
/// 
namespace DLLSaleReport
{
    public class PL_SaleReport : Utility
    {
        public int CustomerAutoId { get; set; }
        public string SalesPerson { get; set; }
        public int CategoryAutoId { get; set; }
        public int SubCategoryId { get; set; }
        public int EmpAutoId { get; set; }
        public int DriverAutoId { get; set; }
        public int OrderStatus { get; set; }
        public string PaymentStatus { get; set; }
        public int ProductAutoId { get; set; }
        public string ProductAutoIdStr { get; set; }
        public string BrandAutoIdStr { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int CustomerType { get; set; }
        public int SalesPersonAutoId { get; set; }
        public DateTime CloseOrderFromDate { get; set; }
        public DateTime CloseOrderToDate { get; set; }
        
        public int SearchBy { get; set; }
    }
    public class BL_SaleReport
    {

        public static void BindSalesPersonandStatus(PL_SaleReport pobj)
        {
            pobj.Opcode = 41;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void BindCustomer(PL_SaleReport pobj)
        {
            pobj.Opcode = 50;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void OrderWiseOpenBalanceReport(PL_SaleReport pobj)
        {
            pobj.Opcode = 42;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void OpenBalanceReportSummary(PL_SaleReport pobj)
        {
            pobj.Opcode = 43;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void OpenBalanceReportDetail(PL_SaleReport pobj)
        {
            pobj.Opcode = 44;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void BindSalesPerson(PL_SaleReport pobj)
        {
            pobj.Opcode = 45;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void SaleBySalesPersonDetail(PL_SaleReport pobj)
        {
            pobj.Opcode = 46;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void SaleByCustomerSalesPersonDetail(PL_SaleReport pobj)
        {
            pobj.Opcode = 55;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void BindProduct(PL_SaleReport pobj)
        {
            pobj.Opcode = 47;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void BindProductSaleReport(PL_SaleReport pobj)
        {
            pobj.Opcode = 48;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void BindProductCategory(PL_SaleReport pobj)
        {
            pobj.Opcode = 49;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void BindProductSubCategory(PL_SaleReport pobj)
        {
            pobj.Opcode = 51;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void ReadytoShipReport(PL_SaleReport pobj)
        {
            pobj.Opcode = 52;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void OrderPaymentStatusReport(PL_SaleReport pobj)
        {
            pobj.Opcode = 53;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void bindStatus(PL_SaleReport pobj)
        {
            pobj.Opcode = 54;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void BindCustomerType(PL_SaleReport pobj)
        {
            pobj.Opcode = 57;
            DL_SaleReport.ReturnData(pobj);
        }
        public static void BindProductDropdown(PL_SaleReport pobj)
        {
            pobj.Opcode = 58;
            DL_SaleReport.ReturnData(pobj);
        }
    }

    public class DL_SaleReport
    {
        public static void ReturnData(PL_SaleReport pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[ProcSalesReport]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode); 
                cmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                cmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                cmd.Parameters.AddWithValue("@DriverAutoId", pobj.DriverAutoId);
                cmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                cmd.Parameters.AddWithValue("@SubCategoryId", pobj.SubCategoryId);
                cmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                cmd.Parameters.AddWithValue("@BrandAutoIdSring", pobj.BrandAutoIdStr);
                cmd.Parameters.AddWithValue("@ProductAutoIdSring", pobj.ProductAutoIdStr);
                cmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                cmd.Parameters.AddWithValue("@PaymentStatus", pobj.PaymentStatus);
                cmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                cmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                cmd.Parameters.AddWithValue("@CustomerType", pobj.CustomerType);
                cmd.Parameters.AddWithValue("@SearchBy", pobj.SearchBy);
              
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }

                if (pobj.CloseOrderFromDate > DateTime.MinValue && pobj.CloseOrderFromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@CloseOrderFromDate", pobj.CloseOrderFromDate);
                }
                if (pobj.CloseOrderToDate > DateTime.MinValue && pobj.CloseOrderToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@CloseOrderToDate", pobj.CloseOrderToDate);
                }
               
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
}