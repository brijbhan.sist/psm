﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ItemWiseReport
/// </summary>
namespace ItemWiseReport
{
    public class PL_ItemWiseReport : Utility
    {
        public int ProductAutoId { get; set; }
        public int Status { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
    public class DL_PL_ItemWiseReport
    {
        public static void ReturnTable(PL_ItemWiseReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("Proc_ProductWiseSaleWithStockReport", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_PL_ItemWiseReport
    {
        public static void getProductSalesStockReport(PL_ItemWiseReport pobj)
        {
            pobj.Opcode = 41;
            DL_PL_ItemWiseReport.ReturnTable(pobj);
        }
        public static void bindDropDown(PL_ItemWiseReport pobj)
        {
            pobj.Opcode = 42;
            DL_PL_ItemWiseReport.ReturnTable(pobj);
        }
    }
}