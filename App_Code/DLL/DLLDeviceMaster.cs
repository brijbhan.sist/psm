﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLDeviceMaster
/// </summary>
namespace DLLDeviceMaster
{
    public class Pl_DeviceMaster : Utility
    {
        public int Autoid { get; set; }
        public string DeviceId { get; set; }
        public int Createdby { get; set; }
        public string DeviceName { get; set; }
        public int Status { get; set; }
        public int IsLocationRequired { get; set; }
        

    }

    public class DL_DeviceMaster
    {
        public static void ReturnTable(Pl_DeviceMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcDeviceMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.Autoid);
                sqlCmd.Parameters.AddWithValue("@Createdby", pobj.Createdby);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@IsLocationRequired", pobj.IsLocationRequired);
                sqlCmd.Parameters.AddWithValue("@DeviceId", pobj.DeviceId);
                sqlCmd.Parameters.AddWithValue("@DeviceName", pobj.DeviceName);

                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);


                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }

    }

    public class BL_DeviceMaster
    {
        public static void insertDevice(Pl_DeviceMaster pobj)
        {
            pobj.Opcode = 11;
            DL_DeviceMaster.ReturnTable(pobj);
        }
        public static void updateDevice(Pl_DeviceMaster pobj)
        {
            pobj.Opcode = 21;
            DL_DeviceMaster.ReturnTable(pobj);
        }
        public static void GetDeviceDetail(Pl_DeviceMaster pobj)
        {
            pobj.Opcode = 31;
            DL_DeviceMaster.ReturnTable(pobj);
        }
        public static void editDevice(Pl_DeviceMaster pobj)
        {
            pobj.Opcode = 41;
            DL_DeviceMaster.ReturnTable(pobj);
        }
        public static void DeleteDevice(Pl_DeviceMaster pobj)
        {
            pobj.Opcode = 42;
            DL_DeviceMaster.ReturnTable(pobj);
        }
    }
}