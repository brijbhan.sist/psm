﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLSalesCommission
/// </summary>
/// 
namespace DLLSalesCommissionWithMigrate
{
    public class PL_SalesCommissionWithMigrate : Utility
    {
        public int SalesPerson { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
    public class BL_SalesCommissionWithMigrate
    {
        public static void BindSalesPerson(PL_SalesCommissionWithMigrate pobj)
        {
            pobj.Opcode = 41;
            DL_SalesCommissionWithMigrate.ReturnData(pobj);
        }

        public static void BindSaleReport(PL_SalesCommissionWithMigrate pobj)
        {
            pobj.Opcode = 42;
            DL_SalesCommissionWithMigrate.ReturnData(pobj);
        }
    }

    public class DL_SalesCommissionWithMigrate
    {
        public static void ReturnData(PL_SalesCommissionWithMigrate pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("ProcSalesCommissionMigrate", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                pobj.Ds = new DataSet();
                cmd.CommandTimeout = 10000;
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception)
            {

            }
        }
    }
}