﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropertyModificationBugMaster
/// </summary>
public class PropertyModificationBugMaster : PropertyUtilityAbstract
{
    DateTime ticketDate;

    public DateTime TicketDate
    {
        get { if (ticketDate == null || ticketDate == DateTime.MinValue) { ticketDate = Convert.ToDateTime("01/01/2010"); } return ticketDate; }
        set { if (ticketDate == null || ticketDate == DateTime.MinValue) { ticketDate = Convert.ToDateTime("01/01/2010"); } ticketDate = value; }
    }
    string ticketID, pageUrl, priority, type, byEmployee, subject, description, status, ticketCloseDate, attach;

    public string TicketID
    {
        get { return ticketID; }
        set { ticketID = value; }
    }

    public string PageUrl
    {
        get { return pageUrl; }
        set { pageUrl = value; }
    }

    public string Priority
    {
        get { return priority; }
        set { priority = value; }
    }

    public string Type
    {
        get { return type; }
        set { type = value; }
    }

    public string ByEmployee
    {
        get { return byEmployee; }
        set { byEmployee = value; }
    }

    public string Subject
    {
        get { return subject; }
        set { subject = value; }
    }

    public string Description
    {
        get { return description; }
        set { description = value; }
    }

    public string Status
    {
        get { return status; }
        set { status = value; }
    }

    public string TicketCloseDate
    {
        get { return ticketCloseDate; }
        set { ticketCloseDate = value; }
    }

    public string Attach
    {
        get { return attach; }
        set { attach = value; }
    }
}