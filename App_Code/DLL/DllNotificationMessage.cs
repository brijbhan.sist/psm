﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllNotificationMessage
{
    public class PL_NotificationMessage : Utility
    {
        public int EmpAutoId { get; set; }
        public int TeamId { get; set; }
        public int Sequence { get; set; }    
        public string Message { get; set; }
        public string Title { get; set; }
        public int AutoId { get; set; }
        public DataTable dtbulkmessageImageUrl { get; set; }
        public int Noblank { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string ExpiryTime { get; set; }
        public string Recipient { get; set; }
        public int MessageAutoId { get; set; }
        public DateTime StartDate { get; set; }
        public string StartTime { get; set; }
        public int IsRepeat { get; set; }
    }
    public class DL_NotificationMessage
    {
        public static void ReturnTable(PL_NotificationMessage pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcNotificationMessage", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@TeamId", pobj.TeamId);
                sqlCmd.Parameters.AddWithValue("@Message", pobj.Message);
                sqlCmd.Parameters.AddWithValue("@Title", pobj.Title); 
                sqlCmd.Parameters.AddWithValue("@dtbulkmessageImageUrl", pobj.dtbulkmessageImageUrl);
                sqlCmd.Parameters.AddWithValue("@Noblank", pobj.Noblank);
                sqlCmd.Parameters.AddWithValue("@Sequence", pobj.Sequence);
                if (pobj.ExpiryDate > DateTime.MinValue) 
                {
                    sqlCmd.Parameters.AddWithValue("@ExpiryDate", pobj.ExpiryDate);
                }
                sqlCmd.Parameters.AddWithValue("@ExpiryTime", pobj.ExpiryTime);
                if (pobj.StartDate > DateTime.MinValue)
                {
                    sqlCmd.Parameters.AddWithValue("@StartDate", pobj.StartDate);
                }
                sqlCmd.Parameters.AddWithValue("@StartTime", pobj.StartTime);
                sqlCmd.Parameters.AddWithValue("@IsRepeat", pobj.IsRepeat);
                sqlCmd.Parameters.AddWithValue("@Recipient", pobj.Recipient);
                sqlCmd.Parameters.AddWithValue("@MessageAutoId", pobj.MessageAutoId);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_NotificationMessage
    {
        public static void SelectRecipient(PL_NotificationMessage pobj)
        {
            pobj.Opcode = 401;
            DL_NotificationMessage.ReturnTable(pobj);
        }
        public static void SaveMessage(PL_NotificationMessage pobj)
        {
            pobj.Opcode = 101;
            DL_NotificationMessage.ReturnTable(pobj);
        }
        public static void EditMessage(PL_NotificationMessage pobj)
        {
            pobj.Opcode = 402;
            DL_NotificationMessage.ReturnTable(pobj);
        }
        public static void GetMessages(PL_NotificationMessage pobj)
        {
            pobj.Opcode = 403;
            DL_NotificationMessage.ReturnTable(pobj);
        }
        public static void updateMessage(PL_NotificationMessage pobj)
        {
            pobj.Opcode = 21;
            DL_NotificationMessage.ReturnTable(pobj);
        }
        public static void deleteMessage(PL_NotificationMessage pobj)
        {
            pobj.Opcode = 31;
            DL_NotificationMessage.ReturnTable(pobj);
        }
    }
}
