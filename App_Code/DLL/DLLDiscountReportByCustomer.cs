﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DLLDetailProductSalesReport
/// </summary>
namespace DLLDiscountReportByCustomer
{
    public class PL_DiscountReportByCustomer : Utility
    {
        public string SalesPerson { get; set; }
        public int EmpAutoId { get; set; } 
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int CustomerAutoId { get; set; }
    }
    public class DL_DiscountReportByCustomer
    {
        public static void ReturnData(PL_DiscountReportByCustomer pobj)
        {
            try

            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("ProcDiscountReportByCustomer", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                cmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);              
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                cmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                cmd.CommandTimeout = 1000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }

    public class BL_DiscountReportByCustomer
    {
        public static void BindProductSaleReport(PL_DiscountReportByCustomer pobj)
        {
            pobj.Opcode = 42;
            DL_DiscountReportByCustomer.ReturnData(pobj);
        }
        public static void BindCustomer(PL_DiscountReportByCustomer pobj)
        {
            pobj.Opcode = 45;
            DL_DiscountReportByCustomer.ReturnData(pobj);
        }
        public static void CustomerDropdown(PL_DiscountReportByCustomer pobj)
        {
            pobj.Opcode = 46;
            DL_DiscountReportByCustomer.ReturnData(pobj);
        }
    }
}