﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data;
using System.Data.SqlClient;

namespace DLLPaymentLogReport
{
    public class PL_PaymentLogReport : Utility
    {
        public int CustomerAutoId { get; set; }
        public int EmpAutoId { get; set; }
        public int PaymentAutoId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime SettledFromDate { get; set; }
        public DateTime SettledToDate { get; set; }
        public int Status { get; set; }
        public string PayId { get; set; }
        public decimal PaymentAmount { get; set; }
        public int Range { get; set; }
        public int PaymentMode { get; set; }
    }

    public class DL_PaymentLogReport
    {
        public static void ReturnTable(PL_PaymentLogReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPaymentLogReport", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@PaymentAutoId", pobj.PaymentAutoId);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@PayId", pobj.PayId);
                if (DateTime.MinValue < pobj.FromDate && DateTime.MaxValue > pobj.FromDate)
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                if (DateTime.MinValue < pobj.ToDate && DateTime.MaxValue > pobj.ToDate)
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                if (DateTime.MinValue < pobj.SettledFromDate && DateTime.MaxValue > pobj.SettledFromDate)
                    sqlCmd.Parameters.AddWithValue("@SettledFromDate", pobj.SettledFromDate);
                if (DateTime.MinValue < pobj.SettledToDate && DateTime.MaxValue > pobj.SettledToDate)
                    sqlCmd.Parameters.AddWithValue("@SettledToDate", pobj.SettledToDate);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@Range", pobj.Range);
                sqlCmd.Parameters.AddWithValue("@PaymentAmount", pobj.PaymentAmount);
                sqlCmd.Parameters.AddWithValue("@PaymentMode", pobj.PaymentMode);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_PaymentLogReport
    {
        public static void BindPaymentLogReport(PL_PaymentLogReport pobj)
        {
            pobj.Opcode = 41;
            DL_PaymentLogReport.ReturnTable(pobj);
        }
        public static void BindDropDown(PL_PaymentLogReport pobj)
        {
            pobj.Opcode = 42;
            DL_PaymentLogReport.ReturnTable(pobj);
        }
        public static void getReceivedPrint(PL_PaymentLogReport pobj)
        {
            pobj.Opcode = 43;
            DL_PaymentLogReport.ReturnTable(pobj);
        }
    }
}
