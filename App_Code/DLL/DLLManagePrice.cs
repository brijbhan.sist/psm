﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DLLManagePrice
/// </summary>
namespace DLLManagePrice
{
    public class PL_ManagePrice : Utility
    {
        public int EmpAutoId { get; set; }
        public int ProductAutoid { get; set; }
        public DataTable TableValueMultiProductAutoId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Qty { get; set; }
        public decimal CostPrice { get; set; }
        public decimal Price { get; set; }
        public decimal MinPrice { get; set; }
        public decimal WMinPrice { get; set; }
        public int ReOrderMark { get; set; }
        public int CostPriceCompare {get; set;}
        public int BrandAutoId { get; set; }
        public int CategoryAutoId { get; set; }
        public int SubcategoryAutoId { get; set; }
        public decimal SRP { get; set; }
    }
    public class DL_ManagePrice
    {
        public static void ReturnTable(PL_ManagePrice pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcManagePrice", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@OpCode", pobj.Opcode);

                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductAutoIdData", pobj.TableValueMultiProductAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductId", pobj.ProductId);
                sqlCmd.Parameters.AddWithValue("@ProductName", pobj.ProductName);
                sqlCmd.Parameters.AddWithValue("@ProductAutoid", pobj.ProductAutoid);
                sqlCmd.Parameters.AddWithValue("@Qty", pobj.Qty);
                sqlCmd.Parameters.AddWithValue("@CostPrice", pobj.CostPrice);
                sqlCmd.Parameters.AddWithValue("@Price", pobj.Price);
                sqlCmd.Parameters.AddWithValue("@MinPrice", pobj.MinPrice);
                sqlCmd.Parameters.AddWithValue("@WMinPrice", pobj.WMinPrice);
                sqlCmd.Parameters.AddWithValue("@ReOrderMark", pobj.ReOrderMark);
                sqlCmd.Parameters.AddWithValue("@BrandAutoId", pobj.BrandAutoId);
                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@SubcategoryAutoId", pobj.SubcategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@SRP", pobj.SRP);
                sqlCmd.Parameters.AddWithValue("@CostPriceCompare", pobj.CostPriceCompare);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
           {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_Manageprice
    {
        public static void getPrice(PL_ManagePrice pobj)
        {
            pobj.Opcode = 101;
            DL_ManagePrice.ReturnTable(pobj);
        }
        public static void UpdarePrice(PL_ManagePrice pobj)
        {
            pobj.Opcode = 102;
            DL_ManagePrice.ReturnTable(pobj);
        }
        public static void updateReOrderMark(PL_ManagePrice pobj)
        {
            pobj.Opcode = 201;
            DL_ManagePrice.ReturnTable(pobj);
        }
        public static void updateChangedPrice(PL_ManagePrice pobj)
        {
            pobj.Opcode = 202;
            DL_ManagePrice.ReturnTable(pobj);
        }
          public static void UpdateChangedReorderMark(PL_ManagePrice pobj)
        {
            pobj.Opcode = 203;
            DL_ManagePrice.ReturnTable(pobj);
        }

        public static void bindCategory(PL_ManagePrice pobj)
        {
            pobj.Opcode = 41;
            DL_ManagePrice.ReturnTable(pobj);
        }
        public static void bindSubcategory(PL_ManagePrice pobj)
        {
            pobj.Opcode = 42;
            DL_ManagePrice.ReturnTable(pobj);
        }
    }
}