﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllEmployee
{
    public class PL_Employee : Utility
    {
        public int AutoId { get; set; }
        public string EmpId { get; set; }
        public string EmpCode { get; set; }
        public string ProfileName { get; set; }
        public int EmpType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }        
        public string Email { get; set; }
        public string Contact { get; set; }
        public string ImageURL { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public int Status { get; set; }
        public string UserName { get; set; }        
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string IpAddress { get; set; }
        public int IsLoginIp { get; set; }
        public int IsAppLogin { get; set; }
        public int EmpAutoId { get; set; }
        public string AssignDeviceAutoId { get; set; }
        public string IpAddressAutoId { get; set; }
        public string DeliveryStartTime { get; set; }
        public string DeliveryCloseTime { get; set; }

    }
    public class DL_Employee
    {
        public static void ReturnTable(PL_Employee pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcEmployeeMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@EmpCode", pobj.EmpCode);
                sqlCmd.Parameters.AddWithValue("@ProfileName", pobj.ProfileName);
                sqlCmd.Parameters.AddWithValue("@EmpType", pobj.EmpType);
                sqlCmd.Parameters.AddWithValue("@FirstName", pobj.FirstName);
                sqlCmd.Parameters.AddWithValue("@LastName", pobj.LastName);
                sqlCmd.Parameters.AddWithValue("@Email", pobj.Email);
                sqlCmd.Parameters.AddWithValue("@Contact", pobj.Contact);
                sqlCmd.Parameters.AddWithValue("@ImageURL", pobj.ImageURL);
                sqlCmd.Parameters.AddWithValue("@Address", pobj.Address);
                sqlCmd.Parameters.AddWithValue("@State", pobj.State);
                sqlCmd.Parameters.AddWithValue("@City", pobj.City);
                sqlCmd.Parameters.AddWithValue("@IsLoginIp", pobj.IsLoginIp);
                sqlCmd.Parameters.AddWithValue("@IpAddress", pobj.IpAddress);
                sqlCmd.Parameters.AddWithValue("@Zipcode", pobj.Zipcode);
                sqlCmd.Parameters.AddWithValue("@UserName", pobj.UserName);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@Password", pobj.Password);
                sqlCmd.Parameters.AddWithValue("@IsAppLogin", pobj.IsAppLogin); 
                sqlCmd.Parameters.AddWithValue("@AssignDeviceAutoId", pobj.AssignDeviceAutoId);
                sqlCmd.Parameters.AddWithValue("@IpAddressAutoId", pobj.IpAddressAutoId);
                sqlCmd.Parameters.AddWithValue("@DeliveryStartTime", pobj.DeliveryStartTime);
                sqlCmd.Parameters.AddWithValue("@DeliveryCloseTime", pobj.DeliveryCloseTime);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_Employee
    {
        public static void insert(PL_Employee pobj)
        {
            pobj.Opcode = 11;
            DL_Employee.ReturnTable(pobj);
        }
        public static void update(PL_Employee pobj)
        {
            pobj.Opcode = 21;
            DL_Employee.ReturnTable(pobj);
        }
        public static void updatepass(PL_Employee pobj)
        {
            pobj.Opcode = 22;
            DL_Employee.ReturnTable(pobj);
        }
        public static void deleteEmpRecord(PL_Employee pobj)
        {
            pobj.Opcode = 31;
            DL_Employee.ReturnTable(pobj);
        }
        public static void selectEmpTypeAndStatus(PL_Employee pobj)
        {
            pobj.Opcode = 41;
            DL_Employee.ReturnTable(pobj);
        }
        public static void selectEmpRecord(PL_Employee pobj)
        {
            pobj.Opcode = 42;
            DL_Employee.ReturnTable(pobj);
        }
        public static void editEmpRecord(PL_Employee pobj)
        {
            pobj.Opcode = 43;
            DL_Employee.ReturnTable(pobj);
        }
        public static void deleteEmp(PL_Employee pobj)
        {
            pobj.Opcode = 44;
            DL_Employee.ReturnTable(pobj);
        }
        public static void BindViewProfile(PL_Employee pobj)
        {
            pobj.Opcode = 45;
            DL_Employee.ReturnTable(pobj);
        }
        public static void loginwith(PL_Employee pobj)
        {
            pobj.Opcode = 46;
            DL_Employee.ReturnTable(pobj);
        }
        public static void getemployeeinfo(PL_Employee pobj)
        {
            pobj.Opcode = 47;
            DL_Employee.ReturnTable(pobj);
        }
    }
}
