﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropertyErrorTicketResponse
/// </summary>
public class PropertyErrorTicketResponse : PropertyUtilityAbstract
{

    string ticketID, actionNo, action, byEmployee, status, developerStatus;

    public string DeveloperStatus
    {
        get { return developerStatus; }
        set { developerStatus = value; }
    }

    public string TicketID
    {
        get { return ticketID; }
        set { ticketID = value; }
    }

    public string ActionNo
    {
        get { return actionNo; }
        set { actionNo = value; }
    }

    public string Action
    {
        get { return action; }
        set { action = value; }
    }

    public string ByEmployee
    {
        get { return byEmployee; }
        set { byEmployee = value; }
    }

    public string Status
    {
        get { return status; }
        set { status = value; }
    }
}