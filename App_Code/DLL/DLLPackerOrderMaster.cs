﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DllUtility;
/// <summary>
/// Summary description for DLLPackerOrderMaster
/// </summary>
/// 
namespace DLLPackerOrderMaster
{
    public class PL_Packer_OrderMaster : Utility
    {
        public int OrderAutoId { get; set; }
        public int CustomerAutoId { get; set; }
        public int OrderStatus { get; set; }
        public string OrderNo { get; set; }
        public int SalesPersonAutoId { get; set; }
        public int EmpAutoId { get; set; }
        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int ReqQty { get; set; }
        public int ShipQty { get; set; }
        public int QtyPerUnit { get; set; }
        public int IsExchange { get; set; }
        public int IsFreeItem { get; set; }
        public DataTable DTPackedItemsQty { get; set; }
        public DataTable AllProduct { get; set; }
        public decimal OverallDisc { get; set; }
        public decimal OverallDiscAmt { get; set; } 
        public decimal TotalTax { get; set; }
        public decimal GrandTotal { get; set; }
        public int PackedBoxes { get; set; }
        public decimal TotalAmount { get; set; }
        public int PackerAutoId { get; set; }  
        public decimal AdjustmentAmt { get; set; }
        public string Remarks { get; set; }
        public DateTime Fromdate { get; set; }
        public DateTime Todate { get; set; }
        public string CheckSecurity { get; set; }
        public string BarCode { get; set; }
        public string ShippingType { get; set; }
        public int ShippingAutoId { get; set; }
        public int EmployeeType { get; set; }
        public string AddedItemKey { get; set; } 
    }

    public class DL_Packer_OrderMaster
    {
        public static void ReturnTable(PL_Packer_OrderMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("Proc_Packer_OrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@ReqQty", pobj.ReqQty);
                sqlCmd.Parameters.AddWithValue("@ShipQuantity", pobj.ShipQty);
                sqlCmd.Parameters.AddWithValue("@QtyPerUnit", pobj.QtyPerUnit);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                sqlCmd.Parameters.AddWithValue("@CheckSecurity", pobj.CheckSecurity);
                sqlCmd.Parameters.AddWithValue("@BarCode", pobj.BarCode); 
                sqlCmd.Parameters.AddWithValue("@DTPackedItemsQty", pobj.DTPackedItemsQty);
                sqlCmd.Parameters.AddWithValue("@AllProduct", pobj.AllProduct);
                sqlCmd.Parameters.AddWithValue("@PackedBoxes", pobj.PackedBoxes);
                sqlCmd.Parameters.AddWithValue("@PackerAutoId", pobj.PackerAutoId);
                
                sqlCmd.Parameters.AddWithValue("@EmployeeType", pobj.EmployeeType);
                sqlCmd.Parameters.AddWithValue("@ShippingAutoId", pobj.ShippingAutoId);  
                sqlCmd.Parameters.AddWithValue("@IsExchange", pobj.IsExchange);
                sqlCmd.Parameters.AddWithValue("@IsFreeItem", pobj.IsFreeItem);
                if (HttpContext.Current.Session["EmpAutoId"] != null)
                    sqlCmd.Parameters.AddWithValue("@EmpAutoId", HttpContext.Current.Session["EmpAutoId"].ToString());
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                if (pobj.Fromdate != DateTime.MinValue && pobj.Fromdate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Fromdate", pobj.Fromdate);
                }
                if (pobj.Todate != DateTime.MinValue && pobj.Todate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Todate", pobj.Todate);
                }
                sqlCmd.Parameters.AddWithValue("@ShippingType", pobj.ShippingType);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_Packer_OrderMaster
    {

        public static void getOrderList(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 41;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }


        public static void getOrderData(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 42;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void bindStatus(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 43;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void clickonSecurity(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 44;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void AddProductQty(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 21;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void checkBarcode(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 45;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void genPacking(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 103;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void bindUnitTypes(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 104;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void UpdateUnitType(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 105;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void checkManagerProvidedKey(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 106;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void checkManagerProvidedKeyBarcode(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 107;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void DeallocateProductQty(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 108;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void UpdateQtyManual(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 109;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void SaveAddBoxes(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 110;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void CheckStocks(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 123;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void getPackerAssignPrintData(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 124;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
        public static void barcode_Print(PL_Packer_OrderMaster pobj)
        {
            pobj.Opcode = 125;
            DL_Packer_OrderMaster.ReturnTable(pobj);
        }
    }
}