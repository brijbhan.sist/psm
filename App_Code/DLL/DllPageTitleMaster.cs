﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllPageTitleMaster
{
    public class PL_PageTitleMaster : Utility
    {
        public int AutoId { get; set; }
        public int PageId { get; set; }
        public string PageUrl { get; set; }
        public string PageTitle { get; set; }
        public string UserDescription { get; set; }
        public string AdminDescription { get; set; }
        public int Status { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public int EmpAutoId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

    }
    public class DL_PageTitleMaster
    {
        public static void ReturnTable(PL_PageTitleMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPageTitleMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@PageId", pobj.PageId);
                sqlCmd.Parameters.AddWithValue("@PageUrl", pobj.PageUrl);
                sqlCmd.Parameters.AddWithValue("@PageTitle", pobj.PageTitle);
                sqlCmd.Parameters.AddWithValue("@UserDescription", pobj.UserDescription);
                sqlCmd.Parameters.AddWithValue("@AdminDescription", pobj.AdminDescription);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@LocationId", pobj.LocationId);
                sqlCmd.Parameters.AddWithValue("@LocationName", pobj.LocationName);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_PageTitleMaster
    {
        public static void savePageTitleDetial(PL_PageTitleMaster pobj)
        {
            pobj.Opcode = 11;
            DL_PageTitleMaster.ReturnTable(pobj);
        }
        public static void updatePageTitleDetial(PL_PageTitleMaster pobj)
        {
            pobj.Opcode = 21;
            DL_PageTitleMaster.ReturnTable(pobj);
        }
        public static void getPageInformation(PL_PageTitleMaster pobj)
        {
            pobj.Opcode = 41;
            DL_PageTitleMaster.ReturnTable(pobj);
        }
        public static void VerifyUserAccess(PL_PageTitleMaster pobj)
        {
            pobj.Opcode = 42;
            DL_PageTitleMaster.ReturnTable(pobj);
        }
        public static void getPageTitleDetail(PL_PageTitleMaster pobj)
        {
            pobj.Opcode = 43;
            DL_PageTitleMaster.ReturnTable(pobj);
        }
        public static void getdata(PL_PageTitleMaster pobj)
        {
            pobj.Opcode = 44;
            DL_PageTitleMaster.ReturnTable(pobj);
        }
    }
}
