﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DllUtility;
/// <summary>
/// Summary description for DLLPSMCTSaleReport
/// </summary>
namespace DLLPSMCTSaleReport
{
    public class PL_PSMCTSaleReport : Utility
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
    public class DL_PSMCTSaleReport
    {
        public static void ReturnTable(PL_PSMCTSaleReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("[ProcPSMCT_ProductSold]", connect.con);
                sqlCmd.CommandTimeout = 100000;
                sqlCmd.CommandType = CommandType.StoredProcedure;
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }

        }
    }
    public class BL_PSMCTSaleReport
    {
        public static void getreport(PL_PSMCTSaleReport pobj)
        {
            DL_PSMCTSaleReport.ReturnTable(pobj);
        }
    }
}