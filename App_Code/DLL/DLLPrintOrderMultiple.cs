﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for DLLPrintOrderMultiple
/// </summary>
namespace DLLPrintOrderMultiple
{
    public class PL_PrintOrderMultiple : Utility
    {
       public int OrderAutoId { get; set; }
        public int LoginEmpType { get; set; }
        public int EmpAutoId { get; set; }
        public string OrderNo { get; set; }
    }
    public class DL_PrintOrderMultiple
    {
        public static void ReturnTable(PL_PrintOrderMultiple pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPrintOrderMultiple", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);               
               // sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
               
                //if (pobj.OrderDate != DateTime.MinValue && pobj.OrderDate != DateTime.MaxValue)
                //{
                //    sqlCmd.Parameters.AddWithValue("@OrderDate", pobj.OrderDate);
                //}
                //if (pobj.DeliveryDate != DateTime.MinValue && pobj.DeliveryDate != DateTime.MaxValue)
                //{
                //    sqlCmd.Parameters.AddWithValue("@DeliveryDate", pobj.DeliveryDate);
                //}
                                          
              //  sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
              //  sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
              //  sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);              
               // sqlCmd.Parameters["@DraftAutoId"].Direction = ParameterDirection.InputOutput;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);             
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_PrintOrderMultiple
    {
        public static void getOrderData(PL_PrintOrderMultiple pobj)
        {
            pobj.Opcode = 41;
            DL_PrintOrderMultiple.ReturnTable(pobj);
        }      
    }
}