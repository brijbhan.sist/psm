﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DLLManageAccessPage
/// </summary>
namespace DLLManageAccessPage
{
    public class PL_ManageAccessPage : Utility
    {
        public int ModuleAutoId { get; set; }
        public int SubModuleAutoId { get; set; }
        public int UserRole { get; set; }
        public string AssignedAction { get; set; }
        public int PageAutoId { get; set; }
        public int AutoId { get; set; }
        public int EmpAutoId { get; set; }
        public int Status { get; set; }
    }
    public class DL_ManageAccessPage
    {
        public static void ReturnTable(PL_ManageAccessPage pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcManageAccessPage", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@OpCode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@ModuleAutoId", pobj.ModuleAutoId);
                //sqlCmd.Parameters.AddWithValue("@SubModuleAutoId", pobj.SubModuleAutoId);
                sqlCmd.Parameters.AddWithValue("@UserRole", pobj.UserRole);
                sqlCmd.Parameters.AddWithValue("@PageAutoId", pobj.PageAutoId);
                sqlCmd.Parameters.AddWithValue("@AssignedAction", pobj.AssignedAction);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch(Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_ManageAccessPage
    {
        public static void insert(PL_ManageAccessPage pobj)
        {
            pobj.Opcode = 11;
            DL_ManageAccessPage.ReturnTable(pobj);
        }
        public static void update(PL_ManageAccessPage pobj)
        {
            pobj.Opcode = 21;
            DL_ManageAccessPage.ReturnTable(pobj);
        }
        public static void select(PL_ManageAccessPage pobj)
        {
            pobj.Opcode = 41;
            DL_ManageAccessPage.ReturnTable(pobj);
        }
        public static void delete(PL_ManageAccessPage pobj)
        {
            pobj.Opcode = 31;
            DL_ManageAccessPage.ReturnTable(pobj);
        }
        public static void getUserType(PL_ManageAccessPage pobj)
        {
            pobj.Opcode = 42;
            DL_ManageAccessPage.ReturnTable(pobj);
        }
        public static void bindSubModule(PL_ManageAccessPage pobj)
        {
            pobj.Opcode = 43;
            DL_ManageAccessPage.ReturnTable(pobj);
        }
        public static void bindModule(PL_ManageAccessPage pobj)
        {
            pobj.Opcode = 44;
            DL_ManageAccessPage.ReturnTable(pobj);
        }
        public static void getPage(PL_ManageAccessPage pobj)
        {
            pobj.Opcode = 45;
            DL_ManageAccessPage.ReturnTable(pobj);
        }
        public static void bindSearchSubModule(PL_ManageAccessPage pobj)
        {
            pobj.Opcode = 46;
            DL_ManageAccessPage.ReturnTable(pobj);
        }
        public static void edit(PL_ManageAccessPage pobj)
        {
            pobj.Opcode = 47;
            DL_ManageAccessPage.ReturnTable(pobj);
        }
    }
}
