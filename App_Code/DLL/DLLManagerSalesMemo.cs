﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DLLManagerSalesMemo
/// </summary>
namespace DLLManagerSalesMemo
{
    public class PL_ManagerSalesMemo : Utility
	{
        public string CancelRemarks { get; set; }
        public string SecurityKey { get; set; }
        public int CreditAutoId { get; set; }
        public int CustomerAutoId { get; set; }
        public int EmpAutoId { get; set; }
        public int Qty { get; set; }
        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int Status { get; set; }
        public int CreditType { get; set; }
        public string CreditNo { get; set; }
        public string Remarks { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int EmpType { get; set; }
        public DataTable TableValue { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal OverallDisc { get; set; }
        public decimal OverallDiscAmt { get; set; }
        public decimal TotalTax { get; set; }
        public decimal GrandTotal { get; set; }
        public int TaxType { get; set; }
        public decimal TaxValue { get; set; }
        public decimal MLQty { get; set; }
        public decimal MLTax { get; set; }
        public decimal AdjustmentAmt { get; set; }
        public string ManagerRemark { get; set; }
        public int CreditMemoType { get; set; }
        public int ReferenceOrderAutoId { get; set; }
    }
    public class DL_ManagerSalesMemo
    {
        public static void ReturnTable(PL_ManagerSalesMemo pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcManagerCreditMemo", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CreditAutoId", pobj.CreditAutoId);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@CreditNo", pobj.CreditNo);
                sqlCmd.Parameters.AddWithValue("@SecurityKey", pobj.SecurityKey);
                sqlCmd.Parameters.AddWithValue("@CancelRemarks", pobj.CancelRemarks);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@CreditType", pobj.CreditType);
                if (pobj.FromDate != DateTime.MinValue && pobj.FromDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@Remark", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@Qty", pobj.Qty);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@EmpType", pobj.EmpType);
                sqlCmd.Parameters.AddWithValue("@TableValue", pobj.TableValue);
                sqlCmd.Parameters.AddWithValue("@IPAddress", pobj.IPAddress);

                sqlCmd.Parameters.AddWithValue("@TotalAmount", pobj.TotalAmount);
                sqlCmd.Parameters.AddWithValue("@OverallDisc", pobj.OverallDisc);
                sqlCmd.Parameters.AddWithValue("@OverallDiscAmt", pobj.OverallDiscAmt);
                sqlCmd.Parameters.AddWithValue("@TotalTax", pobj.TotalTax);
                sqlCmd.Parameters.AddWithValue("@GrandTotal", pobj.GrandTotal);
                sqlCmd.Parameters.AddWithValue("@TaxType", pobj.TaxType);
                sqlCmd.Parameters.AddWithValue("@TaxValue", pobj.TaxValue);
                sqlCmd.Parameters.AddWithValue("@MLQty", pobj.MLQty);
                sqlCmd.Parameters.AddWithValue("@MLTax", pobj.MLTax);
                sqlCmd.Parameters.AddWithValue("@AdjustmentAmt", pobj.AdjustmentAmt);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.AddWithValue("@ManagerRemark", pobj.ManagerRemark);
                sqlCmd.Parameters.AddWithValue("@CreditMemoType", pobj.CreditMemoType);
                sqlCmd.Parameters.AddWithValue("@ReferenceOrderAutoId", pobj.ReferenceOrderAutoId);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_ManagerSalesMemo
    {
        public static void insert(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 11;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void updateCredit(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 21;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void updateCredit1(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 24;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void delete(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 31;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void bindDropdown(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 41;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void BindProduct(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 48;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void SelectUnit(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 42;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void ValidateCreditMemo(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 43;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void select(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 44;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void bindStatus(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 45;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void EditCredit(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 46;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void BindCreditLog(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 47;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void ApprovedCredit(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 22;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void CreditComplete(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 23;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
        public static void PrintCredit(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 49;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }

        public static void CancelCreditMemo(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 50;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }

        public static void checkSecurity(PL_ManagerSalesMemo pobj)
        {
            pobj.Opcode = 51;
            DL_ManagerSalesMemo.ReturnTable(pobj);
        }
    }
}