﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllCategory
{
    public class PL_Category : Utility
    {
        public int CategoryAutoId { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public int SeqNo { get; set; }
        public int IsShow { get; set; }
    }
    public class DL_Category
    {
        public static void ReturnTable(PL_Category pobj)
        {
            try
            { 
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcCategoryMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@CategoryId", pobj.CategoryId);
                sqlCmd.Parameters.AddWithValue("@CategoryName", pobj.CategoryName);
                sqlCmd.Parameters.AddWithValue("@Description", pobj.Description);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@SeqNo", pobj.SeqNo);
                sqlCmd.Parameters.AddWithValue("@IsShow", pobj.IsShow);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch(Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_Category
    {
        public static void insert(PL_Category pobj)
        {
            pobj.Opcode = 11;
            DL_Category.ReturnTable(pobj);
        }
        public static void update(PL_Category pobj)
        {
            pobj.Opcode = 21;
            DL_Category.ReturnTable(pobj);
        }
        public static void delete(PL_Category pobj)
        {
            pobj.Opcode = 31;
            DL_Category.ReturnTable(pobj);
        }  
        public static void select(PL_Category pobj)
        {
            pobj.Opcode = 41;
            DL_Category.ReturnTable(pobj);
        }
        public static void editCategory(PL_Category pobj)
        {
            pobj.Opcode = 42;
            DL_Category.ReturnTable(pobj);
        }
    }
}
