﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DllUtility;

/// <summary>
/// Summary description for DLLErrorTicketLog
/// </summary>
namespace  DLLErrorTicketLog
{
    public class PL_ErrorTicketLog : Utility
	{
        public string Status { get; set; }
        public string DevoloperStatus { get; set; }
        public string Type { get; set; }
        public string Subject { get; set; }
        public string TicketID { get; set; }
        public string EmployeeType { get; set; }
        public string ByEmplyoyee { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }      
        
	}
    public class DL_ErrorTicketLog
    {
        public static void ReturnTable(PL_ErrorTicketLog pobj)
        {
            try
            {
                Config connect = new Config();
                //DataBaseUtilitys dobj = new DataBaseUtilitys();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("ProcErrorTicketLog", connect.con);

                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.Opcode;
                sqlCmd.Parameters.Add("@Type", SqlDbType.NVarChar, 50).Value = pobj.Type;
                sqlCmd.Parameters.Add("@Status", SqlDbType.NVarChar, 50).Value = pobj.Status;
                sqlCmd.Parameters.Add("@DeveloperStatus", SqlDbType.NVarChar).Value = pobj.DevoloperStatus;
                sqlCmd.Parameters.Add("@TicketID", SqlDbType.NVarChar).Value = pobj.TicketID;
                sqlCmd.Parameters.Add("@ByEmployee", SqlDbType.NVarChar).Value = pobj.ByEmplyoyee;
                sqlCmd.Parameters.Add("@EmployeeType", SqlDbType.NVarChar).Value = pobj.EmployeeType;
                sqlCmd.Parameters.Add("@Subject", SqlDbType.NVarChar).Value = pobj.Subject;
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_ErrorTicketLog
    {
        public static void bindDll(PL_ErrorTicketLog pobj)
        {
            pobj.Opcode = 41;
            DL_ErrorTicketLog.ReturnTable(pobj);
        }       
        public static void bingridall(PL_ErrorTicketLog pobj)
        {
            pobj.Opcode = 43;
            DL_ErrorTicketLog.ReturnTable(pobj);
        }      
    }
}