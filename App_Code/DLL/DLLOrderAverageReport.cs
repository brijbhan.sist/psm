﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DLLOrderAverageReport
/// </summary>
namespace DLLOrderAverageReport
{
    public class PL_OrderAverageReport : Utility
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int EmpType { get; set; }
        public int Employee { get; set; }
    }

    public class DL_OrderAverageReport
    {
        public static void ReturnTable(PL_OrderAverageReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand cmd = new SqlCommand("ProcOrderAverageReprot", connect.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                cmd.Parameters.AddWithValue("@Employee", pobj.Employee);
                cmd.Parameters.AddWithValue("@EmpType", pobj.EmpType);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                cmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(cmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = cmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_OrderAverageReport
    {
        public static void bindOrderAverageReport(PL_OrderAverageReport pobj)
        {
            pobj.Opcode = 41;
            DL_OrderAverageReport.ReturnTable(pobj);
        }
        public static void ExportOrderAverageReport(PL_OrderAverageReport pobj)
        {
            pobj.Opcode = 44;
            DL_OrderAverageReport.ReturnTable(pobj);
        }
        public static void GetEmployeeType(PL_OrderAverageReport pobj)
        {
            pobj.Opcode = 42;
            DL_OrderAverageReport.ReturnTable(pobj);
        }
        public static void GetEmployee(PL_OrderAverageReport pobj)
        {
            pobj.Opcode = 43;
            DL_OrderAverageReport.ReturnTable(pobj);
        }
    }
}