﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data;
using System.Data.SqlClient;

namespace DllProductBulkUpload
{
    public class PL_ProductBulkUpload : Utility
    {
        public DataTable TableValue { get; set; }
        public string ProductId { get; set; }
        public string  Category { get; set; }
        public string Subcategory { get; set; }
        public string ProductName { get; set; }
        public string Location { get; set; }
        public string Taxable { get; set; }
        public string TaxRate { get; set; }
        public string P_Barcode { get; set; }
        public string P_MinPrice { get; set; }
        public string P_CostPrice { get; set; }
        public string P_BasePrice { get; set; }
        public string P_SRP { get; set; }
        public string P_CommCode { get; set; }
        public string B_NoOfPieces { get; set; }
        public string B_Barcode { get; set; }
        public string B_MinPrice { get; set; }
        public string B_CostPrice { get; set; }
        public string B_BasePrice { get; set; }
        public string B_SRP { get; set; }
        public string B_CommCode { get; set; }
        public string C_NoOfPieces { get; set; }
        public string C_Barcode { get; set; }
        public string C_MinPrice { get; set; }
        public string C_CostPrice { get; set; }
        public string C_BasePrice { get; set; }
        public string C_SRP { get; set; }
        public string C_CommCode { get; set; }
    }

    public class DL_ProductBulkUpload
    {
        public static void ReturnTable(PL_ProductBulkUpload obj)
        {
            try
            {
                Config con = new Config();
                SqlCommand cmd = new SqlCommand("ProcProductBulkUpload", con.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", obj.Opcode);
                cmd.Parameters.AddWithValue("@TableValue", obj.TableValue);
                cmd.Parameters.AddWithValue("@ProductId", obj.ProductId);
                cmd.Parameters.AddWithValue("@Category", obj.Category);
                cmd.Parameters.AddWithValue("@Subcategory", obj.Subcategory);
                cmd.Parameters.AddWithValue("@ProductName", obj.ProductName);
                cmd.Parameters.AddWithValue("@Location", obj.Location);
                cmd.Parameters.AddWithValue("@Taxable", obj.Taxable);
                cmd.Parameters.AddWithValue("@TaxRate", obj.TaxRate);
                cmd.Parameters.AddWithValue("@P_Barcode", obj.P_Barcode);
                cmd.Parameters.AddWithValue("@P_MinPrice", obj.P_MinPrice);
                cmd.Parameters.AddWithValue("@P_CostPrice", obj.P_CostPrice);
                cmd.Parameters.AddWithValue("@P_BasePrice", obj.P_BasePrice);
                cmd.Parameters.AddWithValue("@P_SRP", obj.P_SRP);
                cmd.Parameters.AddWithValue("@P_CommCode", obj.P_CommCode);
                cmd.Parameters.AddWithValue("@B_Qty", obj.B_NoOfPieces);
                cmd.Parameters.AddWithValue("@B_Barcode", obj.B_Barcode);
                cmd.Parameters.AddWithValue("@B_MinPrice", obj.B_MinPrice);
                cmd.Parameters.AddWithValue("@B_CostPrice", obj.B_CostPrice);
                cmd.Parameters.AddWithValue("@B_BasePrice", obj.B_BasePrice);
                cmd.Parameters.AddWithValue("@B_SRP", obj.B_SRP);
                cmd.Parameters.AddWithValue("@B_CommCode", obj.B_CommCode);
                cmd.Parameters.AddWithValue("@C_Qty", obj.C_NoOfPieces);
                cmd.Parameters.AddWithValue("@C_Barcode", obj.C_Barcode);
                cmd.Parameters.AddWithValue("@C_MinPrice", obj.C_MinPrice);
                cmd.Parameters.AddWithValue("@C_CostPrice", obj.C_CostPrice);
                cmd.Parameters.AddWithValue("@C_BasePrice", obj.C_BasePrice);
                cmd.Parameters.AddWithValue("@C_SRP", obj.C_SRP);
                cmd.Parameters.AddWithValue("@C_CommCode", obj.C_CommCode);

                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(cmd);
                obj.Ds = new DataSet();
                sqlAdp.Fill(obj.Ds);

                obj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                obj.exceptionMessage = cmd.Parameters["@exceptionMessage"].Value.ToString();              
            }
            catch (Exception ex)
            {
                obj.isException = true;
                obj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_ProductBulkUpload
    {
        public static void insertExcelData(PL_ProductBulkUpload obj)
        {
            obj.Opcode = 11;
            DL_ProductBulkUpload.ReturnTable(obj);
        }
        public static void selectExcelData(PL_ProductBulkUpload obj)
        {
            obj.Opcode = 41;
            DL_ProductBulkUpload.ReturnTable(obj);
        }
        public static void FinalUpload(PL_ProductBulkUpload obj)
        {
            obj.Opcode = 12;
            DL_ProductBulkUpload.ReturnTable(obj);
        }
    }
}
