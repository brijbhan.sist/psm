﻿using System;
using DllUtility;
using System.Data.SqlClient;
using System.Data;


namespace DllPackingDetailUpdateLog
{
    public class PL_PackingDetails : Utility
    {
        public int ProductAutoId { get; set; }
    }

    public class DL_PackingDetails
    {
        public static void ReturnTable(PL_PackingDetails pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("PackingDetailUpdateLog", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_PackingDetails
    {

        public static void bindProduct(PL_PackingDetails pobj)
        {
            pobj.Opcode = 41;
            DL_PackingDetails.ReturnTable(pobj);
        }
        public static void select(PL_PackingDetails pobj)
        {
            pobj.Opcode = 44;
            DL_PackingDetails.ReturnTable(pobj);
        }

    }
}
