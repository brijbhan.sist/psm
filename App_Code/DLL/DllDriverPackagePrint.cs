﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DllDriverPackagePrint
/// </summary>
namespace DllDriverPackagePrint
{
    public class PL_DriverPackagePrint : Utility
    {
        public int OrderAutoId { get; set; }
        public string BulkOrderAutoId { get; set; }
        public string OrderNo { get; set; }
        public int LoginEmpType { get; set; }
        public int EmpAutoId { get; set; }
        public int DrvAutoId { get; set; }
        public DateTime AsgnDate { get; set; }
        public int CreditAutoId { get; set; }
        
    }

    public class DL_DriverPackagePrint
    {
        public static void ReturnTable(PL_DriverPackagePrint pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcDriverPackagePrint", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@BulkOrderAutoId", pobj.BulkOrderAutoId);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
             
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
               
                sqlCmd.Parameters.AddWithValue("@DriverAutoId", pobj.DrvAutoId);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@CreditAutoId", pobj.CreditAutoId);

                if (pobj.AsgnDate != DateTime.MinValue && pobj.AsgnDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@AsgnDate", pobj.AsgnDate);
                }
               
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                //sqlCmd.Parameters["@DraftAutoId"].Direction = ParameterDirection.InputOutput;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later";
            }
        }
    }

    public class BL_DriverPackagePrint
    {
        public static void PrintReadytoShipOrder(PL_DriverPackagePrint pobj)
        {
            pobj.Opcode = 101;
            DL_DriverPackagePrint.ReturnTable(pobj);
        }
        public static void getBulkOrderData(PL_DriverPackagePrint pobj)
        {
            pobj.Opcode = 102;
            DL_DriverPackagePrint.ReturnTable(pobj);
        }
        public static void PrintOrderTemplate1(PL_DriverPackagePrint pobj)
        {
            pobj.Opcode = 103;
            DL_DriverPackagePrint.ReturnTable(pobj);
        } 
        public static void getAllOrderData(PL_DriverPackagePrint pobj)
        {
            pobj.Opcode = 104;
            DL_DriverPackagePrint.ReturnTable(pobj);
        }
        public static void GetPackingOrderPrint(PL_DriverPackagePrint pobj)
        {
            pobj.Opcode = 105;
            DL_DriverPackagePrint.ReturnTable(pobj);
        }
        public static void PrintCredit(PL_DriverPackagePrint pobj)
        {
            pobj.Opcode = 106;
            DL_DriverPackagePrint.ReturnTable(pobj);
        }
        public static void getBulkOrderDataTempate2(PL_DriverPackagePrint pobj)
        {
            pobj.Opcode = 107;
            DL_DriverPackagePrint.ReturnTable(pobj);
        }
    }
}




