﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Net.Http;

namespace DllUtility
{
    public class Utility
    {
        public int Opcode { get; set; }
        public DataSet Ds;
        public string exceptionMessage { get; set; }
        public bool isException { get; set; }
        public int PageIndex { get; set; }
        public int PageSize = 10;
        public bool RecordCount { get; set; }
        public string IPAddress { get; set; }
        public int Who { get; set; }
    }
    public class Config
    {
        public SqlConnection con;
        public Config()
        {
            string InitialCatalog;
            string conString;

            if (HttpContext.Current.Session["UserName"] != null)
            {
                string[] keys = HttpContext.Current.Session["UserName"].ToString().Split('@');
                string appId = "";
                if (keys.Length > 1)
                {
                    appId = keys[1];
                }
                InitialCatalog = appId;
            }
            else
            {
                InitialCatalog =   HttpContext.Current.Session["Location"].ToString();
            }

            if (ConfigurationManager.AppSettings["Mode"] == "TestMode")
            {
                conString = @"Data Source=LAPTOP13\SQLEXPRESS;Initial Catalog=" + InitialCatalog + ".a1whm.com;integrated security=true;connect timeout=1800000;";
            }
            else
            {
                conString = @"Data Source=LAPTOP13\SQLEXPRESS;Initial Catalog=" + InitialCatalog + ".a1whm.com;integrated security=true;connect timeout=1800000;";
            }

            con = new SqlConnection(conString);
        }

    }
    public class gZipCompression
    {
        public static void fn_gZipCompression()
        {
            HttpResponse Response = HttpContext.Current.Response;
            string AcceptEncoding = HttpContext.Current.Request.Headers["Accept-Encoding"];
            if (AcceptEncoding.Contains("gzip"))
            {
                Response.Filter = new System.IO.Compression.GZipStream(Response.Filter,
                                          System.IO.Compression.CompressionMode.Compress);
                Response.AppendHeader("Content-Encoding", "gzip");
            }
            else
            {
                Response.Filter = new System.IO.Compression.DeflateStream(Response.Filter,
                                          System.IO.Compression.CompressionMode.Compress);
                Response.AppendHeader("Content-Encoding", "deflate");
            }
        }
    }
}
