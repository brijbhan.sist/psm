﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DllUtility;

/// <summary>
/// Summary description for DLLProductCatalogueReport
/// </summary>
namespace DLLProductCatalogueReport
{
    public class PL_ProductCatalogueReport : Utility
	{
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public int ProductAutoId { get; set; }
        public int CategoryAutoId { get; set; }
        public int SubCategoryAutoId { get; set; }       
	}
    public class DL_ProductCatalogueReport
    {
        public static void ReturnTable(PL_ProductCatalogueReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcProductCatalogueReport", connect.con);
                sqlCmd.CommandTimeout = 100000;
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@ProductId", pobj.ProductId);
                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductName", pobj.ProductName);
                sqlCmd.Parameters.AddWithValue("@SubCategoryAutoId", pobj.SubCategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                
                

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                //sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_ProductCatalogueReport
    {
        public static void selectCategory(PL_ProductCatalogueReport pobj)
        {
            pobj.Opcode = 41;
            DL_ProductCatalogueReport.ReturnTable(pobj);
        }
        public static void selectSubCategory(PL_ProductCatalogueReport pobj)
        {
            pobj.Opcode = 42;
            DL_ProductCatalogueReport.ReturnTable(pobj);
        }
        //public static void selectProduct(PL_ProductCatalogueReport pobj)
        //{
        //    pobj.Opcode = 43;
        //    DL_ProductCatalogueReport.ReturnTable(pobj);
        //}
        public static void GetProductCatelogReportList(PL_ProductCatalogueReport pobj)
        {
            pobj.Opcode = 45;
            DL_ProductCatalogueReport.ReturnTable(pobj);
        }
    }
}