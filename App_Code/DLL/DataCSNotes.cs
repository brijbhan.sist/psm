﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DllUtility;
/// <summary>
/// Summary description for DataCSLog
/// </summary>
public class DataCSNotes
{
    public static void returnTable(PropertyCSNotes pobj)
    {
        Config connect = new Config();
        SqlCommand sqlCmd = new SqlCommand("ProcCSNotesLog", connect.con);
        sqlCmd.CommandType = CommandType.StoredProcedure;
        sqlCmd.Parameters.Add("@OpCode", SqlDbType.Int).Value = pobj.OpCode;

        sqlCmd.Parameters.Add("@EmpId", SqlDbType.NVarChar).Value = pobj.EmpId;
        sqlCmd.Parameters.Add("@CustId", SqlDbType.NVarChar).Value = pobj.CustId;
        sqlCmd.Parameters.Add("@FromPage", SqlDbType.NVarChar).Value = pobj.FromPage;
        sqlCmd.Parameters.Add("@RefrenceNo", SqlDbType.NVarChar).Value = pobj.RefrenceNo;
        sqlCmd.Parameters.Add("@OrderNo", SqlDbType.NVarChar).Value = pobj.OrderNo;
        sqlCmd.Parameters.Add("@Notes", SqlDbType.NVarChar).Value = pobj.Notes;
        sqlCmd.Parameters.Add("@Who", SqlDbType.NVarChar).Value = pobj.Who;

        sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
        sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;

        sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.NVarChar, 500);
        sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

        SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
        pobj.Dt = new DataTable();
        sqlAdp.Fill(pobj.Dt);

        pobj.IsException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
        pobj.ExceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
    }
}