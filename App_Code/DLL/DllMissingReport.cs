﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLDeviceMaster
/// </summary>
namespace DllMissingReport
{
    public class Pl_MissingReport : Utility
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int SalesPerson { get; set; }
        public int CustomerAutoId { get; set; }
        public int CategoryAutoId { get; set; }
        public int SubCategoryAutoId { get; set; }


    }

    public class DL_MissingReport
    {
        public static void ReturnTable(Pl_MissingReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcMissingReport", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@SubCategoryAutoId", pobj.SubCategoryAutoId);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }

    }

    public class BL_MissingReport
    {
        public static void GetMissingReport(Pl_MissingReport pobj)
        {
            pobj.Opcode = 41;
            DL_MissingReport.ReturnTable(pobj);
        }
        public static void bindDropDown(Pl_MissingReport pobj)
        {
            pobj.Opcode = 42;
            DL_MissingReport.ReturnTable(pobj);
        }
        public static void bindCustomer(Pl_MissingReport pobj)
        {
            pobj.Opcode = 44;
            DL_MissingReport.ReturnTable(pobj);
        }
        public static void SubCategory(Pl_MissingReport pobj)
        {
            pobj.Opcode = 43;
            DL_MissingReport.ReturnTable(pobj);
        }
    }
}