﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DLLPrintAllOrder
/// </summary>
namespace DLLPrintAllOrder
{
	public class PL_PrintAllOrder:Utility
	{
        public int OrderAutoId { get; set; }
        public int EndAutoId { get; set; }
	}
    public class DL_PrintAllOrder
    {
        public static void ReturnOrder(PL_PrintAllOrder pobj)
        {
            try
            {
                Config connect = new Config();

                SqlCommand sqlCmd = new SqlCommand("Proc_DayEndPrint", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.AddWithValue("@OpCode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@EndAutoId", pobj.EndAutoId);

                //sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                //sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                //sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                //sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                //pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                //pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_PrintAllOrder
    {
        public static void GetAllOrderID(PL_PrintAllOrder pobj)
        {
            pobj.Opcode = 41;
            DL_PrintAllOrder.ReturnOrder(pobj);
        }
        public static void PrintAllOrder(PL_PrintAllOrder pobj)
        {
            pobj.Opcode = 42;
            DL_PrintAllOrder.ReturnOrder(pobj);
        }
    }
}