﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DllUtility;

/// <summary>
/// Summary description for DLLInvReceiverPO
/// </summary>
namespace DLLInvReceiverPO
{
    public class PL_RecPO : Utility
    {
        public int PoAutoId { get; set; }
        public string POVNumber { get; set; }
        public int ProductId { get; set; }
        public int ProductAutoId { get; set; }
        public string Remark { get; set; }
        public string ReceiverRemark { get; set; }
        public int Status { get; set; }
        public int EmployeeId { get; set; }
        public int RequiredQty { get; set; }
        public int UnitAutoId { get; set; }
        public int QtyPerUnit { get; set; }
        public int POVDraftAutoId { get; set; }
        public int POVRemarks { get; set; }
        public int ReveivedQty { get; set; }
        public int VendorId { get; set; }
        public int RecAutoId { get; set; }
        public string Barcode { get; set; }
        public string Draftitems { get; set; }
        public DateTime POFromDate { get; set; }
        public DateTime POToDate { get; set; }
        public string PODraftId { get; set; }
        public decimal Price { get; set; }
        public string @RecId { get; set; }
    }
    public class DL_RecPO
    {
        public static void ReturnTable(PL_RecPO pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcInvRecPO", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@EmpId", pobj.EmployeeId);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@POAutoId", pobj.PoAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@QtyPerUnit", pobj.QtyPerUnit);
                sqlCmd.Parameters.AddWithValue("@RequiredQty", pobj.RequiredQty);
                sqlCmd.Parameters.AddWithValue("@ReceivedQty", pobj.ReveivedQty);
                sqlCmd.Parameters.AddWithValue("@POVDraftAutoId", pobj.POVDraftAutoId);
                sqlCmd.Parameters.AddWithValue("@VendorId", pobj.VendorId);
                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);
                sqlCmd.Parameters.AddWithValue("@POVNumber", pobj.POVNumber);
                sqlCmd.Parameters.AddWithValue("@Price", pobj.Price);
                sqlCmd.Parameters.AddWithValue("@PODraftId", pobj.PODraftId);
                sqlCmd.Parameters.AddWithValue("@RecAutoId", pobj.RecAutoId);
                sqlCmd.Parameters.AddWithValue("@RecId", pobj.RecId);
                if (pobj.POFromDate > DateTime.MinValue && pobj.POFromDate < DateTime.MaxValue)
                    sqlCmd.Parameters.AddWithValue("@POFromDate", pobj.POFromDate);

                if (pobj.POToDate > DateTime.MinValue && pobj.POToDate < DateTime.MaxValue)
                    sqlCmd.Parameters.AddWithValue("@POToDate", pobj.POToDate);

                sqlCmd.Parameters.AddWithValue("@Draftitems", pobj.Draftitems);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);//ReceiverRemark
                sqlCmd.Parameters.AddWithValue("@Remark", pobj.Remark);
                sqlCmd.Parameters.AddWithValue("@ReceiverRemark", pobj.ReceiverRemark);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                string Beforefill = System.DateTime.Now.ToString();
                sqlAdp.Fill(pobj.Ds);
                string Afterfill = System.DateTime.Now.ToString();
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_RecPO
    {
        public static void ReceiveStockList(PL_RecPO pobj)
        {
            pobj.Opcode = 401;
            DL_RecPO.ReturnTable(pobj);
        }
        public static void ReceieveDetails(PL_RecPO pobj)
        {
            pobj.Opcode = 402;
            DL_RecPO.ReturnTable(pobj);
        }
        public static void ReceiveStockListInvManager(PL_RecPO pobj)
        {
            pobj.Opcode = 403;
            DL_RecPO.ReturnTable(pobj);
        }
        public static void ReceiveListByPO(PL_RecPO pobj)
        {
            pobj.Opcode = 404;
            DL_RecPO.ReturnTable(pobj);
        }
    }
}