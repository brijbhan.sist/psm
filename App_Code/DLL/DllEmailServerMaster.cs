﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DllEmailServerMaster
/// </summary>
namespace DllEmailServerMaster
{
    public class Pl_EmailServer : Utility
    {
        public string UserName { get; set; }
        public string EmailID { get; set; }
        public string Server { get; set; }
        public int Port { get; set; }
        public string Password { get; set; }
        public bool SSL { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }

        public string ToMailID { get; set; }
        public string BCC { get; set; }
        public string CC { get; set; }
        public string Subject { get; set; }
        public string body { get; set; }

    }
    public class Dl_EmailServer
    {
        public static void ReturnTable(Pl_EmailServer pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcEmailServerMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.AddWithValue("@opCode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@SendTo", pobj.UserName);
                sqlCmd.Parameters.AddWithValue("@EmailID", pobj.EmailID);
                sqlCmd.Parameters.AddWithValue("@Password", pobj.Password);
                sqlCmd.Parameters.AddWithValue("@Server", pobj.Server);
                sqlCmd.Parameters.AddWithValue("@Port", pobj.Port);
                sqlCmd.Parameters.AddWithValue("@SSL", pobj.SSL);
                sqlCmd.Parameters.AddWithValue("@Subject", pobj.Subject);
                sqlCmd.Parameters.AddWithValue("@EmailBody", pobj.body);



                if (!string.IsNullOrEmpty(pobj.ToMailID))
                {
                    if (pobj.ToMailID.EndsWith(","))
                    {
                        pobj.ToMailID = pobj.ToMailID.Substring(0, pobj.ToMailID.Length - 1);
                        sqlCmd.Parameters.AddWithValue("@ToEmailId", pobj.ToMailID);
                    }

                }
                if (!string.IsNullOrEmpty(pobj.BCC))
                {
                    if (pobj.BCC.EndsWith(","))
                    {
                        pobj.BCC = pobj.BCC.Substring(0, pobj.BCC.Length - 1);
                        sqlCmd.Parameters.AddWithValue("@BCC", pobj.BCC);
                    }

                }
                if (!string.IsNullOrEmpty(pobj.CC))
                {
                    if (pobj.CC.EndsWith(","))
                    {
                        pobj.CC = pobj.CC.Substring(0, pobj.CC.Length - 1);
                        sqlCmd.Parameters.AddWithValue("@CC", pobj.CC);

                    }
                }

                sqlCmd.Parameters.AddWithValue("@CreatedBy", pobj.CreatedBy);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }

        }
    }
    public class Bl_EmailServer
    {
        public static void select(Pl_EmailServer pobj)
        {
            pobj.Opcode = 61;
            Dl_EmailServer.ReturnTable(pobj);
        }
        public static void update(Pl_EmailServer pobj)
        {
            pobj.Opcode = 21;
            Dl_EmailServer.ReturnTable(pobj);
        }
        public static void insert(Pl_EmailServer pobj)
        {
            pobj.Opcode = 11;
            Dl_EmailServer.ReturnTable(pobj);
        }
        public static void selectServerDetails(Pl_EmailServer pobj)
        {
            pobj.Opcode = 43;
            Dl_EmailServer.ReturnTable(pobj);
        }
        public static void selectEmailId(Pl_EmailServer pobj)
        {
            pobj.Opcode = 61;
            Dl_EmailServer.ReturnTable(pobj);
        }
        public static void sendEmailDataDb(Pl_EmailServer pobj)
        {
            pobj.Opcode = 62;
            Dl_EmailServer.ReturnTable(pobj);
        }
    }

}