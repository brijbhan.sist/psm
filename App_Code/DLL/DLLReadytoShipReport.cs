﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLReadytoShipReport
/// </summary>
/// 
namespace DLLReadytoShipReport
{
    public class PL_ReadytoShipReport : Utility
    {  
    }
    public class BL_ReadytoShipReport
    {

      
        public static void ReadytoShipReport(PL_ReadytoShipReport pobj)
        {
            pobj.Opcode = 41;
            DL_ReadytoShipReport.ReturnData(pobj);
        } 
    }

    public class DL_ReadytoShipReport
    {
        public static void ReturnData(PL_ReadytoShipReport pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[ProcReadytoShipReport]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);  
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
}