﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DLLPayReceiveMaster
{
    public class PL_PayReceiveMaster : Utility
    {
        public int PayAutoId { get; set; }
        public string PayId { get; set; }
        public int CustomerAutoId { get; set; }
        public int EmpAutoId { get; set; }
        public int LoginEmpType { get; set; }
        public DateTime ReceiveDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal ReceiveAmount { get; set; }
        public string ReferenceId { get; set; }
        public int PaymentMode { get; set; }
        public int Status { get; set; }
        public string Remarks { get; set; }
        public string OrderAutoId { get; set; }
    }
    public class DL_PayReceiveMaster
    {
        public static void ReturnTable(PL_PayReceiveMaster pobj)
        {
            try
            { 
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPayReceiveMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@PayAutoId", pobj.PayAutoId);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@ReceiveAmount", pobj.ReceiveAmount);
                sqlCmd.Parameters.AddWithValue("@PaymentMode", pobj.PaymentMode);
                sqlCmd.Parameters.AddWithValue("@ReferenceId", pobj.ReferenceId);
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                sqlCmd.Parameters.AddWithValue("@PayId", pobj.PayId);
                if (pobj.ReceiveDate > DateTime.MinValue && pobj.ReceiveDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ReceiveDate", pobj.ReceiveDate);
                }
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                } 
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch(Exception)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }
    }
    public class BL_PayReceiveMaster
    {
        public static void insert(PL_PayReceiveMaster pobj)
        {
            pobj.Opcode = 11;
            DL_PayReceiveMaster.ReturnTable(pobj);
        }
        public static void update(PL_PayReceiveMaster pobj)
        {
            pobj.Opcode = 21;
            DL_PayReceiveMaster.ReturnTable(pobj);
        }
        public static void delete(PL_PayReceiveMaster pobj)
        {
            pobj.Opcode = 31;
            DL_PayReceiveMaster.ReturnTable(pobj);
        }  
        public static void select(PL_PayReceiveMaster pobj)
        {
            pobj.Opcode = 41;
            DL_PayReceiveMaster.ReturnTable(pobj);
        }
        public static void editPay(PL_PayReceiveMaster pobj)
        {
            pobj.Opcode = 42;
            DL_PayReceiveMaster.ReturnTable(pobj);
        }
        public static void bindStatus(PL_PayReceiveMaster pobj)
        {
            pobj.Opcode = 43;
            DL_PayReceiveMaster.ReturnTable(pobj);
        }
        public static void ViewDueAmount(PL_PayReceiveMaster pobj)
        {
            pobj.Opcode = 44;
            DL_PayReceiveMaster.ReturnTable(pobj);
        }
    }
}
