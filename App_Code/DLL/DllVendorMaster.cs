﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllVendorMaster
{
    public class PL_VendorMaster : Utility
    {
        public int AutoId { get; set; }
        public int EmpAutoId { get; set; }
        public string Location { get; set; }
        public int LocationTypeAutoId { get; set; }
        public int LocationAutoId { get; set; }
        public string VendorId { get; set; }        
        public string VendorName { get; set; }
        public string Address { get; set; }
        public int Country { get; set; }
        public int State { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string ContactPerson { get; set; }
        public string Cell { get; set; }
        public string Office1 { get; set; }
        public string Office2 { get; set; }
        public string Email { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
    }

    public class DL_VendorMaster
    {
        public static void ReturnTable(PL_VendorMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcVendorMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@VendorId", pobj.VendorId);
                sqlCmd.Parameters.AddWithValue("@VendorName", pobj.VendorName);
                sqlCmd.Parameters.AddWithValue("@Address", pobj.Address);
                sqlCmd.Parameters.AddWithValue("@Country", pobj.Country);
                sqlCmd.Parameters.AddWithValue("@State", pobj.State);
                sqlCmd.Parameters.AddWithValue("@City", pobj.City);
                sqlCmd.Parameters.AddWithValue("@Zipcode", pobj.Zipcode);
                sqlCmd.Parameters.AddWithValue("@ContactPerson", pobj.ContactPerson);
                sqlCmd.Parameters.AddWithValue("@Cell", pobj.Cell);
                sqlCmd.Parameters.AddWithValue("@Email", pobj.Email);
                sqlCmd.Parameters.AddWithValue("@Office1", pobj.Office1);
                sqlCmd.Parameters.AddWithValue("@Office2", pobj.Office2);                
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@Location", pobj.Location);
                sqlCmd.Parameters.AddWithValue("@LocationName", pobj.LocationAutoId);
                sqlCmd.Parameters.AddWithValue("@LocationType", pobj.LocationTypeAutoId);
                sqlCmd.Parameters.AddWithValue("@Type", pobj.Type);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);

                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_VendorMaster
    {
        public static void insert(PL_VendorMaster pobj)
        {
            pobj.Opcode = 11;
            DL_VendorMaster.ReturnTable(pobj);
        }
        public static void update(PL_VendorMaster pobj)
        {
            pobj.Opcode = 21;
            DL_VendorMaster.ReturnTable(pobj);
        }
        public static void delete(PL_VendorMaster pobj)
        {
            pobj.Opcode = 31;
            DL_VendorMaster.ReturnTable(pobj);
        }
        public static void selectVendorRecord(PL_VendorMaster pobj)
        {
            pobj.Opcode = 41;
            DL_VendorMaster.ReturnTable(pobj);
        }
        public static void bindDropDown(PL_VendorMaster pobj)
        {
            pobj.Opcode = 42;
            DL_VendorMaster.ReturnTable(pobj);
        }
        public static void editVendorRecord(PL_VendorMaster pobj)
        {
            pobj.Opcode = 43;
            DL_VendorMaster.ReturnTable(pobj);
        }

        public static void bindVendor(PL_VendorMaster pobj)
        {
            pobj.Opcode = 44;
            DL_VendorMaster.ReturnTable(pobj);
        }
        public static void SelectStateCity(PL_VendorMaster pobj)
        {
            pobj.Opcode = 45;
            DL_VendorMaster.ReturnTable(pobj);
        }
        public static void bindLocation(PL_VendorMaster pobj)
        {
            pobj.Opcode = 46;
            DL_VendorMaster.ReturnTable(pobj);
        }
    }
}
