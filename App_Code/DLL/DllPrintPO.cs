﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllPrintPO
{
    public class PL_PrintPO : Utility
    {
        public int PoAutoId { get; set; }
        public int type { get; set; }
    }
    public class DL_PrintPO
    {
        public static void ReturnTable(PL_PrintPO pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPrintPurchaseOrder", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@PoAutoId", pobj.PoAutoId);
                sqlCmd.Parameters.AddWithValue("@type", pobj.type);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }
    }
    public class BL_PrintPO
    {
        public static void PrintPurchaseOrder(PL_PrintPO pobj)
        {
            pobj.Opcode = 401;
            DL_PrintPO.ReturnTable(pobj);
        }  
    }
}
