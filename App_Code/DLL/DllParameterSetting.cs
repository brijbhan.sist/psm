﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DllUtility;
using System.Data;
/// <summary>
/// Summary description for DllParameterSetting
/// </summary>

namespace DllParameterSetting
{
    public class PL_ParameterSetting : Utility
    {
        public int CityId { get; set; }
        public int Stateid { get; set; }
        public int ZipId { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Abbreviation { get; set; }
        public int Status { get; set; }
    }
    public class DL_ParameterSetting
    {
        public static void ReturnTable(PL_ParameterSetting pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProParameterSetting", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CityId", pobj.CityId);
                sqlCmd.Parameters.AddWithValue("@StateId", pobj.Stateid);
                sqlCmd.Parameters.AddWithValue("@ZipId", pobj.ZipId);
                sqlCmd.Parameters.AddWithValue("@StateName", pobj.State);
                sqlCmd.Parameters.AddWithValue("@Abbreviation", pobj.Abbreviation);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@CityName", pobj.City);
                sqlCmd.Parameters.AddWithValue("@ZipCode", pobj.Zip);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_ParameterSetting
    {
        public static void Insert(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 11;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void edit(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 42;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void delete (PL_ParameterSetting pobj)
        {
            pobj.Opcode = 31;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void update(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 21;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void select(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 41;
            DL_ParameterSetting.ReturnTable(pobj);
        }
       
        //here start to city functions

        public static void bindActiveState(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 45;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void bindSearchState_City(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 44;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void insertData_City(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 12;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void tblbindActiveState_City(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 43;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void deleteCity(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 32;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void editCity(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 46;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void updateCity(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 22;
            DL_ParameterSetting.ReturnTable(pobj);
        }

        //here start to Zip functions

        public static void bindActiveCity_Zip(PL_ParameterSetting pobj)
        {
             pobj.Opcode = 47;
             DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void getZipDetail(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 48;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void insertData_Zip(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 13;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void tblbind_Zip(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 48;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void deleteZip(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 33;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void editZip(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 49;
            DL_ParameterSetting.ReturnTable(pobj);
        }
        public static void updateZip(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 23;
            DL_ParameterSetting.ReturnTable(pobj);
        }
       public static void binedState_Zip(PL_ParameterSetting pobj)
        {
            pobj.Opcode = 410;
            DL_ParameterSetting.ReturnTable(pobj);
        }
    }
     
}