﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
 
namespace DllSubcategory
{
    public class PL_Subcategory : Utility
    {
        public int SubcategoryAutoId { get; set; }
        public int CategoryAutoId { get; set; }
        public string SubcategoryId { get; set; }
        public string SubcategoryName { get; set; }
        public string Description { get; set; }
        public int SeqNo { get; set; }
        public int IsShow { get; set; }
        public int SubcategoryTax { get; set; }
        public int Status { get; set; }
    }
    public class DL_Subcategory
    { 
        public static void ReturnTable(PL_Subcategory pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcSubCategoryMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@SubcategoryAutoId", pobj.SubcategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@SubcategoryId", pobj.SubcategoryId);
                sqlCmd.Parameters.AddWithValue("@SubcategoryTax", pobj.SubcategoryTax);
                sqlCmd.Parameters.AddWithValue("@SubcategoryName", pobj.SubcategoryName);
                sqlCmd.Parameters.AddWithValue("@Description", pobj.Description);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@SeqNo", pobj.SeqNo);
                sqlCmd.Parameters.AddWithValue("@IsShow", pobj.IsShow);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }  
        } 
    }
    public class BL_Subcategory
    {
        public static void insert(PL_Subcategory pobj)
        {
            pobj.Opcode = 11;
            DL_Subcategory.ReturnTable(pobj);
        }
        public static void update(PL_Subcategory pobj)
        {
            pobj.Opcode = 21;
            DL_Subcategory.ReturnTable(pobj);
        }
        public static void delete(PL_Subcategory pobj)
        {
            pobj.Opcode = 31;
            DL_Subcategory.ReturnTable(pobj);
        }
        public static void select(PL_Subcategory pobj)
        {
            pobj.Opcode = 41;
            DL_Subcategory.ReturnTable(pobj);
        }
        public static void editSubcategory(PL_Subcategory pobj)
        {
            pobj.Opcode = 42;
            DL_Subcategory.ReturnTable(pobj);
        }
        public static void selectCategory(PL_Subcategory pobj)
        {
            pobj.Opcode = 43;
            DL_Subcategory.ReturnTable(pobj);
        }
    }

}
