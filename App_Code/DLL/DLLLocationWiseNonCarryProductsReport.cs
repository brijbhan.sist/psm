﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
using System.Web;

namespace DLLLocationWiseNonCarryProductsReport
{
    public class PL_NonCarryProductsReport : Utility
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public int CategoryAutoId { get; set; }
        public int Month { get; set; }
        public int SubCategoryAutoId { get; set; }
        public int BrandAutoId { get; set; }
        public string OrderAutoIds { get; set; }

    }

    public class DL_NonCarryProductsReport
    {
        public static void ReturnTable(PL_NonCarryProductsReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcLocationWiseNonCarryProductsReport", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@ProductId", pobj.ProductId);
                sqlCmd.Parameters.AddWithValue("@ProductName", pobj.ProductName);
                sqlCmd.Parameters.AddWithValue("@CategoryAutoId", pobj.CategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@SubCategoryAutoId", pobj.SubCategoryAutoId);
                sqlCmd.Parameters.AddWithValue("@BrandAutoId", pobj.BrandAutoId);
                sqlCmd.Parameters.AddWithValue("@Month", pobj.Month);
                sqlCmd.Parameters.AddWithValue("@OrderAutoIds", pobj.OrderAutoIds);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_NonCarryProductsReport
    {

        public static void Getreport(PL_NonCarryProductsReport pobj)
        {
            pobj.Opcode = 41;
            DL_NonCarryProductsReport.ReturnTable(pobj);
        }
        public static void bindCategory(PL_NonCarryProductsReport pobj)
        {
            pobj.Opcode = 42;
            DL_NonCarryProductsReport.ReturnTable(pobj);
        }
        public static void bindSubcategory(PL_NonCarryProductsReport pobj)
        {
            pobj.Opcode = 43;
            DL_NonCarryProductsReport.ReturnTable(pobj);
        }
        public static void InactiveProduct(PL_NonCarryProductsReport pobj)
        {
            pobj.Opcode = 44;
            DL_NonCarryProductsReport.ReturnTable(pobj);
        }
    }
}
