﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PropertyEmailReceiverMaster
/// </summary>
public class PropertyEmailServerMaster : PropertyUtilityAbstract
{
    private string emailid;

    public string EmailID
    {
        get { return emailid; }
        set { emailid = value; }
    }
    public string UserName { get; set; }
    public int Port { get; set; }
    public string Server { get; set; }
    public string Password { get; set; }
    public bool SSL { get; set; }


    public string RefID { get; set; }
    public string Who { get; set; }
    public string RefName { get; set; }
    public bool Status { get; set; }
    public DateTime CreationDate { get; set; }

}