﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DLLMLQtyReport
/// </summary>
/// 
namespace DLLMLQtyReport
{
    public class PL_MLQtyReport : Utility
    {
        public int CustomerAutoId { get; set; }
        public int ProductAutoId { get; set; }
        public int StateAutoId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
    public class BL_MLQtyReport
    {
        public static void getReport(PL_MLQtyReport pobj)
        {
            DL_MLQtyReport.ReturnData(pobj);
        }
        public static void GetMlqtyExportReport(PL_MLQtyReport pobj)
        {
            DL_MLQtyReport.ReturnData(pobj);
        }
        public static void GetMlqtySummaryExportReport(PL_MLQtyReport pobj)
        {
            DL_MLQtyReport.ReturnData(pobj);
        }
    }

    public class DL_MLQtyReport
    {
        public static void ReturnData(PL_MLQtyReport pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("ProcMLQtyReport", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                cmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate <DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@StateAutoId", pobj.StateAutoId);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar,500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                cmd.CommandTimeout = 1000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}