﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

namespace DllAccountOrderList
{
    public class PL_AccountOrderList : Utility
    {
        public int OrderAutoId { get; set; }
        public int DraftAutoId { get; set; }
        public string OrderNo { get; set; }
        public string CreditNo { get; set; }
        public decimal CreditAmount { get; set; }
        public decimal CreditMemoAmount { get; set; }
        public decimal PayableAmount { get; set; }
        public string CheckSecurity { get; set; }
        public string CustomerName { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int CustomerAutoId { get; set; }
        public int OrderType { get; set; }
        public string Terms { get; set; }
        public int OrderStatus { get; set; }
        public int SalesPersonAutoId { get; set; }
        public int LoginEmpType { get; set; }
        public int EmpAutoId { get; set; }
        public int LogAutoId { get; set; }
        public int PackerAutoId { get; set; }
        public int Times { get; set; }
        public int DrvAutoId { get; set; }
        public int AddrAutoId { get; set; }
        public int BillAddrAutoId { get; set; }
        public int ShipAddrAutoId { get; set; }
        public int AddrType { get; set; }
        public string Address { get; set; }
        public int State { get; set; }
        public string City { get; set; }
        public int ZipAutoId { get; set; }
        public string Zipcode { get; set; }
        public int ShippingType { get; set; }

        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int ReqQty { get; set; }
        public int QtyPerUnit { get; set; }
        public int TaxType { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal minprice { get; set; }
        public decimal SRP { get; set; }
        public decimal GP { get; set; }
        public decimal MLQty { get; set; }
        public decimal MLTax { get; set; }
        public decimal AdjustmentAmt { get; set; }
        public decimal TaxRate { get; set; }
        public decimal IsTaxable { get; set; }
        public decimal IsExchange { get; set; }
        public decimal IsFreeItem { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal OverallDisc { get; set; }
        public decimal OverallDiscAmt { get; set; }
        public decimal ShippingCharges { get; set; }
        public decimal TotalTax { get; set; }
        public decimal GrandTotal { get; set; }
        public int PackedBoxes { get; set; }

        public string Delivered { get; set; }
        public string PaymentRecev { get; set; }
        public string RecevAmt { get; set; }
        public decimal AmtValue { get; set; }
        public string ValueChanged { get; set; }
        public decimal DiffAmt { get; set; }
        public decimal NewTotal { get; set; }
        public int PayThru { get; set; }
        public string Remarks { get; set; }
        public string ManagerRemarks { get; set; }
        public string Barcode { get; set; }
        public string Root { get; set; }
        public string CheckNo { get; set; }

        public DataTable TableValue { get; set; }
        public DataTable TableAutoId { get; set; }
        public DataTable DTPackedItemsQty { get; set; }
        public DataTable CreditTable { get; set; }
        public DataTable AsgnOrder { get; set; }
        public DataTable DelItems { get; set; }
        public DataTable Payment { get; set; }
        public DateTime AsgnDate { get; set; }
        public bool FreeItem { get; set; }
        public DateTime Fromdate { get; set; }
        public DateTime Todate { get; set; }
        public DateTime FromDelDate { get; set; }
        public DateTime ToDelDate { get; set; }
        public string TypeShipping { get; set; }
        public int CustomerTypeAutoId { get; set; }
    }

    public class DL_AccountOrderList
    {
        public static void ReturnTable(PL_AccountOrderList pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcAccountOrderList", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@ShippingType", pobj.ShippingType);
                sqlCmd.Parameters.AddWithValue("@OrderType", pobj.OrderType);
                sqlCmd.Parameters.AddWithValue("@PayableAmount", pobj.PayableAmount);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                sqlCmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                if (pobj.FromDelDate != DateTime.MinValue && pobj.FromDelDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDelDate", pobj.FromDelDate);
                }
                if (pobj.ToDelDate != DateTime.MinValue && pobj.ToDelDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDelDate", pobj.ToDelDate);
                }
                sqlCmd.Parameters.AddWithValue("@DriverAutoId", pobj.DrvAutoId);
                sqlCmd.Parameters.AddWithValue("@CustomerTypeAutoId", pobj.CustomerTypeAutoId);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = "Oops! Something went wrong.Please try later";
            }
        }
    }

    public class BL_AccountOrderList
    {
        public static void bindStatus(PL_AccountOrderList pobj)
        {
            pobj.Opcode = 407;
            DL_AccountOrderList.ReturnTable(pobj);
        }
        public static void getDelOrderList(PL_AccountOrderList pobj)
        {
            pobj.Opcode = 419;
            DL_AccountOrderList.ReturnTable(pobj);
        }
        public static void BindCustomer(PL_AccountOrderList pobj)
        {
            pobj.Opcode = 435;
            DL_AccountOrderList.ReturnTable(pobj);
        }
    }
}
