﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data;
using System.Data.SqlClient;

namespace DLLAllowQtyInPieces
{
    public class PL_AllowQtyInPieces : Utility
    {
        public int AutoId { get; set; }
        public int SalesPersonAutoId { get; set; }
        public DateTime AllowDate { get; set; }
        public int ProductAutoId { get; set; }
        public int AllowQtyInPieces { get; set; }
        public int UsedQty { get; set; }
        public int RemainQty { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int EmpId { get; set; }
    }

    public class DL_AllowQtyInPieces
    {
        public static void ReturnTable(PL_AllowQtyInPieces pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("Proc_AllowQtyInPiece", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@EmpId", pobj.EmpId);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@AllowQtyInPieces", pobj.AllowQtyInPieces);
                sqlCmd.Parameters.AddWithValue("@UsedQty", pobj.UsedQty);
                sqlCmd.Parameters.AddWithValue("@RemainQty", pobj.RemainQty);
                if (pobj.AllowDate != DateTime.MaxValue && pobj.AllowDate != DateTime.MinValue)
                {
                    sqlCmd.Parameters.AddWithValue("@AllowDate", pobj.AllowDate);
                }
                if (pobj.FromDate != DateTime.MaxValue && pobj.FromDate != DateTime.MinValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }

                if (pobj.ToDate != DateTime.MaxValue && pobj.ToDate != DateTime.MinValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                sqlCmd.CommandTimeout = 1000;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;

                pobj.exceptionMessage = "Oops! Something went wrong.Please try later.";
            }
        }
    }

    public class BL_AllowQtyInPieces
    {
        public static void BindProduct(PL_AllowQtyInPieces pobj)
        {
            pobj.Opcode = 41;
            DL_AllowQtyInPieces.ReturnTable(pobj);
        }
        public static void InsertAndUpdate(PL_AllowQtyInPieces pobj)
        {
            pobj.Opcode = 11;
            DL_AllowQtyInPieces.ReturnTable(pobj);
        }
        public static void BindSalesPerson(PL_AllowQtyInPieces pobj)
        {
            pobj.Opcode = 42;
            DL_AllowQtyInPieces.ReturnTable(pobj);
        }

        public static void getList(PL_AllowQtyInPieces pobj)
        {
            pobj.Opcode = 51;
            DL_AllowQtyInPieces.ReturnTable(pobj);
        }
        public static void getProductUnit(PL_AllowQtyInPieces pobj)
        {
            pobj.Opcode = 52;
            DL_AllowQtyInPieces.ReturnTable(pobj);
        }
        public static void Delete(PL_AllowQtyInPieces pobj)
        {
            pobj.Opcode = 53;
            DL_AllowQtyInPieces.ReturnTable(pobj);
        }

        public static void getData(PL_AllowQtyInPieces pobj)
        {
            pobj.Opcode = 54;
            DL_AllowQtyInPieces.ReturnTable(pobj);
        }
        public static void AllocatedProductQtyDetails(PL_AllowQtyInPieces pobj)
        {
            pobj.Opcode = 55;
            DL_AllowQtyInPieces.ReturnTable(pobj);
        }
    }
}
