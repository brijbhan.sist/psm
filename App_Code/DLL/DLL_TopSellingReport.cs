﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace DLL_TopSellingReport
{
	
    public class PL_TopSaleReport : Utility
    {
        public string SalesPersons { get; set; }
        public int CustomerType { get; set; }
        public int ProductId { get; set; }
        public int Type { get; set; }
        public string ProductName { get; set; }       
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
      

    }
    public class BL_TopSaleReport
    {

        public static void BindSalesPerson(PL_TopSaleReport pobj)
        {
            pobj.Opcode = 41;
            DL_TopSaleReport.ReturnData(pobj);
        }
        public static void BindTopSellingReport(PL_TopSaleReport pobj)
        {
            pobj.Opcode = 42;
            DL_TopSaleReport.ReturnData(pobj);
        }

    }

    public class DL_TopSaleReport
    {
        public static void ReturnData(PL_TopSaleReport pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[ProcTopSellingReport]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@ProductId", pobj.ProductId);
                cmd.Parameters.AddWithValue("@Type", pobj.Type);
                cmd.Parameters.AddWithValue("@ProductName",pobj.ProductName);
                cmd.Parameters.AddWithValue("@SalesPersons", pobj.SalesPersons);
                cmd.Parameters.AddWithValue("@CustomerType", pobj.CustomerType);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception)
            {

            }
        }
    }

}