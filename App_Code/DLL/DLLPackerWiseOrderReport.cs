﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DllUtility;
using System.Data;

/// <summary>
/// Summary description for DLLOrderWisePackerReport
/// </summary>
namespace DLLOrderWisePackerReport
{
    public class PL_OrderWisePackerReport : Utility
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int PackerAutoId { get; set; }
    }
    public class DL_OrderWisePackerReport
    {
        public static void ReturnTable(PL_OrderWisePackerReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("OrderWisePackerReport", connect.con);
                sqlCmd.CommandTimeout = 100000;
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@PackerAutoId", pobj.PackerAutoId);
                if (pobj.FromDate != DateTime.MaxValue && pobj.FromDate != DateTime.MinValue)
                {
                    sqlCmd.Parameters.AddWithValue("@DateFrom", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@DateTo", pobj.ToDate);
                }                
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_OrderWisePackerReport
    {
        public static void GetReportDetail(PL_OrderWisePackerReport pobj)
        {
            pobj.Opcode = 41;
            DL_OrderWisePackerReport.ReturnTable(pobj);
        }
        public static void selectPacker(PL_OrderWisePackerReport pobj)
        {
            pobj.Opcode = 42;
            DL_OrderWisePackerReport.ReturnTable(pobj);
        }
    }
}