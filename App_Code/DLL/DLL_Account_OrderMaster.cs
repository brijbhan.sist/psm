﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
using System.Web;

namespace DLL_Account_OrderMaster
{
    public class PL_Account_OrderMaster : Utility
    {
        public int OrderAutoId { get; set; }
        public int CustomerAutoId { get; set; }
        public int OrderStatus { get; set; }
        public int LoginEmpType { get; set; }
        public int EmpAutoId { get; set; }
        public int ProductAutoId { get; set; }
        public int UnitAutoId { get; set; }
        public int ReqQty { get; set; }
        public int QtyPerUnit { get; set; }
        public string Remarks { get; set; }
        public string MLTaxRemark { get; set; }
        
        public DataTable DelItems { get; set; }
        public DateTime Fromdate { get; set; }
        public DateTime Todate { get; set; }
        public string CheckSecurity { get; set; }
        public string CancelRemark { get; set; }
        public decimal NewTotal { get; set; }
        public string PaymentRecev { get; set; }
        public string RecevAmt { get; set; }
        public decimal AmtValue { get; set; }
        public string ValueChanged { get; set; }
        public decimal DiffAmt { get; set; }
        public decimal PayableAmount { get; set; }
        public decimal CreditMemoAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal OverallDisc { get; set; }
        public decimal OverallDiscAmt { get; set; }
        public decimal ShippingCharges { get; set; }
        public decimal TotalTax { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal MLQty { get; set; }
        public decimal MLTax { get; set; }
        public decimal AdjustmentAmt { get; set; }
        public decimal minprice { get; set; }
        public decimal SRP { get; set; }
        public decimal GP { get; set; }
        public decimal IsTaxable { get; set; }
        public decimal IsExchange { get; set; }
        public decimal IsFreeItem { get; set; }
        public string Barcode { get; set; }
    }

    public class DL_Account_OrderMaster
    {
        public static void ReturnTable(PL_Account_OrderMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("Proc_Account_OrderMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
                sqlCmd.Parameters.AddWithValue("@ReqQty", pobj.ReqQty);
                sqlCmd.Parameters.AddWithValue("@QtyPerUnit", pobj.QtyPerUnit);
                sqlCmd.Parameters.AddWithValue("@ProductAutoId", pobj.ProductAutoId);
                sqlCmd.Parameters.AddWithValue("@UnitAutoId", pobj.UnitAutoId);
                sqlCmd.Parameters.AddWithValue("@CheckSecurity", pobj.CheckSecurity);
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@CancelRemark", pobj.CancelRemark);
                sqlCmd.Parameters.AddWithValue("@IPAddress", pobj.IPAddress);

                if (HttpContext.Current.Session["EmpAutoId"] != null)
                    sqlCmd.Parameters.AddWithValue("@EmpAutoId", HttpContext.Current.Session["EmpAutoId"].ToString());
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                if (pobj.Fromdate != DateTime.MinValue && pobj.Fromdate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Fromdate", pobj.Fromdate);
                }
                if (pobj.Todate != DateTime.MinValue && pobj.Todate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Todate", pobj.Todate);
                }
                sqlCmd.Parameters.AddWithValue("@NewTotal", pobj.NewTotal);
                sqlCmd.Parameters.AddWithValue("@PaymentRecev", pobj.PaymentRecev); 
                sqlCmd.Parameters.AddWithValue("@AmtValue", pobj.AmtValue);
                sqlCmd.Parameters.AddWithValue("@ValueChanged", pobj.ValueChanged);
                sqlCmd.Parameters.AddWithValue("@DiffAmt", pobj.DiffAmt);
                sqlCmd.Parameters.AddWithValue("@PayableAmount", pobj.PayableAmount);
                sqlCmd.Parameters.AddWithValue("@DelItems", pobj.DelItems);
                sqlCmd.Parameters.AddWithValue("@CreditMemoAmount", pobj.CreditMemoAmount);
                sqlCmd.Parameters.AddWithValue("@OverallDiscAmt", pobj.OverallDiscAmt);
                sqlCmd.Parameters.AddWithValue("@ShippingCharges", pobj.ShippingCharges);
                sqlCmd.Parameters.AddWithValue("@MLTaxRemark", pobj.MLTaxRemark);

                sqlCmd.Parameters.AddWithValue("@Barcode", pobj.Barcode);
                sqlCmd.Parameters.AddWithValue("@IsFreeItem", pobj.IsFreeItem);
                sqlCmd.Parameters.AddWithValue("@IsExchange", pobj.IsExchange);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_Account_OrderMaster
    {

        public static void getOrderData(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 41;
            DL_Account_OrderMaster.ReturnTable(pobj);
        }

        public static void clickonSecurity(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 42;
            DL_Account_OrderMaster.ReturnTable(pobj);
        }
        public static void clickonSecurityVoid(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 43;
            DL_Account_OrderMaster.ReturnTable(pobj);
        }

        public static void CancelOrder(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 21;
            DL_Account_OrderMaster.ReturnTable(pobj);
        }
        public static void getProduct(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 53;
            DL_Account_OrderMaster.ReturnTable(pobj);
        }

        public static void getUnit(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 54;
            DL_Account_OrderMaster.ReturnTable(pobj);
        }
        public static void selectQtyPrice(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 55;
            DL_Account_OrderMaster.ReturnTable(pobj);
        }

        public static void getTaxType(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 61;
            DL_Account_OrderMaster.ReturnTable(pobj);

        }
        public static void saveDelUpdates(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 105;
            DL_Account_OrderMaster.ReturnTable(pobj);
        }

        public static void updatedeleverOrder(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 207;
            DL_Account_OrderMaster.ReturnTable(pobj);
        }
        public static void AddThroughBarcode(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 106;
            DL_Account_OrderMaster.ReturnTable(pobj);
        }
        public static void clickonSecurityMLTax(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 208;
            DL_Account_OrderMaster.ReturnTable(pobj);
        }
        public static void ConfirmSecurity(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 209;
            DL_Account_OrderMaster.ReturnTable(pobj);
        } 
        public static void clickonSecurityMigrate(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 210;
            DL_Account_OrderMaster.ReturnTable(pobj);
        } 
        public static void MigrateOrder(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 211;
            DL_Account_OrderMaster.ReturnTable(pobj);
        }
        public static void UpdateCostprice(PL_Account_OrderMaster pobj)
        {
            pobj.Opcode = 212;
            DL_Account_OrderMaster.ReturnTable(pobj);
        }
    }
}
