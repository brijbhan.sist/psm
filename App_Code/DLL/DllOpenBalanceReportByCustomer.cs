﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DllUtility;

/// <summary>
/// Summary description for DllOpenBalanceReportByCustomer
/// </summary>
namespace DllOpenBalanceReportByCustomer
{
    public class PL_OpenBalanceReportByCustomer : Utility
    {
        public int CustomerAutoId { get; set; }
        public string SalesPerson { get; set; }
        public int EmpAutoId { get; set; }
        public int CustomerType { get; set; }
        public int SalesPersonAutoId { get; set; }
        public int SearchBy { get; set; }
    }
    public class DL_OpenBalanceReportByCustomer
    {
        public static void ReturnData(PL_OpenBalanceReportByCustomer pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("ProcSalesReportByCustomer", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                cmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                cmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                cmd.Parameters.AddWithValue("@CustomerType", pobj.CustomerType);
                cmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
    public class BL_OpenBalanceReportByCustomer
    {
       
        public static void BindCustomerType(PL_OpenBalanceReportByCustomer pobj)
        {
            pobj.Opcode = 41;
            DL_OpenBalanceReportByCustomer.ReturnData(pobj);
        }
        public static void BindCustomer(PL_OpenBalanceReportByCustomer pobj)
        {
            pobj.Opcode = 42;
            DL_OpenBalanceReportByCustomer.ReturnData(pobj);
        }
        public static void OpenBalanceReportSummary(PL_OpenBalanceReportByCustomer pobj)
        {
            pobj.Opcode = 43;
            DL_OpenBalanceReportByCustomer.ReturnData(pobj);
        }
    }
}