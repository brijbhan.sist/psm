﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DllUtility;

/// <summary>
/// Summary description for DLLViewVenderDetails
/// </summary>
namespace  DLLViewVenderDetails
{
	public class PL_ViewVenderDetails : Utility
	{
        public string VenderId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentURL { get; set; }
        public int EmpAutoId { get; set; }
        public int FileAutoId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string BankName { get; set; }
        public string BankAcc { get; set; }
        public int CardType { get; set; }
        public string CardNo { get; set; }
        public string ExpiryDate { get; set; }
        public string CVV { get; set; }
        public int AutoId { get; set; }
        public string PayId { get; set; }
        public int Status { get; set; }
        public string RoutingNo { get; set; }
        public string Zipcode { get; set; }
        public string Remarks { get; set; }
    }
    public class DL_ViewVenderDetails
    {
        public static void ReturnTable(PL_ViewVenderDetails pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcViewVenderDetails", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@Vendorid", pobj.VenderId);
                sqlCmd.Parameters.AddWithValue("@DocumentName", pobj.DocumentName);
                sqlCmd.Parameters.AddWithValue("@DocumentURL", pobj.DocumentURL);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@FileAutoId", pobj.FileAutoId);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.AddWithValue("@BankName", pobj.BankName);
                sqlCmd.Parameters.AddWithValue("@PayId", pobj.PayId);
                sqlCmd.Parameters.AddWithValue("@BankAcc", pobj.BankAcc);
                sqlCmd.Parameters.AddWithValue("@CardType", pobj.CardType);
                sqlCmd.Parameters.AddWithValue("@CardNo", pobj.CardNo);
                sqlCmd.Parameters.AddWithValue("@CVV", pobj.CVV);
                sqlCmd.Parameters.AddWithValue("@Zipcode", pobj.Zipcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@RoutingNo", pobj.RoutingNo);
                sqlCmd.Parameters.AddWithValue("@ExpiryDate", pobj.ExpiryDate);
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);
                //if (pobj.ExpiryDate > DateTime.MinValue && pobj.ExpiryDate < DateTime.MaxValue)
                //{
                //    sqlCmd.Parameters.AddWithValue("@ExpiryDate", pobj.ExpiryDate);
                //}
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_ViewVenderDetails
    {
        public static void SelectVenderDetails(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 41;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void GetPoList(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 42;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void GetPayHitry(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 43;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void GetPayList(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 44;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void DocumentSave(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 45;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void CustomerDocument(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 46;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void UpdateDocument(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 47;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void DeleteDocument(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 48;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void bindCardType(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 49;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void SaveBankDetails(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 50;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void GetBankDetailsList(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 51;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void EditBankDetails(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 52;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void UpdateBankDetails(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 53;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void DeleteBankDetails(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 54;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void Vendersdelete(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 55;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void getPayLists(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 56;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
        public static void getReceivedBill(PL_ViewVenderDetails pobj)
        {
            pobj.Opcode = 57;
            DL_ViewVenderDetails.ReturnTable(pobj);
        }
    }
}