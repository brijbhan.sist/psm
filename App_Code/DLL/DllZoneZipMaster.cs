﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DllZoneZipMaster
/// </summary>
namespace DllZoneZipMaster
{
    public class PL_ZoneZipMaster : Utility
    {
        public int AutoId { get; set; }
        public string Name { get; set; }
        public int DriverId { get; set; }
        public int CarId { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public int Status { get; set; }
    }

    public class DL_ZoneZipMaster
    {
        public static void Returntable(PL_ZoneZipMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcZoneZipMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@Name", pobj.Name);
                sqlCmd.Parameters.AddWithValue("@ZipCode", pobj.ZipCode);
                sqlCmd.Parameters.AddWithValue("@City", pobj.City);
                sqlCmd.Parameters.AddWithValue("@DriverId", pobj.DriverId);
                sqlCmd.Parameters.AddWithValue("@CarId", pobj.CarId);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_ZoneZipMaster
    {
        public static void Insert(PL_ZoneZipMaster pobj)
        {
            pobj.Opcode = 11;
            DL_ZoneZipMaster.Returntable(pobj);
        }
        public static void Update(PL_ZoneZipMaster pobj)
        {
            pobj.Opcode = 21;
            DL_ZoneZipMaster.Returntable(pobj);
        }
        public static void UpdateZoneZip(PL_ZoneZipMaster pobj)
        {
            pobj.Opcode = 22;
            DL_ZoneZipMaster.Returntable(pobj);
        }
        public static void SelectDriverCar(PL_ZoneZipMaster pobj)
        {
            pobj.Opcode = 41;
            DL_ZoneZipMaster.Returntable(pobj);
        }
        public static void EditZoneMaster(PL_ZoneZipMaster pobj)
        {
            pobj.Opcode = 42;
            DL_ZoneZipMaster.Returntable(pobj);
        }
        public static void SelectAllDriverCar(PL_ZoneZipMaster pobj)
        {
            pobj.Opcode = 43;
            DL_ZoneZipMaster.Returntable(pobj);
        }
        public static void GetZoneList(PL_ZoneZipMaster pobj)
        {
            pobj.Opcode = 44;
            DL_ZoneZipMaster.Returntable(pobj);
        }
    }
}