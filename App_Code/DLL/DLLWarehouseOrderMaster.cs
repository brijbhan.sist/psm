﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DllUtility;
using System.Data.SqlClient;
using System.Data;
/// <summary>
/// Summary description for DllCopyOrderMaster
/// </summary>
namespace DllWarehouseOrderMaster
{
    public class PL_COrderMaster : Utility
    {
        public int OrderAutoId { get; set; }
        public int DraftAutoId { get; set; }
        public string OrderNo { get; set; }
       
        public DateTime OrderDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int CustomerAutoId { get; set; }
       
        public int OrderStatus { get; set; }
        public int SalesPersonAutoId { get; set; }
        public int LoginEmpType { get; set; }
        public int EmpAutoId { get; set; }
        public int PackerAutoId { get; set; }
        public int Times { get; set; }
       
        public string Remarks { get; set; }
        
        public DataTable TableValueMultiOrder { get; set; }
        
        public DateTime AsgnDate { get; set; }
        public DateTime Fromdate { get; set; }
        public DateTime Todate { get; set; }
        public DateTime FromDelDate { get; set; }
        public DateTime ToDelDate { get; set; }
        public string TypeShipping { get; set; }

    }

    public class DL_COrderMaster
    {
        public static void ReturnTable(PL_COrderMaster pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("[ProcWarehouseOrderMaster]", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@OrderAutoId", pobj.OrderAutoId);
                sqlCmd.Parameters.AddWithValue("@OrderNo", pobj.OrderNo);
                sqlCmd.Parameters.AddWithValue("@Times", pobj.Times);
                if (pobj.OrderDate != DateTime.MinValue && pobj.OrderDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@OrderDate", pobj.OrderDate);
                }
                if (pobj.DeliveryDate != DateTime.MinValue && pobj.DeliveryDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@DeliveryDate", pobj.DeliveryDate);
                }
                sqlCmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                sqlCmd.Parameters.AddWithValue("@LoginEmpType", pobj.LoginEmpType);
                sqlCmd.Parameters.AddWithValue("@PackerAutoId", pobj.PackerAutoId);
                sqlCmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                sqlCmd.Parameters.AddWithValue("@OrderStatus", pobj.OrderStatus);
                sqlCmd.Parameters.AddWithValue("@TableValueMultiOrder", pobj.TableValueMultiOrder);
                sqlCmd.Parameters.AddWithValue("@Remarks", pobj.Remarks);

                if (pobj.Fromdate != DateTime.MinValue && pobj.Fromdate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Fromdate", pobj.Fromdate);
                }
                if (pobj.Todate != DateTime.MinValue && pobj.Todate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@Todate", pobj.Todate);
                }
                if (pobj.FromDelDate != DateTime.MinValue && pobj.FromDelDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@FromDelDate", pobj.FromDelDate);
                }
                if (pobj.ToDelDate != DateTime.MinValue && pobj.ToDelDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@ToDelDate", pobj.ToDelDate);
                }
                if (pobj.AsgnDate != DateTime.MinValue && pobj.AsgnDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@AsgnDate", pobj.AsgnDate);
                }
                sqlCmd.Parameters.AddWithValue("@TypeShipping", pobj.TypeShipping);
                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                sqlCmd.Parameters.AddWithValue("@RecordCount", pobj.RecordCount);
                sqlCmd.Parameters.Add("@DraftAutoId", SqlDbType.VarChar, 20).Value = pobj.DraftAutoId;
                sqlCmd.Parameters["@DraftAutoId"].Direction = ParameterDirection.InputOutput;
                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                sqlCmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.DraftAutoId = Convert.ToInt32(sqlCmd.Parameters["@DraftAutoId"].Value);
                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }

    public class BL_COrderMaster
    {



        public static void assignPacker(PL_COrderMaster pobj)
        {
            pobj.Opcode = 2021;
            DL_COrderMaster.ReturnTable(pobj);
        }


        //public static void delete(PL_COrderMaster pobj)
        //{
        //    pobj.Opcode = 301;
        //    DL_COrderMaster.ReturnTable(pobj);
        //}
        public static void selectOrderList(PL_COrderMaster pobj)
        {
            pobj.Opcode = 405;
            DL_COrderMaster.ReturnTable(pobj);
        }

        public static void bindStatus(PL_COrderMaster pobj)
        {
            pobj.Opcode = 407;
            DL_COrderMaster.ReturnTable(pobj);
        }
        public static void getDriverList(PL_COrderMaster pobj)
        {
            pobj.Opcode = 412;
            DL_COrderMaster.ReturnTable(pobj);
        }
        public static void getPackerList(PL_COrderMaster pobj)
        {
            pobj.Opcode = 413;
            DL_COrderMaster.ReturnTable(pobj);
        }


        public static void BindCustomer(PL_COrderMaster pobj)
        {
            pobj.Opcode = 435;
            DL_COrderMaster.ReturnTable(pobj);
        }
       

    }
}