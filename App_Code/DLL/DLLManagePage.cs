﻿using DllUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DLLManagePage
/// </summary>
namespace DLLManagePage
{
    public class PL_ManagePage : Utility
    {
        public int PageAutoId { get; set; }
        public string PageName { get; set; }
        public string PageId { get; set; }
        public string PageUrl { get; set; }
        public string Description { get; set; }
        public string PageAction { get; set; }
        public int Status { get; set; }
        public int PageType { get; set; }
        public int UserType { get; set; }
        public int ActionType { get; set; }
        public int EmpTypeNo { get; set; }
        public int ActionAutoId { get; set; }
        public int ModuleAutoId { get; set; }
        
    }
    public class DL_ManagePage
    {
        public static void ReturnTable(PL_ManagePage pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcPageMaster", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@OpCode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.PageAutoId);
                sqlCmd.Parameters.AddWithValue("@PageName", pobj.PageName);
                sqlCmd.Parameters.AddWithValue("@PageId", pobj.PageId);
                sqlCmd.Parameters.AddWithValue("@PageUrl", pobj.PageUrl);
                sqlCmd.Parameters.AddWithValue("@Description", pobj.Description);
                sqlCmd.Parameters.AddWithValue("@PageAction", pobj.PageAction);
                sqlCmd.Parameters.AddWithValue("@PageType", pobj.PageType);
                sqlCmd.Parameters.AddWithValue("@UserType", pobj.UserType);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);
                sqlCmd.Parameters.AddWithValue("@EmpTypeNo", pobj.EmpTypeNo);
                sqlCmd.Parameters.AddWithValue("@ActionType", pobj.ActionType);
                sqlCmd.Parameters.AddWithValue("@ActionAutoId", pobj.ActionAutoId);
                sqlCmd.Parameters.AddWithValue("@ModuleAutoId", pobj.ModuleAutoId);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();

            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }

    }
    public class BL_ManagePage
    {
        public static void insert(PL_ManagePage pobj)
        {
            pobj.Opcode = 11;
            DL_ManagePage.ReturnTable(pobj);
        }
        public static void saveAction(PL_ManagePage pobj)
        {
            pobj.Opcode = 12;
            DL_ManagePage.ReturnTable(pobj);
        }
        public static void updateAction(PL_ManagePage pobj)
        {
            pobj.Opcode = 22;
            DL_ManagePage.ReturnTable(pobj);
        }
        public static void select(PL_ManagePage pobj)
        {
            pobj.Opcode = 41;
            DL_ManagePage.ReturnTable(pobj);
        }
        public static void getPageByPageId(PL_ManagePage pobj)
        {
            pobj.Opcode = 43;
            DL_ManagePage.ReturnTable(pobj);
        }
        public static void delete(PL_ManagePage pobj)
        {
            pobj.Opcode = 31;
            DL_ManagePage.ReturnTable(pobj);
        }
        public static void update(PL_ManagePage pobj)
        {
            pobj.Opcode = 21;
            DL_ManagePage.ReturnTable(pobj);
        }
        public static void selectUserType(PL_ManagePage pobj)
        {
            pobj.Opcode = 42;
            DL_ManagePage.ReturnTable(pobj);
        }
        public static void getActionList(PL_ManagePage pobj)
        {
            pobj.Opcode = 44;
            DL_ManagePage.ReturnTable(pobj);
        }
        public static void getParentModule(PL_ManagePage pobj)
        {
            pobj.Opcode = 45;
            DL_ManagePage.ReturnTable(pobj);
        }
        public static void checkActionBeforeRemove(PL_ManagePage pobj)
        {
            pobj.Opcode = 32;
            DL_ManagePage.ReturnTable(pobj);
        }
    }

}