﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DllUtility;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DLLEndOfDay
/// </summary>
namespace DLLEndOfDay
{
    public class PL_EndOfDay : Utility
    {
        public int SalesPersonAutoId { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int EmpAutoId { get; set; }
        public DateTime EndTime { get; set; }
    }
    public class DL_EndOfDay
    {
        public static void ReturnData(PL_EndOfDay pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("[ProcEndOfDay]", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                cmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                if (DateTime.MinValue < pobj.EndDate && DateTime.MaxValue > pobj.EndDate)
                    cmd.Parameters.AddWithValue("@EndDate", pobj.EndDate);
                if (DateTime.MinValue < pobj.EndTime && DateTime.MaxValue > pobj.EndTime)
                cmd.Parameters.AddWithValue("@EndTime", pobj.EndTime);

                if (DateTime.MinValue < pobj.FromDate && DateTime.MaxValue > pobj.FromDate)
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                if (DateTime.MinValue < pobj.ToDate && DateTime.MaxValue > pobj.ToDate)
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);

                //cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                //cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
    public class BL_EndOfDay
    {
        public static void InsertRecord(PL_EndOfDay pobj)
        {
            pobj.Opcode = 11;
            DL_EndOfDay.ReturnData(pobj);
        }
        public static void bindSalesPerson(PL_EndOfDay pobj)
        {
            pobj.Opcode = 41;
            DL_EndOfDay.ReturnData(pobj);
        }
        public static void getList(PL_EndOfDay pobj)
        {
            pobj.Opcode = 42;
            DL_EndOfDay.ReturnData(pobj);
        }
    }
}