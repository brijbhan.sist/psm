﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DllUtility;


namespace DllOpenBalanceReportByOrder
{
    public class PL_OpenBalanceReportByOrder : Utility
    {
        public int CustomerAutoId { get; set; }
        public string SalesPerson { get; set; }
        public int EmpAutoId { get; set; }
        public int CustomerType { get; set; }
        public int SalesPersonAutoId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int SearchBy { get; set; }
        public int ShortBy { get; set; }
        public string ShippingType { get; set; }
        public string ordering { get; set; }
    }
    public class DL_OpenBalanceReportByOrder
    {
        public static void ReturnData(PL_OpenBalanceReportByOrder pobj)
        {
            try
            {
                Config conn = new Config();
                SqlCommand cmd = new SqlCommand("ProcOpenBalanceReportByOrder", conn.con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                cmd.Parameters.AddWithValue("@SalesPerson", pobj.SalesPerson);
                cmd.Parameters.AddWithValue("@EmpAutoId", pobj.EmpAutoId);
                cmd.Parameters.AddWithValue("@CustomerAutoId", pobj.CustomerAutoId);
                cmd.Parameters.AddWithValue("@CustomerType", pobj.CustomerType);
                cmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                cmd.Parameters.AddWithValue("@ShippingType", pobj.ShippingType);
                cmd.Parameters.AddWithValue("@SortBy", pobj.ShortBy);
                cmd.Parameters.AddWithValue("@ordering", pobj.ordering);
                if (pobj.FromDate > DateTime.MinValue && pobj.FromDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@FromDate", pobj.FromDate);
                }
                if (pobj.ToDate > DateTime.MinValue && pobj.ToDate < DateTime.MaxValue)
                {
                    cmd.Parameters.AddWithValue("@ToDate", pobj.ToDate);
                }

                cmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                cmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);
                cmd.Parameters.Add("@isException", SqlDbType.Bit);
                cmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                cmd.Parameters["@isException"].Value = pobj.isException;
                cmd.Parameters.Add("@ExceptionMessage", SqlDbType.VarChar, 500);
                cmd.Parameters["@ExceptionMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ExceptionMessage"].Value = pobj.exceptionMessage;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.CommandTimeout = 1000;
                pobj.Ds = new DataSet();
                da.Fill(pobj.Ds);
                pobj.isException = Convert.ToBoolean(cmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = (cmd.Parameters["@ExceptionMessage"].Value).ToString();
            }
            catch (Exception ex)
            {

            }
        }
    }
    public class BL_OpenBalanceReportByOrder
    {
        public static void BindCustomerType(PL_OpenBalanceReportByOrder pobj)
        {
            pobj.Opcode = 41;
            DL_OpenBalanceReportByOrder.ReturnData(pobj);
        }
        public static void BindCustomer(PL_OpenBalanceReportByOrder pobj)
        {
            pobj.Opcode = 42;
            DL_OpenBalanceReportByOrder.ReturnData(pobj);
        }
        public static void OrderWiseOpenBalanceReport(PL_OpenBalanceReportByOrder pobj)
        {
            pobj.Opcode = 43;
            DL_OpenBalanceReportByOrder.ReturnData(pobj);
        }
    }
}