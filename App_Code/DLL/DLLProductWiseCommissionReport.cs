﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DllUtility;
using System.Data;

/// <summary>
/// Summary description for DLLProductWiseCommissionReport
/// </summary>
namespace DLLProductWiseCommissionReport
{
	public class PL_ProductWiseCommissionReport : Utility
	{
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string CommissionCode { get; set; }
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public int SalesPersonAutoId { get; set; }
	}
    public class DL_ProductWiseCommissionReport
    {
        public static void ReturnTable(PL_ProductWiseCommissionReport pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProductWiseCommissionReport", connect.con);
                sqlCmd.CommandTimeout = 100000;
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@CommissionCode", pobj.CommissionCode);
                sqlCmd.Parameters.AddWithValue("@ProductId", pobj.ProductId);
                sqlCmd.Parameters.AddWithValue("@ProductName", pobj.ProductName);
                sqlCmd.Parameters.AddWithValue("@SalesPersonAutoId", pobj.SalesPersonAutoId);
                if (pobj.FromDate != DateTime.MaxValue && pobj.FromDate != DateTime.MinValue)
                {
                    sqlCmd.Parameters.AddWithValue("@DateFrom", pobj.FromDate);
                }
                if (pobj.ToDate != DateTime.MinValue && pobj.ToDate != DateTime.MaxValue)
                {
                    sqlCmd.Parameters.AddWithValue("@DateTo", pobj.ToDate);
                }


                sqlCmd.Parameters.AddWithValue("@PageIndex", pobj.PageIndex);
                sqlCmd.Parameters.AddWithValue("@PageSize", pobj.PageSize);


                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;

                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);

                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BL_ProductWiseCommissionReport
    {
        public static void GetReportDetail(PL_ProductWiseCommissionReport pobj)
        {
            pobj.Opcode = 41;
            DL_ProductWiseCommissionReport.ReturnTable(pobj);
        }
        public static void selectSalesPerson(PL_ProductWiseCommissionReport pobj)
        {
            pobj.Opcode = 42;
            DL_ProductWiseCommissionReport.ReturnTable(pobj);
        }
    }
}