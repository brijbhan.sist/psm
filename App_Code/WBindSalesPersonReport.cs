﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLSaleReport;
using System.Web.Script.Serialization;
using DllUtility;
/// <summary>
/// Summary description for WBindSalesPersonReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WBindSalesPersonReport : System.Web.Services.WebService
{

    public WBindSalesPersonReport()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string BindSalesPerson()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                BL_SaleReport.BindSalesPerson(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string SaleBySalesPersonDetail(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.SalesPerson = jdv["SalesPerson"].ToString();
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                pobj.PageSize = Convert.ToInt16(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt16(jdv["PageIndex"]);
                BL_SaleReport.SaleBySalesPersonDetail(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string SaleByCustomerSalesPersonDetail(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_SaleReport pobj = new PL_SaleReport();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.SalesPerson = jdv["SalesPerson"].ToString();
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                pobj.PageSize = Convert.ToInt16(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt16(jdv["PageIndex"]);
                BL_SaleReport.SaleByCustomerSalesPersonDetail(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}
