﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllCheckMaster;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for WCheckMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WCheckMaster : System.Web.Services.WebService
{

    public WCheckMaster()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    [WebMethod(EnableSession = true)]
    public string CheckSecurity(string datavalue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(datavalue);
        PL_CheckMaster pobj = new PL_CheckMaster();
        try
        {
            if (Session["EmpTypeNo"] != null)
            {
                pobj.EmpTypeAutoId = Convert.ToInt32(Session["EmpTypeNo"].ToString());
                pobj.SecurityKey = jdv["Security"];
                BL_CheckMaster.checkSecurity(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "SessionExpired";

            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string insertCheck(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_CheckMaster pobj = new PL_CheckMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CheckId = jdv["CheckId"];
                pobj.Type = Convert.ToInt32(jdv["Type"]);
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                pobj.VendorAddress = jdv["VendorAddress"];
                pobj.CheckAmount = Convert.ToDecimal(jdv["CheckAmount"]);
                pobj.CheckDate = Convert.ToDateTime(jdv["CheckDate"]);
                pobj.Memo = jdv["Memo"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_CheckMaster.insert(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "session";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getCheckDetail(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_CheckMaster pobj = new PL_CheckMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CheckId = jdv["CheckNo"];
                pobj.Type = Convert.ToInt32(jdv["Type"]);
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.PageIndex = Convert.ToInt32(jdv["pageindex"]);
                pobj.PageSize = Convert.ToInt32(jdv["pagesize"]);

                BL_CheckMaster.select(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "session";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindVendorAddress(string VendorAutoId,string Type)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CheckMaster pobj = new PL_CheckMaster();
            try
            {
                pobj.VendorAutoId = Convert.ToInt32(VendorAutoId);
                pobj.Type = Convert.ToInt32(Type);
                BL_CheckMaster.Vendorselect(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "session";
        }
    }
    /*
    [WebMethod(EnableSession = true)]
    public string bindVendorAddress(int Type)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_CheckMaster pobj = new PL_CheckMaster();
            pobj.EmpTypeAutoId = Type;
            BL_CheckMaster.bindVendor(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Unauthorized";
        }
    }
     * */
    [WebMethod(EnableSession = true)]
    public string editCheck(string CheckAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CheckMaster pobj = new PL_CheckMaster();
            pobj.CheckAutoId = Convert.ToInt32(CheckAutoId);
            BL_CheckMaster.editCheckMaster(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "session";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateCheck(string dataValue)
    {

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_CheckMaster pobj = new PL_CheckMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.Type = Convert.ToInt32(jdv["Type"]);
                pobj.CheckAutoId = Convert.ToInt32(jdv["CheckAutoId"]);
                pobj.CheckId = jdv["CheckId"];
                pobj.VendorAutoId = Convert.ToInt32(jdv["VendorAutoId"]);
                pobj.VendorAddress = jdv["VendorAddress"];
                pobj.CheckAmount = Convert.ToDecimal(jdv["CheckAmount"]);
                pobj.CheckDate = Convert.ToDateTime(jdv["CheckDate"]);
                pobj.Memo = jdv["Memo"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_CheckMaster.update(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "session";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteCheck(string CheckAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CheckMaster pobj = new PL_CheckMaster();
            try
            {
                pobj.CheckAutoId = Convert.ToInt32(CheckAutoId);
                BL_CheckMaster.delete(pobj);
                if (!pobj.isException)
                {

                    return "Success";
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {

                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "session";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getPrintDetails(string CheckAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CheckMaster pobj = new PL_CheckMaster();
            try
            {

                pobj.CheckAutoId = Convert.ToInt32(CheckAutoId);
                BL_CheckMaster.getPrintDetails(pobj);

                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {

                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "session";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindVendor(string Type)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_CheckMaster pobj = new PL_CheckMaster();
            pobj.Type = Convert.ToInt32(Type);
            BL_CheckMaster.bindVendor(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Unauthorized";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getCheckAmount(string CheckAmount)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_CheckMaster pobj = new PL_CheckMaster();
            try
            {

                pobj.CheckAmount = Convert.ToDecimal(CheckAmount);
                BL_CheckMaster.getCheckAmount(pobj);

                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {

                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "session";
        }
    }

}
