﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLPackerReportSummary;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for PackerReportSummary
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class PackerReportSummary : System.Web.Services.WebService {

    public PackerReportSummary () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod (EnableSession = true)]
    public string GetPackerName()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PackerReportSummary pobj = new PL_PackerReportSummary();
                BL_PackerReportSummary.GetPackerName(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod (EnableSession = true)]
    public string GetPackerResport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_PackerReportSummary pobj = new PL_PackerReportSummary();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                if (jdv["DateFrom"] != null && jdv["DateFrom"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["DateFrom"]);
                }
                if (jdv["DateTo"] != null && jdv["DateTo"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["DateTo"]);
                }
                pobj.PackerAutoId = Convert.ToInt32(jdv["PackerAutoId"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_PackerReportSummary.GetPackerResport(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
