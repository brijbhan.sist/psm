﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLRouteMaster;
using System.Web.Script.Serialization;


/// <summary>
/// Summary description for RouteMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class RouteMaster : System.Web.Services.WebService
{

    public RouteMaster()
    {
    }

    [WebMethod(EnableSession = true)]
    public string GetdSalesPerson()
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            PL_RouteMaster pobj = new PL_RouteMaster();
            pobj.AutoId = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"].ToString());
            pobj.SelesPersonId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
            BL_RouteMaster.GetSalesPerson(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "session";
        }
    }
    [WebMethod(EnableSession = true)]
    public string insertRoute(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_RouteMaster pobj = new PL_RouteMaster();
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                pobj.UserAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.RouteName = jdv["RouteName"];


                if (HttpContext.Current.Session["EmpTypeNo"].ToString() == "2")
                {
                    pobj.SelesPersonId = Convert.ToInt32(Session["EmpAutoId"]);
                }
                else
                {

                    pobj.SelesPersonId = Convert.ToInt32(jdv["SalesPersonID"]);
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_RouteMaster.InsertRoute(pobj);

                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    if(pobj.exceptionMessage== "Exists")
                    {
                        return "Exists";
                    }
                    else
                    {
                        return "false";
                    }
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "session";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetRouteList(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_RouteMaster pobj = new PL_RouteMaster();
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            if (HttpContext.Current.Session["EmpTypeNo"].ToString() == "2")
            {
                pobj.SelesPersonId = Convert.ToInt32(Session["EmpAutoId"]);
            }
            else
            {
                pobj.SelesPersonId = Convert.ToInt32(jdv["SalesPersonName"]);
            }
            pobj.RouteName = jdv["RouteName"];
            pobj.Status = Convert.ToInt32(jdv["Status"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            BL_RouteMaster.GetRouteList(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "session";
        }
    }
     [WebMethod(EnableSession = true)]
    public string EditRouteMaster(int AutoId, int Salesperson, int CustomerDetail)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            PL_RouteMaster pobj = new PL_RouteMaster();
            pobj.AutoId = AutoId;
            pobj.SelesPersonId = Salesperson;
            pobj.CustomerAutoId = CustomerDetail;
            BL_RouteMaster.EditRouteMaster(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "session";
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpateRoute(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_RouteMaster pobj = new PL_RouteMaster();
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            try
            {
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.UserAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.RouteName = jdv["RouteName"];
                if (HttpContext.Current.Session["EmpTypeNo"].ToString() == "2")
                {
                    pobj.SelesPersonId = Convert.ToInt32(Session["EmpAutoId"]);
                }
                else
                {

                    pobj.SelesPersonId = Convert.ToInt32(jdv["SalesPersonID"]);
                }
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_RouteMaster.UpdateRoute(pobj);
                if (!pobj.isException)
                {
                    return "success";
                }
                else
                {
                    if (pobj.exceptionMessage == "Exists")
                    {
                        return "Exists";
                    }
                    else
                    {
                        return "false";
                    }
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "session";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateCheckCustomer(string dataValue)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_RouteMaster pobj = new PL_RouteMaster();
                pobj.UserAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["custid"]);
                BL_RouteMaster.InsertCheck(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "session";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetCustomerBySalesPerson(int AutoID)
    {
        if (HttpContext.Current.Session["EmpTypeNo"] != null)
        {
            PL_RouteMaster pobj = new PL_RouteMaster();
            pobj.SelesPersonId = Convert.ToInt32(AutoID);
            BL_RouteMaster.GetCustomerBySalesPerson(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "session";
        }
    }
}
