﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using DLLOrderWisePackerReport;
using DllUtility;
/// <summary>
/// Summary description for WPackerWiseOrderReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WPackerWiseOrderReport : System.Web.Services.WebService
{

    public WPackerWiseOrderReport()
    {
        gZipCompression.fn_gZipCompression();

    }
    [WebMethod (EnableSession = true)]
    public string Bindpacker()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_OrderWisePackerReport pobj = new PL_OrderWisePackerReport();
                BL_OrderWisePackerReport.selectPacker(pobj);
                //return pobj.Ds.GetXml();
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

   [WebMethod(EnableSession = true)]
    public string GetProductWiseCommissionList(string dataValue)
    {
       if (Session["EmpAutoId"] != null)
        {
        try
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_OrderWisePackerReport pobj = new PL_OrderWisePackerReport();
            if (jdv["FromDate"] != null && jdv["FromDate"] != "")
            {
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            }
            if (jdv["ToDate"] != null && jdv["ToDate"] != "")
            {
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            }
            pobj.PackerAutoId = Convert.ToInt32(jdv["PackerAutoId"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            BL_OrderWisePackerReport.GetReportDetail(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        catch
        {
            return "false";
        }
        }
       else
       {
           return "Session Expired";
       }
    }
}
