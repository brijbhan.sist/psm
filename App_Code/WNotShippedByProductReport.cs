﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLWNotShippedByProductReport;
using System.Web.Script.Serialization;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WNotShippedByProductReport : System.Web.Services.WebService {

    public WNotShippedByProductReport()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    [WebMethod(EnableSession = true)]
    public string BindStatussalesperson()
    {
        try
        {

            PL_NotShippedProductReport pobj = new PL_NotShippedProductReport();
            BL_NotShippedProductReport.BindStatussalesperson(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string getNotShippedReport(string dataValue)
    {
        try
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_NotShippedProductReport pobj = new PL_NotShippedProductReport();
            if (jdv["FromDate"] != null && jdv["FromDate"] != "")
            {
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            }
            if (jdv["ToDate"] != null && jdv["ToDate"] != "")
            {
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            }
           
            pobj.ProductName = jdv["ProductName"];            
            pobj.SalsePersonAutoid = Convert.ToInt32(jdv["SalespersonAutoId"]);
            pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            BL_NotShippedProductReport.BindNotShipedReport(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
             
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod(EnableSession = true)]
    public string getNotShipedReportExport(string dataValue)
    {       
        try
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_NotShippedProductReport pobj = new PL_NotShippedProductReport();
            if (jdv["FromDate"] != null && jdv["FromDate"] != "")
            {
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            }
            if (jdv["ToDate"] != null && jdv["ToDate"] != "")
            {
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            }
            pobj.ProductName = jdv["ProductName"];
            pobj.SalsePersonAutoid = Convert.ToInt32(jdv["SalespersonAutoId"]);

            BL_NotShippedProductReport.NotShipedReportExport(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
 }
    

