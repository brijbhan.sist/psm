﻿using DLLOrderAverageReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for WOrderAverageReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WOrderAverageReport : System.Web.Services.WebService
{

    public WOrderAverageReport()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string GetOrderAverageReport(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderAverageReport pobj = new PL_OrderAverageReport();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);

                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                pobj.Employee = Convert.ToInt32(jdv["Employee"]);
                pobj.EmpType = Convert.ToInt32(jdv["EmpType"]);
                BL_OrderAverageReport.bindOrderAverageReport(pobj);

                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch
        {
            return "false";
        }

    }

    [WebMethod(EnableSession = true)]
    public string GetOrderAverageExporReport(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_OrderAverageReport pobj = new PL_OrderAverageReport();
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);

                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);

                BL_OrderAverageReport.ExportOrderAverageReport(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindEmployeeType()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_OrderAverageReport pobj = new PL_OrderAverageReport();
                BL_OrderAverageReport.GetEmployeeType(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindEmployee(string dataValues)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                PL_OrderAverageReport pobj = new PL_OrderAverageReport();
                pobj.EmpType = Convert.ToInt32(jdv["AutoID"]);
                BL_OrderAverageReport.GetEmployee(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
