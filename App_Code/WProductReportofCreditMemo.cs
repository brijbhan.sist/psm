﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLProductReportofCreditMemo;
using System.Web.Script.Serialization;
using DllUtility;
using System.Data;
/// <summary>
/// Summary description for WProductExchangeReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WProductReportofCreditMemo : System.Web.Services.WebService
{
    public WProductReportofCreditMemo()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string ProductReportofCreditMemo(string dataValue)
    {
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                PL_ProductReportofCreditMemo pobj = new PL_ProductReportofCreditMemo();
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
               
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPerson"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["Customer"]);

                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);

                BL_ProductReportofCreditMemo.getReport(pobj);           
                return pobj.Ds.GetXml();              
            }
            else
            {
                return "Session Expired";
            }
        }
        catch 
        {
            return "false";
        }       
    }

    [WebMethod (EnableSession = true)]
    public string BindDropdownlist()
    {
       try
       {
            PL_ProductReportofCreditMemo pobj = new PL_ProductReportofCreditMemo();
           if (Session["EmpAutoId"] != null)
           {
                BL_ProductReportofCreditMemo.bindDropdownList(pobj);
               if (!pobj.isException)
               {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                    if (json == "")
                    {
                        json = "[]";
                    }
                    return json;
                }
               else
               {
                   return "false";
               }
           }
           else
           {
               return "Session Expired";
           }
       }
        catch
       {
           return "false";
       }
    }
    [WebMethod(EnableSession = true)]
    public string BindCustomer(string SalesPersonAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                //var jss = new JavaScriptSerializer();
                //var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ProductReportofCreditMemo pobj = new PL_ProductReportofCreditMemo();
                pobj.SalesPersonAutoId = Convert.ToInt32(SalesPersonAutoId);
                BL_ProductReportofCreditMemo.BindCustomer(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                    if (json == "")
                    {
                        json = "[]";
                    }
                    return json;
                }
                else
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
