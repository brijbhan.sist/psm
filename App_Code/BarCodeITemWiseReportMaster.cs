﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLBarCodeITemWiseReportMaster;
using System.Web.Script.Serialization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class BarCodeITemWiseReportMaster : System.Web.Services.WebService {

    public BarCodeITemWiseReportMaster () {
    }
    [WebMethod(EnableSession =true)]
    public string BindBarCodeITemWiseReportMaster(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_BarCodeITemWiseReportMaster pobj = new PL_BarCodeITemWiseReportMaster();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
           
            BL_BarCodeITemWiseReportMaster.BindBarCodeITemWiseReportMaster(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }
}
