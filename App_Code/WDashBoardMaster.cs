﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllUtility;
using DLLDashboardMaster;
/// <summary>
/// Summary description for WDashBoardMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WDashBoardMaster : System.Web.Services.WebService {

    public WDashBoardMaster () {

        gZipCompression.fn_gZipCompression(); 
    }
    [WebMethod(EnableSession = true)]
    public string getDashboardList()
    {
        PL_DashboardMaster pobj = new PL_DashboardMaster();
        BL_DashboardMaster.select(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return "false";
        }

    }
    [WebMethod(EnableSession = true)]
    public string getSalesRevenue(string Type)
    {
        PL_DashboardMaster pobj = new PL_DashboardMaster();
        pobj.Type = Convert.ToInt32(Type);
        BL_DashboardMaster.getSalesRevenue(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return "false";
        }

    }
    [WebMethod(EnableSession = true)]
    public string CollectionDetails(string Type)
    {
        PL_DashboardMaster pobj = new PL_DashboardMaster();
        pobj.Type = Convert.ToInt32(Type);
        BL_DashboardMaster.CollectionDetails(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return "false";
        }

    }
    [WebMethod(EnableSession = true)]
    public string CreditMemoDetails(string Type)
    {
        PL_DashboardMaster pobj = new PL_DashboardMaster();
        pobj.Type = Convert.ToInt32(Type);
        BL_DashboardMaster.CreditMemoDetails(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return "false";
        }

    }
    [WebMethod(EnableSession = true)]
    public string getHoldPaymentList()
    {
        PL_DashboardMaster pobj = new PL_DashboardMaster();
        pobj.Type = Convert.ToInt32(Session["EmpTypeNo"]);
        BL_DashboardMaster.getHoldPaymentList(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return "false";
        }

    }
    [WebMethod(EnableSession = true)]
    public string getTicketList()
    {
        PL_DashboardMaster pobj = new PL_DashboardMaster();
        pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
        BL_DashboardMaster.getTicketList(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return "false";
        }

    }

    [WebMethod(EnableSession = true)]
    public string getNotification()
    {
        PL_DashboardMaster pobj = new PL_DashboardMaster();
        pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
        pobj.Emptype = Convert.ToInt32(Session["EmpTypeNo"]);
        BL_DashboardMaster.getNotification(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return "false";
        }

    }
    [WebMethod(EnableSession = true)]
    public string getNotification_Message(int AutoId)
    {
        PL_DashboardMaster pobj = new PL_DashboardMaster();
        pobj.AutoId = Convert.ToInt32(AutoId);
        pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
        BL_DashboardMaster.getNotification_Message(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return "false";
        }

    }

    [WebMethod(EnableSession = true)]
    public string getWebOrderList()
    {

        PL_DashboardMaster pobj = new PL_DashboardMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                BL_DashboardMaster.getWebOrderNotification(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
}

