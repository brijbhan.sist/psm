﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLCommissionMaster;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for WCommissionMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WCommissionMaster : System.Web.Services.WebService {

    public WCommissionMaster () {
    }

    [WebMethod(EnableSession = true)]
    public string InsertCommission(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            String CommissionMaster = dataValue;
            PL_CommissionMaster pobj = new PL_CommissionMaster();
            try
            {
                pobj.CommissionXml = CommissionMaster;
                pobj.Createdby = Convert.ToInt32(Session["EmpAutoId"]);
                BL_CommissionMaster.insertCommission(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getCommissionDetail(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_CommissionMaster pobj = new PL_CommissionMaster();
            try
            {
                BL_CommissionMaster.GetCommissionDetails(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
   [WebMethod(EnableSession = true)]
    public string editCommission(int CommissionId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_CommissionMaster pobj = new PL_CommissionMaster();
            pobj.Autoid = CommissionId;
            BL_CommissionMaster.editCommission(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string UpdateCommission(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            String CommissionMaster = dataValue;
            PL_CommissionMaster pobj = new PL_CommissionMaster();
            try
            {
                pobj.CommissionXml = CommissionMaster;
                pobj.Createdby = Convert.ToInt32(Session["EmpAutoId"]);
                BL_CommissionMaster.updateCommission(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
   [WebMethod(EnableSession = true)]
    public string deleteCommission(int CommissionId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_CommissionMaster pobj = new PL_CommissionMaster();
            try
            {
                pobj.Autoid = CommissionId;
                BL_CommissionMaster.DeleteCommission(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    
}
