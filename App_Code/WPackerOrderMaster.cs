﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllUtility;
using DLLPackerOrderMaster;
using Newtonsoft.Json;
using System.Data;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for WPackerOrderMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WPackerOrderMaster : System.Web.Services.WebService
{
    public WPackerOrderMaster()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string bindStatus()
    {
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_Packer_OrderMaster.bindStatus(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string getOrderList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderNo = jdv["OrderNo"];
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoid"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.Fromdate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.Todate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.OrderStatus = Convert.ToInt32(jdv["Status"]);
                pobj.PageIndex = jdv["pageIndex"];
                pobj.ShippingType = jdv["ShippingType"];
                pobj.PageSize = Convert.ToInt32(jdv["pageSize"]);
                BL_Packer_OrderMaster.getOrderList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string getOrderData(string OrderAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_Packer_OrderMaster.getOrderData(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string clickonSecurity(string CheckSecurity)
    {
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                BL_Packer_OrderMaster.clickonSecurity(pobj);
                if (!pobj.isException)
                {
                    if (pobj.Ds.Tables.Count > 0 && pobj.Ds.Tables[0].Rows.Count > 0)
                    {
                        return "true";
                    }
                    else
                    {
                        return "false";
                    }
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string AddProductQty(string dataValue)
    {
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                pobj.ReqQty = Convert.ToInt32(jdv["Qty"]);
                pobj.CheckSecurity = jdv["AddedItemKey"];
                BL_Packer_OrderMaster.AddProductQty(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    [WebMethod(EnableSession = true)]
    public string CheckStock(string dataValue)
    {
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["unitAutoId"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.ShipQty = Convert.ToInt32(jdv["QtyShip"]);
                BL_Packer_OrderMaster.CheckStocks(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();

                }
                else {

                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string generatePacking(string dataValues, string TableValues, string AllProduct)
    {
        DataTable dtOrder = new DataTable();
        DataTable dtAllProduct =new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        dtAllProduct = JsonConvert.DeserializeObject<DataTable>(AllProduct);
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.DTPackedItemsQty = dtOrder;
                }
                if (dtAllProduct.Rows.Count > 0)
                {
                    pobj.AllProduct = dtAllProduct;
                }

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.Remarks = jdv["PackerRemarks"];
                pobj.PackedBoxes = Convert.ToInt32(jdv["PackedBoxes"]);
                pobj.PackerAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_Packer_OrderMaster.genPacking(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    [WebMethod(EnableSession = true)]
    public string checkBarcode(PL_Packer_OrderMaster pobj)
    { 
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_Packer_OrderMaster.checkBarcode(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindUnitTypes(int productAutoId)
    {
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.ProductAutoId = productAutoId;
                BL_Packer_OrderMaster.bindUnitTypes(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getPackerAssignPrintData(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.EmployeeType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.ShippingAutoId = Convert.ToInt32(jdv["ShippingAutoId"]);
                BL_Packer_OrderMaster.getPackerAssignPrintData(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string UpdateUnitType(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitType"]);
                pobj.QtyPerUnit = Convert.ToInt32(jdv["QtyPerUnit"]);
                pobj.ReqQty = Convert.ToInt32(jdv["OrderQty"]);
                BL_Packer_OrderMaster.UpdateUnitType(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string clickonManagerSecurity(string dataValue)
    {
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);

                pobj.CheckSecurity = Convert.ToString(jdv["CheckSecurity"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.ReqQty = Convert.ToInt32(jdv["Quantity"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);

                BL_Packer_OrderMaster.checkManagerProvidedKey(pobj);
                if (!pobj.isException)
                {
                    if (pobj.Ds.Tables.Count > 0 && pobj.Ds.Tables[0].Rows.Count > 0)
                    {
                        return "true";
                    }
                    else
                    {
                        return "false";
                    }
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string clickonManagerSecurityBarcode(string dataValue)
    {
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);

                pobj.CheckSecurity = Convert.ToString(jdv["CheckSecurity"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.ReqQty = Convert.ToInt32(jdv["Qty"]);
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.BarCode = Convert.ToString(jdv["BarCode"]);
                BL_Packer_OrderMaster.checkManagerProvidedKeyBarcode(pobj);
                if (!pobj.isException)
                {
                    if (pobj.Ds.Tables.Count > 0 && pobj.Ds.Tables[0].Rows.Count > 0)
                    {
                        return "true";
                    }
                    else
                    {
                        return "false";
                    }
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string DeallocateProduct(string dataValue)
    {
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["productAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["unitautoid"]);
                pobj.ShipQty = Convert.ToInt32(jdv["qtyShip"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["isFree"]);
                pobj.IsExchange = Convert.ToInt32(jdv["isExchange"]);
                BL_Packer_OrderMaster.DeallocateProductQty(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else if (pobj.exceptionMessage == "Stock")
                {
                    return pobj.Ds.GetXml();
                }
                else if (pobj.exceptionMessage == "Less")
                {
                    return "Less";
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdateQtyManual(string dataValue)
    {
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);

              //  pobj.CheckSecurity = Convert.ToString(jdv["CheckSecurity"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.ReqQty = Convert.ToInt32(jdv["Quantity"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);

                BL_Packer_OrderMaster.UpdateQtyManual(pobj);
                if (!pobj.isException)
                {
                        return "true";
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string SaveAddBoxes(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.ReqQty = Convert.ToInt32(jdv["NoOfBoxes"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                BL_Packer_OrderMaster.SaveAddBoxes(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string barcode_Print(string OrderNo)
    {
        PL_Packer_OrderMaster pobj = new PL_Packer_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderNo = OrderNo;

                BL_Packer_OrderMaster.barcode_Print(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

}
