﻿using System;
using System.Web.Services;
using DllUsersecurityList;
using System.Web.Script.Serialization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class UsersecurityList : System.Web.Services.WebService
{

    [WebMethod(EnableSession = true)]
    public string getUsersecurityList()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_UsersecurityList pobj = new PL_UsersecurityList();
            try
            {
                BL_UsersecurityList.bindUsersecurityList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}
