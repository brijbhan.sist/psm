﻿using ItemWiseReport;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using DllUtility;

/// <summary>
/// Summary description for WItemWiseReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WItemWiseReport : System.Web.Services.WebService {

    public WItemWiseReport()
    {
        gZipCompression.fn_gZipCompression();
    }
    [WebMethod(EnableSession = true)]
    public object bindDropDown()
    {
        PL_ItemWiseReport pobj = new PL_ItemWiseReport();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_PL_ItemWiseReport.bindDropDown(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                JavaScriptSerializer j = new JavaScriptSerializer();
                j.MaxJsonLength = 999999999;
                return j.Deserialize(json, typeof(object)); ;
            }
            else
            {
                return "Session Expired";
            }
        }
        catch
        {
            return "false";
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindPrductSalesStockReport(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_ItemWiseReport pobj = new PL_ItemWiseReport();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageIndex = jdv["PageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_PL_ItemWiseReport.getProductSalesStockReport(pobj);               
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();               
            }
            else
            {
                return "Session Expired";
            }
        }
        catch
        {
            return "false";
        }
    }
    
}
