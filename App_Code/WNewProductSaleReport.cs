﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using DllUtility;
using DLLNewProductSaleReport;
using System.Data;
using Newtonsoft.Json;

/// <summary>
/// Summary description for WNewProductSaleReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WNewProductSaleReport : System.Web.Services.WebService
{

    public WNewProductSaleReport()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string BindCustomer()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_NewProductSalesReport pobj = new PL_NewProductSalesReport();
                BL_NewProductSalesReport.BindCustomer(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return json;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetReportDetail(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_NewProductSalesReport pobj = new PL_NewProductSalesReport();
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.PriceLevelAutoId = Convert.ToInt32(jdv["PriceLevelAutoId"]);
                pobj.SalesPersonString = jdv["SalesAutoId"];
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_NewProductSalesReport.BindProductSaleReport(pobj);
                return pobj.Ds.GetXml();


            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}
