﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllOrderMaster;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
using DllUtility;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class orderList : System.Web.Services.WebService
{
    public orderList()
    {
        gZipCompression.fn_gZipCompression();
    }
    [WebMethod(EnableSession = true)]
    public string bindStatus()
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_OrderMaster.bindStatus(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getOrderList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderNo = jdv["OrderNo"];
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoid"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.Fromdate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.Todate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.OrderStatus = Convert.ToInt32(jdv["Status"]);
                pobj.PageIndex = jdv["pageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                if (Session["EmpType"].ToString() == "Sales Person")
                {
                    pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                }
                pobj.PackerAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.LoginEmpType = Convert.ToInt32(Session["empTypeno"].ToString());
                pobj.TypeShipping = jdv["ShippingType"];
                BL_OrderMaster.selectOrderList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getDelOrdersList(string dataValues)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            PL_OrderMaster pobj = new PL_OrderMaster();
            try
            {
                pobj.OrderNo = jdv["OrderNo"];
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDelDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDelDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = jdv["PageIndex"];
                if (jdv["PayableAmount"] != null && jdv["PayableAmount"] != "")
                {
                    pobj.PayableAmount = Convert.ToDecimal(jdv["PayableAmount"]);
                }
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                pobj.CustomerTypeAutoId = Convert.ToInt32(jdv["CustomerType"]);
                pobj.DrvAutoId = Convert.ToInt32(jdv["DriverAutoId"]);
                pobj.OrderType = Convert.ToInt32(jdv["OrderType"]);
                pobj.OrderStatus = Convert.ToInt32(jdv["Status"]);
                BL_OrderMaster.getDelOrderList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "session";
        }
    }

    [WebMethod(EnableSession = true)]
    public string deleteOrder(int OrderAutoId)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            pobj.OrderAutoId = OrderAutoId;
            BL_OrderMaster.delete(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.exceptionMessage;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }


    [WebMethod(EnableSession = true)]
    public string getDraftOrderList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CustomerName = jdv["CustomerName"];
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.Fromdate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.Todate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageIndex = jdv["pageIndex"];
                if (Session["EmpType"].ToString() == "Sales Person" || Session["EmpType"].ToString() == "Warehouse Manager")
                {
                    pobj.SalesPersonAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_OrderMaster.DraftOrderList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteDraftOrder(int DraftAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_OrderMaster pobj = new PL_OrderMaster();
            try
            {
                pobj.DraftAutoId = Convert.ToInt32(DraftAutoId);
                BL_OrderMaster.deleteDraft(pobj);
                if (!pobj.isException)
                {
                    return "deleted";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindCustomer(string Customertype, string SalesPersonAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_OrderMaster pobj = new PL_OrderMaster();
                pobj.@CustomerTypeAutoId = Convert.ToInt32(Customertype);
                pobj.SalesPersonAutoId = Convert.ToInt32(SalesPersonAutoId);
                BL_OrderMaster.BindCustomer(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
