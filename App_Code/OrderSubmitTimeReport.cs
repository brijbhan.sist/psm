﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLOrderSubmitTimeReport;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for OrderSubmitTimeReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class OrderSubmitTimeReport : System.Web.Services.WebService {

    public OrderSubmitTimeReport () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string GetOrderSubmitReport(string dataValue)
    {
        try
        {
            PL_OrderSubmitTimeReport pobj = new PL_OrderSubmitTimeReport();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            if (jdv["DateFrom"] != null && jdv["DateFrom"] != "")
            {
                pobj.FromDate = Convert.ToDateTime(jdv["DateFrom"]);
            }
            if (jdv["DateTo"] != null && jdv["DateTo"] != "")
            {
                pobj.ToDate = Convert.ToDateTime(jdv["DateTo"]);
            }
            pobj.SalePersonAutoId = Convert.ToInt32(jdv["PackerAutoId"]);
            pobj.RouteStatus = Convert.ToInt32(jdv["RouteStatus"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            BL_OrderSubmitTimeReport.GetOrderSubmit(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string CutomerName()
    {
        try
        {
            PL_OrderSubmitTimeReport pobj = new PL_OrderSubmitTimeReport();
            //var jss = new JavaScriptSerializer();
            //var jdv = jss.Deserialize<dynamic>(dataValue);            
            BL_OrderSubmitTimeReport.GetCustomer(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    
}
