﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
using DllUtility;
using DllPurchaseOrder;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class PurchaseOrderList : System.Web.Services.WebService
{
    public PurchaseOrderList()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string getOrderList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                if (jdv["PoNo"] != null && jdv["PoNo"] != "")
                {
                    pobj.OrderNo = jdv["PoNo"];
                }
                if (jdv["VenderAutoId"] != null && jdv["VenderAutoId"] != "0")
                {
                    pobj.VenderAutoId = Convert.ToInt32(jdv["VenderAutoId"]);
                }
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.OrderStatus = Convert.ToInt32(jdv["Status"]);              
                pobj.PoStatus = jdv["PoStatus"];            
                pobj.PageIndex = jdv["pageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_PurchaseOrder.selectOrderList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getDelOrdersList(string dataValues)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            PL_PurchaseOrder pobj = new PL_PurchaseOrder();
            try
            {
                pobj.OrderNo = jdv["OrderNo"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = jdv["PageIndex"];
                pobj.OrderStatus = Convert.ToInt32(jdv["Status"]);
                BL_PurchaseOrder.getDelOrderList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "session";
        }
    }

    [WebMethod(EnableSession = true)]
    public string deleteOrder(int OrderAutoId)
    {
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            pobj.POAutoId = OrderAutoId;
            BL_PurchaseOrder.delete(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.exceptionMessage;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string UpdateStock(string data)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(data);
        PL_PurchaseOrder pobj = new PL_PurchaseOrder();
        try
        {
            pobj.EmpId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.POAutoId = Convert.ToInt32(jdv["POAutoId"]);
            pobj.OrderNo = jdv["PONo"];
            pobj.ReciveStockRemark = jdv["Remark"];
            pobj.VenderAutoId = Convert.ToInt32(jdv["VenderAutoId"]);
            BL_PurchaseOrder.UpdateStock(pobj);
            if (!pobj.isException)
            {
                return pobj.exceptionMessage;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string deleteDraftOrder(int DraftAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_PurchaseOrder pobj = new PL_PurchaseOrder();
            try
            {
                pobj.DraftAutoId = Convert.ToInt32(DraftAutoId);
                BL_PurchaseOrder.deleteDraft(pobj);
                if (!pobj.isException)
                {
                    return "deleted";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
