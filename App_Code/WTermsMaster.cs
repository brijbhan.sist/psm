﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLTermsMaster;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for WTermsMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WTermsMaster : System.Web.Services.WebService {

    [WebMethod(EnableSession = true)]
    public string insertTerms(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Terms pobj = new PL_Terms();
            try
            {
                pobj.TermsName = jdv["TermsName"];
                pobj.Description = jdv["Description"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_Terms.insert(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }		
    }
    [WebMethod(EnableSession = true)]
    public string getTermsDetail(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Terms pobj = new PL_Terms();
            try
            {
                pobj.TermsId = jdv["TermsId"];
                pobj.TermsName = jdv["TermsName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_Terms.select(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
     [WebMethod(EnableSession = true)]
    public string editTerms(string TermsId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Terms pobj = new PL_Terms();
            pobj.TermsId = TermsId;
            BL_Terms.editTerms(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }
     [WebMethod(EnableSession = true)]
    public string updateTerms(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_Terms pobj = new PL_Terms();
            try
            {
                pobj.TermsId = jdv["TermsId"];
                pobj.TermsName = jdv["TermsName"];
                pobj.Description = jdv["Description"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_Terms.update(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteTerms(string TermsId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Terms pobj = new PL_Terms();
            try
            {
                pobj.TermsId = TermsId;
                BL_Terms.delete(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
}
