﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllShowMessage;
using DllUtility;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WShowMessage : System.Web.Services.WebService {
    public WShowMessage()
    {
        gZipCompression.fn_gZipCompression();
    }
    [WebMethod(EnableSession = true)]
    public string ShowMessage()
    {
        PL_ShowMessage pobj = new PL_ShowMessage();
        if (Session["EmpAutoId"] != null)
        {
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.EmpType = Convert.ToInt32(Session["EmpTypeNo"]);
            BL_ShowMessage.ShowMessage(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string readmessage( string AutoId)
    {
        PL_ShowMessage pobj = new PL_ShowMessage();
        if (Session["EmpAutoId"] != null)
        {
            pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
            pobj.AutoId = Convert.ToInt32(AutoId);
            BL_ShowMessage.readmessage(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
