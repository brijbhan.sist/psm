﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllWarehouseOrderMaster;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
using DllUtility;
/// <summary>
/// Summary description for CopyOrderList
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WarehouseOrderList : System.Web.Services.WebService {

    public WarehouseOrderList()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string bindStatus()
    {
        PL_COrderMaster pobj = new PL_COrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_COrderMaster.bindStatus(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getOrderList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_COrderMaster pobj = new PL_COrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderNo = jdv["OrderNo"];
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoid"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.Fromdate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.Todate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.OrderStatus = Convert.ToInt32(jdv["Status"]);
                pobj.PageIndex = jdv["pageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                if (Session["EmpType"].ToString() == "Sales Person")
                {
                    pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                }
                pobj.PackerAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.LoginEmpType = Convert.ToInt32(Session["empTypeno"].ToString());
                pobj.TypeShipping = jdv["ShippingType"];
                BL_COrderMaster.selectOrderList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }



    //[WebMethod(EnableSession = true)]
    //public string deleteOrder(int OrderAutoId)
    //{
    //    PL_COrderMaster pobj = new PL_COrderMaster();
    //    try
    //    {
    //        pobj.OrderAutoId = OrderAutoId;
    //        BL_COrderMaster.delete(pobj);
    //        if (!pobj.isException)
    //        {
    //            this.Context.Response.ContentType = "application/json; charset=utf-8";
    //            this.Context.Response.StatusCode = 200;
    //            return pobj.exceptionMessage;
    //        }
    //        else
    //        {
    //            return pobj.exceptionMessage;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        return "Oops! Something went wrong.Please try later.";
    //    }
    //}



    [WebMethod(EnableSession = true)]
    public string getDriverList(int LoginEmpType)
    {
        PL_COrderMaster pobj = new PL_COrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.LoginEmpType = LoginEmpType;
                BL_COrderMaster.getDriverList(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getPackerList(int OrderAutoId)
    {
        PL_COrderMaster pobj = new PL_COrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = OrderAutoId;
                BL_COrderMaster.getPackerList(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string assignPacker(string dataValues, string TableValues)
    {
        DataTable OrderId = new DataTable();
        OrderId = JsonConvert.DeserializeObject<DataTable>(TableValues);

        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);

        PL_COrderMaster pobj = new PL_COrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (OrderId.Rows.Count > 0)
                {
                    pobj.TableValueMultiOrder = OrderId;
                }

               // pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.PackerAutoId = Convert.ToInt32(jdv["PackerAutoId"]);
                if (jdv["Times"] != "")
                {
                    pobj.Times = Convert.ToInt32(jdv["Times"]);
                }
                else
                {
                    pobj.Times = 0;
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.Remarks = jdv["Remarks"];
                BL_COrderMaster.assignPacker(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindCustomer(string SalesPersonAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_COrderMaster pobj = new PL_COrderMaster();
                pobj.SalesPersonAutoId = Convert.ToInt32(SalesPersonAutoId);
                BL_COrderMaster.BindCustomer(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
