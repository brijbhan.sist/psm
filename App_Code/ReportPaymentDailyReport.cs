﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLReportPaymentDailyReport;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
using DllUtility;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
 [System.Web.Script.Services.ScriptService]
public class ReportPaymentDailyReport : System.Web.Services.WebService {

    public ReportPaymentDailyReport () {

        gZipCompression.fn_gZipCompression();
    }

     [WebMethod(EnableSession = true)]
    public string GetReportPaymentDailyReport(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ReportPaymentDailyReport pobj = new PL_ReportPaymentDailyReport();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt16(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt16(jdv["PageIndex"]);
                BL_ReportPaymentDailyReport.GetReportPaymentDailyReport(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

     [WebMethod(EnableSession = true)]
     public string GetPaymentDailyReportLog(string dataValue)
     {
         var jss = new JavaScriptSerializer();
         var jdv = jss.Deserialize<dynamic>(dataValue);
         PL_ReportPaymentDailyReport pobj = new PL_ReportPaymentDailyReport();
         try
         {
             if (Session["EmpAutoId"] != null)
             {
                 if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                 {
                     pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                 }               
                 pobj.PageIndex = Convert.ToInt16(jdv["PageIndex"]);
                 BL_ReportPaymentDailyReport.GetPaymentDailyReportLog(pobj);
                 if (!pobj.isException)
                 {
                     return pobj.Ds.GetXml();
                 }
                 else
                 {
                     return pobj.exceptionMessage;
                 }
             }
             else
             {
                 return "Session Expired";
             }

         }
         catch (Exception ex)
         {
             return ex.Message;
         }
     }
    
}
