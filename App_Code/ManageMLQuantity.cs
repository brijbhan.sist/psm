﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllCustomPriceTemplate;
using System.Data;
using System.Web.Script.Serialization;
using DllManageMLQuantity;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ManageMLQuantity : System.Web.Services.WebService
{

    [WebMethod(EnableSession = true)]
    public string getProductList(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ManageMLQuantity pobj = new PL_ManageMLQuantity();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.CategoryAutoId = jdv["CategoryAutoId"];
                pobj.SubcategoryAutoId = jdv["SubcategoryAutoId"];
                pobj.ProductName = jdv["ProductName"];
                if (jdv["Productid"] == "")
                {
                    pobj.ProductAutoId = 0;
                }
                else
                {
                    pobj.ProductAutoId = Convert.ToInt32(jdv["Productid"]);
                }
                BL_ManageMLQuantity.getProductList(pobj);

                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }


    [WebMethod(EnableSession = true)]
    public string UpdateML_And_Weight(string dataValue)
    {
        //var jss = new JavaScriptSerializer();
        //var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ManageMLQuantity pobj = new PL_ManageMLQuantity();
        if (Session["EmpAutoId"] != null)
        {
            string data = dataValue;
            try
            {
                pobj.Xml = data;
                pobj.Updatedby = Convert.ToInt32(Session["EmpAutoId"]);
                BL_ManageMLQuantity.UpdateMlQuantity(pobj);

                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
