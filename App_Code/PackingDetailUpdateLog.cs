﻿using System;
using System.Web.Services;
using DllPackingDetailUpdateLog;
using System.Web.Script.Serialization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class PackingDetailUpdateLog : System.Web.Services.WebService
{

    string Body = "";
    [WebMethod(EnableSession = true)]
    public string bindProduct()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_PackingDetails pobj = new PL_PackingDetails();
            try
            {
                BL_PackingDetails.bindProduct(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getProductList(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {


            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_PackingDetails pobj = new PL_PackingDetails();
            try
            {
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_PackingDetails.select(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}
