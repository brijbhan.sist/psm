﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLPrintOrderMultiple;

/// <summary>
/// Summary description for PrintOrderMultiple
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class PrintOrderMultiple : System.Web.Services.WebService
{

    public PrintOrderMultiple()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string GetOrderDetails(string OrderAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_PrintOrderMultiple pobj = new PL_PrintOrderMultiple();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);

                  pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_PrintOrderMultiple.getOrderData(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }
}
