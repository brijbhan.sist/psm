﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllOrderMaster;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
using DllUtility;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class orderMaster : System.Web.Services.WebService
{
    public orderMaster()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string bindDropdown()
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.SalesPersonAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                BL_OrderMaster.bindDropdown(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindAllDropdown()
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.SalesPersonAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                BL_OrderMaster.bindAllDropdown(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return json;
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindZipcodeDropDowns()
    {
        PL_OrderMaster pobj = new PL_OrderMaster();

        BL_OrderMaster.bindZipcodeDropDowns(pobj);
        if (Session["EmpAutoId"] != null)
        {
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return json;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindPinCodeForCity(string ZipAutoId)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        pobj.ZipAutoId = Convert.ToInt32(ZipAutoId);
        BL_OrderMaster.BindPinCodeForCity(pobj);
        return pobj.Ds.GetXml();

    }
    [WebMethod(EnableSession = true)]
    public string selectAddress(int customerAutoId, string DraftAutoId)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = customerAutoId;
                if (DraftAutoId != "")
                    pobj.DraftAutoId = Convert.ToInt32(DraftAutoId);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_OrderMaster.selectAddress(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindUnitType(int productAutoId)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.ProductAutoId = productAutoId;
                BL_OrderMaster.bindUnitType(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string selectQtyPrice(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                BL_OrderMaster.selectQtyPrice(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    //  public string insertOrderData(string TableValues, string orderData)
    public string insertOrderData(string orderData)
    {
        //DataTable dtOrder = new DataTable();
        //dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(orderData);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.BillAddrAutoId = Convert.ToInt32(jdv["BillAddrAutoId"]);
                pobj.ShipAddrAutoId = Convert.ToInt32(jdv["ShipAddrAutoId"]);
                if (Session["EmpType"].ToString() != "Admin")
                {
                    pobj.SalesPersonAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                if (jdv["OverallDisc"] != "")
                    pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                if (jdv["OverallDiscAmt"] != "")
                    pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                if (jdv["ShippingCharges"] != "")
                    pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharges"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
                pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
                pobj.Remarks = jdv["OrderRemarks"];
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.MLTax = Convert.ToDecimal(jdv["MLTax"]);
                pobj.AdjustmentAmt = Convert.ToDecimal(jdv["AdjustmentAmt"]);

                BL_OrderMaster.insert(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string editOrder(string orderNo, string loginEmpType)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderNo = orderNo;
                pobj.LoginEmpType = Convert.ToInt32(loginEmpType);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_OrderMaster.editOrderDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string updateOrder(string TableValues, string orderData)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(orderData);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }
                if (jdv["DeliveryDate"] != null && jdv["DeliveryDate"] != "")
                {
                    pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
                }
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.Remarks = jdv["Remarks"];
                pobj.ManagerRemarks = jdv["ManagerRemarks"];
                pobj.BillAddrAutoId = Convert.ToInt32(jdv["BillAddrAutoId"]);
                pobj.OrderStatus = Convert.ToInt32(jdv["Status"]);
                pobj.ShipAddrAutoId = Convert.ToInt32(jdv["ShipAddrAutoId"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharges"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
                pobj.ShippingType = jdv["ShippingType"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.CreditNo = jdv["CreditNo"];
                if (jdv["CreditAmount"] != "")
                    pobj.CreditAmount = Convert.ToDecimal(jdv["CreditAmount"]);
                if (jdv["DeductionAmount"] != "")
                    pobj.CreditMemoAmount = Convert.ToDecimal(jdv["DeductionAmount"]);
                if (jdv["PayableAmount"] != "")
                    pobj.PayableAmount = Convert.ToDecimal(jdv["PayableAmount"]);
                if (jdv["PackedBoxes"] != null || jdv["PackedBoxes"] != "")
                {
                    pobj.PackedBoxes = Convert.ToInt32(jdv["PackedBoxes"]);
                }
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.MLTax = Convert.ToDecimal(jdv["MLTax"]);
                pobj.AdjustmentAmt = Convert.ToDecimal(jdv["AdjustmentAmt"]);
                pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
                BL_OrderMaster.updateOrder(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string updatePackedOrder(string TableValues, string orderData)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(orderData);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.DTPackedItemsQty = dtOrder;
                }
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.BillAddrAutoId = Convert.ToInt32(jdv["BillAddrAutoId"]);
                pobj.ShipAddrAutoId = Convert.ToInt32(jdv["ShipAddrAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharges"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);

                BL_OrderMaster.updatePackedOrder(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success!!";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getAddressList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AddrType = Convert.ToInt32(jdv["AddrType"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);

                BL_OrderMaster.getAddressList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string addNewAddr()
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            BL_OrderMaster.addNewAddr(pobj);
            return pobj.Ds.GetXml();
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string saveAddr(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.AddrType = Convert.ToInt32(jdv["AddrType"]);
                pobj.Address = jdv["Address"];
                pobj.Address2 = jdv["Address2"];
                pobj.Zipcode = jdv["Zipcode"];
                pobj.City = jdv["City"];
                pobj.StateName = jdv["State"];
                pobj.Lat = jdv["Lat"];
                pobj.Long = jdv["Long"];;

                BL_OrderMaster.insertAddr(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteAddr(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AddrType = Convert.ToInt32(jdv["AddrType"]);
                pobj.AddrAutoId = Convert.ToInt32(jdv["AddressAutoId"]);
                BL_OrderMaster.deleteAddr(pobj);
                if (!pobj.isException)
                {

                    return pobj.exceptionMessage;
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string changeAddr(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AddrType = Convert.ToInt32(jdv["AddrType"]);
                pobj.AddrAutoId = Convert.ToInt32(jdv["AddressAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                BL_OrderMaster.changeAddr(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string generatePacking(string dataValues, string TableValues)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);

        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.DTPackedItemsQty = dtOrder;
                }

                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);

                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharges"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.Remarks = jdv["PackerRemarks"];
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
                pobj.PackedBoxes = Convert.ToInt32(jdv["PackedBoxes"]);
                pobj.PackerAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.MLTax = Convert.ToDecimal(jdv["MLTax"]);
                pobj.AdjustmentAmt = Convert.ToDecimal(jdv["AdjustmentAmt"]);
                BL_OrderMaster.genPacking(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string checkBarcode(string Barcode)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.Barcode = Barcode;
                BL_OrderMaster.checkBarcode(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string AddProductQty(string ProductAutoId, string UnitAutoId)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.ProductAutoId = Convert.ToInt32(ProductAutoId);
                pobj.UnitAutoId = Convert.ToInt32(UnitAutoId);
                BL_OrderMaster.AddProductQty(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getDriverList(string OrderAutoId, int LoginEmpType)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.LoginEmpType = LoginEmpType;
                BL_OrderMaster.getDriverList(pobj);
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string AssignDriver(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.DrvAutoId = Convert.ToInt32(jdv["DrvAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                if (jdv["AssignDate"] != "")
                {
                    pobj.AsgnDate = Convert.ToDateTime(jdv["AssignDate"]);
                }
                BL_OrderMaster.AssignDriver(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string assignPacker(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.PackerAutoId = Convert.ToInt32(jdv["PackerAutoId"]);
                if (jdv["Times"] != "")
                {
                    pobj.Times = Convert.ToInt32(jdv["Times"]);
                }
                else
                {
                    pobj.Times = 0;
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.Remarks = jdv["Remarks"];
                BL_OrderMaster.assignPacker(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string payDueAmount(string PaymentValues, string CustAutoId)
    {
        DataTable dtPayment = new DataTable();
        dtPayment = JsonConvert.DeserializeObject<DataTable>(PaymentValues);

        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtPayment.Rows.Count > 0)
                {
                    pobj.Payment = dtPayment;
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(CustAutoId);
                BL_OrderMaster.payDueAmount(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string barcode_Print(string OrderNo)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderNo = OrderNo;

                BL_OrderMaster.barcode_Print(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string cancelOrder(string OrderAutoId, string Remarks)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.Remarks = Remarks;
                BL_OrderMaster.cancelOrder(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string ShowTop25SellingProducts(int customerAutoId)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = customerAutoId;
                BL_OrderMaster.ShowTop25SellingProducts(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetBarDetails(string dataValues)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                if (jdv["DraftAutoId"] != "")
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
                if (jdv["DeliveryDate"] != "")
                    pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
                if (jdv["ReqQty"] != "")
                    pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                else
                    pobj.ReqQty = 1;
                pobj.Barcode = jdv["Barcode"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.IsTaxable = Convert.ToInt32(jdv["IsTaxable"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                pobj.Oim_Discount = Convert.ToDecimal(jdv["Oim_Discount"]); 
                BL_OrderMaster.GetBarDetails(pobj);
                if(!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
                
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string SaveDraftOrder(string dataValues)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                if (jdv["DraftAutoId"] != "")
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
                if (jdv["DeliveryDate"] != "")
                    pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["productAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["unitAutoId"]);
                pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                pobj.QtyPerUnit = Convert.ToInt32(jdv["QtyPerUnit"]);
                pobj.UnitPrice = Convert.ToDecimal(jdv["UnitPrice"]);
                if (jdv["minprice"] != "")
                {
                    pobj.minprice = Convert.ToDecimal(jdv["minprice"]);
                }
                else
                {
                    pobj.minprice = 0;
                }
                pobj.SRP = Convert.ToDecimal(jdv["SRP"]);
                pobj.GP = Convert.ToDecimal(jdv["GP"]);
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.IsTaxable = Convert.ToInt32(jdv["IsTaxable"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.Remarks = jdv["Remarks"];
                pobj.Oim_Discount = Convert.ToDecimal(jdv["Oim_Discount"]);
                pobj.Oim_DiscountAmount = Convert.ToDecimal(jdv["Oim_DiscountAmount"]);
                BL_OrderMaster.SaveDraftOrder(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.DraftAutoId.ToString();
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string updateDraftOrder(string dataValues)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
                if (jdv["DeliveryDate"] != "")
                    pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
                pobj.Remarks = jdv["Remark"];
                pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharge"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["DiscAmt"]);
                BL_OrderMaster.UpdateDraftOrder(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.DraftAutoId.ToString();
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string UpdateDraftReq(string dataValues)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                if (jdv["ReqQty"] != "")
                    pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                if (jdv["UnitPrice"] != "")
                    pobj.UnitPrice = Convert.ToDecimal(jdv["UnitPrice"]);

                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.ShippingType= Convert.ToInt32(jdv["ShippingType"]);
                if (jdv["DeliveryDate"] != "")
                    pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
                pobj.Remarks = jdv["Remark"];
                pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharge"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["DiscAmt"]);
                pobj.Oim_Discount = Convert.ToDecimal(jdv["Oim_Discount"]);
                pobj.Oim_DiscountAmount = Convert.ToDecimal(jdv["Oim_DiscountAmount"]);
                BL_OrderMaster.UpdateDraftReq(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string EditDraftOrder(string DraftAutoId, string loginEmpType)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.DraftAutoId = Convert.ToInt32(DraftAutoId);
                pobj.LoginEmpType = Convert.ToInt32(loginEmpType);
                BL_OrderMaster.editDraftDetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }



    [WebMethod(EnableSession = true)]
    public string deductionPayamount(string TableValues, string OrderNo)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.CreditTable = dtOrder;
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.OrderNo = OrderNo;
                BL_OrderMaster.ApplyCreditMemo(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string deleteDeductionAmount(string LogAutoId)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.LogAutoId = Convert.ToInt32(LogAutoId);
                BL_OrderMaster.deleteDeductionAmount(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string SetAsProcess(string OrderAutoId)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_OrderMaster.SetAsProcess(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string DeleteDraftItem(string DataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(DataValue);
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["isfreeitem"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                BL_OrderMaster.DeleteDraftItem(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string clickonSecurity(string CheckSecurity)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                BL_OrderMaster.clickonSecurity(pobj);
                if (!pobj.isException)
                {
                    if (pobj.Ds.Tables.Count > 0 && pobj.Ds.Tables[0].Rows.Count > 0)
                    {
                        return "true";
                    }
                    else
                    {
                        return "false";
                    }
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string ItemTypeUpdate(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {

            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_OrderMaster pobj = new PL_OrderMaster();
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.DraftAutoId = Convert.ToInt32(jdv["ItemAutoId"]);

                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                pobj.IsTaxable = Convert.ToInt32(jdv["IsTaxable"]);
                BL_OrderMaster.ItemTypeUpdate(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";

                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetBarDetailsOrd(string dataValues)
    {
        PL_OrderMaster pobj = new PL_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                if (jdv["DraftAutoId"] != "")
                    pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
                pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
                pobj.Remarks = jdv["Remarks"];
                if (jdv["DeliveryDate"] != "")
                    pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
                if (jdv["ReqQty"] != "")
                    pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                else
                    pobj.ReqQty = 1;
                pobj.Barcode = jdv["Barcode"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.IsTaxable = Convert.ToInt32(jdv["IsTaxable"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                pobj.Oim_Discount= Convert.ToDecimal(jdv["Oim_Discount"]);
                BL_OrderMaster.GetBarDetailsOrd(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }


    [WebMethod(EnableSession = true)]
    public string approveOrder(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {

            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_OrderMaster pobj = new PL_OrderMaster();
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                BL_OrderMaster.approveOrder(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return "false";

                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

}
