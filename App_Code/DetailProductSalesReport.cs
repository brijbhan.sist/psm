﻿using System;
using System.Web.Services;
using DLLDetailProductSalesReport;
using System.Web.Script.Serialization;
using DllUtility;
/// <summary>
/// Summary description for DetailProductSalesReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class DetailProductSalesReport : System.Web.Services.WebService
{
    public DetailProductSalesReport()
    {
        gZipCompression.fn_gZipCompression();
    }
    [WebMethod(EnableSession = true)]
    public string BindCustomer()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_DetailProductSalesRepor pobj = new PL_DetailProductSalesRepor();
                BL_DetailProductSalesRepor.BindCustomer(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindProductCategory()
    {
        PL_DetailProductSalesRepor pobj = new PL_DetailProductSalesRepor();
        BL_DetailProductSalesRepor.BindProductCategory(pobj);
        return pobj.Ds.GetXml();
    }
    [WebMethod(EnableSession = true)]
    public string BindProductSubCategory(string CategoryAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_DetailProductSalesRepor pobj = new PL_DetailProductSalesRepor();
                pobj.CategoryAutoId = Convert.ToInt32(CategoryAutoId);
                BL_DetailProductSalesRepor.BindProductSubCategory(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetReportDetail(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_DetailProductSalesRepor pobj = new PL_DetailProductSalesRepor();
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SalesPerson = Convert.ToInt32(jdv["SalesAutoId"]);
                pobj.SubCategoryId = Convert.ToInt32(jdv["SubCategoryAutoId"]);
                pobj.BrandId = jdv["BrandId"];
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_DetailProductSalesRepor.BindProductSaleReport(pobj);
                return pobj.Ds.GetXml();


            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string BindProductByBrand(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_DetailProductSalesRepor pobj = new PL_DetailProductSalesRepor();
                pobj.CategoryAutoId = Convert.ToInt32(jdv["CategoryAutoId"]);
                pobj.SubCategoryId = Convert.ToInt32(jdv["SubCategoryAutoId"]);
                pobj.BrandId = jdv["BrandId"];
                BL_DetailProductSalesRepor.BindProductByBrand(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string CustomerDropdown(string SalesPersonAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_DetailProductSalesRepor pobj = new PL_DetailProductSalesRepor();
                pobj.SalesPerson = Convert.ToInt32(SalesPersonAutoId);
                BL_DetailProductSalesRepor.CustomerDropdown(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {

                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
