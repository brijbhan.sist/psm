﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLLRequestProduct;
using System.Web.Script.Serialization;
using DllUtility;
using System.Data;
using Newtonsoft.Json;
using System.Data.SqlClient;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class RequestProductMaster : System.Web.Services.WebService
{
    string Body = "";
    public RequestProductMaster()
    {
        gZipCompression.fn_gZipCompression();
    }
    [WebMethod(EnableSession = true)]
    public string bindCategory()
    {
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_Product.bindCategory(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string bindSubcategory(string categoryAutoId)
    {
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.CategoryAutoId = Convert.ToInt32(categoryAutoId);
                BL_Product.bindSubcategory(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "false";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getManagePrice(string ProductAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            PL_Product pobj = new PL_Product();
            pobj.AutoId = Convert.ToInt32(ProductAutoId); ;
            BL_Product.getUnitMaster(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }
    }


       [WebMethod(EnableSession = true)]
    public string editProduct(string ProductId)
    {
        PL_Product pobj = new PL_Product();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(ProductId);
                BL_Product.editProduct(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return "Oops, Something went wrong .please try later.";
        }
    }

       //[WebMethod(EnableSession = true)]
       //public string checkUniqueBarcode(string dataValue)
       //{
       //    var jss = new JavaScriptSerializer();
       //    var jdv = jss.Deserialize<dynamic>(dataValue);
       //    PL_Product pobj = new PL_Product();
       //    try
       //    {
       //        if (Session["EmpAutoId"] != null)
       //        {
                   
       //            pobj.Barcode = jdv["barcode"];
       //            BL_Product.checkUnique(pobj);
       //            if (!pobj.isException)
       //            {
       //                this.Context.Response.ContentType = "application/json; charset=utf-8";
       //                this.Context.Response.StatusCode = 200;
       //                return pobj.Ds.GetXml();
       //            }
       //            else
       //            {
       //                return pobj.exceptionMessage;
       //            }
       //        }
       //        else
       //        {
       //            return "Session Expired";
       //        }

       //    }
       //    catch (Exception ex)
       //    {
       //        return "Oops, Something went wrong .please try later.";
       //    }
       //} 

    
}
