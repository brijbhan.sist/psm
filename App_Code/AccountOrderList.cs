﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
using DllUtility;
using DllAccountOrderList;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class AccountOrderList : System.Web.Services.WebService
{
    public AccountOrderList()
    {
        gZipCompression.fn_gZipCompression();
    }
    [WebMethod(EnableSession = true)]
    public string bindStatus()
    {
        PL_AccountOrderList pobj = new PL_AccountOrderList();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_AccountOrderList.bindStatus(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getDelOrdersList(string dataValues)
    {
        if (Session["EmpAutoId"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValues);
            PL_AccountOrderList pobj = new PL_AccountOrderList();
            try
            {
                pobj.OrderNo = jdv["OrderNo"];
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDelDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDelDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = jdv["PageIndex"];
                if (jdv["PayableAmount"] != null && jdv["PayableAmount"] != "")
                {
                    pobj.PayableAmount = Convert.ToDecimal(jdv["PayableAmount"]);
                }
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.SalesPersonAutoId = Convert.ToInt32(jdv["SalesPersonAutoId"]);
                pobj.CustomerTypeAutoId = Convert.ToInt32(jdv["CustomerType"]);
                pobj.ShippingType= Convert.ToInt32(jdv["ShippingType"]); 
                pobj.DrvAutoId = Convert.ToInt32(jdv["DriverAutoId"]);
                pobj.OrderType = Convert.ToInt32(jdv["OrderType"]);
                pobj.OrderStatus = Convert.ToInt32(jdv["Status"]);
                BL_AccountOrderList.getDelOrderList(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "session";
        }
    }

    [WebMethod(EnableSession = true)]
    public string BindCustomer(string Customertype, string SalesPersonAutoId)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_AccountOrderList pobj = new PL_AccountOrderList();
                pobj.@CustomerTypeAutoId = Convert.ToInt32(Customertype);
                pobj.SalesPersonAutoId = Convert.ToInt32(SalesPersonAutoId);
                BL_AccountOrderList.BindCustomer(pobj);
                return pobj.Ds.GetXml();
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
