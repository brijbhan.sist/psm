﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using DllZoneZipMaster;
/// <summary>
/// Summary description for ZoneZipMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ZoneZipMaster : System.Web.Services.WebService
{

    public ZoneZipMaster()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession=true)]
    public string SelectDriverCar()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ZoneZipMaster pobj = new PL_ZoneZipMaster();

            BL_ZoneZipMaster.SelectDriverCar(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }

     [WebMethod(EnableSession = true)]
    public string SelectAllDriverCar()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ZoneZipMaster pobj = new PL_ZoneZipMaster();

            BL_ZoneZipMaster.SelectAllDriverCar(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string insert(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ZoneZipMaster pobj = new PL_ZoneZipMaster();
                pobj.Name = jdv["Name"];
                if (jdv["DriverID"] != "" && jdv["DriverID"] != "0")
                    pobj.DriverId = Convert.ToInt32(jdv["DriverID"]);
                if (jdv["CarId"] != "" && jdv["CarId"] != "0")
                    pobj.CarId = Convert.ToInt32(jdv["CarId"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                BL_ZoneZipMaster.Insert(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else if (pobj.exceptionMessage == "Zone already exists")
                {
                    return "Exists";
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }

     [WebMethod(EnableSession = true)]
    public string update(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ZoneZipMaster pobj = new PL_ZoneZipMaster();
                pobj.Name = jdv["Name"];
                if (jdv["DriverID"] != "" && jdv["DriverID"] != "0")
                    pobj.DriverId = Convert.ToInt32(jdv["DriverID"]);
                if (jdv["CarId"] != "" && jdv["CarId"] != "0")
                    pobj.CarId = Convert.ToInt32(jdv["CarId"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                BL_ZoneZipMaster.Update(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    if (pobj.exceptionMessage == "Zone already exists")
                    {
                        return "Exists";
                    }
                    else
                    {
                        return "false";
                    }
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }

     [WebMethod(EnableSession = true)]
    public string updateZoneZipCode(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ZoneZipMaster pobj = new PL_ZoneZipMaster();
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.ZipCode = jdv["ZipCode"];
                BL_ZoneZipMaster.UpdateZoneZip(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }

     [WebMethod(EnableSession = true)]
    public string EditZoneMaster(int AutoId, string SZip, string Scity)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ZoneZipMaster pobj = new PL_ZoneZipMaster();
            pobj.AutoId = AutoId;
            pobj.ZipCode = SZip;
            pobj.City = Scity;
            BL_ZoneZipMaster.EditZoneMaster(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }

     [WebMethod(EnableSession = true)]
    public string GetZoneList(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ZoneZipMaster pobj = new PL_ZoneZipMaster();
            pobj.DriverId = Convert.ToInt32(jdv["DriverId"]);
            pobj.CarId = Convert.ToInt32(jdv["CarId"]);
            pobj.Status = Convert.ToInt32(jdv["Status"]);
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            pobj.Name = jdv["ZoneName"];

            BL_ZoneZipMaster.GetZoneList(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Unauthorized access.";
        }
    }



}
