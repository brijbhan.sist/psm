﻿using DLLManageAccessPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for WManageAccessPage
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WManageAccessPage : System.Web.Services.WebService {

    public WManageAccessPage () {


    }

    [WebMethod(EnableSession = true)]
    public string getUserType()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ManageAccessPage pobj = new PL_ManageAccessPage();
            try
            {

                BL_ManageAccessPage.getUserType(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch(Exception)
            {

            }
        }
        return "Hello World";
    }
    [WebMethod(EnableSession = true)]
    public string bindSubModule(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ManageAccessPage pobj = new PL_ManageAccessPage();
                pobj.ModuleAutoId = Convert.ToInt32(jdv["ModuleAutoId"]);
                pobj.UserRole = Convert.ToInt32(jdv["UserRole"]);

                BL_ManageAccessPage.bindSubModule(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "OOPS ! Something went wrong";
            }
        }
        else
        {
            return "OOPS ! Something went wrong";
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindModule()
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_ManageAccessPage pobj = new PL_ManageAccessPage();
            try
            {

                BL_ManageAccessPage.bindModule(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "OOPS ! Something went wrong";
            }
        }
        else
        {
            return "OOPS ! Something went wrong";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getPage(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ManageAccessPage pobj = new PL_ManageAccessPage();
                pobj.ModuleAutoId = Convert.ToInt32(jdv["ModuleAutoId"]);
                pobj.UserRole = Convert.ToInt32(jdv["UserRole"]);
                BL_ManageAccessPage.getPage(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "OOPS ! Something went wrong";
            }
        }
        else
        {
            return "OOPS ! Something went wrong";
        }
    }
    [WebMethod(EnableSession = true)]
    public string insert(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ManageAccessPage pobj = new PL_ManageAccessPage();
                pobj.ModuleAutoId = Convert.ToInt32(jdv["ModuleAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.UserRole = Convert.ToInt32(jdv["UserRole"]);
                pobj.AssignedAction = jdv["AssignedAction"];
                pobj.Status = Convert.ToInt32(1);
                BL_ManageAccessPage.insert(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "OOPS ! Something went wrong";
            }
        }
        else
        {
            return "OOPS ! Something went wrong";
        }
    }

    [WebMethod(EnableSession = true)]
    public string getSavedAccessPageList(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ManageAccessPage pobj = new PL_ManageAccessPage();
                pobj.ModuleAutoId = Convert.ToInt32(jdv["ModuleAutoId"]);
                pobj.UserRole = Convert.ToInt32(jdv["UserRole"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_ManageAccessPage.select(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "OOPS ! Something went wrong";
            }
        }
        else
        {
            return "OOPS ! Something went wrong";
        }
    }

    [WebMethod(EnableSession = true)]
    public string update(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ManageAccessPage pobj = new PL_ManageAccessPage();
                pobj.ModuleAutoId = Convert.ToInt32(jdv["ModuleAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.UserRole = Convert.ToInt32(jdv["UserRole"]);
                pobj.AssignedAction = jdv["AssignedAction"];
                BL_ManageAccessPage.update(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception e)
            {
                return "OOPS ! Something went wrong";
            }
        }
        else
        {
            return "OOPS ! Something went wrong";
        }
    }

    [WebMethod(EnableSession = true)]
    public string delete(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ManageAccessPage pobj = new PL_ManageAccessPage();
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                BL_ManageAccessPage.delete(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "OOPS ! Something went wrong";
            }
        }
        else
        {
            return "OOPS ! Something went wrong";
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindSearchSubModule(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ManageAccessPage pobj = new PL_ManageAccessPage();
                pobj.ModuleAutoId = Convert.ToInt32(jdv["ModuleAutoId"]);

                BL_ManageAccessPage.bindSearchSubModule(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "OOPS ! Something went wrong";
            }
        }
        else
        {
            return "OOPS ! Something went wrong";
        }
    }

    [WebMethod(EnableSession = true)]
    public string edit(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ManageAccessPage pobj = new PL_ManageAccessPage();
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                BL_ManageAccessPage.edit(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "OOPS ! Something went wrong";
            }
        }
        else
        {
            return "OOPS ! Something went wrong";
        }
    }
}
