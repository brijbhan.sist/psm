﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllCustomPriceTemplate;
using System.Data;
using System.Web.Script.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class PriceTemplate : System.Web.Services.WebService
{

    [WebMethod(EnableSession = true)]
    public string getProductList(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_PriceLevel pobj = new PL_PriceLevel();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                if (jdv["PriceLevelAutoId"] != null)
                {
                    pobj.PriceLevelAutoId = jdv["PriceLevelAutoId"];
                }
                pobj.CategoryAutoId = jdv["CategoryAutoId"];
                pobj.SubcategoryAutoId = jdv["SubcategoryAutoId"];
                pobj.CustomerType = jdv["CustomerType"];
                pobj.ProductName = jdv["ProductName"];
                if(jdv["Productid"]=="")
                {
                    pobj.ProductAutoId =0;
                }
                else
                {
                    pobj.ProductAutoId = Convert.ToInt32(jdv["Productid"]);
                }
               
                pobj.PageIndex = jdv["PageIndex"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                BL_PriceLevel.getProductList(pobj);

                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string BindDropdowns()
    {
        PL_PriceLevel pobj = new PL_PriceLevel();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                BL_PriceLevel.BindStatus(pobj);

                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string BindSubcategory(int CategoryAutoId)
    {
        PL_PriceLevel pobj = new PL_PriceLevel();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.CategoryAutoId = CategoryAutoId;
                BL_PriceLevel.BindSubCategory(pobj);

                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string SavePriceLevel(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_PriceLevel pobj = new PL_PriceLevel();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.PriceLevelName = jdv["PriceLevelName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.CustomerType = Convert.ToInt32(jdv["CustomerType"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);

                BL_PriceLevel.SavePriceLevel(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Error";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string EditPriceLevel(string PriceLevelAutoId)
    {
        PL_PriceLevel pobj = new PL_PriceLevel();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.PriceLevelAutoId = Convert.ToInt32(PriceLevelAutoId);
                BL_PriceLevel.EditPriceLevel(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string AddProduct(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_PriceLevel pobj = new PL_PriceLevel();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.PriceLevelAutoId = Convert.ToInt32(jdv["PriceLevelAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);

                BL_PriceLevel.AddProduct(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string RemoveProduct(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_PriceLevel pobj = new PL_PriceLevel();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.PriceLevelAutoId = Convert.ToInt32(jdv["PriceLevelAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);

                BL_PriceLevel.RemoveProduct(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string checkMarkProduct(string priceLevelAutoId)
    {
        PL_PriceLevel pobj = new PL_PriceLevel();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.PriceLevelAutoId = Convert.ToInt32(priceLevelAutoId);

                BL_PriceLevel.checkMarkProduct(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string SavingProductPrice(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_PriceLevel pobj = new PL_PriceLevel();

        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.PriceLevelAutoId = Convert.ToInt32(jdv["PriceLevelAutoId"]);
                pobj.CustomerType = Convert.ToInt32(jdv["CustomerType"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                if (jdv["CustomPrice"] != "")
                {
                    pobj.CustomPrice = Convert.ToDecimal(jdv["CustomPrice"]);
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_PriceLevel.SavingProductPrice(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string UpdatePriceLevel(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_PriceLevel pobj = new PL_PriceLevel();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.PriceLevelAutoId = Convert.ToInt32(jdv["PriceLevelAutoId"]);
                pobj.PriceLevelName = jdv["PriceLevelName"];
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.CustomerType = Convert.ToInt32(jdv["CustomerType"]);
                BL_PriceLevel.UpdatePriceLevel(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string AddAllProduct(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_PriceLevel pobj = new PL_PriceLevel();
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.PriceLevelAutoId = Convert.ToInt32(jdv["PriceLevelAutoId"]);
                pobj.ActionType = Convert.ToInt32(jdv["ActionType"]);
                BL_PriceLevel.AddAllProduct(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string PrintPriceLevel(string PriceLevelAutoId)
    {
        PL_PriceLevel pobj = new PL_PriceLevel();

        try
        {
            pobj.PriceLevelAutoId = Convert.ToInt32(PriceLevelAutoId);
            BL_PriceLevel.PrintPriceLevel(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }


    [WebMethod(EnableSession = true)]
    public string ProductPriceLevelLog(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_PriceLevel pobj = new PL_PriceLevel();

        try
        {
            pobj.PriceLevelAutoId = Convert.ToInt32(jdv["PriceLevelAutoId"]);
            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
            pobj.PageIndex = jdv["PageIndex"];
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            BL_PriceLevel.ProductPriceLevelLog(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }


    [WebMethod(EnableSession = true)]
    public string PrintPriceLevel2(string PriceLevelAutoId)
    {
        PL_PriceLevel pobj = new PL_PriceLevel();

        try
        {
            pobj.PriceLevelAutoId = Convert.ToInt32(PriceLevelAutoId);
            BL_PriceLevel.PrintPriceLevel2(pobj);
            if (!pobj.isException)
            {

                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0].ToString();
                }
                if (json == "")
                {
                    json = "[]";
                }
                return json;
                //this.Context.Response.ContentType = "application/json; charset=utf-8";
                //this.Context.Response.StatusCode = 200;
                //return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }
}
