﻿using DLLManageModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class WManageModule : System.Web.Services.WebService
{
    public WManageModule()
    {

    }
    [WebMethod(EnableSession = true)]
    public string getParentModule()
    {
        PL_ManageModule pobj = new PL_ManageModule();
        BL_ManageModule.getParentModule(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }
    [WebMethod(EnableSession = true)]
    public string insert(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ManageModule pobj = new PL_ManageModule();
            try
            {
                pobj.ModuleId = jdv["ModuleId"];
                pobj.Module = jdv["Module"];
                pobj.iconClass = jdv["iconClass"];
                pobj.HasModule = Convert.ToInt32(jdv["HasModule"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.MenuType = Convert.ToInt32(jdv["MenuType"]);
                if (jdv["SequenceNo"]!="")
                pobj.SequenceNo = Convert.ToInt32(jdv["SequenceNo"]);
                if (jdv["ParentModule"] == "")
                {
                    pobj.ParentModule = 0;
                }
                else
                {
                    pobj.ParentModule = Convert.ToInt32(jdv["ParentModule"]);
                }
                BL_ManageModule.insert(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string getModuleList(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_ManageModule pobj = new PL_ManageModule();
        pobj.Module = jdv["Module"];
        pobj.ParentModule = Convert.ToInt32(jdv["ParentModule"]);
        pobj.Status = Convert.ToInt32(jdv["Status"]);
        pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
        pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
        BL_ManageModule.select(pobj);
        if (!pobj.isException)
        {
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.StatusCode = 200;
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }

    [WebMethod(EnableSession = true)]
    public string update(string dataValue)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            PL_ManageModule pobj = new PL_ManageModule();
            try
            {
                pobj.ModuleId = jdv["ModuleId"];
                pobj.Module = jdv["Module"];
                pobj.HasModule = Convert.ToInt32(jdv["HasModule"]);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.MenuType = Convert.ToInt32(jdv["MenuType"]);
                pobj.AutoId = Convert.ToInt32(jdv["AutoId"]);
                pobj.iconClass = jdv["iconClass"];
                if (jdv["SequenceNo"] != "")
                    pobj.SequenceNo = Convert.ToInt32(jdv["SequenceNo"]);
                if (jdv["ParentModule"] == "")
                {
                    pobj.ParentModule = 0;
                }
                else
                {
                    pobj.ParentModule = Convert.ToInt32(jdv["ParentModule"]);
                }
                BL_ManageModule.update(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return "Success";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "false";
        }

    }
    [WebMethod(EnableSession = true)]
    public string deleteModule(string ModuleAutoId)
    {
        PL_ManageModule pobj = new PL_ManageModule();
        if (Session["EmpAutoId"] != null)
        {
            if (ModuleAutoId != "")
            {
                pobj.AutoId = Convert.ToInt32(ModuleAutoId);
                BL_ManageModule.deleteModule(pobj);
            }
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Unauthorized access.";
        }
    }
}
