﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllOrderMaster;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class updateDelivery : System.Web.Services.WebService
{

    [WebMethod(EnableSession = true)]
    public string getOrderData(string OrderAutoId, int check)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_OrderMaster pobj = new PL_OrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                if (check == 1 && pobj.LoginEmpType == 1)
                {
                    pobj.LoginEmpType = 3;
                }
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_OrderMaster.getOrderData(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string saveDelUpdates(string OrderValues, string TableValues)
    {
        if (Session["EmpTypeNo"] != null)
        {
            DataTable dtOrder = new DataTable();
            dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(OrderValues);
            PL_OrderMaster pobj = new PL_OrderMaster();
            try
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.DelItems = dtOrder;
                }
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharges"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
                pobj.PaymentRecev = jdv["PaymentRecev"];
                pobj.RecevAmt = jdv["RecevAmt"];
                pobj.Remarks = jdv["Remarks"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.MLTax = Convert.ToDecimal(jdv["MLTax"]);
                pobj.AdjustmentAmt = Convert.ToDecimal(jdv["AdjustmentAmt"]);
                BL_OrderMaster.saveDelUpdates(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string updatedeleverOrder(string OrderValues, string TableValues)
    {
        if (Session["EmpTypeNo"] != null)
        {
            DataTable dtOrder = new DataTable();
            dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(OrderValues);
            PL_OrderMaster pobj = new PL_OrderMaster();
            try
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.DelItems = dtOrder;
                }
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharges"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
                pobj.PaymentRecev = jdv["PaymentRecev"];
                pobj.RecevAmt = jdv["RecevAmt"];
                pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
                pobj.MLTax = Convert.ToDecimal(jdv["MLTax"]);
                pobj.AdjustmentAmt = Convert.ToDecimal(jdv["AdjustmentAmt"]);
                pobj.Remarks = jdv["Remarks"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);

                BL_OrderMaster.updatedeleverOrder(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string UpdatePayment(string OrderValues)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(OrderValues);
            PL_OrderMaster pobj = new PL_OrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.AmtValue = Convert.ToDecimal(jdv["AmtValue"]);
                pobj.Remarks = jdv["Remarks"];
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);
                BL_OrderMaster.UpdatePayment(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.exceptionMessage;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string saveDelStatus(string DelStatus)
    {
        if (Session["EmpTypeNo"] != null)
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(DelStatus);
            PL_OrderMaster pobj = new PL_OrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.Delivered = jdv["Delivered"];
                pobj.Remarks = jdv["Remarks"];

                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"]);

                BL_OrderMaster.saveDelStatus(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetOrderPrint(string OrderAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_OrderMaster pobj = new PL_OrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_OrderMaster.GetOrderPrint(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string GetOrderPrintNew(string OrderAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_OrderMaster pobj = new PL_OrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_OrderMaster.GetOrderPrintNew(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }
    [WebMethod(EnableSession = true)]
    public string AddOngetOrderData(string OrderAutoId, int check)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_OrderMaster pobj = new PL_OrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_OrderMaster.AddOngetOrderData(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "SessionExpired";
        }
    }

}
