﻿using DLLWrongProductPrice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for WrongProductPrice
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WrongProductPrice : System.Web.Services.WebService {

    public WrongProductPrice () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string getProductList(string dataValue)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValue);
        PL_WrongProductList pobj = new PL_WrongProductList();
        try
        {
            pobj.ProductName = jdv["ProductName"];
            pobj.Unit = jdv["Unit"];
            pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
            
            pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
            BL_WrongProductPrice.select(pobj);
            if (!pobj.isException)
            {
                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.StatusCode = 200;
                return pobj.Ds.GetXml();
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    
    
}
