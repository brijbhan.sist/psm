﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using DllUtility;
using DLLAPIRequstReport;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WApiRequestReport : System.Web.Services.WebService
{

    public WApiRequestReport()
    {
        gZipCompression.fn_gZipCompression();
    }



    [WebMethod(EnableSession = true)]
    public string BindUser()
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                PL_APIRequstReport pobj = new PL_APIRequstReport();
                BL_APIRequstReport.BindUsers(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetApiLogData(string dataValue)
    {
        if (Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_APIRequstReport pobj = new PL_APIRequstReport();
                pobj.User = jdv["User"];
                if (jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                if (jdv["AppV"] != "0")
                {
                    pobj.AppVer = (jdv["AppV"]);
                }
                if (jdv["Method"] != "0")
                {
                    pobj.Method = Convert.ToInt32(jdv["Method"]);
                }
                pobj.DeviceName = jdv["DeviceId"];
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                BL_APIRequstReport.GetData(pobj);
                return pobj.Ds.GetXml();
            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}
