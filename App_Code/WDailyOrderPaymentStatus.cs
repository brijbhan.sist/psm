﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using DLLSaleReport;
using DllUtility;
/// <summary>
/// Summary description for WDailyOrderPaymentStatus
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WDailyOrderPaymentStatus : System.Web.Services.WebService
{

    public WDailyOrderPaymentStatus()
    {
        gZipCompression.fn_gZipCompression();
    }

    [WebMethod(EnableSession = true)]
    public string OrderPaymentStatusReport(string dataValue)
    {
        try
        {
            PL_SaleReport pobj = new PL_SaleReport();
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);
            if (jdv["FromDate"] != "")
                pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
            if (jdv["ToDate"] != "")
                pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
            if (jdv["CloseOrderFromDate"] != "")
                pobj.CloseOrderFromDate = Convert.ToDateTime(jdv["CloseOrderFromDate"]);
            if (jdv["CloseOrderToDate"] != "")
                pobj.CloseOrderToDate = Convert.ToDateTime(jdv["CloseOrderToDate"]);
            pobj.EmpAutoId = Convert.ToInt32(jdv["SalesPerson"]);
            pobj.DriverAutoId = Convert.ToInt32(jdv["DriverAutoId"]);
            pobj.OrderStatus = Convert.ToInt32(jdv["OrderStatus"]);
            pobj.PaymentStatus = jdv["PaymentStatus"];
            pobj.PageIndex = Convert.ToInt16(jdv["Index"]);
            pobj.PageSize = Convert.ToInt16(jdv["PageSize"]);
            BL_SaleReport.OrderPaymentStatusReport(pobj);
            return pobj.Ds.GetXml();
        }
        catch (Exception)
        {
            return "false";
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindStatus()
    {
        PL_SaleReport pobj = new PL_SaleReport();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                BL_SaleReport.bindStatus(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}
