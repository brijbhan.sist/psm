﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DLL_Account_OrderMaster;
using DllUtility;
/// <summary>
/// Summary description for WAccount_Order
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WAccount_Order : System.Web.Services.WebService
{
    public WAccount_Order()
    {
        gZipCompression.fn_gZipCompression();
    }
    [WebMethod(EnableSession = true)]
    public string getOrderData(string OrderAutoId)
    {
        if (Session["EmpTypeNo"] != null)
        {
            PL_Account_OrderMaster pobj = new PL_Account_OrderMaster();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_Account_OrderMaster.getOrderData(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public string clickonSecurity(string CheckSecurity)
    {
        PL_Account_OrderMaster pobj = new PL_Account_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                BL_Account_OrderMaster.clickonSecurity(pobj);
                if (!pobj.isException)
                {
                    if (pobj.Ds.Tables.Count > 0 && pobj.Ds.Tables[0].Rows.Count > 0)
                    {
                        return "true";
                    }
                    else
                    {
                        return "false";
                    }
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string clickonSecurityVoid(string CheckSecurity, string OrderAutoId)
    {
        PL_Account_OrderMaster pobj = new PL_Account_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.CheckSecurity = CheckSecurity;
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                BL_Account_OrderMaster.clickonSecurityVoid(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string CancelOrder(string CancelRemark, string OrderAutoId)
    {
        PL_Account_OrderMaster pobj = new PL_Account_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.CancelRemark = CancelRemark;
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                pobj.IPAddress = HttpContext.Current.Request.UserHostAddress;
                BL_Account_OrderMaster.CancelOrder(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public string clickonSecurityMLTax(string CheckSecurity, string OrderAutoId)
    {
        PL_Account_OrderMaster pobj = new PL_Account_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                BL_Account_OrderMaster.clickonSecurityMLTax(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }

    [WebMethod(EnableSession = true)]
    public string ConfirmSecurity(string CheckSecurity, string OrderAutoId, string MLTaxRemark)
    {
        PL_Account_OrderMaster pobj = new PL_Account_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                pobj.MLTaxRemark = MLTaxRemark;
                BL_Account_OrderMaster.ConfirmSecurity(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public string clickonSecurityMigrate(string CheckSecurity, string OrderAutoId)
    {
        PL_Account_OrderMaster pobj = new PL_Account_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.CheckSecurity = CheckSecurity;
                pobj.LoginEmpType = Convert.ToInt32(Session["EmpTypeNo"]);
                BL_Account_OrderMaster.clickonSecurityMigrate(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public string MigrateOrder(string Remarks, string OrderAutoId)
    {
        PL_Account_OrderMaster pobj = new PL_Account_OrderMaster();
        try
        {
            if (Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.EmpAutoId = Convert.ToInt32(Session["EmpAutoId"].ToString());
                pobj.Remarks = Remarks;
                BL_Account_OrderMaster.MigrateOrder(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage; ;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
}
