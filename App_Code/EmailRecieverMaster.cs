﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllEmailReceiverMaster;
using System.Web.Script.Serialization;
/// <summary>
/// Summary description for EmailRecieverMaster
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class EmailRecieverMaster : System.Web.Services.WebService {
 
    [WebMethod(EnableSession = true)]
    public string bindAgent()
    {
        if (Session["UserName"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {
            Pl_EmailReciver pobj = new Pl_EmailReciver();
            try
            {
                Bl_EmailReceiverMaster.bindallActivaAgent(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public string bindAgentById(string autoid)
    {
        if (Session["UserName"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {
            Pl_EmailReciver pobj = new Pl_EmailReciver();
            try
            {
                pobj.EmployeeID = autoid;
                Bl_EmailReceiverMaster.selectEmpdetails(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }   

    [WebMethod(EnableSession = true)]
    public string insertEmailReciever(string dataValue)
    {
        if (Session["UserName"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);

            Pl_EmailReciver pobj = new Pl_EmailReciver();
            try
            {
                pobj.CreatedBy = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.EmployeeID = jdv["EmployeeID"];
                pobj.EmployeeName = jdv["EmployeeName"];
                pobj.EmailID = jdv["EmailID"];
                pobj.Category = jdv["Category"];
                Bl_EmailReceiverMaster.insert(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public string getEmailReciever()
    {
        if (Session["UserName"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {
            Pl_EmailReciver pobj = new Pl_EmailReciver();
            try
            {
                Bl_EmailReceiverMaster.bindgrid(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public string getEmailRecieverByID(string autoid)
    {
        if (Session["UserName"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {
            Pl_EmailReciver pobj = new Pl_EmailReciver();
            try
            {
                pobj.EmployeeID = autoid;
                Bl_EmailReceiverMaster.selectEmpdetailsById(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public string updateEmailReciever(string dataValue)
    {
        if (Session["UserName"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {
            var jss = new JavaScriptSerializer();
            var jdv = jss.Deserialize<dynamic>(dataValue);

            Pl_EmailReciver pobj = new Pl_EmailReciver();
            try
            {
                pobj.UpDatedBy = Convert.ToInt32(Session["EmpAutoId"]);
                pobj.EmployeeID = jdv["EmployeeID"];
                pobj.EmployeeName = jdv["EmployeeName"];
                pobj.EmailID = jdv["EmailID"];
                pobj.Category = jdv["Category"];
                Bl_EmailReceiverMaster.update(pobj);
                if (!pobj.isException)
                {
                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public string DeleteEmailReciever(string autoid)
    {
        if (Session["UserName"] == null)
        {
            return "Unauthorized Access";
        }
        else
        {
            Pl_EmailReciver pobj = new Pl_EmailReciver();
            try
            {
                pobj.EmployeeID = autoid;
                Bl_EmailReceiverMaster.delete(pobj);
                if (!pobj.isException)
                {
                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.StatusCode = 200;
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception)
            {
                return "Oops! Something went wrong.Please try later.";
            }
        }
    }
}
