﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="MLQtyReport1.aspx.cs" Inherits="Warehouse_MLQtyReport1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .content-wrapper {
            min-height: 500px !important;
        }
        .tblwth{
            width:9%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title" id="Header"></h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Report</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#" id="mainHeader"></a>
                        </li>
                        <li class="breadcrumb-item active" id="subHeader">
                        </li>
                         <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10019)"><i class="la la-question-circle"></i></a></li> 
                        <input type="hidden" id="HDDomain" runat="server" />
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height:400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-3 col-sm-12">
                                        <select class="form-control border-primary input-sm" id="ddlCustomer" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <select class="form-control border-primary input-sm" id="ddlState" runat="server">
                                            <option value="0">All State</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(1)" placeholder="From Order Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(2)" placeholder="To Order Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm pull-right" id="btnSearch" onclick="getRecords(1)">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="CustomerId tblwth">Customer ID</td>
                                                        <td class="CustomerName left">Customer</td>
                                                        <td class="Address left">Address</td>
                                                        <td class="City tblwth">City</td>
                                                        <td class="StateName tblwth">State </td>
                                                        <td class="Zipcode text-center tblwth">Zip Code</td>
                                                        <td class="MLQty tblwth" style="text-align: right">ML Qty</td>
                                                        <td class="MLTax tblwth"  style="text-align: right">ML Tax</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                  <tfoot>
                                                    <tr style="font-weight:bold;">
                                                        <td colspan="6">Total</td>
                                                        <td id="TotalML_QTY" class="right">0.00</td>
                                                        <td id="TotalML_Tax" class="right">0.00</td>
                                                    </tr>
                                                    <tr style="font-weight:bold;">
                                                        <td colspan="6">Overall Total</td>
                                                        <td id="TotalMLQty" class="right">0.00</td>
                                                        <td id="TotalMLTax" class="right">0.00</td>
                                                    </tr>

                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control input-sm border-primary pagesize" id="ddlPageSize" onchange="getRecords(1)">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
              <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                  <span id="reportHeader"></span>
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead>
                    <tr id="expHeader" style="display:none">
                        <th colspan="8"> </th>
                    </tr>
                    <tr>
                        <td class="PCustomerId tblwth">Customer ID</td>
                        <td class="PCustomerName left ">Customer</td>
                        <td class="PAddress left ">Address</td>
                        <td class="PCity text-center tblwth">City</td>
                        <td class="PStateName text-center tblwth">State </td>
                        <td class="PZipcode text-center tblwth">Zip Code</td>
                        <td class="PMLQty tblwth" style="text-align: right">ML Qty</td>
                        <td class="PMLTax tblwth" style="text-align: right">ML Tax</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                 <tfoot>

                    <tr>
                        <td colspan="6" style="font-weight:bold;">Overall Total</td>
                        <td id="MLQTY" style="font-weight:bold;" class="right">0.00</td>
                        <td id="MLTAX" style="font-weight:bold;" class="right">0.00</td>
                    </tr>

                </tfoot>
            </table>
        </div>
    </div>
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissable fade in" id="alertSuccessDelete" style="display: none;">
            <a aria-label="close" id="successDeleteClose" class="close" style="cursor: pointer;">&times;</a>
            <span></span>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            domain = $(location).attr('hostname');
            if ($('#Hd_Domain').val().toLowerCase() == "psmct") {
                $("#mainHeader").html('Vape Tax Report');
                $("#subHeader").html('By ML Tax');
                $("#Header").html('By ML Tax');
            }
            else {
                $("#mainHeader").html('ML Tax Report');
                $("#subHeader").html('By Customer');
                $("#Header").html('By Customer');
            }
            $('#txtSFromDate').pickadate({
                min: new Date("01/01/2019"),
                format: 'mm/dd/yyyy',
                formatSubmit: 'mm/dd/yyyy',
                selectYears: true,
                selectMonths: true
            });

            $('#txtSToDate').pickadate({
                min: new Date("01/01/2019"),
                format: 'mm/dd/yyyy',
                formatSubmit: 'mm/dd/yyyy',
                selectYears: true,
                selectMonths: true
            });
            BindCustomer();
            var d = new Date();
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var year = d.getFullYear();
            $("#txtSFromDate").val(month + '/' + day + '/' + year);
            $("#txtSToDate").val(month + '/' + day + '/' + year);
        })

        function setdatevalidation(val) {
            var fdate = $("#txtSFromDate").val();
            var tdate = $("#txtSToDate").val();
            if (val == 1) {
                if (fdate == '' || new Date(fdate) > new Date(tdate)) {
                    $("#txtSToDate").val(fdate);
                }
            }
            else if (val == 2) {
                if (fdate == '' || new Date(fdate) > new Date(tdate)) {
                    $("#txtSFromDate").val(tdate);
                }
            }
        }
        function Pagevalue(e) {
            getRecords(parseInt($(e).attr("page")));
        }
        function BindCustomer() {
            $.ajax({
                type: "POST",
                url: "MLQtyReport1.aspx/BindCustomer",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d != "false") {
                            var getData = $.parseJSON(response.d);
                            $("#ddlCustomer option").remove();
                            $("#ddlCustomer").append("<option value='0'>All Customer</option>");
                            $.each(getData[0].Customer, function (index, item) {
                                $("#ddlCustomer").append("<option value=" + item.CUID + ">" + item.CUN + "</option>")
                            });
                            $("#ddlCustomer").select2();
                            $("#ddlState option").remove();
                            $("#ddlState").append("<option value='0'>All State</option>");
                            $.each(getData[0].State, function (index, item) {
                                $("#ddlState").append("<option value=" + item.SId + ">" + item.SN + "</option>")
                            });
                            $("#ddlState").select2();
                        }
                    }
                    else {
                        location.href = "/";
                    }

                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }

        function getRecords(PageIndex) {
            var data = {
                CustomerAutoId: $('#ddlCustomer').val(),
                StateAutoId: $('#ddlState').val(),
                FromDate: $('#txtSFromDate').val(),
                ToDate: $('#txtSToDate').val(),
                PageIndex: PageIndex,
                PageSize: $('#ddlPageSize').val()
            }
            $.ajax({
                type: "POST",
                url: "MLQtyReport1.aspx/BindReport",
                data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d != "false") {
                            var xmldoc = $.parseXML(response.d);
                            var MLReport = $(xmldoc).find("Table1");
                            var OrderTotal = $(xmldoc).find("Table2");
                            var TotalML_Qty = 0; TotalML_Tax = 0;
                            $('#tblOrderList tbody tr').remove();
                            if (MLReport.length > 0) {
                                var Row = $('#tblOrderList thead tr').clone();
                                $(MLReport).each(function () {
                                    $('.CustomerId', Row).text($(this).find('CustomerId').text());
                                    $('.CustomerName', Row).text($(this).find('CustomerName').text());
                                    $('.Address', Row).text($(this).find('Address').text());
                                    $('.City', Row).text($(this).find('City').text());
                                    $('.StateName', Row).text($(this).find('StateName').text());
                                    $('.MLQty', Row).text($(this).find('MLQty').text());
                                    $('.MLTax', Row).text($(this).find('MLTax').text());
                                    $('.Zipcode', Row).text($(this).find('Zipcode').text());
                                    $('#tblOrderList tbody').append(Row);
                                    Row = $('#tblOrderList tbody tr:last-child').clone();

                                    TotalML_Qty = TotalML_Qty + parseFloat($(this).find("MLQty").text());
                                    TotalML_Tax = TotalML_Tax + parseFloat($(this).find("MLTax").text());
                                })
                                $('#EmptyTable').hide();
                                $('#TotalML_QTY').html(TotalML_Qty.toFixed(2));
                                $('#TotalML_Tax').html(TotalML_Tax.toFixed(2));
                            } else {
                                //$('#EmptyTable').show();
                                $('#TotalMLQTY').html('0.00');
                                $('#TotalMLTax').html('0.00');
                                $('#TotalML_QTY').html('0.00');
                                $('#TotalML_Tax').html('0.00');
                               
                            }
                            $(OrderTotal).each(function () {
                                $('#TotalMLQty').html(parseFloat($(this).find('MLQty').text()).toFixed(2));
                                $('#TotalMLTax').html(parseFloat($(this).find('MLTax').text()).toFixed(2));
                            });
                            var pager = $(xmldoc).find("Table");
                            $(".Pager").ASPSnippets_Pager({
                                ActiveCssClass: "current",
                                PagerCssClass: "pager",
                                PageIndex: parseInt(pager.find("PageIndex").text()),
                                PageSize: parseInt(pager.find("PageSize").text()),
                                RecordCount: parseInt(pager.find("RecordCount").text())
                            });
                        }
                    }
                    else {
                        location.href = "/";
                    }
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        function CreateTable(us) {
            row1 = "";
            var image = $("#imgName").val();
            var data = {
                CustomerAutoId: $('#ddlCustomer').val(),
                StateAutoId: $('#ddlState').val(),
                FromDate: $('#txtSFromDate').val(),
                ToDate: $('#txtSToDate').val(),
                PageIndex: 1,
                PageSize: 0
            }
            $.ajax({
                type: "POST",
                url: "MLQtyReport1.aspx/BindReport",
                data: "{'dataValue':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d != "false") {
                            var xmldoc = $.parseXML(response.d);
                            var MLReport = $(xmldoc).find("Table1");
                            var OrderTotal = $(xmldoc).find("Table2");
                            var PrintDate = $(xmldoc).find("Table");
                            $("#PrintTable tbody tr").remove();
                            var Row = $("#PrintTable thead tr:last").clone(true);
                            if (MLReport.length > 0) {
                                $.each(MLReport, function () {
                                    $('.PCustomerId', Row).text($(this).find('CustomerId').text());
                                    $('.PCustomerName', Row).text($(this).find('CustomerName').text());
                                    $('.PAddress', Row).text($(this).find('Address').text());
                                    $('.PCity', Row).text($(this).find('City').text());
                                    $('.PStateName', Row).text($(this).find('StateName').text());
                                    $('.PMLQty', Row).text($(this).find('MLQty').text());
                                    $('.PMLTax', Row).text($(this).find('MLTax').text());
                                    $('.PZipcode', Row).text($(this).find('Zipcode').text());
                                    $("#PrintTable tbody").append(Row);
                                    Row = $("#PrintTable tbody tr:last").clone(true);
                                });
                                $(OrderTotal).each(function () {
                                    $('#MLQTY').html(parseFloat($(this).find('MLQty').text()).toFixed(2));
                                    $('#MLTAX').html(parseFloat($(this).find('MLTax').text()).toFixed(2));
                                });
                                var rptName = "";
                                domain = $(location).attr('hostname');
                                if ($('#Hd_Domain').val().toLowerCase() == "psmct") {
                                    $("#reportHeader").html('Vape Tax Report By ML Tax');
                                    rptName = "Vape Tax Report By ML Tax";
                                }
                                else {
                                    $("#reportHeader").html('ML Tax Report By Customer');
                                    rptName = "ML Tax Report By Customer";
                                }
                                if (us == 1) {
                                    var image = $("#imgName").val();
                                    var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                                    mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                                    $("#PrintDate").text("Print Date : " + (PrintDate.find('PrintDate').text()));
                                    $("#PrintLogo").attr("src", "/Img/logo/" + image);
                                    if ($("#txtSFromDate").val() != "") {
                                        $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                                    }
                                    mywindow.document.write($(PrintTable1).clone().html());
                                    //mywindow.document.write(divToPrint.outerHTML);

                                    mywindow.document.write('</body></html>');
                                    setTimeout(function () {
                                        mywindow.print();
                                    }, 2000);
                                }
                                if (us == 2) {
                                    $("#expHeader").show();
                                    $("#PrintTable").table2excel({                                        
                                        exclude: ".noExl",
                                        name: "Excel Document Name",
                                        filename: rptName.toString(),
                                        fileext: ".xls",
                                        exclude_img: false,
                                        exclude_links: false,
                                        exclude_inputs: false
                                    });
                                }
                            }
                        }
                    }
                    else {
                        location.href = "/";
                    }
                }

            });
        }

        /*---------------Print Code------------------*/
        function PrintElem() {
            if ($('#tblOrderList tbody tr').length > 0) {
            CreateTable(1);
        }
        else {
                toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        }
        /*---------------Export To Excel---------------------------*/
        $("#btnExport").click(function () {
            if ($('#tblOrderList tbody tr').length > 0) {
            CreateTable(2);
            }
            else {
                toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        });
    </script>
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

