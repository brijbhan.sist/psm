﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DllDriverPackagePrint;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Net;

public partial class Manager_DriverPackagePrint : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod(EnableSession = true)]
    public static string PrintReadytoShipOrder(string OrderAutoId)
    {
        PL_DriverPackagePrint pobj = new PL_DriverPackagePrint();
        pobj.BulkOrderAutoId = OrderAutoId;
        BL_DriverPackagePrint.PrintReadytoShipOrder(pobj);
        return pobj.Ds.GetXml();
    }
    [WebMethod(EnableSession = true)]
    public static string getBulkOrderData(string OrderAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_DriverPackagePrint pobj = new PL_DriverPackagePrint();
            try
            {
                //pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.BulkOrderAutoId = OrderAutoId;
             
                pobj.LoginEmpType = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"].ToString());
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                BL_DriverPackagePrint.getBulkOrderData(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                 
                    return json;
                    //return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string PrintOrderTemplate1(string OrderAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_DriverPackagePrint pobj = new PL_DriverPackagePrint();
            try
            {
                pobj.BulkOrderAutoId = OrderAutoId;
                pobj.LoginEmpType = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"].ToString());
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                BL_DriverPackagePrint.PrintOrderTemplate1(pobj);
                if (!pobj.isException)
                {
                  
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string PrintBulkOrderItemTemplate3(string OrderAutoId)
    {

        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {

            PL_DriverPackagePrint pobj = new PL_DriverPackagePrint();
            try
            {
                 
                pobj.BulkOrderAutoId = OrderAutoId;
                pobj.LoginEmpType = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"].ToString());
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                BL_DriverPackagePrint.getBulkOrderData(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                   
                    return json;
                    //return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string getAllOrderData(string OrderAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_DriverPackagePrint pobj = new PL_DriverPackagePrint();
            try
            {
                //pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.BulkOrderAutoId = OrderAutoId;

                pobj.LoginEmpType = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"].ToString());
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                BL_DriverPackagePrint.getAllOrderData(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }

                    return json;
                    //return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string GetPackingOrderPrint(string OrderAutoId)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_DriverPackagePrint pobj = new PL_DriverPackagePrint();
            try
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_DriverPackagePrint.GetPackingOrderPrint(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string PrintCredit(string CreditAutoId)
    {
        PL_DriverPackagePrint pobj = new PL_DriverPackagePrint();
        pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
        BL_DriverPackagePrint.PrintCredit(pobj);
        return pobj.Ds.GetXml();

    }
    [WebMethod(EnableSession = true)]
    public static string getBulkOrderDataTempate2(string OrderAutoId)
    {

        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {

            PL_DriverPackagePrint pobj = new PL_DriverPackagePrint();
            try
            {
                 
                pobj.BulkOrderAutoId = OrderAutoId;
                pobj.LoginEmpType = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"].ToString());
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"].ToString());
                BL_DriverPackagePrint.getBulkOrderDataTempate2(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }
                   
                    return json; 
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string SendMail(string EmailBody)
    {
        var msg = "false";
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            MailMessage message = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            message.From = new MailAddress("rizwan.sist@gmail.com");
            message.To.Add(new MailAddress("rizwan.sist@gmail.com"));
            message.To.Add(new MailAddress("brijbhan.sist@gmail.com"));
            message.To.Add(new MailAddress("dimpal.sist@gmail.com"));
            message.Subject = "Optimo Route Print";
            message.IsBodyHtml = true; 
            message.Body = EmailBody;
            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com"; 
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = new NetworkCredential("dimpal.sist@gmail.com", "dimpal@1234");
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Send(message);
            msg = "true";
            return msg;
        }
        else
        {
            return msg;
        }
    }
}