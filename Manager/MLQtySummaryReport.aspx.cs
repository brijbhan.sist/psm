﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllMLQtySummaryReport;
public partial class Warehouse_MLQtyReport1 : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["DBLocation"] != null)
            {
                HDDomain.Value = Session["DBLocation"].ToString().ToLower();
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public static string BindReport(string CustomerAutoId, string FromDate, string ToDate, string ProductAutoId, string PageIndex, string PageSize)
    {
        PL_MLQtySummaryReport pobj = new PL_MLQtySummaryReport();

        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.Opcode = 42;
                pobj.CustomerAutoId = Convert.ToInt32(CustomerAutoId);
                pobj.PageIndex = Convert.ToInt32(PageIndex);
                pobj.PageSize = Convert.ToInt32(PageSize);
                pobj.ProductAutoId = Convert.ToInt32(ProductAutoId);
                if (FromDate != "")
                    pobj.FromDate = Convert.ToDateTime(FromDate);
                if (FromDate != "")
                    pobj.ToDate = Convert.ToDateTime(ToDate);
                BL_MLQtySummaryReport.getReport(pobj);
                return pobj.Ds.GetXml();

            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Expired Session";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetMlqtySummaryExportReport(string CustomerAutoId, string FromDate, string ToDate, string ProductAutoId)
    {
        PL_MLQtySummaryReport pobj = new PL_MLQtySummaryReport();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.Opcode = 43;
                pobj.CustomerAutoId = Convert.ToInt32(CustomerAutoId);
                pobj.ProductAutoId = Convert.ToInt32(ProductAutoId);
                if (FromDate != "")
                    pobj.FromDate = Convert.ToDateTime(FromDate);
                if (FromDate != "")
                    pobj.ToDate = Convert.ToDateTime(ToDate);
                BL_MLQtySummaryReport.GetMlqtySummaryExportReport(pobj);
                return pobj.Ds.GetXml();
            }
            catch 
            {
                return "false";
            }
        }
        else
        {
            return "Expired Session";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindCustomer()
    {
        PL_MLQtySummaryReport pobj = new PL_MLQtySummaryReport();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.Opcode = 41;
                BL_MLQtySummaryReport.getReport(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;

            }
            catch
            {
                return "false";
            }
        }
        else
        {
            return "Expired Session";
        }
    }
}