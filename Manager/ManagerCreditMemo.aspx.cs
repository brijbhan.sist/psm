﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Sales_CreditMemo : System.Web.UI.Page
{

  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmpType"] == null)
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
        else if (Request.QueryString["PageId"] == null && Session["EmpTypeNo"].ToString() != "2" && Session["EmpTypeNo"].ToString() != "1" && Session["EmpTypeNo"].ToString() != "10" && Session["EmpTypeNo"].ToString() != "12")
        {
            Response.Redirect("/Sales/CreditMemoList.aspx");
        }
    }
}