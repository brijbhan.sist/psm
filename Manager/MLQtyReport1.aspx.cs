﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DLLMLQtyReport;
public partial class Warehouse_MLQtyReport1 : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["DBLocation"] != null)
            {
                HDDomain.Value = Session["DBLocation"].ToString().ToLower();
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public static string BindReport(string dataValue)
    {
        PL_MLQtyReport pobj = new PL_MLQtyReport();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 41;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.StateAutoId = Convert.ToInt32(jdv["StateAutoId"]);
                pobj.PageIndex = Convert.ToInt32(jdv["PageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["FromDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                BL_MLQtyReport.getReport(pobj);
                return pobj.Ds.GetXml();

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetMlqtyExportReport(string dataValue)
    {
        PL_MLQtyReport pobj = new PL_MLQtyReport();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                pobj.Opcode = 42;
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.StateAutoId = Convert.ToInt32(jdv["StateAutoId"]);
                if (jdv["FromDate"] != "")
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                if (jdv["FromDate"] != "")
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                BL_MLQtyReport.GetMlqtyExportReport(pobj);
                return pobj.Ds.GetXml();

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindCustomer()
    {
        PL_MLQtyReport pobj = new PL_MLQtyReport();
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            try
            {
                pobj.Opcode = 43;
                BL_MLQtyReport.getReport(pobj);
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }
                return json;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}