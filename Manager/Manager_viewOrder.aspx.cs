﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_Manager_viewOrder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            string text = File.ReadAllText(Server.MapPath("/Manager/JS/Manager_OrderView.js"));
            Page.Header.Controls.Add(
                new LiteralControl(
                     "<script id='checksdrivRequiredFields'>" + text + "</script>"
                    ));
        }
        string[] GtUrl = Request.Url.Host.ToString().Split('.');
        HDDomain.Value = GtUrl[0].ToLower();
        if(Session["EmpTypeNo"]!=null)
        {
            hiddenEmpTypeVal.Value = Session["EmpTypeNo"].ToString();
        }
        else
        {
            Response.Redirect("/");
        }
    }
}