﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/MasterPage.master" ClientIDMode="Static" AutoEventWireup="true" CodeFile="MLQtySummaryReport.aspx.cs" Inherits="Warehouse_MLQtyReport1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .tblwth {
            width: 9%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
            <h3 class="content-header-title" id="header"></h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Reports</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#" id="mainHeading"></a>
                        </li>
                        <li class="breadcrumb-item active" id="subHeading">
                        </li>
                        <li class="breadcrumb-item active" title="Help"><a href="#" onclick="GetPageInformation(10020)"><i class="la la-question-circle"></i></a>
                             <input type="hidden" id="HDDomain" runat="server" />
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-4 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>

                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="PrintElem()">Print</button>
                    <button type="button" class="dropdown-item" id="btnExport">Export</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body" style="min-height: 400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-3 col-sm-12">
                                        <select class="form-control border-primary input-sm" id="ddlCustomer" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <select class="form-control border-primary input-sm" id="ddlProduct" runat="server">
                                            <option value="0">All Product</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm"  onchange="setdatevalidation(1)" placeholder="From Order Date" id="txtSFromDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">To Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" onchange="setdatevalidation(2)" placeholder="To Order Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnSearch" onclick="getRecords(1)">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="MyTableHeader" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="CustomerName left" style="width: 300px;">Customer</td>
                                                        <td class="OrderDate text-center tblwth">Order Date</td>
                                                        <td class="OrderNo text-center tblwth">Order No</td>
                                                        <td class="ProductName" style="text-align: left;">Product Name</td>
                                                        <td class="TotalPieces text-center tblwth">Qty in Pieces</td>
                                                        <td class="UnitMLQty tblwth" style="text-align: right">Unit ML Qty</td>
                                                        <td class="TotalMLQty tblwth" style="text-align: right">Total ML Qty</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>

                                                    <tr style="font-weight: bold;">
                                                        <td colspan="4">Total</td>
                                                        <td id="TotalPiece" class="center">0</td>
                                                        <td id="TMLQTY" class="right">0.00</td>
                                                        <td id="TotalML_Tax" class="right">0.00</td>
                                                    </tr>
                                                    <tr style="font-weight: bold;">
                                                        <td colspan="4">Overall Total</td>
                                                        <td id="TotalPieces" class="center">0</td>
                                                        <td id="TMLQTYs" class="right">0.00</td>
                                                        <td id="TotalML_Taxs" class="right">0.00</td>
                                                    </tr>

                                                </tfoot>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                  <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control border-primary input-sm pagesize" id="ddlPageSize" onchange="getRecords(1)">
                                            <option value="10">10</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="0">All</option>
                                        </select>
                                    </div>
                                      <div class="col-md-10">
                                        <div class="Pager text-right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="table-responsive" style="display: none;" id="PrintTable1">
            <div class="row" style="margin-bottom: 5px;">
                <div class="col-md-4 col-lg-4 col-sm-4 text-left">
                    <img src="" id="PrintLogo" height="40" />
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-center" style="margin-top: 0.5%; font-size: 12px; color: black; font-weight: bold;">
                  <span id="spPrintHeader"></span> 
                    <br />
                    <span class="text-center DateRangeCSS" style="font-size: 9px; color: black;" id="DateRange"></span>

                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 text-right" id="PrintDate" style="line-height: 3; font-size: 12px; color: black; font-weight: bold;">Print Date</div>
            </div>
            <table class="MyTableHeader PrintMyTableHeader" id="PrintTable">
                <thead>
                    <tr>
                        <td class="PCustomerName left">Customer</td>
                        <td class="POrderDate text-center">Order Date</td>
                        <td class="POrderNo text-center">Order No</td>
                        <td class="PProductName left">Product Name</td>
                        <td class="PTotalPieces text-center">Qty in Pieces</td>
                        <td class="PUnitMLQty" style="text-align: right">Unit ML Qty</td>
                        <td class="PTotalMLQty" style="text-align: right">Total ML Qty</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>

                    <tr>
                        <td colspan="4" style="font-weight: bold;">Overall Total</td>
                        <td id="TotalPiecess" class="center" style="font-weight: bold;">0</td>
                        <td id="TMLQTYss" class="right" style="font-weight: bold;">0.00</td>
                        <td id="TotalML_Taxss" class="right" style="font-weight: bold;">0.00</td>
                    </tr>

                </tfoot>
            </table>
        </div>
    </div>


    <div class="col-md-12">
        <div class="alert alert-success alert-dismissable fade in" id="alertSuccessDelete" style="display: none;">
            <a aria-label="close" id="successDeleteClose" class="close" style="cursor: pointer;">&times;</a>
            <b id="hdgOrderInfo">ML Qty Report</b>
        </div>
    </div>

    <script type="text/javascript">
        function Pagevalue(e) {
            getRecords(parseInt($(e).attr("page")));
        }
        function BindCustomer() {
            $.ajax({
                type: "POST",
                url: "MLQtySummaryReport.aspx/BindCustomer",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d != "false") {


                            var getData = $.parseJSON(response.d);
                            $("#ddlCustomer option").remove();
                            $("#ddlCustomer").append("<option value='0'>All Customer</option>");
                            $.each(getData[0].Customer, function (index, item) {
                                $("#ddlCustomer").append("<option value=" + item.CUID + ">" + item.CUN + "</option>")
                            });

                            $("#ddlCustomer").select2();
                            $("#ddlProduct option").remove();
                            $("#ddlProduct").append("<option value='0'>All Product</option>");
                            $.each(getData[0].Product, function (index, item) {
                                $("#ddlProduct").append("<option value=" + item.PID + ">" + item.PN + "</option>")
                            });
                            $("#ddlProduct").select2();
                        }
                    }
                    else {
                        location.href = "/";
                    }
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }

        function getRecords(PageIndex) {
            $.ajax({
                type: "POST",
                url: "MLQtySummaryReport.aspx/BindReport",
                data: "{'CustomerAutoId':'" + $('#ddlCustomer').val() + "','FromDate':'" + $('#txtSFromDate').val() + "','ToDate':'" + $('#txtSToDate').val() + "','ProductAutoId':'" + $('#ddlProduct').val() + "','PageIndex':'" + PageIndex + "','PageSize':'" + $("#ddlPageSize").val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d != "false") {
                            var xmldoc = $.parseXML(response.d);
                            var MLReport = $(xmldoc).find("Table1");
                            var OrderTotal = $(xmldoc).find("Table2");
                            var UnitMLQty = 0; TotalMLQty = 0; TotalPiece = 0;
                            $('#tblOrderList tbody tr').remove();
                            if (MLReport.length > 0) {
                                var Row = $('#tblOrderList thead tr').clone();

                                $(MLReport).each(function () {
                                    $('.CustomerName', Row).text($(this).find('CustomerName').text());
                                    $('.OrderDate', Row).text($(this).find('OrderDate').text());
                                    $('.OrderNo', Row).text($(this).find('OrderNo').text());
                                    $('.ProductName', Row).text($(this).find('ProductName').text());
                                    $('.TotalPieces', Row).text($(this).find('TotalPieces').text());
                                    $('.UnitMLQty', Row).text($(this).find('UnitMLQty').text());
                                    $('.TotalMLQty', Row).text($(this).find('TotalMLQty').text());
                                    $('#tblOrderList tbody').append(Row);
                                    Row = $('#tblOrderList tbody tr:last-child').clone();

                                    TotalPiece = TotalPiece + parseInt($(this).find("TotalPieces").text());
                                    UnitMLQty = UnitMLQty + parseFloat($(this).find("UnitMLQty").text());
                                    TotalMLQty = TotalMLQty + parseFloat($(this).find("TotalMLQty").text());
                                })
                                $(OrderTotal).each(function () {
                                    $('#TotalPieces').html(parseInt($(this).find('TotalPieces').text()));
                                    $('#TMLQTYs').html(parseFloat($(this).find('UnitMLQty').text()).toFixed(2));
                                    $('#TotalML_Taxs').html(parseFloat($(this).find('TotalMLQty').text()).toFixed(2));
                                });
                                $('#EmptyTable').hide();
                                $('#TotalPiece').html(TotalPiece);
                                $('#TMLQTY').html(UnitMLQty.toFixed(2));
                                $('#TotalML_Tax').html(TotalMLQty.toFixed(2));
                            } else {
                                //  $('#EmptyTable').show();
                                $('#TotalPiece').html('0');
                                $('#TMLQTY').html('0.00');
                                $('#TotalML_Tax').html('0.00');
                                $('#TotalPieces').html('0');
                                $('#TMLQTYs').html('0.00');
                                $('#TotalML_Taxs').html('0.00');
                            }
                            var pager = $(xmldoc).find("Table");
                            $(".Pager").ASPSnippets_Pager({
                                ActiveCssClass: "current",
                                PagerCssClass: "pager",
                                PageIndex: parseInt(pager.find("PageIndex").text()),
                                PageSize: parseInt(pager.find("PageSize").text()),
                                RecordCount: parseInt(pager.find("RecordCount").text())
                            });
                        }
                    }
                    else {
                        location.href = "/";
                    }
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }

        function CreateTable(us) {
            row1 = "";
            var image = $("#imgName").val();
            $.ajax({
                type: "POST",
                url: "MLQtySummaryReport.aspx/BindReport",
                data: "{'CustomerAutoId':'" + $('#ddlCustomer').val() + "','FromDate':'" + $('#txtSFromDate').val() + "','ToDate':'" + $('#txtSToDate').val() + "','ProductAutoId':'" + $('#ddlProduct').val() + "','PageIndex':'" + 1 + "','PageSize':'" + 0 + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d != "false") {
                            var xmldoc = $.parseXML(response.d);
                            var PrintDate = $(xmldoc).find("Table");
                            var MLReport = $(xmldoc).find("Table1");
                            var OrderTotal = $(xmldoc).find("Table2");
                            $("#PrintTable tbody tr").remove();
                            var Row = $("#PrintTable thead tr").clone(true);
                            if (MLReport.length > 0) {
                                $.each(MLReport, function () {
                                    $('.PCustomerName', Row).text($(this).find('CustomerName').text());
                                    $('.POrderDate', Row).text($(this).find('OrderDate').text());
                                    $('.POrderNo', Row).text($(this).find('OrderNo').text());
                                    $('.PProductName', Row).text($(this).find('ProductName').text());
                                    $('.PTotalPieces', Row).text($(this).find('TotalPieces').text());
                                    $('.PUnitMLQty', Row).text($(this).find('UnitMLQty').text());
                                    $('.PTotalMLQty', Row).text($(this).find('TotalMLQty').text());
                                    $("#PrintTable tbody").append(Row);
                                    Row = $("#PrintTable tbody tr:last").clone(true);
                                });
                                $(OrderTotal).each(function () {
                                    $('#TotalPiecess').html(parseInt($(this).find('TotalPieces').text()));
                                    $('#TMLQTYss').html(parseFloat($(this).find('UnitMLQty').text()).toFixed(2));
                                    $('#TotalML_Taxss').html(parseFloat($(this).find('TotalMLQty').text()).toFixed(2));
                                });
                                var rptName = "";
                                domain = $(location).attr('hostname');
                                if ($('#Hd_Domain').val().toLowerCase() == "psmct") {
                                    $("#spPrintHeader").html('Vape Tax Report By % Tax');
                                    rptName = "Vape Tax Report By % Tax";
                                }
                                else {
                                    $("#spPrintHeader").html('ML Tax Report By Product');
                                    rptName = "ML Tax Report By Product";
                                }
                                if (us == 1) {
                                    var image = $("#imgName").val();
                                    var mywindow = window.open('', 'mywindow', 'height=400,width=600');
                                    mywindow.document.write('<html><head><style>.tblwth{width:10%}</style><link rel="stylesheet" type="text/css" href="/Reports/Css/Report.css"/></head><body>');
                                    $("#PrintDate").text("Print Date : " + (PrintDate.find('PrintDate').text()));
                                    $("#PrintLogo").attr("src", "/Img/logo/" + image);
                                    if ($("#txtSFromDate").val() != "") {
                                        $("#DateRange").text("Date Range: " + $("#txtSFromDate").val() + " To " + $("#txtSToDate").val());
                                    }
                                    mywindow.document.write($(PrintTable1).clone().html());

                                    mywindow.document.write('</body></html>');
                                    setTimeout(function () {
                                        mywindow.print();
                                    }, 2000);
                                }
                                if (us == 2) {
                                    $("#PrintTable").table2excel({
                                        exclude: ".noExl",
                                        name: "Excel Document Name",
                                        filename: rptName.toString(),
                                        fileext: ".xls",
                                        exclude_img: true,
                                        exclude_links: true,
                                        exclude_inputs: true
                                    });
                                }
                            }
                        }
                    }
                    else {
                        location.href = "/";
                    }
                }

            });
        }

        /*---------------Print Code------------------*/
        function PrintElem() {
            if ($('#tblOrderList tbody tr').length > 0) {
                CreateTable(1);
            }
            else {
                toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
        /*---------------Export To Excel---------------------------*/
        $("#btnExport").click(function () {
            if ($('#tblOrderList tbody tr').length > 0) {
                CreateTable(2);
            }
            else {
                toastr.error('There is no record in search result', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        });

        function setdatevalidation(val) {
            var fdate = $("#txtSFromDate").val();
            var tdate = $("#txtSToDate").val();
            if (val == 1) {
                if (fdate == '' || new Date(fdate) > new Date(tdate)) {
                    $("#txtSToDate").val(fdate);
                }
            }
            else if (val == 2) {
                if (fdate == '' || new Date(fdate) > new Date(tdate)) {
                    $("#txtSFromDate").val(tdate);
                }
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            domain = $(location).attr('hostname');
            if ($('#Hd_Domain').val().toLowerCase() == "psmct") {
                $("#mainHeading").html('Vape Tax Report');
                $("#subHeading").html('By % Tax');
                $("#header").html('By % Tax');
            }
            else {
                $("#mainHeading").html('ML Tax Report');
                $("#subHeading").html('By Product');
                $("#header").html('By Product');
            }
            BindCustomer();
            $('#txtSFromDate').pickadate({
                min: new Date("01/01/2019"),
                format: 'mm/dd/yyyy',
                formatSubmit: 'mm/dd/yyyy',
                selectYears: true,
                selectMonths: true
            });
            $('#txtSToDate').pickadate({
                min: new Date("01/01/2019"),
                format: 'mm/dd/yyyy',
                formatSubmit: 'mm/dd/yyyy',
                selectYears: true,
                selectMonths: true
            });
            var d = new Date();
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var year = d.getFullYear();
            $("#txtSFromDate").val(month + '/' + day + '/' + year);
            $("#txtSToDate").val(month + '/' + day + '/' + year);
        })
    </script>
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        document.write('<scr' + 'ipt defer type="text/javascript" src="../js/jquery.table2excel.js?v=' + new Date() + '"></scr' + 'ipt>');
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</asp:Content>

