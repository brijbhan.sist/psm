﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" CodeFile="assignedOrders.aspx.cs" Inherits="assignedOrders" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblDrvAsgn tr td {
            padding: 5px 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Assigned Driver</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a>
                        </li>
                        <li class="breadcrumb-item active">Assigned Driver
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12" style="display: none;">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item">Export</button>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body" style="min-height:400px">
        <section id="drag-area2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-3">
                                        <select class="form-control border-primary input-sm" id="ddlSDriver">
                                            <option value="0">All Driver</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">Assigned From Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="From Date" id="txtSFromDate" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 0.5rem;">Assigned To Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm date" placeholder="To Date" id="txtSToDate" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-1">
                                        <button type="button" class="btn btn-info buttonAnimation round box-shadow-1  btn-sm" onclick="getAssignedOrdersList(1)" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblAssignedOrders">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="Action text-center">Action</td>
                                                        <td class="AsgnDt text-center">Assigned Date</td>
                                                        <td class="DrvName">Driver Name</td>
                                                        <td class="AsgnOrders text-center">Assigned Orders</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="ml-auto">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <input type="hidden" id="olddriver" />

    <%-- ------------------------------- Modal View Root--------------------------  --%>
    <div class="modal fade" id="modalAsgnOrders" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Assigned Orders</h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="container-fluid">
                                <div class="row  form-group">
                                    <div class="col-md-3 col-sm-12">
                                        <span><b>Driver :&nbsp;</b></span><span id="drvname"></span>
                                    </div>
                                    <div class="col-md-3  col-sm-12 text-right">
                                        <span><b>Assigned Date :</b>&nbsp;</span><span id="asgnDt"></span>
                                    </div>
                                    <div class="col-md-2 col-sm-6 text-right" style="margin-bottom: 2px; padding: 4px">
                                        <b>Assigned Route :</b>
                                    </div>
                                    <div class="col-md-2 col-sm-6">
                                        <span>
                                            <input type="text" class="form-control input-sm border-primary" id="txtRoot" style="margin-top: 3px" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblDrvAsgn">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="OrderNo text-center">Order No/Date</td>
                                    <td class="OrderDt text-center">Order Date</td>
                                    <td class="StatusType text-center">Status</td>
                                    <td class="Customer">Customer</td>
                                    <td class="Sales">Sales Person</td>

                                    <td class="OrderVal price">Amount</td>
                                    <td class="Stop text-center">Stop</td>
                                    <td class="Remarks">Remark</td>
                                    <td style="display: none;">Delivery Status</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning buttonAnimation round box-shadow-1  btn-sm" id="btnChangeDriver">Change Driver</button>
                    <button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" onclick="saveStoppage()" id="btnsave">Save</button>
                    <button type="button" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm" onclick="print_NewOrder()" id="Button1">Print</button>
                    <button type="button" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm" onclick="driverPackagePrintOption()" id="btnDrvPkgPrint">Driver package print</button>
                    <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1 btn-sm" onclick="PrintMap()" id="btnMapPrint">Print Map</button>
                    <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalMisc" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="card-title">Drivers</h4>
                    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row anyClass">
                        <div class="col-md-12">
                            <div id="insideText">
                            </div>
                            <div id="DriverText" style="display: none">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                          
                                            <div class="row form-group">
                                                <div class="col-sm-12">
                                                    <table class="table table-bordered table-striped" id="tblDriver">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="DrvName">Driver Name</td>
                                                                <td class="AsgnOrders text-center">Assigned Orders</td>
                                                                <td class="Select text-center">Select</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" style="display: none;" id="btnAsgn" onclick="assignDriver()">Assign</button>
                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" style="display: none;" id="btnOk" onclick="packingConfirmed()" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalDriverPrintOption" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="min-width: 550px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Select Invoice Template</h4> 
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" style="padding: 0 !important;">
                                <div class="container" style="padding: 3px !important;">
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered" id="tblDriverPrintOption">
                                                    <thead class="bg-blue white">
                                                        <tr style="display: none;">
                                                            <td class="Action text-center">Action</td>
                                                            <td class="PrintOption">Print Option</td>
                                                            <td class="TemplateType" style="min-width: 200px">Template Type</td>
                                                            <td class="NoOfCopy">NoOfCopy</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                <h5 class="well text-center" id="tblDriverPrintOptionEmptyTable" style="display: none">No data available.</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="hfDrvIdForPrint" />
                        <input type="hidden" class="hiddenShipAddress" />
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="PrintDrvOrder()">Print</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Manager/JS/assignedOrders.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>

</asp:Content>

