﻿<%@ Page Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="ManagerOrder.aspx.cs" Inherits="Manager_Order_Master" ClientIDMode="static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #tblProductDetail tbody .ProName {
            text-align: left;
        }

        .pac-container {
            z-index: 10000 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input id="DraftAutoId" type="hidden" />
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Manage Order</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a></li>
                        <li class="breadcrumb-item">New Order</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <%
                        if (Session["EmpTypeNo"] != null)
                        {
                            if (Session["EmpTypeNo"].ToString() == "7")
                            {
                    %>
                    <a class="dropdown-item" onclick="location.href='/Warehouse/WarehouseorderList.aspx'" runat="server">Order List</a>
                    <%
                        }
                        else if (Session["EmpTypeNo"].ToString() == "8")
                        {
                    %>
                    <a class="dropdown-item" onclick="location.href='/Manager/Manager_orderList.aspx'" runat="server">Order List</a>
                    <%}
                        else
                        {
                    %>
                    <a class="dropdown-item" onclick="location.href='/Sales/OrderList.aspx'" id="linktoOrderList" runat="server">Order List</a>
                    <%
                            }
                        }
                    %>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <input type="hidden" id="hiddenEmpTypeVal" runat="server" />
                        <input type="hidden" id="hiddenStatusCode" runat="server" />
                        <input type="hidden" id="hfMLTax" />
                        <div class="hiddenShipAddress" style="display: none;"></div>
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill">
                                <div class="row">
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Order No</label>
                                        <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />
                                        <div class="form-group">
                                            <input type="text" id="txtOrderId" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Order Date</label>
                                        <div class="form-group">
                                            <input type="text" id="txtOrderDate" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Delivery Date</label>
                                        <div class="form-group input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">
                                                    <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" id="txtDeliveryDate" class="form-control input-sm border-primary" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label class="control-label">Order Status</label>
                                        <input type="hidden" id="hiddenOrderStatus" />
                                        <div class="form-group">
                                            <input type="text" id="txtOrderStatus" class="form-control input-sm border-primary" readonly="readonly" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">Customer</label>
                                        <div class="form-group">
                                            <input type="text" id="ddlCustomer1" class="form-control input-sm border-primary" readonly="readonly" />
                                            <input type="hidden" id="ddlCustomer" class="form-control input-sm border-primary" />
                                            <input type="hidden" id="ddlCustomerType" class="form-control input-sm border-primary" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4 form-group">
                                        <label class="control-label">Customer Type</label>
                                        <input type="text" id="txtCustomerType" class="form-control border-primary input-sm" readonly="readonly" />
                                    </div>
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">Terms</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm border-primary" id="txtTerms" runat="server" disabled="disabled" />
                                        </div>
                                    </div>
                                    <div class="col-md-3  col-sm-3">
                                        <label class="control-label">
                                            Shipping Type <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary ddlreq" id="ddlShippingType" runat="server" onchange="calTotalAmount()">
                                                <option value="0">- Select Shipping Type -</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="drag-area1">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">


                        <div class="card-header">
                            <h4 class="card-title">Customer Addresses</h4>


                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>

                        </div>
                        <div class="card-content collapse">
                            <div class="card-body" id="panelBill1">

                                <div class="row">
                                    <div class="col-md-6  col-sm-6">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <label class="col-md-6 col-sm-6 control-label">
                                                        Billing Address
                                        <small class="pull-right" addrtype="11" id="popBillAddr"><a href="#" data-toggle="modal" data-target="#modalChangeAddress" onclick="getAddressList(this)">[Change Address]</a></small>
                                                    </label>
                                                    <div class="col-md-6 col-sm-6  form-group">
                                                        <div id="divFullBillAddress" class="form-control input-sm border-primary"></div>
                                                        <%--<input type="text" class="form-control input-sm border-primary" id="txtBillAddress" readonly="readonly" />--%>
                                                        <input type="hidden" id="hiddenBillAddrAutoId" />
                                                    </div>
                                                </div>
                                                <%--  <div class="row">
                                                    <label class="col-md-6 col-sm-6  control-label">State</label>
                                                    <div class="col-md-6 col-sm-6  form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtBillState" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-6 col-sm-6  control-label">City</label>
                                                    <div class="col-md-6 col-sm-6  form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtBillCity" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-6 col-sm-6  control-label">Zipcode</label>
                                                    <div class="col-md-6 col-sm-6  form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtBillZip" readonly="readonly" />
                                                    </div>
                                                </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <label class="col-md-6  col-sm-6  control-label">
                                                        Shipping Address
                                        <small class="pull-right" addrtype="22" id="popShipAddr"><a href="#" data-toggle="modal" data-target="#modalChangeAddress" onclick="getAddressList(this)">[Change Address]</a></small>
                                                    </label>
                                                    <div class="col-md-6  col-sm-6  form-group">
                                                        <div id="divFullShippingAddress" class="form-control input-sm border-primary"></div>
                                                        <%--<input type="text" class="form-control input-sm border-primary" id="txtShipAddress" readonly="readonly" />--%>
                                                        <input type="hidden" id="hiddenShipAddrAutoId" />
                                                    </div>
                                                </div>
                                                <%--<div class="row">
                                                    <label class="col-md-6  col-sm-6  control-label">State</label>
                                                    <div class="col-md-6 form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtShipState" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-6  col-sm-6  control-label">City</label>
                                                    <div class="col-md-6  col-sm-6  form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtShipCity" readonly="readonly" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-md-6  col-sm-6  control-label">Zipcode</label>
                                                    <div class="col-md-6  col-sm-6  form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtShipZip" readonly="readonly" />
                                                    </div>
                                                </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="CustDues" style="display: none;">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Customer Order History</h4>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>

                        </div>
                        <div class="card-content collapse">
                            <div class="card-body" id="panelBill3">
                                <div class="col-md-12">



                                    <div class="table-responsive">
                                        <table id="tblCustDues" class="table table-striped table-bordered">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td class="orderNo text-center">Order No</td>
                                                    <td class="orderDate text-center">Order Date</td>
                                                    <td class="value" style="text-align: right;">Order Amount</td>
                                                    <td class="amtPaid" style="text-align: right;">Paid Amount</td>
                                                    <td class="amtDue" style="text-align: right;">Due Amount</td>

                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                            <tfoot style="font-weight: 700; background: oldlace;">
                                                <tr style="text-align: right;">
                                                    <td colspan="2"><b>TOTAL</b></td>
                                                    <td id="sumOrderValue"></td>
                                                    <td id="sumPaid"></td>
                                                    <td id="sumDue"></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <h5 class="well text-center" id="noDues" style="display: none">No Dues.</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="Top25Products" style="display: none;">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Top 25 Selling Products</h4>

                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse">
                            <div class="card-body" id="panelBill4">

                                <div class="col-md-12">

                                    <div class="table-responsive" style="max-height: 200px; overflow-y: auto;">
                                        <table id="tblTop25Products" class="table table-striped table-bordered" style="overflow-y: scroll;">
                                            <thead class="bg-blue white">
                                                <tr>
                                                    <td class="SrNo text-center">SN</td>
                                                    <td class="ProductId text-center">Product ID</td>
                                                    <td class="ProductName">Product Name</td>
                                                    <td class="QtyInStock text-center">Quantity in Stock</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <h5 class="well text-center" id="emptyTable2" style="display: none">No Products.</h5>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="panelProduct">
            <div class="row" id="panelOrderContent">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            <h4 class="card-title">Add Product</h4>


                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a onclick="LoadProducts()" data-action="collapse"><i class="ft-plus" id="LoadProducts"></i></a></li>
                                </ul>
                            </div>

                        </div>

                        <div class="card-content collapse">
                            <div class="card-body" id="panelBill5" style="display:none">
                                <div class="row">
                                    <label class="col-md-1 col-sm-1 control-label">Barcode</label>
                                    <div class="col-md-3  col-sm-3  form-group">
                                        <input type="text" class="form-control input-sm border-primary" id="txtBarcode" runat="server" onfocus="this.select()" disabled="disabled" placeholder="Enter Barcode here" onchange="readBarcode()" />
                                    </div>

                                    <div class="col-md-3  col-sm-3 ">
                                        <div class="alert alert-default alertSmall pull-right " id="alertBarcodeCount" style="display: none; color: #c62828;">
                                            Barcode Count : &nbsp;<span>sd</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-5  col-sm-3 ">
                                        <label class="control-label">
                                            Product <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary selectvalidate" id="ddlProduct" runat="server" style="width: 100% !important" onchange="BindUnittype()">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2  col-sm-2 ">
                                        <label class="control-label">
                                            Unit Type <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <select class="form-control input-sm border-primary ddlreq" onchange="BindunitDetails()" id="ddlUnitType" runat="server">
                                                <option value="0">-Select-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-sm-2">
                                        <label class="control-label" style="word-wrap: normal">
                                            Qty <span class="required">*</span>

                                        </label>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm border-primary ddlreq" id="txtReqQty" runat="server" value="1" maxlength="4" onkeypress="return isNumberKey(event)" />
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-sm-1 ">
                                        <label class="control-label" style="white-space: nowrap">
                                            Is Exchange 
                                        </label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkExchange" onchange="changetype(this,'chkIsTaxable')" />
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-sm-1 " id="istaxable">
                                        <label class="control-label" style="white-space: nowrap">
                                            Is Taxable 
                                        </label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkIsTaxable" onchange="changetype(this,'chkExchange')" />
                                        </div>
                                    </div>
                                    <div class="col-md-1  col-sm-1 " id="freeItem">
                                        <label class="control-label" style="white-space: nowrap">
                                            Is Free Item 
                                        </label>
                                        <div class="form-group">
                                            <input type="checkbox" id="chkFreeItem" />
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="pull-left">
                                            <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" id="btnAdd" style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 form-group">
                                        <div class="alert alert-danger alertSmall" id="alertStockQty" style="display: none; text-align: center; color: white !important">
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div id="divBarcode" style="display: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-2 col-ms-2">
                                                    <label class="control-label">Quantity</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtQty" value="1" maxlength="4" />
                                                    </div>
                                                </div>
                                                <div class="col-md-1  col-sm-1 ">
                                                    <label class="control-label">
                                                        Is Exchange 
                                                    </label>
                                                    <div class="form-group">
                                                        <input type="checkbox" id="IsExchange" onchange="changetype(this,'IsFreeItem')" />
                                                    </div>
                                                </div>
                                                <div class="col-md-1  col-sm-1 ">
                                                    <label class="control-label">
                                                        Is Free Item 
                                                    </label>
                                                    <div class="form-group">
                                                        <input type="checkbox" id="IsFreeItem" onchange="changetype(this,'IsExchange')" />
                                                    </div>
                                                </div>
                                                <script>
                                                    function changetype(e, idhtml) {
                                                        $('#' + idhtml).prop('checked', false);
                                                    }
                                                </script>
                                                <div class="col-md-3  col-ms-3">
                                                    <label class="control-label">Enter Barcode</label>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtScanBarcode" onchange="checkBarcode()" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3  col-ms-3">
                                                    <label class="control-label">
                                                        Product<span class="required">*</span>
                                                        <asp:RequiredFieldValidator ID="rfvitemproduct" runat="server" ErrorMessage="Please Select Product" SetFocusOnError="true"
                                                            ValidationGroup="AddProductQty" ControlToValidate="ddlitemproduct" Display="None" InitialValue="0" ForeColor="Transparent">*</asp:RequiredFieldValidator>
                                                        <asp:ValidatorCalloutExtender runat="server" CssClass="CustomValidatorCalloutStyle"
                                                            ID="ValidatorCalloutExtender1" TargetControlID="rfvitemproduct" PopupPosition="BottomLeft">
                                                        </asp:ValidatorCalloutExtender>
                                                    </label>
                                                    <div class="form-group">
                                                        <select class="form-control input-sm border-primary" id="ddlitemproduct" runat="server" onchange="itemProduct()" style="width: 100% !important">
                                                            <option value="0" style="display: none">-Select-</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2  col-ms-2">
                                                    <div class="pull-left">
                                                        <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="AddProductQty()" style="margin-top: 22px;">&nbsp;&nbsp;&nbsp;Add&nbsp;&nbsp;&nbsp;</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill6">
                                <div class="table-responsive">
                                    <table id="tblProductDetail" class="table table-striped table-bordered">
                                        <thead class="bg-blue white">
                                            <tr>
                                                <td class="Action text-center">Action</td>
                                                <td class="AutoId text-center" style="display: none;">AutoId</td>
                                                <td class="ProId text-center">ID</td>
                                                <td class="ProName">Product Name</td>
                                                <td class="UnitType text-center">Unit</td>
                                                <td class="ReqQty text-center" style="word-wrap: break-word; width: 5%">Qty</td>
                                                <td class="Barcode text-center">Barcode</td>
                                                <td class="QtyShip text-center">Qty
                                                    <br />
                                                    Packed</td>
                                                <td class="QtyRemain text-center" style="display: none;">Qty Remaining</td>
                                                <td class="ItemType text-center" style="display: none;">item Type
                                                </td>
                                                <td class="TtlPcs text-center">Total<br />
                                                    Pieces</td>
                                                <td class="UnitPrice" style="text-align: center;">Unit<br />
                                                    Price</td>
                                                <td class="SRP price">SRP</td>
                                                <td class="GP price">GP (%)</td>
                                                <td class="TaxRate text-center" style="display: none;">Tax</td>
                                                <td class="IsExchange text-center" style="display: none">Exchange</td>
                                                <td class="ItemTotal price">Item Total</td>
                                                <td class="Discount">Discount(%)</td>
                                                <td class="NetPrice price">Net Price</td>
                                                <td class="OM_MinPrice" style="display: none;">OM_Min Price</td>
                                                <td class="OM_CostPrice" style="display: none;">OM_Cost Price</td>
                                                <td class="OM_BasePrice" style="display: none;">OM_Base Price</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <h5 class="well text-center" id="emptyTable" style="display: none">No Product Selected.</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <div class="row">
            <div class="col-md-7 col-sm-7">

                <section id="Div1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Sales Person Details</h4>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill7">
                                        <div class="row">
                                            <div class="col-md-12 form-group">
                                                <label>Remark</label><textarea id="txtOrderRemarks" class="form-control border-primary"></textarea>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </section>

                <section id="Div2" style="display: none">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Order Remark Details</h4>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill10">
                                        <div class="row">
                                            <div class="col-md-12 form-group">
                                                <div class="table-responsive">
                                                    <table id="Table2" class="table table-striped table-bordered">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="SRNO text-center">SN</td>
                                                                <td class="EmployeeName">Employee Name</td>
                                                                <td class="EmployeeType">Employee Type</td>
                                                                <td class="Remarks">Remark</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                </section>


                <section id="ManagerRemarks" style="display: none">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Remark Details</h4>
                                </div>

                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill11">
                                        <div class="row">
                                            <div class="col-md-12 form-group">
                                                <label>Remark</label><textarea id="txtManagerRemarks" maxlength="500" class="form-control border-primary"></textarea>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </section>


                <section id="CusCreditMemo" style="display: none">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Credit Memo</h4>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="panel panel-default">


                                                    <div class="panel-body" id="dCuCreditMemo" style="display: none">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="table-responsive">
                                                                    <table id="tblCreditMemoList" class="table table-striped table-bordered">
                                                                        <thead class="bg-blue white">
                                                                            <tr>
                                                                                <td class="SRNO text-center">SN</td>
                                                                                <td class="Action rowspan  text-center" style="display: none">Action</td>
                                                                                <td class="CreditNo  text-center">Credit No.</td>
                                                                                <td class="CreditDate  text-center">Credit Date</td>
                                                                                <td class="ReturnValue price">Total Value</td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                        <tfoot style="font-weight: 700; background: oldlace;">
                                                                            <tr style="text-align: right;">
                                                                                <td style="display: none" class="rowspan"><b></b></td>
                                                                                <td colspan="3"><b>TOTAL</b></td>
                                                                                <td id="TotalDue"></td>


                                                                                <%--<td id="sumPay" style="padding: 3px 19px;">0.00</td>
                                <td></td>--%>
                                                                            </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                    <h5 class="well text-center" id="H1" style="display: none">No Dues.</h5>
                                                                </div>


                                                            </div>
                                                            <div class="col-md-8">
                                                                <div id="msg"></div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="pull-right">
                                                                    <button type="button" class="btn btn-default btn-sm" id="btnPay_Dues" style="padding: 5px 15px; display: none" onclick="DeductPay()"><b>Apply</b></button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="tblMemoList" class="table table-striped table-bordered" style="display: none">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="Action text-center" style="width: 10%">Action</td>
                                                                <td class="CreditNo text-center" style="width: 10%">Credit No.</td>
                                                                <td class="empName" style="width: 10%">deduction By</td>
                                                                <td class="PayDate" style="width: 20%">Deduction Date</td>
                                                                <td class="DeductionAmount" style="text-align: right; width: 20%">Deducted Amount</td>
                                                                <td class="dueRemarks">Remark</td>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                        <tfoot style="font-weight: 700; background: oldlace;">
                                                            <tr style="text-align: right;">
                                                                <td colspan="4"><b>TOTAL</b></td>
                                                                <td id="Td4"></td>
                                                                <td id="Td1"></td>

                                                            </tr>
                                                        </tfoot>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="row">
                    <div class="col-md-12">

                        <section id="DrvDeliveryInfo" style="display: none">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">

                                        <div class="card-content collapse show">
                                            <div class="card-body" id="panelBill13">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <label class="control-label col-md-2">Delivered</label>
                                                            <div class="col-md-4 form-group">
                                                                <div class="form-control input-sm">
                                                                    <input type="radio" class="radio-inline" name="rblDeliver" value="yes" />&nbsp;Yes
                                            <input type="radio" class="radio-inline" name="rblDeliver" value="no" />&nbsp;No 
                                                                </div>
                                                            </div>
                                                            <label class="control-label col-md-2">Delivery Remark</label>
                                                            <div class="col-md-4 form-group">
                                                                <textarea class="form-control input-sm border-primary" rows="2" placeholder="Enter Payment details here" id="txtRemarks" disabled="disabled"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label class="control-label col-md-2">Comment Type </label>
                                                            <div class="col-md-4 form-group">
                                                                <select id="ddlCommentType" class="form-control input-sm">
                                                                    <option value="0">-Select-</option>
                                                                    <option value="1">Fully Paid</option>
                                                                    <option value="2">Partially Paid</option>
                                                                    <option value="3">Paid for Other Invoice</option>
                                                                    <option value="4">Item Missing</option>
                                                                    <option value="5">Item Damage</option>
                                                                    <option value="6">Wrong Amount for Item</option>
                                                                    <option value="7">Item Return</option>
                                                                    <option value="8">Order Return</option>
                                                                    <option value="9">Other</option>
                                                                </select>
                                                            </div>
                                                            <label class="control-label col-md-2">Comment </label>
                                                            <div class="col-md-4 form-group">
                                                                <textarea class="form-control input-sm border-primary" rows="2" placeholder="Enter Comment here..." id="txtComment"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 form-group">
                                                                <button type="button" id="btnSaveDelStatus" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="saveDelStatus()" style="display: none;"><b>Save</b></button>
                                                                <button type="button" id="btnUpdate" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="saveDelStatus()" style="display: none;"><b>Update</b></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

            </div>

            <div class="col-md-5 col-sm-5">
                <section id="orderSummary">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">

                                <div class="card-content collapse show">
                                    <div class="card-body" id="panelBill8">
                                        <div style="text-align: right">
                                            <div class="row">
                                                <label class="col-sm-5 form-group">Sub Total</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtTotalAmount" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <label class="col-sm-5 form-group">Overall Discount</label>
                                                <div class="col-sm-7 form-group">
                                                    <div style="display: flex;">

                                                        <div class="input-group">
                                                            <input type="text" class="form-control input-sm border-primary" style="text-align: right;" onkeypress="return isNumberDecimalKey(event,this)" id="txtOverallDisc" value="0.00"
                                                                onkeyup="calOverallDisc()" onmouseout="calOverallDisc()" readonly="readonly" />
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text border-radius" style="padding: 0rem 1rem;">%</span>
                                                            </div>
                                                        </div>
                                                        &nbsp;
                                 
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                            </div>
                                            <input type="text" class="form-control input-sm border-primary" style="text-align: right;" onkeyup="calOverallDisc1()" onmouseout="calOverallDisc1()"
                                                onkeypress="return isNumberDecimalKey(event,this)" maxlength="6" id="txtDiscAmt" value="0.00" readonly="readonly" />
                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <label class="col-sm-5 form-group">Shipping Charges</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtShipping" value="0.00" onkeyup="calGrandTotal()"
                                                            onfocus="this.select()" onkeypress="return isNumberDecimalKey(event,this)" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">
                                                    Tax Type<span class="required"></span>

                                                </label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">%</span>
                                                        </div>
                                                        <select class="form-control input-sm border-primary" id="ddlTaxType" onchange="calTotalTax()" runat="server">
                                                            <option value="0" taxvalue="0.0">-Select Tax Type-</option>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">Total Tax</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtTotalTax" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">ML Quantity</label>
                                                <div class="col-sm-7 form-group">
                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtMLQty" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">ML Tax</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtMLTax" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">Weight Quantity</label>
                                                <div class="col-sm-7 form-group">
                                                    <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtWeightQuantity" value="0.00" readonly="readonly" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">Weight Tax</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtWeightTax" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group">Adjustment</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtAdjustment" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-5 form-group"><b>Order Total</b></label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right; font-size: 14px; font-weight: 700;" readonly="readonly" id="txtGrandTotal" value="0.00" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row SMANAGER" style="display: none">
                                                <label class="col-sm-5 form-group">Credit Memo Amount</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">

                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtDeductionAmount" value="0.00" readonly="readonly" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row SMANAGER" style="display: none">
                                                <label class="col-sm-5 form-group">Store Credit Amount <span id="creditShow">(max $<span id="CreditMemoAmount">0.00</span> Available)</span></label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">

                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" disabled style="text-align: right" id="txtStoreCreditAmount" onkeyup="checkcreditAmount()" onfocus="this.select()" value="0.00" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row SMANAGER" style="display: none">
                                                <label class="col-sm-5 form-group">Payable Amount</label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">

                                                            <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        </div>
                                                        <input type="text" class="form-control input-sm border-primary" style="text-align: right" id="txtPaybleAmount" value="0.00" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row" style="display: none;" id="hidenopackedbox">
                                                <label class="col-sm-5 form-group">
                                                    No. of Packed Boxes
                                                </label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtPackedBoxes" maxlength="3" runat="server" onkeypress="return isNumberKey(event)" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="display: none;" id="rowAmountPaid">
                                                <label class="col-sm-5 form-group"><b>Total Paid Amount</b></label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        <input type="text" class="form-control input-sm inputBold border-primary" readonly="readonly" id="txtAmtPaid" value="0.00" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="display: none;" id="rowAmountDue">
                                                <label class="col-sm-5 form-group"><b>Due Amount</b></label>
                                                <div class="col-sm-7 form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-text" style="padding: 0rem 1rem;">$</span>
                                                        <input type="text" class="form-control input-sm inputBold border-primary" readonly="readonly" id="txtAmtDue" value="0.00" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>


        </div>


        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body" id="panelBill9">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button type="button" id="btnReset" class="btn btn-secondary buttonAnimation round box-shadow-1 btn-sm pulse">Reset</button>
                                        <button type="button" id="btnCancel" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none" onclick="deleteDraftOrder()">Cancel </button>

                                        <button type="button" id="btnUpdateOrder" class="btn btn-primary buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none;">Update Order </button>
                                        <button type="button" id="btnPrintOrder" class="btn btn-default buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none; background-color: #00e600" onclick="print_NewOrder()">Print this Order </button>
                                        <button type="button" id="btnAddOnPrintOrder" class="btn btn-default buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none; background-color: #008000" onclick="AddOnprint_NewOrder()">Add-On Print this Order </button>
                                        <button type="button" id="btnEditOrder" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none;" onclick="editPackedOrder()">Edit Order </button>
                                        <button type="button" id="btnGenBar" runat="server" class="btn btn-default buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none; background-color: rgb(46, 99, 46); color: #fff" onclick="GenBar()">Print Order Barcode </button>

                                        <button type="button" id="btnAsgnDrv" class="btn btn-success buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none;" onclick="getDriverList()">Assign Driver </button>
                                        <%-- Remove Set as Process Button :-Request by Nilay as per Client due to inventory Issue-02/09/2021--%>
                                        <%--<button type="button" id="btnSetAsProcess" runat="server" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none;" onclick="SetAsProcess()">Set As Processed </button>--%>
                                        <button type="button" id="btnGenOrderCC" runat="server" class="btn btn-success buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none;" onclick="printOrder_CustCopy()">Generate Customer Copy of Order </button>
                                        <button type="button" id="btnCancelOrder" class="btn btn-warning buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none;" onclick="confirm_Cancellation_Of_Order()">Cancel Order </button>
                                        <button type="button" id="btnBackOrder" class="btn btn-info buttonAnimation round box-shadow-1 btn-sm pulse" style="display: none;" onclick="viewOrderLog()">View Log </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Modal - Change Address -->
    <div class="modal fade" id="modalChangeAddress" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="modalTitle"></h4>
                </div>

                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="tblChangeAdd">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="text-center">Set Default</td>
                                    <td class="text-center">Action</td>
                                    <td>Customer Name</td>
                                    <td>Address</td>
                                    <td>City</td>
                                    <td>State</td>
                                    <td class="text-center">Zip Code</td>

                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12">
                        <div class="pull-right">
                            <small><a href="javascript:;" id="NewAddr" onclick="addNewAddr()">Add new address</a></small>
                        </div>
                    </div>
                    <br />


                    <div class="panel-body">
                        <section style="display: none;" id="panelAddNewAddr">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">

                                        <div class="card-content collapse show">
                                            <div class="card-header">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <b>Add new address details</b>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="card-body" id="panelBill16">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label class="control-label">Address 1<span class="required">*</span></label>
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <textarea id="txtAddr" runat="server" class="form-control border-primary input-sm req" rows="2" onblur="resetField(this.value)" onchange="validateAddress();"></textarea>
                                                        <%--<input type="text" class="form-control border-primary input-sm req" id="txtAddr" runat="server" onblur="resetField(this.value)" onchange="validateAddress();" />--%>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">Address 2</label>
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <input type="text" class="form-control input-sm border-primary" placeholder="Suite/Apartment" maxlength="20" id="txtAddr2" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label class="control-label">City</label>
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtCity1" readonly="true" runat="server" onfocus="this.select()" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">State</label>
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtState1" readonly="true" runat="server" onfocus="this.select()" />

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label class="control-label">Zip Code</label>
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtZipcode" readonly="true" runat="server" onfocus="this.select()" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">Latitude</label>
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtLat" readonly="true" runat="server" onfocus="this.select()" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">Longitude</label>
                                                    </div>
                                                    <div class="col-md-4 form-group">
                                                        <input type="text" class="form-control input-sm border-primary" id="txtLong" readonly="true" runat="server" onfocus="this.select()" />

                                                    </div>
                                                    <div class="col-md-6 form-group">
                                                        <div class="pull-right">
                                                            <button type="button" id="btnSaveAddr" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="saveAddr()">Save</button>
                                                            <button type="button" id="btnUpdateAddr" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" style="display: none">Update</button>
                                                            <button type="button" id="btnCancelUpdate" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm" style="display: none;">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </section>
                        <%-- </div>
                    </div>--%>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal" onclick="modalClose()">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal - Barcode -->
    <div class="modal fade" id="modalMisc" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Order Barcode</h4>
                </div>
                <div class="modal-body">
                    <div id="insideText">
                    </div>
                    <div id="DriverText" style="display: none">
                        <div class="alert alert-success" id="alertSuccMisc" style="display: none; text-align: center;">
                        </div>
                        <div class="alert alert-danger" id="alertDangMisc" style="display: none; text-align: center;">
                        </div>
                        <div class="row form-group" style="margin: 0">
                            <div class="col-md-2 col-sm-6">Assign Date :</div>
                            <div class="col-md-4 col-sm-6 form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text " style="padding: 0rem 1rem;">
                                            <span class="la la-calendar-o"></span>
                                        </span>
                                    </div>
                                    <input type="text" id="txtAssignDate" readonly class="form-control input-sm border-primary" />
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive" id="tblAssignDrv" style="display: none; height: 400px">
                            <div class="row form-group" style="margin: 0">
                                <table class="table table-bordered table-striped" id="tbldriver">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="DrvName">Driver Name</td>
                                            <td class="AsgnOrders text-center">Assigned Orders</td>
                                            <td class="Select text-center">Select</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm pulse" style="display: none;" id="btnAsgn" onclick="assignDriver()">Assign</button>
                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm pulse" style="display: none;" id="btnOk" onclick="packingConfirmed()" data-dismiss="modal">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="OrderBarcode" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-header">
                    Order Cancellation Form
                </div>
                <div class="modal-body">
                    <div id="form-group">
                        <label class="form-group">Cancellation Remark</label>
                    </div>
                    <div id="Div8">
                        <textarea id="txtCancellationRemarks" placeholder="Please Enter Cancellation Remark" class="form-control border-primary" row="4"></textarea>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm pulse" id="Button3" onclick="cancelOrder()">Cancel Order</button>
                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="PopBarCodeforPickBox" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onclick="closePop()">&times;</button>
                    <h4 class="modal-title">Read Packed Boxes Barcode</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            Order No:
                        </div>
                        <div class="col-md-9 form-group">
                            <label id="lblOrderno"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            No Of Boxes:
                        </div>
                        <div class="col-md-9 form-group">
                            <label id="lblNoOfbox"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            Barcode:
                        </div>
                        <div class="col-md-9 form-group">
                            <input type="text" id="txtReadBorCode" class="form-control input-sm border-primary" onchange="readBoxBarcode()" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-9 form-group">

                            <button type="button" id="btnPick1" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" style="display: none;" onclick="pickOrder()"><b>Pick Order</b></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="text-align: right">
                            <span id="baxbarmsg"></span>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblBarcode">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="SRNO text-center">SN</td>
                                            <td class="Barcode text-center">Barcode</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="SavedMassege" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <center style="width: 100%;"><h4>Order Generated.</h4><br /> <p>Do you want to Create Another Order ?</p></center>
                </div>


                <div class="modal-body">
                    <center>
                       <button type="button" class="btn btn-primary buttonAnimation round box-shadow-1 btn-sm" onclick="createorder()">Yes</button>
                       <button type="button" class="btn btn-danger buttonAnimation round box-shadow-1 btn-sm" onclick="nocreateorder()" >No</button>          
                        </center>
                </div>
                <%--    <div class="modal-body">
                    <center>
                     <p> Do you want to Print Invoice ?</p>
                        <br />
                       <button type="button" id="NoPrint" class="btn btn-success buttonAnimation  round box-shadow-1 btn-sm" onclick="printOrder_CustCopy()" >Print</button>          
                    </center>
                </div>--%>
            </div>
        </div>
    </div>
    <div id="msgPop" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
                <br />
                <br />
                <br />
                <center><div id="packermsg"></div></center>

                <div class="modal-body">
                    <center>
                       
                       <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse"  data-dismiss="modal" >Close</button>          
                    </center>
                </div>
            </div>
        </div>
    </div>
    <div id="MODALpoPFORCREDIT" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4 class="modal-title">Approvel pending of Credit Memo for this Customer</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblCreditMemo">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="SRNO text-center">SN</td>
                                            <td class="Action text-center">Action</td>
                                            <td class="CreditNo text-center">Credit No</td>
                                            <td class="CreditDate">Credit Date</td>
                                            <td class="NoofItem text-center">No of Item</td>
                                            <td class="NetAmount price">Net Amount</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="alertmsg" style="color: red"></div>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="skipCreditmemo()" style="margin-left: 10px">Skip</button>

                            <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="CreditProcessed()">Processed</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalOrderLog" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4 class="modal-title">Order Log</h4>
                    <br />
                    <label><b>Order No</b>&nbsp;:&nbsp;<span id="Span1"></span>&nbsp;&nbsp;<b>Order Date</b>&nbsp;:&nbsp;<span id="lblOrderDate"></span></label>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblOrderLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="ActionBy">Action By</td>
                                    <td class="Date text-center">Date</td>
                                    <td class="Action text-center">Action</td>
                                    <td class="Remark">Remark</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="CustomerDueAmount" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4 class="modal-title">Customer Due Amount</h4>

                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblduePayment">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="OrderNo text-center">Order No</td>
                                    <td class="OrderDate">Order Date</td>
                                    <td class="AmtDue">Amt Due</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="closepaymentmodal()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" runat="server" id="HDDomain" />
    <div id="PopPrintTemplate" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4 class="modal-title">Select Invoice Template</h4>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" checked="checked" name="Template" id="chkdefault" />
                                <label>PSM Default  - Invoice Only</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="chkdueamount" />
                                <label>PSM Default</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row" style="display: none">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="PackingSlip" />
                                <label>Packing Slip</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row" style="display: none">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="WithoutCategorybreakdown" />
                                <label for="PackingSlip">Packing Slip - Without Category Breakdown</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row" id="PSMPADefault" style="display: none">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" id="rPSMPADefault" name="Template" />
                                <label>PSM PA Default</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row" id="PSMWPADefault" style="display: none">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" id="rPSMWPADefault" name="Template" />
                                <label>PSM WPA-PA</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row" id="divPSMNPADefault" style="display: none">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="rCheckNPADefault" />
                                <label>PSM NPA Default</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="Template1" />
                                <label>7 Eleven (Type 1)</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="Template2" />
                                <label>7 Eleven (Type 2)</label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="Templ1" />
                                Template 1
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="Templ2" />
                                Template 2
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row" id="divPSMWPANDefault">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="PSMWPANDefault" />
                                <label for="PSMWPADefault">PSM PA - N</label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="PrintOrder()">Print</button>
                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal" onclick="closePrintPop()">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="packerDiv" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Packer List</h4>
                </div>
                <div class="modal-body">
                    <div id="Div4">
                    </div>
                    <div id="Div5">
                        <div class="alert alert-success" id="Div6" style="display: none; text-align: center;">
                        </div>
                        <div class="alert alert-danger" id="alertDangMiscPacker" style="display: none; text-align: center;">
                        </div>
                        <input type="hidden" id="txtHTimes" class="form-control input-sm" />
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="packerList">
                                <thead class="bg-blue white">
                                    <tr>
                                        <td class="DrvName">Packer Name</td>
                                        <td class="Select text-center">Select</td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                Times (Mins) :
                            </div>
                            <div class="col-md-6">
                                <input type="text" id="txtTimes" onkeypress="return isNumberKey(event)" class="form-control input-sm form-group border-primary" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-3">
                                Remark
                            </div>
                            <div class="col-md-9">
                                <textarea id="txtWarehouseRemark" class="form-control input-sm border-primary" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="txtHWarehouseRemark" />
                <div class="modal-footer">

                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm pulse" id="Button1" onclick="assignPacker()">Assign</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="BarCodenotExists" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="barcodemsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="focusonBarcode()">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="SecurityEnabled" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <h4>Check Security </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security </div>
                        <div class="col-md-9">
                            <input type="password" id="txtSecurity" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <span id="errormsg"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="clickonSecurity()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="SecurityEnvalid" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="Sbarcodemsg"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="ClosePop()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function funchek(e) {
            var check = 0;
            if ($(e).prop('checked')) {
                check = 1;
            }
            $('#chkExchangePop').prop('checked', false);
            $('#chkFreePop').prop('checked', false);
            $('#chkTaxablePop').prop('checked', false);
            if (check == 1) {
                $(e).prop('checked', true);
            }
        }
    </script>
    <input type="hidden" id="ItemAutoId" />
    <div class="modal fade" id="PopChangeItemType" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Change Item Type</h4>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <label>Product Id</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="txtProductId" disabled class="form-control input-sm border-primary" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <label style="white-space: nowrap">Product Name</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="txtProductName" disabled  class="form-control input-sm border-primary" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <label>Exchange</label>
                        </div>
                        <div class="col-md-8">
                            <input type="checkbox" id="chkExchangePop" onclick="funchek(this)" />
                        </div>
                    </div>
                    <div class="row form-group freechecjbox">
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <label>Free</label>
                        </div>
                        <div class="col-md-8">
                            <input type="checkbox" id="chkFreePop" onclick="funchek(this)" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <label>Taxable</label>
                        </div>
                        <div class="col-md-8">
                            <input type="checkbox" id="chkTaxablePop" onclick="funchek(this)" />
                        </div>
                    </div>
                    <%-- <div class="row form-group" id="HideTaxType" style="display:none">
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <label>Tax Type</label>
                        </div>
                        <div class="col-md-8">
                            <select id="ddlTaxs" class="form-control border-primary input-sm" runat="server">
                                <option value="0">-Select Tax Type-</option>
                            </select>
                        </div>
                    </div>--%>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="updateIteminList()">Change</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="CloseCheckPop()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="MsgNormal" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Message</h4>
                </div>
                <div class="modal-body">
                    <div style="border-color: #ac2925; border: 1px solid; color: #ac2925; min-height: 45px; line-height: 20px; padding: 9px;">
                        <strong id="lblMsgNormal"></strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <audio class="audios" id="yes_audio" controls preload="none" style="display: none">
        <source src="/Audio/NOExists.mp3" type="audio/mpeg" />
    </audio>
    <audio class="audios" id="No_audio" controls preload="none" style="display: none">
        <source src="/Audio/WrongProduct.mp3" type="audio/mpeg" />
    </audio>
    <div class="modal fade" id="modalProductSalesHistory" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row" style="width: 100%;">
                        <div class="col-md-5">
                            <h4>Product Sales History</h4>
                            <input type="hidden" id="hfProductAutoId" />
                        </div>
                        <div class="col-md-7">
                            <b>[ <span id="ProdId"></span>: <span id="ProdName"></span>]</b>
                        </div>
                    </div>

                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="tblProductSalesHistory">
                                    <thead class="bg-blue white">
                                        <tr>
                                            <td class="OrderDate  text-center">Order Date</td>
                                            <td class="OrderNo  text-center">Order No</td>
                                            <td class="StatusType text-center">Status</td>
                                            <td class="UnitType text-center">Unit</td>
                                            <td class="QtyShip text-center">Qty</td>
                                            <td class="UnitPrice price">Unit Price</td>
                                            <td class="TotalPrice price">Total</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td style="text-align: right" colspan="6"><b>Total</b></td>
                                            <td style="text-align: right"><b id="TotalNetPrice"></b></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right" colspan="6"><b>Overall Total</b></td>
                                            <td style="text-align: right"><b id="OverallNetPrice"></b></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <select class="form-control border-primary input-sm" id="ddlPageSize" onchange="ProductSalesHistory(1)">
                                <option value="10">10</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="500">500</option>
                                <option value="1000">1000</option>
                                <option value="0">All</option>
                            </select>
                        </div>
                        <div class="col-md-10 form-group">
                            <div class="Pager"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="closeSalesHistory()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="ModalSalesQuote" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="width: 24%; text-align: center;">

            <!-- Modal content-->
            <div class="modal-content">
                <input type="hidden" value="" id="shippingtype" />
                <div class="modal-body">
                    <div class="row" style="padding-top: 47px;">
                        <div class="col-md-12 form-group">
                            <h4>This Order is Sales Quote</h4>
                        </div>
                    </div>

                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger  buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="closeSalesQuoteModa()" style="margin-left: 10px">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="zipcodeselect" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Zipcode</h4>
                </div>
                <div class="modal-body" style="padding: 3rem!important;">
                    <div class="row" id="zipcodelist">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btn-sm" onclick="getSelectedZip()">OK</button>
                    <button type="button" class="btn btn-danger btn-sm" onclick="hideclosemodal()">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="PopPrintBarcodeTemplate" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="display: block">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Select Invoice Template</h4>
                </div>
                <div class="modal-body">
                    <div class="container" id="packerAssignPrint">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm pulse" onclick="PrintBarcodeByTemplate()">Print</button>
                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Manager/JS/ManagerOrder.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <script>
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('txtAddr'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                setData(place);

            });
        });
        function setData(place) {
            $(".hiddenShipAddress").html(place.adr_address);
            var textdata = $(".hiddenShipAddress").find(".street-address").text();
            $("#txtAddr").val(textdata);
            var m = 0, check = 0, city = "";
            for (var i = 0; i < place.address_components.length; i++) {
                for (var j = 0; j < place.address_components[i].types.length; j++) {

                    if (place.address_components[i].types[j] == "postal_code") {
                        var zcode = place.address_components[i].long_name;
                        $("#txtZipcode").val(zcode);
                    }
                    else if (place.address_components[i].types[j] == "administrative_area_level_1") {
                        var state = place.address_components[i].long_name;
                        $("#txtState1").val(state);
                    }
                    if (place.address_components[i].types[j] == "locality") {
                        $("#txtCity1").val(place.address_components[i].long_name);
                        city = place.address_components[i].long_name;
                        check = 1;
                    }
                    if (check == 0) {
                        if (place.address_components[i].types[j] == "neighborhood") {
                            $("#txtCity1").val(place.address_components[i].long_name);
                            city = place.address_components[i].long_name;
                        }
                        else if (place.address_components[i].types[j] == "administrative_area_level_3") {
                            $("#txtCity1").val(place.address_components[i].long_name);
                            city = place.address_components[i].long_name;
                        }
                    }
                }
                m++;
            }
            if (m > 4) {
                checkZipcode(zcode, city, state);
                $("#txtLat").val(place.geometry.location.lat);
                $("#txtLong").val(place.geometry.location.lng);
            }
            else {
                swal("", "Invalid address !", "error");
                resetField('');
            }
        }
        function checkZipcode(zipcode, city, state) {

            if (state != undefined && city != undefined && zipcode != undefined) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/Sales/WebAPI/customerMaster.asmx/checkState",
                    dataType: "json",
                    data: "{'state':'" + state.trim() + "','city':'" + city.trim() + "','zcode':'" + zipcode.trim() + "'}",
                    success: function (response) {
                        if (response.d == 'false') {
                            swal("", "Oops, Something went wrong. Please try later.", "warning");
                        }
                        else if (response.d == 'Invalidstate') {
                            swal("", "Invalid State (" + state + ')', "warning");
                            resetField('');
                        }
                    }
                });
            }
            else {
                swal("", "Invalid address.", "warning");
                resetField('');
            }
        }

        function getSelectedZip() {
            var autoid = $('input[name="selectedzip"]:checked').val();
            if ($('input[name="selectedzip"]:checked').length == 0) {
                toastr.error('Please select zipcode.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                return false;
            } else {
                $("#ddlZipCode").val(autoid).change();
                $("#ddlZipCode").attr('disabled', true);
                $("#zipcodeselect").modal("hide");

            }

        }

        function hideclosemodal() {
            $("#zipcodeselect").modal("hide");
        }

        function resetField(val) {
            $("#txtShipAdd").val('');
            $("#txtCity1").val('');
            $("#txtState1").val('');
            $("#txtZipcode").val('');
            $("#txtAddr").val('');
            $("#txtLat").val('');
            $("#txtLong").val('');
        }
        function validateAddress() {
            $("#txtLat").val('');
            $("#txtLong").val('');
        }
    </script>
</asp:Content>
