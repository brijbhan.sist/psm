﻿using DLL_Manager_OrderMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_orderList : System.Web.UI.Page
{
    

    protected void Page_load(object sender, EventArgs e)
    {
        try
        {
           
            hiddenEmpType.Value = Session["EmpTypeNo"].ToString();
            string[] GtUrl = Request.Url.Host.ToString().Split('.');
            HDDomain.Value = GtUrl[0].ToLower();
        }
        catch
        {
            Session.Abandon();
            Response.Redirect("~/Default.aspx", false);
        }
    }
    [WebMethod(EnableSession = true)]
    public static string viewAllRemarks(string OrderAutoId)
    {
        
        PL_Manager_OrderMaster pobj = new PL_Manager_OrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                
                BL_Manager_OrderMaster.viewAllRemarks(pobj);
                if (!pobj.isException)
                {                    
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
}