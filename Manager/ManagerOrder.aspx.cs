﻿using DllManagerOrderMaster;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_Order_Master : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["EmpType"] != null)
        {
            hiddenEmpTypeVal.Value = Session["EmpTypeNo"].ToString();
        }else
        {
            Response.Redirect("/");
        }
    } 
    [WebMethod(EnableSession = true)]
    public static string bindAllDropdown()
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.SalesPersonAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.LoginEmpType = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_ManagerOrderMaster.bindAllDropdown(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }

                    return json;
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindProductDropdown()
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                BL_ManagerOrderMaster.bindProductDropdown(pobj);
                if (!pobj.isException)
                {
                    string json = "";
                    foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                    {
                        json += dr[0];
                    }

                    return json;
                }
                else
                {
                    return pobj.exceptionMessage;
                }

            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindZipcodeDropDowns()
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();

        BL_ManagerOrderMaster.bindZipcodeDropDowns(pobj);
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }

                return json;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string BindPinCodeForCity(string ZipAutoId)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        pobj.ZipAutoId = Convert.ToInt32(ZipAutoId);
        BL_ManagerOrderMaster.BindPinCodeForCity(pobj);
        return pobj.Ds.GetXml();

    }
    [WebMethod(EnableSession = true)]
    public static string selectAddress(int customerAutoId)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = customerAutoId;
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_ManagerOrderMaster.selectAddress(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string bindUnitType(int productAutoId)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.ProductAutoId = productAutoId;
                BL_ManagerOrderMaster.bindUnitType(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string selectQtyPrice(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustAutoId"]);
                pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
                pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
                BL_ManagerOrderMaster.selectQtyPrice(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    //[WebMethod(EnableSession = true)]
    //public static string insertOrderData(string orderData)
    //{
    //    //DataTable dtOrder = new DataTable();
    //    //dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
    //    var jss = new JavaScriptSerializer();
    //    var jdv = jss.Deserialize<dynamic>(orderData);
    //    PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
    //    try
    //    {
    //        if (HttpContext.Current.Session["EmpAutoId"] != null)
    //        {

    //            pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
    //            pobj.BillAddrAutoId = Convert.ToInt32(jdv["BillAddrAutoId"]);
    //            pobj.ShipAddrAutoId = Convert.ToInt32(jdv["ShipAddrAutoId"]);
    //            if (HttpContext.Current.Session["EmpAutoId"].ToString() != "Admin")
    //            {
    //                pobj.SalesPersonAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
    //            }
    //            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
    //            pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
    //            if (jdv["OverallDisc"] != "")
    //                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
    //            if (jdv["OverallDiscAmt"] != "")
    //                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
    //            if (jdv["ShippingCharges"] != "")
    //                pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharges"]);
    //            pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
    //            pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
    //            pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
    //            pobj.Remarks = jdv["OrderRemarks"];
    //            pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
    //            pobj.MLTax = Convert.ToDecimal(jdv["MLTax"]);
    //            pobj.AdjustmentAmt = Convert.ToDecimal(jdv["AdjustmentAmt"]);

    //            BL_ManagerOrderMaster.insert(pobj);
    //            if (!pobj.isException)
    //            {

    //                return pobj.Ds.GetXml();
    //            }
    //            else
    //            {
    //                return pobj.exceptionMessage;
    //            }
    //        }
    //        else
    //        {
    //            return "Session Expired";
    //        }

    //    }
    //    catch (Exception)
    //    {
    //        return "Oops! Something went wrong.Please try later.";
    //    }
    //}

    [WebMethod(EnableSession = true)]
    public static string editOrder(string orderNo, string loginEmpType)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderNo = orderNo;
                pobj.LoginEmpType = Convert.ToInt32(loginEmpType);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_ManagerOrderMaster.editOrderDetails(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string updateOrder(string TableValues, string orderData)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(orderData);
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.TableValue = dtOrder;
                }
                if (jdv["DeliveryDate"] != null && jdv["DeliveryDate"] != "")
                {
                    pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
                }
                pobj.LoginEmpType = Convert.ToInt32(HttpContext.Current.Session["EmpTypeNo"]);
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.Remarks = jdv["Remarks"];
                pobj.ManagerRemarks = jdv["ManagerRemarks"];
                pobj.BillAddrAutoId = Convert.ToInt32(jdv["BillAddrAutoId"]);
                pobj.OrderStatus = Convert.ToInt32(jdv["Status"]);
                pobj.ShipAddrAutoId = Convert.ToInt32(jdv["ShipAddrAutoId"]);                
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharges"]); 
                pobj.ShippingType = jdv["ShippingType"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.CreditNo = jdv["CreditNo"];
                if (jdv["CreditAmount"] != "")
                    pobj.CreditAmount = Convert.ToDecimal(jdv["CreditAmount"]);
                if (jdv["DeductionAmount"] != "")
                    pobj.CreditMemoAmount = Convert.ToDecimal(jdv["DeductionAmount"]); 
                if (jdv["PackedBoxes"] != null || jdv["PackedBoxes"] != "")
                {
                    pobj.PackedBoxes = Convert.ToInt32(jdv["PackedBoxes"]);
                }
                
                pobj.TaxType = Convert.ToInt32(jdv["TaxType"]);
                BL_ManagerOrderMaster.updateOrder(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string updatePackedOrder(string TableValues, string orderData)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(orderData);
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.DTPackedItemsQty = dtOrder;
                }
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.BillAddrAutoId = Convert.ToInt32(jdv["BillAddrAutoId"]);
                pobj.ShipAddrAutoId = Convert.ToInt32(jdv["ShipAddrAutoId"]);
                pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
                pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
                pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
                pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharges"]);
                pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
                pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);

                BL_ManagerOrderMaster.updatePackedOrder(pobj);
                if (!pobj.isException)
                {

                    return "Success!!";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getAddressList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.AddrType = Convert.ToInt32(jdv["AddrType"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);

                BL_ManagerOrderMaster.getAddressList(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string addNewAddr()
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            BL_ManagerOrderMaster.addNewAddr(pobj);
            return pobj.Ds.GetXml();
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string saveAddr(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderNo= jdv["OrderNo"];
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.AddrType = Convert.ToInt32(jdv["AddrType"]);
                pobj.Address = jdv["Address"];
                pobj.Address2 = jdv["Address2"];
                pobj.Zipcode = jdv["Zipcode"];
                pobj.City = jdv["City"];
                pobj.StateName = jdv["State"];
                pobj.Lat = jdv["Lat"];
                pobj.Long = jdv["Long"];
                BL_ManagerOrderMaster.insertAddr(pobj);
                if (!pobj.isException)
                {
                    return pobj.exceptionMessage;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception ex)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string deleteAddr(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.AddrType = Convert.ToInt32(jdv["AddrType"]);
                pobj.AddrAutoId = Convert.ToInt32(jdv["AddressAutoId"]);
                BL_ManagerOrderMaster.deleteAddr(pobj);
                if (!pobj.isException)
                {

                    return pobj.exceptionMessage;
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string changeAddr(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.AddrType = Convert.ToInt32(jdv["AddrType"]);
                pobj.AddrAutoId = Convert.ToInt32(jdv["AddressAutoId"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                BL_ManagerOrderMaster.changeAddr(pobj);
                if (!pobj.isException)
                {

                    return pobj.exceptionMessage;
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    //[WebMethod(EnableSession = true)]
    //public static string generatePacking(string dataValues, string TableValues)
    //{
    //    DataTable dtOrder = new DataTable();
    //    dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);

    //    PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
    //    try
    //    {
    //        if (HttpContext.Current.Session["EmpAutoId"] != null)
    //        {
    //            if (dtOrder.Rows.Count > 0)
    //            {
    //                pobj.DTPackedItemsQty = dtOrder;
    //            }

    //            var jss = new JavaScriptSerializer();
    //            var jdv = jss.Deserialize<dynamic>(dataValues);

    //            pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
    //            pobj.TotalAmount = Convert.ToDecimal(jdv["TotalAmount"]);
    //            pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
    //            pobj.OverallDiscAmt = Convert.ToDecimal(jdv["OverallDiscAmt"]);
    //            pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharges"]);
    //            pobj.TotalTax = Convert.ToDecimal(jdv["TotalTax"]);
    //            pobj.Remarks = jdv["PackerRemarks"];
    //            pobj.GrandTotal = Convert.ToDecimal(jdv["GrandTotal"]);
    //            pobj.PackedBoxes = Convert.ToInt32(jdv["PackedBoxes"]);
    //            pobj.PackerAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
    //            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
    //            pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
    //            pobj.MLTax = Convert.ToDecimal(jdv["MLTax"]);
    //            pobj.AdjustmentAmt = Convert.ToDecimal(jdv["AdjustmentAmt"]);
    //            BL_ManagerOrderMaster.genPacking(pobj);
    //            if (!pobj.isException)
    //            {

    //                return pobj.exceptionMessage;
    //            }
    //            else
    //            {
    //                return pobj.exceptionMessage;
    //            }
    //        }
    //        else
    //        {
    //            return "Session Expired";
    //        }
    //    }
    //    catch (Exception)
    //    {
    //        return "Oops! Something went wrong.Please try later.";
    //    }
    //}

    [WebMethod(EnableSession = true)]
    public static string checkBarcode(string Barcode)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.Barcode = Barcode;
                BL_ManagerOrderMaster.checkBarcode(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string AddProductQty(string ProductAutoId, string UnitAutoId)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.ProductAutoId = Convert.ToInt32(ProductAutoId);
                pobj.UnitAutoId = Convert.ToInt32(UnitAutoId);
                BL_ManagerOrderMaster.AddProductQty(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string getDriverList(string OrderAutoId, int LoginEmpType)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.LoginEmpType = LoginEmpType;
                BL_ManagerOrderMaster.getDriverList(pobj);

                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string AssignDriver(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.DrvAutoId = Convert.ToInt32(jdv["DrvAutoId"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                if (jdv["AssignDate"] != "")
                {
                    pobj.AsgnDate = Convert.ToDateTime(jdv["AssignDate"]);
                }
                BL_ManagerOrderMaster.AssignDriver(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string assignPacker(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.PackerAutoId = Convert.ToInt32(jdv["PackerAutoId"]);
                if (jdv["Times"] != "")
                {
                    pobj.Times = Convert.ToInt32(jdv["Times"]);
                }
                else
                {
                    pobj.Times = 0;
                }
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.Remarks = jdv["Remarks"];
                BL_ManagerOrderMaster.assignPacker(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {

                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    //[WebMethod(EnableSession = true)]
    //public static string payDueAmount(string PaymentValues, string CustAutoId)
    //{
    //    DataTable dtPayment = new DataTable();
    //    dtPayment = JsonConvert.DeserializeObject<DataTable>(PaymentValues);

    //    PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
    //    try
    //    {
    //        if (HttpContext.Current.Session["EmpAutoId"] != null)
    //        {
    //            if (dtPayment.Rows.Count > 0)
    //            {
    //                pobj.Payment = dtPayment;
    //            }
    //            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
    //            pobj.CustomerAutoId = Convert.ToInt32(CustAutoId);
    //            BL_ManagerOrderMaster.payDueAmount(pobj);
    //            if (!pobj.isException)
    //            {

    //                return pobj.Ds.GetXml();
    //            }
    //            else
    //            {
    //                return pobj.exceptionMessage;
    //            }
    //        }
    //        else
    //        {
    //            return "Session Expired";
    //        }
    //    }
    //    catch (Exception)
    //    {
    //        return "Oops! Something went wrong.Please try later.";
    //    }
    //}

    [WebMethod(EnableSession = true)]
    public static string barcode_Print(string OrderNo)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderNo = OrderNo;

                BL_ManagerOrderMaster.barcode_Print(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string cancelOrder(string OrderAutoId, string Remarks)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.Remarks = Remarks;
                BL_ManagerOrderMaster.cancelOrder(pobj);
                if (!pobj.isException)
                {

                    return pobj.Ds.GetXml();
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }

    [WebMethod(EnableSession = true)]
    public static string ShowTop25SellingProducts(int customerAutoId)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.CustomerAutoId = customerAutoId;
                BL_ManagerOrderMaster.ShowTop25SellingProducts(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string GetBarDetails(string dataValues)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValues);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);

                pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
                if (jdv["DeliveryDate"] != "")
                    pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
                if (jdv["ReqQty"] != "")
                    pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
                else
                    pobj.ReqQty = 1;
                pobj.Barcode = jdv["Barcode"];
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.IsTaxable = Convert.ToInt32(jdv["IsTaxable"]);
                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                BL_ManagerOrderMaster.GetBarDetails(pobj);
                return pobj.Ds.GetXml();
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    //[WebMethod(EnableSession = true)]
    //public static string SaveDraftOrder(string dataValues)
    //{
    //    PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
    //    try
    //    {
    //        if (HttpContext.Current.Session["EmpAutoId"] != null)
    //        {
    //            var jss = new JavaScriptSerializer();
    //            var jdv = jss.Deserialize<dynamic>(dataValues);
    //            if (jdv["DraftAutoId"] != "")
    //                pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
    //            pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
    //            pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
    //            if (jdv["DeliveryDate"] != "")
    //                pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
    //            pobj.ProductAutoId = Convert.ToInt32(jdv["productAutoId"]);
    //            pobj.UnitAutoId = Convert.ToInt32(jdv["unitAutoId"]);
    //            pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
    //            pobj.QtyPerUnit = Convert.ToInt32(jdv["QtyPerUnit"]);
    //            pobj.UnitPrice = Convert.ToDecimal(jdv["UnitPrice"]);
    //            if (jdv["minprice"] != "")
    //            {
    //                pobj.minprice = Convert.ToDecimal(jdv["minprice"]);
    //            }
    //            else
    //            {
    //                pobj.minprice = 0;
    //            }
    //            pobj.SRP = Convert.ToDecimal(jdv["SRP"]);
    //            pobj.GP = Convert.ToDecimal(jdv["GP"]);
    //            pobj.MLQty = Convert.ToDecimal(jdv["MLQty"]);
    //            pobj.IsTaxable = Convert.ToInt32(jdv["IsTaxable"]);
    //            pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
    //            pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
    //            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
    //            pobj.Remarks = jdv["Remarks"];
    //            BL_ManagerOrderMaster.SaveDraftOrder(pobj);
    //            if (!pobj.isException)
    //            {

    //                return pobj.DraftAutoId.ToString();
    //            }
    //            else
    //            {

    //                return "false";
    //            }
    //        }
    //        else
    //        {
    //            return "Session Expired";
    //        }
    //    }
    //    catch (Exception)
    //    {
    //        return "Oops! Something went wrong.Please try later.";
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //public static string updateDraftOrder(string dataValues)
    //{
    //    PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
    //    try
    //    {
    //        if (HttpContext.Current.Session["EmpAutoId"] != null)
    //        {
    //            var jss = new JavaScriptSerializer();
    //            var jdv = jss.Deserialize<dynamic>(dataValues);
    //            pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
    //            pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
    //            pobj.ShippingType = Convert.ToInt32(jdv["ShippingType"]);
    //            if (jdv["DeliveryDate"] != "")
    //                pobj.DeliveryDate = Convert.ToDateTime(jdv["DeliveryDate"]);
    //            pobj.Remarks = jdv["Remark"];
    //            pobj.ShippingCharges = Convert.ToDecimal(jdv["ShippingCharge"]);
    //            pobj.OverallDisc = Convert.ToDecimal(jdv["OverallDisc"]);
    //            pobj.OverallDiscAmt = Convert.ToDecimal(jdv["DiscAmt"]);
    //            BL_ManagerOrderMaster.UpdateDraftOrder(pobj);
    //            if (!pobj.isException)
    //            {

    //                return pobj.DraftAutoId.ToString();
    //            }
    //            else
    //            {

    //                return "false";
    //            }
    //        }
    //        else
    //        {
    //            return "Session Expired";
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        return "Oops! Something went wrong.Please try later.";
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //public static string UpdateDraftReq(string dataValues)
    //{
    //    PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
    //    try
    //    {
    //        if (HttpContext.Current.Session["EmpAutoId"] != null)
    //        {
    //            var jss = new JavaScriptSerializer();
    //            var jdv = jss.Deserialize<dynamic>(dataValues);
    //            pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
    //            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
    //            pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
    //            pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
    //            pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
    //            if (jdv["ReqQty"] != "")
    //                pobj.ReqQty = Convert.ToInt32(jdv["ReqQty"]);
    //            if (jdv["UnitPrice"] != "")
    //                pobj.UnitPrice = Convert.ToDecimal(jdv["UnitPrice"]);

    //            BL_ManagerOrderMaster.UpdateDraftReq(pobj);
    //            if (!pobj.isException)
    //            {

    //                return "true";
    //            }
    //            else
    //            {

    //                return "false";
    //            }
    //        }
    //        else
    //        {
    //            return "Session Expired";
    //        }
    //    }
    //    catch (Exception)
    //    {
    //        return "Oops! Something went wrong.Please try later.";
    //    }
    //}
    //[WebMethod(EnableSession = true)]
    //public static string EditDraftOrder(string DraftAutoId, string loginEmpType)
    //{
    //    PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
    //    try
    //    {
    //        if (HttpContext.Current.Session["EmpAutoId"] != null)
    //        {
    //            pobj.DraftAutoId = Convert.ToInt32(DraftAutoId);
    //            pobj.LoginEmpType = Convert.ToInt32(loginEmpType);
    //            BL_ManagerOrderMaster.editDraftDetails(pobj);
    //            if (!pobj.isException)
    //            {

    //                return pobj.Ds.GetXml();
    //            }
    //            else
    //            {
    //                return pobj.exceptionMessage;
    //            }
    //        }
    //        else
    //        {
    //            return "Session Expired";
    //        }
    //    }
    //    catch (Exception)
    //    {
    //        return "Oops! Something went wrong.Please try later.";
    //    }
    //}
    [WebMethod(EnableSession = true)]
    public static string deductionPayamount(string TableValues, string OrderNo)
    {
        DataTable dtOrder = new DataTable();
        dtOrder = JsonConvert.DeserializeObject<DataTable>(TableValues);
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                if (dtOrder.Rows.Count > 0)
                {
                    pobj.CreditTable = dtOrder;
                }
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.OrderNo = OrderNo;
                BL_ManagerOrderMaster.ApplyCreditMemo(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string deleteDeductionAmount(string LogAutoId)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.LogAutoId = Convert.ToInt32(LogAutoId);
                BL_ManagerOrderMaster.deleteDeductionAmount(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string SetAsProcess(string OrderAutoId)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.OrderAutoId = Convert.ToInt32(OrderAutoId);
                BL_ManagerOrderMaster.SetAsProcess(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    //[WebMethod(EnableSession = true)]
    //public static string DeleteDraftItem(string DataValue)
    //{
    //    var jss = new JavaScriptSerializer();
    //    var jdv = jss.Deserialize<dynamic>(DataValue);
    //    PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
    //    try
    //    {
    //        if (HttpContext.Current.Session["EmpAutoId"] != null)
    //        {

    //            pobj.DraftAutoId = Convert.ToInt32(jdv["DraftAutoId"]);
    //            pobj.ProductAutoId = Convert.ToInt32(jdv["ProductAutoId"]);
    //            pobj.UnitAutoId = Convert.ToInt32(jdv["UnitAutoId"]);
    //            pobj.IsFreeItem = Convert.ToInt32(jdv["isfreeitem"]);
    //            pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
    //            BL_ManagerOrderMaster.DeleteDraftItem(pobj);
    //            if (!pobj.isException)
    //            {

    //                return "true";
    //            }
    //            else
    //            {
    //                return pobj.exceptionMessage;
    //            }
    //        }
    //        else
    //        {
    //            return "Session Expired";
    //        }
    //    }
    //    catch (Exception)
    //    {
    //        return "Oops! Something went wrong.Please try later.";
    //    }
    //}
    [WebMethod(EnableSession = true)]
    public static string clickonSecurity(string CheckSecurity)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                pobj.CheckSecurity = CheckSecurity;
                BL_ManagerOrderMaster.clickonSecurity(pobj);
                if (!pobj.isException)
                {
                    if (pobj.Ds.Tables.Count > 0 && pobj.Ds.Tables[0].Rows.Count > 0)
                    {
                        return "true";
                    }
                    else
                    {
                        return "false";
                    }
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string ItemTypeUpdate(string dataValue)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {

            try
            {
                var jss = new JavaScriptSerializer();
                var jdv = jss.Deserialize<dynamic>(dataValue);
                PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
                pobj.OrderAutoId = Convert.ToInt32(jdv["OrderAutoId"]);
                pobj.DraftAutoId = Convert.ToInt32(jdv["ItemAutoId"]);

                pobj.IsExchange = Convert.ToInt32(jdv["IsExchange"]);
                pobj.IsFreeItem = Convert.ToInt32(jdv["IsFreeItem"]);
                pobj.IsTaxable = Convert.ToInt32(jdv["IsTaxable"]);
                BL_ManagerOrderMaster.ItemTypeUpdate(pobj);
                if (!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
                else
                {
                    return "false";

                }
            }
            catch (Exception)
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string DeleteItem(string ItemAutoId)
    {
        PL_ManagerOrderMaster pobj = new PL_ManagerOrderMaster();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {

                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                pobj.OrderAutoId = Convert.ToInt32(ItemAutoId);
                BL_ManagerOrderMaster.deleteitems(pobj);
                if (!pobj.isException)
                {

                    return "true";
                }
                else
                {
                    return pobj.exceptionMessage;
                }
            }
            else
            {
                return "Session Expired";
            }
        }
        catch (Exception)
        {
            return "Oops! Something went wrong.Please try later.";
        }
    }
}