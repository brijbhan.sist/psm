﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DriverPackagePrint.aspx.cs" Inherits="Manager_DriverPackagePrint" %>

<!DOCTYPE html> 

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="/css/bootstrap.min.css" rel="stylesheet" />
    <script src="/js/jquery.min.js"></script>
    <link href="/css/style.css" rel="stylesheet" />
    <script src="/js/bootstrap.min.js"></script>
    <style>
        td {
            padding: 3px 8px;
        }

        table {
            width: 100%;
        }

        #psmDefault {
            font-family:Arial !important;
        }

        .center {
            text-align: center;
        }

        .right {
            text-align: right;
        }

        thead > tr {
            background: #fff;
            color: #000;
            font-weight: 700;
            border-top: 1px solid #5f7c8a;
        }

            thead > tr > td {
                text-align: center !important;
            }

        .InvContent {
            width: 100%;
        }

        .tdLabel {
            font-weight: 700;
        }

        /*#tblProduct > thead > tr > td, #tblProduct > tbody > tr > td {
               width: 50px;
            }*/

        @media print {
            #btnPrint {
                display: none;
            }
        }

        legend {
            border-bottom: 1px solid #5f7c8a;
        }

        .tableCSS > tbody > tr > td,
        .tableCSS > thead > tr > td,
        .tableCSS > tfoot > tr > td {
            border: 1px solid #5f7c8a;
        }

        pre {
            border: none !important;
        }
		body{
		font-size:13px !important;
		}
    </style>
    <title></title>
</head><body id="bodyDrivePackagePrintHtml" style="font-family: Arial !important;font-size: 12px !important;">
     <!-----------------------------------------------------Driver Log------------------------------------------------------>
    <div class="InvContents" style="display:none;font-family: Arial !important;font-size:12px!important">
        <table style="width:100%;">
            <tbody>
                <tr>
                    <td colspan="2">
                        <button type="button" onclick="window.print();" id="btnPrint" style="font-size: 14px;float: right;"><b>Print</b></button>
                        <center>
                            <img src="../images/LOGO_transparent.png" id="logo" class="img-responsive" style="width:20%;"><br />
                            <span style="text-align:center;">
                                <span id="Address"></span>&nbsp;&nbsp;|&nbsp;&nbsp;Phone:<span id="Phone"></span>,Fax:<span id="FaxNo"></span>&nbsp;&nbsp;|&nbsp;&nbsp;<span id="Website"></span>
                            </span>
                        </center>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <legend style="text-align: center; font-weight: 700;" id="PageHeading"></legend>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="table tableCSS" id="tblCust" style="width:100%">
                            <tbody>
                                <tr>
                                    <td class="tdLabel" style="width:10%;">Driver Name</td>
                                    <td id="DriverName" style="width:24%;"></td>

                                    <td class="tdLabel" style="width:12%;">Assigned Date</td>
                                    <td id="AssignDate" style="width:10%;"></td>

                                    <td class="RootName tdLabel" style="width:12%;">Assigned Route</td>
                                    <td id="RootName"></td>


                                </tr>

                            </tbody>
                        </table>


                    </td>

                </tr>
            </tbody>
        </table>
        <fieldset>
            <table border="1" class="table tableCSS" id="tblProduct"  style="width:100%;border-collapse:collapse">
                <thead>
                    <tr>
                        <td class="Stop text-center" style="width: 3%">Stop</td>
                        <td class="OrderNo text-center" style="width: 8%;white-space:nowrap">Ord No/Date</td>
                        <%--<td class="OrderDate" style="width: 10%">Order Date</td>--%>
                        <%--<td class="Schedule" style="width: 10%">Schedule At</td>--%>
                        <td class="CustomerName" style="width: 32%">Customer Name</td>
                        <td class="SalesPerson text-center" style="width: 10%">Sales Person</td>
                        <td class="Remarks">Remarks</td>
                        <td class="GrandTotal" style="white-space: nowrap;text-align:right;width:1%;">Grand Total</td>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </fieldset>

    </div>
    <div id="psmDefault"></div>
    
    <div id="psmWPAPA"></div>
    <div id="psmWPAPACredit"></div>
   <%-- <div id="Template1" style="display:none;font-family: cursive; font-size: 16px">
    </div>
    <div id="Template2" style="display:none;font-family: cursive; font-size: 16px">
    </div>--%>
    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Manager/JS/DriverPackagePrint.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
</body>
</html>
