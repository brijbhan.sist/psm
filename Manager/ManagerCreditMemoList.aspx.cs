﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DllManagerCreditMemoList;

public partial class Manager_ManagerCreditMemoList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string text = File.ReadAllText(Server.MapPath("/Manager/JS/ManagerCreditMemoList.js"));
            Page.Header.Controls.Add(
                 new LiteralControl(
                    "<script id='checksdrivRequiredField'>" + text + "</script>"
                ));
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindAllDropdown()
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_ManagerCreditMemoList pobj = new PL_ManagerCreditMemoList();
            pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
            BL_ManagerCreditMemoList.bindDropdown(pobj);
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }

                return json;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string bindCustomer(string SalesPerson)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_ManagerCreditMemoList pobj = new PL_ManagerCreditMemoList();
            pobj.SalesPerson = Convert.ToInt32(SalesPerson);
            BL_ManagerCreditMemoList.bindCustomer(pobj);
            if (!pobj.isException)
            {
                string json = "";
                foreach (DataRow dr in pobj.Ds.Tables[0].Rows)
                {
                    json += dr[0];
                }

                return json;
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        else
        {
            return "Session Expired";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string getCreditList(string dataValues)
    {
        var jss = new JavaScriptSerializer();
        var jdv = jss.Deserialize<dynamic>(dataValues);
        PL_ManagerCreditMemoList pobj = new PL_ManagerCreditMemoList();
        try
        {
            if (HttpContext.Current.Session["EmpAutoId"] != null)
            {
                pobj.CreditNo = jdv["CreditNo"];
                if (jdv["FromDate"] != null && jdv["FromDate"] != "")
                {
                    pobj.FromDate = Convert.ToDateTime(jdv["FromDate"]);
                }
                if (jdv["ToDate"] != null && jdv["ToDate"] != "")
                {
                    pobj.ToDate = Convert.ToDateTime(jdv["ToDate"]);
                }
                pobj.PageIndex = Convert.ToInt32(jdv["pageIndex"]);
                pobj.PageSize = Convert.ToInt32(jdv["PageSize"]);
                pobj.CustomerAutoId = Convert.ToInt32(jdv["CustomerAutoId"]);
                pobj.SalesPerson = Convert.ToInt32(jdv["SalesPerson"]);
                pobj.Status = Convert.ToInt32(jdv["Status"]);
                pobj.CreditType = Convert.ToInt32(jdv["CreditType"]);
                pobj.EmpAutoId = Convert.ToInt32(HttpContext.Current.Session["EmpAutoId"]);
                BL_ManagerCreditMemoList.select(pobj);
                if(!pobj.isException)
                {
                    return pobj.Ds.GetXml();
                }
               else
                {
                    return pobj.exceptionMessage;
                }

            }
            else
            {
                return "Session Expired";
            }

        }
        catch (Exception)
        {
            return "false";
        }
    }
    [WebMethod(EnableSession = true)]
    public static string viewOrderLog(string CreditAutoId, int sortInCode)
    {
        if (HttpContext.Current.Session["EmpAutoId"] != null)
        {
            PL_ManagerCreditMemoList pobj = new PL_ManagerCreditMemoList();
            pobj.CreditAutoId = Convert.ToInt32(CreditAutoId);
            pobj.sortInCode = Convert.ToInt32(sortInCode);
            BL_ManagerCreditMemoList.BindCreditLog(pobj);
            return pobj.Ds.GetXml();
        }
        else
        {
            return "Session Expired";
        }

    }
}