﻿
var Print = 1, PrintLabel = '';
$(document).ready(function () {
    getOrderData();
});

//                                                     Edit Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData() {
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/WebAPI/WCBulkOrdePrint.asmx/getBulkOrderData", 
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) { 
            if (response.d == 'Session Expired') {
                location.href = '/';
                return;
            }
            var html = '';
            var OrderList = $.parseJSON(response.d);
            for (var i = 0; i < OrderList.length; i++) {
                var Company = OrderList[i];
                var Order = Company.OrderDetails;
                for (var j = 0; j < Order.length; j++) {
                    var OrderDetail = Order[j];

                    html += '<p style="page-break-before: always"></p><fieldset><table class="table tableCSS"><tbody><tr><td colspan="6" style="text-align: center">';
                    html += '<span style="font-size: 20px;font-weight:100">' + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + ' ( Page 1 of <span class=' + OrderDetail.OrderNo + '></span>)</span><span style="font-size: 20px;float:right" ><b><i>SALES ORDER</i></b></span>';
                    html += '</td></tr><tr><td rowspan="2" style="width: 80px !important;"><img src="../Img/logo/' + Company.Logo + '" class="img-responsive"></td>';
                    html += '<td>' + Company.CompanyName + '</td>';
                    html += '<td class="tdLabel" style="white-space: nowrap">Customer ID</td><td>' + OrderDetail.CustomerId + '</td>';
                    html += '<td class="tdLabel" style="white-space: nowrap">Sales Rep</td><td>' + OrderDetail.SalesPerson + '</td></tr>';
                    html += '<tr><td><span>' + Company.Address + '</span></td><td class="tdLabel" style="white-space: nowrap">Customer Contact</td>';
                    html += '<td style="white-space: nowrap">' + OrderDetail.ContactPersonName + '</td><td class="tdLabel">Order No </td>';
                    html += '<td>' + OrderDetail.OrderNo + '</td></tr><tr><td><span>' + Company.Website + '</span></td>';
                    html += '<td>Phone : <span>' + Company.MobileNo + '</span></td><td class="tdLabel">Contact No</td>';
                    html += '<td>' + OrderDetail.Contact + '</td><td class="tdLabel" style="white-space: nowrap">Order Date</td>';
                    html += '<td style="white-space: nowrap">' + OrderDetail.OrderDate + '</td></tr>';
                    html += '<tr><td>' + Company.EmailAddress + '</td><td>Fax : ' + Company.FaxNo + '</td><td class="tdLabel">Terms</td>';
                    html += '<td style="font-size:13px"><b>' + OrderDetail.TermsDesc + '</b></td><td class="tdLabel">Ship Via</td><td>' + OrderDetail.ShippingType + '</td></tr>';
                    if (OrderDetail.BusinessName != '' && OrderDetail.BusinessName != undefined) {
                        html += '<tr><td><span class="tdLabel">Business Name:</span></td><td>' + OrderDetail.BusinessName + '</td></tr>';
                    }
                    html += '<tr><td><span class="tdLabel">Shipping Address</span></td><td colspan="5" style="font-size:13px">' + OrderDetail.ShipAddress + '</td></tr>';
                    html += '<tr><td><span class="tdLabel">Billing Address</span></td><td colspan="5" style="font-size:13px">' + OrderDetail.BillAddress + '</td></tr>';
                    html += '</tbody></table>';

                    html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead><tr>';
                    html += '<thead><tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                    html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                    html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                    html += '<td class="GP" style="text-align: right; width: 15px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                    html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


                    var catId, catName, productname = '', Qty = 0, total = 0.00, count = 1, tr, totalPrice = 0.00;
                    var pageNo = 1, rowcount = 0, TotalMLTax = 0.00, TotalMLQty = 0.00; var totalQtyByCat = 0; TotalNZTax = 0.00; totalTaxVal = 0.00;
                    var orderQty = 0; TotalQuantity = 0;

                    var ItemList = OrderDetail.Item1;
                    
                    for (var l = 0; l < ItemList.length; l++) {
                        var Item = ItemList[l];
                        if (parseInt(OrderDetail.Status) < 3) {
                            orderQty = Item.RequiredQty;
                            totalPrice = parseFloat(Item.NetPrice);
                        } else {
                            orderQty = Item.QtyShip;
                            if (parseInt(Item.QtyShip) == 0) {
                                totalPrice = 0.00;
                            } else {
                                totalPrice = parseFloat(Item.NetPrice);
                            }
                        }
                        rowcount = parseInt(rowcount) + 1;
                        if (Item.IsExchange == 1) {
                            productname = Item.ProductName + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Exchange)' + "</span>";
                        }
                        else if (Item.isFreeItem == 1) {
                            productname = Item.ProductName + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Free)' + "</span>";
                        }
                        else {
                            productname = Item.ProductName;
                        }
                        if (count == 1 || Item.CategoryId == catId) {
                            html += "<tr><td style='text-align:center'>" + orderQty + "</td><td style='text-align:center'>" + Item.UnitType + "</td><td style='text-align:center'>" + Item.TotalPieces + "</td><td style='text-align:center'>" + Item.ProductId + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                            html += "<td>" + Item.Barcode + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + Item.SRP.toFixed(2) + "</td><td  style='text-align: right; width: 15px; white-space: nowrap'>" + parseFloat(Item.GP).toFixed(2) + "</td>";
                            html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + Item.UnitPrice.toFixed(2) + "</td><td style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                            html += "</tr>";
                            catId = Item.CategoryId;
                            catName = Item.CategoryName;
                            total += Number(totalPrice);
                            Qty += Number(orderQty);
                            TotalQuantity += Number(orderQty);
                            TotalMLQty += Number(Item.TotalMLQty);
                            TotalMLTax += Number(Item.TotalMLTax); 
                            if (Number(Item.Tax) > 0) {
                                totalTaxVal = parseFloat(totalTaxVal) + parseFloat(totalPrice);
                                TotalNZTax = totalTaxVal * parseFloat(Item.TaxValue) / 100

                            }
                            if (pageNo == 1 && count == 14 && ItemList.length > 14) {
                                pageNo = Number(pageNo) + 1;
                                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                                html += "<tr><td colspan='10' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " ( Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>)</span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 15px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


                            } else if (((count - 14) % 20) == 0) {
                                pageNo = Number(pageNo) + 1;
                                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                                html += "<tr><td colspan='10' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " ( Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>)</span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 15px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';

                            }

                        } else {
                            count++;
                            html += "<tr style='background: #ccc;font-weight:700;'><td class='totalQty'  style='text-align:center;'>" + Qty + "</td><td  style='text-align:center;'>" + catId + "</td><td colspan='7'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
                            if (TotalNZTax > 0) {
                                html += "<tr style='background:#ccc;font-weight:700;'><td colspan='9' style='white-space: nowrap;text-align:left;'>" + OrderDetail.TaxPrintLabel + "</td><td style='text-align:right'>" + parseFloat(TotalNZTax).toFixed(2) + "</td></tr>";

                            }
                            if (TotalMLTax > 0) {
                                html += "<tr style='background:#ccc;font-weight:700;'><td colspan='8' style='white-space: nowrap'>ML Qty</td><td style='text-align:right'>" + TotalMLQty.toFixed(2) + "</td><td colspan='2' style='text-align:right'>" +
                                    TotalMLTax.toFixed(2) + "</td></tr>";
                                count++;
                            }

                            html += "<tr><td style='text-align:center'>" + orderQty + "</td><td style='text-align:center'>" + Item.UnitType + "</td><td style='text-align:center'>" + Item.TotalPieces + "</td><td style='text-align:center'>" + Item.ProductId + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                            html += "<td>" + Item.Barcode + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(Item.SRP).toFixed(2) + "</td><td  style='text-align: right; width: 15px; white-space: nowrap'>" + parseFloat(Item.GP).toFixed(2) + "</td>";
                            html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(Item.UnitPrice).toFixed(2) + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                            html += "</tr>";
                            Qty = 0;
                            total = 0.00;
                            TotalMLQty = 0.00;
                            TotalMLTax = 0.00;
                            totalTaxVal = 0.00;
                            TotalNZTax = 0.00;
                            if (pageNo == 1 && count == 14 && ItemList.length > 14) {
                                pageNo = Number(pageNo) + 1;
                                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                                html += "<tr><td colspan='10' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " ( Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>)</span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 15px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


                            } else if (((count - 14) % 20) == 0) {
                                pageNo = Number(pageNo) + 1;
                                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                                html += "<tr><td colspan='10' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " ( Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>) </span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 15px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';

                            }
                            catId = Item.CategoryId;
                            catName = Item.CategoryName;
                            total += Number(totalPrice);
                            Qty += Number(orderQty);
                            TotalQuantity += Number(orderQty);
                            TotalMLQty += Number(Item.TotalMLQty);
                            TotalMLTax += Number(Item.TotalMLTax);

                            if (Number(Item.Tax) > 0) {
                                totalTaxVal = parseFloat(totalTaxVal) + parseFloat(totalPrice);
                                TotalNZTax = totalTaxVal * parseFloat(Item.TaxValue) / 100

                            } 
                        }
                        count++;
                        orderQty = 0; 
                    }
                    html += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty' style='text-align:center;'>" + Qty + "</td><td style='text-align:center;'>" + catId + "</td><td colspan='7'>" + catName + "</td><td style='text-align:right;'>" + parseFloat(total).toFixed(2) + "</td></tr>";
                    if (TotalNZTax > 0) {
                        html += "<tr style='background:#ccc;font-weight:700;'><td colspan='9' style='white-space: nowrap;text-align:left;'>" + OrderDetail.TaxPrintLabel + "</td><td style='text-align:right'>" + parseFloat(TotalNZTax).toFixed(2) + "</td></tr>";

                    }
                    if (TotalMLTax > 0) {
                        html += "<tr style='background:#ccc;font-weight:700;'><td colspan='8' style='white-space: nowrap'>ML Qty</td><td style='text-align:right'>" + parseFloat(TotalMLQty).toFixed(2) + "</td><td colspan='2' style='text-align:right'>" +
                            parseFloat(TotalMLTax).toFixed(2) + "</td></tr>";
                    }




                    html += '</tbody></table>'; 

                    //------------------------------------------------------------------------Terms----------------------------------------------------------------------------------------------

                    html += '<fieldset><table style="width: 100%;">';
                    html += '<tbody><tr><td style="width: 66%;" colspan="2"><center><div id="TC">' + Company.TermsCondition + '</div></center></td>';
                    html += '<td style="width: 33%;"><table class="table tableCSS"><tbody>';
                    html += '<tr><td class="tdLabel">Total Qty</td><td id="totalQty" style="text-align: right;">' + TotalQuantity + '</td></tr>';
                    html += '<tr><td class="tdLabel">Sub Total</td><td style="text-align: right;"><span id="totalwoTax">' + total.toFixed(2) + '</span></td></tr>';
                    if (OrderDetail.OverallDiscAmt > 0) {
                        html += '<tr style="display:none"><td class="tdLabel">Discount</td><td style="text-align: right;"><span id="disc">' + parseFloat(OrderDetail.OverallDiscAmt).toFixed(2) + '</span></td> </tr>';
                    }
                    if (OrderDetail.TotalTax > 0) {
                        html += '<tr style="display:none"><td class="tdLabel">' + OrderDetail.TaxPrintLabel + '</td><td style="text-align: right;"><span id="tax">' + parseFloat(OrderDetail.TotalTax).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.ShippingCharges > 0) {
                        html += '<tr style="display:none"><td class="tdLabel">Shipping</td><td style="text-align: right;"><span id="shipCharges">' + parseFloat(OrderDetail.ShippingCharges).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.MLTax > 0) {
                        html += '<tr ><td class="tdLabel" id="Prntlebel" style="diplay:none">' + OrderDetail.MLTaxPrintLabel + '</td><td style="text-align: right;"><span id="MLTax">' + parseFloat(OrderDetail.MLTax).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.WeightTax > 0.00) {
                        html += '<tr style="display:none"><td class="tdLabel"  style="diplay:none">' + OrderDetail.WeightTaxPrintLabel + '</td><td style="text-align: right;"><span id="WeightTax">' + parseFloat(OrderDetail.WeightTax).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.AdjustmentAmt != 0) {
                        html += '<tr style="display:none"><td class="tdLabel">Adjustment</td><td style="text-align: right;"><span id="spAdjustment">' + parseFloat(OrderDetail.AdjustmentAmt).toFixed(2) + '</span></td></tr>';
                    }

                    html += '<tr><td class="tdLabel">Grand Total</td><td style="text-align: right;"><span id="grandTotal">' + parseFloat(parseFloat(total) + parseFloat(OrderDetail.MLTax)).toFixed(2) + '</span></td></tr>';
                    if (OrderDetail.DeductionAmount > 0) {
                        html += '<tr style="display:none"><td class="tdLabel" style="display:none">Credit memo</td><td style="text-align: right;"><span id="spnCreditmemo">' + parseFloat(OrderDetail.DeductionAmount).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.CreditAmount > 0) {
                        html += '<tr style="display:none"><td class="tdLabel" style="display:none">Store Credit</td><td style="text-align: right;"><span id="spnStoreCredit">' + parseFloat(OrderDetail.CreditAmount).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.CreditAmount > 0 || OrderDetail.DeductionAmount > 0) {
                        html += '<tr style="display:none"><td class="tdLabel" >Payable Amount</td><td style="text-align: right;"><span id="spnPayableAmount">' + parseFloat(OrderDetail.PayableAmount).toFixed(2) + '</span></td></tr>';
                    }
                    html += '</tbody></table></td></tr></tbody></table></fieldset>';

                    //-----------------------------------------------------------------------------------Open Balance-------------------------------------------------------------------------------------

                    
                    $('#All').append(html); 
                    $('.' + OrderDetail.OrderNo).html(pageNo);
                    html = '';
                }
            }
            window.print();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
 

