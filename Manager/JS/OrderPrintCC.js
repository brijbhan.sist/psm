﻿
var Print = 0, PrintLabel = '';
$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('OrderAutoId');

    Print = getQueryString('Print');
    if (print == null) {
        Print = 0;
    }
    if (getid != null) {
        getOrderData(getid);
    }
});
//                                                     Edit Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/WebAPI/WPrintOrder.asmx/GetOrderPrint_OrderPrintCC",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
                return;
            }
            var xmldoc = $.parseXML(response.d);
            var Company = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");
            var items = $(xmldoc).find("Table2");
            var DuePayment = $(xmldoc).find("Table3");
            var CreditMemoNo = $(xmldoc).find("Table3");
            if (Print == 1) {
                var check = 0;
                var DueAmount = 0;
                $("#tblduePayment tbody tr").remove();
                var rowdue = $("#tblduePayment thead tr").clone(true);
                if (DuePayment.length > 0) {
                    $(DuePayment).each(function (index) {
                        if (parseFloat($(this).find('AmtDue').text()) > 0) {
                            $(".SrNo", rowdue).text(Number(check) + 1);
                            $(".OrderNo", rowdue).text($(this).find('OrderNo').text());
                            $(".OrderDate", rowdue).text($(this).find('OrderDate').text());
                            $(".AmtDue", rowdue).html($(this).find('AmtDue').text());
                            DueAmount += parseFloat($(this).find('AmtDue').text());
                            $('#tblduePayment tbody').append(rowdue);
                            rowdue = $("#tblduePayment tbody tr:last").clone(true);
                            check = check + 1;
                        }
                    })

                }
                $("#tblduePayment tfoot tr").find("td:eq(1)").text(DueAmount.toFixed(2));
                if (parseFloat(DueAmount) > 0) {
                    $(".DueAmount").show();
                } else {
                    $(".DueAmount").hide();
                }
            } else {
                $(".DueAmount").hide();
            }

            var LoginEmpType = $(xmldoc).find("Table5").find("LoginEmpType").text();

            $("#logo").attr("src", "../Img/logo/" + $(Company).find("Logo").text())
            $("#Address").text($(Company).find("Address").text());
            $("#Phone").text($(Company).find("MobileNo").text());
            $("#FaxNo").text($(Company).find("FaxNo").text());
            $("#Website").text($(Company).find("Website").text());
            $("#Prntlebel").text($(order).find("MLTaxPrintLabel").text());
            $("#weightTaxPrintLabel").text($(order).find("WeightTaxPrintLabel").text());
            $("#stateTaxPrintlabel").text($(order).find("TaxPrintLabel").text());

            $("#TC").html($(Company).find("TermsCondition").text());
            $("#CompanyName").html($(Company).find("CompanyName").text());
            $("#EmailAddress").html($(Company).find("EmailAddress").text());
            if (LoginEmpType == '6' || LoginEmpType == '10') {
                $("#PageHeading").html($(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + ' (Page 1 of <span class="TotalPage"></span>)<span style="float:right;font-weight:600"><i>Sales Order</i></span>');
            } else {
                $("#PageHeading").html($(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + ' (Page 1 of <span class="TotalPage"></span>)<span style="float:right;font-weight:600"><i>Sales Order</i></span>');
            }

            $("#acNo").text($(order).find("CustomerId").text());
            $("#BusinessName").text($(order).find("BusinessName").text());
            if ($(order).find("BusinessName").text() != '') {
                $("#BusinessName").text($(order).find("BusinessName").text());
                $("#BusinessName").closest('tr').show();
            }
            $("#storeName").text($(order).find("ContactPersonName").text());
            $("#ContPerson").text($(order).find("Contact").text());
            $("#terms").text($(order).find("TermsDesc").text());

            $("#shipAddr").text($(order).find("ShipAddr").text());

            $("#billAddr").text($(order).find("BillAddr").text());

            $("#salesRep").text($(order).find("SalesPerson").text());
            $("#so").text($(order).find("OrderNo").text());
            $("#soDate").text($(order).find("OrderDate").text());
            $("#custType").text($(order).find("CustomerType").text());
            $("#pkdBy").text($(order).find("PackerName").text());
            $("#shipVia").text($(order).find("ShippingType").text());
            var html = '<table class="table tableCSS tblProduct" id="tblProduct"><thead><tr>';
            html += '<thead><tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
            html += '<td style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Product Name</td>';
            html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
            html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
            html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
            var row = $("#tblProduct thead tr").clone(true);
            var catId, catName, Qty = 0, total = 0.00, count = 1, tr, totalPrice = 0.00;
            var pageNo = 1,productname = '', rowcount = 0, TotalMLTax = 0.00, TotalMLQty = 0.00;
            var orderQty = 0;
            var totalTaxVal = 0.00;
            var totalCalculatedTax = 0.00, iDisc=0.00;

            $.each(items, function () {
                iDisc = $(this).find("Del_discount").text();
                if (iDisc == '0.00' || iDisc == '0') {
                    iDisc = "";
                }
                else {
                    if (iDisc != '') {
                        iDisc = iDisc.toString() + " %";
                    }
                }
                if (parseInt($(order).find("Status").text()) < 3) {
                    orderQty = $(this).find("RequiredQty").text();
                    totalPrice = parseFloat($(this).find("NetPrice").text());
                } else {
                    orderQty = $(this).find("QtyShip").text();
                    if (parseInt($(this).find("QtyShip").text()) == 0) {
                        totalPrice = 0.00;
                    } else {
                        totalPrice = parseFloat($(this).find("NetPrice").text());
                    }
                }
                
                if ($(this).find("IsExchange").text() == 1) {
                    productname = $(this).find("ProductName").text() + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Exchange)' + "</span>";
                }
                else if ($(this).find("isFreeItem").text() == 1) {
                    productname = $(this).find("ProductName").text() + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Free)' + "</span>";
                }
                else {
                    productname = $(this).find("ProductName").text();
                }
                if (count == 1 || $(this).find("CategoryId").text() == catId) {
                   
                    html += "<tr><td>" + orderQty + "</td><td>" + $(this).find("UnitType").text() + "</td><td>" + $(this).find("TotalPieces").text() + "</td><td>" + $(this).find("ProductId").text() + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                    html += "<td>" + $(this).find("Barcode").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("SRP").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("PrintGP").text() + "</td>";
                    html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + iDisc + "</td><td style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                    html += "</tr>";
                    rowcount = parseInt(rowcount) + 1;
                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    if ($(this).find("Tax").text() > 0) {
                        totalTaxVal = parseFloat(totalTaxVal) + parseFloat(totalPrice);
                        totalCalculatedTax = totalTaxVal * parseFloat($(order).find("TaxValue").text()) / 100
                    }


                    total += Number(totalPrice);
                    Qty += Number(orderQty);
                    TotalMLQty += Number($(this).find("TotalMLQty").text());
                    TotalMLTax += Number($(this).find("TotalMLTax").text());
                    if (pageNo == 1 && rowcount >= 18) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Product Name</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                        html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                    else if (((rowcount) % 26) == 0) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Product Name</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                        html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                } else {
                    html += "<tr style='background: #ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='7'>" + catName + "</td><td></td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
                    rowcount = parseInt(rowcount) + 1;
                    if (rowcount == 18 && pageNo == 1) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + "   (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px;text-align:center">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap;text-align:center">Unit Type</td>';
                        html += '<td class="Qty" style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap;text-align:center">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                        html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                    if (rowcount == 26) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + "   (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px;text-align:center">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap;text-align:center">Unit Type</td>';
                        html += '<td class="Qty" style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap;text-align:center">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                        html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }


                    if (totalCalculatedTax > 0) {
                        html += "<tr style='background:#ccc;font-weight:700;'><td colspan='10' style='white-space: nowrap;text-align:left;'>" + $(order).find("TaxPrintLabel").text()+"</td><td style='text-align:right'>" + parseFloat(totalCalculatedTax).toFixed(2) + "</td></tr>";
                         rowcount = parseInt(rowcount) + 1;
                    }
                    if (TotalMLTax > 0) {
                        html += "<tr style='background:#ccc;font-weight:700;'><td colspan='9' style='white-space: nowrap'>ML Qty</td><td style='text-align:right'>" + TotalMLQty.toFixed(2) + "</td><td style='text-align:right'>" +
                            TotalMLTax.toFixed(2) + "</td></tr>";
                        rowcount = parseInt(rowcount) + 1;
                    }  


                    if (rowcount == 26) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + "   (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px;text-align:center">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap;text-align:center">Unit Type</td>';
                        html += '<td class="Qty" style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap;text-align:center">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                        html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }

                    html += "<tr><td>" + orderQty + "</td><td>" + $(this).find("UnitType").text() + "</td><td>" + $(this).find("TotalPieces").text() + "</td><td>" + $(this).find("ProductId").text() + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                    html += "<td>" + $(this).find("Barcode").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("SRP").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("PrintGP").text() + "</td>";
                    html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + iDisc + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                    html += "</tr>";
                    rowcount = parseInt(rowcount) + 1;
                    Qty = 0;
                    total = 0.00;
                    TotalMLQty = 0.00;
                    TotalMLTax = 0.00;
                    totalTaxVal = 0.00;
                    totalCalculatedTax = 0.00;
                    if (pageNo == 1 && rowcount >= 18) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Product Name</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                        html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;

                    } else if (((rowcount) % 26) == 0) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Product Name</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                        html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    if ($(this).find("Tax").text() > 0) {
                        totalTaxVal = parseFloat(totalTaxVal) + parseFloat(totalPrice);
                        totalCalculatedTax = totalTaxVal * parseFloat($(order).find("TaxValue").text()) / 100
                    }
                    total += Number(totalPrice);
                    Qty += Number(orderQty);
                    TotalMLQty += Number($(this).find("TotalMLQty").text());
                    TotalMLTax += Number($(this).find("TotalMLTax").text());
                    
                }
                
                count++;
                orderQty = 0;


                
            });

            
            html += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='7'>" + catName + "</td><td></td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
            rowcount = Number(rowcount) + 1;
            if (totalCalculatedTax > 0) {
                html += "<tr style='background:#ccc;font-weight:700;'><td colspan='10' style='white-space: nowrap;text-align:left;'>" + $(order).find("TaxPrintLabel").text()+"</td><td style='text-align:right'>" + parseFloat(totalCalculatedTax).toFixed(2) + "</td></tr>";
                rowcount = Number(rowcount) + 1;
            }


            if (TotalMLTax > 0) {
                html += "<tr style='background:#ccc;font-weight:700;'><td colspan='9' style='white-space: nowrap'>ML Qty</td><td style='text-align:right'>" + TotalMLQty.toFixed(2) + "</td><td style='text-align:right'>" +
                    TotalMLTax.toFixed(2) + "</td></tr>";
                rowcount = Number(rowcount) + 1;
            }
            html += '</tbody></table>';
            if ((rowcount == 18 && pageNo == 1)) {
                pageNo = Number(pageNo) + 1;
                html += "<p  style='page-break-before: always'></p><br/>";
                html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + "   (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                html += "</thead > </table>";
                rowcount = 0;
            }

            $('#tableDynamic').html(html);
            $('.TotalPage').html(pageNo);

            var totalQtyByCat = 0;
            $('.tblProduct tbody tr').each(function () {

                totalQtyByCat += Number($(this).find(".totalQty").text());
            });
            $("#totalQty").text(totalQtyByCat); 
            $("#totalwoTax").text($(order).find("TotalAmount").text());

            if (parseFloat($(order).find("OverallDiscAmt").text()) > 0) {
                $("#disc").text('-'+$(order).find("OverallDiscAmt").text());
            } else {
                $("#disc").closest('tr').hide();
            }

            if (parseFloat($(order).find("TotalTax").text()) > 0) {
                $("#tax").text($(order).find("TotalTax").text());
            } else {
                $("#tax").closest('tr').hide();
            }
            if (parseFloat($(order).find("ShippingCharges").text()) > 0.00) {
                $("#shipCharges").text($(order).find("ShippingCharges").text());
            } else {
                $("#shipCharges").closest('tr').hide();
                $("#shpsh").closest('tr').hide();
            }

            if (parseFloat($(order).find("MLTax").text()) > 0) {
               // $("#MLQty").text($(order).find("MLQty").text());
                $("#MLTax").text($(order).find("MLTax").text());
            } else {
                $("#MLTax").closest('tr').hide();
               // $("#MLQty").closest('tr').hide();
            }

            if (parseFloat($(order).find("WeightTax").text()) > 0.00) {
                $("#WeightTax").text($(order).find("WeightTax").text());
            }
            else {
                $("#WeightTax").closest('tr').hide();
            }
            debugger;
            if (parseFloat($(order).find("DeductionAmount").text()) > 0) {
                var CrNo = 'Credit Memo Applied ['
                $.each(CreditMemoNo, function (index) {
                    if (index == CreditMemoNo.length - 1) {
                        CrNo += $(this).find("CreditNo").text() + "]";
                    }
                    else {
                        CrNo += $(this).find("CreditNo").text() + ",";
                    }
                })
                $("#crmNo").html(CrNo);
                if (parseFloat($(order).find("DeductionAmount").text()) > parseFloat($(order).find("GrandTotal").text())) {
                    $("#spnCreditmemo").text('-' + $(order).find("GrandTotal").text());
                }
                else {
                    $("#spnCreditmemo").text('-' + $(order).find("DeductionAmount").text());
                }
            } else {
                $("#spnCreditmemo").closest('tr').hide();
            }
            if (parseFloat($(order).find("AdjustmentAmt").text()) != 0) {
                $("#spAdjustment").text($(order).find("AdjustmentAmt").text());
            } else {
                $("#spAdjustment").closest('tr').hide();
            }

            $("#grandTotal").text($(order).find("GrandTotal").text());

            if (parseFloat($(order).find("CreditAmount").text()) > 0) {
                $("#spnStoreCredit").text($(order).find("CreditAmount").text());
            } else {
                $("#spnStoreCredit").closest('tr').hide();
            }
            if (parseFloat($(order).find("CreditAmount").text()) > 0 || parseFloat($(order).find("DeductionAmount").text()) > 0) {
                $("#spnPayableAmount").text($(order).find("PayableAmount").text());
            } else {
                $("#spnPayableAmount").closest('tr').hide();
            }

            setTimeout(function () { window.print(); }, 200);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}