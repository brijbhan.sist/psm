﻿
var Print = 1, PrintLabel = '';
$(document).ready(function () {
    debugger;
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    if (localStorage.getItem('BulkOrderAutoId') != null) {
        var getids = localStorage.getItem('BulkOrderAutoId');
        getOrderData(getids);
    }

});



//                                                     Edit Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/WebAPI/WPrintOrder.asmx/getBulkOrderData",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            debugger;
            if (response.d == 'Session Expired') {
                location.href = '/';
                return;
            }
            var html = '';
            var OrderList = $.parseJSON(response.d);
            for (var i = 0; i < OrderList.length; i++) {
                var Company = OrderList[i];
                var Order = Company.OrderDetails;
                for (var j = 0; j < Order.length; j++) {
                    var OrderDetail = Order[j];

                    html += '<p style="page-break-before: always"></p><fieldset><table class="table tableCSS"><tbody><tr><td colspan="6" style="text-align: center">';
                    html += '<span style="font-size: 20px;font-weight:100">' + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + ' ( Page 1 of <span class=' + OrderDetail.OrderNo + '></span>)</span><span style="font-size: 20px;float:right" ><b><i>SALES ORDER</i></b></span>';
                    html += '</td></tr><tr><td rowspan="2" style="width: 80px !important;"><img src="../Img/logo/' + Company.Logo + '" class="img-responsive"></td>';
                    html += '<td>' + Company.CompanyName + '</td>';
                    html += '<td class="tdLabel" style="white-space: nowrap">Customer ID</td><td>' + OrderDetail.CustomerId + '</td>';
                    html += '<td class="tdLabel" style="white-space: nowrap">Sales Rep</td><td>' + OrderDetail.SalesPerson + '</td></tr>';
                    html += '<tr><td><span>' + Company.Address + '</span></td><td class="tdLabel" style="white-space: nowrap">Customer Contact</td>';
                    html += '<td style="white-space: nowrap">' + OrderDetail.ContactPersonName + '</td><td class="tdLabel">Order No </td>';
                    html += '<td>' + OrderDetail.OrderNo + '</td></tr><tr><td><span>' + Company.Website + '</span></td>';
                    html += '<td>Phone : <span>' + Company.MobileNo + '</span></td><td class="tdLabel">Contact No</td>';
                    html += '<td>' + OrderDetail.Contact + '</td><td class="tdLabel" style="white-space: nowrap">Order Date</td>';
                    html += '<td style="white-space: nowrap">' + OrderDetail.OrderDate + '</td></tr>';
                    html += '<tr><td>' + Company.EmailAddress + '</td><td>Fax : ' + Company.FaxNo + '</td><td class="tdLabel">Terms</td>';
                    html += '<td style="font-size:13px"><b>' + OrderDetail.TermsDesc + '</b></td><td class="tdLabel">Ship Via</td><td>' + OrderDetail.ShippingType + '</td></tr>';
                    if (OrderDetail.BusinessName != '' && OrderDetail.BusinessName != undefined) {
                        html += '<tr><td><span class="tdLabel">Business Name:</span></td><td>' + OrderDetail.BusinessName + '</td></tr>';
                    }
                    html += '<tr><td><span class="tdLabel">Shipping Address</span></td><td colspan="5" style="font-size:13px">' + OrderDetail.ShipAddress +  '</td></tr>';
                    html += '<tr><td><span class="tdLabel">Billing Address</span></td><td colspan="5" style="font-size:13px">' + OrderDetail.BillAddress + '</td></tr>';
                    html += '</tbody></table>';

                    html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead><tr>';
                    html += '<thead><tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                    html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                    html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                    html += '<td class="GP" style="text-align: right; width: 15px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                    html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


                    var catId, catName, productname = '', Qty = 0, total = 0.00, count = 1, tr, totalPrice = 0.00;
                    var pageNo = 1, rowcount = 0, TotalMLTax = 0.00, TotalMLQty = 0.00; var totalQtyByCat = 0; TotalNZTax = 0.00; totalTaxVal = 0.00;
                    var orderQty = 0; TotalQuantity = 0;

                    var ItemList = OrderDetail.Item1;
                    if (OrderDetail.Item2.length > 0) {
                        ItemList = OrderDetail.Item2;
                    }

                    for (var l = 0; l < ItemList.length; l++) {
                        var Item = ItemList[l];
                        if (parseInt(OrderDetail.Status) < 3) {
                            orderQty = Item.RequiredQty;
                            totalPrice = parseFloat(Item.NetPrice);
                        } else {
                            orderQty = Item.QtyShip;
                            if (parseInt(Item.QtyShip) == 0) {
                                totalPrice = 0.00;
                            } else {
                                totalPrice = parseFloat(Item.NetPrice);
                            }
                        }
                        rowcount = parseInt(rowcount) + 1;
                        if (Item.IsExchange == 1) {
                            productname = Item.ProductName + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Exchange)' + "</span>";
                        }
                        else if (Item.isFreeItem == 1) {
                            productname = Item.ProductName + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Free)' + "</span>";
                        }
                        else {
                            productname = Item.ProductName;
                        }
                        if (count == 1 || Item.CategoryId == catId) {
                            html += "<tr><td style='text-align:center'>" + orderQty + "</td><td style='text-align:center'>" + Item.UnitType + "</td><td style='text-align:center'>" + Item.TotalPieces + "</td><td style='text-align:center'>" + Item.ProductId + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                            html += "<td>" + Item.Barcode + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + Item.SRP.toFixed(2) + "</td><td  style='text-align: right; width: 15px; white-space: nowrap'>" + parseFloat(Item.GP).toFixed(2) + "</td>";
                            html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + Item.UnitPrice.toFixed(2) + "</td><td style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                            html += "</tr>";
                            catId = Item.CategoryId;
                            catName = Item.CategoryName;
                            total += Number(totalPrice);
                            Qty += Number(orderQty);
                            TotalQuantity += Number(orderQty);
                            TotalMLQty += Number(Item.TotalMLQty);
                            TotalMLTax += Number(Item.TotalMLTax);
                            console.log(Number(Item.Tax));
                            if (Number(Item.Tax) > 0) {
                                totalTaxVal = parseFloat(totalTaxVal) + parseFloat(totalPrice);
                                TotalNZTax = totalTaxVal * parseFloat(Item.TaxValue) / 100

                            }
                            if (pageNo == 1 && count == 14 && ItemList.length > 14) {
                                pageNo = Number(pageNo) + 1;
                                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                                html += "<tr><td colspan='10' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " ( Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>)</span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 15px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


                            } else if (((count - 14) % 20) == 0) {
                                pageNo = Number(pageNo) + 1;
                                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                                html += "<tr><td colspan='10' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " ( Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>)</span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 15px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';

                            }

                        } else {
                            count++;
                            html += "<tr style='background: #ccc;font-weight:700;'><td class='totalQty'  style='text-align:center;'>" + Qty + "</td><td  style='text-align:center;'>" + catId + "</td><td colspan='7'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
                            if (TotalNZTax > 0) {
                                html += "<tr style='background:#ccc;font-weight:700;'><td colspan='9' style='white-space: nowrap;text-align:left;'>" + OrderDetail.TaxPrintLabel + "</td><td style='text-align:right'>" + parseFloat(TotalNZTax).toFixed(2) + "</td></tr>";

                            }
                            if (TotalMLTax > 0) {
                                html += "<tr style='background:#ccc;font-weight:700;'><td colspan='8' style='white-space: nowrap'>ML Qty</td><td style='text-align:right'>" + TotalMLQty.toFixed(2) + "</td><td colspan='2' style='text-align:right'>" +
                                    TotalMLTax.toFixed(2) + "</td></tr>";
                                count++;
                            }

                            html += "<tr><td style='text-align:center'>" + orderQty + "</td><td style='text-align:center'>" + Item.UnitType + "</td><td style='text-align:center'>" + Item.TotalPieces + "</td><td style='text-align:center'>" + Item.ProductId + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                            html += "<td>" + Item.Barcode + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(Item.SRP).toFixed(2) + "</td><td  style='text-align: right; width: 15px; white-space: nowrap'>" + parseFloat(Item.GP).toFixed(2) + "</td>";
                            html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(Item.UnitPrice).toFixed(2) + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                            html += "</tr>";
                            Qty = 0;
                            total = 0.00;
                            TotalMLQty = 0.00;
                            TotalMLTax = 0.00;
                            totalTaxVal = 0.00;
                            TotalNZTax = 0.00;
                            if (pageNo == 1 && count == 14 && ItemList.length > 14) {
                                pageNo = Number(pageNo) + 1;
                                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                                html += "<tr><td colspan='10' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " ( Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>)</span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 15px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


                            } else if (((count - 14) % 20) == 0) {
                                pageNo = Number(pageNo) + 1;
                                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                                html += "<tr><td colspan='10' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " ( Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>) </span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 15px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';

                            }
                            catId = Item.CategoryId;
                            catName = Item.CategoryName;
                            total += Number(totalPrice);
                            Qty += Number(orderQty);
                            TotalQuantity += Number(orderQty);
                            TotalMLQty += Number(Item.TotalMLQty);
                            TotalMLTax += Number(Item.TotalMLTax);
                            if (Number(Item.Tax) > 0) {
                                totalTaxVal = parseFloat(totalTaxVal) + parseFloat(totalPrice);
                                TotalNZTax = totalTaxVal * parseFloat(Item.TaxValue) / 100

                            }
                            //TotalNZTax += Number(Item.TaxValue);

                        }
                        count++;
                        orderQty = 0;
                        //var totalQtyByCat = 0;
                        //totalQtyByCat += Number(Qty);
                    }
                    html += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty' style='text-align:center;'>" + Qty + "</td><td style='text-align:center;'>" + catId + "</td><td colspan='7'>" + catName + "</td><td style='text-align:right;'>" + parseFloat(total).toFixed(2) + "</td></tr>";
                    if (TotalNZTax > 0) {
                        html += "<tr style='background:#ccc;font-weight:700;'><td colspan='9' style='white-space: nowrap;text-align:left;'>" + OrderDetail.TaxPrintLabel + "</td><td style='text-align:right'>" + parseFloat(TotalNZTax).toFixed(2) + "</td></tr>";

                    }
                    if (TotalMLTax > 0) {
                        html += "<tr style='background:#ccc;font-weight:700;'><td colspan='8' style='white-space: nowrap'>ML Qty</td><td style='text-align:right'>" + parseFloat(TotalMLQty).toFixed(2) + "</td><td colspan='2' style='text-align:right'>" +
                            parseFloat(TotalMLTax).toFixed(2) + "</td></tr>";
                    }




                    html += '</tbody></table>';
                    //totalQtyByCat += Number(Qty);


                    //------------------------------------------------------------------------Terms----------------------------------------------------------------------------------------------

                    html += '<fieldset><table style="width: 100%;">';
                    html += '<tbody><tr><td style="width: 66%;" colspan="2"><center><div id="TC">' + Company.TermsCondition + '</div></center></td>';
                    html += '<td style="width: 33%;"><table class="table tableCSS"><tbody>';
                    html += '<tr><td class="tdLabel">Total Qty</td><td id="totalQty" style="text-align: right;">' + TotalQuantity + '</td></tr>';
                    html += '<tr><td class="tdLabel">Sub Total</td><td style="text-align: right;"><span id="totalwoTax">' + parseFloat(OrderDetail.TotalAmount).toFixed(2) + '</span></td></tr>';
                    if (OrderDetail.OverallDiscAmt > 0) {
                        html += '<tr><td class="tdLabel">Discount</td><td style="text-align: right;"><span id="disc">' + parseFloat(OrderDetail.OverallDiscAmt).toFixed(2) + '</span></td> </tr>';
                    }
                    if (OrderDetail.TotalTax > 0) {
                        html += '<tr><td class="tdLabel">' + OrderDetail.TaxPrintLabel + '</td><td style="text-align: right;"><span id="tax">' + parseFloat(OrderDetail.TotalTax).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.ShippingCharges > 0) {
                        html += '<tr><td class="tdLabel">Shipping</td><td style="text-align: right;"><span id="shipCharges">' + parseFloat(OrderDetail.ShippingCharges).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.MLTax > 0) {
                        html += '<tr><td class="tdLabel" id="Prntlebel">' + OrderDetail.MLTaxPrintLabel + '</td><td style="text-align: right;"><span id="MLTax">' + parseFloat(OrderDetail.MLTax).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.WeightTax > 0.00) {
                        html += '<tr><td class="tdLabel">' + OrderDetail.WeightTaxPrintLabel + '</td><td style="text-align: right;"><span id="WeightTax">' + parseFloat(OrderDetail.WeightTax).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.AdjustmentAmt != 0) {
                        html += '<tr><td class="tdLabel">Adjustment</td><td style="text-align: right;"><span id="spAdjustment">' + parseFloat(OrderDetail.AdjustmentAmt).toFixed(2) + '</span></td></tr>';
                    }

                    html += '<tr><td class="tdLabel">Grand Total</td><td style="text-align: right;"><span id="grandTotal">' + parseFloat(OrderDetail.GrandTotal).toFixed(2) + '</span></td></tr>';
                    if (OrderDetail.DeductionAmount > 0) {
                        html += '<tr><td class="tdLabel">Credit memo</td><td style="text-align: right;"><span id="spnCreditmemo">' + parseFloat(OrderDetail.DeductionAmount).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.CreditAmount > 0) {
                        html += '<tr><td class="tdLabel">Store Credit</td><td style="text-align: right;"><span id="spnStoreCredit">' + parseFloat(OrderDetail.CreditAmount).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.CreditAmount > 0 || OrderDetail.DeductionAmount > 0) {
                        html += '<tr><td class="tdLabel">Payable Amount</td><td style="text-align: right;"><span id="spnPayableAmount">' + parseFloat(OrderDetail.PayableAmount).toFixed(2) + '</span></td></tr>';
                    }
                    html += '</tbody></table></td></tr></tbody></table></fieldset>';

                    //-----------------------------------------------------------------------------------Open Balance-------------------------------------------------------------------------------------

                    var OpenBalance = OrderDetail.DueOrderList;

                    html = FnManageDueOrder(OpenBalance, html, OrderDetail);


                    var CreditMemo = OrderDetail.CreditDetails;

                    if (CreditMemo != '') {

                        for (var n = 0; n < CreditMemo.length; n++) {

                            var CreditDetail = CreditMemo[n];

                            html = getCreditData(CreditDetail, html, Company);

                        }
                    }

                    $('#All').append(html);
                    $('#All').append(html);
                    $('.' + OrderDetail.OrderNo).html(pageNo);
                    html = '';
                }
            }
            window.print();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
function FnManageDueOrder(OpenBalance, html, OrderDetail) {
    if (OpenBalance.length > 0) {
        var DueAmount = 0, Aging = 0, AmtPaid = 0, GrandTotal = 0;
        html += '<p  style="page-break-before: always"></p><br/><fieldset><table class="table tableCSS" style="width: 100%">';
        html += '<tr><td style="font-size: 20px; text-align: center">OPEN BALANCE</td></tr>';
        html += '<tr><td style="font-size: 20px; text-align: center"><span id="lblCustomerName">' + OrderDetail.CustomerName + '</span></td></tr>';
        html += '</table>';
        html += '<table class="table tableCSS" id="tblduePayment">';
        html += '<thead>';
        html += '<tr><td class="OrderNo  center" style="width: 20%">Order No</td><td class="OrderDate center" style="width: 20%">Order Date</td><td class="GrandTotal right" style="width: 20%;">Order Amount</td><td class="AmtPaid right" style="width: 20%;">Paid Amount</td><td class="AmtDue right">Open Amount</td><td class="Aging center">Aging (Days)</td></tr>';
        html += '</thead>';
        html += '<tbody>';
        for (var m = 0; m < OpenBalance.length; m++) {
            var DueOrder = OpenBalance[m];

            DueAmount += DueOrder.AmtDue;
            Aging += DueOrder.Aging;
            AmtPaid += DueOrder.AmtPaid;
            GrandTotal += DueOrder.GrandTotal;

            html += '<tr><td style="text-align: center">' + DueOrder.OrderNo + '</td><td style="text-align: center">' + DueOrder.OrderDate + '</td><td style="text-align: right">' + parseFloat(DueOrder.GrandTotal).toFixed(2) + '</td><td style="text-align: right">' + parseFloat(DueOrder.AmtPaid).toFixed(2) + '</td><td style="text-align: right">' + parseFloat(DueOrder.AmtDue).toFixed(2) + '</td><td style="text-align: center">' + DueOrder.Aging + '</td>';
            html += '</tr>';

        }
        html += '</tbody > ';
        html += '<tfoot><tr><td style="width: 10%" colspan="2">Total</td><td id="FAmtPaid" style="text-align: right">' + parseFloat(GrandTotal).toFixed(2) + '</td><td id="Td1" style="text-align: right">' + parseFloat(AmtPaid).toFixed(2) + '</td>';

        html += '<td id="FAmtDue" style="text-align: right">' + parseFloat(DueAmount).toFixed(2) + '</td><td class="center"></td>';
        html += '</tr></tfoot>';
        html += '</table></fieldset>';
        html += '</fieldset>';
    }
    return html;
}
function getCreditData(CreditDetail, html, Company) {

    //if (CreditDetail.length > 0) {
    console.log(CreditDetail.OrderNo)
    html += '<p style="page-break-before: always"></p><fieldset><table class="table tableCSS"><tbody><tr><td colspan="6" style="text-align: center">';
    html += '<span style="font-size: 20px;">' + CreditDetail.CustomerName + ' -  ' + CreditDetail.OrderNo + ' ( Page 1 of #########)</span><span style="font-size: 20px;float:right" ><b><i>CREDIT MEMO</i></b></span>';
    html += '</td></tr><tr><td rowspan="2" style="width: 80px !important;"><img src="../Img/logo/' + Company.Logo + '" class="img-responsive"></td>';
    html += '<td>' + Company.CompanyName + '</td>';
    html += '<td class="tdLabel" style="white-space: nowrap">Customer ID</td><td>' + CreditDetail.CustomerId + '</td>';
    html += '<td class="tdLabel" style="white-space: nowrap">Sales Rep</td><td>' + CreditDetail.SalesPerson + '</td></tr>';
    html += '<tr><td><span>' + Company.Address + '</span></td><td class="tdLabel" style="white-space: nowrap">Customer Contact</td>';
    html += '<td style="white-space: nowrap">' + CreditDetail.ContactPersonName + '</td><td class="tdLabel">Credit No </td>';
    html += '<td>' + CreditDetail.OrderNo + '</td></tr><tr><td><span>' + Company.Website + '</span></td>';
    html += '<td>Phone : <span>' + Company.MobileNo + '</span></td><td class="tdLabel">Contact No</td>';
    html += '<td>' + CreditDetail.Contact + '</td><td class="tdLabel" style="white-space: nowrap">Credit Date</td>';
    html += '<td style="white-space: nowrap">' + CreditDetail.OrderDate + '</td></tr>';
    html += '<tr><td>' + Company.EmailAddress + '</td><td>Fax : ' + Company.FaxNo + '</td><td class="tdLabel">Terms</td>';
    html += '<td style="font-size:13px"><b>' + CreditDetail.TermsDesc + '</b></td>';
    if (CreditDetail.shipVia) {
        html += '<td class="tdLabel">Ship Via</td><td>' + CreditDetail.shipVia + '</td>'
    } else {
        html += '<td class="tdLabel">Ship Via</td><td></td>'
    }

    html += '</tr>'
    if (CreditDetail.BusinessName != '') {
        html += '<tr><td><span class="tdLabel">Business Name:</span></td><td>' + CreditDetail.BusinessName + '</td></tr>';
    }
    html += '<tr><td><span class="tdLabel">Shipping Address</span></td><td colspan="5" style="font-size:13px">' + CreditDetail.ShipAddress + '</td></tr>';
    html += '<tr><td><span class="tdLabel">Billing Address</span></td><td colspan="5" style="font-size:13px">' + CreditDetail.BillAddress + '</td></tr>';
    html += '</tbody></table>';
    html += '<table class="table tableCSS tblProduct1" id="tblProduct"><thead><tr>';
    html += '<thead><tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
    html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
    html += '<td class="Barcode" style="width: 10px">UPC</td>';
    html += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
    html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
    var catId, catName, Qty = 0, total = 0.00, count = 1, totalPrice = 0.00, TotalQty = 0;
    var pageNo = 1, rowcount = 0;

    var CreditItemList = CreditDetail.CreditItems;
    for (var c = 0; c < CreditItemList.length; c++) {
        var Items = CreditItemList[c];
        rowcount = parseInt(rowcount) + 1;
        if (parseInt(Items.QtyShip) == 0) {
            totalPrice = 0.00;
        } else {
            totalPrice = parseFloat(Items.NetPrice);
        }
        if (count == 1 || Items.CategoryId == catId) {
            html += "<tr><td style='text-align:center'>" + Items.QtyShip + "</td><td style='text-align:center'>" + Items.UnitType + "</td><td style='text-align:center'>" + Items.ProductId + "</td><td>" + Items.ProductName.replace(/&quot;/g, "\'") + "</td>";
            if (Items.Barcode != undefined) {
                html += "<td>" + Items.Barcode + "</td>";
            } else {
                html += "<td></td>";
            }

            html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(Items.UnitPrice).toFixed(2) + "</td><td style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(totalPrice).toFixed(2) + "</td>";
            html += "</tr>";
            catId = Items.CategoryId;
            catName = Items.CategoryName;
            total += Number(totalPrice);
            Qty += Number(Items.QtyShip);
            TotalQty += Number(Items.QtyShip);
            if (pageNo == 1 && count == 10 && CreditItemList.length > 10) {
                pageNo = Number(pageNo) + 1;
                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                html += '<table class="table tableCSS tblProduct1" id="tblProduct"><thead>';
                html += "<tr><td colspan='9' style='text-align:center'>" + CreditDetail.CustomerName + ' -  ' + CreditDetail.OrderNo + " ( Page " + pageNo + " Of #########)</td></tr>";
                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                html += '<td class="Barcode" style="width: 10px">UPC</td>';
                html += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


            } else if (((count - 10) % 20) == 0) {
                pageNo = Number(pageNo) + 1;
                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                html += '<table class="table tableCSS tblProduct1" id="tblProduct"><thead>';
                html += "<tr><td colspan='9' style='text-align:center'>" + CreditDetail.CustomerName + ' -  ' + CreditDetail.OrderNo + " ( Page " + pageNo + " Of #########)</td></tr>";
                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                html += '<td class="Barcode" style="width: 10px">UPC</td>';
                html += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';

            }
        }
        else {
            html += "<tr style='background: #ccc;font-weight:700;'><td class='totalQty' style='text-align:center;'>" + Qty + "</td><td style='text-align:center;'>" + catId + "</td><td colspan='4'>" + catName + "</td><td style='text-align:right;'>" + parseFloat(total).toFixed(2) + "</td></tr>";
            html += "<tr><td style='text-align:center'>" + Items.QtyShip + "</td><td style='text-align:center'>" + Items.UnitType + "</td><td style='text-align:center'>" + Items.ProductId + "</td><td>" + Items.ProductName.replace(/&quot;/g, "\'") + "</td>";
            html += "<td>" + Items.Barcode + "</td>";
            html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(Items.UnitPrice).toFixed(2) + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(totalPrice).toFixed(2) + "</td>";
            html += "</tr>";
            Qty = 0;
            total = 0.00;

            if (pageNo == 1 && count == 10 && CrediItem.length > 10) {
                pageNo = Number(pageNo) + 1;
                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                html += '<table class="table tableCSS tblProduct2" id="tblProduct1"><thead>';
                html += "<tr><td colspan='9' style='text-align:center'>" + CreditDetail.CustomerName + ' -  ' + CreditDetail.OrderNo + " ( Page " + pageNo + " Of #########)</td></tr>";
                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


            } else if (((count - 10) % 20) == 0) {
                pageNo = Number(pageNo) + 1;
                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                html += '<table class="table tableCSS tblProduct2" id="tblProduct"><thead>';
                html += "<tr><td colspan='9' style='text-align:center'>" + CreditDetail.CustomerName + ' -  ' + CreditDetail.OrderNo + " ( Page " + pageNo + " Of #########)</td></tr>";
                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';

            }
            catId = Items.CategoryId;
            catName = Items.CategoryName;
            total += parseFloat(totalPrice).toFixed(2);
            Qty += Number(Items.QtyShip);
            TotalQty += Number(Items.QtyShip);
        }

    }
    count++;

    html += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty  style='text-align:center;''>" + Qty + "</td><td  style='text-align:center;'>" + catId + "</td><td colspan='4'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
    html += '</tbody></table>';
    html += '<table style="width: 100%;"><tbody><tr><td style="width: 33%;"></td><td style="width: 33%;"></td><td style="width: 33%;"><table class="table tableCSS">';
    html += '<tbody><tr><td class="tdLabel">Total Qty</td><td style="text-align: right;">' + TotalQty + '</td></tr>';
    html += '<tr><td class="tdLabel">Sub Total</td><td style="text-align: right;">' + parseFloat(CreditDetail.GrandTotal).toFixed(2) + '</td></tr>';
    html += '<tr><td class="tdLabel">Discount</td><td style="text-align: right;">' + parseFloat(CreditDetail.OverallDisc).toFixed(2) + '</td></tr>';
    if (parseFloat(CreditDetail.TotalTax) > 0) {
        html += '<tr><td class="tdLabel">' + CreditDetail.TaxPrintLabel + '</td><td style="text-align: right;">' + parseFloat(CreditDetail.TotalTax).toFixed(2) + '</td></tr>';
    }
    if (parseFloat(CreditDetail.MLTax) > 0) {
        html += '<tr><td class="tdLabel">ML Qty</td><td style="text-align: right;">' + parseFloat(CreditDetail.MLQty).toFixed(2) + '</td></tr>';
        html += '<tr><td class="tdLabel" style="white-space: nowrap;" >' + CreditDetail.MLTaxPrintLabel + '</td><td style="text-align: right;">' + parseFloat(CreditDetail.MLTax).toFixed(2) + '</td></tr>';
    }
    if (parseFloat(CreditDetail.AdjustmentAmt) != 0) {
        html += '<tr><td class="tdLabel">Adjustment</td><td style="text-align: right;">' + parseFloat(CreditDetail.AdjustmentAmt).toFixed(2) + '</td></tr>';
    }
    html += '<tr><td class="tdLabel">Grand Total</td><td style="text-align: right;">' + parseFloat(CreditDetail.TotalAmount).toFixed(2) + '</td></tr></tbody>';
    html += '</table>';
    html = html.replace('#########', pageNo);
    return html;
}

