﻿
var Print = 1, PrintLabel = '';
$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('OrderAutoId');
    if (getid != null) {
        //getPrintLabel(getid);
        getOrderData(getid);
    }
});

/*-------------------------------------------------------------------------------------------------------------------------------*/
//function getPrintLabel(OrderAutoId) {
//    $.ajax({
//        type: "POST",
//        async: false,
//        url: "/Manager/WebAPI/WPrintOrder.asmx/getPrintLabel",
//        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        beforeSend: function () {
//            $('#fade').show();
//        },
//        complete: function () {
//            $('#fade').hide();
//        },
//        success: function (response) {
//            if (response.d == 'Session Expired') {
//                location.href = '/';
//                return;
//            }
//            var xmldoc = $.parseXML(response.d);
//            var LabelName = $(xmldoc).find("Table");
//            PrintLabel = $(LabelName).find("PrintLabel").text();
//            //$("#Prntlebel").text(PrintLabel);
//        }
//    })
//}
//                                                     Edit Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/WebAPI/WPrintOrder.asmx/GetPackingOrderPrint",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
                return;
            }
            var xmldoc = $.parseXML(response.d);
            var Company = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");
            var items = $(xmldoc).find("Table2");;

            $("#logo").attr("src", "../Img/logo/" + $(Company).find("Logo").text())
            $("#Address").text($(Company).find("Address").text());
            $("#Phone").text($(Company).find("MobileNo").text());
            $("#FaxNo").text($(Company).find("FaxNo").text());
            $("#Website").text($(Company).find("Website").text());
            $("#TC").html($(Company).find("TermsCondition").text());
            $("#CompanyName").html($(Company).find("CompanyName").text());
            $("#EmailAddress").html($(Company).find("EmailAddress").text());
            $("#PageHeading").html($(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + ' (Page 1 of <span class="TotalPage"></span>)');
            $("#lblCustomerName").html($(order).find("CustomerName").text());

            $("#acNo").text($(order).find("CustomerId").text());
            $("#BusinessName").text($(order).find("BusinessName").text());
            if ($(order).find("BusinessName").text() != '') {
                $("#BusinessName").text($(order).find("BusinessName").text());
                $("#BusinessName").closest('tr').show();
            }
            $("#storeName").text($(order).find("ContactPersonName").text());
            $("#ContPerson").text($(order).find("Contact").text());
            $("#terms").text($(order).find("TermsDesc").text());

            $("#shipAddr").text($(order).find("ShipAddress").text());
            $("#billAddr").text($(order).find("BillAddress").text());

            $("#salesRep").text($(order).find("SalesPerson").text());
            $("#so").text($(order).find("OrderNo").text());
            $("#soDate").text($(order).find("OrderDate").text());
            $("#custType").text($(order).find("CustomerType").text());
            $("#pkdBy").text($(order).find("PackerName").text());
            $("#shipVia").text($(order).find("ShippingType").text());
            var html = '<table class="table tableCSS tblProduct" id="tblProduct"><thead><tr>';
            html += '<thead><tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
            html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product Id</td><td class="ProName" style="white-space: nowrap">Item</td>';
            html += '<td class="Barcode" style="width: 10px">UPC</td></tr></thead><tbody>';

            var catId = '', catName = '', Qty = 0, count = 1;
            var pageNo = 1, productname = '', rowcount = 0;

            $.each(items, function () {
                if ($(this).find("IsExchange").text() == 1) {
                    productname = $(this).find("ProductName").text() + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Exchange)' + "</span>";
                }
                else if ($(this).find("isFreeItem").text() == 1) {
                    productname = $(this).find("ProductName").text() + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Free)' + "</span>";
                }
                else {
                    productname = $(this).find("ProductName").text();
                }
               
                if (rowcount == 0 || $(this).find("CategoryId").text() == catId) {
                    
                    html += "<tr><td>" + $(this).find("QtyShip").text() + "</td><td>" + $(this).find("UnitType").text() + "</td><td>" + $(this).find("TotalPieces").text() + "</td><td>" + $(this).find("ProductId").text() + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                    html += "<td>" + $(this).find("Barcode").text() + "</td>";
                    html += "</tr>";
                    rowcount = parseInt(rowcount) + 1;
                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    Qty += Number($(this).find("QtyShip").text());
                    if (pageNo == 1 && rowcount >= 18) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='10' style='text-align:center;font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td>';
                        html += '</tr></thead><tbody>';
                        rowcount = 0;

                    } else if (((rowcount) % 26) == 0) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='10' style='text-align:center;font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td>';
                        html += '</tr></thead><tbody>';
                        rowcount = 0;
                    }

                } else { 
                    html += "<tr style='background: #ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='4'>" + catName + "</td></tr>";
                    rowcount = parseInt(rowcount) + 1;
                    if (pageNo == 1 && rowcount == 18) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='10' style='text-align:center;font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td>';
                        html += '</tr></thead><tbody>';
                        rowcount = 0;

                    }
                    if (rowcount == 26) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='10' style='text-align:center;font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td>';
                        html += '</tr></thead><tbody>';
                        rowcount = 0;

                    }
                  
                    html += "<tr><td>" + $(this).find("QtyShip").text() + "</td><td>" + $(this).find("UnitType").text() + "</td><td>" + $(this).find("TotalPieces").text() + "</td><td>" + $(this).find("ProductId").text() + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                    html += "<td>" + $(this).find("Barcode").text() + "</td>";
                    html += "</tr>";
                    rowcount = parseInt(rowcount) + 1;
                    Qty = 0;
                    if (pageNo == 1 && rowcount == 18) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='10' style='text-align:center;font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td></tr></thead><tbody>';
                        rowcount = 0;

                    } else if (((rowcount) % 26) == 0) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='10' style='text-align:center;font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    Qty += Number($(this).find("QtyShip").text());
                }

                count++;
            });
            html += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='4'>" + catName + "</td></tr>";

            html += '</tbody></table>';
            if ((rowcount == 18 && pageNo == 1)) {
                pageNo = Number(pageNo) + 1;
                html += "<p  style='page-break-before: always'></p><br/>";
                html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                html += "<tr><td colspan='10' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + "   (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                html += "</thead > </table>";
                rowcount = 0;
            }
            $('#tableDynamic').html(html);
            $('.TotalPage').html(pageNo);
            setTimeout(function () { window.print(); }, 200);


        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
