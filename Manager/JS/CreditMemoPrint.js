﻿
var Print = 0
$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('PrintAutoId');
    if (getid != null) {
        getOrderData(getid);
    }
});

/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                     Edit Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData(CreditAutoId) {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemo.asmx/PrintCredit",
        data: "{'CreditAutoId':'" + CreditAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            console.log(response);
            var xmldoc = $.parseXML(response.d);
            var Company = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");
            var items = $(xmldoc).find("Table2");
            $("#logo").attr("src", "../Img/logo/" + $(Company).find("Logo").text())
            $("#Address").text($(Company).find("Address").text());
            $("#Phone").text($(Company).find("MobileNo").text());
            $("#FaxNo").text($(Company).find("FaxNo").text());
            $("#Website").text($(Company).find("Website").text());
            $("#TC").html($(Company).find("TermsCondition").text());
            $("#CompanyName").html($(Company).find("CompanyName").text());
            $("#EmailAddress").html($(Company).find("EmailAddress").text());
            $('#spLocationCode').html($(Company).find("LocationCode").text());//Added for ML Tax location code

            $("#PageHeading").html($(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + ' (Page 1 of <span class="TotalPage"></span>)');

            $("#acNo").text($(order).find("CustomerId").text());
            $("#BusinessName").text($(order).find("BusinessName").text());
            if ($(order).find("BusinessName").text() != '') {
                $("#BusinessName").text($(order).find("BusinessName").text());
                $("#BusinessName").closest('tr').show();
            }
            $("#storeName").text($(order).find("ContactPersonName").text());
            $("#ContPerson").text($(order).find("Contact").text());
            $("#terms").text($(order).find("TermsDesc").text());

            $("#shipAddr").text($(order).find("ShipAddr").text());

            $("#billAddr").text($(order).find("BillAddr").text());

            $("#salesRep").text($(order).find("SalesPerson").text());
            $("#so").text($(order).find("OrderNo").text());
            $("#soDate").text($(order).find("OrderDate").text());
            $("#custType").text($(order).find("CustomerType").text());
            $("#pkdBy").text($(order).find("PackerName").text());
            var html = '<table class="table tableCSS tblProduct" id="tblProduct"><thead><tr>';
            html += '<thead><tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
            html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Product Name</td>';
          //  html += '<td class="Barcode" style="width: 10px">Barcode</td>';
            html += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
            html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


            //  $("#tableDynamic").html('');
            var row = $("#tblProduct thead tr").clone(true);
            var catId, catName, Qty = 0, total = 0.00, count = 1, tr, totalPrice = 0.00;
            var pageNo = 1, rowcount = 0, taxableProduct = '';;
            $.each(items, function () {
                rowcount = parseInt(rowcount) + 1;
                if (parseInt($(this).find("QtyShip").text()) == 0) {
                    totalPrice = 0.00;
                } else {
                    totalPrice = parseFloat($(this).find("NetPrice").text());
                }
                if (parseInt($(this).find("TaxRate").text()) == 1) {
                    taxableProduct = ' <span>(Taxable)</span>';
                }
                else {
                    taxableProduct = '';
                }
                if (count == 1 || $(this).find("CategoryId").text() == catId) {

                    html += "<tr><td style='text-align:center'>" + $(this).find("QtyShip").text() + "</td><td style='text-align:center'>" + $(this).find("UnitType").text() + "</td><td  style='text-align:center'>" + $(this).find("ProductId").text() + "</td><td>" + $(this).find("ProductName").text().replace(/&quot;/g, "\'") + taxableProduct + "</td>";
                   // html += "<td>" + $(this).find("Barcode").text() + "</td>";
                    html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td><td style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                    html += "</tr>";
                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    total += Number(totalPrice);
                    Qty += Number($(this).find("QtyShip").text());
                    if (pageNo == 1 && rowcount >= 18) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='9' style='font-size:20px;text-align:center;font-weight:400'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Product Name</td>';
                        html += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    } else if (((rowcount) % 26) == 0) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='9' style='font-size:20px;text-align:center;font-weight:400'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Product Name</td>';
                        html += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                } else {
                    html += "<tr style='background: #ccc;font-weight:700;'><td class='totalQty'  style='text-align:center'>" + Qty + "</td><td>" + catId + "</td><td colspan='3'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
                    html += "<tr><td  style='text-align:center'>" + $(this).find("QtyShip").text() + "</td><td style='text-align:center'>" + $(this).find("UnitType").text() + "</td><td style='text-align:center'>" + $(this).find("ProductId").text() + "</td><td>" + $(this).find("ProductName").text().replace(/&quot;/g, "\'") + taxableProduct+ "</td>";
                    html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                    html += "</tr>";
                    Qty = 0;
                    total = 0.00;
                    rowcount = parseInt(rowcount) + 1;
                    if (pageNo == 1 && rowcount >= 18) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='9'  style='font-size:20px;text-align:center;font-weight:400'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Product Name</td>';
                        html +='<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    } else if (((rowcount) % 26) == 0) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='9'  style='font-size:20px;text-align:center;font-weight:400'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Product Name</td>';
                        html += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    total += Number(totalPrice);
                    Qty += Number($(this).find("QtyShip").text());
                }

                count++;
            });
            html += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty' style='text-align:center'>" + Qty + "</td><td>" + catId + "</td><td colspan='3'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
            html += '</tbody></table>';
            $('#tableDynamic').html(html);
            $('.TotalPage').html(pageNo);

            var totalQtyByCat = 0;
            $('.tblProduct tbody tr').each(function () {

                totalQtyByCat += Number($(this).find(".totalQty").text());
            });
            $("#totalQty").text(totalQtyByCat);
            $("#totalwoTax").text($(order).find("GrandTotal").text());


            if (parseFloat($(order).find("OverallDisc").text()) > 0) {
                $("#disc").text($(order).find("OverallDisc").text());
            } else {
                $("#disc").closest('tr').hide();
            }

            if (parseFloat($(order).find("TotalTax").text()) > 0) {
                $("#tax").text($(order).find("TotalTax").text());
            } else {
                $("#tax").closest('tr').hide();
            } 

            if (parseFloat($(order).find("MLTax").text()) > 0) {
                $("#MLQty").text($(order).find("MLQty").text());
                $("#MLTax").text($(order).find("MLTax").text());
            } else {
                $("#MLQty").closest('tr').hide();
                $("#MLTax").closest('tr').hide();
            }
           
            if (parseFloat($(order).find("WeightTaxAmount").text()) > 0) {
                $("#WeightTax").text($(order).find("WeightTaxAmount").text());
            } else {
                $("#WeightTax").closest('tr').hide();
            }
            if (parseFloat($(order).find("AdjustmentAmt").text()) != 0) {

                $("#spAdjustment").text($(order).find("AdjustmentAmt").text());

            } else {
                $("#spAdjustment").closest('tr').hide();
            }

            $("#shipCharges").text($(order).find("ShippingCharges").text());
            $("#grandTotal").text($(order).find("TotalAmount").text());
            window.print();
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}