﻿$(document).ready(function () {
    $('.date').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true
    });
    bindDriver();
    getAssignedOrdersList(1);
});

function Pagevalue(e) {
    getAssignedOrdersList(parseInt($(e).attr("page")));
}

function bindDriver() {
    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/asgnOrderList.asmx/bindDriver",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Drivers = $(xmldoc).find("Table");

            $("#ddlSDriver option:not(:first)").remove();
            $.each(Drivers, function () {
                $("#ddlSDriver").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Driver").text() + "</option>");
            });
            $("#ddlSDriver").select2();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function getAssignedOrdersList(pageIndex) {
    var data = {
        DrvAutoId: $("#ddlSDriver").val(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        PageIndex: pageIndex
    };

    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/asgnOrderList.asmx/getAssignedOrdersList",
        data: "{'searchValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Orderlist = $(xmldoc).find("Table1");

            $("#tblAssignedOrders tbody tr").remove();
            var row = $("#tblAssignedOrders thead tr").clone(true);
            if (Orderlist.length > 0) {
                $("#EmptyTable").hide();
                $.each(Orderlist, function () {
                    $(".DrvName", row).html("<span drvautoid=" + $(this).find("Driver").text() + ">" + $(this).find("DrvName").text() + "</span>");
                    $(".AsgnOrders", row).text($(this).find("TotalOrders").text());
                    $(".AsgnDt", row).text($(this).find("AssignDate").text());
                    $(".Action", row).html("<a href='javascript:;' onclick='get_Drv_Asgn_Order(this,1)'><span class='la la-eye' title='View'></span></a>&nbsp;<a href='javascript:;' onclick='get_Drv_Asgn_Order(this,2)'><span class='la la-map-marker' title='Print Map'></span></a>");
                    $("#tblAssignedOrders tbody").append(row);
                    row = $("#tblAssignedOrders tbody tr:last").clone(true);
                });
            } else {
                $("#EmptyTable").show();
            }

            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function PrintMap() {
    var maparray = [];
    var index = 0; CheckStopNo = 0; var ord = '', AMt = 0;
    $('#tblDrvAsgn tbody tr').each(function () {
        maparray[index] = new Object();
        maparray[index].lat = Number($(this).find('.Customer span').attr('lat'));
        maparray[index].lng = Number($(this).find('.Customer span').attr('long'));
        maparray[index].stopNumber = $(this).find('.Stop').find('input').val();

        if (CheckStopNo != $(this).find('.Stop').find('input').val()) {
            ord = $(this).find('.OrderNo').text();
            AMt = Number($(this).find('.OrderVal').text());
        } else {
            ord = ord + ', ' + $(this).find('.OrderNo').text();
            AMt = Number(AMt) + Number($(this).find('.OrderVal').text());
        }
        CheckStopNo = Number($(this).find('.Stop').find('input').val());
        maparray[index].orderNo = '<b>Order No:</b> ' + ord + '<br/><b>Customer Name:</b> ' + $(this).find('.Customer span').attr('customer') + '<br/><b>Payable Amount:</b> ' + AMt.toFixed(2) + '<br/><b>Sales Person:</b> ' + $(this).find('.Sales').text();

        index = Number(index) + 1;

    })
    localStorage.setItem('GoogleMapTest', JSON.stringify(maparray));

    window.open("/GoogleMap/LocationMap.aspx", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}

function get_Drv_Asgn_Order(e, value) {
    var tr = $(e).closest("tr");
    $('#olddriver').val($(tr).find(".DrvName span").attr("drvautoid"));
    var data = {
        DrvAutoId: $(tr).find(".DrvName span").attr("drvautoid"),
        AsgnDate: $(tr).find(".AsgnDt").text(),
    };
    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/asgnOrderList.asmx/getDrvAsgnOrder",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Orderlist = $(xmldoc).find("Table");
            var root = $(xmldoc).find("Table1");

            $("#drvname").text($(tr).find(".DrvName").text()).attr("drvautoid", $(tr).find(".DrvName span").attr("drvautoid"));
            $("#asgnDt").text($(tr).find(".AsgnDt").text()).attr('asgnDt', $(tr).find(".AsgnDt").text());
            $("#txtRoot").val($(root).find("Root").text());

            $("#tblDrvAsgn tbody tr").remove();
            var row = $("#tblDrvAsgn thead tr").clone(true);
            if (Orderlist.length > 0) {
                var count = 0, isShowDrv = 0;
                $.each(Orderlist, function () {
                    $(".OrderNo", row).html("<span orderautoid=" + $(this).find("AutoId").text() + ">" + $(this).find("OrderNo").text() + "</span>");
                    $(".OrderDt", row).text($(this).find("OrderDate").text());
                    if ($(this).find("Status").text() == "4") {
                        $(".StatusType", row).html("<span class='badge mr-2 badge badge-pill Status_Ready_to_Ship'>" + $(this).find("StatusType").text() + "</span>");
                    }
                    else if ($(this).find("Status").text() == "5") {
                        isShowDrv = 1;
                        $(".StatusType", row).html("<span class='badge mr-2 badge badge-pill Status_Shipped'>" + $(this).find("StatusType").text() + "</span>");
                    }
                    else if ($(this).find("Status").text() == "6") {
                        isShowDrv = 1;
                        $(".StatusType", row).html("<span class='badge mr-2 badge badge-pill Status_Delivered'>" + $(this).find("StatusType").text() + "</span>");
                    }
                    $(".Customer", row).html('<span Lat="' + $(this).find("Lat").text() + '" Long="' + $(this).find("Long").text() + '" customer="' + $(this).find("CustomerName").text() + '">' + $(this).find("CustomerName").text() + "</span></br><span>" + $(this).find("Schedule").text() + "</span>");
                    $(".Sales", row).text($(this).find("SalesPerson").text());
                    $(".OrderVal", row).text($(this).find("GrandTotal").text());
                    if ($(this).find("Status").text() == 6) {
                        $(".Remarks", row).html("<textarea class='form-control input-sm  border-primary' disabled style='width:350px;height:60px'>" + $(this).find("DriverRemarks").text() + '</textarea>');
                    }
                    else {
                        $(".Remarks", row).html("<textarea class='form-control input-sm  border-primary' style='width:590px;height:60px'>" + $(this).find("DriverRemarks").text() + '</textarea>');

                    }
                    if ($(this).find("Status").text() == 6 || $(this).find("Status").text() == 11) {
                        $(".Stop", row).html("<input type='text' class='form-control input-sm text-center  border-primary' disabled style='width:40px;' value='" + $(this).find("Stoppage").text() + "'>");
                    } else {
                        $(".Stop", row).html("<input type='text' class='form-control input-sm text-center  border-primary' onkeypress='return isNumberKey(event)' maxlength='5' style='width:40px;' value='" + $(this).find("Stoppage").text() + "'>");
                        count = 1;
                    }
                    $("#tblDrvAsgn tbody").append(row);
                    row = $("#tblDrvAsgn tbody tr:last").clone(true);
                });
                if (isShowDrv == 0) {
                    $("#btnChangeDriver").show();
                }
                else {
                    $("#btnChangeDriver").hide();
                }
                if (count == 0) {
                    $("#btnsave").hide();
                } else {
                    $("#btnsave").show();
                }
            }
            else {
                $("#btnChangeDriver").hide();
                $("#btnsave").hide();
            }
            if (value == 1) {
                $("#modalAsgnOrders").modal("toggle");
            }
            if (value == 2) {
                PrintMap();
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function saveStoppage() {
    var order = [];
    var route = false;
    if ($("#txtRoot").val() == '') {
        $("#txtRoot").addClass('border-warning');
        route = true;
    } else {
        $("#txtRoot").removeClass('border-warning');
    }

    var data = {
        DrvAutoId: $("#drvname").attr("drvautoid"),
        AsgnDate: $("#asgnDt").text(),
        Root: $("#txtRoot").val(),
    };

    $("#tblDrvAsgn tbody tr").each(function () {
        if ($(this).find('.Stop').find('input').val() == '') {
            route = true;
            $(this).find('.Stop').find('input').addClass('border-warning');
        } else {
            $(this).find('.Stop').find('input').removeClass('border-warning');
        }
        order.push({
            'OrderAutoId': $(this).find('.OrderNo span').attr('orderautoid'),
            'Stoppage': $(this).find('.Stop').find('input').val(),
            'Remarks': $(this).find('.Remarks').find('textarea').val()
        });
    });
    if (!route) {
        $.ajax({
            type: "Post",
            url: "/Manager/WebAPI/asgnOrderList.asmx/saveStoppage",
            data: "{'dataValues':'" + JSON.stringify(data) + "','orders':'" + JSON.stringify(order) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == 'true') {
                    $("#modalAsgnOrders").modal('hide');
                    swal("", "Root assigned successfully.", "success").then(function () {
                        getAssignedOrdersList(1);
                    });
                } else {
                    swal("", response.d, "error");
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        toastr.error('All highlighted fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function print_NewOrder() {
    window.open("/Manager/ReadytoShipOrder.html?AutoId=" + $("#drvname").attr('drvautoid') + "&&AssignDate=" + $("#asgnDt").attr('asgnDt'), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}


//------------------------------------------------------------------------------------------------------

$("#btnChangeDriver").click(function () {
    BindDriverChange();
})

function BindDriverChange() {

    var data = {
        date: $("#asgnDt").text(),
        DriverAutoId: $('#olddriver').val()
    };
    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/asgnOrderList.asmx/getDriverList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var Drivers = $(xmldoc).find("Table");
                var selectDrv = $(xmldoc).find("Table1");
                $("#modalMisc").modal("toggle").find(".modal-dialog").removeClass("modal-sm").end()
                    .find(".modal-title").text("Drivers").end()
                    .find("#tblAssignDrv").show().end()
                    .find("#btnAsgn").show().end()
                    .find("#btnPrint").hide()
                    .find('#insideText').hide();
                $('#DriverText').show();
                $('#insideText').text('');
                $('#btnOk').hide();
                $("#tblDriver tbody tr").remove();
                var row = $("#tblDriver thead tr").clone(true);
                $("#txtAssignDate").attr("disabled", false);
                $.each(Drivers, function () {
                    $(".DrvName", row).html("<span drvautoid =" + $(this).find("AutoId").text() + ">" + $(this).find("Name").text() + "</span>");
                    $(".AsgnOrders", row).html($(this).find("AssignOrders").text());
                    if ($(this).find("AutoId").text() == $(selectDrv).find("Driver").text()) {
                        $(".Select", row).html("<input type='radio' name='driver' class='radio' checked='checked'>");
                    } else {
                        $(".Select", row).html("<input type='radio' name='driver' class='radio' >");
                    }
                    if ($(this).find("EmpTypeAutoId").text() == 2) {
                        $(row).css('background-color', '#ccff99');
                    }
                    else {
                        $(row).css('background-color', 'white');
                    }
                    $("#tblDriver tbody").append(row);
                    row = $("#tblDriver tbody tr:last").clone(true);
                });

                $("#modalMisc").modal("show");
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function assignDriver() {
    var drvAutoId, flag = true;
    $("#tblDriver tbody tr").each(function () {
        if ($(this).find("input[name='driver']").is(":checked")) {
            drvAutoId = $(this).find(".DrvName span").attr("drvautoid");
            flag = false;
        }
    });

    var data = {
        DriverAutoId: drvAutoId,
        AssignDate: $("#asgnDt").text(),
        OldDriverAutoId: $('#olddriver').val()
    };

    if (!flag) {
        $.ajax({
            type: "Post",
            url: "/Manager/WebAPI/asgnOrderList.asmx/AssignDriver",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            async: false,
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == 'true') {
                        swal("", "Driver assigned successfully.", "success"
                        ).then(function () {
                            $("#modalMisc").modal("hide");
                            getAssignedOrdersList(1);
                        });
                    } else {
                        swal("Error!", response.d, "error");
                    }


                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        toastr.error('No Driver selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
var ShipList = [], arrDrvAssignedOrder = [];
function driverPackagePrintOption() {
    var data = {
        DrvAutoId: $('#olddriver').val(),
        AsgnDate: $("#asgnDt").html()
    };
    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/asgnOrderList.asmx/DriverPackagePrintOption",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var ShippingList = $(xmldoc).find("Table");
            var DrvAssignedOrderList = $(xmldoc).find("Table1");
            ShipList= [];
            $.each(ShippingList, function (i, item) {
                ShipList.push({
                    'ShipAutoId': $(this).find("ShippingType").text()
                })
            });
            //arrDrvAssignedOrder
            $.each(DrvAssignedOrderList, function (i, item) {
                arrDrvAssignedOrder.push({
                    'AutoId': $(this).find("AutoId").text(),
                    'OrderNo': $(this).find("OrderNo").text(),
                    'Driver': $(this).find("Driver").text(),
                    'ShippingType': $(this).find("ShippingType").text()
                })
            });
            bindTemplateOption();
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function bindTemplateOption() {
    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/OptimoRoute.asmx/getShipDetails",
        data: "{'TableValues':'" + JSON.stringify(ShipList) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var ShippingType = $(xmldoc).find("Table");
                    var TemplateType = $(xmldoc).find("Table1");
                    TemplateT = '<select class="form-control input-sm border-primary">';
                    TemplateT += "<option value='0'>--Select Template--</option>";
                    $.each(TemplateType, function () {
                        TemplateT += "<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Template").text() + "</option>"
                    })
                    TemplateT += '</select>';
                    var addDefault = 0;
                    $("#tblDriverPrintOption tbody tr").remove();
                    var row = $("#tblDriverPrintOption thead tr:last-child").clone(true);
                    $.each(ShippingType, function (i) {
                        if (addDefault == 0) {
                            $(".Action", row).html('<input type="checkbox" checked name="Action" id="checkDriverAction" />');
                            $(".PrintOption", row).html('Driver Log');
                            $(".TemplateType", row).html('');
                            $(".NoOfCopy", row).html('');
                            addDefault = 1;
                            $("#tblDriverPrintOption tbody").append(row);
                            row = $("#tblDriverPrintOption tbody tr:last-child").clone(true);
                        }
                        $(".Action", row).html('<input type="checkbox" checked name="Action" id="checkAction" />');
                        $(".PrintOption", row).html('<span ShippingId=' + $(this).find("AutoId").text() + '>' + $(this).find("ShippingType").text() + '</span>');
                        $(".TemplateType", row).html(TemplateT);
                        $(".NoOfCopy", row).html('<input type="text" style="width: 20px !important; text-align:center;" id="txtNumberOfCopy" maxlength="1" value="2" class="form-control input-sm border-primary" />');
                        $("#tblDriverPrintOption tbody").append(row);
                        row = $("#tblDriverPrintOption tbody tr:last-child").clone(true);
                    });
                    $("#tblDriverPrintOption tbody tr").show();
                    $("#modalDriverPrintOption").modal('show');
                }
            }
        }
    })
}
function PrintDrvOrder() {
    arrOrdAutoIdList = [], arrShipAutoIdList = [];
    var OrderAutoId = [], drlogorder = '', drlogorder1 = '', drlogorder2 = '', drlogorder3 = '', drlogorder4 = '';
    OrderAutoId[0] = new Object();
    OrderAutoId[0].PrintType = 0;
    OrderAutoId[0].orders = 0;
    OrderAutoId[0].noofcopy = 1;
    OrderAutoId[1] = new Object();
    OrderAutoId[1].PrintType = 0;
    OrderAutoId[1].orders = 0;
    OrderAutoId[1].noofcopy = 1;
    OrderAutoId[2] = new Object();
    OrderAutoId[2].PrintType = 0;
    OrderAutoId[2].orders = 0;
    OrderAutoId[2].noofcopy = 1;
    OrderAutoId[3] = new Object();
    OrderAutoId[3].PrintType = 0;
    OrderAutoId[3].orders = 0;
    OrderAutoId[3].noofcopy = 1;
    OrderAutoId[4] = new Object();
    OrderAutoId[4].PrintType = 0;
    OrderAutoId[4].orders = 0;
    OrderAutoId[4].noofcopy = 1;
    var check = 0, checkVal = 0;
    $("#tblDriverPrintOption tbody tr").each(function (j, tblShip) {
        if ($(tblShip).find('.Action input').prop('checked') == false) {
            check = Number(check) + 1;
        }
        if ($(tblShip).find('.Action input').prop('checked') == true) {
            if ($(tblShip).find('.TemplateType option:selected').val() == "0") {
                checkVal = 1;
            }
            if (Number($(tblShip).find('.NoOfCopy input').val()) <= 0) {
                checkVal = 1;
            }
        }
    })
    if (checkVal == 1) {
        toastr.error('Please fill all field.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else if (Number(check) == Number($("#tblDriverPrintOption tbody tr").length)) {
        toastr.error('Please check atleast one item.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else {
        $("#tblDriverPrintOption tbody tr").each(function (j, tblShip) {
            $.each(arrDrvAssignedOrder, function (i, main) {
                if ($('#olddriver').val() == main.Driver) {
                    if ($(tblShip).find('.Action input').prop('checked') == true) {
                        if (j == 0) {
                            OrderAutoId[0].PrintType = 0;
                            drlogorder += main.AutoId + ',';
                        }
                        if (j != 0) {
                            if ($(tblShip).find('.TemplateType option:selected').val() == 1) {
                                OrderAutoId[1].PrintType = 1;
                                OrderAutoId[1].noofcopy = $(tblShip).find('.NoOfCopy input').val();
                                if ($(tblShip).find('.PrintOption span').attr('shippingid') == main.ShippingType) {
                                    drlogorder1 += main.AutoId + ","
                                }
                            }
                            if ($(tblShip).find('.TemplateType option:selected').val() == 2) {
                                OrderAutoId[2].PrintType = 2;
                                OrderAutoId[2].noofcopy = $(tblShip).find('.NoOfCopy input').val();
                                if ($(tblShip).find('.PrintOption span').attr('shippingid') == main.ShippingType) {
                                    drlogorder2 += main.AutoId + ","
                                }
                            }
                            if ($(tblShip).find('.TemplateType option:selected').val() == 3) {
                                OrderAutoId[3].PrintType = 3;
                                OrderAutoId[3].noofcopy = $(tblShip).find('.NoOfCopy input').val();
                                if ($(tblShip).find('.PrintOption span').attr('shippingid') == main.ShippingType) {
                                    drlogorder3 += main.AutoId + ","
                                }
                            }
                            if ($(tblShip).find('.TemplateType option:selected').val() == 4) {
                                OrderAutoId[4].PrintType = 4;
                                OrderAutoId[4].noofcopy = $(tblShip).find('.NoOfCopy input').val();
                                if ($(tblShip).find('.PrintOption span').attr('shippingid') == main.ShippingType) {
                                    drlogorder4 += main.AutoId + ","
                                }
                            }
                        }
                    }
                }
            });
        });
        OrderAutoId[0].orders = drlogorder; OrderAutoId[1].orders = drlogorder1; OrderAutoId[2].orders = drlogorder2; OrderAutoId[3].orders = drlogorder3; OrderAutoId[4].orders = drlogorder4;
        localStorage.setItem('DriverPackageprint', JSON.stringify(OrderAutoId));
        window.open("/Manager/DriverPackagePrint.aspx", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
}
