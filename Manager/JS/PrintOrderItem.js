﻿
var Print = 1, PrintLabel = '';
$(document).ready(function () {
    debugger;
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('OrderAutoId');
    if (getid != null) {
        getOrderData(getid);
    }

});
//Edit Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/WebAPI/WPrintOrder.asmx/PrintOrderItem",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
                return;
            }
            var xmldoc = $.parseXML(response.d);
            var orderdetails = $(xmldoc).find("Table"); 
            var items = $(xmldoc).find("Table1"); 
            var html = '<table class="table tableCSS tblProduct" id="tblProduct"><thead><tr>';
            html += '<tr><td class="ProName" style="white-space: nowrap">Item</td>';
            html += '<td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td><td class="Qty" style="width: 10px">Qty</td>';
            html += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
            html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
            var  productname = '',  total = 0.00, totalPrice = 0.00;
            var rowcount = 0;
            var orderQty = 0; var total = 0;
            $.each(items, function () {

                orderQty = $(this).find("QtyShip").text();
                if (parseInt($(this).find("QtyShip").text()) == 0) {
                    totalPrice = 0.00;
                } else {
                    totalPrice = parseFloat($(this).find("NetPrice").text());
                }

                rowcount = parseInt(rowcount) + 1;
                if ($(this).find("IsExchange").text() == 1) {
                    productname = $(this).find("ProductName").text() + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Exchange)' + "</span>";
                }
                else if ($(this).find("isFreeItem").text() == 1) {
                    productname = $(this).find("ProductName").text() + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Free)' + "</span>";
                }
                else {
                    productname = $(this).find("ProductName").text();
                }

                total += totalPrice;
                html += "<tr><td>" + productname.replace(/&quot;/g, "\'") + "</td><td>" + $(this).find("UnitType").text() + "</td><td>" + orderQty + "</td>";
                html += "<td style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td>";
                html += "<td style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                html += "</tr>";
            }
            ); 
            html += '</tbody>';
            html += '<tfoot>';
            html += "<tr><td colspan='2'></td><td colspan='2' style='text-align:center'><b>Total</b></td><td style='text-align:right'>" + parseFloat($(orderdetails).find('PayableAmount').text()).toFixed(2) + "</td></tr>"
            html += '</tfoot>';
            html +='</table > ';
            $('#tableDynamic').html(html);

            setTimeout(function () { window.print(); }, 200);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

