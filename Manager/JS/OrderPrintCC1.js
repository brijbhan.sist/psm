﻿
var Print = 0
$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('OrderAutoId');
   
    Print = getQueryString('Print');
    if (print == null) {
        Print = 0;
    }
    if (getid != null) {
        getOrderData(getid);
    }
});

/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                     Edit Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/WebAPI/WPrintOrder.asmx/getOrderData",
        data: "{'OrderAutoId':'" + OrderAutoId + "','check':'" + 2 + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Company = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");
            var items = $(xmldoc).find("Table2");
            var DuePayment = $(xmldoc).find("Table3");
            if (Print == 1) {
                var check = 0; 
                var DueAmount = 0;
                $("#tblduePayment tbody tr").remove();
                var rowdue = $("#tblduePayment thead tr").clone(true);
                if (DuePayment.length > 0) {
                    $(DuePayment).each(function (index) {                        
                        if (parseFloat($(this).find('AmtDue').text()) > 0) {                            
                            $(".SrNo", rowdue).text(Number(check) + 1);
                            $(".OrderNo", rowdue).text($(this).find('OrderNo').text());
                            $(".OrderDate", rowdue).text($(this).find('OrderDate').text());
                            $(".AmtDue", rowdue).html($(this).find('AmtDue').text());
                            DueAmount += parseFloat($(this).find('AmtDue').text());
                            $('#tblduePayment tbody').append(rowdue);
                            rowdue = $("#tblduePayment tbody tr:last").clone(true);
                            check = check + 1;
                        }
                    })

                }
                $("#tblduePayment tfoot tr").find("td:eq(1)").text(DueAmount.toFixed(2));
                if (parseFloat(DueAmount) > 0) {
                    $(".DueAmount").show();
                } else {
                    $(".DueAmount").hide();
                }
            } else {
                $(".DueAmount").hide();
            }

            var LoginEmpType = $(xmldoc).find("Table5").find("LoginEmpType").text();

            $("#logo").attr("src", "../Img/logo/" + $(Company).find("Logo").text())
            $("#Address").text($(Company).find("Address").text());
            $("#Phone").text($(Company).find("MobileNo").text());
            $("#FaxNo").text($(Company).find("FaxNo").text());
            $("#Website").text($(Company).find("Website").text());
            $("#TC").html($(Company).find("TermsCondition").text());
            
            if (LoginEmpType == '6' || LoginEmpType == '10') {
                $("#PageHeading").text('Invoice');
            } else {
                $("#PageHeading").text('Order Customer Copy');
            }

            $("#acNo").text($(order).find("CustomerId").text());
            $("#MLQty").text($(order).find("MLQty").text());
            $("#MLTax").text($(order).find("MLTax").text());
            $("#storeName").text($(order).find("CustomerName").text());
            $("#ContPerson").text($(order).find("Contact").text());
            $("#terms").text($(order).find("TermsDesc").text());

            $("#shipAddr").text($(order).find("ShipAddr").text() + ", " +
                                $(order).find("State2").text() + ", " +
                                $(order).find("City2").text() + ", " +
                                $(order).find("Zipcode2").text());

            $("#billAddr").text($(order).find("BillAddr").text() + ", " +
                                $(order).find("State1").text() + ", " +
                                $(order).find("City1").text() + ", " +
                                $(order).find("Zipcode1").text());

            $("#salesRep").text($(order).find("SalesPerson").text());
            $("#so").text($(order).find("OrderNo").text());
            $("#soDate").text($(order).find("OrderDate").text());
            $("#custType").text($(order).find("CustomerType").text());
            $("#pkdBy").text($(order).find("PackerName").text());
            $("#shipVia").text($(order).find("ShippingType").text());
    
            var row = $("#tblProduct thead tr").clone(true);
            var catId, catName, Qty = 0, total = 0.00, count = 1, tr;
            var orderqty = 0;
            $.each(items, function () {
                if ($(this).find("QtyShip").text() != '')
                {
                    orderqty = $(this).find("QtyShip").text();
                } else {
                    orderqty = $(this).find("RequiredQty").text();
                }
                if (count == 1 || $(this).find("CategoryId").text() == catId) {
                    $(".ProId", row).text($(this).find("ProductId").text());
                    $(".ProName", row).text($(this).find("ProductName").text().replace(/&quot;/g, "\'"));
                    $(".Qty", row).text(orderqty);
                    $(".totalPrice", row).text($(this).find("NetPrice").text());
                    $(".UnitPrice", row).text($(this).find("UnitPrice").text());
                    $(".SRP", row).text($(this).find("SRP").text());
                    $(".GP", row).text($(this).find("PrintGP").text());
                    $(".Tax", row).text($(this).find("TaxValue").text());
                    $(".Barcode", row).text($(this).find("Barcode").text());
                    $('#tblProduct tbody').append(row);
                    row = $("#tblProduct tbody tr:last").clone(true);

                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    total += Number($(this).find("NetPrice").text());
                    Qty += Number(orderqty);

                } else {
                    tr = "<tr style='background: #ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='6'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
                    $('#tblProduct tbody').append(tr);
                    Qty = 0;
                    total = 0.00;
                    $(".ProId", row).text($(this).find("ProductId").text());
                    $(".ProName", row).text($(this).find("ProductName").text().replace(/&quot;/g, "\'"));
                    $(".Qty", row).text(orderqty);
                    $(".totalPrice", row).text($(this).find("NetPrice").text());
                    $(".UnitPrice", row).text($(this).find("UnitPrice").text());
                    $(".SRP", row).text($(this).find("SRP").text());
                    $(".GP", row).text($(this).find("PrintGP").text());
                    $(".Tax", row).text($(this).find("Tax").text());
                    $(".Barcode", row).text($(this).find("Barcode").text());
                    $('#tblProduct tbody').append(row);
                    row = $("#tblProduct tbody tr:last").clone(true);

                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    total += Number($(this).find("NetPrice").text());
                    Qty += Number(orderqty);
                }
                count++;
                orderqty = 0;
            });
            $('#tblProduct tbody').append("<tr style='background:#ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='6'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>");

            var totalQtyByCat = 0;
            $('#tblProduct tbody tr').each(function () {
                totalQtyByCat += Number($(this).find(".totalQty").text());
            });
            $("#totalQty").text(totalQtyByCat);
            $("#totalwoTax").text($(order).find("TotalAmount").text());
            if (parseFloat($(order).find("OverallDisc").text()) > 0) {
                $("#disc").text('-' + $(order).find("OverallDisc").text());
            } else {
                $("#disc").closest('tr').hide();
            }
          //  $("#disc").text($(order).find("OverallDisc").text());
            $("#tax").text($(order).find("TotalTax").text());
            $("#shipCharges").text($(order).find("ShippingCharges").text());
            $("#grandTotal").text($(order).find("GrandTotal").text());
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}