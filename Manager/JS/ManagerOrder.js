﻿var getid = "", WeightTax = 0.00;
var PackerSecurityEnable = false;

$(document).ready(function () {

    $('#bodycaption').addClass('menu-collapsed');
    $('#txtDeliveryDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: true,
        selectMonths: true,
        min: new Date(),
        firstDay: 0
    });

    bindZipCode();
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    getid = getQueryString('OrderNo');
    if (getid != null) {
        bindDropdown();
        editOrder(getid);
        bindAddress();

        if (Number($("#hiddenEmpTypeVal").val()) != 5) {
            $("#btnBackOrder").show();
        }
    }
    else {
        var DraftAutoId = getQueryString('DraftAutoId');
        $("#DraftAutoId").val(DraftAutoId)
        if (DraftAutoId != null) {
            DraftOrder(DraftAutoId)
            $("#btnCancel").show();
            $("#btnReset").hide();
        }
        else {
            bindDropdown();
            $('#txtOrderDate').val((new Date()).format("MM/dd/yyyy"));
            $("#emptyTable").show();
        }
    }
    $("#ddlProduct").select2().on("select2:select", function (e) {

    });
});
var MLTaxRate = 0.00, MLTaxType = 0;
function closePop() {
    if (confirm('Are you sure you want to close')) {
        $('#PopBarCodeforPickBox').modal('hide');
    }
}


function LoadProducts() {
    if ($("#LoadProducts").attr('class') == "ft-plus") {
        var length = $('#ddlProduct > option').length;
        if (length == 1) {
            bindProductDropdown();
        }
    }
};



function bindProductDropdown() {
    $.ajax({
        type: "POST",
        url: "/Manager/ManagerOrder.aspx/bindProductDropdown",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var getData = $.parseJSON(response.d);
                $("#ddlProduct option:not(:first)").remove();
                $.each(getData[0].Product, function (index, item) {
                    $("#ddlProduct").append("<option MLQty='" + item.ML + "' WeightOz='" + item.Oz + "' value='" + item.A + "'>" + item.PN + "</option>");
                });
                $("#ddlProduct").select2();
                $("#panelBill5").show();
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function bindDropdown() {
    $.ajax({
        type: "POST",
        url: "/Manager/ManagerOrder.aspx/bindAllDropdown",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var DropDown = $.parseJSON(response.d);
                for (var i = 0; i < DropDown.length; i++) {
                    var AllDropDownList = DropDown[i];

                    var CustomerList = AllDropDownList.Customer1;
                    if (AllDropDownList.Customer2.length > 0) {
                        CustomerList = AllDropDownList.Customer2;
                    }

                    var Status = AllDropDownList.Status;
                    for (var p = 0; p < Status.length; p++) {
                        var StatusList = Status[p];
                        $('#txtOrderStatus').val(StatusList.StatusType);
                    }

                    var ddlShippingType = $("#ddlShippingType");
                    $("#ddlShippingType option:not(:first)").remove();
                    var ShippingTypeList = AllDropDownList.ShippingType;
                    for (var k = 0; k < ShippingTypeList.length; k++) {
                        var Shipping = ShippingTypeList[k];
                        var option = $("<option />");
                        option.html(Shipping.ShippingType);
                        option.val(Shipping.AutoId);
                        option.attr('taxenabled', Shipping.EnabledTax);
                        ddlShippingType.append(option);
                    }
                    $("#ddlShippingType").val('1');

                }
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function bindZipCode() {
    $.ajax({
        type: "POST",
        url: "/Manager/ManagerOrder.aspx/bindZipcodeDropDowns",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var DropDownList = $.parseJSON(response.d);
                var ddlZipCode = $("#ddlZipCode");
                $("#ddlZipCode option:not(:first)").remove();
                var ZipCode = DropDownList[0].ZipCode1;
                for (var n = 0; n < ZipCode.length; n++) {
                    var ZipCode1DropList = ZipCode[n];
                    var option = $("<option />");
                    option.html(ZipCode1DropList.ZipCode);
                    option.val(ZipCode1DropList.AutoId);
                    option.attr('ZM', ZipCode1DropList.ZM);
                    option.attr('CityId', ZipCode1DropList.CityId);
                    $("#ddlZipCode").append(option);
                }
                ddlZipCode.select2();
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function getCityState(No, e) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Manager/ManagerOrder.aspx/BindPinCodeForCity",
        dataType: "json",
        data: "{'ZipAutoId':'" + $(e).val() + "'}",
        success: function (response) {
            var xmldoc = $.parseXML(response.d);

            var CityDetails = $(xmldoc).find('Table');
            $.each(CityDetails, function () {
                $('#txtState' + No).val($(this).find('StateName').text());
                $("#txtCity" + No).val($(this).find("CityName").text());
            });

        }
    });
}
function BindProduct() {

    $.ajax({
        type: "POST",
        url: "/Manager/ManagerOrder.aspx/BindProduct",
        data: "{'FreeItem':'" + $('#chkFreeItem').prop('checked') + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var product = $(xmldoc).find("Table");

                $("#ddlProduct option:not(:first)").remove();
                $.each(product, function () {
                    $("#ddlProduct").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ProductName").text() + "</option>");
                });
                $("#ddlProduct").select2();
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function closepaymentmodal() {
    $("#CustomerDueAmount").modal('hide');

    if ($("#shippingtype").val() == '11') {
        $("#ModalSalesQuote").modal('show');
    }
}
function closeSalesQuoteModa() {
    $("#ModalSalesQuote").modal('hide');
}
/*-------------------------------- Function to Bind Customer Address and previous due payments ----------------------------------------*/
function bindAddress() {
    var customerAutoId = $("#ddlCustomer").val();
    if (customerAutoId != "0") {
        $("#txtBarcode").removeAttr('disabled', false);
    } else {
        $("#txtBarcode").removeAttr('disabled');
    }
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/ManagerOrder.aspx/selectAddress",
        data: "{'customerAutoId':" + customerAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var BillAddress = $(xmldoc).find("Table");
                var ShipAddress = $(xmldoc).find("Table1");
                var duePayment = $(xmldoc).find("Table2");
                var Terms = $(xmldoc).find("Table3");
                var WeightTaxValue = $(xmldoc).find("Table6");

                if ($(WeightTaxValue).find("Value").text() != '') {
                    WeightTax = $(WeightTaxValue).find("Value").text();
                }
                var check = 0;
                if (duePayment.length) {
                    $('#tblduePayment tbody tr').remove();
                    var row = $("#tblduePayment thead tr").clone(true);
                    $(duePayment).each(function (index) {
                        if (parseFloat($(this).find('AmtDue').text()) > 0) {

                            $(".SrNo", row).text(Number(check) + 1);
                            $(".OrderNo", row).text($(this).find('OrderNo').text());
                            $(".OrderDate", row).text($(this).find('OrderDate').text());
                            $(".AmtDue", row).html($(this).find('AmtDue').text());
                            $('#tblduePayment tbody').append(row);
                            row = $("#tblduePayment tbody tr:last").clone(true);
                            check = check + 1;
                        }
                    })
                    if (duePayment.length > 0) {
                        $("#CustomerDueAmount").modal('show');
                    } else {
                        if ($("#shippingtype").val() == '11') {
                            $("#ModalSalesQuote").modal('show');
                        }
                    }
                }

                $("#hiddenBillAddrAutoId").val($(BillAddress).find("BillAddrAutoId").text());
                $("#txtBillAddress").val($(BillAddress).find("Address").text());
                $("#txtBillState").val($(BillAddress).find("State").text());
                $("#txtBillCity").val($(BillAddress).find("City").text());
                $("#txtBillZip").val($(BillAddress).find("Zipcode").text());

                $("#hiddenShipAddrAutoId").val($(ShipAddress).find("ShipAddrAutoId").text());
                $("#txtShipAddress").val($(ShipAddress).find("Address").text());
                $("#txtShipState").val($(ShipAddress).find("State").text());
                $("#txtShipCity").val($(ShipAddress).find("City").text());
                $("#txtShipZip").val($(ShipAddress).find("Zipcode").text());
                $("#txtTerms").val(Terms.find("TermsDesc").text());

                if ($("#hiddenEmpTypeVal").val() == "2" || $("#hiddenEmpTypeVal").val() == "1") {

                    showDuePayments(duePayment);

                }

                if ($("#hiddenEmpTypeVal").val() == "2" || $("#hiddenEmpTypeVal").val() == "1") {

                    ShowTop25SellingProducts();
                }

                var TaxTypeMaster = $(xmldoc).find("Table4");
                console.log(TaxTypeMaster);
                $("#ddlTaxType option").remove();
                $.each(TaxTypeMaster, function () {
                    $("#ddlTaxType").append("<option TaxValue='" + $(this).find("Value").text() + "' value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TaxableType").text() + "-(" + $(this).find("Value").text() + ")</option>");
                });
                $("#ddlTaxType").removeAttr('disabled', true);

                var MLTax = $(xmldoc).find("Table5");
                MLTaxRate = parseFloat($(MLTax).find('TaxRate').text()).toFixed(2);
                $("#hfMLTax").val(MLTaxRate);
                MLTaxType = $(MLTax).find('MLTaxType').text();
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function showDuePayments(duePayment) {

    $("#tblCustDues tbody tr").remove();
    var row = $("#tblCustDues thead tr").clone(true);

    var sumOrderValue = 0, sumPaid = 0, sumDue = 0;

    if (duePayment.length > 0) {
        $.each(duePayment, function () {
            $("#noDues").hide();
            $(".orderNo", row).html("<span orderautoid='" + $(this).find("AutoId").text() + "'><a href='/Sales/orderMaster.aspx?OrderNo=" + $(this).find("OrderNo").text() + "'>" + $(this).find("OrderNo").text() + "</a></span>");
            $(".orderDate", row).text($(this).find("OrderDate").text());
            $(".value", row).text($(this).find("GrandTotal").text());
            $(".amtPaid", row).text($(this).find("AmtPaid").text()).css("color", "#8bc548");
            $(".amtDue", row).text($(this).find("AmtDue").text()).css("color", "red");
            $(".pay", row).html("<input type='text' class='form-control input-sm' style='width:70px;float:right;text-align:right;' value='0.00' placeholder='0.00' onkeyup='sumPay()' />");
            $(".remarks", row).html("<input type='text' class='form-control input-sm' placeholder='Enter payment details here' />");

            sumOrderValue += Number($(this).find("GrandTotal").text());
            sumPaid += Number($(this).find("AmtPaid").text());
            sumDue += Number($(this).find("AmtDue").text());

            $('#tblCustDues').find("tbody").append(row).end()
                .find("tfoot").show().find("#sumOrderValue").text(sumOrderValue.toFixed(2)).end()
                .find("#sumPaid").text(sumPaid.toFixed(2)).css("color", "#8bc548").end()
                .find("#sumDue").text(sumDue.toFixed(2)).css("color", "red").end()
                .find("#sumPay").text("0.00").end();

            row = $("#tblCustDues tbody tr:last").clone(true);

            if ($("#hiddenEmpTypeVal").val() == 2 && getid == null) {
                $("#CustDues").show();
                $("#btnPay_Dues, #btnClose_Dues").show();
            }

        });
    } else {
        $("#CustDues").hide();
        // $("#btnPay_Dues, #btnClose_Dues").hide();
    }
}

/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                            Summation of Pay Amount against Order Dues
/*-------------------------------------------------------------------------------------------------------------------------------*/
function sumPay() {
    var sumPay = 0.00;
    $("#tblCustDues tbody tr").each(function () {
        sumPay += Number($(this).find(".pay input").val());
    });
    $("#tblCustDues tfoot tr").find("#sumPay").text(sumPay.toFixed(2));
}

/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                           Payment Submission of Dues Orders

function BindUnittype() {
    $("#alertStockQty").hide();
    var productAutoId = $("#ddlProduct option:selected").val();
    if (productAutoId == '0') {
        $("#txtReqQty").val(1);
    }
    $.ajax({
        type: "POST",
        url: "/Manager/ManagerOrder.aspx/bindUnitType",
        data: "{'productAutoId':" + productAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var unitDefault = $(xmldoc).find("Table1");
                var count = 0;

                $("#ddlUnitType option:not(:first)").remove();
                $.each(unitType, function () {
                    $("#ddlUnitType").append("<option value='" + $(this).find("AutoId").text() + "' EligibleforFree='" + $(this).find("EligibleforFree").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                });
                if (unitDefault.length > 0) {
                    if (BUnitAutoId == 0) {
                        $("#ddlUnitType").val($(unitDefault).find('AutoId').text()).change();
                    } else {
                        $("#ddlUnitType").val(BUnitAutoId).change();
                        BUnitAutoId = 0;
                    }
                } else {
                    $("#ddlUnitType").val(0);
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
};

var price, taxRate, SRP, minPrice, CustomPrice, GP, BasePrice; // ---------- Global Variables ---------------

function BindunitDetails() {
    //$("#alertStockQty").hide();
    if ($("#ddlUnitType option:selected").attr('eligibleforfree') == 1) {
        $('#chkFreeItem').attr('disabled', false);
        $('#freeItem').show();
    } else {
        $('#chkFreeItem').attr('disabled', true);
        $('#freeItem').hide();

    }
    $('#chkFreeItem').prop('checked', false);
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        UnitAutoId: $("#ddlUnitType").val(),
        CustAutoId: $("#ddlCustomer").val()
    };

    if (data.UnitAutoId != 0) {
        $.ajax({
            type: "POST",
            url: "/Manager/ManagerOrder.aspx/selectQtyPrice",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var stockAndPrice = $(xmldoc).find("Table");
                    price = parseFloat(stockAndPrice.find("Price").text()).toFixed(2);
                    minPrice = stockAndPrice.find("MinPrice").text();
                    costPrice = stockAndPrice.find("CostPrice").text();
                    CustomPrice = stockAndPrice.find("CustomPrice").text();
                    taxRate = stockAndPrice.find("TaxRate").text();
                    BasePrice = stockAndPrice.find("BasePrice").text();
                    SRP = stockAndPrice.find("SRP").text();
                    GP = stockAndPrice.find("GP").text();
                    if ($("#ddlUnitType").val() != 0) {
                        $("#alertStockQty").text('');
                        if (Number($("#ddlUnitType").val()) == 1) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " case ] ," +
                                " [ Base Price : $" + price + " ]");
                        } else if (Number($("#ddlUnitType").val()) == 2) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " box ] ," +
                                " [ Base Price : $" + price + " ]");
                        } else {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " pcs ] ," +
                                " [ Base Price : $" + price + " ]");
                        }
                        $("#alertStockQty").show();
                    } else {
                        $("#alertStockQty").hide();
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
};

$('#chkIsTaxable').click(function () {
    $('#chkExchange').prop('checked', false);
    $('#chkFreeItem').prop('checked', false);
})
$('#chkExchange').click(function () {
    $('#chkIsTaxable').prop('checked', false);
    $('#chkFreeItem').prop('checked', false);
})
$('#chkFreeItem').click(function () {
    $('#chkIsTaxable').prop('checked', false);
    $('#chkExchange').prop('checked', false);
})
/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                          Add Row to table
/*-------------------------------------------------------------------------------------------------------------------------------*/
$("#btnAdd").click(function () {

    var flag1 = false;
    var validatecheck = dynamicInputTypeSelect2('selectvalidate');
    if (!validatecheck) {
        checkRequiredField();
    }
    //else {
    //    validatecheck = checkRequiredField();
    //}
    if (validatecheck) {
        var productAutoId = $("#ddlProduct option:selected").val();
        var unitAutoId = $("#ddlUnitType option:selected").val();
        var MLQty = $("#ddlProduct option:selected").attr('MLQty');
        var WeightOz = $("#ddlProduct option:selected").attr('WeightOz');
        if (CustomPrice == "") {
            UnitPrice = parseFloat(price).toFixed(2);
        } else {
            if (minPrice > CustomPrice) {
                UnitPrice = parseFloat(minPrice).toFixed(2);
            } else {
                UnitPrice = parseFloat(CustomPrice).toFixed(2);
            }
        }
        var chkIsTaxable = $('#chkIsTaxable').prop('checked');
        var chkExchange = $('#chkExchange').prop('checked');
        var chkFreeItem = $('#chkFreeItem').prop('checked');
        var IsExchange = 0, IsTaxable = 0, IsFreeItem = 0, chkcount = 0;
        if (chkIsTaxable == true) {
            IsTaxable = 1;
            chkcount = 1;
        }
        if (chkExchange == true) {
            IsExchange = 1;
            chkcount = chkcount + 1;
            MLQty = 0.00;
            WeightOz = 0.00;
        }
        if (chkFreeItem == true) {
            IsFreeItem = 1;
            chkcount = chkcount + 1;
            MLQty = 0.00;
            WeightOz = 0.00;
        }
        if (MLTaxType == 0) {
            MLQty = 0.00;
        }
        if (chkcount > 1) {
            toastr.error('Please check only one checkbox.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

            return;
        }
        if ($("#txtReqQty").val() == 0) {
            toastr.error('Required quantity can not be Zero.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

            return;
        }
        //Start
        if (UnitPrice == "undefined" || UnitPrice == "isNaN") {
            toastr.error('Invalid Product.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        if (minPrice == "undefined" || minPrice == "isNaN") {
            toastr.error('Invalid Product.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        //End 04-09-2019 By Rizwan Ahmad

        $("#emptyTable").hide();
        $("#alertStockQty").hide();

        var tr = $("#tblProductDetail tbody tr");
        for (var i = 0; i <= tr.length - 1; i++) {
            if ($(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem && $(tr[i]).find(".ProName > span").attr("productautoid") == productAutoId &&
                $(tr[i]).find(".UnitType > span").attr("unitautoid") == unitAutoId && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange) {
                var reqQty = Number($(tr[i]).find(".ReqQty input").val()) + Number($("#txtReqQty").val());
                $(tr[i]).find(".ReqQty input").val(reqQty);
                flag1 = true;

                rowCal($(tr[i]).find(".ReqQty > input"));
                $('#tblProductDetail tbody tr:first').before($(tr[i]));
                if (chkIsTaxable == true && ($(tr[i]).find(".TaxRate > span").attr("TypeTax")) == "0") {
                    toastr.success('Product has been added successfully but product has been not set taxable, now product cannot be set taxable .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
                else if (chkIsTaxable == false && ($(tr[i]).find(".TaxRate > span").attr("TypeTax")) == "1") {
                    toastr.success('Product has been added succesfully but product has already taxable,so now product has been added with taxable .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    return;
                }
                else {
                    toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    break;
                }
            }

            if (chkFreeItem) {
                if (($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == '0') && $(tr[i]).find(".UnitType > span").attr("unitautoid") != unitAutoId) {
                    flag1 = true;
                    toastr.error("You can't add different unit of added product.", 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#ddlProduct").select2('val', '0');
                    $("#ddlUnitType").val(0);
                    $("#txtReqQty").val("1");
                    return;

                }
            }
            if (chkExchange) {
                if ($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId && $(tr[i]).find(".UnitType > span").attr("unitautoid") != unitAutoId && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == '0' && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange) {
                    flag1 = true;
                    toastr.error("You can't add different unit of added product.", 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    $("#ddlProduct").select2('val', '0');
                    $("#ddlUnitType").val(0);
                    $("#txtReqQty").val("1");
                    return;
                }
            }

            if ($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange
                && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem) {
                flag1 = true;
                toastr.error("You can't add different unit of added product.", 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $("#ddlProduct").select2('val', '0');
                $("#ddlUnitType").val(0);
                $("#txtReqQty").val("1");
                return;
            }
        }

        if (!flag1) {
            var product = $("#ddlProduct option:selected").text().split("--");
            var row = $("#tblProductDetail thead tr").clone(true);
            $(".ProId", row).html("<span IsFreeItem='" + IsFreeItem + "'></span>" + product[0]);
            if (IsFreeItem == 1) {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");

            } else if (IsTaxable == 1) {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");

            } else if (IsExchange == 1) {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");

            } else {
                $(".ProName", row).html("<span ProductAutoId='" + $("#ddlProduct option:selected").val() + "'>" + product[1] + "</span>");
            }
            $(".UnitType", row).html("<span UnitAutoId='" + $("#ddlUnitType option:selected").val() + "' QtyPerUnit='" + $("#ddlUnitType option:selected").attr("QtyPerUnit") + "'>" + $("#ddlUnitType option:selected").text() + "</span>");
            if ($("#hiddenOrderStatus").val() == "3") {
                $(".ReqQty", row).html("<input type='text' maxlength='4' onkeypress='return isNumberKey(event)' onchange='calPieces(this)' class='form-control input-sm text-center' border-primary runat='server' value='" + $("#txtReqQty").val() + "'/>");
            } else {
                $(".ReqQty", row).html("<input type='text' maxlength='4' onkeypress='return isNumberKey(event)' class='form-control input-sm border-primary text-center' runat='server' onkeyup='rowCal(this)' value='" + $("#txtReqQty").val() + "'/>");
            }
            $(".Barcode", row).text("");
            $(".AutoId", row).text("");
            $(".QtyShip", row).html("<input type='text' class='form-control input-sm text-center' onkeypress='return isNumberKey(event)' style='width:70px;' disabled value='0' />");
            $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * parseInt($("#ddlUnitType option:selected").attr("QtyPerUnit")));
            if ($('#chkExchange').prop('checked') == false && $('#chkFreeItem').prop('checked') == false) {

                if (CustomPrice == "") {
                    $(".UnitPrice", row).html("<input type='text' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='" + parseFloat(price).toFixed(2) + "' minprice='" + minPrice + "' BasePrice='" + price + "' />");
                } else {
                    $(".UnitPrice", row).html("<input type='text' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='" + parseFloat(parseFloat(minPrice) > parseFloat(CustomPrice) ? price : CustomPrice).toFixed(2) + "' minprice='" + minPrice + "' BasePrice='" + price + "' />");
                }
            } else {
                $(".UnitPrice", row).html("<input type='text' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='0.00' minprice='0.00' BasePrice='0.00' disabled/>");
            }
            $(".Discount", row).html("<input type='text' class='form-control input-sm border-primary' onkeypress='return isNumberDecimalKey(event,this)' style='text-align:right;width:70px;' maxlength='6' value='0.00' onkeyup='checkdiscount(this)'/>");

            $(".ItemType", row).text('New');
            $(".SRP", row).text(SRP);
            $(".GP", row).text(GP);
            var taxType = '', TypeTax = 0;
            if ($('#chkIsTaxable').prop('checked') == true) {
                taxType = '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>';
                TypeTax = 1;
            }
            var IsExchange1 = '';
            if ($('#chkExchange').prop('checked') == true) {
                IsExchange1 = 'Exchange'
            }

            $(".TaxRate", row).html("<span WeightOz='" + WeightOz + "' MLQty='" + MLQty + "' TypeTax='" + TypeTax + "'> </span>" + taxType);
            if (IsExchange == 1) {
                $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>" + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
            } else {
                $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>");
            }
            $(".NetPrice", row).text("0.00");
            $(".ItemTotal", row).text("0.00");
            $(".QtyRemain", row).text("0");
            $(".OM_MinPrice", row).text(minPrice);
            $(".OM_CostPrice", row).text(costPrice);
            $(".OM_BasePrice", row).text(BasePrice);
            $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-x'></span></a>");
            if ($('#tblProductDetail tbody tr').length > 0) {
                $('#tblProductDetail tbody tr:first').before(row);
            }
            else {
                $('#tblProductDetail tbody').append(row);
            }
            rowCal(row.find(".ReqQty > input"));
            toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

        }

        $('#chkIsTaxable').prop('checked', false);
        $('#chkExchange').prop('checked', false);
        $('#chkFreeItem').prop('checked', false);
        $("#txtBarcode").val('');
        $("#txtBarcode").focus();
        $("#ddlProduct").select2('val', '0');
        $("#ddlUnitType").val(0);
        $("#txtReqQty").val("1");
    }
    else {
        toastr.error('All * fields are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
});
/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                          Calculations
/*-------------------------------------------------------------------------------------------------------------------------------*/
function rowCal(e) {

    var row = $(e).closest("tr");
    var totalPcs = (Number($(e).val()) * Number(row.find(".UnitType span").attr("QtyPerUnit")));
    row.find(".TtlPcs").text(totalPcs);
    row.find(".QtyRemain").text(Number(row.find(".ReqQty input").val()) - Number($(e).val()));
    price = Number(row.find(".UnitPrice input").val());
    Discount = Number(row.find(".Discount input").val());
    var netPrice = (price * Number(row.find(".QtyShip input").val()) - ((price * Number(row.find(".QtyShip input").val()) * Discount) / 100));
    row.find(".NetPrice").text(netPrice.toFixed(2));
    calTotalAmount();
}
function calPieces(e) {
    var row = $(e).closest("tr");
    var totalPcs = (Number($(e).val()) * Number(row.find(".UnitType span").attr("QtyPerUnit")));
    row.find(".TtlPcs").text(totalPcs);
}
function checkdiscount(e) {
    var row = $(e).closest("tr");
    var Discount = Number(row.find(".Discount input").val());
    if (Discount > 100) {
        toastr.error('The discount amount cannot be greater than 100%', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $(row).find('.Discount input').addClass('border-warning');
        $(row).find('.Discount input').val('0.00')
        rowCal(row.find(".ReqQty > input"));
        return false
    }
    else {
        var minprice = $(row).find('.UnitPrice input').attr('minprice');
        disc = parseFloat($(row).find('.Discount input').val()).toFixed(2);
        if (disc > 0) {
            var unitprice = parseFloat($(row).find('.UnitPrice input').val()) || 0;
            var discountamount = parseFloat(unitprice - unitprice * disc / 100);
            if (discountamount < minprice) {
                toastr.error("Unit Price  can't be less than Min price.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                $(row).find('.Discount input').val('0.00');
            }
            $(row).find('.Discount input').removeClass('border-warning');

        }
        rowCal(row.find(".ReqQty > input"));
    }
}
function changePrice(e) {
    var row = $(e).closest("tr");
    rowCal(row.find(".ReqQty > input"));
}

function calTotalAmount() {
    var total = 0.00;


    $("#tblProductDetail tbody tr").each(function () {
        total += Number($(this).find(".NetPrice").text());
    });


    $("#txtTotalAmount").val(total.toFixed(2));
    discountataddItem();
}
function discountataddItem() {

    var factor = 0.00, discountamount = 0.00, totalamt = 0.00;
    if ($("#txtDiscAmt").val() == "") {
        $("#txtDiscAmt").val('0.00');
        $("#txtOverallDisc").val('0.00');
    }
    else if (parseFloat($("#txtDiscAmt").val()) > (parseFloat($("#txtTotalAmount").val()))) {
        $("#txtDiscAmt").val('0.00');
        $("#txtOverallDisc").val('0.00');
    }
    discountamount = Number($("#txtDiscAmt").val()) || 0.00;
    totalamt = Number($("#txtTotalAmount").val()) || 0.00;
    if (totalamt > 0) {
        factor = parseFloat((parseFloat(discountamount) / parseFloat(totalamt)) * 100).toFixed(2) || 0.00;
    }
    $("#txtOverallDisc").val(parseFloat(factor).toFixed(2));
    calTotalTax();
}
function calTotalTax() {

    var totalTax = 0.00, qty;
    var MLQty = 0.00, WeightOzQty = 0;
    $("#tblProductDetail tbody tr").each(function () {

        qty = Number($(this).find(".QtyShip input").val());
        var IsExchan = Number($(this).find(".IsExchange span").attr('isexchange'));
        if (IsExchan == 0) {
            MLQty = parseFloat(MLQty) + (parseFloat($(this).find('.TaxRate span').attr('MLQty')) * qty * parseFloat($(this).find('.UnitType span').attr('qtyperunit')));

            WeightOzQty += (parseFloat($(this).find('.TaxRate span').attr('WeightOz')) * qty * parseFloat($(this).find('.UnitType span').attr('qtyperunit')));
            if ($(this).find('.TaxRate span').attr('typetax') == 1) {
                var totalPrice = Number($(this).find(".NetPrice").text());
                var Disc = totalPrice * Number($("#txtOverallDisc").val()) * 0.01;

                var priceAfterDisc = totalPrice - Disc;
                if ($('#ddlTaxType option:selected').attr("taxvalue") != undefined) {
                    totalTax += priceAfterDisc * Number($('#ddlTaxType option:selected').attr("taxvalue")) * 0.01;
                }
            }
        }
    });
    var taxenabledval = $("#ddlShippingType option:selected").attr('taxenabled');
    if (taxenabledval == 1) {
        $("#txtMLQty").val(MLQty.toFixed(2));
        $("#txtMLTax").val((MLQty * MLTaxRate).toFixed(2));
        $("#txtWeightQuantity").val(WeightOzQty.toFixed(2));
        $("#txtWeightTax").val((WeightOzQty * WeightTax).toFixed(2));
    } else {
        $("#txtMLQty").val("0");
        $("#txtMLTax").val("0.00");
        $("#txtWeightQuantity").val('0.00');
        $("#txtWeightTax").val('0.00');
    }    
    calGrandTotal();
}


function calOverallDisc1() {

    var DiscAmt = parseFloat($("#txtDiscAmt").val()) || 0.00;
    var TotalAmount = (Number($("#txtTotalAmount").val()));
    if ($("#txtDiscAmt").val() != "") {
        if (parseFloat(DiscAmt) > parseFloat(TotalAmount)) {
            $("#txtDiscAmt").val('0.00');
            $("#txtOverallDisc").val('0.00');
        }
        else {
            if (parseFloat(TotalAmount) == 0) {
                $("#txtOverallDisc").val('0.00');
            } else {
                var per = (DiscAmt / TotalAmount) * 100;
                $("#txtOverallDisc").val(per.toFixed(2));
            }
            calTotalTax();
        }
    }
    else {
        $("#txtDiscAmt").val('0.00');
        $("#txtOverallDisc").val('0.00');
    }

}
function calOverallDisc() {
    var factor = 0.00;
    if ($("#txtOverallDisc").val() == "") {
        $("#txtOverallDisc").val('0.00');
    }
    else if (parseFloat($("#txtOverallDisc").val()) > 100) {
        $("#txtOverallDisc").val('0.00');
        toastr.error('Discount % cannot be greater than 100.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    factor = Number($("#txtOverallDisc").val()) * 0.01 || 0.00;

    $("#txtDiscAmt").val((Number($("#txtTotalAmount").val()) * factor).toFixed(2));
    calTotalTax();
}

function calGrandTotal() {
    var DiscAmt = 0.00;
    if ($("#txtDiscAmt").val() != "") {
        DiscAmt = parseFloat($("#txtDiscAmt").val())
    }
    var Shipping = 0.00;
    if ($("#txtShipping").val() != "") {
        Shipping = parseFloat($("#txtShipping").val())
    }

    var grandTotal = parseFloat($("#txtTotalAmount").val()) - parseFloat(DiscAmt) + parseFloat(Shipping)
        + parseFloat($("#txtTotalTax").val()) + parseFloat($("#txtMLTax").val()) + parseFloat($("#txtWeightTax").val());
    var round = Math.round(grandTotal);
    $("#txtAdjustment").val((round - grandTotal).toFixed(2));
    $("#txtGrandTotal").val(round.toFixed(2));
    funcheckprop();

}


function deleteItemrecord(e) {
    var span = document.createElement("span");
    span.innerHTML = "You want to <b style='color:red;'>delete permanently</b> this product from this order .";
    swal({
        title: "Are you sure?",
        content: span,
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, Delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleterow(e);

        } else {
            swal("", "Your product is safe.", "error");
        }
    })
}

function deleterow(e) {

    var tr = $(e).closest('tr');
    if ($(tr).find('.ItemType').text() == 'New') {
        $(e).closest('tr').remove();
        calTotalAmount();
        if ($("#tblProductDetail tbody tr").length == 0) {
            $("#emptyTable").show();
        } else {
            $("#emptyTable").hide();
        }
        swal("", "Item deleted successfully.", "success");

    } else {

        $.ajax({
            type: "POST",
            url: "/Manager/ManagerOrder.aspx/DeleteItem",
            data: "{'ItemAutoId':'" + $(tr).find('.ProId span').attr('itemautoid') + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            async: false,
            success: function (response) {
                if (response.d == 'Session Expired') {
                    location.href = '/';
                } else
                    if (response.d == 'true') {
                        $(e).closest('tr').remove();
                        calTotalAmount();
                        if ($("#tblProductDetail tbody tr").length == 0) {
                            $("#emptyTable").show();
                        } else {
                            $("#emptyTable").hide();
                        }
                        swal("", "Item deleted successfully.", "success");

                    } else {
                        swal("", "Item has not been deleted.", "error");
                    }
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });

    }
}



//}
/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                          Generate Order
/*-------------------------------------------------------------------------------------------------------------------------------*/
var PackedBoxes = 0;
//$("#btnSave").click(function () {
//    var flag1 = false, flag2 = false, flag3 = false;
//    if (Number($('#ddlCustomer').val()) != 0) {
//        if ($('#tblProductDetail tbody tr').length > 0) {
//            $("#tblProductDetail tbody tr").each(function () {
//                if ($(this).find(".ReqQty input").val() == "" || $(this).find(".ReqQty input").val() == "0") {
//                    $(this).find(".ReqQty input").focus().css("border-color", "red");
//                    flag1 = true;
//                } else {
//                    $(this).find(".ReqQty input").blur().css("border-color", "#ccc");
//                }
//                if ($(this).find('.IsExchange span').attr("isexchange") == '0' && $(this).find('.ProId span').attr("isfreeitem") == '0' && (Number($(this).find(".UnitPrice input").val()) < Number($(this).find(".UnitPrice input").attr('minprice')))) {
//                    flag2 = true;
//                    $(this).find(".UnitPrice input").addClass('border-warning');
//                }
//            });

//            if (!flag1 && !flag2 && !flag3) {
//                $("#alertDOrder").hide();
//                var orderData = {
//                    Terms: $("#txtTerms").val(),
//                    DraftAutoId: $("#DraftAutoId").val(),
//                    BillAddrAutoId: $("#hiddenBillAddrAutoId").val(),
//                    ShipAddrAutoId: $("#hiddenShipAddrAutoId").val(),
//                    TotalAmount: $("#txtTotalAmount").val(),
//                    OverallDisc: $("#txtOverallDisc").val(),
//                    OverallDiscAmt: $("#txtDiscAmt").val(),
//                    ShippingCharges: $("#txtShipping").val(),
//                    TotalTax: $("#txtTotalTax").val(),
//                    GrandTotal: $("#txtGrandTotal").val(),
//                    OrderRemarks: $("#txtOrderRemarks").val(),
//                    ShippingType: parseInt($("#ddlShippingType").val()),
//                    TaxType: parseInt($("#ddlTaxType").val()),
//                    MLQty: parseFloat($("#txtMLQty").val()),
//                    MLTax: parseFloat($("#txtMLTax").val()),
//                    AdjustmentAmt: parseFloat($("#txtAdjustment").val())
//                };

//                $.ajax({
//                    type: "Post",
//                    url: "/Manager/ManagerOrder.aspx/insertOrderData",
//                    data: "{'orderData':'" + JSON.stringify(orderData) + "'}",
//                    contentType: "application/json; charset=utf-8",
//                    dataType: "json",
//                    beforeSend: function () {
//                        $.blockUI({
//                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
//                            overlayCSS: {
//                                backgroundColor: '#FFF',
//                                opacity: 0.8,
//                                cursor: 'wait'
//                            },
//                            css: {
//                                border: 0,
//                                padding: 0,
//                                backgroundColor: 'transparent'
//                            }
//                        });
//                    },
//                    complete: function () {
//                        $.unblockUI();
//                    },
//                    success: function (data) {
//                        if (data.d != "Session Expired") {
//                            var xmldoc = $.parseXML(data.d);
//                            var orderDetails = $(xmldoc).find("Table");
//                            $("#txtHOrderAutoId").val($(orderDetails).find('OrderAutoId').text());
//                            $("#SavedMassege").modal('show');

//                        } else {
//                            location.href = '/';
//                        }
//                    },
//                    error: function (result) {
//                        $("#alertSOrder").show();
//                        $("#alertSOrder span").text(result.d);
//                    },
//                    failure: function (result) {
//                        console.log(JSON.parse(result.responseText).d);
//                    }
//                });
//            } else {
//                if (flag1) {

//                    toastr.error(' Required Quantity cannot be left empty.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
//                } else {

//                    toastr.error(' Unit Price can neither be less than Min. Price nor be greater than Max. Price.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

//                }
//            }
//        }
//        else {

//            toastr.error(' No Product Added in the List.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
//        }
//    }
//    else {
//        toastr.error(' Please select customer.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
//    }
//})
/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                     Edit Order Details
var ThisOrderCreditAmount = 0.00;
/*-------------------------------------------------------------------------------------------------------------------------------*/
function editOrder(orderNo) {
    var loginEmpType = "8";
    $.ajax({
        type: "POST",
        url: "/Manager/ManagerOrder.aspx/editOrder",
        data: "{'orderNo':'" + orderNo + "','loginEmpType':'" + loginEmpType + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var order = $(xmldoc).find("Table1");
                var items = $(xmldoc).find("Table2");
                var pkg = $(xmldoc).find("Table3");

                var CreditMemoDetails = $(xmldoc).find("Table5");
                var ApprovedCreditMemo = $(xmldoc).find("Table4");
                var CreditApprovelList = $(xmldoc).find("Table6");
                var RemarksDetails = $(xmldoc).find("Table");
                $("#shippingtype").val($(order).find("ShipAutoId").text());
                $("#Table2 tbody tr").remove();
                if ($(RemarksDetails).length > 0) {
                    $('#Div2').show();

                    var rowtest = $("#Table2 thead tr").clone();
                    $.each(RemarksDetails, function (index) {
                        $(".SRNO", rowtest).text((Number(index) + 1));
                        $(".EmployeeName", rowtest).text($(this).find("EmpName").text());
                        $(".EmployeeType", rowtest).text($(this).find("EmpType").text());
                        $(".Remarks", rowtest).text($(this).find("Remarks").text());
                        $("#Table2 tbody").append(rowtest);
                        rowtest = $("#Table2 tbody tr:last").clone(true);
                    })
                } else {
                    $('#Div2').hide();
                }
                $("#btnUpdate").show();
                $("#btnUpdateOrder").show();
                $("#btnSave").hide();
                $("#btnReset").hide();

                MLTaxRate = parseFloat($(order).find("MLTaxRate").text()).toFixed(2);
                var orderStatus = Number($(order).find("StatusCode").text());
                $("#hiddenStatusCode").val($(order).find("StatusCode").text());
                $("#txtHOrderAutoId").val($(order).find("AutoId").text());
                $("#txtOrderId").val($(order).find("OrderNo").text());
                $("#txtOrderRemarks").val($(order).find("OrderRemarks").text());
                $("#txtPackerRemarks").val($(order).find("PackerRemarks").text());
                $("#txtManagerRemarks").val($(order).find("ManagerRemarks").text());
                $("#txtOrderDate").val($(order).find("OrderDate").text());
                $('#txtAssignDate').pickadate({
                    format: 'mm/dd/yyyy',
                    formatSubmit: 'mm/dd/yyyy',
                    selectYears: true,
                    selectMonths: true,
                    min: -7,
                    firstDay: 0
                });
                $("#ddlCustomer").val($(order).find("CustomerAutoId").text());
                $("#txtCustomerType").val($(order).find("CustomerTypeName").text());
                $("#ddlCustomer1").val($(order).find("CustomerName").text());


                $("#ddlTaxType").val($(order).find("TaxType").text());
                $("#txtDeliveryDate").val($(order).find("DeliveryDate").text());
                $("#txtOrderStatus").val($(order).find("Status").text());
                $("#hiddenOrderStatus").val($(order).find("StatusCode").text());

                $("#txtTerms").val($(order).find("TermsDesc").text());
                $("#ddlShippingType").val($(order).find("ShippingType").text());
                $("#txtTimes").val($(order).find("Times").text());
                $("#txtHTimes").val($(order).find("Times").text());
                $("#txtHWarehouseRemark").val($(order).find("WarehouseRemarks").text());
                $("#txtAdjustment").val($(order).find("AdjustmentAmt").text());
                $("#txtWarehouseRemark").val($(order).find("WarehouseRemarks").text());

                $("#hiddenBillAddrAutoId").val($(order).find("BillAddrAutoId").text());

                $("#divFullBillAddress").html($(order).find("BillAddr").text() + ',<br>' + $(order).find("City1").text() + ', ' + $(order).find("StateCode").text() + ' - ' + $(order).find("Zipcode1").text());
                //$("#txtBillAddress").val($(order).find("BillAddr").text());
                //$("#txtBillState").val($(order).find("State1").text());
                //$("#txtBillCity").val($(order).find("City1").text());
                //$("#txtBillZip").val($(order).find("Zipcode1").text());

                $("#hiddenShipAddrAutoId").val($(order).find("ShipAddrAutoId").text());

                $("#divFullShippingAddress").html($(order).find("ShipAddr").text() + ',<br>' + $(order).find("City2").text() + ', ' + $(order).find("StateCode2").text() + ' - ' + $(order).find("Zipcode2").text());
                //$("#txtShipAddress").val($(order).find("ShipAddr").text());
                //$("#txtShipState").val($(order).find("State2").text());
                //$("#txtShipCity").val($(order).find("City2").text());
                //$("#txtShipZip").val($(order).find("Zipcode2").text());

                $("#txtTotalAmount").val($(order).find("TotalAmount").text());
                $("#txtDiscAmt").val($(order).find("OverallDiscAmt").text());


                $("#txtMLQty").val($(order).find("MLQty").text());
                $("#txtMLTax").val($(order).find("MLTax").text());

                $("#txtWeightQuantity").val($(order).find("Weigth_OZQty").text());
                $("#txtWeightTax").val($(order).find("Weigth_OZTaxAmount").text());

                if ($(order).find("Weigth_OZTax").text() != '') {
                    WeightTax = $(order).find("Weigth_OZTax").text();
                }
                $("#txtTotalTax").val($(order).find("TotalTax").text());
                $("#txtGrandTotal").val($(order).find("GrandTotal").text());
                $("#txtDeductionAmount").val($(order).find("DeductionAmount").text());
                $("#CreditMemoAmount").text($(order).find("CustomerCredit").text());

                if ($(order).find("CreditAmount").text() != "") {
                    $("#txtStoreCreditAmount").val($(order).find("CreditAmount").text());


                }
                if (parseFloat($(order).find("CreditAmount").text()) < 0) {
                    ThisOrderCreditAmount = parseFloat($(order).find("CreditAmount").text().replace('-', ''));
                }
                if ((orderStatus == 3 || orderStatus == 8 || orderStatus == 9 || orderStatus == 10) && loginEmpType == 8) {
                    $("#creditShow").show();
                } else {
                    $("#creditShow").hide();
                }

                if ($(order).find('PayableAmount').text() != '') {
                    $("#txtPaybleAmount").val($(order).find("PayableAmount").text());
                } else {
                    $("#txtPaybleAmount").val($(order).find("GrandTotal").text());
                }
                if ($(order).find("DeductionAmount").text() != "")
                    $("#txtDeductionAmount").val($(order).find("DeductionAmount").text());
                $("#txtPackedBoxes").val($(order).find("PackedBoxes").text());
                PackedBoxes = $(order).find("PackedBoxes").text();

                $("#txtAmtPaid").val($(order).find("AmtPaid").text());
                $("#txtAmtDue").val($(order).find("AmtDue").text());
                $("#txtOverallDisc").val($(order).find("OverallDisc").text());
                $("#txtShipping").val($(order).find("ShippingCharges").text());
                $("#tblProductDetail tbody tr").remove();
                var row = $("#tblProductDetail thead tr").clone(true);

                $.each(items, function () {
                    $(".AutoId", row).text($(this).find("ItemAutoId").text());

                    $(".ProId", row).html("<span EligibleforFree='" + $(this).find("EligibleforFree").text() + "' isFreeItem='" + $(this).find("isFreeItem").text() + "' ItemAutoId='" + $(this).find("ItemAutoId").text() + "'></span><productid>" + $(this).find("ProductId").text() + "</productid>");

                    if ($(this).find("isFreeItem").text() == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'></span><productName>" + $(this).find("ProductName").text() + "</productName> <item class='badge badge badge-pill badge-success'>Free</item>");
                    }
                    else if ($(this).find("IsExchange").text() == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'></span><productName>" + $(this).find("ProductName").text() + "</productName> <item class='badge badge badge-pill badge-primary'>Exchange</item>");
                    }
                    else if ($(this).find("Tax").text() == 1) {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'></span><productName>" + $(this).find("ProductName").text() + "</productName> <item class='badge badge badge-pill badge-danger'>Taxable</item>");
                    }
                    else {
                        $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'></span><productName>" + $(this).find("ProductName").text() + "</productName> <item class='badge badge badge-pill'></item>");
                    }
                    $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                    $(".ReqQty", row).html("<input type='text' maxlength='4' disabled='disabled' class='form-control input-sm border-primary text-center' onkeypress='return isNumberKey(event)' runat='server' onkeyup='rowCal(this)' value='" + $(this).find("RequiredQty").text() + "'/>");
                    $(".PerPrice", row).text($(this).find("PerpiecePrice").text());
                    $(".ItemType", row).text('Old');

                    $(".TtlPcs", row).text($(this).find("TotalPieces").text());
                    $(".QtyRemain", row).text($(this).find("RemainQty").text());
                    $(".OM_MinPrice", row).text($(this).find("OM_MinPrice").text());
                    $(".OM_BasePrice", row).text($(this).find("OM_BasePrice").text());

                    $(".OM_CostPrice", row).text($(this).find("OM_CostPrice").text());
                    $(".ItemTotal", row).text($(this).find("ItemTotal").text());
                    if (Number($(this).find("IsExchange").text()) == 0 && Number($(this).find("isFreeItem").text()) == 0) {
                        $(".Discount", row).html("<input type='text' class='form-control input-sm border-primary' onkeypress='return isNumberDecimalKey(event,this)' style='text-align:right;width:70px;' maxlength='6' value='" + parseFloat($(this).find("Discount").text()).toFixed(2) + "' onkeyup='checkdiscount(this)'/>");
                    }
                    else {
                        $(".Discount", row).html("<input type='text' class='form-control input-sm border-primary' disabled onkeypress='return isNumberDecimalKey(event,this)' style='text-align:right;width:70px;' maxlength='6' value='" + parseFloat($(this).find("Discount").text()).toFixed(2) + "' onkeyup='checkdiscount(this)'/>");

                    }

                    $(".NetPrice", row).text($(this).find("NetPrice").text());
                    if (Number($(this).find("IsExchange").text()) == 0 && Number($(this).find("isFreeItem").text()) == 0) {
                        $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' onkeypress='return isNumberDecimalKey(event,this)' style='text-align:right;width:70px;' maxlength='6' value='" + $(this).find("UnitPrice").text() + "' onkeyup='changePrice(this)' minprice='" + $(this).find("MinPrice").text() + "' baseprice='" + $(this).find("Price").text() + "'/>");
                    } else {
                        $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' onkeypress='return isNumberDecimalKey(event,this)' style='text-align:right;width:70px;' maxlength='6'  value='" + $(this).find("UnitPrice").text() + "' disabled onkeyup='changePrice(this)' minprice='" + $(this).find("MinPrice").text() + "' baseprice='" + $(this).find("Price").text() + "'/>");
                    }

                    $(".SRP", row).text($(this).find("SRP").text());
                    $(".GP", row).text($(this).find("GP").text());
                    if (Number($(this).find("Tax").text()) == 1) {
                        $(".TaxRate", row).html('<span typetax="1" MLQty="' + $(this).find('UnitMLQty').text() + '" WeightOz="' + $(this).find('WeightOz').text() + '"></span>' + '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>');
                    } else {
                        $(".TaxRate", row).html('<span typetax="0" MLQty="' + $(this).find('UnitMLQty').text() + '" WeightOz="' + $(this).find('WeightOz').text() + '"></span>');
                    }
                    if (Number($(this).find("IsExchange").text()) == 1) {
                        $(".IsExchange", row).html('<span IsExchange="1"></span>' + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                    } else {
                        $(".IsExchange", row).html('<span IsExchange="0"></span>');
                    }

                    $(".Barcode", row).text($(this).find("Barcode").text());
                    if ($(this).find("QtyShip").text() == null || $(this).find("QtyShip").text() == "") {
                        $(".QtyShip", row).html("<input type='text' class='form-control input-sm border-primary text-center' onkeypress='return isNumberKey(event)' style='width:70px;' value='0' />");
                    } else {
                        if (orderStatus > 2) {
                            $(".QtyShip", row).html("<input type='text' class='form-control input-sm border-primary text-center' onkeypress='return isNumberKey(event)' style='width:70px;' value='" + $(this).find("QtyShip").text() + "' />");
                        } else {
                            $(".QtyShip", row).html("<input type='text' class='form-control input-sm border-primary text-center' onkeypress='return isNumberKey(event)' style='width:70px;' value='" + $(this).find("QtyShip").text() + "' />");
                        }
                    }
                    if (orderStatus == 3 || orderStatus == 4 || orderStatus == 9 || orderStatus == 10) {
                        $(".Action", row).html("<a title='Product History' href='javascript:;' id='history' onclick='ProductList(this)'><span class='la la-history'></span></a>&nbsp;&nbsp;<a href='javascript:;' id='deleterow' onclick='EditOrderItem(this)'><span class='ft-edit'></span></a>&nbsp;&nbsp;<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-delete'></span></a>");
                    }
                    else {
                        $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-delete'></span></a>");
                    }

                    $('#tblProductDetail tbody').append(row);

                    row = $("#tblProductDetail tbody tr:last").clone(true);

                });


                //$("input[type='text']").attr("disabled", true);
                $("textarea").attr("disabled", true);
                $("#packerdetails").hide();
                if (Number(orderStatus) > 2) {
                    $('#txtPackerRemarks').attr('disabled', true);
                    $("#packerdetails").show()
                }
                $("select").attr("disabled", true);
                $("#panelProduct").hide();
                $("#btnUpdate").hide();
                $("#btnUpdateOrder").hide();
                $("#tblProductDetail").find(".Action").hide();
                $("#popBillAddr").hide();
                $("#popShipAddr").hide();
                $("#hidenopackedbox").show();
                $('#packerdetails').hide();
                $('#ManagerRemarks').show();


                /*-----------------------------------------------------------------------------------------*/
                $("#btnSetAsProcess").hide();
                /*-----------------------------------------------------------------------------------------*/

                if ((orderStatus == 3 || orderStatus == 9 || orderStatus == 10) || orderStatus == 4) {
                    $("#tblProductDetail tbody tr").each(function () {
                        if (Number($(this).find(".ReqQty input").val()) != Number($(this).find(".QtyShip input").val())) {
                            $("#btnBackOrder").show();
                        }
                    });
                    if ($(order).find("ShippingType").text() != 6 && $(order).find("ShippingType").text() != 5) {
                        if (orderStatus != 9) {
                            $("#btnAsgnDrv").show()
                        };
                    }
                    if ((orderStatus == 3 || orderStatus == 9 || orderStatus == 10)) {
                        $("#btnSetAsProcess").show();
                    }
                    if (orderStatus == 3 || orderStatus == 9 || orderStatus == 10 || orderStatus == 4) {
                        $("#btnEditOrder").show();
                    }
                    if ($(order).find("Driver").text() != null && $(order).find("Driver").text() != "") {
                        $("#btnGenOrderCC").show();
                    }
                }

                if (orderStatus != 5 && orderStatus != 6 && orderStatus != 11 && orderStatus != 8) {
                    $("#btnCancelOrder").show();
                }
                if (orderStatus > 2) {
                    $("#btnGenBar").show();
                }

                /*-----------------------------------------------------------------------------------------*/
                if (orderStatus > 3) {
                    $("#btnGenOrderCC").show();
                    $("#popBillAddr").hide();

                }



                if (orderStatus == 4 && $(order).find("ShippingType").text() == 5) {
                    $("#btnUpdateDel").show();
                }

                $("#txtReadBorCode").removeAttr("disabled");
                if (orderStatus >= 3) {

                    $("#tblProductDetail").find(".Barcode").show().end()
                        .find(".QtyShip").show();
                    $("#divPkg").show();

                    $("#Table1 tbody tr").remove();
                    var rowP = $("#Table1 thead tr:last").clone(true);
                    if (pkg.length > 0) {
                        $.each(pkg, function (index) {
                            $(".SRNO", rowP).text(Number(index) + 1);
                            $(".PackedId", rowP).text($(this).find("PackingId").text());
                            $(".PackedDate", rowP).text($(this).find("PkgDate").text());
                            $(".PackedBy", rowP).text($(this).find("Packer").text());
                            $('#Table1').find("tbody").append(rowP);
                            rowP = $("#Table1 tbody tr:last").clone(true);
                        });
                    }

                    if ($(order).find("Driver").text() != null && $(order).find("Driver").text() != "") {
                        $("#btnAsgnDrv").html("<b>Change Driver</b>");
                        if (orderStatus == 7) {
                            $("#btnAsgnDrv").html("<b>Reassign Driver</b>");
                        }
                        $("#divPkg").append("<span>Driver : <b>" + $(order).find("DrvName").text() + "</b><br />Assigned on : <b>" + $(order).find("AssignDate").text() + "</b></span>");
                        $("#divPkg").show();
                    }


                    if (orderStatus == 7 || orderStatus == 8) {
                        $("#divPkg").append("<br /><span style='color:red;'>Undelivery Remark : <b>" + $(order).find("DrvRemarks").text() + "</b></span>");
                        $("#divPkg").show();
                    }

                    $(".SMANAGER").show()
                    if (Number(CreditMemoDetails.length) > 0) {
                        $('#MODALpoPFORCREDIT').modal('show');
                        $("#tblCreditMemo tbody tr").remove();
                        var rowc = $("#tblCreditMemo thead tr:last").clone(true);
                        $.each(CreditMemoDetails, function (index) {
                            $(".SRNO", rowc).text(Number(index) + 1);
                            $(".Action", rowc).html("<input type='radio' name='credit' value=" + $(this).find("CreditAutoId").text() + "> ");
                            $(".CreditNo", rowc).html("<a target='_blank' href='/Manager/ManagerCreditMemo.aspx?PageId=" + $(this).find("CreditAutoId").text() + "'>" + $(this).find("CreditNo").text() + "</a>");
                            $(".CreditDate", rowc).text($(this).find("CreditDate").text());
                            $(".NoofItem", rowc).text($(this).find("NoofItem").text());
                            $(".NetAmount", rowc).text($(this).find("NetAmount").text());
                            $("#tblCreditMemo tbody").append(rowc)
                            rowc = $("#tblCreditMemo tbody tr:last").clone(true);
                        });
                    }
                    $("#TotalDue").text('0.00');
                    $("#deductionAmount").text('0.00');
                    if (Number(ApprovedCreditMemo.length) > 0) {
                        var TotalDue = 0.00;
                        $("#CusCreditMemo").show();
                        $('#dCuCreditMemo').css('display', 'block')
                        $("#tblCreditMemoList tbody tr").remove();
                        var rowc1 = $("#tblCreditMemoList thead tr:last").clone(true);
                        $.each(ApprovedCreditMemo, function (index) {

                            $(".SRNO", rowc1).text(Number(index) + 1);
                            if ($(this).find('OrderAutoId').text() != '') {
                                $(".Action", rowc1).html("<input type='checkbox' OrderAutoId='" + $(this).find('OrderAutoId').text() + "' checked name='creditMemo' onclick='funcheckprop(this)' value=" + $(this).find("CreditAutoId").text() + "> ");
                            } else {
                                $(".Action", rowc1).html("<input type='checkbox' OrderAutoId='" + $(this).find('OrderAutoId').text() + "' name='creditMemo' onclick='funcheckprop(this)' value=" + $(this).find("CreditAutoId").text() + "> ");
                            }
                            $(".CreditNo", rowc1).html("<a target='_blank' href='/Sales/CreditMemo.aspx?PageId=" + $(this).find("CreditAutoId").text() + "'>" + $(this).find("CreditNo").text() + "</a>");
                            $(".CreditDate", rowc1).text($(this).find("CreditDate").text());
                            $(".ReturnValue", rowc1).text($(this).find("ReturnValue").text());
                            $(".amtDeducted", rowc1).text($(this).find("amtDeducted").text());
                            TotalDue = parseFloat(TotalDue) + parseFloat($(this).find("ReturnValue").text());

                            $("#tblCreditMemoList tbody").append(rowc1)
                            rowc1 = $("#tblCreditMemoList tbody tr:last").clone(true);
                        });
                        $("#TotalDue").text(TotalDue.toFixed(2));

                    } else {
                        $("#CusCreditMemo").hide();
                        $('#dCuCreditMemo').css('display', 'none')
                    }

                    $("#tblMemoList tbody tr").remove();
                    $("#Td4").text('0.00');
                }


                if (orderStatus == 3 || orderStatus == 4 || orderStatus == 9 || orderStatus == 10) {
                    editPackedOrder();
                }
                if (loginEmpType == 8 && orderStatus == 9) {
                    $("#btnAsgnDrv").hide();
                }
                if (orderStatus == 8) {
                    $("#btnGenOrderCC").hide();
                } else {
                    $("#btnGenOrderCC").show();
                }
                //if (Number(LoginPerson) == 8) {
                $("#ddlShippingType").attr("disabled", false);
                //}
                $('#txtDeliveryDate').attr('disabled', false);

                $("#ddlCustomerType").val($(order).find("CustomerType").text());

                if ($(order).find("CustomerType").text() == '3') {
                    $("#istaxable").hide();
                }



            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}

function EditOrderItem(e) {
    var tr = $(e).closest('tr');
    var ItemAutoId = $(tr).find('.ProId span').attr('ItemAutoId');
    $('#txtProductId').val($(tr).find('.ProId productid').html());
    $('#txtProductName').val($(tr).find('.ProName productName').html());
    if ($(tr).find('.TaxRate span').attr('typetax') == '1') {
        $('#chkTaxablePop').prop('checked', true);
    } else {
        $('#chkTaxablePop').prop('checked', false);
    }
    if ($(tr).find('.IsExchange span').attr('isexchange') == '1') {
        $('#chkExchangePop').prop('checked', true);
    } else {
        $('#chkExchangePop').prop('checked', false);
    }

    if ($(tr).find('.ProId span').attr('isfreeitem') == '1') {
        $('#chkFreePop').prop('checked', true);
        $('.freechecjbox').show();

    } else {
        $('.freechecjbox').hide();
        $('#chkFreePop').prop('checked', false);
    }

    if ($(tr).find('.ProId span').attr('EligibleforFree') == '1') {
        $('.freechecjbox').show();

    } else {
        $('.freechecjbox').hide();
    }



    $('#ItemAutoId').val(ItemAutoId);
    $('#PopChangeItemType').modal('show');
}
function CloseCheckPop() {
    $('#PopChangeItemType').modal('hide');
}

function updateIteminList() {
    if ($("#ddlTaxType").val() == '0') {
        swal("", "Tax Type is not available for this Customer.", "error");
    } else {
        swal({
            title: "Are you sure?",
            text: "You want to change Item name.",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, Cancel.",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes, Change it.",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                updateItemTypefromDB();
            } else {
                $("#PopChangeItemType").modal('hide');
            }
        })
    }
}

function updateItemTypefromDB() {
    var Exchange = 0, FreeItem = 0, Taxable = 0;
    if ($("#chkExchangePop").prop('checked')) {
        Exchange = 1;
    }
    if ($("#chkFreePop").prop('checked')) {
        FreeItem = 1;
    }
    if ($("#chkTaxablePop").prop('checked')) {
        Taxable = 1;
    }
    var dataValue = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        ItemAutoId: $("#ItemAutoId").val(),
        IsExchange: Exchange,
        IsFreeItem: FreeItem,
        IsTaxable: Taxable
    }
    $.ajax({
        type: "POST",
        url: "/Manager/ManagerOrder.aspx/ItemTypeUpdate",
        data: "{'dataValue':'" + JSON.stringify(dataValue) + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        async: false,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            debugger
            if (response.d == 'Session Expired') {
                location.href = "/";
            } else if (response.d == 'false') {
                swal("", "Oops! Something went wrong. Please try later.", "error")
            } else {
                var xmldoc = $.parseXML(response.d);
                var order = $(xmldoc).find("Table");
                $("#tblProductDetail tbody tr").each(function () {
                    if ($(this).find('.ProId span').attr('itemautoid') == $('#ItemAutoId').val()) {
                        $(this).find('.ProName item').removeClass("badge-success");
                        $(this).find('.ProName item').removeClass("badge-primary");
                        $(this).find('.ProName item').removeClass("badge-danger");
                        $(this).find('.TaxRate span').attr('typetax', 0);
                        $(this).find('.IsExchange span').attr('isexchange', 0);
                        $(this).find('.ProId span').attr('isfreeitem', 0);

                        if (Exchange == 0 && FreeItem == 0) {
                            if ($(order).find('CustomPrice').text() == "") {
                                $(this).find(".UnitPrice input").removeAttr('disabled');
                                $(this).find(".UnitPrice input").val($(order).find('Price').text());
                                $(this).find(".NetPrice").text(parseFloat(parseFloat($(order).find('Price').text()) * parseInt($(this).find(".QtyShip input").val())).toFixed(2));
                            } else {
                                $(this).find(".UnitPrice input").removeAttr('disabled');
                                $(this).find(".UnitPrice input").val(parseFloat(parseFloat($(order).find('MinPrice').text()) > parseFloat($(order).find('CustomPrice').text()) ? $(order).find('Price').text() : $(order).find('CustomPrice').text()).toFixed(2));
                                $(this).find(".NetPrice").text(parseFloat(parseFloat($(this).find(".UnitPrice input").val()) * parseInt($(this).find(".QtyShip input").val())).toFixed(2));
                            }
                        } else {
                            $(this).find(".UnitPrice input").attr('disabled', 'disabled');
                            $(this).find(".UnitPrice input").val('0.00');
                            $(this).find(".NetPrice").text('0.00');
                            $(this).find(".Discount input").val('0.00');
                            $(this).find(".Discount input").attr('disabled', 'disabled');
                        }
                        if (Exchange == 1) {
                            $(this).find('.ProName item').addClass("badge-primary");
                            $(this).find('.ProName item').html('Exchange');
                            $(this).find('.IsExchange span').attr('isexchange', 1);
                        } else if (FreeItem == 1) {
                            $(this).find('.ProName item').addClass("badge-success");
                            $(this).find('.ProName item').html('Free');
                            $(this).find('.ProId span').attr('isfreeitem', 1);
                        } else if (Taxable == 1) {
                            $(this).find('.ProName item').addClass("badge-danger");
                            $(this).find('.ProName item').html('Taxable');
                            $(this).find('.TaxRate span').attr('typetax', 1);
                        } else {
                            $(this).find('.ProName item').html('');
                        }
                    }
                });
                calTotalAmount();
                swal("", "Item changed successfully.", "success").then(function () {
                    $("#PopChangeItemType").modal('hide');
                });
            }
        },
        error: function (result) {
        },
        failure: function (result) {

        }
    })
}


function funcheckprop(e) {
    $('#txtStoreCreditAmount').attr('disabled', true);
    if ($("#txtStoreCreditAmount").val() == '') {
        $("#txtStoreCreditAmount").val('0.00');
    }
    if ($(e).attr('id') != 'txtStoreCreditAmount') {
        $("#txtStoreCreditAmount").val('0.00');
    }
    var tr = $(e).closest('tr');
    var deductionAmount = 0.00;
    var GrandTotal = $("#txtGrandTotal").val();
    var CreditLimit = parseFloat($("#CreditMemoAmount").text());
    var payable = 0.00;
    $("#tblCreditMemoList tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            deductionAmount += parseFloat($(this).find('.ReturnValue').text());
        }
    });

    //if (parseFloat(deductionAmount) > parseFloat(GrandTotal)) {
    //    payable = 0.00;
    //} else {
    //    payable = parseFloat(GrandTotal) - parseFloat(deductionAmount);
    //}
    //$("#txtDeductionAmount").val(deductionAmount.toFixed(2));
    //$("#txtPaybleAmount").val(payable.toFixed(2));
    //$("#txtStoreCreditAmount").val('0.00');

    $("#txtDeductionAmount").val(parseFloat(deductionAmount).toFixed(2));
    if (parseFloat(deductionAmount) > parseFloat(GrandTotal)) {
        payable = 0.00;
        $("#txtStoreCreditAmount").val(parseFloat(GrandTotal).toFixed(2) - parseFloat(deductionAmount).toFixed(2));
    } else {
        payable = parseFloat(GrandTotal) - parseFloat(deductionAmount);
    }
    $("#txtPaybleAmount").val(payable.toFixed(2));
    checkcreditAmount();
}

function checkcreditAmount() {

    var GrandAmount = parseFloat($("#txtGrandTotal").val());
    var DeductionAmount = 0.00;
    if ($("#txtDeductionAmount").val() != "") {
        DeductionAmount = parseFloat($("#txtDeductionAmount").val());
    }

    var NetAmount = parseFloat(GrandAmount) - parseFloat(DeductionAmount);
    var StoreCreditAmount = 0.00
    if ($("#txtStoreCreditAmount").val() != "") {
        StoreCreditAmount = parseFloat($("#txtStoreCreditAmount").val());
    }
    var CreditMemoAmount = 0.00;
    if ($("#CreditMemoAmount").text() != '') {
        CreditMemoAmount = parseFloat($("#CreditMemoAmount").text());
    }

    if (parseFloat(NetAmount) < parseFloat(StoreCreditAmount)) {
        $("#txtStoreCreditAmount").val('0.00');
        StoreCreditAmount = '0.00';
    }
    if (CreditMemoAmount < StoreCreditAmount) {
        $("#txtStoreCreditAmount").val('0.00');
        StoreCreditAmount = '0.00';
    }
    var payamount = (parseFloat(NetAmount) - parseFloat(StoreCreditAmount));
    if (parseFloat(payamount) > 0) {
        $("#txtPaybleAmount").val(payamount.toFixed(2));
    } else {
        $("#txtPaybleAmount").val();
    }

}
function fn_calculateDudection(e) {
    var tr = $(e).closest('tr');
    if (parseFloat($(e).val()) > parseFloat($(tr).find('.amtDue').text())) {
        $(e).val('0.00');
    }
    var deductionAmount = 0.00;
    $("#tblCreditMemoList tbody tr").each(function () {

        if ($(this).find('.Action input').prop('checked') == true) {

            deductionAmount += parseFloat($(this).find('.ReturnValue').text());

        }
    });

    $("#txtDeductionAmount").val(deductionAmount.toFixed(2));
    calTotalAmount();

}
//Credit Memo ProcessedIsExchange
function CreditProcessed() {
    var test = 0;
    $("#tblCreditMemo tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            location.href = '/Manager/ManagerCreditMemo.aspx?PageId=' + $(this).find('.Action input').val();
            test = 1;
        }
    });

    if (test == 0) {
        $("#alertmsg").html('Please Check any one item for Process.');
        setTimeout(function () {
            $("#alertmsg").html('')
        }, 5000)
    }
}
function skipCreditmemo() {
    $("#MODALpoPFORCREDIT").modal('hide');
}


/*start deduction pay amount for credit memo*/
function DeductPay() {


    var icount = 0;
    var check = false;
    var deductionamount = 0.00;
    var Deductiontable = [];
    $("#tblCreditMemoList tbody tr").each(function () {




        if (parseFloat($(this).find('.Action input').val()) > 0) {
            if ($(this).find('.Action input').attr('OrderAutoId') == '') {
                if ($(this).find('.Action input').prop('checked') == true) {
                    Deductiontable[icount] = new Object();
                    Deductiontable[icount].CreditAutoId = $(this).find('.Action input').val() || 0;
                    Deductiontable[icount].AmtDeduction = $(this).find('.amtDeduction input').val() || 0;
                    Deductiontable[icount].Remarks = $(this).find('.dueRemarks input').val() || '';
                    icount++;
                    deductionamount += parseFloat($(this).find('.amtDeduction input').val());
                }
            } else if (parseFloat($(this).find('.amtDeduction input').val()) > 0) {
                Deductiontable[icount] = new Object();
                Deductiontable[icount].CreditAutoId = $(this).find('.Action input').val() || 0;
                Deductiontable[icount].AmtDeduction = $(this).find('.amtDeduction input').val() || 0;
                Deductiontable[icount].Remarks = $(this).find('.dueRemarks input').val() || '';
                icount++;
                deductionamount += parseFloat($(this).find('.amtDeduction input').val());
            }

        }
    })


    //if ($(this).find('.Action input').prop('checked') == true) {

    //    if ($(this).find('.amtDeduction input').val() == '' || parseFloat($(this).find('.amtDeduction input').val()) <= 0) {
    //        $(this).find('.amtDeduction input').css('border-color', '#ff0000bf');
    //        $(this).find('.amtDeduction input').focus();
    //        check = true;
    //    }

    //} else {
    //    $(this).find('.amtDeduction input').css('border-color', '#ddd');
    //}

    var GrandAmount = $("#txtGrandTotal").val();
    if (parseFloat(GrandAmount) >= parseFloat(deductionamount)) {
        if (Number(Deductiontable.length) > 0) {
            $.ajax({
                type: "POST",
                url: "/Manager/ManagerOrder.aspx/deductionPayamount",
                data: "{'TableValues':'" + JSON.stringify(Deductiontable) + "','OrderNo':'" + getid + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d == 'Session Expired') {
                        location.href = "/";
                    } else if (response.d == "true") {
                        $("#msg").html('Saved Successfully.').css('color', 'green');
                        editOrder(getid);
                    } else {
                        $("#msg").html(response.d).css('color', 'red');
                    }

                    setTimeout(function () {
                        $("#msg").html('');
                    }, 5000);
                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        } else {
            $("#msg").html('Deduction amount should be greater than 0').css('color', 'red');
            setTimeout(function () {
                $("#msg").html('');
            }, 5000);
        }
    } else {
        $("#msg").html('Deduction amount should be less or equal than Grand Amount').css('color', 'red');
        setTimeout(function () {
            $("#msg").html('');
        }, 5000);

    }
}

function deleteDeductionAmount(e) {

    if (confirm('Are you sure you want delete this Item')) {

        $.ajax({
            type: "POST",
            url: "/Manager/ManagerOrder.aspx/deleteDeductionAmount",
            data: "{'LogAutoId':'" + $(e).val() + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d == 'Session Expired') {
                    location.href = "/";
                } else if (response.d == "true") {
                    $("#msg").html('deleted Successfully.').css('color', 'green');
                    editOrder(getid);
                } else {
                    $("#msg").html(response.d).css('color', 'red');
                }

                setTimeout(function () {
                    $("#msg").html('');
                }, 5000);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}




/*-------------------------------------------------------------------------------------------------------------------*/
//                                       Packer print new Order by this function. 
/*-------------------------------------------------------------------------------------------------------------------*/

function print_NewOrder() {
    localStorage.setItem('checkprint', 1);
    window.open("/Packer/OrderPrint.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}

function itemProduct() {
    if (!PackerSecurityEnable) {
        $("#txtSecurity").val('');
        $("#txtSecurity").removeAttr('disabled');
        $('#SecurityEnabled').modal('show');
        $("#txtSecurity").focus();
    }
}

function AddProductQty() {
    if (!PackerSecurityEnable) {
        itemProduct();
        return;
    }
    if (Page_ClientValidate('AddProductQty')) {
        if ($("#txtQty").val() != '' || $("#txtQty").val() != '') {
            $.ajax({
                type: "Post",
                async: false,
                url: "/Manager/ManagerOrder.aspx/AddProductQty",
                data: "{'ProductAutoId':'" + $("#ddlitemproduct").val() + "','UnitAutoId':'" + $("#ddlitemproduct").find(':selected').attr('unitautoid') + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        var xmldoc = $.parseXML(response.d);
                        var product = $(xmldoc).find("Table");

                        var prodAutoId = $("#ddlitemproduct").val();
                        var unitAutoId = $("#ddlitemproduct").find(':selected').attr('unitautoid');
                        var stock = $(product).find("Stock").text();
                        var barcode = $(product).find("Barcode").text();
                        var flag1 = true, qty = 0, pieces = 0;
                        var countforscan = 0;
                        if (product.length > 0) {
                            $("#tblProductDetail tbody tr").each(function () {
                                var IsExchange = 0, IsFreeItem = 0;
                                if ($("#IsExchange").prop('checked')) {
                                    IsExchange = 1;
                                }
                                if ($("#IsFreeItem").prop('checked')) {
                                    IsFreeItem = 1;
                                }
                                if ($(this).find(".ProId span").attr("IsFreeItem") == IsFreeItem && $(this).find(".ProName span").attr("productautoid") == prodAutoId && $(this).find(".UnitType span").attr("unitautoid") == unitAutoId && IsExchange == $(this).find(".IsExchange span").attr("isexchange")) {
                                    qty = Number($("#txtQty").val()) + Number($(this).find(".QtyShip input").val());
                                    pieces = qty * Number($(this).find(".UnitType span").attr('qtyperunit'));

                                    if (pieces <= stock && qty <= Number($(this).find(".ReqQty input").val())) {
                                        $(this).find(".Barcode").text(barcode);
                                        if (qty == Number($(this).find(".ReqQty input").val())) {
                                            $(this).find(".QtyShip input").val(qty).attr("disabled", false).css("background-color", "#fff").removeClass("verify");
                                            countforscan = countforscan + 1;
                                        } else {
                                            $(this).find(".QtyShip input").val(qty).attr("disabled", false).css("background-color", "#FFECB3").addClass("verify");
                                        }

                                        $(this).find(".QtyRemain").text(Number($(this).find(".QtyRemain").text()) - qty);
                                        rowCal($(this).find(".QtyShip input"));

                                        var trfind = $(this);
                                        $('#tblProductDetail tbody tr:first').before(trfind);
                                    } else {
                                        if (pieces > stock) {
                                            $("#barcodemsg").html("Error ! Qty is not avaiable in stock");
                                            $("#BarCodenotExists").modal("show");
                                        }

                                        if (qty > Number($(this).find(".ReqQty input").val())) {
                                            $("#barcodemsg").html("Error ! Shipped Qty cannot be more than Required Qty");
                                            $("#BarCodenotExists").modal("show");

                                        }
                                    }

                                } else if ($(this).find(".ProName span").attr("productautoid") == prodAutoId) {
                                    if (Number($(this).find(".QtyShip input").val()) != Number($(this).find(".ReqQty input").val())) {
                                        countforscan = countforscan + 1;
                                    }
                                }
                            });


                            $("#tblProductDetail tbody tr").each(function () {

                                if ($(this).find(".ProName span").attr("productautoid") == prodAutoId) {
                                    if (Number($(this).find(".ReqQty input").val()) == Number($(this).find(".QtyShip input").val())) {
                                        if (countforscan == 0)
                                            countforscan = 1;
                                    } else {
                                        countforscan = 2;
                                    }
                                }
                            })
                            if (Number(countforscan) == 1) {
                                $("#ddlitemproduct option[value='" + prodAutoId + "']").remove();
                            }
                        } else {
                            $("#barcodemsg").html("Error ! Barcode does not exist");
                            $("#BarCodenotExists").modal("show");
                        }


                        $("#txtScanBarcode").val("").focus();
                        $("#txtQty").val("1");
                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        }
        else {
            $("#barcodemsg").html("Message ! Please enter the qty");
            $("#BarCodenotExists").modal("show");
        }
    }
}

function checkBarcode() {
    $.ajax({
        type: "Post",
        url: "/Manager/ManagerOrder.aspx/checkBarcode",
        data: "{'Barcode':'" + $("#txtScanBarcode").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var product = $(xmldoc).find("Table");
                var prodAutoId = $(product).find("ProductAutoId").text();
                var unitAutoId = $(product).find("UnitAutoId").text();
                var stock = $(product).find("Stock").text();
                var flag1 = true, qty = 0, pieces = 0;
                var countforscan = 0;
                if (product.length > 0) {
                    $("#tblProductDetail tbody tr").each(function () {
                        var IsExchange = 0, IsFreeItem = 0;;
                        if ($("#IsExchange").prop('checked')) {
                            IsExchange = 1;
                        }
                        if ($("#IsFreeItem").prop('checked')) {
                            IsFreeItem = 1;
                        }
                        if ($(this).find(".ProId span").attr("IsFreeItem") == IsFreeItem && $(this).find(".ProName span").attr("productautoid") == prodAutoId && $(this).find(".UnitType span").attr("unitautoid") == unitAutoId && IsExchange == $(this).find(".IsExchange span").attr("isexchange")) {
                            qty = Number($("#txtQty").val()) + Number($(this).find(".QtyShip input").val());
                            pieces = qty * Number($(this).find(".UnitType span").attr('qtyperunit'));
                            if (pieces <= stock && qty <= Number($(this).find(".ReqQty input").val())) {

                                $(this).find(".Barcode").text($("#txtScanBarcode").val());
                                if (qty == Number($(this).find(".ReqQty input").val())) {
                                    $("#ddlitemproduct option[value='" + prodAutoId + "']").remove();
                                    $(this).find(".QtyShip input").val(qty).attr("disabled", false).css("background-color", "#ECEFF1").removeClass("verify");
                                    countforscan = countforscan + 1;
                                } else {
                                    $(this).find(".QtyShip input").val(qty).attr("disabled", false).css("background-color", "#FFECB3").addClass("verify");
                                }
                                $(this).find(".QtyRemain").text(Number($(this).find(".QtyRemain").text()) - qty);
                                rowCal($(this).find(".QtyShip input"));

                                var trfind = $(this);
                                $('#tblProductDetail tbody tr:first').before(trfind);
                                $("#txtScanBarcode").val("").focus();

                            } else {
                                if (pieces > stock) {
                                    $("#barcodemsg").html("Error ! Qty is not avaiable in stock");
                                    $("#BarCodenotExists").modal("show");
                                    $("#txtScanBarcode").val("");
                                    $("#txtScanBarcode").blur();
                                }

                                if (qty > Number($(this).find(".ReqQty input").val())) {
                                    $("#barcodemsg").html("Error ! Shipped Qty cannot be more than Required Qty");
                                    $("#BarCodenotExists").modal("show");
                                    $("#txtScanBarcode").val("");
                                    $("#txtScanBarcode").blur();
                                }
                            }
                            flag1 = false;
                        }

                    });
                    if (countforscan > 1) {
                        $("#ddlitemproduct option[value='" + prodAutoId + "']").hide();
                    }
                    if (flag1) {
                        $("#barcodemsg").html("Error ! Wrong Item");
                        $("#BarCodenotExists").modal("show");
                        $("#txtScanBarcode").val("");
                        $("#txtScanBarcode").blur();
                        $("#No_audio")[0].play();
                    }
                } else {
                    $("#barcodemsg").html("Error ! Barcode does not exist");
                    $("#BarCodenotExists").modal("show");
                    $("#txtScanBarcode").val("");
                    $("#txtScanBarcode").blur();
                    $("#yes_audio")[0].play();
                }
                $("#IsExchange").prop('checked', false);
                $("#IsFreeItem").prop('checked', false);

                $("#txtQty").val("1");
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*-------------------------------------------------------------------------------------------------------------------*/

function GenBar() {
    getPackerAssignPrintData($("#ddlShippingType").val());
}

function getPackerAssignPrintData(ShippingAutoId) {

    var data = {
        ShippingAutoId: ShippingAutoId
    };
    $.ajax({
        type: "POST",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/getPackerAssignPrintData",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {

            var xmldoc = $.parseXML(result.d);
            var packerPrint = $(xmldoc).find('Table');
            var packerPrintHtml = ``;
            if (packerPrint.length > 0) {
                if (packerPrint.length == 1) {
                    $.each(packerPrint, function () {
                        var Url = $(this).find("Url").text();
                        var finalUrl = Url + "?OrderId=" + $("#txtOrderId").val();
                        window.open(finalUrl, "popUpWindow", "height=400,width=400,left=10,top=10,,scrollbars=yes,menubar=no");
                    });

                }
                else {
                    var i = 1;

                    $.each(packerPrint, function () {
                        if (i == 1) {
                            var checkStatus = "checked";
                        }
                        packerPrintHtml += `<div class="clearfix"></div>
                        <div class="row form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="Templ` + i + `" value="` + $(this).find("Url").text() + `" class="printTemplateClass" ` + checkStatus + ` />
                                `+ $(this).find("PrintTemplate").text() + `
                            </div>
                        </div>`;

                        i++;

                    });
                    $('#PopPrintBarcodeTemplate').modal('show');
                    $("#packerAssignPrint").html(packerPrintHtml);
                }

            } else {
                toastr.error('Template not found', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            }


        },
        error: function (result) {

        }

    });
}
function PrintBarcodeByTemplate() {
    if ($('input.printTemplateClass:checked').length == 0) {
        toastr.error('Please select template .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else {
        var Url = $('input.printTemplateClass:checked').val();
        var finalUrl = Url + "?OrderId=" + $("#txtOrderId").val();
        window.open(finalUrl, "popUpWindow", "height=400,width=400,left=10,top=10,,scrollbars=yes,menubar=no");
    }
}
function editPackedOrder() {
    debugger
    $("#panelProduct").show().find("input").attr("disabled", false).end()
        .find("select").attr("disabled", false);

    if ($("#hiddenStatusCode").val() < 3) {
        $("#tblProductDetail tbody tr").find(".QtyShip input").attr("disabled", true).end()
            .find(".UnitPrice input").attr("disabled", false).end()
            .find(".ReqQty input").attr("disabled", false);
    } else {

        $("#tblProductDetail tbody tr").find(".QtyShip input").attr("disabled", true).end()
            .find(".UnitPrice input").attr("disabled", false).end();

    }
    $("#txtStoreCreditAmount").attr('disabled', true);
    $("#tblProductDetail").find(".Action").show();
    $("#txtOverallDisc").attr("disabled", false);
    $("#txtDiscAmt").attr("disabled", false);

    $("#txtOverallDisc").removeAttr("readonly");
    $("#txtDiscAmt").removeAttr("readonly");

    $("#txtShipping").attr("disabled", false);
    $("#txtPackedBoxes").attr("disabled", false);
    $('#txtManagerRemarks').attr('disabled', false);
    $("#popBillAddr").show();
    $("#popShipAddr").show();
    $("#btnBackOrder").show();
    $("#btnAsgnDrv").show();
    $("#btnEditOrder").hide();
    $("#btnAssignPacker").hide();
    $("#btnGenBar").show();
    $("#btnGenOrderCC").show();
    $("#btnUpdateOrder").show();
    $("#btnSetAsProcess").show();
    $(".rowspan").show();
    $("#btnCancelOrder").show();
    $("#tblProductDetail tbody tr").each(function () {
        if ($(this).find('.IsExchange span').attr('IsExchange') == '1' || $(this).find('.ProId span').attr('isFreeItem') == '1') {
            $(this).find('.UnitPrice input').attr('disabled', true);
        }
    });
    if ($("#hiddenEmpTypeVal").val() == 8 || $("#hiddenEmpTypeVal").val() == 7) {
        $('#tblProductDetail tbody tr').find('.QtyShip input').attr('disabled', true);
    }
    if ($("#hiddenEmpTypeVal").val() == 7) {
        $('#freeItem').show();
        $('.SRP').hide();
        $('.UnitPrice').hide();
        $('.GP').hide();
        $('.TaxRate').hide();
        $('.NetPrice').hide();
        $('.Action> a').hide();
        $('#orderSummary').hide();
        $("#txtPackedBoxes").attr('disabled', true);
        $("#chkIsTaxable").attr('disabled', true);
    } else {
        $('#freeItem').hide();
    }

    if ($("#hiddenEmpTypeVal").val() == 7) {
        $("#btnCancelOrder").hide();
        $("#btnAsgnDrv").hide();
    }
}

/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                          Get Drivers Names
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getDriverList() {
    debugger
    if (parseFloat($("#txtOverallDisc").val()) > 100 && parseFloat($("#txtPaybleAmount").val())<0) {
        toastr.error('Please manage discount before assign driver.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    $.ajax({
        type: "Post",
        url: "/Manager/ManagerOrder.aspx/getDriverList",
        data: "{'OrderAutoId':'" + $("#txtHOrderAutoId").val() + "','LoginEmpType':'5'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var Drivers = $(xmldoc).find("Table");
                var selectDrv = $(xmldoc).find("Table1");
                $("#modalMisc").modal("toggle").find(".modal-dialog").removeClass("modal-sm").end()
                    .find(".modal-title").text("Drivers").end()
                    .find("#tblAssignDrv").show().end()
                    .find("#btnAsgn").show().end()
                    .find("#btnPrint").hide()
                    .find('#insideText').hide();
                $('#DriverText').show();
                $('#insideText').text('');
                $('#btnOk').hide();
                $("#tbldriver tbody tr").remove();
                var row = $("#tbldriver thead tr").clone(true);
                var picker = $('#txtAssignDate').pickadate('picker')
                picker.set('select', new Date($(selectDrv).find("AssignDate").text()));
                $("#txtAssignDate").attr("disabled", false);
                $.each(Drivers, function () {
                    $(".DrvName", row).html("<span drvautoid =" + $(this).find("AutoId").text() + ">" + $(this).find("Name").text() + "</span>");
                    $(".AsgnOrders", row).html($(this).find("AssignOrders").text());
                    if ($(this).find("AutoId").text() == $(selectDrv).find("Driver").text()) {
                        $(".Select", row).html("<input type='radio' name='driver' class='radio' onchange = 'changeDriver(this)' checked='checked'>");
                        $("#btnAsgn").attr("disabled", true);
                    } else {
                        $(".Select", row).html("<input type='radio' name='driver' class='radio' onchange = 'changeDriver(this)' >");
                    }
                    $("#tbldriver tbody").append(row);

                    row = $("#tbldriver tbody tr:last").clone(true);
                });
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function changeDriver(e) {
    var btnAsgn = $(e).closest(".modal-dialog").find("#btnAsgn");

    $(btnAsgn).attr("disabled", false);
}
/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                         Assign Order to Driver
/*-------------------------------------------------------------------------------------------------------------------------------*/
function assignDriver() {
    var drvAutoId, flag = true;
    $("#tblAssignDrv tbody tr").each(function () {
        if ($(this).find("input[name='driver']").is(":checked")) {
            drvAutoId = $(this).find(".DrvName span").attr("drvautoid");
            flag = false;
        }
    });

    var data = {
        DrvAutoId: drvAutoId,
        OrderAutoId: $("#txtHOrderAutoId").val(),
        AssignDate: $("#txtAssignDate").val()
    };

    if (!flag) {
        $.ajax({
            type: "Post",
            url: "/Manager/ManagerOrder.aspx/AssignDriver",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == 'Some Product have unit price less than min Price. Please fix it and try again.') {
                        swal("", response.d, "error");
                    } else {
                        swal("", "Driver assigned successfully", "success"
                        ).then(function () {
                            location.href = '/Manager/Manager_viewOrder.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                        });

                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        $("#alertDangMisc").html("<strong>Error : No Driver selected !</strong>").show();
        setTimeout(function () {
            $("#alertDangMisc").hide();
        }, 3000);
    }
}
/*-------------------------------REA------------------------------------------------------------------------------------------------*/
//                                                         Print Order Customer Copy
/*-------------------------------------------------------------------------------------------------------------------------------*/
function printOrder_CustCopy() {
    $("#SavedMassege").modal('hide');
    if ($('#HDDomain').val() == 'psmpa') {
        $("#PSMPADefault").show();
        $("#rPSMPADefault").prop('checked', true);
    }
    else if ($('#HDDomain').val() == 'psmwpa') {
        $("#PSMWPADefault").show();
        $("#rPSMWPADefault").prop('checked', true);
    }
    else if ($('#HDDomain').val() == 'psmnpa') {//PSMNPA Default changed on 11/01/2019
        $("#divPSMNPADefault").show();
        $("#PSMPADefault").hide();
        $("#rCheckNPADefault").prop('checked', true);
    }
    else {
        $("#chkdueamount").prop('checked', true);
        $("#PSMPADefault").closest('.row').hide();
    }
    $('#PopPrintTemplate').modal('show');
    $("#SavedMassege").modal('hide');
}
function PrintOrder() {

    $('#PopPrintTemplate').modal('hide');
    if ($("#chkdueamount").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF1.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#chkdefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCC.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template1").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template2").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF3.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#rPSMPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#rPSMWPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF6.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PackingSlip").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF5.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#WithoutCategorybreakdown").prop('checked') == true) {
        window.open("/Manager/WithoutCategorybreakdown.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#rCheckNPADefault").prop('checked') == true) { //PSMNPA Default changed on 11/01/2019
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#printOrderItemList").prop('checked') == true) {
        window.open("/Manager/PrintOrderItem.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ1").prop('checked') == true) {
        window.open("/Manager/PrintOrderItem.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ2").prop('checked') == true) {
        window.open("/Manager/PrintOrderItemTemplate2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PSMWPANDefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCFPN.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    closePrintPop();
}

function closePrintPop() {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    getid = getQueryString('OrderNo');
    if (getid == null) {
        location.href = '/Sales/orderMaster.aspx';
    }

}
/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                          Update Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
$("#btnUpdateOrder").click(function () {
    if ($("#hiddenEmpTypeVal").val() == '8') {
        if ($('#txtPackedBoxes').val() == '') {
            toastr.error('No. of Packed Boxes is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            $('#txtPackedBoxes').addClass('border-warning');
            return;
        }
    }
    var flag1 = false, flag2 = false, chkmin = false, chkmin2 = false, CheckTax = false;
    var CreditNo = '';
    $("#tblCreditMemoList tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            CreditNo += $(this).find('.Action input').val() + ',';
        }
    })
    if ($("#tblProductDetail tbody tr").length > 0) {
        $("#tblProductDetail tbody tr").each(function () {
            if ($(this).find(".ReqQty input").val() == "" || $(this).find(".ReqQty input").val() == null || Number($(this).find(".ReqQty input").val())=='0') {
                $(this).find(".ReqQty input").focus().addClass("border-warning");
                flag1 = true;
            } else {
                $(this).find(".ReqQty input").blur().removeClass("border-warning");
            }

            if ($("#hiddenEmpTypeVal").val() != '7') {
                if (($(this).find('.ProId span').attr('IsFreeItem') == 0 && $(this).find('.IsExchange span').attr("isexchange") != 1) && Number($(this).find(".UnitPrice input").val()) < Number($(this).find(".UnitPrice input").attr('minprice'))) {
                    flag2 = true;
                    $(this).find(".UnitPrice input").addClass('border-warning');
                }
            }
            if ($(this).find('.TaxRate span').attr('typetax') == '1' && $("#ddlCustomerType").val() != '3') {
                if (!$("#ddlTaxType").val()) {
                    CheckTax = true;
                }
            }
            if ($(this).find('.Discount input').val() == "") {
                $(this).find('.Discount input').addClass('border-warning');
                chkmin = true;
            }
            var minprice = $(this).find('.UnitPrice input').attr('minprice');
            disc = parseFloat($(this).find('.Discount input').val()).toFixed(2);
            if (disc > 0) {

                var unitprice = parseFloat($(this).find('.UnitPrice input').val()) || 0;
                var discountamount = parseFloat(unitprice - unitprice * disc / 100);
                if (discountamount < minprice) {
                    $(this).find(".Discount input").addClass('border-warning');
                    chkmin2 = true;
                }
            }
        });

        if (chkmin) {
            toastr.error('The discount amount cannot be empty.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        if (chkmin2) {
            toastr.error("Unit Price  can't be less than Min price.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            return;
        }
        if (CheckTax == true) {
            toastr.error('Tax is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
        else if (!flag1 && !flag2) {
            var Product = [];

            $("#tblProductDetail tbody tr").each(function () {
                Product.push({
                    'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
                    'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
                    'QtyPerUnit': $(this).find('.UnitType').find('span').attr('QtyPerUnit'),
                    'RequiredQty': $(this).find('.ReqQty input').val(),
                    'Barcode': $(this).find('.Barcode').text(),
                    'QtyShip': $(this).find('.QtyShip input').val(),
                    'TotalPieces': $(this).find('.TtlPcs').text(),
                    'UnitPrice': $(this).find('.UnitPrice input').val(),
                    'SRP': $(this).find('.SRP').text(),
                    'GP': $(this).find('.GP').text(),
                    'Tax': $(this).find('.TaxRate span').attr('typetax'),
                    'IsExchange': $(this).find('.IsExchange span').attr('IsExchange'),
                    'IsFreeItem': $(this).find('.ProId span').attr('IsFreeItem'),
                    'NetPrice': $(this).find('.NetPrice').text(),
                    'ItemType': $(this).find('.ItemType').text(),
                    'AutoId': $(this).find('.AutoId').text(),
                    'OM_MinPrice': $(this).find('.OM_MinPrice').text(),
                    'OM_CostPrice': $(this).find('.OM_CostPrice').text(),
                    'OM_BasePrice': $(this).find('.OM_BasePrice').text(),
                    'OM_Discount': $(this).find('.Discount input').val()

                });
            });
            var orderData = {
                OrderAutoId: $("#txtHOrderAutoId").val(),
                Status: $("#hiddenStatusCode").val(),
                OrderDate: $("#txtOrderDate").val(),
                DeliveryDate: $("#txtDeliveryDate").val(),
                CustomerAutoId: $("#ddlCustomer").val(),
                Remarks: $("#txtOrderRemarks").val().trim(),
                BillAddrAutoId: $("#hiddenBillAddrAutoId").val(),
                ShipAddrAutoId: $("#hiddenShipAddrAutoId").val(),
                OverallDiscAmt: $("#txtDiscAmt").val().trim() || 0,
                ShippingCharges: parseFloat($("#txtShipping").val().trim()) || 0,
                PackedBoxes: $("#txtPackedBoxes").val().trim() || 0,
                ShippingType: parseInt($("#ddlShippingType").val()),
                CreditNo: CreditNo,
                CreditAmount: $("#txtStoreCreditAmount").val().trim(),
                DeductionAmount: $("#txtDeductionAmount").val().trim(),
                ManagerRemarks: $("#txtManagerRemarks").val().trim(),
                TaxType: parseFloat($("#ddlTaxType").val())
            };
            $.ajax({
                type: "Post",
                url: "/Manager/ManagerOrder.aspx/updateOrder",
                data: JSON.stringify({ TableValues: JSON.stringify(Product), orderData: JSON.stringify(orderData) }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d == 'true') {
                            if ($("#hiddenEmpTypeVal").val() == '8') {
                                swal("", "Order updated successfully.", "success").then(function () {
                                    location.href = '/Manager/Manager_viewOrder.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                                });
                            } else {
                                swal("", "Order updated successfully.", "success").then(
                                    function () {
                                        location.href = '/Manager/Manager_viewOrder.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                                    }
                                );
                            }
                        } else {
                            swal("", response.d, "error");
                        }

                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                },
                failure: function (result) {
                }
            });
        } else {
            if (flag1) {

                toastr.error('Required Quantity cannot be left empty.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            } else {

                toastr.error('Unit Price can not be less than Min. Price.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
    } else {

        toastr.error('No Product Added in the List', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }

});
function ClosePop() {
    $('#SecurityEnvalid').modal('hide');
    if ($("#hiddenEmpTypeVal").val() == '8') {
        location.href = '/Manager/Manager_viewOrder.aspx?OrderAutoId=' + $('#txtHOrderAutoId').val();
    }
}
$("#btnReset").click(function () {
    window.location.href = "/Sales/orderMaster.aspx";
})

function resetAfterOrderGenerate() {
    $("input[type='text']").val('');
    $("select option").val(0);
    $('#txtOrderDate').val((new Date()).format("MM/dd/yyyy"));
    bindDropdown();
    $("#tblProductDetail tbody tr").remove();
    $("#ddlProduct").val(0);
    $("#emptyTable").show();
    $("#panelOrderContent input[type='text']").val("0.00");
}
/*-------------------------------------------------------------------------------------------------------------------*/
//                                                  Get Address List
/*-------------------------------------------------------------------------------------------------------------------*/
var AddrType; //----Global Variable For Address Type(Value is '11' for Billing Addr and '22' for Shipping Addr)

function getAddressList(e) {
    resetAddNewAddr();
    if (e == '11' || e == '22') {
        AddrType = e;
    } else {
        AddrType = $(e).closest("small").attr("AddrType");
    }

    var data = {
        AddrType: AddrType,
        CustomerAutoId: $("#ddlCustomer").val()
    }

    $.ajax({
        type: "Post",
        url: "/Manager/ManagerOrder.aspx/getAddressList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var AddressList = $(xmldoc).find("Table");

                if (data.AddrType == "11") {
                    $("#modalTitle").text("Billing Address List");
                } else {
                    $("#modalTitle").text("Shipping Address List");
                }
                $("#tblChangeAdd tbody tr").remove();
                var row = $("#tblChangeAdd thead tr").clone(true);
                $.each(AddressList, function () {
                    $("td", row).eq(0).html("<input type='radio' class='radio-inline' name='defaultAddr' onchange='changeAddr(" + $(this).find("AutoId").text() + ")'><span AddressAutoId='" + $(this).find("AutoId").text() + "'></span>");
                    if ($(this).find("IsDefault").text() == '1') {
                        $("td", row).eq(0).find("input[type='radio']").attr("checked", true);
                    }
                    $("td", row).eq(2).html($(this).find("CustomerName").text());
                    $("td", row).eq(3).html($(this).find("Address").text());
                    $("td", row).eq(4).html($(this).find("State").text());
                    $("td", row).eq(5).text($(this).find("City").text());
                    $("td", row).eq(6).text($(this).find("Zipcode").text());
                    if ($(this).find("IsDefault").text() == '1') {
                        $("td", row).eq(1).html("-");
                    }
                    else {
                        $("td", row).eq(1).html("<a href='javascript:;' onclick='deleterecord(" + $(this).find("AutoId").text() + ")'><span class='ft-x'></span></a>");
                    }
                    $("#tblChangeAdd tbody").append(row);
                    row = $("#tblChangeAdd tbody tr:last").clone(true);
                });
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function changeAddr(addrAutoId) {
    var data = {
        AddressAutoId: addrAutoId,
        CustomerAutoId: $("#ddlCustomer").val(),
        AddrType: AddrType
    }

    $.ajax({
        type: "POST",
        url: "/Manager/ManagerOrder.aspx/changeAddr",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                getAddressList(AddrType);
                $("#alertSChangeAddr").show();
                $("#alertSChangeAddr span").text("Default Address has been Changed !!!");
                $(".close").click(function () {
                    $("#alertSChangeAddr").hide();
                });
            } else {
                location.href = '/';
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function addNewAddr() {
    $("#panelAddNewAddr").show();
    $('#txtAddr').prop("disabled", false);
    $('#txtAddr2').prop("disabled", false);
}

function saveAddr() {

    $("#txtAddr").removeClass("border-warning");
    $("#ddlZipCode").closest('div').find('.select2-selection--single').removeAttr('style');

    if ($("#ddlCustomer").val() != '0') {
        if ($("#txtAddr").val() != "") {
            if ($("#ddlZipCode").val() != '0') {
                var data = {
                    OrderNo: $("#txtOrderId").val(),
                    CustomerAutoId: $("#ddlCustomer").val(),
                    AddrType: AddrType,
                    Address: $("#txtAddr").val(),
                    Address2: $("#txtAddr2").val(),
                    Zipcode: $("#txtZipcode").val(),
                    City: $("#txtCity1").val(),
                    State: $("#txtState1").val(),
                    Lat: $("#txtLat").val(),
                    Long: $("#txtLong").val(),
                }

                $.ajax({
                    type: "Post",
                    url: "/Manager/ManagerOrder.aspx/saveAddr",
                    data: "{'dataValues':'" + JSON.stringify(data) + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (data) {
                        if (data.d == 'AddrExist') {
                            swal("", "Duplicate Address. Address is already exist.", "warning");
                            return;
                        }
                        if (data.d != "Session Expired") {
                            swal("", "Customer address saved successfully.", "success");
                            getAddressList(AddrType);
                            bindAddress();
                            resetAddNewAddr();
                        } else {
                            location.href = '/';
                        }

                    },
                    error: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    },
                    failure: function (result) {
                        console.log(JSON.parse(result.responseText).d);
                    }
                });
            }
            else {
                $("#ddlZipCode").closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
                toastr.error('All * fields are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
            }
        }
        else {
            if ($("#ddlZipCode").val() == '0') {
                $("#ddlZipCode").closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
            }

            $("#txtAddr").addClass("border-warning");
            toastr.error('All * fields are mandatory', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        }
    }
    else {

        $("#ddlCustomer").closest('div').attr('style', 'border:1px solid #FF9149  !important');
        toastr.error('Please enter customer.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}

function deleterecord(AddressAutoId) {
    swal({
        title: "Are you sure?",
        text: "You want to delete customer address.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, Delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleteAddr(AddressAutoId);

        } else {
            swal("", "Customer address is safe.", "error");
        }
    })
}

function deleteAddr(AddressAutoId) {
    var data = {
        AddrType: AddrType,
        AddressAutoId: AddressAutoId
    }
    $.ajax({
        type: "Post",
        url: "/Manager/ManagerOrder.aspx/deleteAddr",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (data) {

            if (data.d != "Session Expired") {
                if (data.d == "Success") {
                    resetAddNewAddr();
                    swal("", " Customer address successfully deleted.", "success");
                    getAddressList(AddrType);
                }
                else {
                    swal("", "Customer address can't deleted.", "error");
                }

            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
            $("#alertDChangeAddr").show();
            $("#alertDChangeAddr span").text("This Address is set as Default Address. Select another Address as Default then Try Again.");
            $(".close").click(function () {
                $("#alertDChangeAddr").hide();
            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function resetAddNewAddr() {
    $("#panelAddNewAddr input[type='text']").val("");
    $("#panelAddNewAddr textarea").val("");
    $("#ddlZipCode").val(0).change();
    $('#txtAddr').removeClass('border-warning');
    $("#ddlZipCode").closest('div').find('.select2-selection--single').removeAttr('style');
}

function modalClose() {
    $("#panelAddNewAddr").hide();
    $(".alert").hide();
    bindAddress();
}


function confirm_Cancellation_Of_Order() {
    $("#OrderBarcode").modal("toggle");
    $('#txtCancellationRemarks').val('');
    $('#txtCancellationRemarks').removeAttr('disabled');
    $('#txtCancellationRemarks').css('border-color', 'none');
}

function cancelOrder() {
    if ($('#txtCancellationRemarks').val() == '') {
        $('#txtCancellationRemarks').css('border-color', 'red');
        $('#txtCancellationRemarks').focus();
        return;
    }
    $.ajax({
        type: "Post",
        url: "/Manager/ManagerOrder.aspx/cancelOrder",
        data: "{'OrderAutoId':'" + $("#txtHOrderAutoId").val() + "','Remarks':'" + $('#txtCancellationRemarks').val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var status = $(xmldoc).find("Table");
                $("#txtOrderStatus").val($(status).find("StatusType").text());
                $("#hiddenOrderStatus").val($(status).find("Status").text());
                $("#btnEditOrder").hide();
                $("#btnCancelOrder").hide();
                $("#btnAsgnDrv").hide();
                $("#OrderBarcode").modal("toggle");
                //swal("", " Order has been cancelled successfully.", "success");
                //editOrder(getid);
                swal("", "Order has been cancelled successfully.", "success").then(function () {
                    location.href = '/Manager/Manager_viewOrder.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                });

            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function printInv() {
    window.open('/Manager/generateInvoice.html', '_blank');
}
//-------------------------------------------------- NEW DEVELOPMENT DTD 28 JULY 2017 ONWARDS ---------------------------------------------------//

function ShowTop25SellingProducts() {
    $.ajax({
        type: "POST",
        url: "/Manager/ManagerOrder.aspx/ShowTop25SellingProducts",
        data: "{'customerAutoId':" + parseInt($("#ddlCustomer").val()) + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var topProducts = $(xmldoc).find("Table");

                $("#tblTop25Products tbody tr").remove();
                var row = $("#tblTop25Products thead tr").clone(true);

                if (topProducts.length > 0) {
                    $("#emptyTable2").hide();
                    $.each(topProducts, function (index) {
                        $(".SrNo", row).text(index + 1);
                        $(".ProductId", row).html("<span productautoid='" + $(this).find("ProductAutoId").text() + "'> <a href='javascript:void(0)' onclick='getProductQty(" + $(this).find("ProductAutoId").text() + ")'>" + $(this).find("ProductId").text() + "</a></span>");
                        $(".ProductName", row).text($(this).find("ProductName").text());
                        $(".QtyInStock", row).text($(this).find("Stock").text());

                        $('#tblTop25Products').find("tbody").append(row);
                        row = $("#tblTop25Products tbody tr:last").clone(true);
                    });
                } else {
                    $("#emptyTable2").show();
                }

                if ($("#hiddenStatusCode").val() == 1) {
                    $("#Top25Products").hide();
                }
                else {
                    $("#Top25Products").show();
                }


            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

/*---------------------------------------------------------------------------------------------------------------------------------------*/
//                                                        Barcode Reading
/*---------------------------------------------------------------------------------------------------------------------------------------*/
var BUnitAutoId = 0;
function readBarcode() {
    var Barcode = $("#txtBarcode").val();
    if (Barcode != "") {
        var chkIsTaxable = $('#chkIsTaxable').prop('checked');
        var chkExchange = $('#chkExchange').prop('checked');
        var chkFreeItem = $('#chkFreeItem').prop('checked');
        var IsExchange = 0, IsTaxable = 0, IsFreeItem = 0;
        if (chkIsTaxable == true) {
            IsTaxable = 1;
        }
        if (chkExchange == true) {
            IsExchange = 1;
        }
        if (chkFreeItem == true) {
            IsFreeItem = 1;

        }
        var data = {
            CustomerAutoId: $("#ddlCustomer").val(),
            Barcode: Barcode,
            ShippingType: $("#ddlShippingType").val(),
            DeliveryDate: $("#txtDeliveryDate").val(),
            ReqQty: '1',
            IsTaxable: IsTaxable,
            IsExchange: IsExchange,
            IsFreeItem: IsFreeItem
        }
        $.ajax({
            type: "POST",
            url: "/Manager/ManagerOrder.aspx/GetBarDetails",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                var xmldoc = $.parseXML(response.d);
                var product = $(xmldoc).find("Table");
                if (product.length > 0) {
                    var productAutoId = $(product).find('ProductAutoId').text();
                    var unitAutoId = $(product).find('UnitType').text();
                    $("#DraftAutoId").val($(product).find('DraftAutoId').text());
                    var flag1 = false;
                    var tr = $("#tblProductDetail tbody tr");
                    for (var i = 0; i <= tr.length - 1; i++) {
                        if ($(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem && $(tr[i]).find(".ProName > span").attr("productautoid") == productAutoId &&
                            $(tr[i]).find(".UnitType > span").attr("unitautoid") == unitAutoId && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange) {
                            var reqQty = Number($(tr[i]).find(".ReqQty input").val()) + 1;
                            $(tr[i]).find(".ReqQty input").val(reqQty);
                            flag1 = true;
                            rowCal($(tr[i]).find(".ReqQty > input"));
                            $('#tblProductDetail tbody tr:first').before($(tr[i]));
                            if (chkIsTaxable == true && ($(tr[i]).find(".TaxRate > span").attr("TypeTax")) == "0") {
                                toastr.success('Product has been added successfully but product has been not set taxable, now product cannot be set taxable .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                clear();
                                return;
                            }
                            else if (chkIsTaxable == false && ($(tr[i]).find(".TaxRate > span").attr("TypeTax")) == "1") {
                                toastr.success('Product has been added succesfully but product has already taxable, so now product has been added with taxable .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                clear();
                                return;
                            }
                            else {
                                toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                clear();
                                return;
                            }
                        }

                        if (chkFreeItem) {
                            if (($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == '0') && $(tr[i]).find(".UnitType > span").attr("unitautoid") != unitAutoId) {
                                flag1 = true;
                                toastr.error("You can't add different unit of added product.", 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                clear();
                                return;

                            }
                        }
                        if (chkExchange) {
                            if ($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId && $(tr[i]).find(".UnitType > span").attr("unitautoid") != unitAutoId && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == '0' && $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange) {
                                flag1 = true;
                                toastr.error("You can't add different unit of added product.", 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                clear();
                                return;
                            }
                        }

                        if ($(tr[i]).find(".ProName span").attr("productautoid") == productAutoId &&
                            $(tr[i]).find(".IsExchange > span").attr("IsExchange") == IsExchange
                            && $(tr[i]).find(".ProId > span").attr("IsFreeItem") == IsFreeItem) {
                            flag1 = true;
                            toastr.error("You can't add different unit of added product.", 'Worning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                            clear();
                            return;
                        }
                    }

                    if (!flag1) {
                        var row = $("#tblProductDetail thead tr").clone(true);
                        $(".ProId", row).html($(product).find('ProductId').text() + "<span IsFreeItem='" + IsFreeItem + "'></span>");

                        if (IsFreeItem == 1) {
                            $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-success'>Free</product>");
                        }
                        else if (IsExchange == 1) {
                            $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-primary'>Exchange</product>");
                        }
                        else if (IsTaxable == 1) {
                            $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>" + " <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                        }
                        else {
                            $(".ProName", row).html("<span ProductAutoId='" + productAutoId + "'>" + $(product).find('ProductName').text() + "</span>");
                        }
                        $(".UnitType", row).html("<span UnitAutoId='" + unitAutoId + "' QtyPerUnit='" + $(product).find('UnitQty').text() + "'>" + $(product).find('UnitName').text() + "</span>");
                        $(".ReqQty", row).html("<input type='text' maxlength='4' onkeypress='return isNumberKey(event)' class='form-control input-sm border-primary text-center' runat='server' onkeyup='rowCal(this)' value='1'/>");
                        $(".Barcode", row).text("");
                        $(".Discount", row).html("<input type='text' class='form-control input-sm border-primary' onkeypress='return isNumberDecimalKey(event,this)' style='text-align:right;width:70px;' maxlength='6' value='0.00' onkeyup='checkdiscount(this)'/>");
                        $(".QtyShip", row).html("<input type='text' class='form-control input-sm text-center' onkeypress='return isNumberKey(event)' style='width:70px;' disabled value='0' onkeyup='rowCal(this)' />");
                        $(".TtlPcs", row).text(parseInt($("#txtReqQty").val()) * $(product).find('UnitQty').text());
                        if ($('#chkExchange').prop('checked') == false && $('#chkFreeItem').prop('checked') == false) {
                            if ($(product).find('CustomPrice').text() == "") {
                                $(".UnitPrice", row).html("<input type='text' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='" + $(product).find('Price').text() + "' minprice='" + $(product).find('MinPrice').text() + "' BasePrice='" + $(product).find('Price').text() + "' />");
                            } else {
                                $(".UnitPrice", row).html("<input type='text' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='" + (parseFloat($(product).find('CustomPrice').text()) >= parseFloat($(product).find('MinPrice').text()) ? $(product).find('CustomPrice').text() : $(product).find('Price').text()) + "' minprice='" + $(product).find('MinPrice').text() + "' BasePrice='" + $(product).find('Price').text() + "' />");
                            }
                        } else {
                            $(".UnitPrice", row).html("<input type='text' onkeypress='return isNumberDecimalKey(event,this)' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:70px;text-align:right;' value='0.00' minprice='0.00' BasePrice='0.00' disabled/>");
                        }
                        $(".SRP", row).text($(product).find('SRP').text());
                        $(".GP", row).text($(product).find('GP').text());
                        var taxType = '', TypeTax = 0;
                        if ($('#chkIsTaxable').prop('checked') == true) {
                            taxType = 'Taxable';
                            TypeTax = 1;
                        }
                        var IsExchange1 = '';
                        if ($('#chkExchange').prop('checked') == true) {
                            IsExchange1 = 'Exchange';
                        }
                        if (TypeTax == 1) {
                            $(".TaxRate", row).html("<span TypeTax='" + TypeTax + "' MLQty='" + $(product).find('MLQty').text() + "' WeightOz='" + $(product).find('WeightOz').text() + "'> </span>" + '<TaxRate style="font-weight: bold" class="la la-check-circle success center"></TaxRate>');
                        } else {
                            $(".TaxRate", row).html("<span TypeTax='" + TypeTax + "' MLQty='" + $(product).find('MLQty').text() + "' WeightOz='" + $(product).find('WeightOz').text() + "'> </span>");
                        }
                        if (IsExchange == 1) {
                            $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>" + '<IsExchange style="font-weight: bold" class="la la-check-circle success center"></IsExchange>');
                        } else {
                            $(".IsExchange", row).html("<span IsExchange='" + IsExchange + "'> </span>");
                        }

                        $(".NetPrice", row).text("0.00");
                        $(".ItemTotal", row).text("0.00");
                        $(".OM_MinPrice", row).text(parseFloat($(product).find('MinPrice').text()).toFixed(2));
                        $(".OM_CostPrice", row).text(parseFloat($(product).find('CostPrice').text()).toFixed(2));
                        $(".OM_BasePrice", row).text(parseFloat($(product).find('BasePrice').text()).toFixed(2));
                        $(".AutoId", row).text('0');
                        $(".ItemType", row).text('New');
                        $(".QtyRemain", row).text("0");
                        $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleteItemrecord(this)'><span class='ft-x'></span></a>");
                        if ($('#tblProductDetail tbody tr').length > 0) {
                            $('#tblProductDetail tbody tr:first').before(row);
                        }
                        else {
                            $('#tblProductDetail tbody').append(row);
                        }
                        rowCal(row.find(".ReqQty > input"));
                        $("#txtQuantity, #txtTotalPieces").val("0");
                        if ($('#tblProductDetail tbody tr').length > 0) {
                            $('#emptyTable').hide();
                            $("#ddlCustomer").attr('disabled', true);
                        }

                        toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                        $("#txtBarcode").val('');
                        $("#txtBarcode").focus();
                    }
                    else {
                        toastr.success('Product added successfully.', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    }
                    $("#txtBarcode").val('');
                    $("#txtBarcode").focus();

                } else {
                    swal({
                        title: "Error!",
                        text: "Barcode does not exists.",
                        icon: "error",
                        closeOnClickOutside: false
                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            $("#txtBarcode").val('');
                            $("#txtBarcode").focus();
                        }
                    });
                    $("#yes_audio")[0].play();
                    clear();
                }
                //$('#chkIsTaxable').prop('checked', false);
                //$('#chkExchange').prop('checked', false);
                //$('#chkFreeItem').prop('checked', false);

            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    } else {
        $("#ddlProduct").val(0).change();
        $("#alertBarcodeCount").hide();
        $("#ddlProduct").attr('disabled', false);
        $("#ddlUnitType").attr('disabled', false);
    }
}
function clear() {
    $("#panelProduct select").val(0);
    $("#txtQuantity, #txtTotalPieces").val("0");
    $("#alertBarcodeCount").hide();
    $("#txtBarcode").val('');
    $("#txtReqQty").focus();
    $("#txtBarcode").blur();
    $("#txtBarcode").val('');
    $("#txtBarcode").focus();
}

function getProductQty(AutoId) {
    $("#ddlProduct").val(AutoId).change();
}
function nocreateorder() {
    if (Number($("#hiddenEmpTypeVal").val()) == 1) {
        location.href = '/Sales/orderList.aspx';
    }
    else if (Number($("#hiddenEmpTypeVal").val()) == 7) {
        location.href = '/Sales/orderList.aspx';
    }
    else {
        location.href = '/Sales/orderList.aspx';
    }
}

function createorder() {
    location.href = '/Sales/orderMaster.aspx';
}
var NoofBoxRead = 0
function PickupOrderbyBarCode() {
    NoofBoxRead = 0;
    $("#txtReadBorCode").attr('disabled', false);
    $("#baxbarmsg").html('');
    $("#PopBarCodeforPickBox").modal('show');
    $("#btnPick1").hide();
    $("#lblOrderno").text($("#txtOrderId").val());
    $("#lblNoOfbox").text($("#txtPackedBoxes").val());
    $("#tblBarcode tbody tr").remove();
}

function readBoxBarcode() {
    var count = 0;
    var checkbarcode = true;
    $("#tblBarcode tbody tr").each(function () {
        count++;
    });

    if (count < $("#txtPackedBoxes").val()) {

        for (var i = 1; i <= $("#txtPackedBoxes").val(); i++) {
            if ($("#txtReadBorCode").val().toUpperCase() == ($("#txtOrderId").val().toUpperCase() + '/' + i)) {
                $("#tblBarcode tbody tr").each(function (index, item) {
                    if (($("#txtOrderId").val() + '/' + i) == $(item).find('.Barcode').text()) {
                        checkbarcode = false;
                    }
                });
                if (checkbarcode == true) {


                    NoofBoxRead = (Number(NoofBoxRead) + 1);
                    var PackedBoxes = $("#txtPackedBoxes").val();
                    var DuePackedBox = (Number(PackedBoxes) - Number(NoofBoxRead))

                    $("#baxbarmsg").css('color', 'Green');
                    $("#txtReadBorCode").val('');
                    $("#txtReadBorCode").focus();
                    if (Number(DuePackedBox) == 0) {
                        $("#txtReadBorCode").attr('disabled', true);
                        $("#baxbarmsg").html('All boxes picked.');
                        $("#btnPick1").show();
                        var row = $("#tblBarcode thead tr").clone(true);

                        $(".SRNO", row).html(NoofBoxRead);
                        $(".Barcode", row).text($("#txtOrderId").val() + '/' + i);
                        $("#tblBarcode tbody").append(row);
                        i = Number($("#txtPackedBoxes").val() + 1);

                    } else {
                        $("#baxbarmsg").html(DuePackedBox + ' Out Of ' + $("#txtPackedBoxes").val() + ' remain');
                        //$("#baxbarmsg").html('you still need ' + DuePackedBox + ' box left to pick up');
                        var row = $("#tblBarcode thead tr").clone(true);

                        $(".SRNO", row).html(NoofBoxRead);
                        $(".Barcode", row).text($("#txtOrderId").val() + '/' + i);
                        $("#tblBarcode tbody").append(row);
                        i = Number($("#txtPackedBoxes").val() + 1);
                    }
                }
                else {
                    $("#baxbarmsg").html('Bar Code already scan');
                    $("#btnPick1").hide();
                    $("#baxbarmsg").css('color', 'Red');
                    i = Number($("#txtPackedBoxes").val() + 1);
                }
            } else {
                $("#baxbarmsg").html('Invalid Bar Code');
                $("#btnPick1").hide();
                $("#baxbarmsg").css('color', 'Red');

            }
        }
    }

}

function viewOrderLog() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WOrderLog.asmx/viewOrderLog",
        data: "{'OrderAutoId':" + $("#txtHOrderAutoId").val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var orderlog = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");

            $("#Span1").text(order.find("OrderNo").text());
            $("#lblOrderDate").text(order.find("OrderDate").text());

            $("#tblOrderLog tbody tr").remove();
            var row = $("#tblOrderLog thead tr").clone(true);
            if (orderlog.length > 0) {
                $("#EmptyTable").hide();
                $.each(orderlog, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("ActionDate").text());
                    $(".Action", row).text($(this).find("Action").text());
                    $(".Remark", row).text($(this).find("Remarks").text());

                    $("#tblOrderLog tbody").append(row);
                    row = $("#tblOrderLog tbody tr:last").clone(true);
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}
function AssignedPacker() {
    $('#packerDiv').modal('show');
    getPackerList();
    $("#txtTimes").attr('disabled', false);
    $("#txtTimes").val($('#txtHTimes').val());
    $("#txtWarehouseRemark").attr('disabled', false);
    $("#txtWarehouseRemark").val($('#txtHWarehouseRemark').val());
}
function getPackerList() {

    $.ajax({
        type: "Post",
        url: "/Manager/ManagerOrder.aspx/getDriverList",
        data: "{'OrderAutoId':'" + $("#txtHOrderAutoId").val() + "','LoginEmpType':'3'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var Drivers = $(xmldoc).find("Table");
                var selectDrv = $(xmldoc).find("Table1");
                $("#packerList tbody tr").remove();
                var row = $("#packerList thead tr").clone(true);
                $.each(Drivers, function () {
                    $(".DrvName", row).html("<span pkdautoid =" + $(this).find("AutoId").text() + ">" + $(this).find("Name").text() + "</span>");
                    if ($(this).find("AutoId").text() == $(selectDrv).find("PackerAutoId").text()) {
                        $(".Select", row).html("<input type='radio' name='packer' class='radio' onchange = 'changePacker(this)' checked='checked'>");
                        $("#btnAsgn").attr("disabled", true);
                    } else {
                        $(".Select", row).html("<input type='radio' name='packer' class='radio' onchange = 'changePacker(this)' >");
                    }
                    $("#packerList tbody").append(row);

                    row = $("#packerList tbody tr:last").clone(true);
                });
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
//                                                         Assign Order to Driver
/*-------------------------------------------------------------------------------------------------------------------------------*/
function assignPacker() {
    var PackerAutoId, flag = true;
    $("#packerList tbody tr").each(function () {
        if ($(this).find("input[name='packer']").is(":checked")) {
            PackerAutoId = $(this).find(".DrvName span").attr("pkdautoid");
            flag = false;
        }
    });

    var data = {
        PackerAutoId: PackerAutoId,
        OrderAutoId: $("#txtHOrderAutoId").val(),
        Times: $("#txtTimes").val(),
        Remarks: $("#txtWarehouseRemark").val()
    };

    if (!flag) {
        $.ajax({
            type: "Post",
            url: "/Manager/ManagerOrder.aspx/assignPacker",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {

                    if (response.d == 'true') {
                        swal("", "Packer assigned successfully.", "success").then(function () {
                            $('#packerDiv').modal('hide');
                            editOrder(getid);
                            $('#btnAssignPacker').html('<b>Change Packer</b>');
                        });
                    } else {
                        swal("", response.d, "error")
                    }

                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                swal("", "Oops! Something went wrong. Please try later.", "error")
            },
            failure: function (result) {
                swal("", "Oops! Something went wrong. Please try later.", "error")
            }
        });
    } else {
        toastr.error('Error : No Packer selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function changePacker(e) {
    var btnAsgn = $(e).closest(".modal-dialog").find("#btnAsgn");
    $(btnAsgn).attr("disabled", false);
}
function SetAsProcess() {
    swal({
        title: "Are you sure?",
        text: "You want to set status as processed.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            AfterYesSetAsProcess();

        } else {
            swal("", "Your status is not change.", "error");
        }
    })
}

function AfterYesSetAsProcess() {

    $.ajax({
        type: "Post",
        url: "/Manager/ManagerOrder.aspx/SetAsProcess",
        data: "{'OrderAutoId':'" + $("#txtHOrderAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                swal("", "Order has been set as processed succesfully.", "success").then(function () {
                    window.location.href = '/Manager/ManagerOrder.aspx?OrderNo=' + $('#txtOrderId').val();
                });


            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}


function focusonBarcode() {
    $("#BarCodenotExists").modal('hide');
    if ($("#hiddenEmpTypeVal").val() == "2" || $("#hiddenEmpTypeVal").val() == "7" || $("#hiddenEmpTypeVal").val() == "8") {
        $("#txtBarcode").focus();
    } else if ($("#hiddenEmpTypeVal").val() == "3") {
        $("#txtScanBarcode").focus();
    }
}

function clickonSecurity() {

    $.ajax({
        type: "Post",
        url: "/Manager/ManagerOrder.aspx/clickonSecurity",
        data: "{'CheckSecurity':'" + $("#txtSecurity").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    PackerSecurityEnable = true;
                    $("#SecurityEnabled").modal('hide');
                    $('#btnpkd').removeAttr('disabled');
                } else {
                    $('#SecurityEnvalid').modal('show');
                    $('#Sbarcodemsg').html('Invalid Security Key.')
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}
function AddOnprint_NewOrder() {
    localStorage.setItem('checkprint', 2);
    window.open("/Packer/OrderPrint.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}


function ProductList(e) {
    var tr = $(e).closest('tr');
    var ProductAutoId = tr.find('.ProName span').attr('productautoid');
    $("#ProdId").html(tr.find('.ProId').text());
    $("#ProdName").html(tr.find('.ProName productname').html());
    $("#hfProductAutoId").val(ProductAutoId);
    ProductSalesHistory(1);
}

function ProductSalesHistory(PageIndex) {
    var data = {
        ProductAutoId: $("#hfProductAutoId").val(),
        CustomerAutoId: $("#ddlCustomer").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/WebAPI/WManager_OrderList.asmx/ProductSalesHistory",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {

            var xmldoc = $.parseXML(response.d);
            var product = $(xmldoc).find("Table1");
            var NetPrice = 0.00;
            if (product.length > 0) {
                $("#tblProductSalesHistory tbody tr").remove();
                var row = $("#tblProductSalesHistory thead tr:last").clone(true);
                $.each(product, function (index) {
                    $(".SRNO", row).text(Number(index) + 1);
                    $(".OrderDate", row).html($(this).find("OrderDate").text());
                    $(".OrderNo", row).text($(this).find("OrderNo").text());
                    if (Number($(this).find("isFreeItem").text()) == 1) {
                        $(".UnitType", row).html('<span>' + $(this).find("UnitType").text() + ' <a href="#" style="color:green" title="Free">*</a></span>');
                    }
                    else if (Number($(this).find("IsExchange").text()) == 1) {
                        $(".UnitType", row).html('<span>' + $(this).find("UnitType").text() + ' <a href="#"  style="color:green" title="Exchange">**</a></span>');
                    }
                    else {
                        $(".UnitType", row).text($(this).find("UnitType").text());
                    }
                    $(".QtyShip", row).text($(this).find("QtyShip").text());
                    $(".UnitPrice", row).text($(this).find("UnitPrice").text());
                    $(".TotalPrice", row).text($(this).find("TotalPrice").text());

                    if (Number($(this).find("StatusCode").text()) == 1) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_New'>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 2) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Processed'>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 3) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Packed'>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 4) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Ready_to_Ship '>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 5) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Shipped'>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 6) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Delivered'>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 7) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Undelivered'>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 8) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_cancelled' >" + $(this).find("StatusType").text() + "</span>");
                    }
                    else if (Number($(this).find("StatusCode").text()) == 9) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Add_On'>" + $(this).find("StatusType").text() + "</span>");
                    }
                    else if (Number($(this).find("StatusCode").text()) == 10) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Add_On_Packed'>" + $(this).find("StatusType").text() + "</span>");
                    }
                    else if (Number($(this).find("StatusCode").text()) == 11) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Close'>" + $(this).find("StatusType").text() + "</span>");
                    }

                    NetPrice += Number($(this).find("TotalPrice").text());

                    $("#tblProductSalesHistory tbody").append(row);
                    row = $("#tblProductSalesHistory tbody tr:last").clone(true);
                });
                $("#EmptyTable").hide();
                $("#tblProductSalesHistory").show();
                $("#ddlPageSize").show();
                $("#ddlPageSize").removeAttr('disabled');
            } else {
                $("#EmptyTable").show();
                $("#ddlPageSize").hide();
                $("#tblProductSalesHistory").hide();
            }
            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
            $("#TotalNetPrice").html(parseFloat(NetPrice).toFixed(2));
            var OverAll = $(xmldoc).find("Table2");
            $("#OverallNetPrice").html($(OverAll).find("OverALllTotalPrice").text());
            $("#modalProductSalesHistory").modal('show');
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function Pagevalue(e) {
    ProductSalesHistory(parseInt($(e).attr("page")));
};
function closeSalesHistory() {
    $("#ddlPageSize").val(10);
    $("#modalProductSalesHistory").modal('hide');
}