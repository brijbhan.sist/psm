﻿
var Print = 1, PrintLabel = '';
$(document).ready(function () {

    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    if (localStorage.getItem('BulkItemOrderAutoId') != null) {
        var getids = localStorage.getItem('BulkItemOrderAutoId');
        getOrderData(getids);
    }

});
//Item Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/WebAPI/WPrintOrder.asmx/PrintBulkOrderItemTemplate3",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
                return;
            }
            var topHtml = ``;
            var orderData = $.parseJSON(response.d);
            console.log(orderData);
            $.each(orderData[0].OrderDetails, function (index, order) {

                topHtml = `<div class="row">
        <center><span style="text-align:center;font-weight:bold;font-size:20px;">Sales Order</span></center>
    </div><fieldset>
                                <table class="table-borderless">
                                    <tbody>

                                        <tr>
                                            <td class="tdLabel" style="white-space: nowrap;width:5%;">Customer Name :-</td>
                                            <td id="CustomerName" style="white-space: nowrap;width:35%;">`+ order.CustomerName+`</td>
                                            <td class="tdLabel" style="white-space: nowrap;width:5%;">Order No :-</td>
                                            <td id="OrderNo" style="white-space: nowrap;width:15%;">`+ order.OrderNo+`</td>
                                        </tr>
                                        <tr>
                                            <td class="tdLabel" style="white-space: nowrap;width:5%;">Address :-</td>
                                            <td id="Address" style="white-space: nowrap;width:35%;">`+ order.ShipAddress + `</td>
                                             <td class="tdLabel" style="white-space: nowrap;width:5%;">Date :-</td>
                                            <td id="OrderNo" style="white-space: nowrap;width:15%;">04/10/2020</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </fieldset>`;

                topHtml += `<table class="table tableCSS tblProduct" id="tblProduct"><thead><tr>
                            <tr><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>
                            <td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td><td class="Qty" style="width: 10px">Qty</td>
                            <td class="UnitPrice" style="text-align: right; width: 10px">Price</td>
                            <td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>`;



                //var row = $("#tblProduct thead tr").clone(true);
                var catId, catName, productname = '', Qty = 0, total = 0.00, count = 1, tr, totalPrice = 0.00;
                var pageNo = 1, rowcount = 0, TotalMLTax = 0.00, TotalMLQty = 0.00;
                var orderQty = 0; var total = 0;
                $.each(order.Item2, function (index, item) {

                    orderQty = item.QtyShip;
                    if (parseInt(item.QtyShip) ==0) {
                        totalPrice = 0.00;
                    } else {
                        totalPrice = parseFloat(item.NetPrice);
                    }

                    rowcount = parseInt(rowcount) + 1;
                    if (item.IsExchange == 1) {
                        productname = item.ProductName + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Exchange)' + "</span>";
                    }
                    else if (item.isFreeItem == 1) {
                        productname = item.ProductName + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Free)' + "</span>";
                    }
                    else {
                        productname = item.ProductName;
                    }

                    total += totalPrice;

                    topHtml += `<tr>
                            <td>`+ item.ProductId + `</td>
                            <td>` + productname.replace(/&quot;/g, "\'") + `</td>
                            <td>` + item.UnitType + `</td>
                            <td>` + orderQty + `</td>
                            <td style='text-align: right; width: 10px; white-space: nowrap'>` + item.UnitPrice+ `</td>
                            <td style='text-align: right; width: 10px; white-space: nowrap'>` + totalPrice.toFixed(2) + `</td>
                            </tr>`;
                });

                topHtml += `</tbody><tfoot>
                        <tr>
                        <td colspan='2'></td><td colspan='3' style='text-align:center'><b>Sub Total</b></td><td style='text-align:right'>` + parseFloat(total).toFixed(2) + `</td></tr>`;
                if (order.OverallDiscAmt != 0) {
                    topHtml += `<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Discount</b></td><td style='text-align:right'>` + parseFloat(order.OverallDiscAmt).toFixed(2) + `</td></tr>`;
                }
                if (order.DeductionAmount != 0) {
                    topHtml += `<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Credit Memo</b></td><td style='text-align:right'>` + parseFloat(order.DeductionAmount).toFixed(2) + `</td></tr>`
                }
                if (order.AdjustmentAmt != 0) {
                    topHtml += `<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Adjustment</b></td><td style='text-align:right'>` + parseFloat(order.AdjustmentAmt).toFixed(2) + `</td></tr>`
                }
                topHtml += `<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Grand Total</b></td><td style='text-align:right'>` + parseFloat(order.PayableAmount).toFixed(2) + `</td></tr>`
                topHtml += `</tfoot>'</table>`;
                topHtml += `<p  style='page-break-before: always'></p>`; 
                $('.InvContent').append(topHtml);
                $('.InvContent').append(topHtml);

            });

            setTimeout(function () { window.print(); }, 2000);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}



