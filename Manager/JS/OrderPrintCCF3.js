﻿
var Print = 1, PrintLabel = '';
$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('OrderAutoId');
    if (getid != null) {
        getOrderData(getid);
    }
});
//                                                     Edit Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/WebAPI/WPrintOrder.asmx/getOrderData",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
                return;
            }
            var xmldoc = $.parseXML(response.d);
            var Company = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");
            var items = $(xmldoc).find("Table2");
            var DuePayment = $(xmldoc).find("Table3");
            var CreditMemoNo = $(xmldoc).find("Table5");
            if (Print == 1) {
                var check = 0;
                var DueAmount = 0, Aging = 0, AmtPaid = 0, GrandTotal = 0;
                $("#tblduePayment tbody tr").remove();
                var rowdue = $("#tblduePayment thead tr").clone(true);
                if (DuePayment.length > 0) {
                    $(DuePayment).each(function (index) {
                        if (parseFloat($(this).find('AmtDue').text()) > 0) {
                            $(".SrNo", rowdue).text(Number(check) + 1);
                            $(".OrderNo", rowdue).text($(this).find('OrderNo').text());
                            $(".OrderDate", rowdue).text($(this).find('OrderDate').text());
                            $(".AmtDue", rowdue).html($(this).find('AmtDue').text());
                            $(".GrandTotal", rowdue).html($(this).find('GrandTotal').text());
                            $(".AmtPaid", rowdue).html($(this).find('AmtPaid').text());
                            $(".Aging", rowdue).html($(this).find('Aging').text());
                            DueAmount += parseFloat($(this).find('AmtDue').text());
                            Aging += parseInt($(this).find('Aging').text());
                            AmtPaid += parseInt($(this).find('AmtPaid').text());
                            GrandTotal += parseInt($(this).find('GrandTotal').text());
                            $('#tblduePayment tbody').append(rowdue);
                            rowdue = $("#tblduePayment tbody tr:last").clone(true);
                            check = check + 1;
                        }
                    })

                }
                $('#Aging').text(Aging);
                $("#tblduePayment tfoot tr").find("td:eq(1)").text(GrandTotal.toFixed(2));
                $("#tblduePayment tfoot tr").find("td:eq(3)").text(DueAmount.toFixed(2));
                $("#tblduePayment tfoot tr").find("td:eq(2)").text(AmtPaid.toFixed(2));
                if (parseFloat(DueAmount) > 0) {
                    $(".DueAmount").show();
                } else {
                    $(".DueAmount").hide();
                }
            } else {
                $(".DueAmount").hide();
            }

            var LoginEmpType = $(xmldoc).find("Table5").find("LoginEmpType").text();

            $("#logo").attr("src", "../Img/logo/" + $(Company).find("Logo").text())
            $("#Address").text($(Company).find("Address").text());
            $("#Phone").text($(Company).find("MobileNo").text());
            $("#FaxNo").text($(Company).find("FaxNo").text());
            $("#Website").text($(Company).find("Website").text());
            $("#TC").html($(Company).find("TermsCondition").text());
            $("#CompanyName").html($(Company).find("CompanyName").text());
            $("#EmailAddress").html($(Company).find("EmailAddress").text());
            $("#PageHeading").html($(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + ' (Page 1 of <span class="TotalPage"></span>)');
            $("#lblCustomerName").html($(order).find("CustomerName").text());
            $("#Prntlebel").text($(order).find("MLTaxPrintLabel").text());
            $("#weightTaxPrintLabel").text($(order).find("WeightTaxPrintLabel").text());
            $("#stateTaxPrintlabel").text($(order).find("TaxPrintLabel").text());
            $("#acNo").text($(order).find("CustomerId").text());
            $("#BusinessName").text($(order).find("BusinessName").text());
            if ($(order).find("BusinessName").text() != '') {
                $("#BusinessName").text($(order).find("BusinessName").text());
                $("#BusinessName").closest('tr').show();
            }

            $("#storeName").text($(order).find("ContactPersonName").text());
            $("#ContPerson").text($(order).find("Contact").text());
            $("#terms").text($(order).find("TermsDesc").text());

            $("#shipAddr").text($(order).find("ShipAddr").text());

            $("#billAddr").text($(order).find("BillAddr").text());

            $("#salesRep").text($(order).find("SalesPerson").text());
            $("#so").text($(order).find("OrderNo").text());
            $("#soDate").text($(order).find("OrderDate").text());
            $("#custType").text($(order).find("CustomerType").text());
            $("#pkdBy").text($(order).find("PackerName").text());
            $("#shipVia").text($(order).find("ShippingType").text());
            var html = '<table class="table tableCSS tblProduct" id="tblProduct"><thead><tr>';
            html += '<thead><tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
            html += '<td style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
            html += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
            html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


            //  $("#tableDynamic").html('');
            var row = $("#tblProduct thead tr").clone(true);
            var catId, catName, Qty = 0, total = 0.00, count = 1, tr, totalPrice = 0.00;
            var pageNo = 1, productname = '', rowcount = 0, TotalMLTax = 0.00, TotalMLQty = 0.00, iDisc=0.00;
            var chk = 0, orderQty = 0;
            $.each(items, function () {
                iDisc = $(this).find("Del_discount").text();
                if (iDisc == '0.00' || iDisc == '0') {
                    iDisc = "";
                }
                else {
                    if (iDisc != '') {
                        iDisc = iDisc.toString() + " %";
                    }
                }
                if (parseInt($(order).find("Status").text()) < 3) {
                    orderQty = $(this).find("RequiredQty").text();
                    totalPrice = parseFloat($(this).find("NetPrice").text());
                } else {
                    orderQty = $(this).find("QtyShip").text();
                    if (parseInt($(this).find("QtyShip").text()) == 0) {
                        totalPrice = 0.00;
                    } else {
                        totalPrice = parseFloat($(this).find("NetPrice").text());
                    }
                }
                if ($(this).find("IsExchange").text() == 1) {
                    productname = $(this).find("ProductName").text() + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Exchange)' + "</span>";
                }
                else if ($(this).find("isFreeItem").text() == 1) {
                    productname = $(this).find("ProductName").text() + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Free)' + "</span>";
                }
                else {
                    productname = $(this).find("ProductName").text();
                }

                if (rowcount == 0 || $(this).find("CategoryId").text() == catId) {

                    html += "<tr><td>" + orderQty + "</td><td>" + $(this).find("UnitType").text() + "</td><td>" + $(this).find("TotalPieces").text() + "</td><td>" + $(this).find("ProductId").text() + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                    html += "<td>" + $(this).find("Barcode").text() + "</td>";
                    html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + iDisc + "</td><td style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                    html += "</tr>";
                    rowcount = parseInt(rowcount) + 1;
                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    total += Number(totalPrice);
                    Qty += Number(orderQty);

                    TotalMLQty += Number($(this).find("TotalMLQty").text());
                    TotalMLTax += Number($(this).find("TotalMLTax").text());
                    if (pageNo == 1 && rowcount >= 18) {
                        pageNo = Number(pageNo) + 1;                      
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                    else if (((rowcount) % 26) == 0) {
                        pageNo = Number(pageNo) + 1;                       
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }

                } else {

                    html += "<tr style='background: #ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='5'>" + catName + "</td><td></td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";

                    rowcount = parseInt(rowcount) + 1;
                    if (rowcount == 18 && pageNo == 1) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                    if (rowcount == 26) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                    if (TotalMLTax > 0) {
                        html += "<tr style='background:#ccc;font-weight:700;'><td colspan='7' style='white-space: nowrap'>ML Qty</td><td style='text-align:right'>" + TotalMLQty.toFixed(2) + "</td><td style='text-align:right'>" +
                            TotalMLTax.toFixed(2) + "</td></tr>";
                        rowcount = parseInt(rowcount) + 1;
                    }

                    if (rowcount == 26) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                    html += "<tr><td>" + orderQty + "</td><td>" + $(this).find("UnitType").text() + "</td><td>" + $(this).find("TotalPieces").text() + "</td><td>" + $(this).find("ProductId").text() + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                    html += "<td>" + $(this).find("Barcode").text() + "</td>";
                    html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + iDisc + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                    html += "</tr>";
                    Qty = 0;
                    rowcount = parseInt(rowcount) + 1;
                    total = 0.00;
                    TotalMLQty = 0.00;
                    TotalMLTax = 0.00;

                    if (pageNo == 1 && rowcount >= 18) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                    else if (((rowcount) % 26) == 0) {
                        pageNo = Number(pageNo) + 1;
                        html += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        html += '<td style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        html += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                    catName = $(this).find("CategoryName").text();
                    total += Number(totalPrice);
                    Qty += Number(orderQty);

                    TotalMLQty += Number($(this).find("TotalMLQty").text());
                    TotalMLTax += Number($(this).find("TotalMLTax").text());
                }

                count++;
            });
            html += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='5'>" + catName + "</td><td></td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
            if (TotalMLTax > 0) {
                html += "<tr style='background:#ccc;font-weight:700;'><td colspan='7' style='white-space: nowrap'>ML Qty</td><td style='text-align:right'>" + TotalMLQty.toFixed(2) + "</td><td style='text-align:right'>" +
                    TotalMLTax.toFixed(2) + "</td></tr>";
                rowcount = parseInt(rowcount) + 1;
            }
            html += "</tbody></table>";
            if ((rowcount == 18 && pageNo == 1)) {
                pageNo = Number(pageNo) + 1;
                html += "<p  style='page-break-before: always'></p><br/>";
                html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + "   (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                html += "</thead > </table>";
                rowcount = 0;
            }
            $('#tableDynamic').html(html);
            $('.TotalPage').html(pageNo);

            var totalQtyByCat = 0;
            $('.tblProduct tbody tr').each(function () {

                totalQtyByCat += Number($(this).find(".totalQty").text());
            });
            $("#totalQty").text(totalQtyByCat);
            $("#totalwoTax").text($(order).find("TotalAmount").text());


            if (parseFloat($(order).find("OverallDiscAmt").text()) > 0) {
                $("#disc").text('-' + $(order).find("OverallDiscAmt").text());
            } else {
                $("#disc").closest('tr').hide();
            }

            if (parseFloat($(order).find("TotalTax").text()) > 0) {
                $("#tax").text($(order).find("TotalTax").text());
            } else {
                $("#tax").closest('tr').hide();
            }

            if (parseFloat($(order).find("ShippingCharges").text()) > 0.00) {
                $("#shipCharges").text($(order).find("ShippingCharges").text());
            } else {
                $("#shipCharges").closest('tr').hide();
                $("#shpsh").closest('tr').hide();
            }


            if (parseFloat($(order).find("MLTax").text()) > 0) {
                $("#MLQty").text($(order).find("MLQty").text());
                $("#MLTax").text($(order).find("MLTax").text());
            } else {
                $("#MLTax").closest('tr').hide();
                $("#MLQty").closest('tr').hide();
            }

            if (parseFloat($(order).find("WeightTax").text()) > 0.00) {
                $("#WeightTax").text($(order).find("WeightTax").text());
            }
            else {
                $("#WeightTax").closest('tr').hide();
            }

            if (parseFloat($(order).find("DeductionAmount").text()) > 0) {
                var CrNo = 'Credit Memo Applied ['
                $.each(CreditMemoNo, function (index) {
                    if (index == CreditMemoNo.length - 1) {
                        CrNo += $(this).find("CreditNo").text() + "]";
                    }
                    else {
                        CrNo += $(this).find("CreditNo").text() + ",";
                    }
                })
                $("#crmNo").html(CrNo);
                if (parseFloat($(order).find("DeductionAmount").text()) > parseFloat($(order).find("GrandTotal").text())) {
                    $("#spnCreditmemo").text('-' + $(order).find("GrandTotal").text());
                }
                else {
                    $("#spnCreditmemo").text('-' + $(order).find("DeductionAmount").text());
                }
            } else {
                $("#spnCreditmemo").closest('tr').hide();
            }
            if (parseFloat($(order).find("AdjustmentAmt").text()) != 0) {
                $("#spAdjustment").text($(order).find("AdjustmentAmt").text());
            } else {
                $("#spAdjustment").closest('tr').hide();
            }

            $("#grandTotal").text($(order).find("GrandTotal").text());

            if (parseFloat($(order).find("CreditAmount").text()) > 0) {
                $("#spnStoreCredit").text($(order).find("CreditAmount").text());
            } else {
                $("#spnStoreCredit").closest('tr').hide();
            }
            if (parseFloat($(order).find("CreditAmount").text()) > 0 || parseFloat($(order).find("DeductionAmount").text()) > 0) {
                $("#spnPayableAmount").text($(order).find("PayableAmount").text());
            } else {
                $("#spnPayableAmount").closest('tr').hide();
            }

            var CreditMemoDetails = $(xmldoc).find('Table5');
            if (CreditMemoDetails.length > 0) {
                $(CreditMemoDetails).each(function () {
                    getCreditData($(this).find('CreditAutoId').text())
                });
                setTimeout(function () { window.print(); }, 200);
            } else {
                setTimeout(function () { window.print(); }, 200);
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function getCreditData(CreditAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "/Sales/WebAPI/WCreditMemo.asmx/PrintCredit",
        data: "{'CreditAutoId':'" + CreditAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Company = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");
            var items = $(xmldoc).find("Table2");
            var DuePayment = $(xmldoc).find("Table3");
            var gethtmlwhole = '';
            var pageNo = 1, rowcount = 0;
            gethtmlwhole += '<p style="page-break-before: always"></p><fieldset><table class="table tableCSS"><tbody><tr><td colspan="6" style="text-align: center">';
            gethtmlwhole += '<span style="font-size: 20px;font-weight: 500!Important">' + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + ' (Page ' + pageNo + ' Of <span class="TotalPageC"></span>)<span style="float:right;"><b><i>Credit Memo</i></b></span></span>';
            gethtmlwhole += '</td></tr><tr><td rowspan="2" style="width: 80px !important;"><img src="../Img/logo/' + $(Company).find("Logo").text() + '" class="img-responsive"></td>';
            gethtmlwhole += '<td>' + $(Company).find("CompanyName").text() + '</td>';
            gethtmlwhole += '<td class="tdLabel" style="white-space: nowrap">Customer ID</td><td>' + $(order).find("CustomerId").text() + '</td>';
            gethtmlwhole += '<td class="tdLabel" style="white-space: nowrap">Sales Rep</td><td>' + $(order).find("SalesPerson").text() + '</td></tr>';
            gethtmlwhole += '<tr><td><span>' + $(Company).find("Address").text() + '</span></td><td class="tdLabel" style="white-space: nowrap">Customer Contact</td>';
            gethtmlwhole += '<td style="white-space: nowrap">' + $(order).find("ContactPersonName").text() + '</td><td class="tdLabel">Credit No </td>';
            gethtmlwhole += '<td>' + $(order).find("OrderNo").text() + '</td></tr><tr><td><span>' + $(Company).find("Website").text() + '</span></td>';
            gethtmlwhole += '<td>Phone:<span>' + $(Company).find("MobileNo").text() + '</span></td><td class="tdLabel">Contact No</td>';
            gethtmlwhole += '<td>' + $(order).find("Contact").text() + '</td><td class="tdLabel" style="white-space: nowrap">Credit Date</td>';
            gethtmlwhole += '<td style="white-space: nowrap">' + $(order).find("OrderDate").text() + '</td></tr>';
            gethtmlwhole += '<tr><td>' + $(Company).find("EmailAddress").text() + '</td><td>Fax: ' + $(Company).find("FaxNo").text() + '</td><td class="tdLabel">Terms</td>';
            gethtmlwhole += '<td>' + $(order).find("TermsDesc").text() + '</td><td class="tdLabel">Ship Via</td><td>' + $(order).find("shipVia").text() + '</td></tr>';
            if ($(order).find("BusinessName").text() != '') {
                gethtmlwhole += '<tr><td><span class="tdLabel">Business Name:</span></td><td>' + $(order).find("BusinessName").text() + '</td></tr>';
            }
            gethtmlwhole += '<tr><td><span class="tdLabel">Shipping Address:</span></td><td colspan="5">' + $(order).find("ShipAddr").text() + ", " +
                $(order).find("State2").text() + ", " +
                $(order).find("City2").text() + ", " +
                $(order).find("Zipcode2").text() + '</td></tr>';
            gethtmlwhole += '<tr><td><span class="tdLabel">Billing Address:</span></td><td colspan="5">' + $(order).find("BillAddr").text() + ", " +
                $(order).find("State1").text() + ", " +
                $(order).find("City1").text() + ", " +
                $(order).find("Zipcode1").text() + '</td></tr>';
            gethtmlwhole += '</tbody></table></fieldset>';

            gethtmlwhole += '<fieldset><table class="table tableCSS tblProduct" id="tblProduct"><thead><tr>';
            gethtmlwhole += '<thead><tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
            gethtmlwhole += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td colspan="3" class="ProName" style="white-space: nowrap">Item</td>';
            //gethtmlwhole += '<td class="Barcode" style="width: 10px">UPC</td>';
            gethtmlwhole += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
            gethtmlwhole += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
            var catId, catName, Qty = 0, total = 0.00, count = 1, tr, totalPrice = 0.00, TotalQty = 0;

            $.each(items, function () {
                rowcount = parseInt(rowcount) + 1;
                if (parseInt($(this).find("QtyShip").text()) == 0) {
                    totalPrice = 0.00;
                } else {
                    totalPrice = parseFloat($(this).find("NetPrice").text());
                }
                if (count == 1 || $(this).find("CategoryId").text() == catId) {

                    gethtmlwhole += "<tr><td>" + $(this).find("QtyShip").text() + "</td><td>" + $(this).find("UnitType").text() + "</td><td>" + $(this).find("ProductId").text() + "</td><td colspan='3'>" + $(this).find("ProductName").text().replace(/&quot;/g, "\'") + "</td>";
                    //gethtmlwhole += "<td>" + $(this).find("Barcode").text() + "</td>";
                    gethtmlwhole += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td><td style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                    gethtmlwhole += "</tr>";
                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    total += Number(totalPrice);
                    Qty += Number($(this).find("QtyShip").text());
                    TotalQty += Number($(this).find("QtyShip").text());
                    if (pageNo == 1 && rowcount >= 18) {
                        pageNo = Number(pageNo) + 1;
                        gethtmlwhole += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        gethtmlwhole += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        gethtmlwhole += "<tr><td colspan='9' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPageC'></span>)</span></td></tr>";
                        gethtmlwhole += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        gethtmlwhole += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td colspan="3" class="ProName" style="white-space: nowrap">Item</td>';
                        //gethtmlwhole += '<td class="Barcode" style="width: 10px">UPC</td>';
                        gethtmlwhole += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                        gethtmlwhole += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    } else if (((rowcount) % 26) == 0) {
                        pageNo = Number(pageNo) + 1;
                        gethtmlwhole += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        gethtmlwhole += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        gethtmlwhole += "<tr><td colspan='9' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPageC'></span>)</span></td></tr>";
                        gethtmlwhole += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        gethtmlwhole += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td colspan="3" class="ProName" style="white-space: nowrap">Item</td>';
                        //gethtmlwhole += '<td class="Barcode" style="width: 10px">UPC</td>';
                        gethtmlwhole += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                        gethtmlwhole += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }

                } else {
                    gethtmlwhole += "<tr style='background: #ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='5'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
                    gethtmlwhole += "<tr><td>" + $(this).find("QtyShip").text() + "</td><td>" + $(this).find("UnitType").text() + "</td><td>" + $(this).find("ProductId").text() + "</td><td colspan='3'>" + $(this).find("ProductName").text().replace(/&quot;/g, "\'") + "</td>";
                    //gethtmlwhole += "<td>" + $(this).find("Barcode").text() + "</td>";
                    gethtmlwhole += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                    gethtmlwhole += "</tr>";
                    Qty = 0;
                    total = 0.00;
                    rowcount = parseInt(rowcount) + 1;
                    if (pageNo == 1 && rowcount >= 18) {
                        pageNo = Number(pageNo) + 1;   
                        gethtmlwhole += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        gethtmlwhole += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        gethtmlwhole += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        gethtmlwhole += "<tr><td colspan='9' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPageC'></span>)</span></td></tr>";
                        gethtmlwhole += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        gethtmlwhole += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td colspan="3" class="ProName" style="white-space: nowrap">Item</td>';
                        //gethtmlwhole += '<td class="Barcode" style="width: 10px">UPC</td>';
                        gethtmlwhole += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                        gethtmlwhole += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;

                    } else if (((rowcount) % 26) == 0) {
                        pageNo = Number(pageNo) + 1;
                        gethtmlwhole += "</tbody></table><p  style='page-break-before: always'></p><br/>";
                        gethtmlwhole += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                        gethtmlwhole += "<tr><td colspan='9' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPageC'></span>)</span></td></tr>";
                        gethtmlwhole += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        gethtmlwhole += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td colspan="3" class="ProName" style="white-space: nowrap">Item</td>';
                        //gethtmlwhole += '<td class="Barcode" style="width: 10px">UPC</td>';
                        gethtmlwhole += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                        gethtmlwhole += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                        rowcount = 0;
                    }
                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    total += Number(totalPrice);
                    Qty += Number($(this).find("QtyShip").text());
                    TotalQty += Number($(this).find("QtyShip").text());
                }

                count++;
            });
            gethtmlwhole += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='5'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
            gethtmlwhole += '</tbody></table></fieldset>';
            gethtmlwhole += '<fieldset><table style="width: 100%;"><tbody><tr><td style="width: 33%;"></td><td style="width: 33%;"></td><td style="width: 33%;"><table class="table tableCSS">';
            gethtmlwhole += '<tbody><tr><td class="tdLabel">Total Qty</td><td style="text-align: right;">' + TotalQty + '</td></tr>';
            gethtmlwhole += '<tr><td class="tdLabel">Sub Total</td><td style="text-align: right;">' + $(order).find("GrandTotal").text() + '</td></tr>';
            if (parseFloat($(order).find("OverallDisc").text()) > 0) {
                gethtmlwhole += '<tr><td class="tdLabel">Discount</td><td style="text-align: right;">' + $(order).find("OverallDisc").text() + '</td></tr>';
            }

            if (parseFloat($(order).find("TotalTax").text()) > 0) {
                gethtmlwhole += '<tr><td class="tdLabel">Tax</td><td style="text-align: right;">' + $(order).find("TotalTax").text() + '</td></tr>';
            }
            if (parseFloat($(order).find("MLTax").text()) > 0) {
                gethtmlwhole += '<tr><td class="tdLabel" style="white-space: nowrap;">' + $(order).find("MLTaxPrintLabel").text() + '</td><td style="text-align: right;">' + $(order).find("MLTax").text() + '</td></tr>';
            }
            if (parseFloat($(order).find("WeightTaxAmount").text()) > 0) {
                gethtmlwhole += '<tr><td class="tdLabel" style="white-space: nowrap;">Weight Tax</td><td style="text-align: right;">' + $(order).find("WeightTaxAmount").text() + '</td></tr>';
            }
            //$(order).find("MLTaxPrintLabel").text()
            if (parseFloat($(order).find("AdjustmentAmt").text()) != 0) {
                gethtmlwhole += '<tr><td class="tdLabel">Adjustment</td><td style="text-align: right;">' + $(order).find("AdjustmentAmt").text() + '</td></tr>';
            }
            gethtmlwhole += '<tr><td class="tdLabel">Grand Total</td><td style="text-align: right;">' + $(order).find("TotalAmount").text() + '</td></tr>';
            gethtmlwhole += '</tbody></table></fieldset>';

            $('#CRtableDynamic').append(gethtmlwhole);
            $('.TotalPageC').html(pageNo);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}