﻿var rowupdate = "";
var CustomerId; var MLQtyRate = 0.00;
$(document).ready(function () {
    var getid = getQueryString('OrderAutoId');
    if (getid != null) {
        getOrderData(getid);
        $("#txtHOrderAutoId").val(getid);
    }
});
var getQueryString = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/WebAPI/WManager_OrderList.asmx/getOrderData",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var order = $(xmldoc).find("Table");
            var product = $(xmldoc).find("Table1");
            var duePayment = $(xmldoc).find("Table2");
            var CreditMemoDetails = $(xmldoc).find("Table3");
            var RemarksDetails = $(xmldoc).find("Table4");
            var pkg = $(xmldoc).find("Table5");
            var CreditDetails = $(xmldoc).find("Table6");
            if ($(order).find("StatusCode").text() == 4 || $(order).find("StatusCode").text() == 5 || $(order).find("StatusCode").text() == 7) {
                $("#showedit").show();
            } else {
                $("#showedit").hide();
            }
            $("#shippingtype").val($(order).find("ShipAutoId").text());
            if (Number(CreditDetails.length) > 0 && $(order).find("StatusCode").text() <= 4) {
                $('#MODALpoPFORCREDIT').modal('show');
                $("#tblCreditMemo tbody tr").remove();
                var rowc = $("#tblCreditMemo thead tr:last").clone(true);
                $.each(CreditDetails, function (index) {
                    $(".SRNO", rowc).text(Number(index) + 1);
                    $(".Action", rowc).html("<input type='radio' name='credit' value=" + $(this).find("CreditAutoId").text() + "> ");
                    $(".CreditNo", rowc).html("<a target='_blank' href='/Manager/ManagerCreditMemo.aspx?PageId=" + $(this).find("CreditAutoId").text() + "'>" + $(this).find("CreditNo").text() + "</a>");
                    $(".CreditDate", rowc).text($(this).find("CreditDate").text());
                    $(".NoofItem", rowc).text($(this).find("NoofItem").text());
                    $(".NetAmount", rowc).text($(this).find("NetAmount").text());
                    $("#tblCreditMemo tbody").append(rowc);
                    rowc = $("#tblCreditMemo tbody tr:last").clone(true);
                });
            } else {
                if ($("#shippingtype").val() == '11') {
                    $("#ModalSalesQuote").modal('show');
                }
            }
            $("#tblPacked tbody tr").remove();
            var rowP = $("#tblPacked thead tr:last").clone(true);
            if (pkg.length > 0) {
                $("#tblPackedDetails").show();
                $.each(pkg, function (index) {
                    $(".SRNO", rowP).text(Number(index) + 1);
                    $(".PackedId", rowP).text($(this).find("PackingId").text());
                    $(".PackedDate", rowP).text($(this).find("PkgDate").text());
                    $(".PackedBy", rowP).text($(this).find("Packer").text());
                    $('#tblPacked').find("tbody").append(rowP);
                    rowP = $("#tblPacked tbody tr:last").clone(true);
                });
            } else {
                $("#tblPackedDetails").hide();
            }

            $("#Table2 tbody tr").remove();
            if ($(RemarksDetails).length > 0) {
                $("#OrderRemarksDetails").show();
                var rowtest = $("#Table2 thead tr").clone();
                $.each(RemarksDetails, function (index) {
                    $(".SRNO", rowtest).html((Number(index) + 1));
                    $(".EmployeeName", rowtest).html($(this).find("EmpName").text());
                    $(".EmployeeType", rowtest).html($(this).find("EmpType").text());
                    $(".Remarks", rowtest).html($(this).find("Remarks").text());
                    $("#Table2 tbody").append(rowtest);
                    rowtest = $("#Table2 tbody tr:last").clone(true);
                });
            }
            var totalCredit = 0.00;
            if (Number(CreditMemoDetails.length) > 0) {
                $("#CusCreditMemo").show();
                $("#tblCreditMemoListDetails").show();
                $("#tblCreditMemoList tbody tr").remove();
                var rowc = $("#tblCreditMemoList thead tr:last").clone(true);
                $.each(CreditMemoDetails, function (index) {

                    $(".SRNO", rowc).text(Number(index) + 1);
                    $(".Action", rowc).html("<input type='radio' name='credit' value=" + $(this).find("CreditAutoId").text() + "> ");
                    $(".CreditNo", rowc).text($(this).find("CreditNo").text());
                    $(".CreditDate", rowc).text($(this).find("CreditDate").text());
                    $(".ReturnValue", rowc).text($(this).find("ReturnValue").text());
                    totalCredit += parseFloat($(this).find("ReturnValue").text())
                    $("#tblCreditMemoList tbody").append(rowc)
                    rowc = $("#tblCreditMemoList tbody tr:last").clone(true);
                });
            } else {
                $("#CusCreditMemo").hide();
            }
            $("#TotalDue").text(totalCredit.toFixed(2));

            CustomerId = $(order).find("CustomerId").text();
            $('#btneditOrder').hide();
            $('#btnGenBar').hide();
            $('#btnAsgnDrv').hide();
            $('#btnGenOrderCC').hide();
            $('#btnSetAsProcess').hide();
            $('#btnCancelOrder').hide();
            if (Number($(order).find("StatusCode").text()) == 1 || Number($(order).find("StatusCode").text()) == 2) {
                $('#btnCancelOrder').show();
            }
            else if (Number($(order).find("StatusCode").text()) == 3 || Number($(order).find("StatusCode").text()) == 9 || Number($(order).find("StatusCode").text()) == 10 || Number($(order).find("StatusCode").text()) == 4 || Number($(order).find("StatusCode").text()) == 7) {
                $('#btnCancelOrder').show();
                $('#btnGenBar').show();
                $('#btnAsgnDrv').show();
                if (Number($(order).find("StatusCode").text()) != 7) {
                    $('#btnSetAsProcess').show();
                    $('#btneditOrder').show();
                    $('#btnGenOrderCC').show();
                }
            }
            else if (Number($(order).find("StatusCode").text()) == 5 || Number($(order).find("StatusCode").text()) == 6) { // As per Brijbhan Sir
                $("#btnGenBar").show();
                $("#btnGenOrderCC").show();
                $('#btnCancelOrder').show();
            }
            
            if (Number($(order).find("StatusCode").text()) == 8) {
                $("#btnChangeSalesPerson").hide();
            }

            if ($(order).find("Driver").text() != null && $(order).find("Driver").text() != "") {
                $("#btnAsgnDrv button").html("<b>Change Driver</b>");

                if ($(order).find("StatusCode").text() == 7) {
                    $("#btnAsgnDrv button").html("<b>Reassign Driver</b>");
                }
                $("#divPkg").append("<span>Driver : <b>" + $(order).find("DrvName").text() + "</b><br />Assigned on : <b>" + $(order).find("AssignDate").text() + "</b></span>");
                $("#divPkg").show();
            }
            if (Number($(order).find("StatusCode").text()) == 6 || Number($(order).find("StatusCode").text()) == 11) {
                $("#txtDelivered").val("Yes");
            } else {
                $("#txtDelivered").val('No');
            }

            $("#txtRemarks").val($(order).find("DrvRemarks").text());
            if ($(order).find("PaymentRecev").text() == null || $(order).find("PaymentRecev").text() == "") {
                $("#AccountDeliveryInfo").hide();
            } else {
                $("#AccountDeliveryInfo").show();
            }
            $('#txtRecievedPayment').val($(order).find("PaymentRecev").text());
            $("#txtAmtPaid").text($(order).find("AmtPaid").text());
            $("#txtAmtDue").val($(order).find("AmtDue").text());
            $("#txtAcctRemarks").val($(order).find("AcctRemarks").text());
            $("#txtTaxType").val($(order).find("TaxType").text());
            $("#txtHOrderAutoId").val($(order).find("AutoId").text());
            $("#txtOrderId").val($(order).find("OrderNo").text());
            $("#lblOrderno").text($(order).find("OrderNo").text());
            $("#txtOrderType").val($(order).find("OrderType").text());
            $("#hfShippingAutoId").val($(order).find("ShipAutoId").text());
            $("#txtShippingType").val($(order).find("ShippingType").text());
            $("#txtSalesperson").val($(order).find("SalesPerson").text());
            $("#txtCustomerType").val($(order).find("CustomerType").text());
            if (Number($(order).find("StatusCode").text()) > 5) {
                $('#DrvDeliveryInfo').show();
            }
            $("#txtOrderDate").val($(order).find("OrderDate").text());
            $('#txtAssignDate').pickadate({
                format: 'mm/dd/yyyy',
                formatSubmit: 'mm/dd/yyyy',
                selectYears: true,
                selectMonths: true,
                min: -7,
                firstDay: 0
            });
            if ($(order).find("CommentType").text() != '')
                $("#ddlCommentType").val($(order).find("CommentType").text());
            $("#txtComment").val($(order).find("Comment").text());
            $("#txtDeliveryDate").val($(order).find("DeliveryDate").text());
            $("#txtOrderStatus").val($(order).find("Status").text());
            $("#txtCustomer").val($(order).find("CustomerName").text());
            $("#hiddenCustAutoId").val($(order).find("CustAutoId").text());
            $("#txtTerms").val($(order).find("Terms").text());
            $("#txtTerms").val($(order).find("TermsDesc").text());
            $("#OrderRemarks").val($(order).find("OrderRemarks").text());
            $("#txtAdjustment").val($(order).find("AdjustmentAmt").text());
            $("#txtMLQty").val($(order).find("MLQty").text());
            $("#txtWeightQty").val($(order).find("WeightOzQty").text());

            $("#txtMLTax").val($(order).find("MLTax").text());
            $("#txtWeightTax").val($(order).find("WeightOzTotalTax").text());

            if ($(order).find("CreditAmount").text() != "") {
                $("#txtStoreCreditAmount").val($(order).find("CreditAmount").text());
            }
            if ($(order).find('PayableAmount').text() != '') {
                $("#txtPaybleAmount").val($(order).find("PayableAmount").text());
            } else {
                $("#txtPaybleAmount").val($(order).find("GrandTotal").text());
            }
            if ($(order).find("DeductionAmount").text() != "")
                $("#txtDeductionAmount").val($(order).find("DeductionAmount").text());
            $("#CreditMemoAmount").text($(order).find("CustomerCredit").text());

            $("#txtBillAddress").val($(order).find("BillAddr").text());
            //$("#txtBillState").val($(order).find("State1").text());
            //$("#txtBillCity").val($(order).find("City1").text());
            //$("#txtBillZip").val($(order).find("Zipcode1").text());
            if ($(order).find('isMLManualyApply').text() == '1') {
                $('#btnRemoveMlTax button').html('Remove ML Tax');
            } else {
                $('#btnRemoveMlTax button').html('Add ML Tax')
            }
            $("#hfMLTaxStatus").val($(order).find('isMLManualyApply').text()); 
            if ($(order).find("StatusCode").text() != 8 && $(order).find("EnabledTax").text() == 1) {
                $('#btnRemoveMlTax').show();
            } else {
                $('#btnRemoveMlTax').hide();
            }

            $("#txtShipAddress").val($(order).find("ShipAddr").text());
            //$("#txtShipState").val($(order).find("State2").text());
            //$("#txtShipCity").val($(order).find("City2").text());
            //$("#txtShipZip").val($(order).find("Zipcode2").text());

            $("#txtTotalAmount").val($(order).find("TotalAmount").text());
            $("#txtOverallDisc").val($(order).find("OverallDisc").text());
            $("#txtDiscAmt").val($(order).find("OverallDiscAmt").text());
            $("#txtShipping").val($(order).find("ShippingCharges").text());
            $("#txtTotalTax").val($(order).find("TotalTax").text());
            $("#txtGrandTotal").val($(order).find("GrandTotal").text());
            $("#hiddenGrandTotal").val($(order).find("GrandTotal").text());
            $("#txtPackedBoxes").val($(order).find("PackedBoxes").text());
            $("#lblNoOfbox").text($(order).find("PackedBoxes").text());
            showDuePayments(duePayment);
            $("#tblProductDetail tbody tr").remove();
            var row = $("#tblProductDetail thead tr").clone(true);
            $.each(product, function () {
                $(".Action", row).html('<a href="#" title="Product sales history" onclick="ProductList(this)"><span class="la la-history"></span></a>');
                $(".ProId", row).html($(this).find("ProductId").text());
                if (Number($(this).find("isFreeItem").text()) == 1) {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span> <product class='badge badge badge-pill badge-success'>Free</product>");

                } else if (Number($(this).find("IsExchange").text()) == 1) {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span> <product class='badge badge badge-pill badge-primary'>Exchange</product>");
                }
                else if (Number($(this).find("Tax").text()) == 1) {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span> <product class='badge badge badge-pill badge-danger'>Taxable</product>");
                } else {
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                }
                $(".UnitType", row).html($(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")");
                $(".TtlPcs", row).text($(this).find("TotalPieces").text());
                $(".ItemTotal", row).text($(this).find("ItemTotal").text());
                $(".Discount", row).text(parseFloat($(this).find("Discount").text()).toFixed(2));
                $(".NetPrice", row).text($(this).find("NetPrice").text());
                //var unitPrice = $(this).find("UnitPrice").text();
                //var PriceHtml = "<b>Min Price - " + $(this).find("MinPrice").text()+"</b>";
                //PriceHtml += "</br><b>Base Price - " + $(this).find("BasePrice").text() + "</b>";


                //if (($(order).find("StatusCode").text() == 3 || $(order).find("StatusCode").text() == 4 || $(order).find("StatusCode").text() == 5) && $(this).find("IsExchange").text() == 0 && $(this).find("isFreeItem").text() == 0 && ($(this).find("UnitPrice").text() < $(this).find("MinPrice").text())) {
                //    $(".UnitPrice", row).html($(this).find("UnitPrice").text() + "<span class='la la-question-circle' data-toggle='tooltip' title='" + PriceHtml + "'></span>");
                //    $(".UnitPrice", row).addClass("redCell");
                //} else {
                $(".UnitPrice", row).html($(this).find("UnitPrice").text());
                //    $(".UnitPrice", row).addClass("removeredCell");
                //}
            

                $(".RequiredQty", row).text($(this).find("RequiredQty").text());
           
                $(".SRP", row).text($(this).find("SRP").text());
                $(".GP", row).text($(this).find("GP").text());
                if (Number($(this).find("Tax").text()) == 1) {
                    $(".TaxRate", row).html('<span class="la la-check-circle success"></span>');
                } else {
                    $(".TaxRate", row).html('');
                }
                if (Number($(this).find("IsExchange").text()) == 1) {
                    $(".IsExchange", row).html('<span class="la la-check-circle success"></span>');
                } else {
                    $(".IsExchange", row).html('');
                }
                $(".Barcode", row).text($(this).find("Barcode").text());

                $(".QtyShip", row).text($(this).find("QtyShip").text());
             
                if ($(order).find("Status").text() == "Close") {
                    $(".PerPrice", row).text($(this).find("PerpiecePrice").text());
                    if ($(this).find("QtyDel").text() != "") {
                        $(".QtyDel", row).html($(this).find("QtyDel").text());
                    } else {
                        $(".QtyDel", row).html($(this).find("TotalPieces").text());
                    }
                    $(".FreshReturn", row).html($(this).find("FreshReturnQty").text());
                    $(".DemageReturn", row).html($(this).find("DamageReturnQty").text());
                    $(".MissingItem", row).html($(this).find("MissingItemQty").text());
                    if (Number($(this).find("FreshReturnQty").text()) > 0 || Number($(this).find("DamageReturnQty").text()) > 0) {
                        $(row).css('background-color', '#efcccc');
                    } else if (Number($(this).find("MissingItemQty").text()) > 0) {
                        $(row).css('background-color', '#edb3b3');
                    } else {
                        $(row).css('background-color', '#fff');
                    }

                } else {
                    $('#tblProductDetail td:nth-child(10)').hide();
                    $('#tblProductDetail td:nth-child(11)').hide();
                    $('#tblProductDetail td:nth-child(12)').hide();
                    $('#tblProductDetail td:nth-child(13)').hide();
                    $('#tblProductDetail td:nth-child(14)').hide();
                    $(".MissingItem", row).hide();
                    $(".FreshReturn", row).hide();
                    $(".DemageReturn", row).hide();
                    $(".QtyDel", row).hide();
                    $(".PerPrice", row).hide();
                }
               
              
                
                if (Number($(this).find("RequiredQty").text()) != Number($(this).find("QtyShip").text())) {
                    $(row).css('background-color', '#87dabc');  //11/01/2019 21:41 PM
                }
                else {
                    $(row).css('background-color', '#fff');
                }
                $('#tblProductDetail tbody').append(row);
                row = $("#tblProductDetail tbody tr:last").clone(true);
            });
            if (Number($(order).find("StatusCode").text()) < 3) {
                $('#PackingSlip').closest('.row').hide();
                $('#WithoutCategorybreakdown').closest('.row').hide();
            } else {
                $('#PackingSlip').closest('.row').show();
                $('#WithoutCategorybreakdown').closest('.row').show();
            }
            //uuu
            if (Number($(order).find("StatusCode").text()) == 9) {
                $("#btnAsgnDrv").hide();
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip({
        html: true
    });
});

function showDuePayments(duePayment) {
    $("#tblCustDues tbody tr").remove();
    var row = $("#tblCustDues thead tr").clone(true);

    var sumOrderValue = 0,
        sumPaid = 0,
        sumDue = 0;

    if (duePayment.length > 0) {
        $("#noDues").hide();
        $("#CustomerPaymentsHistory").show();
        $.each(duePayment, function () {
            $(".orderNo", row).html("<span orderautoid='" + $(this).find("AutoId").text() + "'><a href='#'>" + $(this).find("OrderNo").text() + "</a></span>");
            $(".orderDate", row).text($(this).find("OrderDate").text());
            $(".value", row).text($(this).find("GrandTotal").text());
            $(".OrderType", row).text($(this).find("OrderType").text());
            $(".amtPaid", row).text($(this).find("AmtPaid").text()).css("color", "#8bc548");
            $(".amtDue", row).text($(this).find("AmtDue").text()).css("color", "red");
            $(".pay", row).html("<input type='text' class='form-control input-sm' style='width:100px;float:right;text-align:right;' value='0.00' placeholder='0.00' onkeyup='sumPay()' />");
            $(".remarks", row).html("<input type='text' class='form-control input-sm' placeholder='Enter payment details here' />");

            sumOrderValue += Number($(this).find("GrandTotal").text());
            sumPaid += Number($(this).find("AmtPaid").text());
            sumDue += Number($(this).find("AmtDue").text());

            $('#tblCustDues').find("tbody").append(row).end()
                .find("tfoot").show().find("#sumOrderValue").text(sumOrderValue.toFixed(2)).end()
                .find("#sumPaid").text(sumPaid.toFixed(2)).css("color", "#8bc548").end()
                .find("#sumDue").text(sumDue.toFixed(2)).css("color", "red").end()
                .find("#sumPay").text("0.00").end();

            row = $("#tblCustDues tbody tr:last").clone(true);
        });
        $("#CustDues").show();
        if ($("#hiddenEmpTypeVal").val() != "5") {
            $("#CustDues").find("#btnPay_Dues").show().end()
                .find("#tblCustDues").find(".pay").show().end()
                .find(".remarks").show().end()
                .find("tfoot > tr > td").show();
        }
    } else {
        $("#tblCustDues").find("tbody tr").remove().end().find("tfoot").hide();
        $("#noDues").show();
        $("#CustDues").show();
        $("#btnPay_Dues, #btnClose_Dues").hide();
        $("CustomerPaymentsHistory").hide();
    }
}
/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                            Summation of Pay Amount against Order Dues
function PayNowDelivery() {
    location.href = '/Sales/ViewCustomerDetails.aspx?customerId=' + CustomerId + '&Type=1';
}
function printOrder_CustCopy() {
    if ($('#HDDomain').val() == 'psmpa') {
        $("#divPSMPADefault").show();
        $("#PSMPADefault").prop('checked', true);
    }
   else if ($('#HDDomain').val() == 'psmwpa') {
        $("#divPSMWPADefault").show(); 
    }
    else if ($('#HDDomain').val() == 'psmnpa') {
        $("#divPSMNPADefault").show();
        $("#divPSMPADefault").hide();
        $("#rPSMNPADefault").prop('checked', true);
    }
    else {
        $("#chkdueamount").prop('checked', true);
        $("#PSMPADefault").closest('.row').hide();
    }
    $('#PopPrintTemplate').modal('show');
}
function PrintOrder() {
    $('#PopPrintTemplate').modal('hide');
    if ($("#chkdueamount").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF1.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#chkdefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCC.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template1").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template2").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF3.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#PSMPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PSMWPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF6.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PackingSlip").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF5.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#WithoutCategorybreakdown").prop('checked') == true) {
        window.open("/Manager/WithoutCategorybreakdown.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#rPSMNPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ1").prop('checked') == true) {
        window.open("/Manager/PrintOrderItem.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ2").prop('checked') == true) {
        window.open("/Manager/PrintOrderItemTemplate2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PSMWPANDefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCFPN.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
}

function saveDelUpdates() {
    location.href = '/Sales/OrderMaster.aspx?OrderNo=' + $("#txtOrderId").val();
}

function GenBar() {
    getPackerAssignPrintData($("#hfShippingAutoId").val());
}
function getPackerAssignPrintData(ShippingAutoId) {
    var data = {
        ShippingAutoId: ShippingAutoId
    };
    $.ajax({
        type: "POST",
        url: "/Packer/WebAPI/WPackerOrderMaster.asmx/getPackerAssignPrintData",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (result) {

            var xmldoc = $.parseXML(result.d);
            var packerPrint = $(xmldoc).find('Table');
            var packerPrintHtml = ``;
            if (packerPrint.length > 0) {
                if (packerPrint.length == 1) {
                    $.each(packerPrint, function () {
                        var Url = $(this).find("Url").text();
                        var finalUrl = Url + "?OrderId=" + $("#txtOrderId").val();
                        window.open(finalUrl, "popUpWindow", "height=400,width=400,left=10,top=10,,scrollbars=yes,menubar=no");
                    });

                }
                else {
                    var i = 1;

                    $.each(packerPrint, function () {
                        if (i == 1) {
                            var checkStatus = "checked";
                        }
                        packerPrintHtml += `<div class="clearfix"></div>
                        <div class="row form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <input type="radio" name="Template" id="Templ` + i + `" value="` + $(this).find("Url").text() + `" class="printTemplateClass" ` + checkStatus + ` />
                                `+ $(this).find("PrintTemplate").text() + `
                            </div>
                        </div>`;

                        i++;

                    });
                    $('#PopPrintBarcodeTemplate').modal('show');
                    $("#packerAssignPrint").html(packerPrintHtml);
                }

            } else {
                toastr.error('Template not found', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            }


        },
        error: function (result) {

        }

    });
}
function PrintBarcodeByTemplate() {
    if ($('input.printTemplateClass:checked').length == 0) {
        toastr.error('Please select template .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else {
        var Url = $('input.printTemplateClass:checked').val();
        var finalUrl = Url + "?OrderId=" + $("#txtOrderId").val();
        window.open(finalUrl, "popUpWindow", "height=400,width=400,left=10,top=10,,scrollbars=yes,menubar=no");
    }
}
function getDriverList() {
    if (parseFloat($("#txtOverallDisc").val()) > 100 && parseFloat($("#txtPaybleAmount").val()) < 0) {
        toastr.error('Please manage discount before assign driver.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/getDriverList",
        data: "{'OrderAutoId':'" + $("#txtHOrderAutoId").val() + "','LoginEmpType':'5'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var Drivers = $(xmldoc).find("Table");
                var selectDrv = $(xmldoc).find("Table1");
                $("#modalMisc").modal("toggle").find(".modal-dialog").removeClass("modal-sm").end()
                    .find(".modal-title").text("Drivers").end()
                    .find("#tblAssignDrv").show().end()
                    .find("#btnAsgn").show().end()
                    .find("#btnPrint").hide()
                    .find('#insideText').hide();
                $('#DriverText').show();
                $('#insideText').text('');
                $('#btnOk').hide();
                $("#tblAssignDrv tbody tr").remove();
                var row = $("#tbldriver thead tr").clone(true);
                $('#txtAssignDate').val($(selectDrv).find("AssignDate").text());
                var picker = $('#txtAssignDate').pickadate('picker')
                picker.set('select', new Date($(selectDrv).find("AssignDate").text()));
                $("#txtAssignDate").attr("disabled", false);

                $.each(Drivers, function (index) {
                    if (index == 0) {
                        $(row).css('background-color', '#ccff99');
                    }
                    else {
                        $(row).css('background-color', 'white');
                    }
                    $(".DrvName", row).html("<span drvautoid =" + $(this).find("AutoId").text() + ">" + $(this).find("Name").text() + "</span>");
                    $(".AsgnOrders", row).html($(this).find("AssignOrders").text());
                    if ($(this).find("AutoId").text() == $(selectDrv).find("Driver").text()) {
                        $(".Select", row).html("<input type='radio' name='driver' class='radio' onchange = 'changeDriver(this)' checked='checked'>");
                        $("#btnAsgn").attr("disabled", true);
                    } else {
                        $(".Select", row).html("<input type='radio' name='driver' class='radio' onchange = 'changeDriver(this)' >");
                    }
                    $("#tbldriver tbody").append(row);

                    row = $("#tbldriver tbody tr:last").clone(true);
                });

            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function SetAsProcess() {
    swal({
        title: "Are you sure?",
        text: "you want to set status as Processed ?",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, Processed it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            AfterYesSetAsProcess()

        } else {
            swal("", "Process cancelled.", "error");
        }
    })
}

function AfterYesSetAsProcess() {

    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/SetAsProcess",
        data: "{'OrderAutoId':'" + $("#txtHOrderAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                swal("", "Order has been set as processed successfully.", "success").then(function () {
                    window.location.href = '/Manager/Manager_viewOrder.aspx?OrderAutoId=' + $('#txtHOrderAutoId').val();
                });


            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });


}


function changeDriver(e) {
    var btnAsgn = $(e).closest(".modal-dialog").find("#btnAsgn");
    $(btnAsgn).attr("disabled", false);
}
function assignDriver() {
    if (checksdrivRequiredField()) {
        var drvAutoId, flag = true;
        $("#tblAssignDrv tbody tr").each(function () {
            if ($(this).find("input[name='driver']").is(":checked")) {
                drvAutoId = $(this).find(".DrvName span").attr("drvautoid");
                flag = false;
            }
        });

        var data = {
            DriverAutoId: drvAutoId,
            OrderAutoId: $("#txtHOrderAutoId").val(),
            AssignDate: $("#txtAssignDate").val()
        };

        if (!flag) {
            $.ajax({
                type: "Post",
                url: "/Manager/WebAPI/WManager_OrderList.asmx/AssignDriver",
                data: "{'dataValues':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                async: false,

                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d == 'true') {
                            swal("", "Driver assigned successfully", "success"
                            ).then(function () {
                                location.href = '/Manager/Manager_viewOrder.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                            });
                        } else {
                            swal("Error!", response.d, "error");
                        }
                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        } else {
            swal("Error!", "No Driver selected !", "error");

        }
    }
    else {
        toastr.error('Assign Date Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function confirm_Cancellation_Of_Order() {
    $("#OrderBarcode").modal("toggle");
    $('#txtCancellationRemarks').val('');
    $('#txtCancellationRemarks').css('border-color', 'none');
}

function cancelOrder() {
    if (checkRequiredField()) {
        swal({
            title: "Are you sure?",
            text: "You want to cancel order.",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, Cancel!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,

                },
                confirm: {
                    text: "Yes, Cancel it!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                cancelOrder1();


            } else {
                $("#OrderBarcode").modal("toggle");
            }
        })
    }
    else {
        toastr.error('Cancellation Remarks is Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function cancelOrder1() {

    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/cancelOrder",
        data: "{'OrderAutoId':'" + $("#txtHOrderAutoId").val() + "','Remarks':'" + $('#txtCancellationRemarks').val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    swal("", "Order cancelled successfully.", "success").then(function () {
                        location.href = '/Manager/Manager_viewOrder.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                    });
                } else {
                    swal("Error!", response.d, "error");
                }

            } else {
                location.href = '/';

            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}

function viewOrderLog() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WOrderLog.asmx/viewOrderLog",
        data: "{'OrderAutoId':" + $("#txtHOrderAutoId").val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,

        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var orderlog = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");

            $("#Span1").text(order.find("OrderNo").text());
            $("#lblOrderDate").text(order.find("OrderDate").text());

            $("#tblOrderLog tbody tr").remove();
            var row = $("#tblOrderLog thead tr").clone(true);
            if (orderlog.length > 0) {
                $("#EmptyTable").hide();
                $.each(orderlog, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("ActionDate").text());
                    $(".Action", row).text($(this).find("Action").text());
                    $(".Remark", row).text($(this).find("Remarks").text());

                    $("#tblOrderLog tbody").append(row);
                    row = $("#tblOrderLog tbody tr:last").clone(true);
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}
function EditOrder() {
    location.href = '/Manager/ManagerOrder.aspx?OrderNo=' + $("#txtOrderId").val();
}
function CreditProcessed() {
    var test = 0;
    $("#tblCreditMemo tbody tr").each(function () {
        if ($(this).find('.Action input').prop('checked') == true) {
            location.href = '/Manager/ManagerCreditMemo.aspx?PageId=' + $(this).find('.Action input').val();
            test = 1;
        }
    });

    if (test == 0) {
        swal("Error!", "Please Check any one item for Process.", "warning");
    }
}
function skipCreditmemo() {
    $("#MODALpoPFORCREDIT").modal('hide');
    if ($("#shippingtype").val() == '11') {
        $("#ModalSalesQuote").modal('show');
    }
}
function closeSalesQuoteModa() {
    $("#ModalSalesQuote").modal('hide');
}
function RemoveMlTax() {
    $('#SecurityEnabled').modal('show');
}
var i = 0;
function clickonSecurity() {

    if (checksecRequiredField()) {
        i = 0;
        $.ajax({
            type: "Post",
            url: "/Manager/WebAPI/WManager_OrderList.asmx/clickonSecurity",
            data: "{'CheckSecurity':'" + $("#txtSecurity").val() + "','OrderAutoId':'" + $("#txtHOrderAutoId").val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            async: false,
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == 'true') {
                        $('#SecurityEnabled').modal('hide');
                        $('#modalMLTaxConfirmation').modal('show');
                    } else {
                        toastr.error('Access Denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                    }
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
    else {
        toastr.error('Security Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function ConfirmMLTax() {
    var Status = Number($("#hfMLTaxStatus").val());
    var text = "";
    if (Status == 1) {
        text = "You want to remove ML Tax.";
    }
    else {
        text = "You want to add ML Tax.";
    }
    if (checksecRequiredFieldML()) {
        swal({
            title: "Are you sure?",
            text: text,
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, Cancel.",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                $("#modalMLTaxConfirmation").modal('hide');
                ConfirmSecurity();
            } else {
                closeModal: true
            }
        })
    }
    else {
        toastr.error('Security Required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function ConfirmSecurity() {

    i = 0;
    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/ConfirmSecurity",
        data: "{'CheckSecurity':'" + $("#txtSecurity").val() + "','OrderAutoId':'" + $("#txtHOrderAutoId").val() + "','MLTaxRemark':'" + $("#txtMLRemark").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,

        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    $('#btnpkd').removeAttr('disabled');
                    var txt = "";
                    if (Number($("#hfMLTaxStatus").val()) == 1) {
                        txt="ML Tax removed successfully", "success";
                    }
                    else {
                        txt="ML Tax added successfully", "success";
                    }

                    swal("", txt, "success"
                    ).then(function () {
                        location.href = '/Manager/Manager_viewOrder.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                    });

                    i = 1;
                } else {
                    toastr.error('Access Denied.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function ClosePop() {
    if (i == 1) {
        getOrderData($("#txtHOrderAutoId").val());
    }
    $('#SecurityEnvalid').modal('hide');
}

function checksecRequiredField() {
    var boolcheck = true;
    $('.reqseq').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}
function checksecRequiredFieldML() {
    var boolcheck = true;
    $('.reqML').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}
function checksdrivRequiredField() {
    var boolcheck = true;
    $('.reqdriv').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}
function checksecRequiredFieldRem() {
    var boolcheck = true;
    $('.reqremark').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}

function ProductList(e) {
    var tr = $(e).closest('tr');
    var ProductAutoId = tr.find('.ProName span').attr('productautoid');
    $("#ProdId").html(tr.find('.ProId').text());
    $("#ProdName").html(tr.find('.ProName span').html());
    $("#hfProductAutoId").val(ProductAutoId);
    ProductSalesHistory(1);
}

function ProductSalesHistory(PageIndex) {

    var data = {
        ProductAutoId: $("#hfProductAutoId").val(),
        CustomerAutoId: $("#hiddenCustAutoId").val(),
        PageSize: $("#ddlPageSize").val(),
        PageIndex: PageIndex
    };
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/WebAPI/WManager_OrderList.asmx/ProductSalesHistory",
        data: "{'dataValue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {

            var xmldoc = $.parseXML(response.d);
            var product = $(xmldoc).find("Table1");
            var NetPrice = 0.00;
            if (product.length > 0) {
                $("#tblProductSalesHistory tbody tr").remove();
                var row = $("#tblProductSalesHistory thead tr:last").clone(true);
                $.each(product, function (index) {
                    $(".SRNO", row).text(Number(index) + 1);
                    $(".OrderDate", row).html($(this).find("OrderDate").text());
                    $(".OrderNo", row).text($(this).find("OrderNo").text());
                    if (Number($(this).find("isFreeItem").text()) == 1) {
                        $(".UnitType", row).html('<span>' + $(this).find("UnitType").text() + ' <a href="#" style="color:green" title="Free">*</a></span>');
                    }
                    else if (Number($(this).find("IsExchange").text()) == 1) {
                        $(".UnitType", row).html('<span>' + $(this).find("UnitType").text() + ' <a href="#"  style="color:green" title="Exchange">**</a></span>');
                    }
                    else {
                        $(".UnitType", row).text($(this).find("UnitType").text());
                    }
                    $(".QtyShip", row).text($(this).find("QtyShip").text());
                    $(".UnitPrice", row).text($(this).find("UnitPrice").text());
                    $(".TotalPrice", row).text($(this).find("TotalPrice").text());

                    if (Number($(this).find("StatusCode").text()) == 1) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_New'>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 2) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Processed'>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 3) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Packed'>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 4) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Ready_to_Ship '>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 5) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Shipped'>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 6) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Delivered'>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 7) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Undelivered'>" + $(this).find("StatusType").text() + "</span>");
                    } else if (Number($(this).find("StatusCode").text()) == 8) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_cancelled' >" + $(this).find("StatusType").text() + "</span>");
                    }
                    else if (Number($(this).find("StatusCode").text()) == 9) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Add_On'>" + $(this).find("StatusType").text() + "</span>");
                    }
                    else if (Number($(this).find("StatusCode").text()) == 10) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Add_On_Packed'>" + $(this).find("StatusType").text() + "</span>");
                    }
                    else if (Number($(this).find("StatusCode").text()) == 11) {
                        $(".StatusType", row).html("<span class='badge badge badge-pill Status_Close'>" + $(this).find("StatusType").text() + "</span>");
                    }

                    NetPrice += Number($(this).find("TotalPrice").text());

                    $("#tblProductSalesHistory tbody").append(row);
                    row = $("#tblProductSalesHistory tbody tr:last").clone(true);
                });
                $("#EmptyTable").hide();
                $("#tblProductSalesHistory").show();
                $("#ddlPageSize").show();
            } else {
                $("#EmptyTable").show();
                $("#ddlPageSize").hide();
                $("#tblProductSalesHistory").hide();
            }
            var pager = $(xmldoc).find("Table");
            $(".Pager").ASPSnippets_Pager({
                ActiveCssClass: "current",
                PagerCssClass: "pager",
                PageIndex: parseInt(pager.find("PageIndex").text()),
                PageSize: parseInt(pager.find("PageSize").text()),
                RecordCount: parseInt(pager.find("RecordCount").text())
            });
            $("#TotalNetPrice").html(parseFloat(NetPrice).toFixed(2));
            var OverAll = $(xmldoc).find("Table2");
            $("#OverallNetPrice").html($(OverAll).find("OverALllTotalPrice").text());
            $("#modalProductSalesHistory").modal('show');
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function Pagevalue(e) {
    ProductSalesHistory(parseInt($(e).attr("page")));
};
function closeSalesHistory() {
    $("#ddlPageSize").val(10);
    $("#modalProductSalesHistory").modal('hide');
}

function bindSalesperson() {
    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/bindSalesperson",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var salesperson = $(xmldoc).find("Table");
                $.each(salesperson, function () {
                    $("#ddlsalesperson").append($("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("SalesPerson").text().replace(/&quot;/g, "\'") + "</option>"));
                });

                $("#ddlsalesperson").select2();

            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function ChangerSalesPerson() {
    bindSalesperson();
    $("#ModalSalespersonChange").modal('show');
}

function changeSalesPerson() {
    if (checkchSalesRequired()) {
        var data = {
            OrderAutoId: $("#txtHOrderAutoId").val(),
            Remarks: $('#txtSalesRemark').val(),
            SalesAutoId: $('#ddlsalesperson').val(),
        };
        $.ajax({
            type: "Post",
            url: "/Manager/WebAPI/WManager_OrderList.asmx/changeSalesPerson",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            async: false,
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d == 'true') {
                        swal("", "Sales person changed successfully.", "success").then(function () {
                            location.href = '/Manager/Manager_viewOrder.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                        });
                    } else {
                        swal("Error!", response.d, "error");
                    }

                } else {
                    location.href = '/';

                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}

function checkchSalesRequired() {
    var boolcheck = true;
    $('.reqSales').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}
function ChangeStatus() {
    $("#ChangeStatus").modal('show');
}
function UpdateOrderStatus() {

    if (checksecRequiredFieldRem()) {
        swal({
            title: "Are you sure?",
            text: "You want to change status.",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, cancel.",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes,Change it.",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                changestatustoPacked();

            }
        })
    } else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function changestatustoPacked() {
    var data = {
        OrderAutoId: $("#txtHOrderAutoId").val(),
        Status: $('#ddlChangeStatus').val(),
        Remarks: $('#txtOrderRemarks').val(),
    };
    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/changeOrderStatus",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == 'true') {
                    //$("#ChangeStatus").modal('hide');
                    //getOrderData($("#txtHOrderAutoId").val());
                    swal("", "Order Status changed successfully.", "success").then(function () {
                        location.href = '/Manager/Manager_viewOrder.aspx?OrderAutoId=' + $("#txtHOrderAutoId").val();
                    });
                } else {
                    swal("Error!", response.d, "error");
                }

            } else {
                location.href = '/';

            }

        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}