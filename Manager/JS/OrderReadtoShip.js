﻿$(document).ready(function () {

    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('AutoId');
    var OrderDate = getQueryString('AssignDate');
    if (getid != null) {

    }
    getOrderData(getid, OrderDate);
});

/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                     Edit Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData(DriverAutoId, AssignDate) { 
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/WebAPI/asgnOrderList.asmx/PrintReadytoShipOrder",
        data: "{'DriverAutoId':'" + DriverAutoId + "','AssignDate':'" + AssignDate + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Company = $(xmldoc).find("Table");
            var items = $(xmldoc).find("Table1");
            $("#logo").attr("src", "../Img/logo/" + $(Company).find("Logo").text());
            $("#Address").text($(Company).find("Address").text());
            $("#Phone").text($(Company).find("MobileNo").text());
            $("#FaxNo").text($(Company).find("FaxNo").text());
            $("#Website").text($(Company).find("Website").text());
            $("#TC").html($(Company).find("TermsCondition").text());



            var row = $("#tblProduct thead tr").clone(true);
            var catId, catName, Qty = 0, total = 0.00, count = 1, tr;

            $.each(items, function (index) {
                if (index == 0) {
                    $("#DriverName").html($(this).find('Driver').text());
                    $("#RootName").html($(this).find('RootName').text());
                    $("#AssignDate").html($(this).find('AssignDate').text());
                }
                $(".OrderNo", row).html($(this).find("OrderNo").text()+'<br/>'+ $(this).find("OrderDate").text().replace(/&quot;/g, "\'"));
                $(".Stop", row).html($(this).find("Stoppage").text());
                $(".Remarks", row).html($(this).find("DriverRemarks").text());
                //$(".OrderDate", row).html($(this).find("OrderDate").text().replace(/&quot;/g, "\'"));
                $(".CustomerName", row).html($(this).find("CustomerName").text());
                $(".SalesPerson", row).html($(this).find("SalesPerson").text());//SalesPerson
                $(".GrandTotal", row).html($(this).find("GrandTotal").text());
                //$(".Schedule", row).html($(this).find("ScheduleAt").text());
                $('#tblProduct tbody').append(row);
                row = $("#tblProduct tbody tr:last").clone(true);


            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}