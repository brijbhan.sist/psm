﻿var OrdData = new Array();var EmailPrint=''
$(document).ready(function () {    
    var allOrd = "", tempOrdArr = [];
    if (localStorage.getItem('DriverPackageprint') != null) {
        OrdData = JSON.parse(localStorage.getItem('DriverPackageprint'));
        $.each(OrdData, function (i, items) {
            if (items.orders != "") {
                tempOrdArr = items.orders.split(',');
                if (tempOrdArr.length != 0) {
                    $.each(tempOrdArr, function (n, ord) {
                        if (ord != "") {
                            allOrd += ord + ',';
                        }
                    })
                }
            }
        })
        getAllOrderData(allOrd);
    }
});
function getAllOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "DriverPackagePrint.aspx/getAllOrderData",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            if (response.d != "Session Expired") {                
                var DropDown = $.parseJSON(response.d);
                var Orderlist = DropDown[0].Orderlist;
                if (OrdData[0].PrintType == "0" && OrdData[0].orders != "") {
                    getOrderData(OrdData[0].orders);
                }
                if (Orderlist.length > 0) {
                    for (var i = 0; i < Orderlist.length; i++) {
                        for (var Arr = 0; Arr < OrdData.length; Arr++) {
                            var str = OrdData[Arr].orders;
                            var strarray = str.split(',');
                            for (var k = 0; k < strarray.length; k++) {
                                if (Orderlist[i].AutoId == strarray[k]) {
                                    if (OrdData[Arr].PrintType == "1") {
                                        getOrderData_PSMDefault(strarray[k], OrdData[Arr].noofcopy);
                                    }
                                    if (OrdData[Arr].PrintType == "2") {
                                        getOrderData_Template1(strarray[k], OrdData[Arr].noofcopy);
                                    }
                                    if (OrdData[Arr].PrintType == "3") {
                                        getOrderData_Template2(strarray[k], OrdData[Arr].noofcopy);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if (localStorage.getItem('EmailPrint') != null) {
                    debugger;  sendMail();
                        localStorage.removeItem('EmailPrint');
                    }
                }
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}


/*-------------------------------------------------------------------------------------------------------------------------------*/
//                                                     Edit Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "DriverPackagePrint.aspx/PrintReadytoShipOrder",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Company = $(xmldoc).find("Table");
            if (Company.length > 0) {
                $(".InvContents").show();
            }
            var items = $(xmldoc).find("Table1");
            $("#logo").attr("src", "../Img/logo/" + $(Company).find("Logo").text());
            $("#Address").text($(Company).find("Address").text());
            $("#Phone").text($(Company).find("MobileNo").text());
            $("#FaxNo").text($(Company).find("FaxNo").text());
            $("#Website").text($(Company).find("Website").text());
            $("#TC").html($(Company).find("TermsCondition").text());



            var row = $("#tblProduct thead tr").clone(true);
            var catId, catName, Qty = 0, total = 0.00, count = 1, tr;

            $.each(items, function (index) {
                if (index == 0) {
                    $("#DriverName").html($(this).find('Driver').text());
                    $("#RootName").html($(this).find('RootName').text());
                    $("#AssignDate").html($(this).find('AssignDate').text());
                }
                $(".OrderNo", row).html($(this).find("OrderNo").text() + '<br/>' + $(this).find("OrderDate").text().replace(/&quot;/g, "\'"));
                $(".Stop", row).html($(this).find("Stoppage").text());
                $(".Schedule", row).html($(this).find("ScheduleAt").text());
                $(".Remarks", row).html($(this).find("DriverRemarks").text());
                $(".OrderDate", row).html($(this).find("OrderDate").text().replace(/&quot;/g, "\'"));
                $(".CustomerName", row).html($(this).find("CustomerName").text());
                $(".SalesPerson", row).html($(this).find("SalesPerson").text());//SalesPerson
                $(".GrandTotal", row).html($(this).find("GrandTotal").text());
                $('#tblProduct tbody').append(row);
                row = $("#tblProduct tbody tr:last").clone(true);


            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
//var Print = 1, PrintLabel = '';

//                                                     Edit Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData_PSMDefault(OrderAutoId, countCopy) {
    $.ajax({
        type: "POST",
        async: false,
        url: "DriverPackagePrint.aspx/getBulkOrderData",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
                return;
            }
           
            var html = '';
            var OrderList = $.parseJSON(response.d);
            for (var i = 0; i < OrderList.length; i++) {
                var Company = OrderList[i];
                var Order = Company.OrderDetails;
                for (var j = 0; j < Order.length; j++) {
                    var OrderDetail = Order[j];
                    html += '<div id="All"  style="font-family:Arial !important;font-size:12px!important">';
                    html += '<p style="page-break-before: always"></p><fieldset style="font-family:Arial !important;font-size:12px!important"><table class="table tableCSS"  style="width:100%;border-collapse:collapse;"><tbody><tr><td colspan="6" style="text-align: center">';
                    html += '<span style="font-size: 20px;font-weight:100">' + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + ' (Page 1 of <span class=' + OrderDetail.OrderNo + '></span>)</span><span style="font-size: 20px;float:right" ><b><i>Sales Order</i></b></span>';
                    html += '</td></tr><tr><td rowspan="2" style="width: 80px !important;"><img src="../Img/logo/' + Company.Logo + '" class="img-responsive"></td>';
                    html += '<td>' + Company.CompanyName + '</td>';
                    html += '<td class="tdLabel" style="white-space: nowrap">Customer ID</td><td>' + OrderDetail.CustomerId + '</td>';
                    html += '<td class="tdLabel" style="white-space: nowrap">Sales Rep</td><td>' + OrderDetail.SalesPerson + '</td></tr>';
                    html += '<tr><td><span>' + Company.Address + '</span></td><td class="tdLabel" style="white-space: nowrap">Customer Contact</td>';
                    html += '<td style="white-space: nowrap">' + OrderDetail.ContactPersonName + '</td><td class="tdLabel">Order No </td>';
                    html += '<td>' + OrderDetail.OrderNo + '</td></tr><tr><td><span>' + Company.Website + '</span></td>';
                    html += '<td>Phone:<span>' + Company.MobileNo + '</span></td><td class="tdLabel">Contact No</td>';
                    html += '<td>' + OrderDetail.Contact + '</td><td class="tdLabel" style="white-space: nowrap">Order Date</td>';
                    html += '<td style="white-space: nowrap">' + OrderDetail.OrderDate + '</td></tr>';
                    html += '<tr><td>' + Company.EmailAddress + '</td><td>Fax: ' + Company.FaxNo + '</td><td class="tdLabel">Terms</td>';
                    html += '<td><b>' + OrderDetail.TermsDesc + '</b></td><td class="tdLabel">Ship Via</td><td>' + OrderDetail.ShippingType + '</td></tr>';
                    if (OrderDetail.BusinessName != '' && OrderDetail.BusinessName != undefined) {
                        html += '<tr><td><span class="tdLabel">Business Name:</span></td><td>' + OrderDetail.BusinessName + '</td></tr>';
                    }
                    html += '<tr><td><span class="tdLabel">Shipping Address</span></td><td colspan="5">' + OrderDetail.ShipAddr + '</td></tr>';
                    html += '<tr><td><span class="tdLabel">Billing Address</span></td><td colspan="5" >' + OrderDetail.BillAddr + '</td></tr>';
                    html += '</tbody></table>';

                    html += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead><tr>';
                    html += '<thead><tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                    html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                    html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                    html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                    html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


                    var catId, catName, productname = '', Qty = 0, total = 0.00, count = 1, tr, totalPrice = 0.00;
                    var pageNo = 1, rowcount = 0, TotalMLTax = 0.00, TotalMLQty = 0.00; var totalQtyByCat = 0; TotalNZTax = 0.00; totalTaxVal = 0.00;
                    var orderQty = 0; TotalQuantity = 0, iDisc=0.00;

                    var ItemList = OrderDetail.Item1;
                    if (OrderDetail.Item2.length > 0) {
                        ItemList = OrderDetail.Item2;
                    }

                    for (var l = 0; l < ItemList.length; l++) {
                        var Item = ItemList[l];
                        iDisc = Item.Del_discount;
                        if (iDisc == '0.00' || iDisc == '0') {
                            iDisc = "";
                        }
                        else {
                            if (iDisc != '') {
                                iDisc = iDisc.toString() + " %";
                            }
                        }
                        if (parseInt(OrderDetail.Status) < 3) {
                            orderQty = Item.RequiredQty;
                            totalPrice = parseFloat(Item.NetPrice);
                        } else {
                            orderQty = Item.QtyShip;
                            if (parseInt(Item.QtyShip) == 0) {
                                totalPrice = 0.00;
                            } else {
                                totalPrice = parseFloat(Item.NetPrice);
                            }
                        }
                        if (Item.IsExchange == 1) {
                            productname = Item.ProductName + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Exchange)' + "</span>";
                        }
                        else if (Item.isFreeItem == 1) {
                            productname = Item.ProductName + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Free)' + "</span>";
                        }
                        else {
                            productname = Item.ProductName;
                        }
                        if (rowcount == 0 || Item.CategoryId == catId) {
                            
                            html += "<tr><td  style='text-align:center' >" + orderQty + "</td><td style='text-align:center'>" + Item.UnitType + "</td><td style='text-align:center'>" + Item.TotalPieces + "</td><td style='text-align:center'>" + Item.ProductId + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                            html += "<td>" + Item.Barcode + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + Item.SRP.toFixed(2) + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + Item.GP + "</td>";
                            html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + Item.UnitPrice.toFixed(2) + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + iDisc + "</td><td style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                            html += "</tr>";
                            rowcount = parseInt(rowcount) + 1;
                            catId = Item.CategoryId;
                            catName = Item.CategoryName;

                            total += Number(totalPrice);
                            Qty += Number(orderQty);
                            TotalQuantity += Number(orderQty);
                            TotalMLQty += Number(Item.TotalMLQty);
                            TotalMLTax += Number(Item.TotalMLTax);
                            if (Number(Item.Tax) > 0) {
                                totalTaxVal = parseFloat(totalTaxVal) + parseFloat(totalPrice);
                                TotalNZTax = totalTaxVal * parseFloat(Item.TaxValue) / 100

                            }
                            if (pageNo == 1 && rowcount >= 18) {
                                
                                pageNo = Number(pageNo) + 1;
                                html += "'</tbody></table><p style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                                html += "<tr><td colspan='11' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " (Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>)</span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                                rowcount = 0;

                            } else if (((rowcount) % 26) == 0) {
                                pageNo = Number(pageNo) + 1;
                                html += "'</tbody></table><p style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                                html += "<tr><td colspan='11' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " (Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>)</span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                                rowcount = 0;
                            }

                        } else {

                            count++;
                            html += "<tr style='background: #ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='8'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
                            rowcount = parseInt(rowcount) + 1;
                            if (rowcount == 18 && pageNo == 1) {
                                pageNo = Number(pageNo) + 1;
                                html += "</tbody></table><p style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct" style="width:100%;border-collapse:collapse;"><thead>';
                                html += "<tr><td colspan='11' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " (Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>)</span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px;text-align:center">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap;text-align:center">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap;text-align:center">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                                rowcount = 0;
                            }
                            if (rowcount == 26) {
                                pageNo = Number(pageNo) + 1;
                                html += "</tbody></table><p style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                                html += "<tr><td colspan='11' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " (Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>)</span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px;text-align:center">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap;text-align:center">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap;text-align:center">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                                rowcount = 0;
                            }

                            if (TotalNZTax > 0) {
                                html += "<tr style='background:#ccc;font-weight:700;'><td colspan='9' style='white-space: nowrap;text-align:left;'>" + OrderDetail.TaxPrintLabel + "</td><td style='text-align:right'>" + parseFloat(TotalNZTax).toFixed(2) + "</td></tr>";
                                rowcount = parseInt(rowcount) + 1;
                            }
                            if (TotalMLTax > 0) {
                                html += "<tr style='background:#ccc;font-weight:700;'><td colspan='8' style='white-space: nowrap'>ML Qty</td><td style='text-align:right'>" + TotalMLQty.toFixed(2) + "</td><td colspan='2' style='text-align:right'>" +
                                    TotalMLTax.toFixed(2) + "</td></tr>";
                                count++;
                                rowcount = parseInt(rowcount) + 1;
                            }
                            if (rowcount == 26) {
                                pageNo = Number(pageNo) + 1;
                                html += "</tbody></table><p style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                                html += "<tr><td colspan='11' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " (Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>)</span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px;text-align:center">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap;text-align:center">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px;text-align:center">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap;text-align:center">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                                rowcount = 0;
                            }
                            html += "<tr><td style='text-align:center'>" + orderQty + "</td><td style='text-align:center'>" + Item.UnitType + "</td><td style='text-align:center'>" + Item.TotalPieces + "</td><td style='text-align:center'>" + Item.ProductId + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                            html += "<td>" + Item.Barcode + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(Item.SRP).toFixed(2) + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(Item.GP).toFixed(2) + "</td>";
                            html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(Item.UnitPrice).toFixed(2) + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + iDisc + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                            html += "</tr>";
                            rowcount = parseInt(rowcount) + 1;
                            Qty = 0;
                            total = 0.00;
                            TotalMLQty = 0.00;
                            TotalMLTax = 0.00;
                            totalTaxVal = 0.00;
                            TotalNZTax = 0.00;
                            if (pageNo == 1 && rowcount >= 18) {
                                pageNo = Number(pageNo) + 1;
                                html += "'</tbody></table><p style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"><thead>';
                                html += "<tr><td colspan='11' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " (Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>)</span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                                rowcount = 0;

                            } else if (((rowcount) % 26) == 0) {
                                pageNo = Number(pageNo) + 1;
                                html += "'</tbody></table><p style='page-break-before: always'></p><br/>";
                                html += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                                html += "<tr><td colspan='11' style='text-align:center;'><span style='font-size:20px;font-weight:100'>" + OrderDetail.CustomerName + ' -  ' + OrderDetail.OrderNo + " (Page " + pageNo + " Of <span class=" + OrderDetail.OrderNo + "></span>) </span></td></tr>";
                                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                                html += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                                html += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                                rowcount = 0;
                            }
                            catId = Item.CategoryId;
                            catName = Item.CategoryName;
                            total += Number(totalPrice);
                            Qty += Number(orderQty);
                            TotalQuantity += Number(orderQty);
                            TotalMLQty += Number(Item.TotalMLQty);
                            TotalMLTax += Number(Item.TotalMLTax);
                            if (Number(Item.Tax) > 0) {
                                totalTaxVal = parseFloat(totalTaxVal) + parseFloat(totalPrice);
                                TotalNZTax = totalTaxVal * parseFloat(Item.TaxValue) / 100

                            }

                        }
                        count++;
                        orderQty = 0;
                    }
                    html += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='8'>" + catName + "</td><td style='text-align:right;'>" + parseFloat(total).toFixed(2) + "</td></tr>";
                    rowcount = Number(rowcount) + 1;
                    if (TotalNZTax > 0) {
                        html += "<tr style='background:#ccc;font-weight:700;'><td colspan='9' style='white-space: nowrap;text-align:left;'>" + OrderDetail.TaxPrintLabel + "</td><td style='text-align:right'>" + parseFloat(TotalNZTax).toFixed(2) + "</td></tr>";
                        rowcount = Number(rowcount) + 1;
                    }
                    if (TotalMLTax > 0) {
                        html += "<tr style='background:#ccc;font-weight:700;'><td colspan='8' style='white-space: nowrap'>ML Qty</td><td style='text-align:right'>" + parseFloat(TotalMLQty).toFixed(2) + "</td><td colspan='2' style='text-align:right'>" +
                            parseFloat(TotalMLTax).toFixed(2) + "</td></tr>";
                        rowcount = Number(rowcount) + 1;
                    }




                    html += '</tbody></table>';
                    if ((rowcount == 18 && pageNo == 1)) {
                        pageNo = Number(pageNo) + 1;
                        html += "<p style='page-break-before: always'></p><br/>";
                        html += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                        html += "<tr><td colspan='11' style='text-align:center'><span style='font-size: 20px !Important;font-weight:500 !Important'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + "   (Page " + pageNo + " Of <span class='TotalPage'></span>)</span></td></tr>";
                        html += "</thead > </table>";
                        rowcount = 0;
                    }
                    //------------------------------------------------------------------------Terms----------------------------------------------------------------------------------------------

                    html += '<fieldset style="padding:0px;margin:0px"><table style="width: 100%;border-collapse:collapse;">';
                    html += '<tbody><tr><td style="width: 66%;padding: 0 10px 0 0;" colspan="2"><center><div id="TC">' + Company.TermsCondition + '</div></center></td>';
                    html += '<td style="width: 33%;padding:0"><table class="table tableCSS"  style="width:100%;border-collapse:collapse;"><tbody>';
                    html += '<tr><td class="tdLabel">Total Qty</td><td id="totalQty" style="text-align: right;">' + TotalQuantity + '</td></tr>';
                    html += '<tr><td class="tdLabel">Sub Total</td><td style="text-align: right;"><span id="totalwoTax">' + parseFloat(OrderDetail.TotalAmount).toFixed(2) + '</span></td></tr>';
                    if (OrderDetail.OverallDiscAmt > 0) {
                        html += '<tr><td class="tdLabel">Discount</td><td style="text-align: right;"><span id="disc">' + parseFloat(OrderDetail.OverallDiscAmt).toFixed(2) + '</span></td> </tr>';
                    }
                    if (OrderDetail.TotalTax > 0) {
                        html += '<tr><td class="tdLabel">' + OrderDetail.TaxPrintLabel + '</td><td style="text-align: right;"><span id="tax">' + parseFloat(OrderDetail.TotalTax).toFixed(2) + '</span></td></tr>';
                    }
                    html += '<tr><td class="tdLabel">Shipping</td><td style="text-align: right;"><span id="shipCharges">' + parseFloat(OrderDetail.ShippingCharges).toFixed(2) + '</span></td></tr>';
                    if (OrderDetail.MLTax > 0) {
                        html += '<tr><td class="tdLabel" id="Prntlebel">' + OrderDetail.MLTaxPrintLabel + '</td><td style="text-align: right;"><span id="MLTax">' + parseFloat(OrderDetail.MLTax).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.WeightTax > 0.00) {
                        html += '<tr><td class="tdLabel">' + OrderDetail.WeightTaxPrintLabel + '</td><td style="text-align: right;"><span id="WeightTax">' + parseFloat(OrderDetail.WeightTax).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.AdjustmentAmt != 0) {
                        html += '<tr><td class="tdLabel">Adjustment</td><td style="text-align: right;"><span id="spAdjustment">' + parseFloat(OrderDetail.AdjustmentAmt).toFixed(2) + '</span></td></tr>';
                    }

                    html += '<tr><td class="tdLabel">Grand Total</td><td style="text-align: right;"><span id="grandTotal">' + parseFloat(OrderDetail.GrandTotal).toFixed(2) + '</span></td></tr>';
                    if (OrderDetail.DeductionAmount > 0) {
                        html += '<tr><td class="tdLabel">Credit memo</td><td style="text-align: right;"><span id="spnCreditmemo">' + parseFloat(OrderDetail.DeductionAmount).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.CreditAmount > 0) {
                        html += '<tr><td class="tdLabel">Store Credit</td><td style="text-align: right;"><span id="spnStoreCredit">' + parseFloat(OrderDetail.CreditAmount).toFixed(2) + '</span></td></tr>';
                    }
                    if (OrderDetail.CreditAmount > 0 || OrderDetail.DeductionAmount > 0) {
                        html += '<tr><td class="tdLabel">Payable Amount</td><td style="text-align: right;"><span id="spnPayableAmount">' + parseFloat(OrderDetail.PayableAmount).toFixed(2) + '</span></td></tr>';
                    }
                    html += '</tbody></table></td></tr></tbody></table></fieldset>';
                    html += '</div>';


                    //-----------------------------------------------------------------------------------Open Balance-------------------------------------------------------------------------------------

                    var OpenBalance = OrderDetail.DueOrderList;

                    html = FnManageDueOrder(OpenBalance, html, OrderDetail);


                    var CreditMemo = OrderDetail.CreditDetails;

                    if (CreditMemo != '') {

                        for (var n = 0; n < CreditMemo.length; n++) {

                            var CreditDetail = CreditMemo[n];

                            html = getCreditData(CreditDetail, html, Company);

                        }
                    }
                    for (var i = 1; i <= countCopy; i++) {
                        $('#psmDefault').append(html);
                        $('.' + OrderDetail.OrderNo).html(pageNo);
                    }
                   
                    html = '';
                }
            }
           
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });

}
function FnManageDueOrder(OpenBalance, html, OrderDetail) {
    if (OpenBalance.length > 0) {
        var DueAmount = 0, Aging = 0, AmtPaid = 0, GrandTotal = 0;
        html += '<p  style="page-break-before: always"></p><br/><fieldset><table class="table tableCSS" style="width: 100%;border-collapse:collapse;">';
        html += '<tr><td style="font-size: 20px; text-align: center">Open Balance</td></tr>';
        html += '<tr><td style="font-size: 2blob:file:///84a92579-fc6d-4a72-bc06-863a84d180380px; text-align: center"><span id="lblCustomerName">' + OrderDetail.CustomerName + '</span></td></tr>';
        html += '</table>';
        html += '<table class="table tableCSS" id="tblduePayment"  style="width:100%;border-collapse:collapse;">';
        html += '<thead>';
        html += '<tr><td class="OrderNo  center" style="width: 20%">Order No</td><td class="OrderDate center" style="width: 20%">Order Date</td><td class="GrandTotal right" style="width: 20%;">Order Amount</td><td class="AmtPaid right" style="width: 20%;">Paid Amount</td><td class="AmtDue right">Open Amount</td><td class="Aging center">Aging (Days)</td></tr>';
        html += '</thead>';
        html += '<tbody>';
        for (var m = 0; m < OpenBalance.length; m++) {
            var DueOrder = OpenBalance[m];

            DueAmount += DueOrder.AmtDue;
            Aging += DueOrder.Aging;
            AmtPaid += DueOrder.AmtPaid;
            GrandTotal += DueOrder.GrandTotal;

            html += '<tr><td style="text-align: center">' + DueOrder.OrderNo + '</td><td style="text-align: center">' + DueOrder.OrderDate + '</td><td style="text-align: right">' + parseFloat(DueOrder.GrandTotal).toFixed(2) + '</td><td style="text-align: right">' + parseFloat(DueOrder.AmtPaid).toFixed(2) + '</td><td style="text-align: right">' + parseFloat(DueOrder.AmtDue).toFixed(2) + '</td><td style="text-align: center">' + DueOrder.Aging + '</td>';
            html += '</tr>';

        }
        html += '</tbody > ';
        html += '<tfoot><tr><td style="width: 10%" colspan="2">Total</td><td id="FAmtPaid" style="text-align: right">' + parseFloat(GrandTotal).toFixed(2) + '</td><td id="Td1" style="text-align: right">' + parseFloat(AmtPaid).toFixed(2) + '</td>';

        html += '<td id="FAmtDue" style="text-align: right">' + parseFloat(DueAmount).toFixed(2) + '</td><td class="center"></td>';
        html += '</tr></tfoot>';
        html += '</table></fieldset>';
        html += '</fieldset>';
    }
    return html;
}
function getCreditData(CreditDetail, html, Company) {

    html += '<p style="page-break-before: always"></p><fieldset id="credit"><table class="table tableCSS" style="width:100%;border-collapse:collapse;"><tbody><tr><td colspan="6" style="text-align: center">';
    html += '<span style="font-size: 20px;">' + CreditDetail.CustomerName + ' -  ' + CreditDetail.OrderNo + ' (Page 1 of #########)</span><span style="font-size: 20px;float:right" ><b><i>CREDIT MEMO</i></b></span>';
    html += '</td></tr><tr><td rowspan="2" style="width: 80px !important;"><img src="../Img/logo/' + Company.Logo + '" class="img-responsive"></td>';
    html += '<td>' + Company.CompanyName + '</td>';
    html += '<td class="tdLabel" style="white-space: nowrap">Customer ID</td><td>' + CreditDetail.CustomerId + '</td>';
    html += '<td class="tdLabel" style="white-space: nowrap">Sales Rep</td><td>' + CreditDetail.SalesPerson + '</td></tr>';
    html += '<tr><td><span>' + Company.Address + '</span></td><td class="tdLabel" style="white-space: nowrap">Customer Contact</td>';
    html += '<td style="white-space: nowrap">' + CreditDetail.ContactPersonName + '</td><td class="tdLabel">Credit No </td>';
    html += '<td>' + CreditDetail.OrderNo + '</td></tr><tr><td><span>' + Company.Website + '</span></td>';
    html += '<td>Phone:<span>' + Company.MobileNo + '</span></td><td class="tdLabel">Contact No</td>';
    html += '<td>' + CreditDetail.Contact + '</td><td class="tdLabel" style="white-space: nowrap">Credit Date</td>';
    html += '<td style="white-space: nowrap">' + CreditDetail.OrderDate + '</td></tr>';
    html += '<tr><td>' + Company.EmailAddress + '</td><td>Fax: ' + Company.FaxNo + '</td><td class="tdLabel">Terms</td>';
    html += '<td><b>' + CreditDetail.TermsDesc + '</b></td>';
    if (CreditDetail.shipVia) {
        html += '<td class="tdLabel">Ship Via</td><td>' + CreditDetail.shipVia + '</td>'
    } else {
        html += '<td class="tdLabel">Ship Via</td><td></td>'
    }

    html += '</tr>'
    if (CreditDetail.BusinessName != '' && CreditDetail.BusinessName != undefined) {
        html += '<tr><td><span class="tdLabel">Business Name</span></td><td>' + CreditDetail.BusinessName + '</td></tr>';
    }
    html += '<tr><td><span class="tdLabel">Shipping Address</span></td><td colspan="5">' + CreditDetail.ShipAddr + '</td></tr>';
    html += '<tr><td><span class="tdLabel">Billing Address</span></td><td colspan="5">' + CreditDetail.BillAddr + '</td></tr>';
    html += '</tbody></table>';
    html += '<table class="table tableCSS tblProduct1" id="tblProduct"  style="width:100%;border-collapse:collapse;">';
    html += '<thead><tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
    html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
    html += '<td class="Barcode" style="width: 10px">UPC</td>';
    html += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
    html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
    var catId, catName, Qty = 0, total = 0.00, count = 1, totalPrice = 0.00, TotalQty = 0;
    var pageNo = 1, rowcount = 0;

    var CreditItemList = CreditDetail.CreditItems;
    for (var c = 0; c < CreditItemList.length; c++) {
        var Items = CreditItemList[c];
        rowcount = parseInt(rowcount) + 1;
        if (parseInt(Items.QtyShip) == 0) {
            totalPrice = 0.00;
        } else {
            totalPrice = parseFloat(Items.NetPrice);
        }
        if (count == 1 || Items.CategoryId == catId) {
            html += "<tr><td style='text-align:center'>" + Items.QtyShip + "</td><td style='text-align:center'>" + Items.UnitType + "</td><td style='text-align:center'>" + Items.ProductId + "</td><td>" + Items.ProductName.replace(/&quot;/g, "\'") + "</td>";
            if (Items.Barcode != undefined) {
                html += "<td>" + Items.Barcode + "</td>";
            } else {
                html += "<td></td>";
            }

            html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(Items.UnitPrice).toFixed(2) + "</td><td style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(totalPrice).toFixed(2) + "</td>";
            html += "</tr>";
            catId = Items.CategoryId;
            catName = Items.CategoryName;
            total += Number(totalPrice);
            Qty += Number(Items.QtyShip);
            TotalQty += Number(Items.QtyShip);
            if (pageNo == 1 && count == 10 && CreditItemList.length > 10) {
                pageNo = Number(pageNo) + 1;
                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                html += '<table class="table tableCSS tblProduct1" id="tblProduct" style="width:100%;border-collapse:collapse;"><thead>';
                html += "<tr><td colspan='9' style='text-align:center'>" + CreditDetail.CustomerName + ' -  ' + CreditDetail.OrderNo + " (Page " + pageNo + " Of #########)</td></tr>";
                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                html += '<td class="Barcode" style="width: 10px">UPC</td>';
                html += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


            } else if (((count - 10) % 20) == 0) {
                pageNo = Number(pageNo) + 1;
                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                html += '<table class="table tableCSS tblProduct1" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                html += "<tr><td colspan='9' style='text-align:center'>" + CreditDetail.CustomerName + ' -  ' + CreditDetail.OrderNo + " (Page " + pageNo + " Of #########)</td></tr>";
                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                html += '<td class="Barcode" style="width: 10px">UPC</td>';
                html += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';

            }
        }
        else {
            html += "<tr style='background: #ccc;font-weight:700;'><td class='totalQty'  style='text-align:center'>" + Qty + "</td><td  style='text-align:center'>" + catId + "</td><td colspan='4'  style='text-align:center'>" + catName + "</td><td style='text-align:right;'>" + parseFloat(total).toFixed(2) + "</td></tr>";
            html += "<tr><td  style='text-align:center'>" + Items.QtyShip + "</td><td  style='text-align:center'>" + Items.UnitType + "</td><td  style='text-align:center'>" + Items.ProductId + "</td><td>" + Items.ProductName.replace(/&quot;/g, "\'") + "</td>";
            html += "<td>" + Items.Barcode + "</td>";
            html += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(Items.UnitPrice).toFixed(2) + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + parseFloat(totalPrice).toFixed(2) + "</td>";
            html += "</tr>";
            Qty = 0;
            total = 0.00;

            if (pageNo == 1 && count == 10 && CrediItem.length > 10) {
                pageNo = Number(pageNo) + 1;
                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                html += '<table class="table tableCSS tblProduct2" id="tblProduct1"  style="width:100%;border-collapse:collapse;"><thead>';
                html += "<tr><td colspan='9' style='text-align:center'>" + CreditDetail.CustomerName + ' -  ' + CreditDetail.OrderNo + " (Page " + pageNo + " Of #########)</td></tr>";
                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


            } else if (((count - 10) % 20) == 0) {
                pageNo = Number(pageNo) + 1;
                html += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                html += '<table class="table tableCSS tblProduct2" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                html += "<tr><td colspan='9' style='text-align:center'>" + CreditDetail.CustomerName + ' -  ' + CreditDetail.OrderNo + " (Page " + pageNo + " Of #########)</td></tr>";
                html += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                html += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                html += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
                html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';

            }
            catId = Items.CategoryId;
            catName = Items.CategoryName;
            total += parseFloat(totalPrice).toFixed(2);
            Qty += Number(Items.QtyShip);
            TotalQty += Number(Items.QtyShip);
        }

    }
    count++;

    html += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty'  style='text-align:center'>" + Qty + "</td><td  style='text-align:center'>" + catId + "</td><td colspan='4'  style='text-align:center'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
    html += '</tbody></table>';
    html += '<table style="width: 100%;border-collapse:collapse;"><tbody><tr><td style="width: 33%;padding:0"></td><td style="width: 33%;padding:0"></td><td style="width: 33%;padding:0"><table class="table tableCSS">';
    html += '<tbody><tr><td class="tdLabel">Total Qty</td><td style="text-align: right;">' + TotalQty + '</td></tr>';
    html += '<tr><td class="tdLabel">Sub Total</td><td style="text-align: right;">' + parseFloat(CreditDetail.GrandTotal).toFixed(2) + '</td></tr>';
    html += '<tr><td class="tdLabel">Discount</td><td style="text-align: right;">' + parseFloat(CreditDetail.OverallDisc).toFixed(2) + '</td></tr>';
    if (parseFloat(CreditDetail.TotalTax) > 0) {
        html += '<tr><td class="tdLabel">' + CreditDetail.TaxPrintLabel + '</td><td style="text-align: right;">' + parseFloat(CreditDetail.TotalTax).toFixed(2) + '</td></tr>';
    }
    if (parseFloat(CreditDetail.MLTax) > 0) {
        html += '<tr><td class="tdLabel">ML Qty</td><td style="text-align: right;">' + parseFloat(CreditDetail.MLQty).toFixed(2) + '</td></tr>';
        html += '<tr><td class="tdLabel" style="white-space: nowrap;" >' + CreditDetail.MLTaxPrintLabel + '</td><td style="text-align: right;">' + parseFloat(CreditDetail.MLTax).toFixed(2) + '</td></tr>';
    }
    if (parseFloat(CreditDetail.AdjustmentAmt) != 0) {
        html += '<tr><td class="tdLabel">Adjustment</td><td style="text-align: right;">' + parseFloat(CreditDetail.AdjustmentAmt).toFixed(2) + '</td></tr>';
    }
    html += '<tr><td class="tdLabel">Grand Total</td><td style="text-align: right;">' + parseFloat(CreditDetail.TotalAmount).toFixed(2) + '</td></tr></tbody>';
    html += '</table></tbody></table></fieldset>';
    html = html.replace('#########', pageNo);
    return html;
}


function getOrderData_Template1(OrderAutoId, countCopy) {
    $.ajax({
        type: "POST",
        async: false,
        url: "DriverPackagePrint.aspx/getBulkOrderDataTempate2",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
                return;
            }
            var topHtml = ``;
            var orderData = $.parseJSON(response.d);
            $.each(orderData, function (index, order) {
                if (index == 0) {
                    topHtml = `<p style='page-break-before: always'></p>`
                }
                topHtml += `<div class="row" style="font-family:cursive !important;font-size: 16px;width:100%!Important">
        <center><span style="text-align:center;font-weight:bold;font-size:20px;">Sales Order</span></center>
    </div>`;

                topHtml += `<fieldset><table class="table tableCSS tblProduct" id="tblProduct" style="font-family:cursive !important;font-size: 16px"><thead><tr>
                            <tr><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>
                            <td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td><td class="Qty" style="width: 10px">Qty</td>
                            <td class="UnitPrice" style="text-align: right; width: 10px">Price</td>
                            <td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>`;



                var catId, catName, productname = '', Qty = 0, total = 0.00, count = 1, tr, totalPrice = 0.00;
                var pageNo = 1, rowcount = 0, TotalMLTax = 0.00, TotalMLQty = 0.00;
                var orderQty = 0; var total = 0;
                var productdetails;
                if (order.Status == 11) {
                    productdetails = order.DelItem;
                } else {
                    productdetails = order.notDelItem;
                }
                $.each(productdetails, function (index, item) {

                    orderQty = item.QtyShip;
                    if (parseInt(item.QtyShip) == 0) {
                        totalPrice = 0.00;
                    } else {
                        totalPrice = parseFloat(item.NetPrice);
                    }

                    rowcount = parseInt(rowcount) + 1;
                    if (item.IsExchange == 1) {
                        productname = item.ProductName + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Exchange)' + "</span>";
                    }
                    else if (item.isFreeItem == 1) {
                        productname = item.ProductName + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Free)' + "</span>";
                    }
                    else {
                        productname = item.ProductName;
                    }

                    total += totalPrice;

                    topHtml += `<tr>
                            <td style="text-align:center">`+ item.ProductId + `</td>
                            <td>` + productname.replace(/&quot;/g, "\'") + `</td>
                            <td  style="text-align:center">` + item.UnitType + `</td>
                             <td  style="text-align:center">` + orderQty + `</td>
                            <td style='text-align: right; width: 10px; white-space: nowrap'>` + item.UnitPrice + `</td>
                            <td style='text-align: right; width: 10px; white-space: nowrap'>` + totalPrice.toFixed(2) + `</td>
                            </tr>`;
                });

                topHtml += `</tbody><tfoot>
                        <tr>
                        <td colspan='2'></td><td colspan='3' style='text-align:center'><b>Sub Total</b></td><td style='text-align:right'>` + parseFloat(total).toFixed(2) + `</td></tr>`;
                if (order.OverallDiscAmt != 0) {
                    topHtml += `<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Discount</b></td><td style='text-align:right'>` + parseFloat(order.OverallDiscAmt).toFixed(2) + `</td></tr>`;
                }
                if (order.DeductionAmount != 0) {
                    topHtml += `<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Credit Memo</b></td><td style='text-align:right'>` + parseFloat(order.DeductionAmount).toFixed(2) + `</td></tr>`
                }
                if (order.AdjustmentAmt != 0) {
                    topHtml += `<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Adjustment</b></td><td style='text-align:right'>` + parseFloat(order.AdjustmentAmt).toFixed(2) + `</td></tr>`
                }
                topHtml += `<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Grand Total</b></td><td style='text-align:right'>` + parseFloat(order.PayableAmount).toFixed(2) + `</td></tr>`
                topHtml += `</tfoot>'</table></fieldset>`;
                topHtml += `<p  style='page-break-before: always'></p>`;
                //topHtml += '</div>';
                for (var i = 1; i <= countCopy; i++) {
                    $('#psmDefault').append(topHtml);
                }
                topHtml = '';
            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}



/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData_Template2(OrderAutoId, countCopy) {
    $.ajax({
        type: "POST",
        async: false,
        url: "DriverPackagePrint.aspx/getBulkOrderDataTempate2",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
                return;
            }
            var topHtml = ``;
            var orderData = $.parseJSON(response.d);
            $.each(orderData, function (index, order) {
                if (index == 0) {
                    topHtml = `<p style='page-break-before: always'></p>`
                }
                topHtml += `<div class="row" style="font-family:cursive !important;width:100% !Important">
        <center><span style="text-align:center;font-weight:bold;font-size:20px;">Sales Order</span></center>
    </div><fieldset  style="font-family: Arial;">
                                <table class="table-borderless" style="font-family:cursive !important;font-size: 16px;width:100%;border-collapse:collapse;">
                                    <tbody>

                                        <tr>
                                            <td class="tdLabel" style="white-space: nowrap;width:5%;">Customer Name :-</td>
                                            <td id="CustomerName" style="white-space: nowrap;width:35%;">`+ order.CustomerName + `</td>
                                            <td class="tdLabel" style="white-space: nowrap;width:5%;">Order No :-</td>
                                            <td id="OrderNo" style="white-space: nowrap;width:15%;">`+ order.OrderNo + `</td>
                                        </tr>
                                        <tr>
                                            <td class="tdLabel" style="white-space: nowrap;width:5%;">Address :-</td>
                                            <td id="Address" style="white-space: nowrap;width:35%;">`+ order.ShipAddr + `</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </fieldset>`;

                topHtml += `<fieldset><table class="table tableCSS tblProduct" id="tblProduct" style="font-family:cursive !important;font-size:16px;width:100%;border-collapse:collapse;"><thead><tr>
                            <tr><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>
                            <td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td><td class="Qty" style="width: 10px">Qty</td>
                            <td class="UnitPrice" style="text-align: right; width: 10px">Price</td>
                            <td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>`;



                var catId, catName, productname = '', Qty = 0, total = 0.00, count = 1, tr, totalPrice = 0.00;
                var pageNo = 1, rowcount = 0, TotalMLTax = 0.00, TotalMLQty = 0.00;
                var orderQty = 0; var total = 0;
                var productdetails;
                if (order.Status == 11) {
                    productdetails = order.DelItem;
                } else {
                    productdetails = order.notDelItem;
                }
                $.each(productdetails, function (index, item) {

                    orderQty = item.QtyShip;
                    if (parseInt(item.QtyShip) == 0) {
                        totalPrice = 0.00;
                    } else {
                        totalPrice = parseFloat(item.NetPrice);
                    }

                    rowcount = parseInt(rowcount) + 1;
                    if (item.IsExchange == 1) {
                        productname = item.ProductName + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Exchange)' + "</span>";
                    }
                    else if (item.isFreeItem == 1) {
                        productname = item.ProductName + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Free)' + "</span>";
                    }
                    else {
                        productname = item.ProductName;
                    }

                    total += totalPrice;

                    topHtml += `<tr>
                              <td style="text-align:center">`+ item.ProductId + `</td>
                            <td>` + productname.replace(/&quot;/g, "\'") + `</td>
                            <td  style='text-align:center'>` + item.UnitType + `</td>
                             <td style="text-align:center">` + orderQty + `</td>
                            <td style='text-align: right; width: 10px; white-space: nowrap'>` + item.UnitPrice + `</td>
                            <td style='text-align: right; width: 10px; white-space: nowrap'>` + totalPrice.toFixed(2) + `</td>
                            </tr>`;
                });

                topHtml += `</tbody><tfoot>
                        <tr>
                        <td colspan='2'></td><td colspan='3' style='text-align:center'><b>Sub Total</b></td><td style='text-align:right'>` + parseFloat(total).toFixed(2) + `</td></tr>`;
                if (order.OverallDiscAmt != 0) {
                    topHtml += `<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Discount</b></td><td style='text-align:right'>` + parseFloat(order.OverallDiscAmt).toFixed(2) + `</td></tr>`;
                }
                if (order.DeductionAmount != 0) {
                    topHtml += `<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Credit Memo</b></td><td style='text-align:right'>` + parseFloat(order.DeductionAmount).toFixed(2) + `</td></tr>`
                }
                if (order.AdjustmentAmt != 0) {
                    topHtml += `<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Adjustment</b></td><td style='text-align:right'>` + parseFloat(order.AdjustmentAmt).toFixed(2) + `</td></tr>`
                }
                topHtml += `<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Grand Total</b></td><td style='text-align:right'>` + parseFloat(order.PayableAmount).toFixed(2) + `</td></tr>`
                topHtml += `</tfoot>'</table> </fieldset>`;
                topHtml += `<p  style='page-break-before: always'></p>`;
                for (var i = 1; i <= countCopy; i++) {
                    $('#psmDefault').append(topHtml);
                }
                topHtml = '';
            });
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function getOrderDataForWPAPA(OrderAutoId, countCopy) {
    $.ajax({
        type: "POST",
        async: false,
        url: "DriverPackagePrint.aspx/GetPackingOrderPrint",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            var wpahtml = '';
            if (response.d == 'SessionExpired') {
                location.href = '/';
            }
            var xmldoc = $.parseXML(response.d);
            var Company = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");
            var items = $(xmldoc).find("Table2");
            var DuePayment = $(xmldoc).find("Table3");
            var CreditMemoNo = $(xmldoc).find("Table4");
            var check = 0;
            var DueAmount = 0, Aging = 0, AmtPaid = 0, GrandTotal = 0; var totalCalculatedTax = 0;

            wpahtml += ' <div class="InvContent" style="font-family: Arial;">'
            wpahtml += '<fieldset>'
            wpahtml += '<table class="table tableCSS"  style="width:100%;border-collapse:collapse;">'
            wpahtml += '<tbody>'
            wpahtml += '<tr>'
            wpahtml += '<td colspan="6" style="text-align: center">'
            wpahtml += ' <span style="font-size: 20px;" id="PageHeading">' + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + ' (Page 1 of <span class="TotalPage"></span>)</span>'
            wpahtml += '<span style="font-size: 20px;float:right;font-weight:600"><i>Sales Order</i></span>'
            wpahtml += ' </td>'
            wpahtml += ' </tr>'
            wpahtml += '<tr>'
            wpahtml += '<td rowspan="2" style="width: 80px !important;">'
            wpahtml += '<img src="../images/LOGO_transparent.png" id="logo" class="img-responsive">'
            wpahtml += '</td>'
            wpahtml += ' <td><span id="CompanyName">' + $(Company).find("CompanyName").text() + '</span></td>'
            wpahtml += ' <td class="tdLabel" style="white-space: nowrap">Customer ID</td>'
            wpahtml += '<td id="acNo" style="white-space: nowrap">' + $(order).find("CustomerId").text() + '</td>'
            wpahtml += '<td class="tdLabel" style="white-space: nowrap">Sales Rep</td>'
            wpahtml += '<td id="salesRep" style="white-space: nowrap">' + $(order).find("SalesPerson").text() + '</td>'
            wpahtml += ' </tr>'
            wpahtml += ' <tr>'
            wpahtml += ' <td><span id="Address">' + $(Company).find("Address").text() + '</span></td>'
            wpahtml += '<td class="tdLabel" style="white-space: nowrap">Customer Contact</td>'
            wpahtml += '<td id="storeName" style="white-space: nowrap">' + $(order).find("ContactPersonName").text() + '</td>'
            wpahtml += '<td class="tdLabel">Order No </td>'
            wpahtml += '<td id="so">' + $(order).find("OrderNo").text() + '</td>'
            wpahtml += '</tr>'
            wpahtml += '<tr>'
            wpahtml += ' <td><span id="Website">' + $(Company).find("Website").text() + '</span></td>'
            wpahtml += ' <td>Phone:<span id="Phone">' + $(Company).find("MobileNo").text() + '</span></td>'
            wpahtml += ' <td class="tdLabel">Contact No</td>'
            wpahtml += ' <td id="ContPerson">' + $(order).find("Contact").text() + '</td>'
            wpahtml += ' <td class="tdLabel" style="white-space: nowrap">Order Date</td>'
            wpahtml += '<td id="soDate" style="white-space: nowrap">' + $(order).find("OrderDate").text() + '</td>'
            wpahtml += ' </tr>'
            wpahtml += '<tr>'
            wpahtml += ' <td><span id="EmailAddress">' + $(Company).find("EmailAddress").text() + '</span></td>'
            wpahtml += ' <td>Fax:<span id="FaxNo">' + $(Company).find("FaxNo").text() + '</span></td>'
            wpahtml += '<td class="tdLabel">Terms</td>'
            wpahtml += ' <td style="font-size: 13px"><b><span id="terms">' + $(order).find("TermsDesc").text() + '</span></b> </td>'
            wpahtml += ' <td class="tdLabel">Ship Via</td>'
            wpahtml += '<td id="shipVia">' + $(order).find("shipVia").text() + '</td>'
            wpahtml += '</tr>'
            wpahtml += ' <tr style="display: none">'
            wpahtml += ' <td><span class="tdLabel">Business Name:</span></td>'
            wpahtml += '<td colspan="5"><span id="BusinessName">' + $(order).find("BusinessName").text() + '</span></td>'
            wpahtml += '</tr>'
            wpahtml += '  <tr>'
            wpahtml += '<td><span class="tdLabel">Shipping Address</span></td>'
            wpahtml += ' <td colspan="5"><span id="shipAddr" >' + $(order).find("ShipAddr").text() + '</span></td>'
            wpahtml += '</tr>'
            wpahtml += '  <tr>'
            wpahtml += '<td><span class="tdLabel">Billing Address</span></td>'
            wpahtml += '<td colspan="5"><span id="billAddr">' + $(order).find("BillAddr").text() + '</span></td>'
            wpahtml += '</tr>'
            wpahtml += '</tbody>'
            wpahtml += '</table>'
            wpahtml += '</fieldset>'

            wpahtml += ' <div style="padding:10px">'
            wpahtml += ' <table class="table tableCSS" id="tblProduct" style="padding:10px;width:100%;border-collapse:collapse;">'
            wpahtml += ' <thead>'
            wpahtml += '  <tr>'
            wpahtml += '<td class="Qty" style="width: 10px">Qty</td>'
            wpahtml += ' <td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>'
            wpahtml += ' <td class="Qty" style="width: 10px">U/M</td>'
            wpahtml += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td>'
            wpahtml += '<td class="ProName" style="white-space: nowrap">Product Name</td>'
            wpahtml += '<td class="Barcode" style="width: 10px">Barcode</td>'
            wpahtml += ' <td class="SRP" style="text-align: right; width: 10px">SRP</td>'
            wpahtml += ' <td class="GP" style="text-align: right; width: 10px">GP(%)</td>'
            wpahtml += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>'
            wpahtml += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td>'
            wpahtml += ' </tr>'
            wpahtml += ' </thead>'
            wpahtml += '<tbody>'

            var catId, catName, productname = '', Qty = 0, total = 0.00, count = 1, tr, totalPrice = 0.00;
            var pageNo = 1, rowcount = 0, TotalMLTax = 0.00, TotalMLQty = 0.00, ordQty = 0, iDisc=0.00;
            $.each(items, function () {
                rowcount = parseInt(rowcount) + 1;
                iDisc = $(this).find("Del_discount").text();
                if (iDisc == '0.00' || iDisc == '0') {
                    iDisc = "";
                }
                else {
                    if (iDisc != '') {
                        iDisc = iDisc.toString() + " %";
                    }
                }
                if (parseInt($(order).find("stautoid").text()) <= 2) {
                    ordQty = parseInt($(this).find("RequiredQty").text());
                }
                else {
                    ordQty = parseInt($(this).find("QtyShip").text());
                }
                if (parseInt($(this).find("QtyShip").text()) == 0) {
                    totalPrice = 0.00;
                } else {
                    totalPrice = parseFloat($(this).find("NetPrice").text());
                }
                if ($(this).find("IsExchange").text() == 1) {
                    productname = $(this).find("ProductName").text() + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Exchange)' + "</span>";
                }
                else if ($(this).find("isFreeItem").text() == 1) {
                    productname = $(this).find("ProductName").text() + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Free)' + "</span>";
                }
                else {
                    productname = $(this).find("ProductName").text();
                }
                if (count == 1 || $(this).find("CategoryId").text() == catId) {

                    wpahtml += "<tr><td  style='text-align:center'>" + ordQty + "</td><td style='text-align:center'>" + $(this).find("UnitType").text() + "</td><td style='text-align:center'>" + $(this).find("TotalPieces").text() + "</td><td style='text-align:center'>" + $(this).find("ProductId").text() + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                    wpahtml += "<td>" + $(this).find("Barcode").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("SRP").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("PrintGP").text() + "</td>";
                    wpahtml += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + iDisc + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + iDisc + "</td><td style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                    wpahtml += "</tr>";
                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    total += Number(totalPrice);
                    Qty += Number(ordQty);
                    TotalMLQty += Number($(this).find("TotalMLQty").text());
                    TotalMLTax += Number($(this).find("TotalMLTax").text());
                    if (pageNo == 1 && count == 14 && items.length > 14) {
                        pageNo = Number(pageNo) + 1;
                        wpahtml += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                        wpahtml += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                        wpahtml += "<tr><td colspan='11' style='text-align:center'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        wpahtml += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        wpahtml += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        wpahtml += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                        wpahtml += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        wpahtml += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


                    } else if (((count - 14) % 20) == 0) {
                        pageNo = Number(pageNo) + 1;
                        wpahtml += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                        wpahtml += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                        wpahtml += "<tr><td colspan='11' style='text-align:center'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        wpahtml += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        wpahtml += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        wpahtml += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                        wpahtml += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        wpahtml += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';

                    }

                }
                else {
                    count++;
                    wpahtml += "<tr style='background: #ccc;font-weight:700;'><td class='totalQty'  style='text-align:center'>" + Qty + "</td><td  style='text-align:center'>" + catId + "</td><td colspan='8'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
                    if (totalCalculatedTax > 0) {
                        wpahtml += "<tr style='background:#ccc;font-weight:700;'><td colspan='9' style='white-space: nowrap;text-align:left;'>" + $(order).find("TaxPrintLabel").text() + "</td><td style='text-align:right'>" + parseFloat(totalCalculatedTax).toFixed(2) + "</td></tr>";

                    }
                    if (TotalMLTax > 0) {
                        wpahtml += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty' colspan='2'></td><td style='white-space: nowrap'>ML Qty</td><td style='text-align:right'>" + TotalMLQty.toFixed(2) + "</td><td  colspan='4' style='white-space: nowrap;'>" + PrintLabel + "</td><td colspan='2' style='text-align:right'>" +
                            TotalMLTax.toFixed(2) + "</td></tr>";
                        count++;
                    }
                    wpahtml += "<tr><td  style='text-align:center'>" + ordQty + "</td><td style='text-align:center'>" + $(this).find("UnitType").text() + "</td><td style='text-align:center'>" + $(this).find("TotalPieces").text() + "</td><td style='text-align:center'>" + $(this).find("ProductId").text() + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td>";
                    wpahtml += "<td>" + $(this).find("Barcode").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("SRP").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("PrintGP").text() + "</td>";
                    wpahtml += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + iDisc + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                    wpahtml += "</tr>";
                    Qty = 0;
                    total = 0.00;
                    TotalMLQty = 0.00;
                    TotalMLTax = 0.00;
                    if (pageNo == 1 && count == 14 && items.length > 14) {
                        pageNo = Number(pageNo) + 1;
                        wpahtml += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                        wpahtml += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                        wpahtml += "<tr><td colspan='11' style='text-align:center'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        wpahtml += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        wpahtml += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        wpahtml += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                        wpahtml += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        wpahtml += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


                    } else if (((count - 14) % 20) == 0) {
                        pageNo = Number(pageNo) + 1;
                        wpahtml += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                        wpahtml += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                        wpahtml += "<tr><td colspan='11' style='text-align:center'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPage'></span>)</td></tr>";
                        wpahtml += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        wpahtml += '<td class="Qty" style="width: 10px">U/M</td><td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td class="ProName" style="white-space: nowrap">Item</td>';
                        wpahtml += '<td class="Barcode" style="width: 10px">UPC</td><td class="SRP" style="text-align: right; width: 10px">SRP</td>';
                        wpahtml += '<td class="GP" style="text-align: right; width: 10px">GP(%)</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        wpahtml += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';

                    }
                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    total += Number(totalPrice);
                    Qty += ordQty;
                    TotalMLQty += Number($(this).find("TotalMLQty").text());
                    TotalMLTax += Number($(this).find("TotalMLTax").text());
                }

                count++;
            });
            wpahtml += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty'  style='text-align:center'>" + Qty + "</td><td>" + catId + "</td><td colspan='8'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
            if (totalCalculatedTax > 0) {
                wpahtml += "<tr style='background:#ccc;font-weight:700;'><td colspan='9' style='white-space: nowrap;text-align:left;'>" + $(order).find("TaxPrintLabel").text() + "</td><td style='text-align:right'>" + parseFloat(totalCalculatedTax).toFixed(2) + "</td></tr>";
            }
            if (TotalMLTax > 0) {
                wpahtml += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty' colspan='2'></td><td style='white-space: nowrap'>ML Qty</td><td style='text-align:right'>" + TotalMLQty.toFixed(2) + "</td><td  colspan='4' style='white-space: nowrap;'>" + PrintLabel + "	</td><td colspan='2' style='text-align:right'>" +
                    TotalMLTax.toFixed(2) + "</td></tr>";
            }
            wpahtml += '</tbody></table></div>';

            wpahtml += '  <fieldset>'
            wpahtml += ' <table style="width: 100%;border-collapse:collapse;">'
            wpahtml += ' <tbody>'
            wpahtml += ' <tr>'
            wpahtml += ' <td style="width: 66%;padding: 0 10px 0 0;">'
            wpahtml += '<div id="TC">' + $(Company).find("TermsCondition").text() + '</div>'
            wpahtml += '</td>'
            wpahtml += ' <td style="width: 33%;padding:0">'
            wpahtml += '<table class="table tableCSS"  style="width:100%;border-collapse:collapse;">'
            wpahtml += ' <tbody>'
            wpahtml += ' <tr>'
            wpahtml += ' <td class="tdLabel">Total Qty</td>'
            wpahtml += ' <td id="totalQty" style="text-align: right;">' + Qty + '</td>'
            wpahtml += '</tr>'
            wpahtml += '<tr>'
            wpahtml += '<td class="tdLabel">Sub Total</td>'
            wpahtml += ' <td style="text-align: right;"><span id="totalwoTax">' + $(order).find("TotalAmount").text() + '</span></td>'
            wpahtml += ' </tr>'
            if (parseFloat($(order).find("OverallDiscAmt").text()) > 0) {
                wpahtml += '<tr>'
                wpahtml += ' <td class="tdLabel">Discount</td>'
                wpahtml += '<td style="text-align: right;"><span id="disc">' + '-' + $(order).find("OverallDiscAmt").text() + '</span></td>'
                wpahtml += ' </tr>'
            }
            if (parseFloat($(order).find("TotalTax").text()) > 0) {
                wpahtml += '<tr>'
                wpahtml += '  <td class="tdLabel" id="stateTaxPrintlabel">' + $(order).find("TaxPrintLabel").text() + '</td>'
                wpahtml += ' <td style="text-align: right;"><span id="tax">' + $(order).find("TotalTax").text() + '</span></td>'
                wpahtml += '</tr>'
            }
            if (parseFloat($(order).find("ShippingCharges").text()) > 0) {
                wpahtml += '<tr>'
                wpahtml += '<td class="tdLabel">Shipping</td>'
                wpahtml += '<td style="text-align: right;"><span id="shipCharges">' + $(order).find("ShippingCharges").text() + '</span></td>'
                wpahtml += ' </tr>'
            }
            if (parseFloat($(order).find("MLTax").text()) > 0) {
                wpahtml += '<tr>'
                wpahtml += ' <td class="tdLabel"  id="Prntlebel">ML Tax</td>'
                wpahtml += '<td style="text-align: right;"><span id="MLTax">' + $(order).find("MLTax").text() + '</span></td>'
                wpahtml += '</tr>'
            }
            if (parseFloat($(order).find("WeightTax").text()) > 0) {
                wpahtml += ' <tr>'
                wpahtml += ' <td class="tdLabel" id="weightTaxPrintLabel">Weight Tax</td>'
                wpahtml += '<td style="text-align: right;"><span id="WeightTax">' + $(order).find("WeightTax").text() + '</span></td>'
                wpahtml += '</tr>'
            }
            if (parseFloat($(order).find("AdjustmentAmt").text()) > 0) {
                wpahtml += ' <tr>'
                wpahtml += '<td class="tdLabel">Adjustment</td>'
                wpahtml += '<td style="text-align: right;"><span id="spAdjustment">' + $(order).find("AdjustmentAmt").text() + '</span></td>'
                wpahtml += '</tr>'
            }
            wpahtml += '<tr>'
            wpahtml += '<td class="tdLabel">Grand Total</td>'
            wpahtml += '<td style="text-align: right;"><span id="grandTotal">' + $(order).find("GrandTotal").text() + '</span></td>'
            wpahtml += ' </tr>'
            wpahtml += '<tr>'


            var CrNo = 'Credit Memo Applied ['
            $.each(CreditMemoNo, function (index) {
                if (index == CreditMemoNo.length - 1) {
                    CrNo += $(this).find("CreditNo").text() + "]";
                }
                else {
                    CrNo += $(this).find("CreditNo").text() + ",";
                }
            })
            $("#crmNo").html(CrNo);

            if (parseFloat($(order).find("DeductionAmount").text()) > 0) {
                wpahtml += ' <td class="tdLabel" id="crmNo">Credit memo</td>'
                wpahtml += '<td style="text-align: right;"><span id="spnCreditmemo">' + CrNo + '</span></td>'
            }
            wpahtml += '</tr>'
            wpahtml += '<tr>'
            if (parseFloat($(order).find("CreditAmount").text()) > 0) {
                wpahtml += ' <td class="tdLabel">Store Credit</td>'
                wpahtml += '<td style="text-align: right;"><span id="spnStoreCredit">' + $(order).find("CreditAmount").text() + '</span></td>'
                wpahtml += '</tr>'
            }
            wpahtml += '<tr>'
            wpahtml += '<td class="tdLabel">Payable Amount</td>'
            wpahtml += '<td style="text-align: right;"><span id="spnPayableAmount">' + $(order).find("PayableAmount").text() + '</span></td>'
            wpahtml += '</tr>'
            wpahtml += ' </tbody>'
            wpahtml += '</table>'
            wpahtml += '</td>'
            wpahtml += ' </tr>'
            wpahtml += ' </tbody>'
            wpahtml += '</table>'
            wpahtml += '</fieldset>'
            wpahtml += '<fieldset>'
            wpahtml += '<legend></legend>'
            wpahtml += ' <center>'
            wpahtml += ' <pre id="TC"></pre > '
            wpahtml += ' </center>'
            wpahtml += '</fieldset>'
            wpahtml += ' </div>'

            //Due Amount
            if (DuePayment.length > 0) {
                wpahtml += ' <div class="DueAmount  InvContent" style="display: none">'
                wpahtml += ' <p style="page-break-before: always"></p>'
                wpahtml += '<fieldset>'
                wpahtml += ' <table class="table tableCSS" style="width: 100%;border-collapse:collapse;">'
                wpahtml += '<tr>'
                wpahtml += '<td style="font-size: 20px; text-align: center">Open Balance</td>'
                wpahtml += '</tr>'
                wpahtml += '<tr>'
                wpahtml += ' <td style="font-size: 20px; text-align: center">'
                wpahtml += '<span id="lblCustomerName">' + $(order).find("CustomerName").text() + '</span>'
                wpahtml += '</td>'
                wpahtml += '</tr>'
                wpahtml += '</table>'
                wpahtml += ' <table class="table tableCSS" id="tblduePayment"  style="width:100%;border-collapse:collapse;">'
                wpahtml += '<thead>'
                wpahtml += '<tr>'
                wpahtml += '<td class="OrderNo  center" style="width: 20%">Order No</td>'
                wpahtml += '<td class="OrderDate center" style="width: 20%">Order Date</td>'
                wpahtml += '<td class="GrandTotal right" style="width: 20%;">Order Amount</td>'
                wpahtml += '<td class="AmtPaid right" style="width: 20%;">Paid Amount</td>'
                wpahtml += ' <td class="AmtDue right">Open Amount</td>'
                wpahtml += '<td class="Aging center">Aging (Days)</td>'
                wpahtml += ' </tr>'
                wpahtml += ' </thead>'
                wpahtml += '<tbody>'
                $(DuePayment).each(function (index) {
                    if (parseFloat($(this).find('AmtDue').text()) > 0) {
                        wpahtml += '<tr>';
                        wpahtml += '<td>' + Number(check) + 1 + '</td>';
                        wpahtml += '<td>' + $(this).find('OrderNo').text() + '</td>';
                        wpahtml += '<td>' + $(this).find('OrderDate').text() + '</td>';
                        wpahtml += '<td>' + $(this).find('AmtDue').text() + '</td>';
                        wpahtml += '<td>' + $(this).find('GrandTotal').text() + '</td>';
                        wpahtml += '<td>' + $(this).find('AmtPaid').text() + '</td>';
                        wpahtml += '<td>' + $(this).find('Aging').text() + '</td>';
                        wpahtml += '<td>' + $(this).find('AmtDue').text() + '</td>';
                        wpahtml += '<td>' + $(this).find('Aging').text() + '</td>';
                        wpahtml += '<td>' + $(this).find('AmtPaid').text() + '</td>';
                        Aging += parseInt();
                        AmtPaid += parseInt();
                        GrandTotal += parseInt($(this).find('GrandTotal').text());
                        check = check + 1;
                    }
                    wpahtml += '</tr>'
                });
                wpahtml += '</tbody>'
                wpahtml += ' <tfoot>'
                wpahtml += ' <tr>'
                wpahtml += '<td style="width: 10%" colspan="2">Total</td>'
                wpahtml += '<td id="FAmtPaid" style="text-align: right">' + GrandTotal.toFixed(2) + '</td>'
                wpahtml += '<td id="Td1" style="text-align: right">' + DueAmount.toFixed(2) + '</td>'
                wpahtml += '<td id="FAmtDue" style="text-align: right">' + AmtPaid.toFixed(2) + '</td>'
                wpahtml += '<td class="center"></td>'
                wpahtml += '</tr>'
                wpahtml += '</tfoot>'
                wpahtml += ' </table>'
                wpahtml += '  </fieldset>'
                wpahtml += '</div>'
                for (var i = 1; i <= countCopy; i++) {
                    $('#psmWPAPA').append(wpahtml);
                }
            }

            var CreditMemoDetails = $(xmldoc).find('Table4');
            if (CreditMemoDetails.length > 0) {
                $(CreditMemoDetails).each(function () {
                    getCreditDataForWPAPA($(this).find('CreditAutoId').text(), countCopy)
                });
                setTimeout(function () { window.print(); }, 200);
            } else {
                setTimeout(function () { window.print(); }, 200);
            }
        }
    })
}


function getCreditDataForWPAPA(CreditAutoId, countCopy) {
    $.ajax({
        type: "POST",
        async: false,
        url: "DriverPackagePrint.aspx/PrintCredit",
        data: "{'CreditAutoId':'" + CreditAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var Company = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");
            var items = $(xmldoc).find("Table2");
            var DuePayment = $(xmldoc).find("Table3");
            var gethtmlwhole = '';
            var pageNo = 1, rowcount = 0;
            gethtmlwhole += '<p style="page-break-before: always"></p><fieldset><table class="table tableCSS"  style="width:100%;border-collapse:collapse;"><tbody><tr><td colspan="6" style="text-align: center">';
            gethtmlwhole += '<span style="font-size: 20px;">' + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + ' (Page ' + pageNo + ' Of <span class="TotalPageC"></span>)<span style="float:right;"><b><i>CREDIT MEMO</i></b></span></span>';
            gethtmlwhole += '</td></tr><tr><td rowspan="2" style="width: 80px !important;"><img src="../Img/logo/' + $(Company).find("Logo").text() + '" class="img-responsive"></td>';
            gethtmlwhole += '<td>' + $(Company).find("CompanyName").text() + '</td>';
            gethtmlwhole += '<td class="tdLabel" style="white-space: nowrap">Customer ID</td><td>' + $(order).find("CustomerId").text() + '</td>';
            gethtmlwhole += '<td class="tdLabel" style="white-space: nowrap">Sales Rep</td><td>' + $(order).find("SalesPerson").text() + '</td></tr>';
            gethtmlwhole += '<tr><td><span>' + $(Company).find("Address").text() + '</span></td><td class="tdLabel" style="white-space: nowrap">Customer Contact</td>';
            gethtmlwhole += '<td style="white-space: nowrap">' + $(order).find("ContactPersonName").text() + '</td><td class="tdLabel">Credit No </td>';
            gethtmlwhole += '<td>' + $(order).find("OrderNo").text() + '</td></tr><tr><td><span>' + $(Company).find("Website").text() + '</span></td>';
            gethtmlwhole += '<td>Phone:<span>' + $(Company).find("MobileNo").text() + '</span></td><td class="tdLabel">Contact No</td>';
            gethtmlwhole += '<td>' + $(order).find("Contact").text() + '</td><td class="tdLabel" style="white-space: nowrap">Credit Date</td>';
            gethtmlwhole += '<td style="white-space: nowrap">' + $(order).find("OrderDate").text() + '</td></tr>';
            gethtmlwhole += '<tr><td>' + $(Company).find("EmailAddress").text() + '</td><td>Fax: ' + $(Company).find("FaxNo").text() + '</td><td class="tdLabel">Terms</td>';
            gethtmlwhole += '<td><b>' + $(order).find("TermsDesc").text() + '</b></td><td class="tdLabel">Ship Via</td><td>' + $(order).find("shipVia").text() + '</td></tr>';
            if ($(order).find("BusinessName").text() != '') {
                gethtmlwhole += '<tr><td><span class="tdLabel">Business Name:</span></td><td>' + $(order).find("BusinessName").text() + '</td></tr>';
            }
            gethtmlwhole += '<tr><td><span class="tdLabel">Shipping Address</span></td><td colspan="5" >' + $(order).find("ShipAddr").text() + '</td></tr>';
            gethtmlwhole += '<tr><td><span class="tdLabel">Billing Address</span></td><td colspan="5">' + $(order).find("BillAddr").text() + '</td></tr>';
            gethtmlwhole += '</tbody></table>';

            gethtmlwhole += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead><tr>';
            gethtmlwhole += '<thead><tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
            gethtmlwhole += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td colspan="3" class="ProName" style="white-space: nowrap">Item</td>';
            //gethtmlwhole += '<td class="Barcode" style="width: 10px">UPC</td>';
            gethtmlwhole += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
            gethtmlwhole += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
            var catId, catName, Qty = 0, total = 0.00, count = 1, tr, totalPrice = 0.00, TotalQty = 0, iDisc=0.00;

            $.each(items, function () {
                iDisc = $(this).find("Del_discount").text();
                if (iDisc == '0.00' || iDisc == '0') {
                    iDisc = "";
                }
                else {
                    if (iDisc != '') {
                        iDisc = iDisc.toString() + " %";
                    }
                }
                rowcount = parseInt(rowcount) + 1;
                if (parseInt($(this).find("QtyShip").text()) == 0) {
                    totalPrice = 0.00;
                } else {
                    totalPrice = parseFloat($(this).find("NetPrice").text());
                }
                if (count == 1 || $(this).find("CategoryId").text() == catId) {

                    gethtmlwhole += "<tr><td style='text-align:center'>" + $(this).find("QtyShip").text() + "</td><td style='text-align:center'>" + $(this).find("UnitType").text() + "</td><td style='text-align:center'>" + $(this).find("ProductId").text() + "</td><td colspan='3'>" + $(this).find("ProductName").text().replace(/&quot;/g, "\'") + "</td>";
                    //gethtmlwhole += "<td>" + $(this).find("Barcode").text() + "</td>";
                    gethtmlwhole += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + iDisc + "</td><td style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                    gethtmlwhole += "</tr>";
                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    total += Number(totalPrice);
                    Qty += Number($(this).find("QtyShip").text());
                    TotalQty += Number($(this).find("QtyShip").text());
                    if (pageNo == 1 && count == 10 && items.length > 10) {
                        pageNo = Number(pageNo) + 1;
                        gethtmlwhole += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                        gethtmlwhole += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                        gethtmlwhole += "<tr><td colspan='9' style='text-align:center'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPageC'></span>)</td></tr>";
                        gethtmlwhole += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        gethtmlwhole += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td colspan="3" class="ProName" style="white-space: nowrap">Item</td>';
                        //gethtmlwhole += '<td class="Barcode" style="width: 10px">UPC</td>';
                        gethtmlwhole += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        gethtmlwhole += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


                    } else if (((count - 10) % 20) == 0) {
                        pageNo = Number(pageNo) + 1;
                        gethtmlwhole += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                        gethtmlwhole += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                        gethtmlwhole += "<tr><td colspan='9' style='text-align:center'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPageC'></span>)</td></tr>";
                        gethtmlwhole += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        gethtmlwhole += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td colspan="3" class="ProName" style="white-space: nowrap">Item</td>';
                        //gethtmlwhole += '<td class="Barcode" style="width: 10px">UPC</td>';
                        gethtmlwhole += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        gethtmlwhole += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';
                    }

                } else {
                    gethtmlwhole += "<tr style='background: #ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='5'>" + catName + "</td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
                    gethtmlwhole += "<tr><td>" + $(this).find("QtyShip").text() + "</td><td>" + $(this).find("UnitType").text() + "</td><td>" + $(this).find("ProductId").text() + "</td><td colspan='3'>" + $(this).find("ProductName").text().replace(/&quot;/g, "\'") + "</td>";
                    //gethtmlwhole += "<td>" + $(this).find("Barcode").text() + "</td>";
                    gethtmlwhole += "<td  style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td><td  style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                    gethtmlwhole += "</tr>";
                    Qty = 0;
                    total = 0.00;

                    if (pageNo == 1 && count == 10 && items.length > 10) {
                        pageNo = Number(pageNo) + 1;
                        gethtmlwhole += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                        gethtmlwhole += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                        gethtmlwhole += "<tr><td colspan='9' style='text-align:center'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPageC'></span>)</td></tr>";
                        gethtmlwhole += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        gethtmlwhole += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td colspan="3" class="ProName" style="white-space: nowrap">Item</td>';
                        gethtmlwhole += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        gethtmlwhole += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';


                    } else if (((count - 10) % 20) == 0) {
                        pageNo = Number(pageNo) + 1;
                        gethtmlwhole += "'</tbody></table><p  style='page-break-before: always'></p><br/>";
                        gethtmlwhole += '<table class="table tableCSS tblProduct" id="tblProduct"  style="width:100%;border-collapse:collapse;"><thead>';
                        gethtmlwhole += "<tr><td colspan='9' style='text-align:center'>" + $(order).find("CustomerName").text() + ' -  ' + $(order).find("OrderNo").text() + " (Page " + pageNo + " Of <span class='TotalPageC'></span>)</td></tr>";
                        gethtmlwhole += '<tr><td class="Qty" style="width: 10px">Qty</td><td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td>';
                        gethtmlwhole += '<td class="ProId" style="width: 10px; white-space: nowrap">Product ID</td><td colspan="3" class="ProName" style="white-space: nowrap">Item</td>';
                        gethtmlwhole += '<td class="Barcode" style="width: 10px">UPC</td><td class="UnitPrice" style="text-align: right; width: 10px">Price</td><td class="UnitPrice" style="text-align: right; width: 10px">CD</td>';
                        gethtmlwhole += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';

                    }
                    catId = $(this).find("CategoryId").text();
                    catName = $(this).find("CategoryName").text();
                    total += Number(totalPrice);
                    Qty += Number($(this).find("QtyShip").text());
                    TotalQty += Number($(this).find("QtyShip").text());
                }
                count++;
            });
            gethtmlwhole += "<tr style='background:#ccc;font-weight:700;'><td class='totalQty'>" + Qty + "</td><td>" + catId + "</td><td colspan='5'>" + catName + "</td><td></td><td style='text-align:right;'>" + total.toFixed(2) + "</td></tr>";
            gethtmlwhole += '</tbody></table>';
            gethtmlwhole += '<table style="width: 100%;border-collapse:collapse;"><tbody><tr><td style="width: 33%;padding:0"></td><td style="width: 33%;padding:0"></td><td style="width: 33%;padding:0"><table class="table tableCSS">';
            gethtmlwhole += '<tbody><tr><td class="tdLabel">Total Qty</td><td style="text-align: right;">' + TotalQty + '</td></tr>';
            gethtmlwhole += '<tr><td class="tdLabel">Sub Total</td><td style="text-align: right;">' + $(order).find("GrandTotal").text() + '</td></tr>';

            if (parseFloat($(order).find("OverallDisc").text()) > 0) {
                gethtmlwhole += '<tr><td class="tdLabel">Discount</td><td style="text-align: right;">' + $(order).find("OverallDisc").text() + '</td></tr>';
            }
            if (parseFloat($(order).find("TotalTax").text()) > 0) {
                gethtmlwhole += '<tr><td class="tdLabel">Tax</td><td style="text-align: right;">' + $(order).find("TotalTax").text() + '</td></tr>';
            }
            if (parseFloat($(order).find("MLTax").text()) > 0) {
                gethtmlwhole += '<tr><td class="tdLabel">ML Qty</td><td style="text-align: right;">' + $(order).find("MLQty").text() + '</td></tr>';
                gethtmlwhole += '<tr><td class="tdLabel" style="white-space: nowrap;" >' + $(order).find("MLTaxPrintLabel").text() + '</td><td style="text-align: right;">' + $(order).find("MLTax").text() + '</td></tr>';
            }
            if (parseFloat($(order).find("AdjustmentAmt").text()) != 0) {
                gethtmlwhole += '<tr><td class="tdLabel">Adjustment</td><td style="text-align: right;">' + $(order).find("AdjustmentAmt").text() + '</td></tr>';
            }
            gethtmlwhole += '<tr><td class="tdLabel">Grand Total</td><td style="text-align: right;">' + $(order).find("TotalAmount").text() + '</td></tr></tbody>';
            gethtmlwhole += '</table>';

            $('#CRtableDynamic').append(gethtmlwhole);
            $('.TotalPageC').html(pageNo);
            for (var i = 1; i <= countCopy; i++) {
                $('#psmWPAPACredit').append(gethtmlwhole);
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function sendMail() {
    
    var mailBody = $(".InvContents").html();
    $.ajax({
        type: "POST",
        async: false,
        url: "DriverPackagePrint.aspx/SendMail",
        //data: "{'EmailBody':'" + mailBody + "'}",
        data: JSON.stringify({ EmailBody: mailBody.toString() }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            if (response.d == "true") {
                console.log("Email send successfully");
            }
            else {
                console.log("Oops, Something went worng. Please try later.");
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

