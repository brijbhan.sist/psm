﻿var WeightOz = 0.00; // Global Variable
$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var CreditAutoId = getQueryString('PageId');

    Orderrmarkdeatil(CreditAutoId);
    if (CreditAutoId != null) {
        $("#btnSave").hide();
        $("#btnReset").hide();
        $("#btnUpdate").show();
        $("#btnPrintOrder").show();
        $("#CreditAutoId").val(CreditAutoId);
        editCredit(CreditAutoId);


    } else {
        $("#btnSave").show();
        $("#btnReset").show();
        $("#btnUpdate").hide();
        $("#btnPrintOrder").hide();
    }

    $("#ddlProduct").select2().on("select2:select", function (e) {

    });

    $("#ddlCustomer").select2().on("select2:select", function (e) {
    });
});

var MLTaxRate = 0.00;
function ChangeProduct() {

    var productAutoId = $("#ddlProduct option:selected").val();
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/MCreditMemo.asmx/bindUnitType",
        data: "{'ProductAutoId':" + productAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {

            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var unitDefault = $(xmldoc).find("Table1");
                var count = 0;

                $("#ddlUnitType option:not(:first)").remove();
                $.each(unitType, function () {
                    $("#ddlUnitType").append("<option value='" + $(this).find("AutoId").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                });

                if (unitDefault.length > 0) {
                    $("#ddlUnitType").val($(unitDefault).find('AutoId').text()).change();
                } else {
                    $("#ddlUnitType").val(0);
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(result.d);
        },
        failure: function (result) {
            console.log(result.d);
        }
    });

}

function AddinList() {
    var chkIsTaxable = $('#chkIsTaxable').prop('checked');
    var IsTaxable = 0;
    var taxType = ''
    if (chkIsTaxable == true) {
        IsTaxable = 1;

        taxType = '<tax class="la la-check-circle success"></tax>';
    }
    var MLQty = $("#ddlProduct option:selected").attr('MLQty');
    if (CustCreditmemoRequiredField()) {
        var data = {
            CustomerAutoId: $("#ddlCustomer").val(),
            ProductAutoId: $("#ddlProduct").val(),
            UnitAutoId: $("#ddlUnitType").val(),
            Qty: $("#txtReqQty").val(),
        }
        $.ajax({
            type: "POST",
            url: "/Manager/WebAPI/MCreditMemo.asmx/ValidateCreditMemo",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {

                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    timeout: 2000, //unblock after 2 seconds
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },

            async: false,
            success: function (response) {
                if (response.d != "Session Expired") {
                    $("#ddlCustomer").attr('disabled', true);
                    var flag = false;
                    var xmldoc = $.parseXML(response.d);
                    var items = $(xmldoc).find("Table");
                    $("#emptyTable").hide();
                    if (items.length > 0) {
                        if (Number($(items).find('ReturnQty').text()) <= Number($(items).find('Qty').text())) {
                            $("#tblProductDetail tbody tr").each(function () {
                                if ($(this).find(".ProName span").attr('ProductAutoId') == $(items).find('ProductAutoId').text() &&
                                    $(this).find(".UnitType span").attr('UnitAutoId') == $(items).find('UnitAutoId').text()) {
                                    $(this).find('.ReturnQty input').val($(items).find('ReturnQty').text());
                                    $(this).find('.NetPrice').text($(items).find('NetPrice').text());
                                    flag = true;
                                }
                            });
                            if (!flag) {
                                var row = $("#tblProductDetail thead tr").clone(true);
                                $.each(items, function () {
                                    $(".ProId", row).text($(this).find("ProductId").text());
                                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                                    $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                                    $(".TaxRate", row).html(taxType + "<span MLQty='" + $(this).find("MLQty").text() + "' IsTaxable='" + IsTaxable + "'> </span>");
                                    $(".ReturnQty", row).html("<span MaxQty ='" + $(this).find('Qty').text() + "'></span><input type='text' class='form-control input-sm  border-primary' onkeyup='getcalCulation(this)' onkeypress='return isNumberKey(event)' style='width:100px;' value='" + $(this).find("ReturnQty").text() + "' />");
                                    $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary text-center' onkeyup='changePrice(this)' onkeypress='return isNumberDecimalKey(event,this)' style='width:100px;text-align:right;' value='" + $(this).find("CostPrice").text() + "' />");
                                    $(".SRP", row).text($(this).find("SRP").text());
                                    $(".AcceptedQty", row).text($(this).find("ReturnQty").text());
                                    $(".NetPrice", row).text($(this).find("NetPrice").text());
                                    $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                                    if ($('#tblProductDetail tbody tr').length > 0) {
                                        $('#tblProductDetail tbody tr:first').before(row);
                                    }
                                    else {
                                        $('#tblProductDetail tbody').append(row);
                                    }
                                    row = $("#tblProductDetail tbody tr:last").clone(true);

                                });
                            }
                            $("#txtReqQty").val('');
                            $("#ddlProduct").val('0').change();
                        } else {

                            if (Number($(items).find('UnitAutoId').text()) == 1) {
                                swal("", "Return qty should be less or equal to order qty : " + $(items).find('Qty').text() + '[case]', "error");
                            } else if (Number($(items).find('UnitAutoId').text()) == 2) {
                                swal("", "Return qty should be less or equal to order qty : " + $(items).find('Qty').text() + '[box]', "error");
                            } else {
                                swal("", "Return qty should be less or equal to order qty : " + $(items).find('Qty').text() + '[pcs]', "error");

                            }

                        }

                        $("#txtReqQty").val(1);
                        calTotalAmount();
                    }
                    else {

                        swal("", "No order qty ramining for credit memo.", "error");
                    }
                } else {
                    location.href = '/';
                }
            },
            failure: function (result) {
                swal("", result.d, "error");
            },
            error: function (result) {
                swal("", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function getcalCulation(e) {
    var tr = $(e).closest('tr');
    var maxReturnQty = 0.00;
    maxReturnQty = tr.find('.ReturnQty span').attr('maxqty');
    var ReturnQty = 0;
    if (tr.find('.ReturnQty input').val() != '') {
        ReturnQty = tr.find('.ReturnQty input').val();
    }
    tr.find('.AcceptedQty').text(ReturnQty);
    var UnitType = tr.find('.UnitType span').attr('unitautoid');
    if (Number(ReturnQty) > Number(maxReturnQty)) {

        if (Number(UnitType) == 1) {
            swal("", "Return qty should be less or equal to order qty : " + maxReturnQty + '[case]', "error");

        } else if (Number(UnitType) == 3) {
            swal("", "Return qty should be less or equal to order qty : " + maxReturnQty + '[Box]', "error");

        } else {
            swal("", "Return qty should be less or equal to order qty : " + maxReturnQty + '[pcs]', "error");
        }
        $(e).val(maxReturnQty);
    }
    changePrice(e);
}

function deleterows(e) {
    $(e).closest('tr').remove();
    if ($("#tblProductDetail tbody tr").length == 0) {
        $("#ddlCustomer").attr('disabled', false);
        $("#emptyTable").show();
    } else {
        $("#emptyTable").hide();
    }
    calTotalAmount();
}

function deleterow(e) {

    swal({
        title: "Are you sure?",
        text: "You want to delete this credit memo.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel.",
                value: true,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            deleterows(e);

        } else {
            swal("", "Credit memo is safe.", "success");
        }
    })
}


function calTotalAmount() {
    var total = 0.00;
    $("#tblProductDetail tbody tr").each(function () {
        total += Number($(this).find(".NetPrice").text());
    });
    $("#txtTotalAmount").val(total.toFixed(2));
    calTotalTax();
}
function calTotalTax() {
    debugger;
    var totalTax = 0.00, qty;
    var MLQty = 0.00, WeighOzQuantity = 0, TaxValue = 0;
    $("#tblProductDetail tbody tr").each(function () {
        qty = Number($(this).find(".AcceptedQty input").val()) || 0;
        if (qty != 0) {
            MLQty = parseFloat(MLQty) + (parseFloat($(this).find('.TaxRate span').attr('MLQty')) * qty * parseFloat($(this).find('.UnitType span').attr('qtyperunit')));
            WeighOzQuantity = parseFloat(WeighOzQuantity) + (parseFloat($(this).find('.TaxRate span').attr('WeightOz')) * qty * parseFloat($(this).find('.UnitType span').attr('qtyperunit')));
        }
        else {
            MLQty = 0.00;
            WeighOzQuantity = 0.00;
        }
        if ($(this).find('.TaxRate span').attr('istaxable') == 1) {
            var totalPrice = Number($(this).find(".UnitPrice input").val()) * qty;
            var Disc = totalPrice * Number($("#txtOverallDisc").val()) * 0.01;
            var priceAfterDisc = totalPrice - Disc;
         
            if ($("#ddlTaxType").val() != null) {
                totalTax += (priceAfterDisc * Number($('#ddlTaxType option:selected').attr("TaxValue")) * 0.01) || 0;
            }
        }
    });
    if ($("#ddlCreditMemoType").val() == '1') {
        $("#txtMLQty").val('0.00');
        $("#txtMLTax").val('0.00');
        $("#txtWeightQuantity").val('0.00');
        $("#txtWeightTax").val('0.00')
        $("#txtTotalTax").val('0.00');
    }
    else {
        $("#txtMLQty").val(MLQty.toFixed(2));
        $("#txtMLTax").val((MLQty * MLTaxRate).toFixed(2));
        $("#txtWeightQuantity").val((WeighOzQuantity).toFixed(2));
        $("#txtWeightTax").val((WeighOzQuantity * WeightOz).toFixed(2))
        $("#txtTotalTax").val(totalTax.toFixed(2));
    }
    calGrandTotal();
}

function calOverallDisc1() {
    var DiscAmt = parseFloat($("#txtDiscAmt").val()).toFixed(2) || 0.00;  
    var TotalAmount = parseFloat(Number($("#txtTotalAmount").val())).toFixed(2) || 0.00;
    var per = 0.00;
    if (parseFloat(DiscAmt) > parseFloat(TotalAmount)) {
        toastr.error('Discount Amount should be less than Total Amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtDiscAmt").val('0.00');
        $("#txtOverallDisc").val('0.00');
        $("#txtDiscAmt").select();
    } else {
        if (parseFloat(TotalAmount) > 0) {
            per = ((DiscAmt / TotalAmount) * 100) || 0;
            $("#txtOverallDisc").val(per.toFixed(2));
        }
        else {
            $("#txtDiscAmt").val('0.00');
            $("#txtOverallDisc").val('0.00');
            $("#txtDiscAmt").select();
        }
    }
    calTotalTax();
}
function calOverallDisc() {
    debugger
    var factor = 0.00;
    if ($("#txtOverallDisc").val() == "") {
        $("#txtOverallDisc").val('0.00');
    }
    else if (parseFloat($("#txtOverallDisc").val()) > 100) {
        $("#txtOverallDisc").val('0.00');
        toastr.error('Discount % cannot be greater than 100.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    factor = Number($("#txtOverallDisc").val()) * 0.01 || 0.00;
    $("#txtDiscAmt").val((Number($("#txtTotalAmount").val()) * factor).toFixed(2));
    calTotalTax();
}
function calGrandTotal() {
    var DiscPercent = 0.00, totAmt = 0.00, DiscAmt = 0.00;
    if ($("#txtOverallDisc").val() != "") {
        DiscPercent = parseFloat($("#txtOverallDisc").val())
    }
    if (parseFloat($("#txtTotalAmount").val())!=0.00) {
        totAmt = parseFloat($("#txtTotalAmount").val()).toFixed(2);
        DiscAmt = parseFloat((totAmt * DiscPercent) / 100).toFixed(2) || 0;
        parseFloat($("#txtDiscAmt").val(DiscAmt)).toFixed(2);
    }
    var grandTotal = Number($("#txtTotalAmount").val()) - parseFloat(DiscAmt) + Number($("#txtTotalTax").val()) + parseFloat($("#txtMLTax").val()) + parseFloat($("#txtWeightTax").val());
    var round = Math.round(grandTotal);
    $("#txtAdjustment").val((round - grandTotal).toFixed(2));
    $("#txtGrandTotal").val(round.toFixed(2));
    if (round == 0) {
        $("#txtWeightQuantity").val('0.00');
        $("#txtWeightTax").val('0.00')
    }
}

function changePrice(e) {

    var row = $(e).closest("tr");
    $(row).find(".UnitPrice input").css("border-color", "#eee");
    var NetPrice = 0.0;
    var UnitPrice = Number(row.find(".UnitPrice > input").val());
    var ReturnQty = Number(row.find(".ReturnQty input").val());
    NetPrice = parseFloat(Number(ReturnQty) * parseFloat(UnitPrice));
    $(row).find('.NetPrice').text(NetPrice.toFixed(2));
    calTotalAmount();
}

function refresh() {
    $("#tblProductDetail tbody tr").remove();
    calTotalAmount();
}

function Reset() {
    refresh();
    $("#ddlCustomer").val('0').change();
    $("#txtRemarks").val('');
    $("#txtOverallDisc").val('0.00');
    $("#txtDiscAmt").val('0.00');
    $("#txtAdjustment").val('0.00');
    $("#txtGrandTotal").val('0.00');
}


$('.close').click(function () {
    $(this).closest('div').hide();

});

function Orderrmarkdeatil(CreditAutoId) {

    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/MCreditMemo.asmx/OrderRemarkDeatil",
        data: "{'CreditAutoId':" + CreditAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var index = 0;
            var ManagerRemark = $(xmldoc).find("Table1");
            $("#tblOrderrmarkdeatil tbody tr").remove();
            var row = $("#tblOrderrmarkdeatil thead tr").clone(true);
            if (ManagerRemark.length > 0) {
                $("#EmptyTable").hide();
                $.each(ManagerRemark, function (index) {

                    $(".SRNO", row).html((Number(index) + 1));
                    $(".EmployeeName", row).text($(this).find("EmpName").text());
                    $(".EmployeeType", row).text($(this).find("EmpType").text());
                    $(".Remarks", row).html($(this).find("Remarks").text());

                    $("#tblOrderrmarkdeatil tbody").append(row);
                    row = $("#tblOrderrmarkdeatil tbody tr:last").clone(true);
                });
            }
            else {
                $("#OrderRemrak").hide();
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function editCredit(CreditAutoId) {
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/MCreditMemo.asmx/editCredit",
        data: "{'CreditAutoId':'" + CreditAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        beforeSend: function () {

            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var CreditOrder = $(xmldoc).find("Table");
                var items = $(xmldoc).find("Table1");
                var EmpType = $(xmldoc).find("Table2");
                var RemarksDetails = $(xmldoc).find("Table3");
                var BindTaxType = $(xmldoc).find("Table4");
                var Unit = $(xmldoc).find("Table5");
                var DueAmount = $(xmldoc).find("Table6");
                var CreditMemoTypeList = $(xmldoc).find("Table7");
                var CStatus = "";
                debugger;
                if (BindTaxType.length > 0) {
                    $("#ddlTaxType option:not(:first)").remove();
                    $.each(BindTaxType, function () {
                        $("#ddlTaxType").append("<option TaxValue='" + $(this).find("Value").text() + "' value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TaxableType").text() + '(' + $(this).find("Value").text() + ')' + "</option>");
                    });
                    $("#ddlTaxType").val($(CreditOrder).find("TaxTypeAutoId").text());
                }


                if ($(CreditOrder).find("WeightTax").text() != '') {
                    WeightOz = $(CreditOrder).find("WeightTax").text()
                }

                var UnitDetails = [];

                $.each(Unit, function () {
                    UnitDetails.push({
                        AutoId: $(this).find("UnitAutoId").text(),
                        UnitType: $(this).find("UnitType").text(),
                        ProductId: $(this).find("ProductId").text(),
                        Qty: $(this).find("Qty").text(),
                    });
                });
                localStorage.setItem("UnitItems", JSON.stringify(UnitDetails))
                MLTaxRate = parseFloat($(CreditOrder).find("MLTaxPer").text());
                $("#txtOrderId").val($(CreditOrder).find("CreditNo").text());
                $("#txtOrderDate").val($(CreditOrder).find("CreditDate").text());
                $("#txtOrderStatus").val($(CreditOrder).find("STATUS").text());
                CStatus = $(CreditOrder).find("STATUS").text();
                $("#CreditType").val($(CreditOrder).find("CreditType").text());
                $("#txtRemarks").val($(CreditOrder).find("Remarks").text()).attr('disabled', true)
                $("#txtCustomer").val($(CreditOrder).find("CustomerName").text()).change();
                $("#txtManagerRemarks").val($(CreditOrder).find("ManagerRemark").text());
                var ReferenceOrderAutoId = $(CreditOrder).find("ReferenceOrderAutoId").text();
                console.log($(CreditOrder).find("ReferenceOrderAutoId").text());
                $("#ddlCreditMemoType option:not(:first)").remove();
                $.each(CreditMemoTypeList, function () {
                    $("#ddlCreditMemoType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("CreditType").text() + "</option>");
                });             
                $("#hiddenOrderStatus").val($(CreditOrder).find("StatusCode").text());
                //  updated by satish 11/05/2019
                if ($(CreditOrder).find("StatusCode").text() == 3 && $(CreditOrder).find("referenceorderno").text() == '') {
                    $('#btnCancelCreditMemo').show();
                } else {
                    $('#btnCancelCreditMemo').hide();
                }
                //  end Update
                var row = $("#tblProductDetail thead tr").clone(true);
                $("#tblProductDetail tbody tr").remove();
                var FreshReturnUnitAutoId = 0;
                var DamageReturnUnitAutoId = 0;
                var MissingItemUnitAutoId = 0;
                $.each(items, function () {
                    if ($(this).find("QtyPerUnit_Fresh").text() != '') {
                        FreshReturnUnitAutoId = $(this).find("QtyPerUnit_Fresh").text();
                    } else {
                        FreshReturnUnitAutoId = $(this).find("UnitAutoId").text();
                    }
                    if ($(this).find("QtyPerUnit_Damage").text() != '') {
                        DamageReturnUnitAutoId = $(this).find("QtyPerUnit_Damage").text();
                    } else {
                        DamageReturnUnitAutoId = $(this).find("UnitAutoId").text();
                    }
                    if ($(this).find("QtyPerUnit_Missing").text() != '') {
                        MissingItemUnitAutoId = $(this).find("QtyPerUnit_Missing").text();
                    } else {
                        MissingItemUnitAutoId = $(this).find("UnitAutoId").text();
                    }
                    $(".ItemAutoId", row).text($(this).find("ItemAutoId").text());
                    $(".ProId", row).text($(this).find("ProductId").text());
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                    $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                    $(".AcceptedQty", row).html("<span MAXQty='" + $(this).find("MAXQty ").text() + "'></span><input disabled='disabled' type='text' class='form-control input-sm border-primary text-center' onkeyup='getcalCulation1(this)' onkeypress='return isNumberKey(event)' style='width:100px;' value='" + $(this).find("AcceptedQty").text() + "' />");
                    if ($(EmpType).find('EmpType').text() != 8) {
                        if ($(CreditOrder).find("StatusCode").text() == 1) {
                            $(".ReturnQty", row).html("<span MAXQty='" + $(this).find("MAXQty ").text() + "'></span><input type='text' class='form-control input-sm border-primary' onkeyup='getcalCulation(this)' onkeypress='return isNumberKey(event)' style='width:100px;' value='" + $(this).find("RequiredQty").text() + "' />");
                        } else {
                            $(".ReturnQty", row).html($(this).find("RequiredQty").text());
                        }
                    } else {

                        $(".ReturnQty", row).html($(this).find("RequiredQty").text());
                    }

                    if ($(CreditOrder).find("StatusCode").text() == 1) {
                        $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' onkeyup='changePrice1(this)' style='width:100px;text-align:right;border:1px solid #3b4781;'  onkeypress='return isNumberDecimalKey(event,this)' value='" + $(this).find("ManagerUnitPrice").text() + "' />");
                    } else {
                        $(".UnitPrice", row).html($(this).find("ManagerUnitPrice").text());
                    }
                    $(".TtlPcs", row).text($(this).find("TotalPeice").text());
                    $(".SRP", row).text($(this).find("SRP").text());
                    $(".TaxRate", row).html('<span WeightOz=' + $(this).find("WeightOz").text() + ' MLQty=' + $(this).find("UnitMLQty").text() + ' istaxable=' + $(this).find("TaxRate").text() + '>' + (($(this).find("TaxRate").text() == 1) ? '<tax class="la la-check-circle success"></span>' : '') + '</tax>');
                    $(".NetPrice", row).text($(this).find("NetAmount").text());
                    if (CStatus == "Approved" || CStatus == "Cancelled" || CStatus == "Decline") {
                        if ($(this).find("QtyReturn_Fresh").text() != "") {
                            $(".FreshReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text' disabled='disabled' class='form-control input-sm border-primary text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("QtyReturn_Fresh").text() + "' onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary'  disabled></select></div></div>");
                        } else {
                            $(".FreshReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text'   disabled='disabled' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0' onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary'  disabled></select></div></div>");
                        }
                        if ($(this).find("QtyReturn_Damage").text() != "") {
                            $(".DemageReturn", row).html("<div class='input-group-prepend'><input type='text' disabled='disabled' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("QtyReturn_Damage").text() + "'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='demageunit form-control input-sm border-primary'   disabled></select></div>");
                        } else {
                            $(".DemageReturn", row).html("<div class='input-group-prepend'><input type='text' disabled='disabled' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='demageunit form-control input-sm border-primary'   disabled></select></div>");
                        }

                        if ($(this).find("QtyReturn_Missing").text() != "") {
                            $(".MissingItem", row).html("<div class='input-group-prepend'><input type='text' disabled='disabled' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("QtyReturn_Missing").text() + "'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='missingunit form-control input-sm border-primary'   disabled></select></div>");
                        } else {
                            $(".MissingItem", row).html("<div class='input-group-prepend'><input type='text' disabled='disabled' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='missingunit form-control input-sm border-primary'   disabled></select></div>");
                        }
                    }
                    else {
                        if ($(this).find("QtyReturn_Fresh").text() != "") {
                            $(".FreshReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text' onkeyup='calculateReturnQty(this)' class='form-control input-sm border-primary text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("QtyReturn_Fresh").text() + "' disabled onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary'  disabled></select></div></div>");
                        } else {
                            $(".FreshReturn", row).html("<div class='input-group'><div class='input-group-prepend'><input type='text' onkeyup='calculateReturnQty(this)' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0' onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='freshunit form-control input-sm border-primary'  disabled></select></div></div>");
                        }
                        if ($(this).find("QtyReturn_Damage").text() != "") {
                            $(".DemageReturn", row).html("<div class='input-group-prepend'><input type='text' onkeyup='calculateReturnQty(this)' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("QtyReturn_Damage").text() + "' disabled  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='demageunit form-control input-sm border-primary'   disabled></select></div>");
                        } else {
                            $(".DemageReturn", row).html("<div class='input-group-prepend'><input type='text' onkeyup='calculateReturnQty(this)' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='demageunit form-control input-sm border-primary'   disabled></select></div>");
                        }

                        if ($(this).find("QtyReturn_Missing").text() != "") {
                            $(".MissingItem", row).html("<div class='input-group-prepend'><input type='text' onkeyup='calculateReturnQty(this)' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='" + $(this).find("QtyReturn_Missing").text() + "'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='missingunit form-control input-sm border-primary'   disabled></select></div>");
                        } else {
                            $(".MissingItem", row).html("<div class='input-group-prepend'><input type='text' onkeyup='calculateReturnQty(this)' class='form-control input-sm border-primary  text-center' style='width:50px;padding:0.25rem 0.25rem;margin-right: 2px;' value='0'  onkeypress='return isNumberKey(event)'/><select style='width:58px;padding:0' class='missingunit form-control input-sm border-primary'   disabled></select></div>");
                        }
                    }
                    $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                    $('#tblProductDetail tbody').append(row);
                    var getUnit = JSON.parse(localStorage.getItem('UnitItems'));
                    $.grep(getUnit, function (e) {
                        if (e.ProductId == $(row).find(".ProId").text()) {
                            if ($(this).find("FreshReturnUnitAutoId").text() != 0) {
                                if ($(row).find(".UnitType span").attr('UnitAutoId') == e.AutoId) {

                                    row.find('.freshunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                    row.find('.demageunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                    row.find('.missingunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                }
                                else {
                                    row.find('.freshunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                    row.find('.demageunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                    row.find('.missingunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                }
                            } else {
                                if (FreshReturnUnitAutoId == e.AutoId) {
                                    row.find('.freshunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                }
                                else {
                                    row.find('.freshunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                }
                                if (DamageReturnUnitAutoId == e.AutoId) {
                                    row.find('.demageunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                }
                                else {
                                    row.find('.demageunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                }
                                if (MissingItemUnitAutoId == e.AutoId) {
                                    row.find('.missingunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + " selected='selected'>" + e.UnitType + "</option>"));
                                }
                                else {
                                    row.find('.missingunit').append($("<option value=" + e.AutoId + " Qty=" + e.Qty + ">" + e.UnitType + "</option>"));
                                }
                            }
                        }

                    });
                    $(".Del_MinPrice", row).text($(this).find("Del_MinPrice").text());
                    $(".Del_CostPrice", row).text($(this).find("Del_CostPrice").text());
                    row = $("#tblProductDetail tbody tr:last").clone(true);
                });
                  $("#ddlCreditMemoType").val($(CreditOrder).find("CreditMemoType").text()).change();
                if ($(RemarksDetails).length > 0) {

                    $("#Table3 tbody tr").remove();
                    $("#OrderRemarksDetails").show();
                    var rowtest = $("#Table3 thead tr").clone();
                    $.each(RemarksDetails, function (index) {
                        $(".SRNO", rowtest).html((Number(index) + 1));
                        $(".EmployeeName", rowtest).html($(this).find("EmpName").text());
                        $(".EmployeeType", rowtest).html($(this).find("EmpType").text());
                        $(".Remarks", rowtest).html($(this).find("Remarks").text());
                        $("#Table3 tbody").append(rowtest);
                        rowtest = $("#Table3 tbody tr:last").clone(true);
                    });
                }
                if ($(EmpType).find('EmpType').text() == 8) {
                    $(".AcceptedQty").show();
                }
                $("#btnPrintOrder").show();
                if ($(EmpType).find('EmpType').text() != 6) {
                    if ($(EmpType).find('EmpType').text() == 2 || $(EmpType).find('EmpType').text() == 1) {
                        if ($(CreditOrder).find("StatusCode").text() == 1) {
                            $("#btnUpdate").show();
                            $(".Action").show();
                            $('#btnAdd').closest('#Productpannel').show();
                            $("#txtDiscAmt").attr('disabled', false);
                            $("#txtOverallDisc").attr('disabled', false);
                            $("#ddlTaxType").attr('disabled', false);
                        } else {
                            $("#btnUpdate").hide();
                            $(".AcceptedQty").show();
                            $(".Action").hide();
                            $('#btnAdd').closest('#Productpannel').hide();
                            $("#txtDiscAmt").attr('disabled', true);
                            $("#txtOverallDisc").attr('disabled', true);
                            $("#ddlTaxType").attr('disabled', true);
                        }
                    } else {
                        if ($(CreditOrder).find("StatusCode").text() == 1) {
                            $("#btnUpdate").hide();
                            $("#btnBackOrder").show();
                            $('.ReturnQty input').attr('disabled', true);
                            $('.UnitPrice input').attr('disabled', true);
                            $("#btnApproved").show();
                            $("#btnCancel").show();
                            $(".Action").hide();
                            $('#btnAdd').closest('#Productpannel').hide();
                        } else {

                            if ($(EmpType).find('EmpType').text() == 6 && $(CreditOrder).find("StatusCode").text() == 2) {
                                $("#btnComplete").show();
                            } else {
                                $("#btnComplete").hide();
                            }

                            $("#btnUpdate").hide();
                            $("#btnApproved").hide();
                            $("#btnCancel").hide();
                            $(".Action").hide();
                            $("#Productpannel").hide();
                            $('#btnAdd').closest('#Productpannel').hide();

                        }
                        if ($(EmpType).find('EmpType').text() == 10) {
                            $('#btnSave').closest('.row').hide();
                        }
                    }
                    $("#txtTotalAmount").val($(CreditOrder).find("GrandTotal").text());
                    $("#txtOverallDisc").val($(CreditOrder).find("OverallDisc").text());
                    $("#txtDiscAmt").val($(CreditOrder).find("OverallDiscAmt").text());                  
                    $("#txtGrandTotal").val($(CreditOrder).find("TotalAmount").text());
                    if ($("#ddlCreditMemoType").val() == '1') {
                        $("#txtMLQty").val('0.00');
                        $("#txtMLTax").val('0.00');
                        $("#txtWeightQty").val('0.00');
                        $("#txtWeightTax").val('0.00');
                        $("#txtTotalTax").val('0.00');
                    }
                    else {
                        $("#txtMLQty").val($(CreditOrder).find("MLQty").text());
                        $("#txtMLTax").val($(CreditOrder).find("MLTax").text());
                        $("#txtWeightQuantity").val($(CreditOrder).find("WeightTotalQuantity").text());
                        $("#txtWeightTax").val($(CreditOrder).find("WeightTaxAmount").text());
                        $("#txtTotalTax").val($(CreditOrder).find("TotalTax").text());
                    }
                    $("#txtAdjustment").val($(CreditOrder).find("AdjustmentAmt").text());
                } else {
                    $("#btnUpdate").hide();
                    $("#btnApproved").hide();
                    $("#btnCancel").hide();
                    $(".Action").hide();
                    $(".AcceptedQty").show();
                    $(".ReturnQty input").attr('disabled', true);
                    $(".AcceptedQty input").attr('disabled', true);
                    $(".UnitPrice input").attr('disabled', true);
                    $('#btnAdd').closest('#Productpannel').hide();
                    $("#btnComplete").hide();
                }

                if ($(EmpType).find('EmpType').text() == 8) {
                    if ($(CreditOrder).find("StatusCode").text() == 1) {
                        $("#hideRemark").show();
                        $("#txtManagerRemarks").attr('disabled', false);
                    }
                    else {
                        $("#hideRemark").show()
                        $("#txtManagerRemarks").attr('disabled', true);
                    }
                }
                if ($(EmpType).find('EmpType').text() == 8) {
                    if ($(CreditOrder).find("StatusCode").text() == 1) {
                        //$("#txtDiscAmt").attr('disabled', false);
                        //$("#txtOverallDisc").attr('disabled', false);
                        $("#ddlTaxType").attr('disabled', false);
                    }
                    else {
                        $("#txtDiscAmt").attr('disabled', true);
                        $("#txtOverallDisc").attr('disabled', true);
                        $("#ddlTaxType").attr('disabled', true);
                    }
                }
                else {
                    if ($(CreditOrder).find("StatusCode").text() == 3) {
                        if ($(CreditOrder).find("ManagerRemark").text() == "") {
                            $("#hideRemark").hide();
                        }
                        else {
                            $("#hideRemark").show()
                            //$("#txtManagerRemarks").attr('disabled', true);
                        }

                    }
                    else if ($(CreditOrder).find("StatusCode").text() == 5) {
                        if ($(CreditOrder).find("ManagerRemark").text() == "") {
                            $("#hideRemark").hide();
                        }
                        else {
                            $("#hideRemark").show();
                            //$("#txtManagerRemarks").attr('disabled', true);
                        }
                    }
                    else {
                        $("#hideRemark").hide();
                    }
                }


                var amtDue = 0.00, sn = 0;
                if (ReferenceOrderAutoId != '0' && DueAmount.length > 0) {
                    $("#tblduePayment tbody tr").remove();
                    var row = $("#tblduePayment thead tr").clone(true);
                    $.each(DueAmount, function () {
                        if ($(CreditOrder).find("StatusCode").text() == 1) {
                            sn = Number(sn) + 1;
                            if ($(this).find("AutoId").text() == ReferenceOrderAutoId) {
                                $(".Actions", row).html('<input checked  onclick="checkItem(this)" type="checkbox" name="Action">');
                            }
                            else {
                                $(".Actions", row).html('<input type="checkbox"  onclick="checkItem(this)" name="Action">');
                            }
                            $(".OrderNo", row).html('<span OrderAutoId=' + $(this).find("AutoId").text() + '>' + $(this).find("OrderNo").text() + '</span>');
                            $(".OrderDate", row).text($(this).find("OrderDate").text());
                            $(".AmtDue", row).text($(this).find("AmtDue").text());
                            $(".SrNo", row).html(sn);
                            amtDue += parseFloat($(this).find("AmtDue").text());
                            $('#tblduePayment tbody').append(row);
                            row = $("#tblduePayment tbody tr:last").clone(true);
                        }
                        else {
                            sn = Number(sn) + 1;
                            $(".Actions", row).html('<input  onclick="checkItem(this)" checked type="checkbox" name="Action">');
                            $(".OrderNo", row).html('<span OrderAutoId=' + $(this).find("AutoId").text() + '>' + $(this).find("OrderNo").text() + '</span>');
                            $(".OrderDate", row).text($(this).find("OrderDate").text());
                            $(".AmtDue", row).text($(this).find("AmtDue").text());
                            $(".SrNo", row).html(sn);
                            amtDue += parseFloat($(this).find("AmtDue").text());
                            $('#tblduePayment tbody').append(row);
                            row = $("#tblduePayment tbody tr:last").clone(true);
                        }
                    });

                    $("#panelDueAmount").show();
                    $("#TotalDueAmount").html(parseFloat(amtDue).toFixed(2));
                    blockCopyPaste();
                }
                else {
                    $("#panelDueAmount").hide();
                    $("#tblduePayment tbody tr").remove();
                }
            } else {
                location.href = '/';
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function calculateReturnQty(e) {
    debugger;
    var row = $(e).closest('tr');
    var ReturnQty = $(row).find('.FreshReturn input').val();
    var DamageQty = $(row).find('.DemageReturn input').val();
    var AcceptedQty = $(row).find('.AcceptedQty  input').val();
    if (Number(ReturnQty) != 0) {
        $(row).find('.FreshReturn input').removeClass('border-warning');
    }
    if (Number(ReturnQty) != 0) {
        $(row).find('.DemageReturn input').removeClass('border-warning');
    }
    ReturnFm = parseInt(ReturnQty) + parseInt(DamageQty);
    if (ReturnQty == "") {
        $(row).find('.FreshReturn input').val(0)
    }
    else if (DamageQty == "") {
        $(row).find('.DemageReturn input').val(0)
    }
    else if (ReturnFm > Number(AcceptedQty)) {
        toastr.error('Fresh+Damage quantity should be equal to accepted quantity.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        if (Number(AcceptedQty) == '0') {
            $(row).find('.FreshReturn input').val('0');
            $(row).find('.DemageReturn input').val('0');
        }
        else {
            $(row).find('.FreshReturn input').val($(row).find('.FreshReturn input').attr('value'));
            $(row).find('.DemageReturn input').val($(row).find('.DemageReturn input').attr('value'));
        }
    }
    else {
        $(row).find('.FreshReturn input').removeClass('border-warning');
        $(row).find('.DemageReturn input').removeClass('border-warning');
    }
}
function ApprovedOrders(Status) {

    var confirmtest = '';
    var Product = [];
    var message = '';
    if (Status == 3) {
        message = 'Approved successfully.';

    } else {
        message = 'Credit Memo Declined successfully.'

    }
    $("#tblProductDetail tbody tr").each(function () {

        Product.push({
            'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
            'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
            'RequiredQty ': $(this).find('.AcceptedQty input').val(),
            'QtyPerUnit': $(this).find('.UnitType').find('span').attr('qtyperunit'),
            'UnitPrice': $(this).find('.UnitPrice input').val(),
            'SRP': 0.00,
            'Tax': $(this).find('.TaxRate span').attr('istaxable'),
            'NetPrice': $(this).find('.NetPrice').text(),
            'FreshReturnUnitAutoId': $(this).find('.FreshReturn select').val(),
            'DamageReturnUnitAutoId': $(this).find('.DemageReturn select').val(),
            'MissingItemUnitAutoId': $(this).find('.MissingItem select').val(),
            'FreshReturnQty': $(this).find('.FreshReturn input').val(),
            'DamageReturnQty': $(this).find('.DemageReturn input').val(),
            'MissingItemQty': $(this).find('.MissingItem  input').val()
        });
    });
    var CreditData = {
        CreditAutoId: $('#CreditAutoId').val(),
        ManagerRemark: $("#txtManagerRemarks").val(),
        Status: Status
    };

    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/MCreditMemo.asmx/ApprovedCredit",
        data: JSON.stringify({ CreditData: JSON.stringify(CreditData), TableValues: JSON.stringify(Product) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (response) {
            swal("", message, "success");
            $("#btnBackOrder").hide();
            editCredit($('#CreditAutoId').val());

        },
        failure: function (result) {
            swal("", result.d, "error");
        },
        error: function (result) {

            swal("", result.d, "error");

        }
    });
}

// updated by satish  06/11/2019
function ApprovedOrder() {
    var flage = false;
    if (reqdremarkCreditmemoRequiredField()) {
        if ($('#tblProductDetail tbody tr').length > 0) {
            $("#tblProductDetail tbody tr").each(function () {
                var ReturnQty = $(this).find('.FreshReturn input').val();
                var DamageQty = $(this).find('.DemageReturn input').val();
                var AcceptedQty = $(this).find('.AcceptedQty  input').val();
                ReturnFm = parseInt(ReturnQty) + parseInt(DamageQty);
                if (parseInt(ReturnFm) != parseInt(AcceptedQty)) {
                    $(this).find(".FreshReturn input").focus().addClass("border-warning");
                    toastr.error('Fresh+Damage quantity should be equal to accepted quantity.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                    flage = true
                }

            })
            if (!flage) {

                swal({
                    title: "Are you sure?",
                    text: "You want to approved this credit memo.",
                    icon: "warning",
                    showCancelButton: true,
                    buttons: {
                        cancel: {
                            text: "No, Cancel.",
                            value: null,
                            visible: true,
                            className: "btn-warning",
                            closeModal: false,
                        },
                        confirm: {
                            text: "Yes, Approved it.",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: false
                        }
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        ApprovedOrders(3);

                    } else {
                        swal("", "Your credit memo is safe.", "error");
                    }
                })
            }
        }
    }
    else {
        toastr.error('All * fields are  mandatory .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
};


function Cancelcreditmemo() {
    if (reqdremarkCreditmemoRequiredField()) {
        swal({
            title: "Are you sure?",
            text: "You want to decline this credit memo.",
            icon: "warning",
            showCancelButton: true,
            allowOutsideClick: false,
            closeOnClickOutside: false,

            buttons: {
                cancel: {
                    text: "No, Cancel.",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes, Decline it.",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                ApprovedOrders(5);
            }
        })
    }
    else {
        toastr.error('All * fields are  mandatory .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function checkItem(e) {
    var i = 1;
    if ($(e).prop('checked')) {
        i = 0;
    }
    $("#tblduePayment tbody tr").each(function () {
        $(this).find(".Actions input").prop('checked', false)
    });
    var row = $(e).closest('tr');
    if (i == 0)
        $(row).find(".Actions input").prop('checked', true);
}
//  end  updated by satish  06/11/2019

function CompleteOrders() {
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/MCreditMemo.asmx/CompleteOrder",
        data: "{'CreditAutoId':" + $('#CreditAutoId').val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'


                }
            });
        },

        success: function (response) {
            swal("", "Credit memo complete successfully.", "success");
            editCredit($('#CreditAutoId').val());
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function CompleteOrder() {
    swal({
        title: "Are you sure?",
        text: "You want to Complete? This Credit Memo.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm:
            {
                text: "Yes, Complete it.",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(isConfirm => {
        if (isConfirm) {
            CompleteOrders();
        }
        else {
            // swal("", "Thanks You:)", "error");            
        }
    })
}
function getcalCulation1(e) {
    debugger;
    var tr = $(e).closest('tr');
    var maxReturnQty = 0.00; var Return_Qty = 0.00, qtyPerUnit = 0;
    maxReturnQty = tr.find('.ReturnQty').text()||0;
    Return_Qty = tr.find('.AcceptedQty input').val() || 0;
    if (Return_Qty == 0) {
        tr.find('.AcceptedQty input').val('0');
    }
    qtyPerUnit = tr.find('.UnitType span').attr('qtyperunit')||0;
    if (Number(Return_Qty) > Number(maxReturnQty)) {
        tr.find('.FreshReturn  input').val(maxReturnQty);
        tr.find('.TtlPcs').text(Number(maxReturnQty) * Number(qtyPerUnit));
    }
    else {
        tr.find('.FreshReturn  input').val(Return_Qty);
        tr.find('.TtlPcs').text(Number(Return_Qty) * Number(qtyPerUnit));
    }
    var AcceptedQty = 0;
    if (tr.find('.AcceptedQty input').val() != '') {
        AcceptedQty = tr.find('.AcceptedQty input').val();
    }
    if (Number(AcceptedQty) != 0) {
        tr.find('.AcceptedQty input').removeClass('border-warning');
    }
    if (Number(AcceptedQty) > Number(maxReturnQty)) {
        $(e).val(maxReturnQty);
        toastr.error("Accepted quantity should be equal to return quantity.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    changePrice1(e);
}
$("#btnBackOrder").click(function () {
    $("#btnUpdate1").show();
    $("#hideRemark").show();
    $(this).hide();
    $("#btnApproved").hide();
    $("#btnCancel").hide();
    $("#btnBackupdate").show();
    $("#ddlTaxType").attr('disabled', true);
    $(".AcceptedQty input").attr('disabled', false);
    $(".UnitPrice input").attr('disabled', false);
    $(".FreshReturn input").attr('disabled', false);
    $(".DemageReturn input").attr('disabled', false);
    $("#ddlCreditMemoType").attr('disabled', false);
    $("#txtManagerRemarks").attr('disabled', false);
    $("#txtDiscAmt").attr('disabled', false);
    $("#txtOverallDisc").attr('disabled', false);
    blockCopyPaste();
});
$("#btnBackupdate").click(function () {
    $("#btnUpdate1").hide();
    $("#hideRemark").show();
    $(this).hide();
    $("#btnApproved").show();
    $("#btnCancel").show();
    $("#btnBackOrder").show();
    $(".AcceptedQty input").attr('disabled', true);
    $(".UnitPrice input").attr('disabled', true);
    $("#ddlCreditMemoType").attr('disabled', true);
    $("#txtDiscAmt").attr('disabled', true);
    $("#txtOverallDisc").attr('disabled', true);
});
function changePrice1(e) { 
    debugger;
    var tr = $(e).closest('tr');
    var AcceptedQty = 0.00;
    var UnitPrice = 0.00;
    var DiscountPercent = parseFloat($("#txtOverallDisc").val());
    if (tr.find('.AcceptedQty input').val() != '') {
        AcceptedQty = parseFloat(tr.find('.AcceptedQty input').val());
    }
    if (tr.find('.UnitPrice input').val() == '') {
        tr.find('.UnitPrice input').val('0.00');  
    }
    else {
        UnitPrice = parseFloat(tr.find('.UnitPrice input').val()) || 0.00;
    }
    var NetAmount = parseFloat(AcceptedQty) * parseFloat(UnitPrice);
    tr.find('.NetPrice').text(NetAmount.toFixed(2));
    parseFloat($("#txtDiscAmt").val((NetAmount * DiscountPercent) / 100)).toFixed(2);
    calTotalAmount1();
}
function calTotalAmount1() {
    var total = 0.00;
    $("#tblProductDetail tbody tr").each(function () {
        total += Number($(this).find(".NetPrice").text());
    });
    $("#txtTotalAmount").val(total.toFixed(2));
    if (parseFloat(total) > 0) {
        $("#txtOverallDisc").removeAttr('disabled');
        $("#txtDiscAmt").removeAttr('disabled');
    }
    else {
        $("#txtOverallDisc").attr('disabled', true);
        $("#txtDiscAmt").attr('disabled', true);
    }
    calTotalTax();
}
//function calTotalTax1() { 
//    var totalTax = 0.00, qty;
//    var MLQty = 0.00;
//    $("#tblProductDetail tbody tr").each(function () {
//        qty = Number($(this).find(".AcceptedQty input").val()) || 0;
//        if (qty != 0) {
//            MLQty = parseFloat(MLQty) + (parseFloat($(this).find('.TaxRate span').attr('MLQty')) * qty * parseFloat($(this).find('.UnitType span').attr('qtyperunit')));
//        }
//        else {
//            MLQty = 0.00;
//        }
//        if ($(this).find('.TaxRate span').attr('istaxable') == 1) {
//            var totalPrice = Number($(this).find(".UnitPrice input").val()) * qty;
//            var Disc = totalPrice * Number($("#txtOverallDisc").val()) * 0.01;
//            var priceAfterDisc = totalPrice - Disc;
//            var taxVlue = $('#ddlTaxType option:selected').attr("TaxValue") || 0;
//            totalTax += priceAfterDisc * Number(taxVlue) * 0.01;
//        }
//    });
//    $("#txtTotalTax").val(totalTax.toFixed(2));

//    if ($("#ddlCreditMemoType").val() == '1') {
//        $("#txtMLQty").val('0.00');
//        $("#txtMLTax").val('0.00');
//        $("#txtWeightQty").val('0.00');
//        $("#txtWeightTax").val('0.00');
//    }
//    else {
//        $("#txtMLQty").val(MLQty.toFixed(2));
//        $("#txtMLTax").val((MLQty * MLTaxRate).toFixed(2));
//    }
//    //if (Number($("#hfMLTaxStatus").val()) == '0') {
//    //    $("#txtMLTax").val('0.00');
//    //}
//    calGrandTotal();
//}

function checkInptuTypeInTable() {
    var boolcheck = true;
    $('#tblProductDetail').find('input[type=text]').each(function () {
        if ($(this).val() == '') {
            $(this).addClass('border-warning');
            boolcheck = false;
        }
        else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}

$("#btnUpdate1").click(function () {
    var flag1 = false, flag2 = false, AcptdMsg = 0;
    var Product = [];
    if ($('#tblProductDetail tbody tr').length > 0) {
        $("#tblProductDetail tbody tr").each(function () { 
            if ($(this).find(".UnitPrice input").val() == "" || $(this).find(".UnitPrice input").val() == null || parseFloat($(this).find(".UnitPrice  input").val()) == 0.00) {
                $(this).find(".UnitPrice input").focus().addClass("border-warning");
                flag1 = true;
            }
            var ReturnQty = $(this).find('.FreshReturn input').val();
            var DamageQty = $(this).find('.DemageReturn input').val();
            var AcceptedQty = $(this).find('.AcceptedQty  input').val();
            ReturnFm = parseInt(ReturnQty) + parseInt(DamageQty);
            if (parseInt(ReturnFm) != parseInt(AcceptedQty)) {
                $(this).find(".FreshReturn input").focus().addClass("border-warning");
                toastr.error('Fresh+Damage quantity should be equal to accepted quantity.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                flag2 = true
            }
        });
        var ReferenceOrderAutoId = 0;
        $("#tblduePayment tbody tr").each(function () {
            var row = $(this);
            if (row.find('.Actions input').prop('checked') == true) {
                if (row.find(".OrderNo span").attr("OrderAutoId") != '') {
                    ReferenceOrderAutoId = Number(row.find(".OrderNo span").attr("OrderAutoId")) || 0;
                }
            }
        })
        if (!flag2) {
            if (!flag1) {
                $("#tblProductDetail tbody tr").each(function () {
                    Product.push({
                        'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
                        'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
                        'RequiredQty ': $(this).find('.AcceptedQty input').val(),
                        'QtyPerUnit': $(this).find('.UnitType').find('span').attr('qtyperunit'),
                        'UnitPrice': $(this).find('.UnitPrice input').val(),
                        'SRP': 0.00,
                        'Tax': $(this).find('.TaxRate span').attr('istaxable'),
                        'NetPrice': $(this).find('.NetPrice').text(),
                        'FreshReturnUnitAutoId': $(this).find('.FreshReturn select').val(),
                        'DamageReturnUnitAutoId': $(this).find('.DemageReturn select').val(),
                        'MissingItemUnitAutoId': $(this).find('.MissingItem select').val(),
                        'FreshReturnQty': $(this).find('.FreshReturn input').val(),
                        'DamageReturnQty': $(this).find('.DemageReturn input').val(),
                        'MissingItemQty': $(this).find('.MissingItem  input').val()
                    });
                });
                var data = {
                    CreditAutoId: $("#CreditAutoId").val(),
                    ManagerRemark: $("#txtManagerRemarks").val(),
                    ReferenceOrderAutoId: ReferenceOrderAutoId,
                    CreditMemoType: $("#ddlCreditMemoType").val(),
                    OverallDisc: $("#txtOverallDisc").val(),
                    OverallDiscAmt: $("#txtDiscAmt").val(),
                }
                $.ajax({
                    type: "Post",
                    url: "/Manager/WebAPI/MCreditMemo.asmx/updateOrderData1",
                    data: "{'TableValues':'" + JSON.stringify(Product) + "','dataValue':'" + JSON.stringify(data) + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            timeout: 2000, //unblock after 2 seconds
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },

                    success: function (data) {
                        if (data.d != "Session Expired") {
                            swal("", "Credit Memo updated successfully.", "success").then(function () {
                                location.reload();
                            });;
                            $("#txtManagerRemarks").text('');
                        } else {
                            location.href = '/';
                        }
                    },
                    error: function (result) {
                        swal("", "Oops, Something went wrong.", "error");

                    },
                    failure: function (result) {
                        swal("", result.d, "error");
                    }
                });
            }
            else {
                //if (AcptdMsg == 1) {
                //    toastr.error("Accepted quantity can't be left blank or zero.", 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                //}
                //else {
                    toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                //}
            }
        }
    }
});

function print_NewOrder() {
    window.open("/Manager/CreditMemoPrint.html?PrintAutoId=" + $("#CreditAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}

function CustCreditmemoRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlsreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {

            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}

function reqdremarkCreditmemoRequiredField() {
    var boolcheck = true;
    $('.reqdremark').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}

// update by satish  11/05/2019

var check = false;
$('#btnCancelCreditMemo').click(function () {

    if (check == false) {
        $("#SecurityEnabledVoid").modal('show');
    } else {
        $("#ModelCancelRemark").modal('show');
    }
})

function clickonSecurityVoid() {
    if ($("#txtSecurityVoid").val() == "") {
        $("#txtSecurityVoid").addClass('border-warning');
        toastr.error('Security key is required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    var data = {
        Security: $("#txtSecurityVoid").val()
    }
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/MCreditMemo.asmx/CheckSecurity",
        data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'SessionExpired') {
                location.href = '/';
            } else {
                if (check == false) {
                    var xmldoc = $.parseXML(response.d);
                    var orderList = $(xmldoc).find("Table");
                    if (orderList.length > 0) {
                        $.each(orderList, function () {
                            if ($("#txtSecurityVoid").val() == $(orderList).find("SecurityValue").text()) {
                                check = true
                                $("#SecurityEnabledVoid").modal('hide');
                                $("#ModelCancelRemark").modal('show');
                            }
                        });
                    }
                    else {
                        swal("", "Access denied.", "warning", {
                            button: "OK",
                        }).then(function () {
                            $("#txtSecurityVoid").val('');
                            $("#txtSecurityVoid").focus();
                        })
                    }
                }
                else {
                    $("#SecurityEnabledVoid").modal('hide');
                }
            }
        },
        failure: function (result) {
            swal("", "Oops! Something went wrong. Please try later.", "error");
        },
        error: function (result) {
            swal("", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}
function SaveCancelReason() {
    if ($("#ReasonCancel").val().trim() == "") {
        $("#ReasonCancel").addClass('border-warning');
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        return;
    }
    cancelRemark();
}


function cancelRemark() {
    swal({
        title: "Are you sure?",
        text: "You want to cancel this credit memo.",
        icon: "warning",
        showCancelButton: true,
        closeOnClickOutside: false,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Cancel it.",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            Cancelcreditmemo2();
        }
    })
}

function Cancelcreditmemo2() {
    var data = {
        CancelRemark: $("#ReasonCancel").val().trim(),
        CreditAutoId: $('#CreditAutoId').val()
    }
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/MCreditMemo.asmx/CancelCreditMomo",
        //data: "{'datavalue':'" + JSON.stringify(data) + "'}",
        data: JSON.stringify({ datavalue: JSON.stringify(data)}),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {

            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (response) {
            if (response.d != 'Session Expired') {
                if (response.d != 'false') {
                    swal("", "Credit memo cancelled successfully.", "success");
                    $("#ModelCancelRemark").modal('hide');
                    $("#ReasonCancel").val('');
                    editCredit($('#CreditAutoId').val());
                    Orderrmarkdeatil($('#CreditAutoId').val());

                } else {
                    swal("", "Oops, Something went wrong.", "error");
                }
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function blockCopyPaste() {
    $('#txtOverallDisc').on("cut copy paste", function (e) {
        e.preventDefault();
    });
    $('#txtDiscAmt').on("cut copy paste", function (e) {
        e.preventDefault();
    });
    $("#tblProductDetail input").on("cut copy paste", function (e) {
        e.preventDefault();
    });
}
