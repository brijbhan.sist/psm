﻿
var Print = 1, PrintLabel = '';
$(document).ready(function () { 
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var getid = getQueryString('OrderAutoId');
    if (getid != null) {
        getOrderData(getid);
    }

});
//Edit Order Details
/*-------------------------------------------------------------------------------------------------------------------------------*/
function getOrderData(OrderAutoId) {
    $.ajax({
        type: "POST",
        async: false,
        url: "/Manager/WebAPI/WPrintOrder.asmx/PrintOrderItemTemplate2",
        data: "{'OrderAutoId':'" + OrderAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (response) {
            if (response.d == 'Session Expired') {
                location.href = '/';
                return;
            }
            var xmldoc = $.parseXML(response.d);
            var CustDetails = $(xmldoc).find("Table");
            var items = $(xmldoc).find("Table1");
            var CreditMemoNo = $(xmldoc).find("Table2");
            $("#CustomerId").text($(CustDetails).find("CustomerId").text());
            $("#CustomerName").text($(CustDetails).find("CustomerName").text());
            $("#OrderNo").text($(CustDetails).find("OrderNo").text());
            $("#Address").html($(CustDetails).find("Address").text() + ", " + $(CustDetails).find("City").text() + ", " + $(CustDetails).find("StateCode").text() + " - " + $(CustDetails).find("Zipcode").text())
            var html = '<table class="table tableCSS tblProduct" id="tblProduct"><thead><tr>';
            html += '<tr><td class="ProId" style="width: 10px; white-space: nowrap">Product Id</td><td class="ProName" style="white-space: nowrap">Item</td>';
            html += '<td class="UnitType" style="width: 10px; white-space: nowrap">Unit Type</td><td class="Qty" style="width: 10px">Qty</td>';
            html += '<td class="UnitPrice" style="text-align: right; width: 10px">Price</td>';
            html += '<td class="totalPrice" style="text-align: right; width: 10px; white-space: nowrap">Total Price</td></tr></thead><tbody>';



            var row = $("#tblProduct thead tr").clone(true);
            var catId, catName, productname = '', Qty = 0, total = 0.00, count = 1, tr, totalPrice = 0.00;
            var pageNo = 1, rowcount = 0, TotalMLTax = 0.00, TotalMLQty = 0.00;
            var orderQty = 0; var total = 0;
            $.each(items, function () {

                orderQty = $(this).find("QtyShip").text();
                if (parseInt($(this).find("QtyShip").text()) == 0) {
                    totalPrice = 0.00;
                } else {
                    totalPrice = parseFloat($(this).find("NetPrice").text());
                }

                rowcount = parseInt(rowcount) + 1;
                if ($(this).find("IsExchange").text() == 1) {
                    productname = $(this).find("ProductName").text() + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Exchange)' + "</span>";
                }
                else if ($(this).find("isFreeItem").text() == 1) {
                    productname = $(this).find("ProductName").text() + '  ' + "<span style='font-style:italic; font-weight:600'>" + '(Free)' + "</span>";
                }
                else {
                    productname = $(this).find("ProductName").text();
                }

                total += totalPrice;
                html += "<tr><td>" + $(this).find("ProductId").text() + "</td><td>" + productname.replace(/&quot;/g, "\'") + "</td><td>" + $(this).find("UnitType").text() + "</td><td>" + orderQty + "</td>";
                html += "<td style='text-align: right; width: 10px; white-space: nowrap'>" + $(this).find("UnitPrice").text() + "</td>";
                html += "<td style='text-align: right; width: 10px; white-space: nowrap'>" + totalPrice.toFixed(2) + "</td>";
                html += "</tr>";
            }
            );
            html += '</tbody>';
            html += '<tfoot>';
            html += "<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Sub Total</b></td><td style='text-align:right'>" + parseFloat(total).toFixed(2) + "</td></tr>"
            if ($(CustDetails).find("OverallDiscAmt").text() != 0) {
                html += "<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Discount</b></td><td style='text-align:right'>" + parseFloat($(CustDetails).find("OverallDiscAmt").text()).toFixed(2) + "</td></tr>"
            }
            var CrNo = 'Credit Memo Applied [', creditLabel = '';
            if ($(CustDetails).find("CreditAmount").text() != 0) {
                $.each(CreditMemoNo, function (index) {
                    if (index == CreditMemoNo.length - 1) {
                        CrNo += $(this).find("CreditNo").text() + "]";
                    }
                    else {
                        CrNo += $(this).find("CreditNo").text() + ",";
                    }
                })
                if (CreditMemoNo.length > 0) {
                    creditLabel = CrNo;
                }
                if (parseFloat($(CustDetails).find("CreditAmount").text()) > parseFloat(total)) {
                    html += "<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>" + creditLabel + "</b></td><td style='text-align:right'>-" + parseFloat(total).toFixed(2) + "</td></tr>"
                }
                else {
                    html += "<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>" + creditLabel + "</b></td><td style='text-align:right'>-" + parseFloat($(CustDetails).find("CreditAmount").text()).toFixed(2) + "</td></tr>"
                }
            }
            if ($(CustDetails).find("AdjustmentAmt").text() != 0) {
                html += "<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Adjustment</b></td><td style='text-align:right'>" + parseFloat($(CustDetails).find("AdjustmentAmt").text()).toFixed(2) + "</td></tr>"
            }
                html += "<tr><td colspan='2'></td><td colspan='3' style='text-align:center'><b>Grand Total</b></td><td style='text-align:right'>" + parseFloat($(CustDetails).find("PayableAmount").text()).toFixed(2) + "</td></tr>"
            html += '</tfoot>';
            html += '</table > ';
            $('#tableDynamic').html(html);
            $('.TotalPage').html(pageNo);

            setTimeout(function () { window.print(); }, 200);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

