﻿$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    var CreditAutoId = getQueryString('PageId');
    bindDropdown();
    if (CreditAutoId != null) {
        $("#btnSave").hide();
        $("#btnReset").hide();
        $("#btnUpdate").show();
        $("#btnPrintOrder").show();
        $("#CreditAutoId").val(CreditAutoId);
        editCredit(CreditAutoId);
    } else {
        $("#btnSave").show();
        $("#btnReset").show();
        $("#btnUpdate").hide();
        $("#btnPrintOrder").hide();
    }

    $("#ddlProduct").select2().on("select2:select", function (e) {

    });

    $("#ddlCustomer").select2().on("select2:select", function (e) {
    });
});
function bindDropdown() {

    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemo.asmx/bindDropdown",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var customer = $(xmldoc).find("Table");
                var product = $(xmldoc).find("Table1");
                $("#ddlCustomer option:not(:first)").remove();
                $.each(customer, function () {
                    $("#ddlCustomer").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Customer").text() + "</option>");
                });
                $("#ddlCustomer").select2();
                var CurrentDate = $(xmldoc).find("Table2");
                $("#txtOrderDate").val($(CurrentDate).find('CurrentDate').text());
                $("#CreditType").val($(CurrentDate).find('CreditType').text());

            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
var MLTaxRate = 0.00;
function changeCustomer() {
    if ($("#ddlCustomer").val() != "0") {
        $("#btnAdd").removeAttr('disabled')
    } else {
        $("#btnAdd").attr('disabled', 'disabled')

    }
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemo.asmx/BindProduct",
        data: "{'CustomerAutoId':'" + $("#ddlCustomer").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var product = $(xmldoc).find("Table");
                var MLTaxDetails = $(xmldoc).find("Table1");
                var TaxDetails = $(xmldoc).find("Table2");
                $("#ddlProduct option:not(:first)").remove();
                $.each(product, function () {
                    $("#ddlProduct").append("<option MLQty='" + $(this).find("MLQty").text() + "' value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ProductName").text() + "</option>");
                });
                $("#ddlProduct").select2();
                if (MLTaxDetails.length > 0) {
                    MLTaxRate = $(MLTaxDetails).find('TaxRate').text();
                }
                $("#ddlTaxType option").remove();
                $.each(TaxDetails, function () {
                    $("#ddlTaxType").append("<option taxvalue='" + $(this).find("Value").text() + "' value='" + $(this).find("AutoId").text() + "'>" + $(this).find("TaxableType").text() + "</option>");
                });
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function ChangeProduct() {

    var productAutoId = $("#ddlProduct option:selected").val();
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemo.asmx/bindUnitType",
        data: "{'ProductAutoId':" + productAutoId + "}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var unitType = $(xmldoc).find("Table");
                var unitDefault = $(xmldoc).find("Table1");
                var count = 0;

                $("#ddlUnitType option:not(:first)").remove();
                $.each(unitType, function () {
                    $("#ddlUnitType").append("<option value='" + $(this).find("AutoId").text() + "' QtyPerUnit='" + $(this).find("Qty").text() + "' >" + $(this).find("UnitType").text() + " (" + $(this).find("Qty").text() + " pcs" + ")</option>");
                });

                if (unitDefault.length > 0) {
                    $("#ddlUnitType").val($(unitDefault).find('AutoId').text()).change();
                } else {
                    $("#ddlUnitType").val(0);
                }
            } else {
                location.href = '/';
            }

        },
        error: function (result) {
            console.log(result.d);
        },
        failure: function (result) {
            console.log(result.d);
        }
    });

}

function AddinList() {
    debugger;
    var chkIsTaxable = $('#chkIsTaxable').prop('checked');
    var IsTaxable = 0;
    var taxType = ''
    if (chkIsTaxable == true) {
        IsTaxable = 1;

        taxType = '<tax class="la la-check-circle success"></tax>';
    }
    var MLQty = $("#ddlProduct option:selected").attr('MLQty');
    if (CustCreditmemoRequiredField()) {
        var data = {
            CustomerAutoId: $("#ddlCustomer").val(),
            ProductAutoId: $("#ddlProduct").val(),
            UnitAutoId: $("#ddlUnitType").val(),
            Qty: $("#txtReqQty").val(),
        }
        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/WCreditMemo.asmx/ValidateCreditMemo",
            data: "{'dataValue':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            async: false,
            success: function (response) {
                if (response.d != "Session Expired") {
                    $("#ddlCustomer").attr('disabled', true);
                    var flag = false;
                    var xmldoc = $.parseXML(response.d);
                    var items = $(xmldoc).find("Table");
                    $("#emptyTable").hide();
                    if (items.length > 0) {
                        if (Number($(items).find('ReturnQty').text()) <= Number($(items).find('Qty').text())) {
                            $("#tblProductDetail tbody tr").each(function () {
                               
                                if ($(this).find(".ProName span").attr('ProductAutoId') == $(items).find('ProductAutoId').text() &&
                                    $(this).find(".UnitType span").attr('UnitAutoId') == $(items).find('UnitAutoId').text()) {
                                    var qty = (parseInt($(items).find('ReturnQty').text()) + parseInt($(this).find('.ReturnQty input').val())) || 1;
                                    $(this).find('.ReturnQty input').val(qty);
                                    var NetPrice = parseFloat($(items).find('NetPrice').text()) * parseInt(qty);
                                    $(this).find('.NetPrice').text(NetPrice.toFixed(2));
                                    flag = true;
                                    $('#tblProductDetail tbody tr:first').before($(this));

                                    toastr.success('Product added successfully .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                                }
                                else if ($(this).find(".ProName span").attr('ProductAutoId') == $(items).find('ProductAutoId').text() &&
                                    $(this).find(".UnitType span").attr('UnitAutoId') != $(items).find('UnitAutoId').text()) {
                                    swal("Warning!", "You can't add different unit of added product.", "error");
                                    flag = true;
                                    return;
                                }
                            });
                            if (!flag) {
                                var row = $("#tblProductDetail thead tr").clone(true);
                                $.each(items, function () {
                                    $(".ProId", row).text($(this).find("ProductId").text());
                                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                                    $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                                    $(".TaxRate", row).html(taxType + "<span MLQty='" + $(this).find("MLQty").text() + "' IsTaxable='" + IsTaxable + "'> </span>");
                                    $(".ReturnQty", row).html("<span MaxQty ='" + $(this).find('Qty').text() + "'></span><input type='text' class='form-control input-sm  border-primary text-center' onkeyup='getcalCulation(this)' onkeypress='return isNumberKey(event)' style='width:100px;' value='" + $(this).find("ReturnQty").text() + "' />");
                                    $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' onkeyup='changePrice(this)' onkeypress='return isNumberDecimalKey(event,this)' style='width:100px;text-align:right;' value='" + $(this).find("CostPrice").text() + "' />");
                                    $(".SRP", row).text($(this).find("SRP").text());
                                    $(".AcceptedQty", row).text($(this).find("ReturnQty").text());
                                    $(".NetPrice", row).text($(this).find("NetPrice").text());
                                    $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                                    if ($('#tblProductDetail tbody tr').length > 0) {
                                        $('#tblProductDetail tbody tr:first').before(row);
                                    }
                                    else {
                                        $('#tblProductDetail tbody').append(row);
                                    }
                                    row = $("#tblProductDetail tbody tr:last").clone(true);

                                });

                                toastr.success('Product added successfully .', 'Success', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                            }
                            $("#txtReqQty").val('');
                            $("#ddlProduct").val('0').change();
                        } else {

                            if (Number($(items).find('UnitAutoId').text()) == 1) {
                                swal("Warning!", "Return qty should be less or equal to Order Qty : " + $(items).find('Qty').text() + '[case]', "error");
                            } else if (Number($(items).find('UnitAutoId').text()) == 2) {
                                swal("Warning!", "Return qty should be less or equal to Order Qty : " + $(items).find('Qty').text() + '[box]', "error");
                            } else {
                                swal("Warning!", "Return qty should be less or equal to Order Qty : " + $(items).find('Qty').text() + '[pcs]', "error");

                            }

                        }

                        $("#txtReqQty").val(1);
                        calTotalAmount();
                        $('#chkIsTaxable').prop('checked',false);

                    } else {

                        swal("Error!", "No order qty ramining for credit memo", "error");
                    }
                } else {
                    location.href = '/';
                }
            },
            failure: function (result) {
                swal("Error!", result.d, "error");
            },
            error: function (result) {
                swal("Error!", result.d, "error");
            }
        });
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function getcalCulation(e) {


    var tr = $(e).closest('tr');
    var maxReturnQty = 0.00;
    maxReturnQty = tr.find('.ReturnQty span').attr('maxqty');
    var ReturnQty = 0;
    if (tr.find('.ReturnQty input').val() != '') {
        ReturnQty = tr.find('.ReturnQty input').val();
    }
    tr.find('.AcceptedQty').text(ReturnQty);
    var UnitType = tr.find('.UnitType span').attr('unitautoid');
    if (Number(ReturnQty) > Number(maxReturnQty)) {

        if (Number(UnitType) == 1) {
            swal("Warning!", "Return qty should be less or equal to Order Qty : " + maxReturnQty + '[case]', "error");

        } else if (Number(UnitType) == 3) {
            swal("Warning!", "Return qty should be less or equal to Order Qty : " + maxReturnQty + '[Box]', "error");

        } else {
            swal("Warning!", "Return qty should be less or equal to Order Qty : " + maxReturnQty + '[pcs]', "error");
        }
        $(e).val(maxReturnQty);
    }
    changePrice(e);
}

function deleterow(e) {
    $(e).closest('tr').remove();
    if ($("#tblProductDetail tbody tr").length == 0) {
        $("#ddlCustomer").attr('disabled', false);
        $("#emptyTable").show();
    } else {
        $("#emptyTable").hide();
    }
    calTotalAmount();
}

function deleterows(e) {

    swal({
        title: "Are you sure?",
        text: "You want to delete this credit memo.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: true,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Delete it!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            deleterows(e);

        } else {
            swal("Success", "Credit memo is safe:)", "success");
        }
    })
}


function calTotalAmount() {
    var total = 0.00;
    $("#tblProductDetail tbody tr").each(function () {
        total += Number($(this).find(".NetPrice").text());
    });
    $("#txtTotalAmount").val(total.toFixed(2));
    calTotalTax();
}
function calTotalTax() {

    var totalTax = 0.00, qty;
    var MLQty = 0.00;
    $("#tblProductDetail tbody tr").each(function () {
        qty = Number($(this).find(".ReturnQty input").val());
        MLQty = parseFloat(MLQty) + (parseFloat($(this).find('.TaxRate span').attr('MLQty')) * qty * parseFloat($(this).find('.UnitType span').attr('qtyperunit')));
        if ($(this).find('.TaxRate span').attr('istaxable') == 1) {
            var totalPrice = Number($(this).find(".UnitPrice input").val()) * qty;
            var Disc = totalPrice * Number($("#txtOverallDisc").val()) * 0.01;
            var priceAfterDisc = totalPrice - Disc;

            if ($("#ddlTaxType").val() != null) {
                totalTax += (priceAfterDisc * Number($('#ddlTaxType option:selected').attr("TaxValue")) * 0.01) || 0;
            }

        }
    });

    $("#txtMLQty").val(MLQty.toFixed(2));
    $("#txtTotalTax").val(totalTax.toFixed(2));
    $("#txtMLTax").val((MLQty * MLTaxRate).toFixed(2));
    calGrandTotal();
}

function calOverallDisc1() {
    //At this function check the quantity and unit price null and empty 
    //thats reasion not work proper the calculation so thats reasion remove the code
    var DiscAmt = parseFloat($("#txtDiscAmt").val()) || 0.00;
    var TotalAmount = (Number($("#txtTotalAmount").val())) || 0.00;
    var per = 0.00;
    if (TotalAmount > 0) {
        per = ((DiscAmt / TotalAmount) * 100) || 0;
    }
    if (DiscAmt > TotalAmount) {
        toastr.error('Discount Amount should be less than Total Amount.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        $("#txtDiscAmt").val('0.00');
        $("#txtOverallDisc").val('0.00');
        $("#txtDiscAmt").select();
    } else {
        $("#txtOverallDisc").val(per.toFixed(2));
    }
    calTotalTax();
}
function calOverallDisc() {
    //At this function check the quantity and unit price null and empty 
    //thats reasion not work proper the calculation so thats reasion remove the code
    var factor = 0.00;
    factor = Number($("#txtOverallDisc").val()) * 0.01;
    $("#txtDiscAmt").val((Number($("#txtTotalAmount").val()) * factor).toFixed(2));
    calTotalTax();

}
function calGrandTotal() {
    var DiscAmt = 0.00;
    if ($("#txtDiscAmt").val() != "") {
        DiscAmt = parseFloat($("#txtDiscAmt").val())
    }
    var grandTotal = Number($("#txtTotalAmount").val()) - parseFloat(DiscAmt) + Number($("#txtTotalTax").val()) + parseFloat($("#txtMLTax").val());;
    var round = Math.round(grandTotal);
    $("#txtAdjustment").val((round - grandTotal).toFixed(2));
    $("#txtGrandTotal").val(round.toFixed(2));
}

function changePrice(e) {

    var row = $(e).closest("tr");
    $(row).find(".UnitPrice input").css("border-color", "#eee");
    var NetPrice = 0.0;
    var UnitPrice = Number(row.find(".UnitPrice > input").val());
    var ReturnQty = Number(row.find(".ReturnQty input").val());
    NetPrice = parseFloat(Number(ReturnQty) * parseFloat(UnitPrice));
    $(row).find('.NetPrice').text(NetPrice.toFixed(2));
    calTotalAmount();
}

function refresh() {
    $("#tblProductDetail tbody tr").remove();
    calTotalAmount();

}

function Reset() {
    refresh();
    $("#ddlCustomer").val('0').change();
    $("#txtRemarks").val('');
    $("#txtOverallDisc").val('0.00');
    $("#txtDiscAmt").val('0.00');
    $("#txtAdjustment").val('0.00');
    $("#txtGrandTotal").val('0.00');
    $("#ddlCustomer").attr('disabled', false);
}

$("#btnSave").click(function () {
    var flag1 = false, flag2 = false, flag3 = false;

    if ($('#tblProductDetail tbody tr').length > 0) {
        $("#tblProductDetail tbody tr").each(function () {
            if ($(this).find(".ReturnQty input").val() == "" || $(this).find(".ReturnQty input").val() == null || $(this).find(".ReturnQty input").val() == 0) {
                $(this).find(".ReturnQty input").focus().addClass("border-warning");
                flag1 = true;
            }
            if ($(this).find(".UnitPrice input").val() == "" || $(this).find(".UnitPrice input").val() == null) {
                $(this).find(".UnitPrice input").focus().addClass("border-warning");
                flag1 = true;
            }
        });

        if (!flag1) {

            var taxvalue = 0;
            var overallDisc = 0.00;
            var overallDiscAmt = 0.00;
            if ($("#ddlTaxType").val() != null) {
                taxvalue = $('#ddlTaxType option:selected').attr("TaxValue");
            }
            if ($("#txtOverallDisc").val() != "") {
                overallDisc = $("#txtOverallDisc").val();
            }
            if ($("#txtDiscAmt").val() != "") {
                overallDiscAmt = $("#txtDiscAmt").val();
            }

            var orderData = {
                CustomerAutoId: $("#ddlCustomer").val(),
                Remarks: $("#txtRemarks").val(),
                TotalAmount: $("#txtTotalAmount").val(),
                OverallDisc: overallDisc,
                OverallDiscAmt: overallDiscAmt,
                TotalTax: $("#txtTotalTax").val(),
                GrandTotal: $("#txtGrandTotal").val(),
                TaxType: $("#ddlTaxType").val(),
                TaxValue: taxvalue,
                MLQty: parseFloat($("#txtMLQty").val()),
                MLTax: parseFloat($("#txtMLTax").val()),
                AdjustmentAmt: parseFloat($("#txtAdjustment").val())
            };
            var Product = [];
            var SRP = 0;
            $("#tblProductDetail tbody tr").each(function () {
                if ($(this).find('.SRP').text() != '') {
                    SRP = $(this).find('.SRP').text();
                } else {
                    SRP = 0;
                }
                Product.push({
                    'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
                    'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
                    'RequiredQty ': $(this).find('.ReturnQty input').val(),
                    'QtyPerUnit': $(this).find('.UnitType').find('span').attr('qtyperunit'),
                    'UnitPrice': $(this).find('.UnitPrice input').val(),
                    'SRP': SRP,
                    'Tax': $(this).find('.TaxRate').find('span').attr('istaxable'),
                    'NetPrice': $(this).find('.NetPrice').text()
                });
            });
            $.ajax({
                type: "Post",
                url: "/Sales/WebAPI/WCreditMemo.asmx/insertOrderData",
                data: "{'TableValues':'" + JSON.stringify(Product) + "','dataValue':'" + JSON.stringify(orderData) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    if (data.d != "Session Expired") {
                        if (data.d == 'true') {
                            swal("", "Credit memo generated successfully.", "success").then(function () {
                                Reset();

                            });
                        } else {
                            swal("Error !", data.d, "error");

                        }

                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    swal("Error!", "Oops, some things went wrongs.", "error");

                },
                failure: function (result) {
                    swal("Error!", result.d, "error");
                }
            });
        } else {
            toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
    }
    else {
        toastr.error('No Product Added into the Table.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
})

$('.close').click(function () {
    $(this).closest('div').hide();

});
function editCredit(CreditAutoId) {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemo.asmx/editCredit",
        data: "{'CreditAutoId':'" + CreditAutoId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var CreditOrder = $(xmldoc).find("Table");
                var items = $(xmldoc).find("Table1");
                var EmpType = $(xmldoc).find("Table2");

                $("#txtOrderId").val($(CreditOrder).find("CreditNo").text());
                $("#txtOrderDate").val($(CreditOrder).find("CreditDate").text());
                $("#txtOrderStatus").val($(CreditOrder).find("STATUS").text());
                $("#txtRemarks").val($(CreditOrder).find("Remarks").text()).attr('disabled', true)
                $("#ddlCustomer").val($(CreditOrder).find("CustomerAutoId").text()).change();
                $("#txtManagerRemarks").val($(CreditOrder).find("ManagerRemark").text());

                $("#ddlCustomer").attr('disabled', true);
                $("#hiddenOrderStatus").val($(CreditOrder).find("StatusCode").text());
                var row = $("#tblProductDetail thead tr").clone(true);
                $.each(items, function () {
                    $(".ProId", row).text($(this).find("ProductId").text());
                    $(".ProName", row).html("<span ProductAutoId='" + $(this).find("ProductAutoId").text() + "' class='" + $(this).find("ProductId").text() + "'>" + $(this).find("ProductName").text() + "</span>");
                    $(".UnitType", row).html("<span UnitAutoId='" + $(this).find("UnitAutoId").text() + "' QtyPerUnit='" + $(this).find("QtyPerUnit").text() + "'>" + $(this).find("UnitType").text() + " (" + $(this).find("QtyPerUnit").text() + " pcs" + ")</span>");
                    $(".AcceptedQty", row).html("<span MAXQty='" + $(this).find("MAXQty ").text() + "'></span><input disabled='disabled' type='text' class='form-control input-sm' onkeyup='getcalCulation1(this)' onkeypress='return isNumberKey(event)' style='width:100px;' value='" + $(this).find("AcceptedQty").text() + "' />");
                    if ($(EmpType).find('EmpType').text() != 8) {
                        if ($(CreditOrder).find("StatusCode").text() == 1) {
                            $(".ReturnQty", row).html("<span MAXQty='" + $(this).find("MAXQty ").text() + "'></span><input type='text' class='form-control input-sm' onkeyup='getcalCulation(this)' onkeypress='return isNumberKey(event)' style='width:100px;' value='" + $(this).find("RequiredQty").text() + "' />");
                        } else {
                            $(".ReturnQty", row).html($(this).find("RequiredQty").text());
                        }
                    } else {

                        $(".ReturnQty", row).html($(this).find("RequiredQty").text());
                    }

                    if ($(CreditOrder).find("StatusCode").text() == 1) {
                        if ($(EmpType).find('EmpType').text() == 8) {
                            $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' onkeyup='changePrice1(this)' style='width:100px;text-align:right;border:1px solid #3b4781;'  onkeypress='return isNumberDecimalKey(event,this)' value='" + $(this).find("ManagerUnitPrice").text() + "' />");

                        } else {
                            $(".UnitPrice", row).html("<input type='text' class='form-control input-sm border-primary' onkeyup='changePrice(this)' style='width:100px;text-align:right;border:1px solid #3b4781;'  onkeypress='return isNumberDecimalKey(event,this)' value='" + $(this).find("ManagerUnitPrice").text() + "' />");
                        }
                    } else {
                        $(".UnitPrice", row).html($(this).find("UnitPrice").text());
                    }
                    $(".SRP", row).text($(this).find("SRP").text());
                    $(".TaxRate", row).html('<span mlqty=' + $(this).find("UnitMLQty").text() + ' istaxable=' + $(this).find("TaxRate").text() + '>' + (($(this).find("TaxRate").text() == 1) ? '<tax class="la la-check-circle success"></span>' : '') + '</tax>');
                    $(".NetPrice", row).text($(this).find("NetAmount").text());
                    $(".Action", row).html("<a href='javascript:;' id='deleterow' onclick='deleterow(this)'><span class='ft-x'></span></a>");
                    $('#tblProductDetail tbody').append(row);
                    row = $("#tblProductDetail tbody tr:last").clone(true);

                });
                if ($(EmpType).find('EmpType').text() == 8) {
                    $(".AcceptedQty").show();
                }
                $("#btnPrintOrder").show();
                if ($(EmpType).find('EmpType').text() != 6) {
                    if ($(EmpType).find('EmpType').text() == 2 || $(EmpType).find('EmpType').text() == 1) {
                        if ($(CreditOrder).find("StatusCode").text() == 1) {
                            $("#btnUpdate").show();
                            $(".Action").show();
                            $('#btnAdd').closest('#Productpannel').show();
                            $("#txtDiscAmt").attr('disabled', false);
                            $("#txtOverallDisc").attr('disabled', false);
                            $("#ddlTaxType").attr('disabled', false);
                        } else {
                            $("#btnUpdate").hide();
                            $(".AcceptedQty").show();
                            $(".Action").hide();
                            $('#btnAdd').closest('#Productpannel').hide();
                            $("#txtDiscAmt").attr('disabled', true);
                            $("#txtOverallDisc").attr('disabled', true);
                            $("#ddlTaxType").attr('disabled', true);
                        }
                    } else {
                        if ($(CreditOrder).find("StatusCode").text() == 1) {
                            $("#btnUpdate").hide();
                            $("#btnBackOrder").show();
                            $('.ReturnQty input').attr('disabled', true);
                            $('.UnitPrice input').attr('disabled', true);
                            $("#btnApproved").show();
                            $("#btnCancel").show();
                            $(".Action").hide();
                            $('#btnAdd').closest('#Productpannel').hide();
                        } else {

                            if ($(EmpType).find('EmpType').text() == 6 && $(CreditOrder).find("StatusCode").text() == 2) {
                                $("#btnComplete").show();
                            } else {
                                $("#btnComplete").hide();
                            }

                            $("#btnUpdate").hide();
                            $("#btnApproved").hide();
                            $("#btnCancel").hide();
                            $(".Action").hide();
                            $("#Productpannel").hide();
                            $('#btnAdd').closest('#Productpannel').hide();

                        }
                        if ($(EmpType).find('EmpType').text() == 10) {
                            $('#btnSave').closest('.row').hide();
                        }
                    }
                    $("#txtTotalAmount").val($(CreditOrder).find("GrandTotal").text());
                    $("#txtOverallDisc").val($(CreditOrder).find("OverallDisc").text());
                    $("#txtDiscAmt").val($(CreditOrder).find("OverallDiscAmt").text());
                    $("#txtTotalTax").val($(CreditOrder).find("TotalTax").text());
                    $("#txtGrandTotal").val($(CreditOrder).find("TotalAmount").text());
                    $("#txtMLQty").val($(CreditOrder).find("MLQty").text());
                    $("#txtMLTax").val($(CreditOrder).find("MLTax").text());
                    $("#txtAdjustment").val($(CreditOrder).find("AdjustmentAmt").text());
                    MLTaxRate = parseFloat((CreditOrder).find("ApplyMLQty").text());
                } else {
                    $("#btnUpdate").hide();
                    $("#btnApproved").hide();
                    $("#btnCancel").hide();
                    $(".Action").hide();
                    $(".AcceptedQty").show();
                    $(".ReturnQty input").attr('disabled', true);
                    $(".AcceptedQty input").attr('disabled', true);
                    $(".UnitPrice input").attr('disabled', true);
                    $('#btnAdd').closest('#Productpannel').hide();
                    $("#btnComplete").hide();
                }

                if ($(EmpType).find('EmpType').text() == 8) {
                    if ($(CreditOrder).find("StatusCode").text() == 1) {
                        $("#hideRemark").show();
                    }
                    else {
                        $("#hideRemark").show()
                        $("#txtManagerRemarks").attr('disabled', true);



                    }
                }
                if ($(EmpType).find('EmpType').text() == 8) {
                    if ($(CreditOrder).find("StatusCode").text() == 1) {
                        $("#txtDiscAmt").attr('disabled', false);
                        $("#txtOverallDisc").attr('disabled', false);
                        $("#ddlTaxType").attr('disabled', false);
                    }
                    else {
                        $("#txtDiscAmt").attr('disabled', true);
                        $("#txtOverallDisc").attr('disabled', true);
                        $("#ddlTaxType").attr('disabled', true);
                    }
                }
                else {
                    if ($(CreditOrder).find("StatusCode").text() == 3) {
                        if ($(CreditOrder).find("ManagerRemark").text() == "") {
                            $("#hideRemark").hide();
                        }
                        else {
                            $("#hideRemark").show()
                            $("#txtManagerRemarks").attr('disabled', true);
                        }

                    }
                    else if ($(CreditOrder).find("StatusCode").text() == 5) {
                        if ($(CreditOrder).find("ManagerRemark").text() == "") {
                            $("#hideRemark").hide();
                        }
                        else {
                            $("#hideRemark").show();
                            $("#txtManagerRemarks").attr('disabled', true);
                        }
                    }
                    else {
                        $("#hideRemark").hide();
                    }

                }

                if ($(CreditOrder).find("StatusCode").text() == '3') {
                    $('input[type="text"]').attr('readonly');
                }
            } else {
                location.href = '/';
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
$("#btnUpdate").click(function () {
    var flag1 = false;
    if ($('#tblProductDetail tbody tr').length > 0) {
        $("#tblProductDetail tbody tr").each(function () {
            if ($(this).find(".ReturnQty input").val() == "" || $(this).find(".ReturnQty input").val() == null || $(this).find(".ReturnQty input").val() == 0) {
                $(this).find(".ReturnQty input").focus().css("border-color", "orange");
                flag1 = true;
            }
            if ($(this).find(".UnitPrice input").val() == "" || $(this).find(".UnitPrice input").val() == null) {
                $(this).find(".UnitPrice input").focus().css("border-color", "orange");
                flag1 = true;
            }
        });

        if (!flag1) {

            var Product = [];
            var SRP = 0;
            var taxvalue = 0;
            var overallDisc = 0.00;
            var overallDiscAmt = 0.00;
            if ($("#ddlTaxType").val() != null) {
                taxvalue = $('#ddlTaxType option:selected').attr("TaxValue");
            }
            if ($("#txtOverallDisc").val() != "" && $("#txtOverallDisc").val() != null) {
                overallDisc = $("#txtOverallDisc").val();
            }
            if ($("#txtDiscAmt").val() != "") {
                overallDiscAmt = $("#txtDiscAmt").val();
            }
            $("#tblProductDetail tbody tr").each(function () {
                if ($(this).find('.SRP').text() != '') {
                    SRP = $(this).find('.SRP').text();
                } else {
                    SRP: 0;
                }
                Product.push({
                    'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
                    'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
                    'RequiredQty ': $(this).find('.ReturnQty input').val(),
                    'QtyPerUnit': $(this).find('.UnitType').find('span').attr('qtyperunit'),
                    'UnitPrice': $(this).find('.UnitPrice input').val(),
                    'SRP': SRP,
                    'Tax': $(this).find('.TaxRate').find('span').attr('istaxable'),
                    'NetPrice': $(this).find('.NetPrice').text()
                });
            });
            var orderData = {
                CustomerAutoId: $("#ddlCustomer").val(),
                Remarks: $("#txtRemarks").val(),
                TotalAmount: $("#txtTotalAmount").val(),
                OverallDisc: overallDisc,
                OverallDiscAmt: overallDiscAmt,
                TotalTax: $("#txtTotalTax").val(),
                GrandTotal: $("#txtGrandTotal").val(),
                TaxType: $("#ddlTaxType").val(),
                TaxValue: taxvalue,
                MLQty: parseFloat($("#txtMLQty").val()),
                MLTax: parseFloat($("#txtMLTax").val()),
                AdjustmentAmt: parseFloat($("#txtAdjustment").val())
            };
            $.ajax({
                type: "Post",
                url: "/Sales/WebAPI/WCreditMemo.asmx/updateOrderData",
                data: "{'TableValues':'" + JSON.stringify(Product) + "','dataValue':'" + JSON.stringify(orderData) + "','CreditAutoId':'" + $("#CreditAutoId").val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },

                success: function (data) {
                    if (data.d != "Session Expired") {
                        swal("", "Data has been update successfully.", "success");
                    } else {
                        location.href = '/';
                    }
                },
                error: function (result) {
                    swal("Error!", "Oops, some things went wrongs.", "error");
                },
                failure: function (result) {
                    swal("Error!", result.d, "error");
                }
            });
        } else {
            toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
    }
    else {
        swal("Error!", "No product added into the table.", "error");
    }

})
function ApprovedOrders(Status) {

    var confirmtest = '';
    var message = '';
    if (Status == 3) {
        message = 'Approved successfully';

    } else {
        message = 'Credit cancel successfully'
    }
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemo.asmx/ApprovedCredit",
        data: "{'CreditAutoId':" + $('#CreditAutoId').val() + ",Status:'" + Status + "',ManagerRemark:'" + $("#txtManagerRemarks").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            swal("", message, "success");
            $("#btnBackOrder").hide();
            editCredit($('#CreditAutoId').val());

        },
        failure: function (result) {
            swal("Error!", result.d, "error");
        },
        error: function (result) {

            swal("Error!", result.d, "error");

        }
    });
}

function ApprovedOrder() {
    swal({
        title: "Are you sure?",
        text: "You want to approved this credit memo.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm: {
                text: "Yes, Approved it!",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            ApprovedOrders(3);

        } else {
            swal("Success", "Thanks You:)", "success");
        }
    })
}

function Cancelcreditmemo() {
    if (reqdremarkCreditmemoRequiredField()) {

        swal({
            title: "Are you sure?",
            text: "You want to decline this credit memo.",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, cancel!",
                    value: null,
                    visible: true,
                    className: "btn-warning",
                    closeModal: false,
                },
                confirm:
                      {
                          text: "Yes, Decline it!",
                          value: true,
                          visible: true,
                          className: "",
                          closeModal: false
                      }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                ApprovedOrders(5);
            }
            else {
                swal("", "Thanks You:)", "error");

            }

        })
    }
    else {
        toastr.error('All * fields are mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function CompleteOrders() {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WCreditMemo.asmx/CompleteOrder",
        data: "{'CreditAutoId':" + $('#CreditAutoId').val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (response) {
            swal("", "Credit Memo has been completed successfully.", "success");
            editCredit($('#CreditAutoId').val());
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function CompleteOrder() {
    swal({
        title: "Are you sure?",
        text: "You want to Complete? This Credit Memo!",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: false,
            },
            confirm:
                  {
                      text: "Yes, Complete it!",
                      value: true,
                      visible: true,
                      className: "",
                      closeModal: true
                  }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            CompleteOrders();
        }
        else {
            swal("", "Thanks You:)", "error");
        }
    })
}
function getcalCulation1(e) {

    var tr = $(e).closest('tr');
    var maxReturnQty = 0.00;
    maxReturnQty = tr.find('.ReturnQty').text()
    var AcceptedQty = 0;
    if (tr.find('.AcceptedQty input').val() != '') {
        AcceptedQty = tr.find('.AcceptedQty input').val();
    }
    if (Number(AcceptedQty) > Number(maxReturnQty)) {
        $(e).val(maxReturnQty);
    }
    changePrice1(e);
}
$("#btnBackOrder").click(function () {
    $("#btnUpdate1").show();
    $("#hideRemark").show();
    $(this).hide();
    $("#btnApproved").hide();
    $("#btnCancel").hide();
    $("#btnBackupdate").show();
    $("#ddlTaxType").attr('disabled', true);
    $(".AcceptedQty input").attr('disabled', false);
    $(".UnitPrice input").attr('disabled', false);
});
$("#btnBackupdate").click(function () {
    $("#btnUpdate1").hide();
    $("#hideRemark").show();
    $(this).hide();
    $("#btnApproved").show();
    $("#btnCancel").show();
    $("#btnBackOrder").show();
    $(".AcceptedQty input").attr('disabled', true);
    $(".UnitPrice input").attr('disabled', true);
});
function changePrice1(e) {

    var tr = $(e).closest('tr');
    var AcceptedQty = 0.00;
    var UnitPrice = 0.00;
    if (tr.find('.AcceptedQty input').val() != '') {
        AcceptedQty = parseFloat(tr.find('.AcceptedQty input').val());
    }
    if (tr.find('.UnitPrice input').val() != '') {
        UnitPrice = parseFloat(tr.find('.UnitPrice input').val());
    }
    var NetAmount = parseFloat(AcceptedQty) * parseFloat(UnitPrice);
    tr.find('.NetPrice').text(NetAmount.toFixed(2));
    calTotalAmount1();
}

function calTotalAmount1() {
    var total = 0.00;
    $("#tblProductDetail tbody tr").each(function () {
        total += Number($(this).find(".NetPrice").text());
    });
    $("#txtTotalAmount").val(total.toFixed(2));
    calTotalTax1();
}
function calTotalTax1() {

    var totalTax = 0.00, qty;
    var MLQty = 0.00;
    $("#tblProductDetail tbody tr").each(function () {
        qty = Number($(this).find(".AcceptedQty input").val()) || 0;
        MLQty = parseFloat(MLQty) + (parseFloat($(this).find('.TaxRate span').attr('MLQty')) * qty * parseFloat($(this).find('.UnitType span').attr('qtyperunit')));

        if ($(this).find('.TaxRate span').attr('istaxable') == 1) {
            var totalPrice = Number($(this).find(".UnitPrice input").val()) * qty;
            var Disc = totalPrice * Number($("#txtOverallDisc").val()) * 0.01;
            var priceAfterDisc = totalPrice - Disc;
            totalTax += priceAfterDisc * Number($('#ddlTaxType option:selected').attr("TaxValue")) * 0.01;
        }
    });

    $("#txtMLQty").val(MLQty.toFixed(2));
    $("#txtTotalTax").val(totalTax.toFixed(2));
    $("#txtMLTax").val((MLQty * MLTaxRate).toFixed(2));
    calGrandTotal();
}
$("#btnUpdate1").click(function () {
    var Product = [];
    $("#tblProductDetail tbody tr").each(function () {

        Product.push({
            'ProductAutoId': $(this).find('.ProName').find('span').attr('ProductAutoId'),
            'UnitAutoId': $(this).find('.UnitType').find('span').attr('UnitAutoId'),
            'RequiredQty ': $(this).find('.AcceptedQty input').val(),
            'QtyPerUnit': $(this).find('.UnitType').find('span').attr('qtyperunit'),
            'UnitPrice': $(this).find('.UnitPrice input').val(),
            'SRP': $(this).find('.SRP').text() || 0,
            'Tax': $(this).find('.TaxRate span').attr('istaxable'),
            'NetPrice': $(this).find('.NetPrice').text()
        });
    });
    $.ajax({
        type: "Post",
        url: "/Sales/WebAPI/WCreditMemo.asmx/updateOrderData1",
        data: "{'TableValues':'" + JSON.stringify(Product) + "','CreditAutoId':'" + $("#CreditAutoId").val() + "','ManagerRemark':'" + $("#txtManagerRemarks").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },

        success: function (data) {
            if (data.d != "Session Expired") {
                swal("", "Details has been updated successfully.", "success");
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");

        },
        failure: function (result) {
            swal("Error!", result.d, "error");
        }
    });

});

function print_NewOrder() {
    window.open("/manager/CreditMemoPrint.html?PrintAutoId=" + $("#CreditAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}

function CustCreditmemoRequiredField() {
    var boolcheck = true;
    $('.req').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    $('.ddlsreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    $('.ddlreq').each(function () {
        if ($(this).val() == '' || $(this).val() == '0' || $(this).val() == 'undefined' || $(this).val() == 'Select') {
            boolcheck = false;
            $(this).closest('div').find('.select2-selection--single').attr('style', 'border:1px solid #FF9149  !important');
        } else {

            $(this).closest('div').find('.select2-selection--single').removeAttr('style');
        }
    });
    return boolcheck;
}

function reqdremarkCreditmemoRequiredField() {
    var boolcheck = true;
    $('.reqdremark').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });
    return boolcheck;
}

function UnitDetails() {
    debugger;
    if ($("#ddlUnitType option:selected").attr('eligibleforfree') == 1) {
        $('#chkFreeItem').attr('disabled', false);
    } else {
        $('#chkFreeItem').attr('disabled', true);
    }
    $('#chkFreeItem').prop('checked', false);
    var data = {
        ProductAutoId: $("#ddlProduct").val(),
        UnitAutoId: $("#ddlUnitType").val(),
        CustAutoId: $("#ddlCustomer").val()
    };

    if (data.UnitAutoId != 0) {
        $.ajax({
            type: "POST",
            url: "/Sales/WebAPI/WCreditMemo.asmx/selectQtyPrice",
            data: "{'dataValues':'" + JSON.stringify(data) + "'}",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var stockAndPrice = $(xmldoc).find("Table");
                    price = stockAndPrice.find("Price").text();
                    minPrice = stockAndPrice.find("MinPrice").text();
                    CustomPrice = stockAndPrice.find("CustomPrice").text();
                    taxRate = stockAndPrice.find("TaxRate").text();
                    SRP = stockAndPrice.find("SRP").text();
                    GP = stockAndPrice.find("GP").text();
                    if ($("#ddlUnitType").val() != 0) {
                        $("#alertStockQty").text('');
                        if (Number($("#ddlUnitType").val()) == 1) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " case ] ," +
                                " [ Base Price : $" + price + " ]");
                        } else if (Number($("#ddlUnitType").val()) == 2) {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " box ] ," +
                                " [ Base Price : $" + price + " ]");
                        } else {
                            $("#alertStockQty").append(" [ Stock : " + $(stockAndPrice).find("Stock").text() + " pcs ] ," +
                                " [ Base Price : $" + price + " ]");
                        }
                        $("#alertStockQty").show();
                    } else {
                        $("#alertStockQty").hide();
                    }
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
};

