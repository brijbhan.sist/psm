﻿var OrdAutoId = 0; Driver = 0; OrderStatus = 0;
localStorage.setItem('Driver', '0');
localStorage.setItem('Orderstatus', '0');
var planningDate; var ddlDrv = "", arrDrvId = [], TimeFrom = "", TimeTo = "", ddlCar = "", saveStatus = 0, ddlShippingType = "", ddlTemplateType="",dateDifference;
var Key = "5d928b200fa32bf01e0211417edb3f90oDHOnEILQQ";

$(document).ready(function () {
    bindStatus();
    BindAllOrderType();
    BindDriverDDL();
    getOrderList(1);
   
    $('#txtSFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    $('#txtSToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    $('#txtDeliveryFromDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    $('#txtDeliveryToDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    $('#txtAssignDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: false,
    });
    $('#txtPlanningDate').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy',
        selectYears: false,
        selectMonths: true,
    });
    var datePicker = $('#txtPlanningDate').pickadate('picker')
    datePicker.set('min', -7);
    datePicker.set('max', +7);
    datePicker.set('select', new Date());
    $("#OptimoKey").val('');
})
function Pagevalue(e) {
    getOrderList(parseInt($(e).attr("page")));
};
function bindStatus() {
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/bindStatus",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);    
                var status = $(xmldoc).find("Table");
                var customer = $(xmldoc).find("Table1");
                var SalesPerson = $(xmldoc).find("Table2");
                var ShippingType = $(xmldoc).find("Table4");
                var StateType = $(xmldoc).find("Table5");
                $("#ddlSStatus").attr('multiple', 'multiple')
                $("#ddlSStatus option:not(:first)").remove();
                $.each(status, function () {
                    if ($(this).find("AutoId").text() == '3' || $(this).find("AutoId").text() == '10') {
                        $("#ddlSStatus").append("<option selected='selected' value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                    }
                    else {
                        $("#ddlSStatus").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StatusType").text() + "</option>");
                    }
                });
                $("#ddlSStatus").select2({
                    placeholder: 'All Status',
                    allowClear: true
                });

                $("#ddlCustomer option:not(:first)").remove();
                $.each(customer, function () {
                    $("#ddlCustomer").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Customer").text() + "</option>");
                });
                $("#ddlCustomer").select2();
                $("#ddlSalesPerson").attr('multiple', 'multiple')
                $("#ddlSalesPerson option:not(:first)").remove();
                $.each(SalesPerson, function () {
                    $("#ddlSalesPerson").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("EmpName").text() + "</option>");
                });
                $("#ddlSalesPerson").select2({
                    placeholder: 'All Sales Person',
                    allowClear: true
                });
               

                $("#ddlShippingType").attr('multiple', 'multiple')
                $("#ddlShippingType option:not(:first)").remove();
                $.each(ShippingType, function () {
                    $("#ddlShippingType").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("ShippingType").text() + "</option>");
                });
                $("#ddlShippingType").select2({
                    placeholder: 'All Shipping Type',
                    allowClear: true
                });


                $("#ddlState").attr('multiple', 'multiple')
                $("#ddlState option:not(:first)").remove();
                $.each(StateType, function () {
                    $("#ddlState").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("StateName").text() + "</option>");
                });
                $("#ddlState").select2({
                    placeholder: 'All Shipping State',
                    allowClear: true
                });

            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
$("#ddlPageSize").change(function () {
    $("#divSlectedOrder").hide();
    $("#bCountSelectedOrder").html('');
    getOrderList(1);
});
function getOrderList(pageIndex) {
    $("#check-all").prop('checked', false);
    var ShippingType = '0', SalesPerson = '0',Status='3',State='0';
    if ($("#ddlSStatus option:selected").length == 0) {
        Status = '0';
    }
    $("#ddlShippingType option:selected").each(function (i) {
        if (i == 0) {
            ShippingType = $(this).val() + ',';
        } else {
            ShippingType += $(this).val() + ',';
        }
    });
    $("#ddlSalesPerson option:selected").each(function (i) {
        if (i == 0) {
            SalesPerson = $(this).val() + ',';
        } else {
            SalesPerson += $(this).val() + ',';
        }
    });
    $("#ddlSStatus option:selected").each(function (i) {
        if (i == 0) {
            Status = $(this).val() + ',';
        } else {
            Status += $(this).val() + ',';
        }
    });
    $("#ddlState option:selected").each(function (i) {
        if (i == 0) {
            State = $(this).val() + ',';
        } else {
            State += $(this).val() + ',';
        }
    });
    var data = {
        OrderNo: $("#txtSOrderNo").val().trim(),
        CustomerAutoid: $("#ddlCustomer").val(),
        SalesPerson: SalesPerson.toString(),
        FromDate: $("#txtSFromDate").val(),
        ToDate: $("#txtSToDate").val(),
        DeliveryFromDate: $("#txtDeliveryFromDate").val(),
        DeliveryToDate: $("#txtDeliveryToDate").val(),
        Status: Status.toString(),
        OrderType: $("#ddlOrderType").val(),
        pageIndex: pageIndex,
        PageSize: $("#ddlPageSize").val(),
        ShippingType: ShippingType.toString(),
        Driver: $("#ddlDriver").val(),
        OrderBy: $("#ddlOrderBy").val(),
        State: State.toString(),
    };
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/getOrderList",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                var xmldoc = $.parseXML(response.d);
                var orderList = $(xmldoc).find("Table1");
                if (orderList.length > 0) {
                    $("#EmptyTable").hide();
                    $("#tblOrderList tbody tr").remove();
                    var row = $("#tblOrderList thead tr").clone(true);
                    $.each(orderList, function () {
                        if ($(this).find("ShipId").text() == '2' || $(this).find("ShipId").text() == '7' || $(this).find("ShipId").text() == '4') {
                            $(row).css('background-color', '#ffe6e6');
                        }
                        else if ($(this).find("ShipId").text() == '8') {
                            $(row).css('background-color', '#eb99ff');
                        }
                        else if ($(this).find("ShipId").text() == '9') {
                            $(row).css('background-color', '#e6fff2');
                        }
                        else if ($(this).find("ShipId").text() == '10') {
                            $(row).css('background-color', '#ccff99');
                        }
                        else if (Number($(this).find("ShipId").text()) == 11 || Number($(this).find("ShipId").text()) == 12 || Number($(this).find("ShipId").text()) == 13  || Number($(this).find("ShipId").text()) == 15 || Number($(this).find("ShipId").text()) == 16) {
                            $(row).css('background-color', '#ff5200ab');
                        }
                        else {
                            $(row).css('background-color', '#fff');
                        }                      
                        //if ($(this).find("SA_Lat").text() == 'N/A' || $(this).find("SA_Long").text() == 'N/A' || $(this).find("SA_Lat").text() == '' || $(this).find("SA_Long").text() == '') {
                        //    checkLatLong = Number(checkLatLong) + 1;
                        //    $(row).css('background-color', '#660033');
                        //    $(row).css('color', '#fff');
                        //}
                        //else {
                            //$(row).css('background-color', '#fff');
                        //    $(row).css('color', '#131414');
                        //}
                        $(".Shipping", row).text($(this).find("ShippingType").text());
                        $(".orderNo", row).html("<span OrderAutoId='" + $(this).find("AutoId").text() + "'>" + $(this).find("OrderNo").text() + "</span>");
                        $(".orderDt", row).text($(this).find("OrderDate").text());
                        $(".cust", row).html('<span OrdStatus=' + $(this).find("StatusCode").text()+' customerAutoId=' + $(this).find("CustomerAutoId").text() + '>' + $(this).find("CustomerName").text() + '</span>');
                        $(".value", row).text($(this).find("GrandTotal").text());
                        $(".product", row).text($(this).find("NoOfItems").text());
                        $(".payableAmt", row).text(parseFloat($(this).find("PayableAmount").text()).toFixed(2));
                        $(".SalesPerson", row).text($(this).find("SalesPerson").text());
                        $(".CreditMemo", row).text($(this).find("CreditMemo").text());
                        $(".OrderAutoId", row).text($(this).find("AutoId").text());
                        $(".Stoppage", row).text($(this).find("StopNo").text());
                        $(".DeliveryDate", row).text($(this).find("DeliveryDate").text());
                        var logView = '';
                        if (Number($(this).find("StatusCode").text()) == 1) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_New'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 2) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Processed'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 3) {
                            logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Assign to driver"></a>';
                            $(".status", row).html("<span class='badge badge-pill Status_Packed' style='background-color:#66ff99'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 4) {
                            logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Change driver"></a>';
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Ready_to_Ship' style='background-color:#AB47BC'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 5) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Shipped'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 6) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Delivered'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 7) {
                            logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Asign to driver"></a>';
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Undelivered'>" + $(this).find("Status").text() + "</span>");
                        } else if (Number($(this).find("StatusCode").text()) == 8) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_cancelled' >" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 9) {
                            // logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Asign to driver"></a>';
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Add_On' style='background-color:green'>" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 10) {
                            logView = '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="BindDriver(this)" class="la la-truck" title="Asign to driver"></a>';
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Add_On_Packed' style='background-color:#61b960'>" + $(this).find("Status").text() + "</span>");
                        }
                        else if (Number($(this).find("StatusCode").text()) == 11) {
                            $(".status", row).html("<span class='badge mr-2 badge badge-pill Status_Close'>" + $(this).find("Status").text() + "</span>");
                        }
                        if (Number($(this).find("StatusCode").text()) == 8) {
                            $(".action", row).html("<input type='checkbox' name='table_records' id='chkProduct' onclick='chkProduct_OnClick(" + $(this).find("AutoId").text() + ");' >&nbsp;&nbsp;<b><a href='/Manager/Manager_viewOrder.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'><span class='la la-eye' title='View detail'></span></a>&nbsp;<a href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'><span class='la la-history' title='Log'></span></a>&nbsp;" + logView + "&nbsp;<a href='javascript:;' onclick='SendEmailPopup(" + $(this).find("AutoId").text() + ")'><span class='ft-mail' title='Send Mail'></span></a> &nbsp;<a href='javascript:;'><span class='ft-edit' title='Edit'></span></a>&nbsp;<a href='javascript:;' onclick='viewAllRemarks(" + $(this).find("AutoId").text() + ")'><span class='la la-eyedropper' title='View Remark'></span></a></b>");
                        } else {
                            $(".action", row).html("<input type='checkbox' name='table_records' id='chkProduct' onclick='chkProduct_OnClick(" + $(this).find("AutoId").text() + ");' >&nbsp;&nbsp;<b><a href='/Manager/Manager_viewOrder.aspx?OrderAutoId=" + $(this).find("AutoId").text() + "'><span class='la la-eye' title='View detail'></span></a>&nbsp;<a href='javascript:;' onclick='viewOrderLog(" + $(this).find("AutoId").text() + ")'><span class='la la-history' title='Log'></span></a>&nbsp;<a href='javascript:;' onclick='PrintOrderPop(" + $(this).find("AutoId").text() + "," + Number($(this).find("StatusCode").text()) + ")'><span class='icon-printer' title='Print'></span></a>" + logView + " &nbsp;<a href='javascript:;' onclick='SendEmailPopup(" + $(this).find("AutoId").text() + ")'><span class='ft-mail' title='Send Mail'></span></a> &nbsp;<a href='javascript:;' onclick='showpopupUpdateShipAdd(" + $(this).find("AutoId").text() + ")'><span class='ft-edit' title='Edit'></span></a>&nbsp;<a title='Add driver remark' href='javascript:;' onclick='OpenPopupRemark(" + $(this).find("AutoId").text() + ")'><span class='ft-plus'></span></a>&nbsp;<a href='javascript:;' onclick='viewAllRemarks(" + $(this).find("AutoId").text() + ")'><span class='la la-eyedropper' title='View Remark'></span></a></b>");
                        }
                       
                        $("#tblOrderList tbody").append(row);
                        row = $("#tblOrderList tbody tr:last").clone(true);
                    });
                } else {
                    $("#EmptyTable").show();
                    $("#tblOrderList tbody tr").remove();
                }

                var pager = $(xmldoc).find("Table");
                $(".Pager").ASPSnippets_Pager({
                    ActiveCssClass: "current",
                    PagerCssClass: "pager",
                    PageIndex: parseInt(pager.find("PageIndex").text()),
                    PageSize: parseInt(pager.find("PageSize").text()),
                    RecordCount: parseInt(pager.find("RecordCount").text())
                });
            } else {
                location.href = "/";
            }

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function showpopupUpdateShipAdd(OrderAutoId) {
    $("#hdnordAutoId").val(OrderAutoId);
    $("#ShipAddressPopup").modal('show');
    $("#txtZipCode").val('');
    $("#txtCity").val('');
    $("#txtState").val('');
    $("#txtLats").val('');
    $("#txtLongitude").val('');
    $("#lblLat").html('');
    $("#lblLong").html('');

    BindshippingAddress();
}
function UpdateShipAdd() {
    if ($('#txtshippingaddress').val() == "" || $("#txtLats").val() == "" || $("#txtLongitude").val() == "") {
        toastr.error('Invalid shipping address.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    }
    var data = {
        OrderAutoId: $("#hdnordAutoId").val(),
        ShippingAddress: $('#txtaddress').val(),
        ShippingAddress2: $('#txtshippingaddress2').val(),
        lat: $('#txtLats').val(),
        long: $('#txtLongitude').val(),
        state: $('#txtState').val(),
        city: $('#txtCity').val(),
        zipcode: $('#txtZipCode').val(),
        address: $('#txtaddress').val()

    };
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/UpdateShippingAddress",
        data: "{'dataValues':'" + JSON.stringify(data) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'true') {
                swal('', 'Shipping address updated successfully.', 'success');
                $("#ShipAddressPopup").modal('hide');
            }
            else {
                swal('', 'Opps something went wrong.', 'error');
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

function BindshippingAddress() {
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/BindshippingAddress",
        data: "{'OrderAutoId':'" + $("#hdnordAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var shipdetails = $(xmldoc).find("Table");

            if (($(shipdetails).find('OrdShippingAddress').text()) == null || ($(shipdetails).find('OrdShippingAddress').text()) == '') {
                $("#txtshippingaddress").val($(shipdetails).find('shipAdd').text());
            } else {
                $("#txtshippingaddress").val($(shipdetails).find('SA_Address').text());
            }

            $("#lblOrderNumber").text($(shipdetails).find('OrderNo').text());
            $("#lblCustomername").text($(shipdetails).find('CustomerName').text());
            $("#txtLats").val($(shipdetails).find('SA_Lat').text());
            $("#txtLongitude").val($(shipdetails).find('SA_Long').text());
            $("#txtState").val($(shipdetails).find('SA_State').text());
            $("#txtCity").val($(shipdetails).find('SA_City').text());
            $("#txtZipCode").val($(shipdetails).find('SA_Zipcode').text());
            $("#txtaddress").val($(shipdetails).find('SA_Address').text());
            $("#txtshippingaddress2").val($(shipdetails).find('SA_Address2').text());
            

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function SendEmailPopup(OrderAutoId) {
    window.open("/EmailSender/SendEmail.aspx?OrderAutoId=" + OrderAutoId + "", "popUpWindow", "height=700,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");

}
function SendMail(OrderId) {
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/ViewOrderDetails",
        data: "{'OrderAutoId':" + OrderId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            var xmldoc = $.parseXML(response.d);
            var orderx = $(xmldoc).find("Table");
            var PrintLbl = $(xmldoc).find("Table1");
            $("#lblOrderSNo").text(orderx.find("OrderNo").text());
            $("#lblCustomerName").text(orderx.find("CustomerName").text());
            $("#txtTo").val(orderx.find("Email").text());
            $("#hdnPrintLabel").val(PrintLbl.find("PrintLabel").text());
            var Subject = "Order No : " + orderx.find("OrderNo").text();
            $("#txtSubject").val(Subject);
            var emailBody = '<p>Dear <b>' + orderx.find("CustomerName").text() + ',</b></p>';
            emailBody += '<p>Your <b>ORDER # ' + orderx.find("OrderNo").text() + '</b> for <b>$' + parseFloat(orderx.find("PayableAmount").text()).toFixed(2) + '</b> attached. Please remit payment at your earliest convenience.</p>';
            emailBody += '<p>Thank you for your business - We appreciate it very much.</p>';
            emailBody += '<p>Sincerely ,</p>';
            emailBody += '<p><b>PRICESMART</b></p>';
            emailBody += '<p><b>1(888)51MYPSM</b> ';
            emailBody += '<b><br>1(888)516-9776</b></p>';
            $("#txtEmailBody").summernote('code', emailBody);

        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalSendMail").modal('show');
    OrdAutoId = OrderId;
    $("#txtTo").val('');
    $("#txtSubject").val('');
    $("#txtEmailBody").val('');
}

$("#btnSendMail").click(function () {

    if (dynamicALL('reqMail')) {
        if ($("#txtSubject").val() == "") {
            swal({
                title: "Are you sure?",
                text: "You want to send mail without subject.",
                icon: "warning",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "No, cancel.",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Yes, Send it.",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    Sending()
                }
            })
        }
        else if ($("#txtEmailBody").val() == "") {
            swal({
                title: "Are you sure?",
                text: "You want to send mail without message.",
                icon: "warning",
                showCancelButton: true,
                buttons: {
                    cancel: {
                        text: "No, cancel.",
                        value: null,
                        visible: true,
                        className: "btn-warning",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Yes, Send it.",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
            }).then(function (isConfirm) {
                if (isConfirm) {
                    Sending()
                }
            })
        }
        else {
            Sending();
        }

    } //--------------------------
    else {
        toastr.error('All * fields are mandatory .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
})
function Sending() {
    var data = {
        To: $('#txtTo').val(),
        Subject: $('#txtSubject').val(),
        EmailBody: $('#txtEmailBody').val(),
        OrderId: OrdAutoId,
        PrintLabel: $('#hdnPrintLabel').val(),
    };
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/getMailBoddy",
        data: JSON.stringify({ dataValue: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.d == 'true') {
                swal('', 'Email sent successfully.', 'success');
                $("#modalSendMail").modal('hide');
                // OrdAutoId = OrderId;
            } else if (response.d == 'receiverMail') {
                swal('', 'Email address could not be found.', 'error');
            } else if (response.d == 'EmailNotSend') {
                swal('', 'Email address could not be found or was misspelled.', 'error');
            }
            else if (response.d == 'false') {
                swal('', 'Opps, Something went wrong.', 'error');
            } else if (response.d == 'Session Expired') {
                location.href = '/';
            }
            else {
                swal('', 'Opps, Something went wrong.', 'error');
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function PrintOrderPop(OrderAutoId, status) {
    $("#chkdueamount").prop('checked', true);
    $('#txtHOrderAutoId').val(OrderAutoId)
    $('#PopPrintTemplate').modal('show');
    if (Number(status < 3)) {
        $('#PackingSlip').closest('.row').hide();
        $('#WithoutCategorybreakdown').closest('.row').hide();
    } else {
        $('#PackingSlip').closest('.row').show();
        $('#WithoutCategorybreakdown').closest('.row').show();
    }

    if ($('#HDDomain').val() == 'psmpa') {
        $("#divPSMPADefault").show();
        $("#PSMPADefault").prop('checked', true);
    }
    else if ($('#HDDomain').val() == 'psmwpa') {
        $("#divPSMWPADefault").show();
        
    }
    else if ($('#HDDomain').val() == 'psmnpa') {
        $("#divPSMNPADefault").show();
        $("#divPSMPADefault").hide();
        $("#rPSMNPADefault").prop('checked', true);
    }

    else {
       
        $("#PSMPADefault").closest('.row').hide();
    }
}

function PrintOrder() {

    $('#PopPrintTemplate').modal('hide');
    if ($("#chkdueamount").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF1.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#chkdueamount_Tax").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF1_Tax.html?OrderAutoId=" + $("#txtHOrderAutoId").val() + "", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#chkdefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCC.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template1").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Template2").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF3.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PSMPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#PSMWPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF6.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    } else if ($("#PSMWPANDefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCFPN.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#PackingSlip").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF5.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#WithoutCategorybreakdown").prop('checked') == true) {
        window.open("/Manager/WithoutCategorybreakdown.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#rPSMNPADefault").prop('checked') == true) {
        window.open("/Manager/OrderPrintCCF4.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ1").prop('checked') == true) {
        window.open("/Manager/PrintOrderItem.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
    else if ($("#Templ2").prop('checked') == true) {
        window.open("/Manager/PrintOrderItemTemplate2.html?OrderAutoId=" + $("#txtHOrderAutoId").val(), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
}
function print_NewOrder(e) {
    window.open("/Packer/OrderPrint.html?OrderAutoId=" + $(e).closest("tr").find(".orderNo span").attr("OrderAutoId"), "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    setTimeout(function () {
        getOrderList(1);
    }, 2000);
}

$("#btnSearch").click(function () {
    $("#divSlectedOrder").hide();
    $("#bCountSelectedOrder").html('');//bCountSelectedCustomers
    $("#bCountSelectedCustomers").html('');
    var Driver = $("#ddlDriver").val();
    var OrderStatus = $("#ddlSStatus").val();
    localStorage.setItem('Driver', Driver);
    localStorage.setItem('Orderstatus', OrderStatus);
    getOrderList(1);
});

function viewOrderLog(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "/Sales/WebAPI/WOrderLog.asmx/viewOrderLog",
        data: "{'OrderAutoId':" + OrderAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var orderlog = $(xmldoc).find("Table");
            var order = $(xmldoc).find("Table1");

            $("#lblOrderNo").text(order.find("OrderNo").text());
            $("#lblOrderDate").text(order.find("OrderDate").text());

            $("#tblOrderLog tbody tr").remove();
            var row = $("#tblOrderLog thead tr").clone(true);
            if (orderlog.length > 0) {
                $("#EmptyTable").hide();
                $.each(orderlog, function (index) {
                    $(".SrNo", row).text(index + 1);
                    $(".ActionBy", row).text($(this).find("EmpName").text());
                    $(".Date", row).text($(this).find("ActionDate").text());
                    $(".Action", row).text($(this).find("Action").text());
                    $(".Remark", row).text($(this).find("Remarks").text());

                    $("#tblOrderLog tbody").append(row);
                    row = $("#tblOrderLog tbody tr:last").clone(true);
                });
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalOrderLog").modal('show');
}


function BindDriver(e) {

    var tr = $(e).closest('tr');
    $('#txtHOrderAutoId').val($(tr).find('.orderNo span').attr('OrderAutoId'));
    if (parseFloat($(tr).find('.payableAmt').text()) < 0) {
        toastr.error('Please manage discount before assign driver.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else {
        $.ajax({
            type: "Post",
            url: "/Manager/WebAPI/WManager_OrderList.asmx/getDriverList",
            data: "{'OrderAutoId':'" + $("#txtHOrderAutoId").val() + "','LoginEmpType':'5'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var Drivers = $(xmldoc).find("Table");
                    var selectDrv = $(xmldoc).find("Table1");
                    $("#modalMisc").modal("toggle").find(".modal-dialog").removeClass("modal-sm").end()
                        .find(".modal-title").text("Drivers").end()
                        .find("#tblAssignDrv").show().end()
                        .find("#btnAsgn").show().end()
                        .find("#btnPrint").hide()
                        .find('#insideText').hide();
                    $('#DriverText').show();
                    $('#insideText').text('');
                    $('#btnOk').hide();
                    $("#tblDriver tbody tr").remove();
                    var row = $("#tblDriver thead tr").clone(true);
                    var picker = $('#txtAssignDate').pickadate('picker')
                    picker.set('min', -7);
                    picker.set('select', new Date($(selectDrv).find("AssignDate").text()));
                    $("#txtAssignDate").attr("disabled", false);
                    $.each(Drivers, function (index) {
                        $(".DrvName", row).html("<span drvautoid =" + $(this).find("AutoId").text() + ">" + $(this).find("Name").text() + "</span>");
                        $(".AsgnOrders", row).html($(this).find("AssignOrders").text());
                        if ($(this).find("AutoId").text() == $(selectDrv).find("Driver").text()) {
                            $(".Select", row).html("<input type='radio' name='driver' class='radio' onchange = 'changeDriver(this)' checked='checked'>");
                            $("#btnAsgn").attr("disabled", true);
                        } else {
                            $(".Select", row).html("<input type='radio' name='driver' class='radio' onchange = 'changeDriver(this)' >");
                        }
                        if (index == 0) {
                            $(row).css('background-color', '#ccff99');
                        }
                        else {
                            $(row).css('background-color', 'white');
                        }
                        $("#tblDriver tbody").append(row);
                        row = $("#tblDriver tbody tr:last").clone(true);
                    });
                } else {
                    location.href = '/';
                }

            },
            error: function (result) {
                console.log(JSON.parse(result.responseText).d);
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}
function assignDriver() {

    if (checksdrivRequiredField()) {
        var drvAutoId, flag = true;
        $("#tblDriver tbody tr").each(function () {
            if ($(this).find("input[name='driver']").is(":checked")) {
                drvAutoId = $(this).find(".DrvName span").attr("drvautoid");
                flag = false;
            }
        });

        var data = {
            DriverAutoId: drvAutoId,
            OrderAutoId: $("#txtHOrderAutoId").val(),
            AssignDate: $("#txtAssignDate").val()
        };

        if (!flag) {
            $.ajax({
                type: "Post",
                url: "/Manager/WebAPI/WManager_OrderList.asmx/AssignDriver",
                data: "{'dataValues':'" + JSON.stringify(data) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI({
                        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                        overlayCSS: {
                            backgroundColor: '#FFF',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                async: false,
                success: function (response) {
                    if (response.d != "Session Expired") {
                        if (response.d == 'true') {
                            getOrderList(1);
                            swal("", "Driver assigned successfully.", "success"
                            ).then(function () {
                                $("#modalMisc").modal("toggle");
                            });
                        }
                        else if (response.d != "InvalidDriver")
                        {
                            swal("", "Unauthorized Access.", "error");
                        }
                        else {
                            swal("", response.d, "error");
                        }


                    } else {
                        location.href = '/';
                    }

                },
                error: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                },
                failure: function (result) {
                    console.log(JSON.parse(result.responseText).d);
                }
            });
        } else {
            toastr.error('No driver selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
    }
    else {
        toastr.error('Assign date required.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}

function changeDriver(e) {
    var btnAsgn = $(e).closest(".modal-dialog").find("#btnAsgn");
    $(btnAsgn).attr("disabled", false);

}

function checksdrivRequiredField() {

    var boolcheck = true;
    $('.reqdriv').each(function () {
        if ($(this).val() == '') {
            boolcheck = false;
            $(this).addClass('border-warning');
        } else {
            $(this).removeClass('border-warning');
        }
    });

    return boolcheck;
}
//------------------------------------------------------------------Bind Driver--------------------------------------------
function BindDriverDDL() {

    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/BindDriver",
        data: "{}",
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },

        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            var StateDetails = $(xmldoc).find("Table");

            $("#ddlDriver option:not(:first)").remove();
            $.each(StateDetails, function () {
                $("#ddlDriver").append("<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Driver").text() + "</option>");
            });
            $("#ddlDriver").select2();
        },
        error: function (response) {
            console.log(JSON.parse(response.responseText).d);
        }
    });
}
function chkProduct_OnClick(e) {
    var countCustomer = [], countOrder = 0, custOrd = 0; check = 0;
    $("#tblOrderList tbody tr").each(function (i, tbl) {
        check = 0;
        if ($(this).find('.action #chkProduct').prop('checked') == true) {
            if ($(this).find('.cust span').attr('ordstatus') == "3" || $(this).find('.cust span').attr('ordstatus') == "10") {
                  countOrder = Number(countOrder) + 1;
                $.each(countCustomer, function (i, cc) {
                    if (cc.CustomerId == $(tbl).find('.cust span').attr('customerautoid')) {
                        check = 1;
                    }
                });
                if (check == 0) {
                    countCustomer.push({
                        CustomerId: $(this).find('.cust span').attr('customerautoid'),
                        CustomerOrder: 1
                    })
                    custOrd = Number(custOrd) + 1;
                }
            }
        }
    })
    if (countOrder > 0) {
        $("#divSlectedOrder").show();
        $("#bCountSelectedOrder").html(countOrder);
        $("#bCountSelectedCustomers").html(custOrd);
    }
    else {
        $("#divSlectedOrder").hide();
        $("#bCountSelectedOrder").html('');
        $("#bCountSelectedCustomers").html('');
    }
}
$("#check-all").change(function () {
    if ($(this).prop("checked")) {
        $("#tblOrderList input:checkbox:not(:disabled)").prop('checked', $(this).prop("checked"));
    }
    else {
        $("#tblOrderList input:checkbox").prop('checked', $(this).prop("checked"));
    }
    var countCustomer = [], countOrder = 0, custOrd = 0; check = 0;
    $("#tblOrderList tbody tr").each(function (i, tbl) {
        check = 0;
        if ($(this).find('.action #chkProduct').prop('checked') == true) {
            if ($(this).find('.cust span').attr('ordstatus') == "3" || $(this).find('.cust span').attr('ordstatus') == "10") {
                countOrder = Number(countOrder) + 1;
                $.each(countCustomer, function (i, cc) {
                    if (cc.CustomerId == $(tbl).find('.cust span').attr('customerautoid')) {
                        check = 1;
                    }
                });
                if (check == 0) {
                    countCustomer.push({
                        CustomerId: $(this).find('.cust span').attr('customerautoid'),
                        CustomerOrder: 1
                    })
                    custOrd = Number(custOrd) + 1;
                }
            }
        }
    })
    if (countOrder > 0) {
        $("#divSlectedOrder").show();
        $("#bCountSelectedOrder").html(countOrder);
        $("#bCountSelectedCustomers").html(custOrd);
    }
    else {
        $("#divSlectedOrder").hide();
        $("#bCountSelectedOrder").html('');
        $("#bCountSelectedCustomers").html('');
    }
})

$("#btnBulkPrint1").click(function () {
    if (localStorage.getItem('Driver') == '0') {
        toastr.error('No driver selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else {
        if (localStorage.getItem('Orderstatus') != '4') {
            toastr.error('Select only ready to ship order status ', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
        else {
            var flag = false; var OrderAutoIds = 0;
            //data = new Array();

            //alert(OrderAutoId.join(", "));
            var OrderAutoId = [];
            $('input:checkbox:checked', '#tblOrderList tbody').each(function (index, item) {
                var row = $(item).closest('tr');
                OrderAutoIds = row.find('.OrderAutoId').text(),
                    OrderAutoId.push(OrderAutoIds);
                flag = true;
            });
            var OAutoId = OrderAutoId.join(",");
            localStorage.setItem('BulkOrderAutoId', OAutoId);
            //if (flag) {

            //    window.open("/Manager/BulkOrderPrint.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            //} else {
            //    toastr.error('Please check atleast one Order', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            //}
        }
    }
})
function PrintPSMDefault() {
    var flag = false; var OrderAutoIds = 0;
    var OrderAutoId = [];
    $('input:checkbox:checked', '#tblOrderList tbody').each(function (index, item) {
        var row = $(item).closest('tr');
        OrderAutoIds = row.find('.OrderAutoId').text(),
            OrderAutoId.push(OrderAutoIds);
        flag = true;
    });
    var OAutoId = OrderAutoId.join(",");
    localStorage.setItem('BulkOrderAutoId', OAutoId);
    if (flag) {
        if (localStorage.getItem('Driver') == '0') {
            toastr.error('No driver selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
        else {
            if (localStorage.getItem('Orderstatus') != '4') {
                toastr.error('Select only ready to ship order status ', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            }
            else {
                window.open("/Manager/BulkOrderPrint.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
        }

    } else {
        toastr.error('Please check atleast one order', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
//-----------------------------------------------------------------Bind All Order Type-------------------------------------
function BindAllOrderType() {
    $.ajax({
        type: "POST",
        url: "/Admin/DropDownMaster.aspx/BindAllOrderType",
        data: "{}",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        async: false,
        success: function (response) {
           
            if (response.d != "Session Expired") {
                var data = $.parseJSON(response.d);               
                $("#ddlOrderType option:not(:first)").remove();
                $.each(data, function (index, item) {
           
                    $("#ddlOrderType").append("<option value='" + data[index].AutoId + "'>" + data[index].OrderType + "</option>");
                });
                $("#ddlOrderType").select2();
            } else {
                location.href = '/';
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}

//----------------Optimo Route code start---------------------------- 
function tConvert(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
        time = time.slice(1);  // Remove full string match value
        time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
}
function checkSecurity() {
    debugger;
    $("#divSlectedOrder").hide();
    $("#bCountSelectedOrder").html('');
    var confirmPopup = 0, chkReq = 0;
    //InvalidShipOrd = 0;
    //checkLatLong = '';
    $("#tblOrderList tbody tr").each(function (i) {
        if ($(this).find('.action #chkProduct').prop('checked') == true || $(this).find('.action #check-all').prop('checked') == true) {
            if ($(this).find('.status span').html() != "Packed" && $(this).find('.status span').html() != "Add-On-Packed") {
                confirmPopup = Number(confirmPopup) + 1;
            }
            chkReq = Number(chkReq) + 1;
            //if ($(this).find(".cust span").attr('lt') == 'N/A' || $(this).find(".cust span").attr('lg') == 'N/A' || $(this).find(".cust span").attr('lg') == '' || $(this).find(".cust span").attr('lt') == '') {
            //    InvalidShipOrd += $(this).find(".orderNo span").html() + ",";
                //checkLatLong = Number(checkLatLong) + 1;
            //}
        }
    })
    //if (checkLatLong > 0) {
    //    swal("", InvalidShipOrd + " has invalid shipping address.", "warning");
    //    checkLatLong = 0;
    //    return;
    //}
    if (chkReq == 0) {
        toastr.error('Please select atleast one packed order.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    }
    if ($("#OptimoKey").val() == "") {
        if (confirmPopup == 0) {
            $("#mdSecurityCheck").modal('show');
        }
        else {
            toastr.error('Please select only packed and add on packed order.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
    }
    else {
        if (confirmPopup == 0) {
            actionAssigRoute();
        }
        else {
            toastr.error('Please select packed order.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
    }
}
function clickonSecurity() {
    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/OptimoRoute.asmx/clickonSecurity",
        data: "{'CheckSecurity':'" + $("#txtOptimoKey").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //async:true,
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var KeyTable = $(xmldoc).find("Table");
                    $("#mdSecurityCheck").modal('hide');
                    // $("#OptimoKey").val($(KeyTable).find('OptimoKey').text());
                    $("#txtOptimoKey").val('');
                    actionAssigRoute();
                } else {
                    toastr.error('Access denied .', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function actionAssigRoute() {
    
    $.ajax({
        type: "Post",
        url: "/Manager/WebAPI/OptimoRoute.asmx/getDateDifference",
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            //$.blockUI({
            //    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            //    overlayCSS: {
            //        backgroundColor: '#FFF',
            //        opacity: 0.8,
            //        cursor: 'wait'
            //    },
            //    css: {
            //        border: 0,
            //        padding: 0,
            //        backgroundColor: 'transparent'
            //    }
            //});
        },
        complete: function () {
            //$.unblockUI();
        },
        success: function (response) {
            
            if (response.d != "Session Expired") {
                if (response.d != "false") {
                    var xmldoc = $.parseXML(response.d);
                    var difference = $(xmldoc).find("Table");
                    dateDifference = $(difference).find('DateDifference').text();
                    var d = new Date();
                    planningDate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                    $("#hfPlanningDate").val(planningDate);
                    $("#hfDriverLimit").val($(difference).find("OptimoDriverLimit").text());
                    $("#txtPlanningDate").val((d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear());
                    var date = new Date($("#hfPlanningDate").val()),
                        days = parseInt(dateDifference, 10);
                    if (!isNaN(date.getTime())) {
                        date.setDate(date.getDate() + days);
                        var dt = new Date(date);
                        planningDate = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
                        $("#hfPlanningDate").val(planningDate);
                    } 
                    disableAllDriver();
                    PopupRouteOrderList();
                    $("#number").val('0');

                } else {
                    toastr.error('Oops, Something went wrong.Please try later.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            } else {
                location.href = '/';
            }
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
  
}
function PopupRouteOrderList() {
   
    Orders = []; var check = 0;
    $("#tblOrderList tbody tr").each(function (i) {
        if ($(this).find('.action #chkProduct').prop('checked') == true) {
            if ($(this).find('.status span').html() == "Packed" || $(this).find('.status span').html() == "Add-On-Packed") {
                Orders.push({
                    'OrderAutoId': $(this).find('.orderNo span').attr('orderautoid')
                });
            }
            else {
                check = 1;
            }
        }
    })
    if (check == 1) {
        toastr.error('Please select only packed and add on packed order.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    }
    else {
        $.ajax({
            type: "Post",
            url: "/Manager/WebAPI/OptimoRoute.asmx/getOrderAndDriver",
            data: "{'TableValues':'" + JSON.stringify(Orders) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {

                if (response.d != "Session Expired") {
                    var xmldoc = $.parseXML(response.d);
                    var RouteTable = $(xmldoc).find("Table");
                    var DriverTable = $(xmldoc).find("Table1");
                    var TimeWindow = $(xmldoc).find("Table2");
                    var CarList = $(xmldoc).find("Table3");
                    var Company_Location = $(xmldoc).find("Table4");

                    localStorage.setItem('CompanyStart_Lat', $(Company_Location).find("optimoStart_Lat").text());
                    localStorage.setItem('CompanyStart_Long', $(Company_Location).find("optimoStart_Long").text());

                    ddlCar = '<select class="input-sm border-primary" style="border-radius:3px;height:28px">';
                    ddlCar += "<option value='0'>--Select--</option>";
                    $.each(CarList, function () {
                        ddlCar += "<option value='" + $(this).find("CarAutoId").text() + "'>" + $(this).find("CarName").text() + "</option>"
                    })
                    ddlCar += '</select>';

                    TimeFrom = '<select class="input-sm border-primary" style="border-radius:3px;height:28px">';
                    TimeFrom += "<option value='0'>--Select--</option>";
                    $.each(TimeWindow, function () {
                        TimeFrom += "<option value='" + $(this).find("TimeFrom").text() + "'>" + $(this).find("FromTime").text() + "</option>"
                    })
                    TimeFrom += '</select>';

                    
                    localStorage.setItem('UnscheduldedOrder', JSON.stringify(response.d));
                    $("#tblRouteOrder tbody tr").remove();
                    var row = $("#tblRouteOrder thead tr:last-child").clone(true);
                    if (RouteTable.length > 0) {
                        $("#EmptyTable").hide();
                        $("#popupOptimoOrderDetails").modal('show');
                        $.each(RouteTable, function (index, rtdata) {
                            var check = false, referNo = "", OrdAutoId = "", ShipType = "",checkLat=0;
                            $("#tblRouteOrder tbody tr").each(function (j, item) {
                                if ($(item).find(".SrNo span").attr('CustomerAutoId') == $(rtdata).find("CustomerAutoId").text()) {
                                    if ($(item).find(".Orders").html() == '') {
                                        referNo = $(rtdata).find("OrderNo").text();
                                    }
                                    else {
                                        referNo = $(item).find(".Orders").html() + ',' + $(rtdata).find("OrderNo").text();
                                    }
                                    $(item).find(".Orders").html(referNo);

                                    if ($(item).find(".OrderAutoId").html() == '') {
                                        OrdAutoId = $(rtdata).find("OrderAutoId").text();
                                    }
                                    else {
                                        OrdAutoId = $(item).find(".OrderAutoId").html() + ',' + $(rtdata).find("OrderAutoId").text();
                                    }
                                    $(item).find(".OrderAutoId").html(OrdAutoId);

                                    if ($(item).find(".Ships").html() == '') {
                                        ShipType = $(rtdata).find("ShippingTypeAutoId").text();
                                    }
                                    else {
                                        ShipType = $(item).find(".Ships").html() + ',' + $(rtdata).find("ShippingTypeAutoId").text();
                                    }
                                    $(item).find(".Ships").html(ShipType);
                                    check = true;

                                }
                            });
                            if (!check) {
                                 if ($(this).find("SA_Lat").text() != 'N/A' && $(this).find("SA_Long").text() != 'N/A' && $(this).find("SA_Lat").text() != '' && $(this).find("SA_Long").text() != '') {
                                   $(".Action", row).html('<input type="checkbox" checked name="table_recod" onclick="countCheckOrder()" id="chkOrder">');
                                    $(".SrNo", row).html((index + 1) + '<span OrderNo=' + $(this).find("OrderNo").text() + ' CustomerAutoId=' + $(rtdata).find('CustomerAutoId').text() + '>');
                                    $(".OrderNo", row).html('<span OrderAutoId=' + $(this).find("OrderAutoId").text() + ' OderDate=' + $(this).find("OrderDate").text() + ' GrandAmount=' + $(this).find("GrandTotal").text() + '>' + $(this).find("OrderNo").text() + '</span>');
                                    $(".Customer", row).html($(this).find("CustomerName").text());
                                    $(".Cust", row).html("<span  ShippingTypeAutoId='" + $(this).find("ShippingTypeAutoId").text() + "' drvRemarks='" + $(this).find("DriverRemarks").text() + "'>" + $(this).find("CustomerName").text() + "<br><b style='font-size:11px'>" + $(this).find("shipAdd").text() + "</b></span>");
                                    $(".SalesPerson", row).html($(this).find("SalesPerson").text());
                                    $(".Lat", row).html($(this).find("SA_Lat").text());
                                    $(".Long", row).html($(this).find("SA_Long").text());
                                    $(".From", row).html(TimeFrom);
                                    $(".To", row).html(TimeFrom);
                                    row.find(".From > select").val($(this).find("OptimotwFrom").text()).change();
                                    row.find(".To > select").val($(this).find("OptimotwTo").text()).change();
                                    $(".Address", row).html($(this).find("shipAdd").text());
                                    $(".Orders", row).html('');
                                    $(".OrderAutoId", row).html('');
                                    $(".Ships", row).html('');
                                    $("#tblRouteOrder tbody").append(row);
                                    row = $("#tblRouteOrder tbody tr:last-child").clone(true);
                                }
                            }
                        });
                        $("#tblRouteOrder tbody tr").show();

                        $("#tblUnRouteOrder tbody tr").remove();
                        var row = $("#tblUnRouteOrder thead tr:last-child").clone(true);
                        $.each(RouteTable, function (index, rtdata) {
                            var check = false, referNo = "";
                            $("#tblUnRouteOrder tbody tr").each(function (j, item) {
                                if ($(item).find(".SrNo span").attr('CustomerAutoId') == $(rtdata).find("CustomerAutoId").text()) {
                                    if ($(item).find(".Orders").html() == '') {
                                        referNo = $(rtdata).find("OrderNo").text();
                                    }
                                    else {
                                        referNo = $(item).find(".Orders").html() + ',' + $(rtdata).find("OrderNo").text();
                                    }
                                    $(item).find(".Orders").html(referNo);
                                    check = true;
                                }
                            });
                            if (!check) {
                                if ($(this).find("SA_Lat").text() == 'N/A' || $(this).find("SA_Long").text() == 'N/A' || $(this).find("SA_Lat").text() == '' || $(this).find("SA_Long").text() == '') {
                                    $(".SrNo", row).html((index + 1) + '<span OrderNo=' + $(this).find("OrderNo").text() + ' CustomerAutoId=' + $(rtdata).find('CustomerAutoId').text() + '>');
                                    $(".OrderNo", row).html($(this).find("OrderNo").text());
                                    $(".Customer", row).html($(this).find("CustomerName").text());
                                    $(".Cust", row).html("<span drvRemarks='" + $(this).find("DriverRemarks").text() + "'>" + $(this).find("CustomerName").text() + "<br><b style='font-size:11px'>" + $(this).find("shipAdd").text() + "</b></span>");
                                    $(".SalesPerson", row).html($(this).find("SalesPerson").text());
                                    $(".Lat", row).html($(this).find("SA_Lat").text());
                                    $(".Long", row).html($(this).find("SA_Long").text());
                                    $(".From", row).html(TimeFrom);
                                    $(".To", row).html(TimeFrom);
                                    row.find(".From > select").val($(this).find("OptimotwFrom").text()).change();
                                    row.find(".To > select").val($(this).find("OptimotwTo").text()).change();
                                    $(".Address", row).html($(this).find("shipAdd").text());
                                    $(".Orders", row).html('');
                                    $("#tblUnRouteOrder tbody").append(row);
                                    row = $("#tblUnRouteOrder tbody tr:last-child").clone(true);
                                }
                            }
                        });
                        $("#tblUnRouteOrder tbody tr").show();

                    }
                    $("#tblOrderList tbody tr").each(function (i) {
                        if ($(this).find('.action input').prop('checked') == true) {
                            $(this).find('.action input').prop('checked', false);
                        }
                    });
                    $("#check-all").prop('checked', false);

                    $('#totalCustomerCount').html($('#tblRouteOrder tbody tr').length);
                    $('#totalCheckedOrder').html($('#tblRouteOrder tbody tr').length);
                    $('#totalAddedDriver').html('0');
                    var NoOfOrder = 0, checkNoOrd = 0, NoOfUnrouteOrder = 0, chkNoRoute = 0;
                    $("#tblRouteOrder tbody tr").each(function () {
                        if ($(this).find('.Orders').text() != '') {
                            checkNoOrd = $(this).find('.Orders').text().split(',');
                            $.each(checkNoOrd, function () {
                                NoOfOrder = Number(NoOfOrder) + 1;
                            })
                        }
                    })
                    $('#totalOrderCount').html(Number(NoOfOrder) + Number($('#tblRouteOrder tbody tr').length));

                    $("#tblUnRouteOrder tbody tr").each(function () {
                        if ($(this).find('.Orders').text() != '') {
                            chkNoRoute = $(this).find('.Orders').text().split(',');
                            $.each(chkNoRoute, function () {
                                NoOfUnrouteOrder = Number(NoOfUnrouteOrder) + 1;
                            })
                        }
                    })
                    $('#totalUnRouteOrderCount').html(Number(NoOfUnrouteOrder) + Number($('#tblUnRouteOrder tbody tr').length));

                    ddlDrv = '<select onchange="fillDriverTime(this)" class="form-control input-sm border-primary">';
                    ddlDrv += "<option value='0'>--Select Driver--</option>";
                    $.each(DriverTable, function () {
                        ddlDrv += "<option StartLocation='" + $(this).find("StartLocation").text() + "' EndLocation='" + $(this).find("EndLocation").text() + "'  StartTime='" + $(this).find("StartTime").text() + "' EndTime='" + $(this).find("EndTime").text() + "' CarNo='" + $(this).find("CarNo").text() + "' value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Name").text() + "</option>"
                    })
                    ddlDrv += '</select>';

                    var tblDrvList = ""; $("#divDriverList").html('');
                    tblDrvList += ' <table class="table table-striped table-bordered" id="tblDriverList">'
                    tblDrvList += '<thead class="bg-blue white">'
                    tblDrvList += '<tr>'
                    tblDrvList += '<td class="DriverNo text-center">Driver No.</td>'
                    tblDrvList += '<td class="SelectedDriver">Selected Driver</td>'
                    tblDrvList += '<td class="StartTime text-center">Start Time</td>'
                    tblDrvList += '<td class="EndTime text-center">End Time</td>'
                    tblDrvList += '<td class="Action text-center">Action</td>'
                    tblDrvList += '</tr>'
                    tblDrvList += '</thead >'
                    tblDrvList += '<tbody></tbody><tfoot><tr><td class="DriverNo text-center"></td><td class="SelectedDriver">' + ddlDrv + '</td><td  class="StartTime text-center"></td><td  class="EndTime text-center"></td><td class="Action text-center"><button type="button" class="btn btn-success buttonAnimation round box-shadow-1  btn-sm" onclick="addDriver(this)">Add</button></td></tr></tfoot>'
                    tblDrvList += '</table >'
                    $("#divDriverList").html(tblDrvList);
                } else {
                    location.href = '/';
                }
            },
            error: function (result) {
            },
            failure: function (result) {
                console.log(JSON.parse(result.responseText).d);
            }
        });
    }
}
function fillDriverTime(e) {

    var tr = $(e).closest('tr');
    if (tr.find('.SelectedDriver option:selected').val() != "0") {
        tr.find('.StartTime').html(TimeFrom);
        tr.find('.EndTime').html(TimeFrom);
        // tr.find('.Car').html(ddlCar);
        tr.find('.StartTime > select').val(tr.find('.SelectedDriver option:selected').attr('starttime'));
        tr.find('.EndTime > select').val(tr.find('.SelectedDriver option:selected').attr('endtime'));
    }
}
function deleteAll() {
    
    var check = 0, TimeCheck = 0;
    if ($("#chkBalanceByDriver").prop("checked") == true) {
        if ($("#ddlBalancingFactor").val() == '0.0') {
            check = 1;
        }
    }
    var trRow = $('tr');
    $("#tblRouteOrder tbody tr").each(function (i) {
        if ($(this).find('.Action #chkOrder').prop('checked') == true) {
            if ($(this).find('.From > select').val() != undefined) {
                From = $(this).find('.From > select').val();
                From = From.slice(0, 2);
                From1 = $(this).find('.From > select').val();
                From1 = From1.slice(3, 5);
                To = $(this).find('.To > select').val();
                To = To.slice(0, 2);
                To1 = $(this).find('.To > select').val();
                To1 = To1 = To1.slice(3, 5);
                if (Number(From) > Number(To)) {
                    trRow.removeClass('selected');
                    $(this).closest('tr').attr('style', 'background-color:#FF9149 ');
                    TimeCheck = 1;
                }
                else if (Number(From) == Number(To)) {
                    if (Number(From1) > Number(To1) || Number(From1) == Number(To1)) {
                        TimeCheck = 1;
                        trRow.removeClass('selected');
                        $(this).closest('tr').attr('style', 'background-color:#FF9149 ');
                    }
                    else {
                        trRow.removeClass('selected');
                        $(this).closest('tr').removeAttr('style');
                    }
                }
                else {
                    trRow.removeClass('selected');
                    $(this).closest('tr').removeAttr('style');
                }
            }
        }
    });
    if (check == 1) {
        toastr.error('Please select balancing factor.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else if (TimeCheck == 1) {
        toastr.error('Invalid time. Please select a valid time', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else {
        if (Number($('#tblDriverList tbody tr').length) > 0) {
            $.ajax({
                type: "Post",
                url: "https://api.optimoroute.com/v1/delete_all_orders" + "?key=" + Key,
                data: JSON.stringify({ date: $("#hfPlanningDate").val() }),
                header: { 'content-type': 'application/json' },
                beforeSend: function () {
                    //$.blockUI({
                    //    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    //    overlayCSS: {
                    //        backgroundColor: '#FFF',
                    //        opacity: 0.8,
                    //        cursor: 'wait'
                    //    },
                    //    css: {
                    //        border: 0,
                    //        padding: 0,
                    //        backgroundColor: 'transparent'
                    //    }
                    //});
                },
                complete: function () {
                    //$.unblockUI();
                },
                success: function (response) {
                    tblRouteOrder = [];
                    $("#tblRouteOrder tbody tr").each(function (i) {
                        if ($(this).find(".Lat").text() != '' && $(this).find(".Long").text() != '') {
                            if ($(this).find('.Action #chkOrder').prop('checked') == true) {
                                
                                tblRouteOrder.push({
                                    'OrderAutoId': $(this).find('.OrderNo span').attr('orderautoid'),
                                    'OrderNo': $(this).find('.OrderNo').text(),
                                    'Lat': $(this).find('.Lat').text(),
                                    'Long': $(this).find('.Long').text(),
                                    'Address': $(this).find('.Address').text(),
                                    'OrderAutoIds': $(this).find('.OrderAutoId').text(),
                                    'Orders': $(this).find('.Orders').text(),
                                    'CustomerName': $(this).find('.Customer').text(),
                                    'SalesPerson': $(this).find('.SalesPerson').text(),
                                    'DriverRemarks': $(this).find('.Cust span').attr('drvRemarks'),
                                    'From': $(this).find('.From select').val(),
                                    'To': $(this).find('.To > select').val(),
                                    'FromText': $(this).find('.From option:selected').text(),
                                    'ToText': $(this).find('.To option:selected').text(),
                                    'OrderDate': $(this).find('.OrderNo span').attr('orderdate'),
                                    'GrandTotal': $(this).find('.OrderNo span').attr('GrandAmount'),
                                    'Shipp': $(this).find('.Cust span').attr('shippingtypeautoid'),
                                    'Shippings': $(this).find('.Ships').text()

                                });
                            }
                        }
                    });
                    $('#tblRouteOrder .From select').attr('disabled', true);
                    $('#tblRouteOrder .To select').attr('disabled', true);
                    localStorage.setItem('tblRouteOrder', JSON.stringify(tblRouteOrder));
                    arrDrvId = [];
                    $("#tblDriverList tbody tr").each(function (i) {
                        arrDrvId.push({
                            'DriverID': $(this).find('.DriverNo span').attr('drvautoid'),
                            'StartTime': $(this).find('.StartTime > select').val(),
                            'EndTime': $(this).find('.EndTime > select').val(),
                            'StartTimeText': $(this).find('.StartTime option:selected').text(),
                            'EndTimeText': $(this).find('.EndTime option:selected').text(),
                            'Car': $(this).find('.Car > select').val(),
                        });
                    });
                    CreateOptimoOrder();
                }
            })
        }
        else {
            toastr.error('Please add driver.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
    }
}
function CreateOptimoOrder() {
    
    var data = [];
    $("#tblRouteOrder tbody tr").each(function (i) {
        if ($(this).find('.Action #chkOrder').prop('checked') == true) {
            var Alocation = {
                address: $(this).find('.Address').text(),
                locationName: $(this).find('.Address').text(),
                latitude: Number($(this).find('.Lat').text()),
                longitude: Number($(this).find('.Long').text())
            };
            var timeWindows = [{
                twFrom: $(this).find('.From > select').val(),
                twTo: $(this).find('.To > select').val()
            }]
            data.push({
                orderNo: $(this).find('.OrderNo').text(),
                date: $("#hfPlanningDate").val(),
                duration: Number($("#txtDuration").val()),
                location: Alocation,
                timeWindows: timeWindows
            })
        }
    });
    var order = {
        orders: data
    }
    $.ajax({
        type: "Post",
        url: "https://api.optimoroute.com/v1/create_or_update_orders" + "?key=" + Key,

        data: JSON.stringify(order),
        header: { 'content-type': 'application/json' },
        beforeSend: function () {
            //$.blockUI({
            //    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            //    overlayCSS: {
            //        backgroundColor: '#FFF',
            //        opacity: 0.8,
            //        cursor: 'wait'
            //    },
            //    css: {
            //        border: 0,
            //        padding: 0,
            //        backgroundColor: 'transparent'
            //    }
            //});
        },
        complete: function () {
            //$.unblockUI();
        },
        success: function (response) {
            
            if (response.success == true) {
                PlanRoute();
            }
        },
        error: function (result) {
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function disableAllDriver() {
    for (var i = 1; i <= 6; i++) {
        callDriverAPI(i, false, $("#hfPlanningDate").val());
    }
}
function increaseValue() {
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById('number').value = value;
    if (Number(value) > 6) {
        toastr.error('Maximum 6 drivers allowed.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        $("#number").val('6');
    }
    else {
        enableDisableDriver(value, true);
    }
}
function decreaseValue() {
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    value < 1 ? value = 1 : '';
    value--;
    document.getElementById('number').value = value;
    if (Number(value) > 0) {
        enableDisableDriver(Number(value) + 1, false);
    }
}
function enableDisableDriver(count, actionType) {

    callDriverAPI(count, actionType, $("#hfPlanningDate").val());
}
function callDriverAPI(DriverId, actionType, planningDate) {
    $.ajax({
        type: "Post",
        url: "https://api.optimoroute.com/v1/update_driver_parameters" + "?key=" + Key,
        data: JSON.stringify({ externalId: DriverId.toString(), enabled: actionType, date: planningDate }),
        header: { 'content-type': 'application/json' },
        beforeSend: function () {
            //$.blockUI({
            //    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            //    overlayCSS: {
            //        backgroundColor: '#FFF',
            //        opacity: 0.8,
            //        cursor: 'wait'
            //    },
            //    css: {
            //        border: 0,
            //        padding: 0,
            //        backgroundColor: 'transparent'
            //    }
            //});
        },
        complete: function () {
            //$.unblockUI();
        },
        success: function (response) {

            if (response.success == true) {
            }
        },
        error: function (result) {
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function addDriver(e) {
    
        var tr = $(e).closest('tr'), drvNo = "", check = 0, TimeCheck = 0, From = "", From1 = "", To = "", To1 = "";
        if (tr.find('.StartTime > select').val() != undefined) {
            From = tr.find('.StartTime > select').val();
            From = From.slice(0, 2);
            From1 = tr.find('.StartTime > select').val();
            From1 = From1.slice(3, 5);
            To = tr.find('.EndTime > select').val();
            To = To.slice(0, 2);
            To1 = tr.find('.EndTime > select').val();
            To1 = To1 = To1.slice(3, 5);
            if (Number(From) > Number(To)) {
                $("#btnPlanRoute").attr('disabled', true);
                TimeCheck = 1;
            }
            else if (Number(From) == Number(To)) {
                if (Number(From1) > Number(To1) || Number(From1) == Number(To1)) {
                    $("#btnPlanRoute").attr('disabled', true);
                    TimeCheck = 1;
                }
                else {
                    $("#btnPlanRoute").attr('disabled', false);
                }
            }
            else {
                $("#btnPlanRoute").attr('disabled', false);
            }
        }
        if (tr.find('.SelectedDriver option:selected').val() == "0") {
            toastr.error('Please select driver.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
        else if (TimeCheck == 1) {
            toastr.error('Invalid time. Please select a valid time', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
        else {
            //if ($('#tblDriverList tr').length - 1 == "1") {
            //    drvNo = "Driver 1";
            //}
            //else if ($('#tblDriverList tr').length - 1 == "2") {
            //    drvNo = "Driver 2";
            //}
            //else if ($('#tblDriverList tr').length - 1 == "3") {
            //    drvNo = "Driver 3";
            //}
            //else if ($('#tblDriverList tr').length - 1 == "4") {
            //    drvNo = "Driver 4";
            //}
            //else if ($('#tblDriverList tr').length - 1 == "5") {
            //    drvNo = "Driver 5";
            //}
            //else if ($('#tblDriverList tr').length - 1 == "6") {
            //    drvNo = "Driver 6";
            //}
            drvNo = "Driver " + ($('#tblDriverList tr').length - 1).toString();
            check = 0;
            $("#tblDriverList tbody tr").each(function (i) {
                if (tr.find('.SelectedDriver option:selected').val() == $(this).find(".DriverNo span").attr('drvAutoId')) {
                    check = 1;
                }
                else {
                    if (Number($('#tblDriverList tr').length - 1) == $("#hfDriverLimit").val()) {
                        $(this).find(".Action").text('');
                    }
                }
            });
            if (Number($('#tblDriverList tr').length - 1) > $("#hfDriverLimit").val()) {
                toastr.error('Maximum ' + $("#hfDriverLimit").val()+' drivers allowed.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                tr.find('.SelectedDriver').html(ddlDrv);
            }
            else {
                if (check == '1') {
                    toastr.error('Please add other driver.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
                    tr.find('.SelectedDriver').html(ddlDrv);
                }
                else {
                    var tblLength = $('#tblDriverList tr').length - 1 || 0;
                    tableBody = $("#tblDriverList tbody");
                    var tds = '<tr>';
                    tds += '<td class="DriverNo text-center"><span drvAutoId=' + tr.find('.SelectedDriver option:selected').val() + ' rowId=' + tblLength + '>' + drvNo + '<span></td><td class="SelectedDriver">' + tr.find('.SelectedDriver option:selected').text() + '</td><td  class="StartTime text-center">' + TimeFrom + '</td><td class="EndTime text-center">' + TimeFrom + '</td><td  class="Action text-center"><button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" onclick="removeDriver(this)">Remove</button></td>';
                    tds += '</tr>';
                    $("#tblDriverList tbody").append(tds);
                    $("#tblDriverList tbody tr").each(function () {
                        if (tr.find('.SelectedDriver option:selected').val() == $(this).find('.DriverNo span').attr('drvautoid')) {
                            $(this).find(".StartTime > select").val(tr.find('.StartTime option:selected').val()).change();
                            $(this).find(".EndTime > select").val(tr.find('.EndTime option:selected').val()).change();
                            $(this).find(".StartTime > select").attr('disabled', true);
                            $(this).find(".EndTime > select").attr('disabled', true);
                        }
                    })
                    var workTimeFrom = tr.find('.StartTime option:selected').val(),
                        workTimeTo = tr.find('.EndTime option:selected').val(),
                        StartLocation = tr.find('.SelectedDriver option:selected').attr('startlocation'),
                        EndLocation = tr.find('.SelectedDriver option:selected').attr('endlocation');
                    tr.find('.SelectedDriver').html(ddlDrv);
                    tr.find('.StartTime').html('');
                    tr.find('.EndTime').html('');
                    EnableDriver(Number(tblLength), true, $("#hfPlanningDate").val(), workTimeFrom, workTimeTo, parseFloat(StartLocation), parseFloat(EndLocation));
                    $("#totalAddedDriver").html($("#tblDriverList tbody tr").length);
                }
            }
        }
    }

function EnableDriver(DriverId, actionType, planningDate, workTimeFrom, workTimeTo, LocationLat, LocationLong) {
    $.ajax({
        type: "Post",
        url: "https://api.optimoroute.com/v1/update_driver_parameters" + "?key=" + Key,
        data: JSON.stringify({ externalId: DriverId.toString(), enabled: actionType, date: planningDate, workTimeFrom: workTimeFrom, workTimeTo: workTimeTo, startLatitude: LocationLat, startLongitude: LocationLong, endLatitude: LocationLat, endLongitude: LocationLong }),
        header: { 'content-type': 'application/json' },
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.success == true) {
            }
        },
        error: function (result) {
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function removeDriver(e) {

    var tr = $(e).closest('tr'), num = 0;;
    num = Number(tr.find('.DriverNo span').attr('rowid'));
    enableDisableDriver(num, false);
    $(e).closest('tr').remove();
    $('#tblDriverList tbody tr:last .Action').html('<button type="button" class="btn btn-danger buttonAnimation round box-shadow-1  btn-sm" onclick="removeDriver(this)">Remove</button>');
    $("#totalAddedDriver").html($("#tblDriverList tbody tr").length);
}
function PlanRoute() {
    
    var PlanDate = $("#hfPlanningDate").val();
    var balanceBy = "";
    if ($("#chkNoBalancing").prop("checked") == true) {
        var data = {
            date: $("#hfPlanningDate").val()
        }
    }
    else if ($("#chkBalance").prop("checked") == true) {
        if ($("#chkWorkTime").prop("checked") == true) {
            balanceBy = "WT";
        }
        else {
            balanceBy = "NUM";
        }
        var data = {
            date: $("#hfPlanningDate").val(),
            balancing: "ON",
            balanceBy: balanceBy
        }
    }
    else if ($("#chkBalanceByDriver").prop("checked") == true) {
        if ($("#chkWorkTime").prop("checked") == true) {
            balanceBy = "WT";
        }
        else {
            balanceBy = "NUM";
        }
        var orders = [];
        $("#tblRouteOrder tbody tr").each(function (i, main) {
            
            if ($(main).find('.Action #chkOrder').prop('checked') == true) {
                if ($(main).find('.OrderNo span').html() != "") {
                    if (i == $("#tblRouteOrder tbody tr").length - 1) {
                        orders.push($(main).find('.OrderNo span').html());
                    }
                    else {
                        orders.push($(main).find('.OrderNo span').html());
                    }
                }
            }
        });
        var data = {
            date: $("#hfPlanningDate").val(),
            balancing: "ON_FORCE",
            balanceBy: balanceBy,
            balancingFactor: parseFloat($("#ddlBalancingFactor").val()),
            useOrders: orders
        }
    }
    $.ajax({
        type: "Post",
        url: "https://api.optimoroute.com/v1/start_planning" + "?key=" + Key,
        data: JSON.stringify(data),
        header: { 'content-type': 'application/json' },
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            
            if (response.success == true) {
                $("#hfPlanningId").val(response.planningId);
                $("#btnPlanRoute").hide();
                $("#btnGetRoute").show();
                swal("", "Route has been planned successfully.", "success");
                $("#txtPlanningDate").attr("disabled", true);
            }
            else {
                swal("", "Oops, Something went wrong.Try later.", "error");
            }
        }
    })
}
function GetPlanningStatus() {

    $.ajax({
        type: "GET",
        url: "https://api.optimoroute.com/v1/get_planning_status" + "?key=" + Key + "&planningId=" + Number($("#hfPlanningId").val()),
        data: {},
        header: { 'content-type': 'application/json' },
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.success == true) {
                if (response.status == "F") {
                    GetRoutes();
                    $('.nav-tabs').find('a:eq(1)').removeClass('active');
                    $('.nav-tabs').find('a:eq(0)').attr('class', 'nav-link active');
                    $('#unscheduled').removeClass('active');
                    $('#scheduled').attr('class', 'row tab-pane active');
                    $('#unschCount').hide();
                    $('#schCount').show();
                }
                else if (response.status == "R") {
                    swal("", "Planning is in under process.", "success");
                }
                else if (response.status == "C") {
                    swal("", "Planning is cancelled by user.", "success");
                    $("#btnPlanRoute").show();
                    $("#btnGetRoute").hide();
                }
                else if (response.status == "E") {
                    swal("", "Oops, Something went wrong.Try again", "success");
                    $("#btnPlanRoute").show();
                    $("#btnGetRoute").hide();
                }
            }
        }
    })
}
function GetRoutes() {

    $.ajax({
        type: "GET",
        url: "https://api.optimoroute.com/v1/get_routes" + "?key=" + Key + "&date=" + $("#hfPlanningDate").val(),
        data: {},
        header: { 'content-type': 'application/json' },
        beforeSend: function () {
            //$.blockUI({
            //    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            //    overlayCSS: {
            //        backgroundColor: '#FFF',
            //        opacity: 0.8,
            //        cursor: 'wait'
            //    },
            //    css: {
            //        border: 0,
            //        padding: 0,
            //        backgroundColor: 'transparent'
            //    }
            //});
        },
        complete: function () {
            //$.unblockUI();
        },
        success: function (response) {
          
            if (response.success == true) {
                $("#popupRouteDetails").modal('show');
                $("#popupOptimoOrderDetails").modal('hide');

                var totalUnscheduleOrd = 0, chkUnscheduleOrd = [];
                var RouteDetails = response.routes;
                var OrdNumber = [], drvId = [], NoOfOrder = 0, checkNoOrd = [];
                var OrderList = JSON.parse(localStorage.getItem('UnscheduldedOrder'));
                var tblRouteOrder = JSON.parse(localStorage.getItem('tblRouteOrder'));
               
                var xmldoc = $.parseXML(OrderList);
                var DriverList = $(xmldoc).find("Table1");
                $("#tblRouteDetails tbody tr").remove();
                var row = $("#tblRouteDetails thead tr:last-child").clone();
                if (RouteDetails.length > 0) {
                    var noOfDriver = Number($('#tblDriverList tr').length - 1);
                    for (i = 1; i <= noOfDriver; i++) {
                        for (j = 0; j < RouteDetails.length; j++) {
                            var item = RouteDetails[j];
                            console.log(item);
                            if (i == item.driverExternalId) {
                                $.each(RouteDetails[j].stops, function (k, items) {
                                    $.each(tblRouteOrder, function (n, trd) {
                                        if (items.orderNo == trd.OrderNo) {
                                            $(".OrderAutoIds", row).html(trd.OrderAutoIds);
                                            $(".Orders", row).html(trd.Orders);
                                            $(".Customer", row).html('<span lat="' + trd.Lat + '" Long="' + trd.Long + '" deliveredaddress="' + items.address + '" customer="' + trd.CustomerName + '" amount="' + trd.GrandTotal + '" otherorders="' + trd.Orders + '">' + trd.CustomerName + '<br><b style="font-size:11px">' + items.address + '</b><br><b style="font-size:11px">' + trd.Orders + '</b></span>');
                                            $(".SalesPerson", row).html('<span OrdAutoId="' + trd.OrderAutoId + '" Shipp="' + trd.Shipp + '" cName="' + trd.CustomerName + '" cAddress="' + items.address + '">' + trd.SalesPerson + '</span>');
                                            $(".Remark", row).html('<textarea rows="1" class="form-control border-primary input-sm text-center" cols="50">' + trd.DriverRemarks + '</textarea>');
                                            $(".From", row).html(trd.FromText);
                                            $(".To", row).html(trd.ToText);
                                            $(".Shippings", row).html(trd.Shippings);
                                        }
                                    });

                                    $(".OrderNo", row).html(items.orderNo);
                                    $(".Stop", row).html(items.stopNumber);
                                    $(".scheduledAt", row).html(tConvert(items.scheduledAt));
                                    $(".Driver", row).html(item.driverExternalId);
                                    OrdNumber.push({
                                        orderNo: items.orderNo
                                    })
                                    $("#tblRouteDetails tbody").append(row);
                                    row = $("#tblRouteDetails tbody tr:last-child").clone();
                                });
                                drvId.push({
                                    driverId: item.driverExternalId
                                })
                                break;
                            }
                        }
                    }
                
                    $("#tblRouteDetails tbody tr").show();
                    var match = 0;
                    if (tblRouteOrder.length > 0) {
                        $("#tblUnscheduledOrder tbody tr").remove();
                        var rows = $("#tblUnscheduledOrder thead tr:last-child").clone();
                        $.each(tblRouteOrder, function (j, item) {
                            match = 0;
                            $.each(OrdNumber, function (i, orn) {
                                if (orn.orderNo == item.OrderNo) {
                                    match = 1;
                                }
                            });
                            if (match == 0) {
                                $(".OrderNo", rows).text(item.OrderNo);
                                $(".Customer", rows).html('<span>' + item.CustomerName + '<br><b style="font-size:11px">' + item.Address + '</b><br><b style="font-size:11px">' + item.Orders + '</b></span>');//
                                //$(".Delivery", rows).html('<span>' + item.FromText + ' :' + item.ToText + '</span>');
                                $(".From", rows).html(item.FromText);
                                $(".To", rows).html(item.ToText);
                                $(".SalesPerson", rows).html(item.SalesPerson);
                                $(".Remark", rows).html(item.DriverRemarks);

                                $("#tblUnscheduledOrder tbody").append(rows);
                                rows = $("#tblUnscheduledOrder tbody tr:last-child").clone();
                            }
                        });
                        $("#tblUnscheduledOrder tbody tr").show();
                    }
                    else {
                        $("#tblUnscheduledOrder").hide();
                    }

                    var ddl = '<select onchange="checkDriver(this)" class="form-control input-sm border-primary">';
                    ddl += "<option value='0'>--Select Driver--</option>";
                    $.each(DriverList, function () {
                        ddl += "<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Name").text() + "</option>"
                    })
                    ddl += '</select>';

                    $("#tblDrvList tbody tr").remove();
                    var row = $("#tblDrvList thead tr:last-child").clone();
                    var totalOrder = 0;
                    $.each(drvId, function (o, d) {
                       
                        $.each(RouteDetails, function (m, rd) {
                        
                            if (d.driverId == rd.driverExternalId) {
                                
                                var extID = o + 1;
                                $(".ExternalDriver", row).html('<span DriverPlanningAutoId=' + arrDrvId[m].DriverID + '>' + extID+'</span>');
                                NoOfOrder = 0;
                                $("#tblRouteDetails tbody tr").each(function (o, itm) {
                                    if (rd.driverExternalId == $(this).find('.Driver').text()) {
                                        NoOfOrder = Number(NoOfOrder) + 1;
                                        if ($(this).find('.Orders').text() != '') {
                                            checkNoOrd = $(this).find('.Orders').text().split(',');
                                            NoOfOrder = Number(NoOfOrder) + checkNoOrd.length;
                                        }
                                    }
                                })
                                totalOrder = Number(totalOrder) + Number(NoOfOrder);
                                $(".TotalOrder", row).html(NoOfOrder);
                                $(".Stop", row).html(rd.stops.length);
                                $(".DriverList", row).html(ddl);
                                row.find(".DriverList > select").val(arrDrvId[m].DriverID).change();
                               
                                $(".drvStartTime", row).html(arrDrvId[m].StartTimeText);
                                $(".drvEndTime", row).html(arrDrvId[m].EndTimeText);
                                $(".drvCar", row).html(ddlCar);
                                $(".drvAction", row).html("<a href='javascript:; ' onclick='viewDriverOrder(this)'><span class='la la-history' title='View order'></span></a>&nbsp;&nbsp;<a onclick='printDriverOrder(this)' href='javascript:; '><span class='la la-print' title='Print'></span></a>&nbsp;&nbsp;<a onclick='printGoogleMap(this)' href='javascript:; '><span class='la la-map-marker' title='Print Map'></span></a>");
                                $("#tblDrvList tbody").append(row);
                                row = $("#tblDrvList tbody tr:last").clone(true);
                            }
                        });
                    });
                    $("#bDelDate").html($("#txtPlanningDate").val());
                    $('#scheduleCount').html(totalOrder);
                    $('#btotalSchedule').html(totalOrder);
                    $('#totalCustomer').html($('#tblRouteDetails tbody tr').length);
                    $("#tblUnscheduledOrder tbody tr").each(function (o, itm) {
                        totalUnscheduleOrd = Number(totalUnscheduleOrd) + 1;
                        if ($(this).find('.Orders').text() != '') {
                            chkUnscheduleOrd = $(this).find('.Orders').text().split(',');
                            totalUnscheduleOrd = Number(totalUnscheduleOrd) + chkUnscheduleOrd.length;
                        }
                    })
                    $('#unscheduleCount').html(totalUnscheduleOrd);
                    $('#btotalUnschedule').html(totalUnscheduleOrd);

                    $('#totalOrder').html(Number(totalOrder) + Number(totalUnscheduleOrd));

                }
                else {
                    if (tblRouteOrder.length > 0) {
                        $("#tblUnscheduledOrder tbody tr").remove();
                        var rows = $("#tblUnscheduledOrder thead tr:last-child").clone();
                        $.each(tblRouteOrder, function (j, item) {
                            $(".OrderNo", rows).text(item.OrderNo);
                            $(".Customer", rows).html(item.CustomerName);//
                            $(".Duration", rows).html('<input type="text" style="width:50px" value="15" class="border-primary input-sm text-center">');
                            $(".Lat", rows).text(item.Lat);
                            $(".Long", rows).text(item.Long);
                            $(".Address", rows).text(item.Address);
                            $(".Orders", rows).text(item.Orders);
                            $("#tblUnscheduledOrder tbody").append(rows);
                            rows = $("#tblUnscheduledOrder tbody tr:last-child").clone();
                        });
                        $("#tblUnscheduledOrder tbody tr").each(function (o, itm) {
                            totalUnscheduleOrd = Number(totalUnscheduleOrd) + 1;
                            if ($(this).find('.Orders').text() != '') {
                                chkUnscheduleOrd = $(this).find('.Orders').text().split(',');
                                totalUnscheduleOrd = Number(totalUnscheduleOrd) + chkUnscheduleOrd.length;
                            }
                        })
                        $('#btotalUnschedule').html(totalUnscheduleOrd);
                        $('#unscheduleCount').html(totalUnscheduleOrd);
                        $('#totalOrder').html(totalUnscheduleOrd);
                        $("#bDelDate").html($("#txtPlanningDate").val());
                        $('#scheduleCount').html('0');
                        $('#totalCustomer').html($('#tblUnscheduledOrder tr').length - 1);
                    }
                    $('.nav-tabs').find('a:eq(0)').removeClass('active');
                    $('.nav-tabs').find('a:eq(1)').attr('class', 'nav-link active');
                    $('#scheduled').removeClass('active');
                    $('#unscheduled').attr('class', 'row tab-pane active');
                    $('#unschCount').show();
                    $('#schCount').hide();
                    toastr.error('No route details found. Please replan route', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
                }
            }
        }
    })
}
function ChangePlanningDate() {
    var date = new Date($("#txtPlanningDate").val()),
        days = parseInt(dateDifference, 10);
    if (!isNaN(date.getTime())) {
        date.setDate(date.getDate() + days);
        var dt = new Date(date);
        planningDate = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
        $("#hfPlanningDate").val(planningDate);
    } 
    disableAllDriver();
    $("#number").val('0');
    $("#tblDriverList tbody tr").remove();
}
function ReplanRoute() {

    swal({
        title: "Are you sure?",
        text: ".You want to replan.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Cancel.",
                value: null,
                visible: true,
                icon: "warning",
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes.",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            $("#popupRouteDetails").modal('hide');
            $("#popupOptimoOrderDetails").modal('show');
            $("#decrease").removeAttr("disabled");
            $("#increase").removeAttr("disabled");
            $("#txtPlanningDate").removeAttr("disabled");
            $("#btnPlanRoute").show();
            $("#btnGetRoute").hide();
            $('#tblRouteOrder .From select').removeAttr('disabled');
            $('#tblRouteOrder .To select').removeAttr('disabled');
        }
    });
}
function checkDriver(e) {

    var row = $(e).closest("tr");
    var match = 0, drviD = row.find(".DriverList > select option:selected").val();
    $("#tblDrvList tbody tr").each(function (i) {
        if (drviD == $(this).find(".DriverList > select option:selected").val()) {
            match = Number(match) + 1;
        }
    });
    if (match > 1) {
        $("#btnSaveRoute").attr('disabled', true);
        toastr.error('Please select different driver.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
        row.find(".DriverList > select").val('0');
    }
    else {
        $("#btnSaveRoute").removeAttr('disabled');
    }
}
function showHideCount(a) {
    if (a == 1) {
        $("#schCount").show();
        $("#unschCount").hide();
    }
    else {
        $("#schCount").hide();
        $("#unschCount").show();
    }
}
function SaveRouteDetails() {
    
    var mapDrv = 0, checkCar = 0, duplicateCar = 0;
    $("#tblDrvList tbody tr").each(function (i) {
        if ($(this).find(".DriverList select").val() == '0') {
            mapDrv = 1;
        }
    });
    $("#tblDrvList tbody tr").each(function (i, main) {
        duplicateCar = 0;
        if ($(this).find('.drvCar > select').val() == '0') {
            checkCar = 1;
        }
        else {
            $("#tblDrvList tbody tr").each(function (i, sub) {
                if ($(main).find('.drvCar > select').val() == $(this).find('.drvCar > select').val()) {
                    duplicateCar = Number(duplicateCar) + 1;
                }
            })
        }
    });
    if (mapDrv == 1) {
        toastr.error('Please map driver first before save.', 'Error', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
    else if (checkCar == 1) {
        toastr.error('Please assign car to the driver.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else if (Number(duplicateCar) > 1) {
        toastr.error('Please assign different car to each driver.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else {
        swal({
            title: "Are you sure?",
            text: "You want to save.",
            icon: "warning",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No, Cancel.",
                    value: null,
                    icon: "warning",
                    visible: true,
                    className: "btn-warning",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes.",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            }
        }).then(function (isConfirm) {
            if (isConfirm) {
                var Orders = [], driverLog = [], ord = []; 
                $("#tblRouteDetails tbody tr").each(function (i, main) {
                    var drvID = 0, drvCar, driverPlanningAutoId = 0, driverStarTime = '', driverEndTime = '', isotherOrder = 0;
                    $("#tblDrvList tbody tr").each(function (j, sub) {
                        
                      
                        if ($(main).find('.Driver').text() == $(sub).find('.ExternalDriver span').html()) {
                            
                            
                          
                                drvID = $(sub).find(".DriverList > select option:selected").val(),
                                    drvCar = $(sub).find(".drvCar > select option:selected").val(),
                                    driverLog.push({
                                        'PlanningId': $("#hfPlanningId").val(),
                                        'DriverNo': $(sub).find('.ExternalDriver span').html(),
                                        'DriverAutoId': $(sub).find(".DriverList > select option:selected").val(),
                                        'DriverPlanningAutoId': $(sub).find('.ExternalDriver span').attr('driverplanningautoid'),
                                        'DriverStartTime': $(sub).find('.drvStartTime').html(),
                                        'DraverEndTime': $(sub).find('.drvEndTime').html(),
                                        'NoOfStop': $(sub).find('.Stop').text(),
                                        'CarAutoId': $(sub).find(".drvCar > select option:selected").val(),
                                        'RouteDate': $("#txtPlanningDate").val()
                                    })
                           
                        }
                    });
                    if ($(main).find('.Orders').text() != "") {
                        ord = $(main).find('.Orders').text().split(',');
                    }
                    else {
                        ord = 0;
                    }
                    if (ord.length > 0) {
                        Orders.push({
                            'OrderNo': $(main).find('.OrderNo').text(),
                            'Driver': drvID,
                            'CarId': drvCar,
                            'Stop': $(main).find('.Stop').text(),
                            'Remark': $(main).find('.Remark textarea').val(),
                            'ScheduleDate': $("#txtPlanningDate").val(),
                            'Lat': $(main).find('.Customer span').attr('lat'),
                            'Long': $(main).find('.Customer span').attr('long'),
                            'DeliveryFrom': $(main).find('.From').text(),
                            'DeliveryTo': $(main).find('.To').text(),
                            'ScheduleAt': $(main).find('.scheduledAt').text(),
                            'DeliveredAddress': $(main).find('.Customer span').attr('deliveredaddress'),
                        });
                        $.each(ord, function (n, or) {
                            if (drvID != 0) {
                                Orders.push({
                                    'OrderNo': ord[n],
                                    'Driver': drvID,
                                    'CarId': drvCar,
                                    'Stop': $(main).find('.Stop').text(),
                                    'Remark': $(main).find('.Remark textarea').val(),
                                    'ScheduleDate': $("#txtPlanningDate").val(),
                                    'Lat': $(main).find('.Customer span').attr('lat'),
                                    'Long': $(main).find('.Customer span').attr('long'),
                                    'DeliveryFrom': $(main).find('.From').text(),
                                    'DeliveryTo': $(main).find('.To').text(),
                                    'ScheduleAt': $(main).find('.scheduledAt').text(),
                                    'DeliveredAddress': $(main).find('.Customer span').attr('deliveredaddress'),
                                });
                            }
                        })
                    }
                    else {
                        if (drvID != 0) {
                            Orders.push({
                                'OrderNo': $(main).find('.OrderNo').text(),
                                'Driver': drvID,
                                'CarId': drvCar,
                                'Stop': $(main).find('.Stop').text(),
                                'Remark': $(main).find('.Remark textarea').val(),
                                'ScheduleDate': $("#txtPlanningDate").val(),
                                'Lat': $(main).find('.Customer span').attr('lat'),
                                'Long': $(main).find('.Customer span').attr('long'),
                                'DeliveryFrom': $(main).find('.From').text(),
                                'DeliveryTo': $(main).find('.To').text(),
                                'ScheduleAt': $(main).find('.scheduledAt').text(),
                                'DeliveredAddress': $(main).find('.Customer span').attr('deliveredaddress'),
                            });
                        }
                    }
                })
                console.log(Orders)
                $.ajax({
                    type: "Post",
                    url: "/Manager/WebAPI/OptimoRoute.asmx/SaveRouteDetails",
                    //data: "{'TableValues':'" + JSON.stringify(Orders) + "','driverLog':'" + JSON.stringify(driverLog) + "'}",
                    data: JSON.stringify({ TableValues: JSON.stringify(Orders), driverLog: JSON.stringify(driverLog) }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    beforeSend: function () {
                        $.blockUI({
                            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                            overlayCSS: {
                                backgroundColor: '#FFF',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        if (response.d == "true") {
                            $("#btnSaveRoute").hide();
                            $("#btnRePlanRoute").hide();
                            $("#btnPrint").show();
                            $('.Remark textarea').attr('disabled', true);
                            $('#tblDrvList .DriverList select').attr('disabled', true);
                            $('#tblDrvList .drvCar select').attr('disabled', true);
                            $('#tblDriverOrderLog .Remark textarea').attr('disabled', true);
                            swal("", "Route details saved successfully.", "success").then(function (isConfirm) {
                                if (isConfirm) {
                                    saveStatus = 1;
                                }
                            });
                        }
                        else if (response.d == "Duplicate driver") {
                            swal("", "Duplicate driver. You can't create multiple route with same driver on same date.", "warning");
                        }
                        else {
                            swal("", "Oops, Something went wrong. Please try later.", "error");
                        }
                    }
                })
            }
        })
    }
}
function closeRoutePopup() {
    swal({
        title: "Are you sure?",
        text: "You want to cancel this auto route planning.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Don't Cancel.",
                value: null,
                visible: true,
                icon: "warning",
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Cancel It.",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            location.reload();
        }
    });
}
function PlanRouteClose() {
    swal({
        title: "Are you sure?",
        text: "You want to cancel this auto route planning.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "No, Don't Cancel.",
                value: null,
                visible: true,
                icon: "warning",
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes, Cancel It.",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            $("#popupOptimoOrderDetails").modal('hide');
        }
         
    });
}
function bulkprint() {
    window.open("/Manager/BulkOrderPrint2.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
}
function popupPrintOption() {
    var checkOrdStatus = 0
    $("#tblOrderList tbody tr").each(function (o, itm) {
        if ($(this).find('.status span').text() != 'Ready To Ship') {
            checkOrdStatus = 1;
        }
    })
    if (checkOrdStatus == 1) {
        toastr.error('Please select ready to ship order.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else {
        if ($("#ddlDriver").val() == '0') {
            toastr.error('Please select driver.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
        else {
            $("#modalPrintOption").modal('show');
        }
    }
}
function BulkPrintItemTemplate1() {
    var flag = false; var OrderAutoIds = 0;
    var OrderAutoId = [];
    $('input:checkbox:checked', '#tblOrderList tbody').each(function (index, item) {
        var row = $(item).closest('tr');
        OrderAutoIds = row.find('.OrderAutoId').text(),
            OrderAutoId.push(OrderAutoIds);
        flag = true;
    });
    var OAutoId = OrderAutoId.join(",");
    localStorage.setItem('BulkTemplate1OrderAutoId', OAutoId);
    if (flag) {
        if (localStorage.getItem('Driver') == '0') {
            toastr.error('No driver selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
        else {
            if (localStorage.getItem('Orderstatus') != '4') {
                toastr.error('Select only ready to ship order status ', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            }
            else {
                window.open("/Manager/Template1.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
        }

    } else {
        toastr.error('Please check atleast one order', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }

}
function PrintOptionOrder() {
    var check = 0;
    if ($("#chkPsmDefaultOption").prop('checked') == true) {
        PrintPSMDefault();
    }
    else if ($("#chkTemplateTwoOption").prop('checked') == true) {
        BulkPrintItem();
    }
    else if ($("#chkTemplateOneOption").prop('checked') == true) {
        BulkPrintItemTemplate1();
    }

    else {
        check = 1;
    }
    if (check == 1) {
        toastr.error('Please select an option.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
}
function BulkPrintItem() {
    var flag = false; var OrderAutoIds = 0;
    var OrderAutoId = [];
    $('input:checkbox:checked', '#tblOrderList tbody').each(function (index, item) {
        var row = $(item).closest('tr');
        OrderAutoIds = row.find('.OrderAutoId').text(),
            OrderAutoId.push(OrderAutoIds);
        flag = true;
    });
    var OAutoId = OrderAutoId.join(",");
    localStorage.setItem('BulkItemOrderAutoId', OAutoId);
    if (flag) {
        if (localStorage.getItem('Driver') == '0') {
            toastr.error('No driver selected.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        }
        else {
            if (localStorage.getItem('Orderstatus') != '4') {
                toastr.error('Select only ready to ship order status ', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
            }
            else {
                window.open("/Manager/Template2.html", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
            }
        }

    } else {
        toastr.error('Please check atleast one order', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }

}
function OpenPopupRemark(OrderAutoId) {

    $("#hfOrdAutoId").val(OrderAutoId);
    bindDriverRemark();
}
function UpdateDriverRemark() {
    if ($('#txtDriverRemark').val() == "") {
        toastr.error('Remark is mandatory.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
        return;
    }
    var data = {
        OrderAutoId: $("#hfOrdAutoId").val(),
        DriverRemark: $('#txtDriverRemark').val(),
    };
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/UpdateDriverRemark",
        data: JSON.stringify({ dataValues: JSON.stringify(data) }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
          
            if (response.d == 'true') {
                swal('', 'Driver remark is updated successfully.', 'success');
                $("#modalDriverRemark").modal('hide');
            }
            else {
                swal('', 'Opps something went wrong.', 'error');
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function bindDriverRemark() {
    $.ajax({
        type: "POST",
        url: "/Manager/WebAPI/WManager_OrderList.asmx/getDriverRemark",
        data: "{'OrderAutoId':'" + $("#hfOrdAutoId").val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            var xmldoc = $.parseXML(response.d);
            var DriverRemark = $(xmldoc).find("Table");
            $("#txtDriverRemark").val($(DriverRemark).find('DriverRemarks').text());
            $("#modalDriverRemark").modal('show');
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function NoBalancing() {
    if ($("#chkNoBalancing").prop("checked") == true) {
        $("#divBalanceBy").hide();
        $("#divBalanceFactor").hide();
        $("#chkWorkTime").removeAttr("checked");
        $("#chkNoOfOrdPerDriver").removeAttr("checked");
        $("#ddlBalancingFactor").val('0');

    }
    else {
        $("#divBalanceBy").show();
        $("#divBalanceFactor").show();
        $("#chkWorkTime").removeAttr("checked");
        $("#chkNoOfOrdPerDriver").removeAttr("checked");
        $("#ddlBalancingFactor").val('0').change();
    }
}
function BalanceRoute() {
    if ($("#chkBalance").prop("checked") == true) {
        $("#divBalanceBy").show();
        $("#divBalanceFactor").hide();
        $("#chkWorkTime").attr("checked", true);
    }
}
function BalanceUseDriver() {
    if ($("#chkBalanceByDriver").prop("checked") == true) {
        $("#divBalanceBy").show();
        $("#divBalanceFactor").show();
        $("#chkWorkTime").attr("checked", true);
        $("#ddlBalancingFactor").val(0.7).change();
    }
}
function viewDriverOrder(e) {
    var arrDrvRemark = [], driverId = "";
    var tr = $(e).closest('tr');
    driverId = tr.find('.ExternalDriver').text();
    $("#bDriverName").html(tr.find('.DriverList option:selected').text());
    $("#bTotalStop").html(tr.find('.Stop').text());

    $("#tblRouteDetails tbody tr").each(function (i, main) {
        if (driverId == $(main).find('.Driver').text()) {
            arrDrvRemark.push(
                {
                    "OrderNo": $(this).find(".OrderNo").text(),
                    "Customer": $(this).find(".SalesPerson span").attr('cName'),
                    "Address": $(this).find(".SalesPerson span").attr('cAddress'),
                    "SalesPerson": $(this).find(".SalesPerson").text(),
                    "Stop": $(this).find(".Stop").text(),
                    "scheduledAt": $(this).find(".scheduledAt").text(),
                    "Remark": $(this).find(".Remark textarea").val(),
                    "OthersOrder": $(this).find(".Orders").text()
                })
        }
    });
    $("#tblDriverOrderLog tbody tr").remove();
    var rows = $("#tblDriverOrderLog thead tr").clone();
    $.each(arrDrvRemark, function (j, item) {
        console.log(item.OrderNo)
        $(".OrderNo", rows).text(item.OrderNo);
        $(".Customer", rows).html("<span>" + item.Customer + "<br><b style='font-size:11px'>" + item.Address + "</b><br><b style='font-size:11px;max-width: 300px !important; white-space: normal'>" + item.OthersOrder + " </b></span>");
        $(".SalesPerson", rows).text(item.SalesPerson);
        $(".Stop", rows).text(item.Stop);
        $(".ScheduleAt", rows).text(item.scheduledAt);
        $(".Orders", rows).text(item.OthersOrder)
        if (saveStatus == 1) {
            $(".Remark", rows).html('<textarea disabled rows="1" onchange="UpdateRemark(this)" class="form-control border-primary input-sm text-center" cols="50">' + item.Remark + '</textarea>');
        }
        else {
            $(".Remark", rows).html('<textarea rows="1" onchange="UpdateRemark(this)" class="form-control border-primary input-sm text-center" cols="50">' + item.Remark + '</textarea>');
        }
        $("#tblDriverOrderLog tbody").append(rows);
        rows = $("#tblDriverOrderLog tbody tr:last-child").clone();
    });
    $("#modalDriverOrderLog").modal('show');
    arrDrvRemark.length = 0;
}
function printDriverOrder(e) {
    
    var ShipList = [], arrShip = [];
    if (saveStatus == 1) {
        var tr = $(e).closest('tr');
        $("#modalDriverPrintOption").modal('show');

        $("#hfDrvIdForPrint").val(tr.find('.ExternalDriver').text());
        $("#tblRouteDetails tbody tr").each(function (o, itm) {
            if ($("#hfDrvIdForPrint").val() == $(this).find('.Driver').text()) {
                ShipList.push({
                    'ShipAutoId': $(this).find('.SalesPerson span').attr('shipp')
                })
                if ($(itm).find('.Shippings').text() != "") {
                    arrShip = $(itm).find('.Shippings').text().split(',');
                }
                else {
                    arrShip = 0;
                }
                if (arrShip.length != 0) {
                    $.each(arrShip, function (n, or) {
                        ShipList.push({
                            'ShipAutoId': arrShip[n]
                        });
                    })
                }
            }
        })
        $.ajax({
            type: "Post",
            url: "/Manager/WebAPI/OptimoRoute.asmx/getShipDetails",
            data: "{'TableValues':'" + JSON.stringify(ShipList) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function () {
                $.blockUI({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#FFF',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.d != "Session Expired") {
                    if (response.d != "false") {
                        var xmldoc = $.parseXML(response.d);
                        var ShippingType = $(xmldoc).find("Table");
                        var TemplateType = $(xmldoc).find("Table1");
                        TemplateT = '<select class="form-control input-sm border-primary">';
                        TemplateT += "<option value='0'>--Select Template--</option>";
                        $.each(TemplateType, function () {
                            TemplateT += "<option value='" + $(this).find("AutoId").text() + "'>" + $(this).find("Template").text() + "</option>"
                        })
                        TemplateT += '</select>';
                        var addDefault = 0;
                        $("#tblDriverPrintOption tbody tr").remove();
                        var row = $("#tblDriverPrintOption thead tr:last-child").clone(true);
                        $.each(ShippingType, function (i) {
                            if (addDefault == 0) {
                                $(".Action", row).html('<input type="checkbox" checked name="Action" id="checkDriverAction" />');
                                $(".PrintOption", row).html('Driver Log');
                                $(".TemplateType", row).html('');
                                $(".NoOfCopy", row).html('');
                                addDefault = 1;
                                $("#tblDriverPrintOption tbody").append(row);
                                row = $("#tblDriverPrintOption tbody tr:last-child").clone(true);
                            }
                            $(".Action", row).html('<input type="checkbox" checked name="Action" id="checkAction" />');
                            $(".PrintOption", row).html('<span ShippingId=' + $(this).find("AutoId").text() + '>' + $(this).find("ShippingType").text() + '</span>');
                            $(".TemplateType", row).html(TemplateT);
                            $(".NoOfCopy", row).html('<input type="text" style="width: 20px !important; text-align:center;" id="txtNumberOfCopy" value="2" class="form-control input-sm border-primary" />');
                            $("#tblDriverPrintOption tbody").append(row);
                            row = $("#tblDriverPrintOption tbody tr:last-child").clone(true);
                        });
                        $("#tblDriverPrintOption tbody tr").show();
                    }
                }
            }
        })

    }
    else {
        toastr.error('Please save route details before print.', 'Error', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
    }
}
function countCheckOrder() {
    var countCheckOrder = 0;
    $("#tblRouteOrder tbody tr").each(function (i) {
        if ($(this).find('.Action #chkOrder').prop('checked') == true) {
            countCheckOrder = Number(countCheckOrder) + 1;
        }
    })
    $("#totalCheckedOrder").html(countCheckOrder);
}
function UpdateRemark(e) {
    
    var tr = $(e).closest('tr');
    $("#tblRouteDetails tbody tr").each(function (i, main) {
        if (tr.find('.OrderNo').text() == $(main).find('.OrderNo').text()) {
            $(this).find(".Remark textarea").val(tr.find('.Remark textarea').val());
        }
    });
}
function PrintDrvOrder() {
    
    arrOrdAutoIdList = [], arrShipAutoIdList = [];
    var OrderAutoId = [], drlogorder = '', drlogorder1 = '', drlogorder2 = '', drlogorder3 = '';
    OrderAutoId[0] = new Object();
    OrderAutoId[0].PrintType = 0;
    OrderAutoId[0].orders = 0;
    OrderAutoId[0].noofcopy = 1;
    OrderAutoId[1] = new Object();
    OrderAutoId[1].PrintType = 0;
    OrderAutoId[1].orders = 0;
    OrderAutoId[1].noofcopy = 1;
    OrderAutoId[2] = new Object();
    OrderAutoId[2].PrintType = 0;
    OrderAutoId[2].orders = 0;
    OrderAutoId[2].noofcopy = 1;
    OrderAutoId[3] = new Object();
    OrderAutoId[3].PrintType = 0;
    OrderAutoId[3].orders = 0;
    OrderAutoId[3].noofcopy = 1;
    var check = 0, checkVal = 0;
    $("#tblDriverPrintOption tbody tr").each(function (j, tblShip) {
        if ($(tblShip).find('.Action input').prop('checked') == false) {
            check = Number(check) + 1;
        }
        if ($(tblShip).find('.Action input').prop('checked') == true) {
            if ($(tblShip).find('.TemplateType option:selected').val() == "0") {
                checkVal = 1;
            }
            if (Number($(tblShip).find('.NoOfCopy input').val()) <= 0) {
                checkVal = 1;
            }
        }
    })
    if (checkVal == 1) {
        toastr.error('Please fill all feilds.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else if (Number(check) == Number($("#tblDriverPrintOption tbody tr").length)) {
        toastr.error('Please check atleast one item.', 'Warning', { positionClass: 'toast-top-center', containerId: 'toast-top-right' });
    }
    else {
        $("#tblDriverPrintOption tbody tr").each(function (j, tblShip) {
            $("#tblRouteDetails tbody tr").each(function (i, main) {
                if ($("#hfDrvIdForPrint").val() == $(main).find('.Driver').text()) {
                    if ($(tblShip).find('.Action input').prop('checked') == true) {
                        if (j == 0) {
                            OrderAutoId[0].PrintType = 0;
                            drlogorder += $(main).find('.SalesPerson span').attr('ordautoid') + ',';
                            if ($(main).find('.OrderAutoIds').text() != "") {
                                arrOrdAutoIdList = $(main).find('.OrderAutoIds').text().split(','); 
                            }
                            else {
                                arrOrdAutoIdList.length = 0; 
                            }
                            if (arrOrdAutoIdList.length != 0) {
                                $.each(arrOrdAutoIdList, function (n, or) { 
                                    drlogorder += arrOrdAutoIdList[n] + ','; 
                                })
                            }
                        }
                        if (j != 0) {
                            if ($(tblShip).find('.TemplateType option:selected').val() == 1) {
                                OrderAutoId[1].PrintType = 1;
                                OrderAutoId[1].noofcopy = $(tblShip).find('.NoOfCopy input').val();
                                if ($(tblShip).find('.PrintOption span').attr('shippingid') == $(main).find('.SalesPerson span').attr('shipp')) {
                                    drlogorder1 += $(main).find('.SalesPerson span').attr('ordautoid') + ","
                                }
                                if ($(main).find('.OrderAutoIds').text() != "") {
                                    arrOrdAutoIdList = $(main).find('.OrderAutoIds').text().split(',');
                                    arrShipAutoIdList = $(main).find('.Shippings').text().split(',');
                                }
                                else {
                                    arrOrdAutoIdList.length = 0;
                                    arrShipAutoIdList.length = 0;
                                }
                                if (arrOrdAutoIdList.length != 0) {
                                    $.each(arrShipAutoIdList, function (n, or) {
                                        if ($(tblShip).find('.PrintOption span').attr('shippingid') == arrShipAutoIdList[n]) {
                                            drlogorder1 += arrOrdAutoIdList[n] + ",";
                                        }
                                    })
                                }
                            }
                            if ($(tblShip).find('.TemplateType option:selected').val() == 2) {
                                OrderAutoId[2].PrintType = 2;
                                OrderAutoId[2].noofcopy = $(tblShip).find('.NoOfCopy input').val();
                                if ($(tblShip).find('.PrintOption span').attr('shippingid') == $(main).find('.SalesPerson span').attr('shipp')) {
                                    drlogorder2 += $(main).find('.SalesPerson span').attr('ordautoid') + ","
                                }
                                if ($(main).find('.OrderAutoIds').text() != "") {
                                    arrOrdAutoIdList = $(main).find('.OrderAutoIds').text().split(',');
                                    arrShipAutoIdList = $(main).find('.Shippings').text().split(',');
                                }
                                else {
                                    arrOrdAutoIdList.length = 0;
                                    arrShipAutoIdList.length = 0;
                                }
                                if (arrOrdAutoIdList.length != 0) {
                                    $.each(arrShipAutoIdList, function (n, or) {
                                        if ($(tblShip).find('.PrintOption span').attr('shippingid') == arrShipAutoIdList[n]) {
                                            drlogorder2 += arrOrdAutoIdList[n] + ",";
                                        }
                                    })
                                }
                            }
                            if ($(tblShip).find('.TemplateType option:selected').val() == 3) {
                                OrderAutoId[3].PrintType = 3;
                                OrderAutoId[3].noofcopy = $(tblShip).find('.NoOfCopy input').val();
                                if ($(tblShip).find('.PrintOption span').attr('shippingid') == $(main).find('.SalesPerson span').attr('shipp')) {
                                    drlogorder3 += $(main).find('.SalesPerson span').attr('ordautoid') + ","
                                }
                                if ($(main).find('.OrderAutoIds').text() != "") {
                                    arrOrdAutoIdList = $(main).find('.OrderAutoIds').text().split(',');
                                    arrShipAutoIdList = $(main).find('.Shippings').text().split(',');
                                }
                                else {
                                    arrOrdAutoIdList.length = 0;
                                    arrShipAutoIdList.length = 0;
                                }
                                if (arrOrdAutoIdList.length != 0) {
                                    $.each(arrShipAutoIdList, function (n, or) {
                                        if ($(tblShip).find('.PrintOption span').attr('shippingid') == arrShipAutoIdList[n]) {
                                            drlogorder3 += arrOrdAutoIdList[n] + ","
                                        }
                                    })
                                }
                            }
                        }
                    }
                }
            });
        });
        OrderAutoId[0].orders = drlogorder; OrderAutoId[1].orders = drlogorder1; OrderAutoId[2].orders = drlogorder2; OrderAutoId[3].orders = drlogorder3;
        localStorage.setItem('DriverPackageprint', JSON.stringify(OrderAutoId));
        localStorage.setItem('EmailPrint','1');
        window.open("/Manager/DriverPackagePrint.aspx", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");
    }
}
function EnableShippingTemplate(n) {
    if (n == 1) {
        $("#ddlShipping").attr("disabled", true);
        $("#ddlTemplate").attr("disabled", true);
        $("#txtNumberOfCopy").attr("disabled", true);
    }
    else {
        if ($("#chkDrvCustomerCopy").prop("checked") == true) {
            $("#ddlShipping").removeAttr("disabled");
            $("#ddlTemplate").removeAttr("disabled");
            $("#txtNumberOfCopy").removeAttr("disabled");
        }
        else {
            $("#ddlShipping").attr("disabled", true);
            $("#ddlTemplate").attr("disabled", true);
            $("#txtNumberOfCopy").attr("disabled", true);
        }
    }
}
function setDate(value, type) {
    var DelFromDate = $("#txtDeliveryFromDate").val();
    var DelToDate = $("#txtDeliveryToDate").val();
    if (type == 1) {

        if (DelToDate == '') {
            $("#txtDeliveryToDate").val(DelFromDate);
        }

    } else if (type == 2) {
        if (DelFromDate == '') {
            $("#txtDeliveryFromDate").val(DelToDate);
        }
    }
}
function printGoogleMap(e) {
    var tr = $(e).closest('tr');
    var ExternalDriver = $(tr).find('.ExternalDriver').text();
    var maparray = []; maparray1 = [];
    var index = 0; index1 = 0;

   
    $('#tblRouteDetails tbody tr').each(function () {
        if (ExternalDriver == $(this).find('.Driver').text()) {
            maparray[index] = new Object();
            maparray[index].lat = Number($(this).find('.Customer span').attr('lat'));
            maparray[index].lng = Number($(this).find('.Customer span').attr('long'));
            maparray[index].stopNumber = $(this).find('.Stop').text();
            maparray[index].orderNo = $(this).find('.OrderNo').text();
            index = Number(index) + 1;
        }
    })
    localStorage.setItem('GoogleMap', JSON.stringify(maparray));

    $('#tblRouteDetails tbody tr').each(function () {
        if (ExternalDriver == $(this).find('.Driver').text()) {
            maparray1[index1] = new Object();
            maparray1[index1].lat = Number($(this).find('.Customer span').attr('lat'));
            maparray1[index1].lng = Number($(this).find('.Customer span').attr('long'));
            maparray1[index1].stopNumber = $(this).find('.Stop').text();
            maparray1[index1].orderNo = '<b>Order No:</b> ' + $(this).find('.OrderNo').text() + '<br/><b>Customer Name:</b> ' + $(this).find('.Customer span').attr('customer') + '<br/><b>Payable Amount:</b> ' + $(this).find('.Customer span').attr('amount') + '<br/><b>Sales Person:</b> ' + $(this).find('.SalesPerson').text();
            index1 = Number(index1) + 1;
        }
    })
  
    localStorage.setItem('GoogleMapTest', JSON.stringify(maparray1));

    window.open("/GoogleMap/LocationMap.aspx", "popUpWindow", "height=600,width=1030,left=10,top=10,,scrollbars=yes,menubar=no");

}

function viewAllRemarks(OrderAutoId) {
    $.ajax({
        type: "POST",
        url: "Manager_orderList.aspx/viewAllRemarks",
        data: "{'OrderAutoId':" + OrderAutoId + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            $.blockUI({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#FFF',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var xmldoc = $.parseXML(response.d);
            
            var orderRemarks = $(xmldoc).find("Table");
            var i = 1;
            
            $("#tblAllremarks tbody tr").remove();
            var row = $("#tblAllremarks thead tr").clone(true);
            if (orderRemarks.length > 0) {
                $("#tblAllremarksEmptyTable").hide();
                $.each(orderRemarks, function (index) {
                    $(".SRNO", row).text(i);                    
                    $(".EmpName", row).text($(this).find("EmpName").text());
                    $(".EmpType", row).text($(this).find("EmpType").text());
                    $(".Rmarks", row).text($(this).find("Remarks").text());

                    $("#tblAllremarks tbody").append(row);
                    row = $("#tblAllremarks tbody tr:last").clone(true);
                    i++;
                });
                
            }
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        },
        error: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
    $("#modalAllRemark").modal('show');
}