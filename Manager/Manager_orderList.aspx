﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="Manager_orderList.aspx.cs" Inherits="Manager_orderList" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .Pager {
            text-align: right;
        }

        #navbar1 ul li {
            float: left;
            margin: 2px;
            padding: 0 15px 0 15px;
        }

        .anyClass {
            height: 400px;
            overflow-y: scroll;
        }

        .incdec {
            width: 43px !important;
            height: 28px !important;
            border-radius: 2px;
            border: 1px blue inset;
        }

        .pad {
            padding: 0 !important
        }

        #tblRouteDetails th, #tblRouteDetails td {
            padding: 5px 5px !important;
        }

        #tblUnscheduledOrder th, #tblUnscheduledOrder td {
            padding: 5px 5px !important;
        }

        #tblDrvList th, #tblDrvList td {
            padding: 5px 5px !important;
        }

        .addScrollFirst {
            overflow-x: hidden !important;
            height: 520px;
            overflow-y:auto;

        }

        .addScroll {
            overflow-y: auto;
            height: 460px;
        }

        ul {
            list-style-type: none !important
        }

        .pac-container {
            z-index: 10000 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Order List</h3>
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/Admin/mydashboard.aspx">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Manage Order</a></li>
                        <li class="breadcrumb-item">Order List</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
            <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2"
                    id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    Action</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <button type="button" class="dropdown-item" onclick="checkSecurity()" data-animation="pulse" id="btnAssignRoot">Assign Route</button>
                    <button type="button" class="dropdown-item" onclick="popupPrintOption()">Print</button>
                    <button type="button" class="dropdown-item" onclick="bulkprint()">Print Tax Invoice</button>

                    <%--  <button type="button" class="dropdown-item" id="btnBulkPrint">Print</button>
                    <button type="button" class="dropdown-item" onclick="BulkPrintItem()">Bulk Print</button>--%>
                    <input type="hidden" id="OptimoKey" />
                    <input type="hidden" id="hfPlanningDate" />
                    <input type="hidden" id="hfDriverLimit" />
                </div>
            </div>
        </div>

    </div>

    <div class="content-body">
        <section id="drag-area">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <input id="HDDomain" runat="server" type="hidden" />
                        <input type="hidden" id="txtHOrderAutoId" class="form-control input-sm" />
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" style="width: 100% !important;" id="ddlCustomer" runat="server">
                                            <option value="0">All Customer</option>
                                        </select>
                                    </div>



                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">From Order Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Order From Date" id="txtSFromDate" onfocus="this.select()" />

                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text " style="padding: 0rem 1rem;">To Order Date <span class="la la-calendar-o"></span>

                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Order To Date" id="txtSToDate" onfocus="this.select()" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input type="text" class="form-control border-primary input-sm" placeholder="Order No" id="txtSOrderNo" onfocus="this.select()" />
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" style="padding: 0rem 1rem;">From Delivery Date <span class="la la-calendar-o"></span>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Delivery From Date" id="txtDeliveryFromDate" onfocus="this.select()" onchange="setDate(this.value,1)" />

                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text " style="padding: 0rem 1rem;">To Delivery Date <span class="la la-calendar-o"></span>

                                                </span>
                                            </div>
                                            <input type="text" class="form-control border-primary input-sm" placeholder="Delivery To Date" id="txtDeliveryToDate" onfocus="this.select()" onchange="setDate(this.value,2)" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group" id="SalesPerson">
                                        <select class="form-control border-primary input-sm" style="width: 100% !important;" id="ddlSalesPerson" runat="server">
                                           
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" style="width: 100% !important;" id="ddlSStatus">
                                           
                                        </select>
                                    </div>



                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" style="width: 100% !important;" id="ddlShippingType">
                                          
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" style="width: 100% !important;" id="ddlDriver">
                                            <option value="0">All Driver</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select id="ddlOrderType" style="width: 100% !important;" class="form-control border-primary input-sm">
                                            <option value="0">All Order Type</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select id="ddlOrderBy" style="width: 100% !important;" class="form-control border-primary input-sm">
                                            <option value="1">Sorting By Customer</option>
                                            <option selected="selected" value="2">Sorting By Order Date Time</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <select class="form-control border-primary input-sm" style="width: 100% !important;" id="ddlState">
                                           
                                        </select>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSearch">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <input type="hidden" id="hiddenPackerAutoId" runat="server" />
                                <input type="hidden" id="hiddenEmpType" runat="server" />
                                <div id="divSlectedOrder" class="row form-group text-right" style="display: none">
                                    <div class="col-md-12 text-rights">
                                        [ Total selected orders : <b id="bCountSelectedOrder"></b>]&nbsp;&nbsp;&nbsp;&nbsp;      
                                       [ Total Stop : <b id="bCountSelectedCustomers"></b>]
                                    </div>

                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="tblOrderList">
                                                <thead class="bg-blue white">
                                                    <tr>
                                                        <td class="action text-center">
                                                            <input type="checkbox" id="check-all" />
                                                            Action</td>
                                                        <td class="status text-center">Status</td>
                                                        <td class="orderNo text-center">Order No</td>
                                                        <td class="orderDt text-center">Order Date</td>
                                                        <td class="SalesPerson">Sales Person</td>
                                                        <td class="cust">Customer</td>
                                                        <td class="payableAmt price">Payable ($)</td>
                                                        <td class="product text-center">Products</td>
                                                        <td class="CreditMemo text-center">Credit Memo</td>
                                                        <td class="Shipping text-center">Shipping Type</td>
                                                        <td class="DeliveryDate text-center">Delivery Date</td>
                                                        <td class="OrderAutoId text-center" style="display: none;">OrderAutoId</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                            <h5 class="well text-center" id="EmptyTable" style="display: none">No data available.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2" style="padding-right: 110px">
                                        <div class="form-group">
                                            <select id="ddlPageSize" class="form-control border-primary input-sm">
                                                <option selected="selected" value="10">10</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                                <option value="500">500</option>
                                                <option value="1000">1000</option>
                                                <option value="0">All</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-12">
                                        <div class="Pager"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="modalSendMail" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row" style="width: 100%">
                        <div class="col-md-2">
                            <h4 class="modal-title"><b>Send Mail</b></h4>
                        </div>
                        <div class="col-md-3">
                            <input type="hidden" id="hdnPrintLabel" />
                            <b>Order No</b>&nbsp;:&nbsp;<span id="lblOrderSNo"></span>
                        </div>
                        <div class="col-md-7">
                            <b>Customer Name</b>&nbsp;:&nbsp;<span id="lblCustomerName"></span>
                        </div>

                    </div>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row form-group">
                                        <div class="col-sm-2">
                                            <label class="control-label">
                                                To<span class="required">&nbsp;*</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-10 form-group">
                                            <div class="input-group">
                                                <input type="text" id="txtTo" class="form-control border-primary input-sm reqMail" onfocus="this.select()" onchange="validateEmail(this);" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row form-group">
                                        <div class="col-sm-2">Subject<span class="required">&nbsp;*</span></div>
                                        <div class="col-sm-10 form-group">
                                            <div class="input-group">
                                                <input type="text" id="txtSubject" class="form-control border-primary input-sm reqMail" onfocus="this.select()" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row form-group">
                                        <div class="col-sm-2">Message</div>
                                        <div class="col-sm-10 form-group">
                                            <div class="input-group">
                                                <textarea id="txtEmailBody" class="form-control border-primary input-sm" rows="5" placeholder="Enter your message here" runat="server"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--  </div>--%>
                </div>
                <div class="modal-footer">
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" class="btn btn-success  buttonAnimation pull-right round box-shadow-1  btn-sm" data-animation="pulse" id="btnSendMail">Send</button>
                    </div>
                    <div class="btn-group mr-1 pull-right">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="modalMisc" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="card-title">Drivers</h4>
                    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row anyClass">
                        <div class="col-md-12">
                            <div id="insideText">
                            </div>
                            <div id="DriverText" style="display: none">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <%--  <div class="table-responsive" id="tblAssignDrv" style="display: none;">--%>
                                            <div class="row form-group">
                                                <div class="col-sm-2">Assign Date :</div>
                                                <div class="col-sm-5 form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text " style="padding: 0rem 1rem;">
                                                                <span class="la la-calendar-o"></span>
                                                            </span>
                                                        </div>


                                                        <input type="text" id="txtAssignDate" class="form-control border-primary input-sm reqdriv" onfocus="this.select()" />


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-sm-12">
                                                    <table class="table table-bordered table-striped" id="tblDriver">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="DrvName">Driver Name</td>
                                                                <td class="AsgnOrders text-center">Assigned Orders</td>
                                                                <td class="Select text-center">Select</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <%--   </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" style="display: none;" id="btnAsgn" onclick="assignDriver()">Assign</button>
                    <button type="button" class="btn btn-info buttonAnimation pull-right round box-shadow-1 btn-sm" style="display: none;" id="btnOk" onclick="packingConfirmed()" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="modalOrderLog" class="modal fade text-left show" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Order Log</h4>
                        </div>
                        <div class="col-md-12">
                            <label><b>Order No </b>&nbsp;:&nbsp;<span id="lblOrderNo"></span>&nbsp;&nbsp;<b>Order Date</b>&nbsp;:&nbsp;<span id="lblOrderDate"></span></label>
                        </div>
                    </div>



                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="tblOrderLog">
                            <thead class="bg-blue white">
                                <tr>
                                    <td class="SrNo text-center">SN</td>
                                    <td class="ActionBy">Action By</td>
                                    <td class="Date text-center">Date</td>
                                    <td class="Action">Action</td>
                                    <td class="Remark" style="white-space: normal">Remark</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div id="PopPrintTemplate" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Select Invoice Template</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="chkdefault" />
                                            <label for="chkdefault">PSM Default  - Invoice Only</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="chkdueamount_Tax"/>
                                            <label for="chkdueamount">PSM Default With Tax</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="chkdueamount" />
                                            <label for="chkdueamount">PSM Default</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="PackingSlip" />
                                            <label for="PackingSlip">Packing Slip</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="WithoutCategorybreakdown" />
                                            <label for="PackingSlip">Packing Slip - Without Category Breakdown</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row" id="divPSMPADefault" style="display: none">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="PSMPADefault" />
                                            <label for="PSMPADefault">PSM PA Default</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row" id="divPSMWPADefault" style="display: none">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="PSMWPADefault" />
                                            <label for="PSMWPADefault">PSM WPA-PA</label>
                                        </div>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                    <div class="row form-group" id="divPSMNPADefault" style="display: none">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="rPSMNPADefault" />
                                            PSM NPA Default
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="Template1" />
                                            <label for="Template1">7 Eleven (Type 1)</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="Template2" />
                                            <label for="Template2">7 Eleven (Type 2)</label>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="Templ1" />
                                            Template 1
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="Templ2" />
                                            Template 2
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row" id="divPSMWPANDefault">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="PSMWPANDefault" />
                                            <label for="PSMWPADefault">PSM PA - N</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="PrintOrder()">Print</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="ShipAddressPopup" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                   
                    <div class="row">
                         <div class="col-md-12 col-sm-12  form-group">
                            <h4 class="modal-title"><strong>Order No :</strong> <span id="lblOrderNumber"></span></h4>                          
                            <h4 class="modal-title"><strong>Customer Name :</strong> <span id="lblCustomername"></span></h4>
                        </div>
                        <div class="col-md-12">
                            <h4 class="modal-title">Shipping Address   <span style="color: red; text-align: right; font-size: 12px;">(Please enter Address, City, State and Zipcode)</span></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12 col-sm-12  form-group">
                            <label class="control-label">Address 1</label>
                            <input type="hidden" id="hdnordAutoId" />
                            <input type="text" id="txtshippingaddress" placeholder="Please Enter Shipping Address" autocomplete="off" class="form-control border-primary input-sm" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12  form-group">
                            <label class="control-label">Address 2</label>
                            <input type="text" id="txtshippingaddress2" placeholder="Suite/Apartment" autocomplete="off" class="form-control border-primary input-sm" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12  form-group">
                            <label class="control-label">Address</label>
                            <input type="text" id="txtaddress" placeholder="Please Enter Address" autocomplete="off" class="form-control border-primary input-sm" disabled />
                        </div>
                        <div class="col-md-6 col-sm-12  form-group">
                            <label class="control-label">Zip code</label>
                            <input type="text" id="txtZipCode" disabled maxlength="5" class="form-control border-primary input-sm" />
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12  form-group">
                            <label class="control-label">City</label>
                            <input type="text" id="txtCity" autocomplete="off" disabled class="form-control border-primary input-sm" />
                        </div>
                        <div class="col-md-6 col-sm-12  form-group">
                            <label class="control-label">State</label>
                            <input type="text" id="txtState" disabled class="form-control border-primary input-sm" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12  form-group">
                            <label class="control-label">Latitude</label>
                            <input type="text" id="txtLats" disabled class="form-control border-primary input-sm" />
                        </div>

                        <div class="col-md-6 col-sm-12  form-group">
                            <label class="control-label">Longitude</label>
                            <input type="text" id="txtLongitude" disabled class="form-control border-primary input-sm" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12  form-group">
                            <span id="lblLat"></span>
                            <span id="lblLong"></span>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="UpdateShipAdd()">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalDriverRemark" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title">Remark</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12  form-group">
                            <label class="control-label">Driver Remark</label>
                            <input type="hidden" id="hfOrdAutoId" />
                            <textarea id="txtDriverRemark" maxlength="500" placeholder="Driver Remark" class="form-control border-primary req" row="2"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="UpdateDriverRemark()">Update</button>
                </div>
            </div>
        </div>
    </div>
    <div id="popupOptimoOrderDetails" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row w-100">
                        <div class="col-md-2">
                            <h4 class="modal-title">Order Details</h4>
                        </div>
                        <div class="col-md-2">
                            <h4 class="modal-title">[<span>Total Stop :  <b id="totalCustomerCount"></b></span>]</h4>
                        </div>
                        <div class="col-md-2">
                            <h4 class="modal-title">[<span>Route Order :  <b id="totalOrderCount"></b></span>]</h4>
                        </div>
                        <div class="col-md-2">
                            <h4 class="modal-title">[<span>Unroute Order :  <b id="totalUnRouteOrderCount"></b></span>]</h4>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control border-primary input-sm text-center" onchange="ChangePlanningDate()" id="txtPlanningDate" placeholder="Select Planning Date" />
                        </div>
                        <div class="col-md-1">
                            <input type="text" class="form-control border-primary input-sm text-center w-25" value="15" id="txtDuration" placeholder="Duration" />
                        </div>
                    </div>
                </div>
                <div class="modal-body addScrollFirst">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body" id="navbar1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                Balancing :
                                                 <ul>
                                                     <li>
                                                         <input type="radio" name="balancing" onclick="NoBalancing()" class="form-check-input" id="chkNoBalancing" />No Balancing</li>
                                                     <li>
                                                         <input type="radio" name="balancing" onclick="BalanceRoute()" class="form-check-input" id="chkBalance" />Balance routes</li>
                                                     <li>
                                                         <input type="radio" name="balancing" onclick="BalanceUseDriver()" checked class="form-check-input" id="chkBalanceByDriver" />Balance routes and use all drivers</li>
                                                 </ul>
                                            </div>
                                            <div class="col-md-4" id="divBalanceBy">
                                                Balance by :
                                                  <ul>
                                                      <li>
                                                          <input type="radio" name="balanceBy" checked class="form-check-input" id="chkWorkTime" />Work time</li>
                                                      <li>
                                                          <input type="radio" name="balanceBy" class="form-check-input" id="chkNoOfOrdPerDriver" />Number of orders per driver</li>
                                                  </ul>
                                            </div>
                                            <div class="col-md-2" id="divBalanceFactor">
                                                Balancing Factor :
                                                <select id="ddlBalancingFactor" class="form-control border-primary input-sm w-50">
                                                    <option value="0.0">-Select-</option>
                                                    <option value="0.1">10%</option>
                                                    <option value="0.2">20%</option>
                                                    <option value="0.3">30%</option>
                                                    <option value="0.4">40%</option>
                                                    <option value="0.5">50%</option>
                                                    <option value="0.6">60%</option>
                                                    <option value="0.7" selected="selected">70%</option>
                                                    <option value="0.8">80%</option>
                                                    <option value="0.9">90%</option>
                                                    <option value="1.0">100%</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body" id="navbar2">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <div id="divDriverList"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-9 col-sm12">
                                                <ul class="nav nav-tabs nav-topline">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="base-route" data-toggle="tab" aria-controls="tab21" href="#route" aria-expanded="true">Route Order </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="base-notroute" data-toggle="tab" aria-controls="tab22" href="#notroute" aria-expanded="false">Unroute Order</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="tab-content px-1 pt-1 border-grey border-lighten-2 border-0-top">
                                            <div role="tabpanel" class="row tab-pane active" id="route" aria-expanded="true" aria-labelledby="base-route">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered" id="tblRouteOrder">
                                                                <thead class="bg-blue white">
                                                                    <tr>
                                                                        <td rowspan="2" class="Action text-center">Action</td>
                                                                        <td rowspan="2" class="OrderAutoId" style="display: none">Order AutoId</td>
                                                                        <td rowspan="2" class="SrNo text-center" style="display: none">SN</td>
                                                                        <td rowspan="2" class="OrderNo text-center">Order No</td>
                                                                        <td rowspan="2" class="Customer" style="display: none">Customer</td>
                                                                        <td rowspan="2" class="Cust">Customer</td>
                                                                        <td rowspan="2" class="SalesPerson">Sales Person</td>
                                                                        <td rowspan="2" class="Lat text-center" style="display: none">Lat</td>
                                                                        <td rowspan="2" class="Long text-center" style="display: none">Long</td>
                                                                        <td colspan="2" class="text-center">Delivery Time</td>
                                                                        <td rowspan="2" class="Address" style="display: none">Address</td>
                                                                        <td rowspan="2" class="Ships" style="display: none">Shippings</td>
                                                                        <td rowspan="2" class="Orders" style="max-width: 300px !important; white-space: normal !important">Other Orders</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="From text-center">From</td>
                                                                        <td class="To text-center">To</td>
                                                                    </tr>
                                                                    <tr style="display: none">
                                                                        <td class="Action text-center">Action</td>
                                                                        <td class="OrderAutoId" style="display: none">Order AutoId</td>
                                                                        <td class="SrNo text-center" style="display: none">SN</td>
                                                                        <td class="OrderNo text-center">Order No</td>
                                                                        <td class="Customer" style="display: none">Customer</td>
                                                                        <td class="Cust">Customer</td>
                                                                        <td class="SalesPerson">Sales Person</td>
                                                                        <td class="Lat text-center" style="display: none">Lat</td>
                                                                        <td class="Long text-center" style="display: none">Long</td>
                                                                        <td class="From text-center">From</td>
                                                                        <td class="To text-center">To</td>
                                                                        <td class="Address" style="display: none">Address</td>
                                                                        <td class="Ships" style="display: none">Shippings</td>
                                                                        <td class="Orders" style="max-width: 300px !important; white-space: normal !important">Other Orders</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div role="tabpanel" class="row tab-pane" id="notroute" aria-expanded="true" aria-labelledby="base-notroute">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered" id="tblUnRouteOrder">
                                                                <thead class="bg-blue white">
                                                                    <tr>
                                                                        <td rowspan="2" class="SrNo text-center" style="display: none">SN</td>
                                                                        <td rowspan="2" class="OrderNo text-center">Order No</td>
                                                                        <td rowspan="2" class="Customer" style="display: none">Customer</td>
                                                                        <td rowspan="2" class="Cust">Customer</td>
                                                                        <td rowspan="2" class="SalesPerson">Sales Person</td>
                                                                        <td rowspan="2" class="Lat text-center" style="display: none">Lat</td>
                                                                        <td rowspan="2" class="Long text-center" style="display: none">Long</td>
                                                                        <td colspan="2" class="text-center">Delivery Time</td>
                                                                        <td rowspan="2" class="Address" style="display: none">Address</td>
                                                                        <td rowspan="2" class="Orders" style="max-width: 300px !important; white-space: normal !important">Other Orders</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="From text-center">From</td>
                                                                        <td class="To text-center">To</td>
                                                                    </tr>
                                                                    <tr style="display: none">
                                                                        <td class="SrNo text-center" style="display: none">SN</td>
                                                                        <td class="OrderNo text-center">Order No</td>
                                                                        <td class="Customer" style="display: none">Customer</td>
                                                                        <td class="Cust">Customer</td>
                                                                        <td class="SalesPerson">Sales Person</td>
                                                                        <td class="Lat text-center" style="display: none">Lat</td>
                                                                        <td class="Long text-center" style="display: none">Long</td>
                                                                        <td class="From text-center">From</td>
                                                                        <td class="To text-center">To</td>
                                                                        <td class="Address" style="display: none">Address</td>
                                                                        <td class="Orders" style="max-width: 300px !important; white-space: normal !important">Other Orders</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row w-100">
                        <div class="col-md-6 text-left">
                            <h4 class="modal-title"><span>[ Total selected Stop :  <b id="totalCheckedOrder"></b>]</span>  <span style="margin-right: 50px;">[ Total added driver :  <b id="totalAddedDriver"></b>] </span></h4>

                        </div>
                        <div class="col-md-6 text-right">
                            <button type="button" onclick="PlanRouteClose()" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse">Close</button>
                            <button type="button" style="margin-right: 5px;" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnPlanRoute" onclick="deleteAll()">Plan Route</button>
                            <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" style="display: none" id="btnGetRoute" onclick="GetPlanningStatus()">Get Route</button>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="popupRouteDetails" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row w-100">
                        <div class="col-md-2">
                            <h4 class="modal-title">Route Details</h4>
                        </div>
                        <div class="col-md-7">
                            <h4 class="modal-title"><span style="margin: 25px">Total Stop :  <b id="totalCustomer"></b></span>
                                <span style="margin: 25px">Total Order :  <b id="totalOrder"></b></span>
                                <span style="margin: 25px">Scheduled :  <b id="btotalSchedule"></b></span>
                                <span style="color: red">Unscheduled :  <b id="btotalUnschedule"></b></span>
                            </h4>
                        </div>
                        <div class="col-md-3 text-right">
                            <h4 class="modal-title">[<span>Delivery Date :  <b id="bDelDate"></b></span>]</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body addScroll" id="navbar">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered" id="tblDrvList">
                                                        <thead class="bg-blue white">
                                                            <tr>
                                                                <td class="ExternalDriver text-center">Driver No</td>
                                                                <td class="DriverList">Assign Driver</td>
                                                                <td class="Stop text-center">Stop</td>
                                                                <td class="TotalOrder text-center">Orders</td>
                                                                <td class="drvStartTime text-center">Start Time</td>
                                                                <td class="drvEndTime text-center">End Time</td>
                                                                <td class="drvCar text-center">Car</td>
                                                                <td class="drvAction text-center">Action</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-9 col-sm12">
                                                <ul class="nav nav-tabs nav-topline">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="base-scheduled" onclick="showHideCount(1)" data-toggle="tab" aria-controls="tab21" href="#scheduled" aria-expanded="true">Scheduled Order List</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="base-unscheduled" onclick="showHideCount(2)" data-toggle="tab" aria-controls="tab22" href="#unscheduled" aria-expanded="false">Unscheduled Order List</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3 text-right" style="display: none">
                                                <ul class="nav nav-tabs nav-topline">
                                                    <li class="float-right">
                                                        <span id="schCount">Total Sechedule Order : <b id="scheduleCount"></b></span><span id="unschCount" style="display: none">Total Unschedule Order : <b id="unscheduleCount"></b></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="tab-content px-1 pt-1 border-grey border-lighten-2 border-0-top">
                                            <div role="tabpanel" class="row tab-pane active" id="scheduled" aria-expanded="true" aria-labelledby="base-scheduled">

                                                <div class="row form-group">
                                                    <div class="col-md-12">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered" id="tblRouteDetails">
                                                                <thead class="bg-blue white">
                                                                    <tr>
                                                                        <td rowspan="2" class="OrderNo text-center">Order No</td>
                                                                        <td rowspan="2" class="Customer">Customer</td>
                                                                        <td rowspan="2" class="SalesPerson">Sales Person</td>
                                                                        <td colspan="2" class="text-center">Delivery Time</td>
                                                                        <td rowspan="2" class="Driver text-center">Driver</td>
                                                                        <td rowspan="2" class="Stop text-center">Stop</td>
                                                                        <td rowspan="2" class="scheduledAt text-center">Schedule At</td>
                                                                        <td rowspan="2" class="Remark" style="white-space: normal">Remark</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="From text-center" style="width: 75px !important; display: none">From</td>
                                                                        <td class="To text-center" style="width: 75px !important; display: none">To</td>
                                                                    </tr>
                                                                    <tr style="display: none">
                                                                        <td class="OrderNo text-center">Order No</td>
                                                                        <td class="Customer" style="max-width: 300px !important; white-space: normal !important">Customer</td>
                                                                        <td class="SalesPerson">Sales Person</td>
                                                                        <td class="From text-center">From</td>
                                                                        <td class="To text-center">To</td>
                                                                        <td class="Driver text-center">Driver</td>
                                                                        <td class="Stop text-center">Stop</td>

                                                                        <td class="scheduledAt text-center">Schedule At</td>
                                                                        <td class="Remark" style="white-space: normal">Remark</td>
                                                                        <td class="OrderAutoIds" style="display: none">Other Orders</td>
                                                                        <td class="Shippings" style="display: none">Shippings</td>

                                                                        <td class="Orders" style="max-width: 300px !important; white-space: normal !important; display: none">Other Orders</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                            <h5 class="well text-center" id="ScheduledEmptyTable" style="display: none">No data available.</h5>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row tab-pane" id="unscheduled" aria-labelledby="base-unscheduled">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered" id="tblUnscheduledOrder">
                                                                <thead class="bg-blue white">
                                                                    <tr>
                                                                        <td rowspan="2" class="OrderNo text-center">Order No</td>
                                                                        <td rowspan="2" class="Customer" style="max-width: 300px !important; white-space: normal !important">Customer</td>
                                                                        <td rowspan="2" class="SalesPerson">Sales Person</td>
                                                                        <td colspan="2" class="text-center">Delivery Time</td>
                                                                        <td rowspan="2" class="Remark" style="white-space: normal">Remark</td>
                                                                    </tr>
                                                                    <tr style="display: none">
                                                                        <td class="OrderNo text-center">Order No</td>
                                                                        <td class="Customer" style="max-width: 300px !important; white-space: normal !important">Customer</td>
                                                                        <td class="SalesPerson">Sales Person</td>
                                                                        <td class="From text-center">From</td>
                                                                        <td class="To text-center">To</td>
                                                                        <td class="Remark" style="white-space: normal">Remark</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                            <h5 class="well text-center" id="UnScheduledEmptyTable" style="display: none">No data available.</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="closeRoutePopup()" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm pulse">Close</button>
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnSaveRoute" onclick="SaveRouteDetails()">Save</button>
                    <button type="button" class="btn btn-warning buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnRePlanRoute" onclick="ReplanRoute()">Replan Route</button>
                    <%--<button type="button" style="display:none" class="btn btn-info buttonAnimation pull-right round box-shadow-1  btn-sm" id="btnPrint" >Print</button>--%>

                    <input type="hidden" id="hfPlanningId" />
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="mdSecurityCheck" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Security Check </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">Security</div>
                        <div class="col-md-9">
                            <input type="password" id="txtOptimoKey" class="form-control input-sm border-primary" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <span id="errormsgVoid"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1 btn-sm" onclick="clickonSecurity()">OK</button>
                    <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1 btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modalPrintOption" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Select Invoice Template</h4>
                        <button type="button" class="close pull-right" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="chkPsmDefaultOption" />
                                            <label for="chkdefault">PSM Default</label>
                                        </div>
                                    </div>
                                    <%-- <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="chkTemplateOneOption" />
                                            <label for="chkdefault">Template 1</label>
                                        </div>
                                    </div>--%>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <input type="radio" name="Template" id="chkTemplateTwoOption" />
                                            <label for="chkdefault">Template 2</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="PrintOptionOrder()">Print</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalDriverOrderLog" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row w-100">
                            <div class="col-md-2">
                                <h4 class="modal-title">Driver Order Details</h4>
                            </div>
                            <div class="col-md-4 text-right">
                                <h4>[ Driver Name : <b id="bDriverName"></b>]</h4>
                            </div>
                            <div class="col-md-3">
                                <h4>[ Total Stop : <b id="bTotalStop"></b>]</h4>
                            </div>
                        </div>
                        <button type="button" class="close pull-right" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body addScroll" id="navbar12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered" id="tblDriverOrderLog">
                                                    <thead class="bg-blue white">
                                                        <tr>
                                                            <td class="Stop text-center">Stop</td>
                                                            <td class="Customer" style="max-width: 200px !important; white-space: normal !important">Customer</td>
                                                            <td class="OrderNo text-center">Order No</td>
                                                            <td class="SalesPerson">Sales Person</td>
                                                            <td class="ScheduleAt text-center">Schedule At</td>
                                                            <td class="Remark text-center" style="white-space: normal">Remark</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                <h5 class="well text-center" id="DriverOrderLogEmptyTable" style="display: none">No data available.</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalDriverPrintOption" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="min-width: 550px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Select Invoice Template</h4>
                        <button type="button" class="close pull-right" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" style="padding: 0 !important;">
                                <div class="container" style="padding: 3px !important;">

                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered" id="tblDriverPrintOption">
                                                    <thead class="bg-blue white">
                                                        <tr style="display: none;">
                                                            <td class="Action text-center">Action</td>
                                                            <td class="PrintOption">Print Option</td>
                                                            <td class="TemplateType" style="min-width: 200px">Template Type</td>
                                                            <td class="NoOfCopy">NoOfCopy</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                <h5 class="well text-center" id="tblDriverPrintOptionEmptyTable" style="display: none">No data available.</h5>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="hfDrvIdForPrint" />
                        <input type="hidden" class="hiddenShipAddress" />
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success buttonAnimation pull-right round box-shadow-1  btn-sm" onclick="PrintDrvOrder()">Print</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modalAllRemark" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="min-width: 550px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">View All Remarks</h4>
                        <button type="button" class="close pull-right" data-dismiss="modal" style="margin-right: 0;">×</button>
                    </div>
                    <div class="modal-body ">
                        <div class="row">
                            <div class="col-md-12" style="padding: 0 !important;">
                                <div class="container" style="padding: 3px !important;">

                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered" id="tblAllremarks">
                                                    <thead class="bg-blue white">
                                                        <tr>
                                                            <td class="SRNO text-center">Sr. No</td>
                                                            <td class="EmpType">Emp Type</td>
                                                            <td class="EmpName">Emp Name</td>
                                                            <td class="Rmarks" style="min-width: 200px; white-space: normal">Remark</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                                <h5 class="well text-center" id="tblAllremarksEmptyTable" style="display: none">No data available.</h5>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger buttonAnimation pull-right round box-shadow-1  btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../js/summernote.js"></script>
    <link href="../css/summernote.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            $('#txtEmailBody').summernote();

        });
    </script>

    <script type="text/javascript">
        document.write('<scr' + 'ipt type="text/javascript" src="/Manager/JS/Manager_OrderList.js?v=' + new Date() + '"></sc' + 'ript>');
    </script>
    <script>
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('txtshippingaddress'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                setData(place);

            });
        });
        function setData(place) {
            var m = 0, check = 0;
            var addressShip = '';
            $(".hiddenShipAddress").html(place.adr_address);
            var textdata = $(".hiddenShipAddress").find(".street-address").text();//+ " ," + $(".hiddenShipAddress").find(".locality").text();
            $("#txtshippingaddress").val('');
            $("#txtaddress").val(textdata);
            $("#txtshippingaddress").val(textdata);
            for (var i = 0; i < place.address_components.length; i++) {
                for (var j = 0; j < place.address_components[i].types.length; j++) {


                    if (place.address_components[i].types[j] == "postal_code") {

                        var zcode = place.address_components[i].long_name;
                        $("#txtZipCode").val(zcode);
                    }
                    else if (place.address_components[i].types[j] == "administrative_area_level_1") {
                        $("#txtState").val(place.address_components[i].long_name);
                    }
                    if (place.address_components[i].types[j] == "locality") {
                        $("#txtCity").val(place.address_components[i].long_name);
                        check = 1;
                    }
                    if (place.address_components[i].types[j] == "neighborhood" && check == 0) {
                        $("#txtCity").val(place.address_components[i].long_name);
                        city = place.address_components[i].long_name;
                        check = 1;
                    }
                    if (place.address_components[i].types[j] == "administrative_area_level_3" && check == 0) {
                        $("#txtCity").val(place.address_components[i].long_name);
                        city = place.address_components[i].long_name;
                    }
                }
                m++;
            }
            if (m > 4) {
                $("#txtLats").val(place.geometry.location.lat);
                $("#txtLongitude").val(place.geometry.location.lng);
                $("#txtLat").val(place.geometry.location.lat);
                $("#txtLong").val(place.geometry.location.lng);
            }
            else {
                swal("", "Invalid address !", "error");
                resetField('');
            }
        }
        function checkZipcode(zipcode) {
            var zipcodeHtml = ``;
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/Sales/WebAPI/customerMaster.asmx/checkZipcode",
                dataType: "json",//$('option:selected', a).val();
                data: "{'ZipCode':'" + zipcode + "'}",
                success: function (response) {
                    var xmldoc = $.parseXML(response.d);

                    var zipcodeData = $(xmldoc).find('Table');
                    if (zipcodeData.length == 0) {
                        swal("", "Zipcode not registered !", "error");
                        resetField('');
                    } else if (zipcodeData.length > 0) {
                        // alert('exist');
                    }
                }
            });
        }

        function resetField(val) {
            if (val == '') {
                $("#txtshippingaddress").val('');
                $("#txtLat").val('');
                $("#txtLong").val('');
                $("#lblLat").html('');
                $("#lblLong").html('');
            }
        }
        function validateAddress() {
            $("#txtLat").val('');
            $("#txtLong").val('');

        }
    </script>
    <input type="hidden" id="txtLat" />
    <input type="hidden" id="txtLong" />
</asp:Content>

